/*!****************************************************************************
 *
 * @file
 * SK_BoxIntf.h
 *
 * Contains the specification of #SK_BoxIntf, which is a generic interface for
 * storage objects.
 *
 * #SK_BoxIntf makes a large effort to be as configurable as possible, whilst
 * maintaining a high-degree of implementation freedom.
 *
 *
 *
 *
 * KNOWN PROBLEMS
 *
 * 1)
 * 	How exactly could this data structure be exposed in publicly-accessible
 * 	headers? Consider a library that uses this interface, gets compiled
 * 	with a specific version of this header, and then a program linking to
 *	it uses a different version of the header. Wouldn't it be possible
 * 	for the library and the program linking to it to have different
 * 	definitions of #SK_BoxIntf without either knowing that? That could
 * 	cause some very dangerous silent errors.
 *
 * 	Essentially, every publicly-accessible header has this potential
 * 	problem with structs, which is why it is not good practice to expose
 *	their definitions unless absolutely necessary. Unfortunately, I don't
 *	see how this interface specification is useful without exposing its
 *	definition. (Perhaps THAT assumption is incorrect.)
 *
 *	Arguably, this struct should ideally never change once
 *	publicly-exposed, much like a public interface composed of function
 *	declarations. After all, this struct is an "*Intf". However, there are
 *	modifications that would be considered ABI breakages with #SK_BoxIntf
 *	that would not be for a simple list of function declarations. EX :
 *	Adding a function to #SK_BoxIntf, so the result of calling
 *	(sizeof (SK_BoxIntf)) is now different.
 *
 *	Is the take-away here that every change to #SK_BoxIntf represents a
 *	major-semantic-version increment? As of right now, that would be the
 *	current solution.
 *
 *	Would including a semantic version in the identifier of
 *	#SK_BoxIntf ameliorate this problem? Perhaps "SK_BoxIntf"
 *	could be a macro that expands to "SK_BoxIntf_v_0_0_1" or something
 *	similar. That way, at least version mis-matches could be easily
 *	detected with an #error directive.
 *
 *****************************************************************************/




#ifndef SK_BOX_INTF_H
#define SK_BOX_INTF_H




#include <SlyResult-0-x-x.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>




#include "SK_MemIntf.h"
#include "SK_MemStd.h"
#include "SK_misc.h"




/*!
 *  Function pointer type used in #SK_BoxIntf to represent a function that
 *  copies a constant number of bytes from #src to #dst.
 *
 *  The number of bytes copied is determined by the function itself. For
 *  example, SK_byte_copy_b3 () copies 3 bytes from #src to #dst.
 *
 *  @param dst			Where to copy bytes TO.
 *
 *  				This must NOT be NULL.
 *
 *  @param src			Where to copy bytes FROM
 *
 *				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

typedef

SlyDr (* SK_BoxIntf_prim_copy_func)
(
	void *		dst,

	const void *	src
);




/*!
 *  Function pointer type used in #SK_BoxIntf to represent a function that
 *  swaps a constant number of bytes between 2 memory regions.
 *
 *  The number of bytes swapped is determined by the function itself. For
 *  example, SK_byte_swap_b3 () swaps 3 bytes between #ptr_0 and #ptr_1.
 *
 *  @param ptr_0		A memory region to swap bytes with.
 *
 *  				This must NOT be NULL.
 *
 *  @param ptr_1		Another memory region to swap bytes with.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code.
 */

typedef

SlyDr (* SK_BoxIntf_prim_swap_func)
(
	void *	ptr_0,

	void *	ptr_1
);




// TODO : Resume work on #SK_BoxReqsRegi after #SK_Regi is complete
//
// /*!
//  *  This type is used inside of #SK_BoxStatReqs for the field #req_regi. It
//  *  is given a definition inside of "SK_BoxReqsRegi.c".
//  *
//  *  This declaration is placed here so that this file does not need to #include
//  *  "SK_BoxReqsRegi.h", as many users of #SK_BoxIntf will not require it.
//  */
//
// typedef struct SK_BoxReqsRegi SK_BoxReqsRegi;




/*!
 *  Used to describe a set of desired static parameters for a storage object
 *  implementing the interface #SK_BoxIntf. These parameters MUST stay constant
 *  for the lifetime of the storage object that uses them.
 *
 *  It is assumed that these parameters will need to be known when the data
 *  structure is initialized.
 *
 *  @warning
 *  These parameters should NOT change over the lifetime of the associated
 *  instance of the data structure implementing #SK_BoxIntf.
 */

typedef struct SK_BoxStatReqs
{

	/*!
	 *  The interface that should be used to perform memory allocations /
	 *  deallocations in the storage data structure.
	 *
	 *  If uncertain, this should be made to point to #SK_MemStd.
	 */

	const SK_MemIntf * mem;




	/*!
	 *  The size in bytes of the elements that will be stored inside of the
	 *  storage data structure.
	 *
	 *  @note
	 *  See the foot-note "Consequences of #elem_size" before making any
	 *  design decisions related to this field. It has a surprisingly large
	 *  effect on the design of #SK_BoxIntf.
	 */

	size_t elem_size;




	/*!
	 *  A hint describing the number of elements to allocate as part of the
	 *  static memory of the data structure.
	 *
	 *  Since these elements would be stored in the static memory of the
	 *  storage data structure, a pointer to an array is unnecessary to
	 *  access these elements. In other words, this hint generally allows
	 *  for some number of elements to be accessed with 1 less memory
	 *  dereference operation.
	 *
	 *  The penalty for using this hint is that since these elements would
	 *  be stored in the static memory of the storage data structure, they
	 *  can not be removed if less elements are actually needed. This makes
	 *  the data structure more memory-inefficient for small sets.
	 *
	 *  @warning
	 *  Since this is a hint, it CAN be ignored by the storage data
	 *  structure.
	 */

	size_t hint_num_stat_elems;




	/*!
	 *  A hint describing the minimum number of elements that should be
	 *  stored in contigious memory sections at a time.
	 *
	 *  This is a very useful hint to have, because having several elements
	 *  close together in memory makes the storage data structure more
	 *  cache-friendly. This can have a huge impact on how the storage data
	 *  structure performs when elements are accessed in sequential
	 *  indices.
	 *
	 *  For example, a linked-list data structure that stores elements in
	 *  groups could use this hint to select the number elements stored in
	 *  each grouping separated by links. If this value is 16 as opposed to
	 *  1, that would cut the number of links to follow by 16 for the
	 *  average index. Since following each link typically incurs a
	 *  cache-miss, that is a huge difference in performance.
	 *
	 *  If this is 0, the storage data structure should select what it
	 *  believes to be best.
	 *
	 *  @warning
	 *  Since this is a hint, it CAN be ignored by the storage data
	 *  structure.
	 */

	size_t hint_elems_per_block;




	/*!
	 *  A hint that indicates a usable #SK_BoxReqsRegi which may be used by
	 *  storage objects to store #SK_BoxReqs.
	 *
	 *  This CAN be NULL, in which case no #SK_BoxReqsRegi will be
	 *  specified by this #SK_BoxStatReqs. If this is NOT NULL, then the
	 *  #SK_BoxReqsRegi specified by #req_req MUST be valid during the
	 *  life-time of all the storage objects that have made use of this
	 *  #SK_BoxStatReqs.
	 *
	 *  #SK_BoxIntf implementations MAY require #req_req to be non-NULL in
	 *  order to initialize storage objects. To determine if this is the
	 *  case, the function #SK_BoxIntf.need_req_regi () can be used.
	 *
	 *  The purpose of #SK_BoxReqsRegi is that it provides a place to store
	 *  #SK_BoxReqs that can be shared by multiple storage objects, so that
	 *  duplication of #SK_BoxReqs structures in memory is unnecessary.
	 *  This is very useful for #SK_BoxIntf implementations that have
	 *  sub-storage-objects within storage-objects. Those
	 *  sub-storage-objects still need to be configured, so unless those
	 *  configurations were stored "somewhere else", then each
	 *  configuration would need to be stored per-storage-object, although
	 *  1000's of storage-objects may have the same configuration for their
	 *  sub-storage-objects.
	 *
	 *  Note that this #SK_BoxReqsRegi MAY be the the one that contains the
	 *  #SK_BoxReqs that references this #SK_BoxStatReqs instance.
	 *
	 *  @note
	 *  All of the SK_BoxReqsRegi_* () functions that will interact with
	 *  #req_regi are thread-safe.
	 *
	 *  FIXME : Ensure this field is a good idea, this idea is still very
	 *  new
	 */

	//SK_BoxReqsRegi * req_regi;




	//FIXME : Similar to #req_regi, would it be useful to have a pointer to
	//a (SK_BoxJitRegi) in this struct, which would be a registry of
	//compiled #SK_BoxJit's?




	//FIXME : Should this be a hint?
	//
	//	size_t	hint_recursion_level;
	//
	//This hint would indicate how many times an #SK_BoxIntf implementation
	//should use itself to implement sub-storage-objects.
	//
	//For example, if #hint_recursion_level were 3, then a tree #SK_BoxIntf
	//implementation could store its element blocks as trees, which also
	//store their element blocks as trees, which also store their element
	//blocks as trees, which store their element blocks as arrays at the
	//end.
	//
	//If such a hint were provided, should it be a static hint or a dynamic
	//hint? I currently think that it only makes sense as a static hint,
	//because changing it from say 3 to 10 would require a great deal of
	//dynamic memory allocation for the example above.




	//FIXME : Should there be a flag called "lockable" here? This could
	//represent whether or not the storage object should store a lock that
	//need to be reserved whenever modifiying it. That would make resize
	//operations thread-safe, for instance. While this seems convenient, I
	//believe it may be better to require such a lock to be external,
	//because whatever is managing the threads could then make sure that
	//the new threads are aware of the size change.




	//FIXME : Should there be a flag called "elem_lockable", which
	//indicates that each element has a lock associated with it? If this
	//were the case, then reading / writing to the same element from
	//multiple threads could automatically be made thread safe




	//FIXME : Should there be a hint here for "max_avg_links" for use by
	//linked-list / tree implementations of #SK_BoxIntf? It would represent
	//how many links on average every node should have to other nodes. For
	//a tree, this would represent how many children every node should have
	//on average. For a linked-list, this would represent if it should be
	//forward, forward-and-backwards, or something else entirely.

}

SK_BoxStatReqs;




/*!
 *  Used to describe a set of IMPLIED static parameters for a storage object
 *  implementing the interface #SK_BoxIntf. These parameters MUST stay constant
 *  for the lifetime of the storage object that uses them. The values in this
 *  struct are derived from values contained in #SK_BoxStatReqs.
 *
 *  Unlike #SK_BoxStatReqs, these values are not permitted to be changed
 *  manually. These values MUST get set using SK_BoxReqs_prep (). Otherwise,
 *  the values in this struct are not guaranteed to be usable, and may cause
 *  storage object operations to exhibit memory access errors.
 */

typedef struct SK_BoxStatImpl
{

	/*!
	 *  Indicates whether #prim_copy and #prim_swap should be used
	 *  or not.
	 *
	 *  If this is 1, then #prim_copy and #prim_swap MUST point to valid
	 *  functions.
	 *
	 *  If this is 0, then #prim_copy and #prim_swap should not be
	 *  referenced by the #SK_BoxIntf, and they may be NULL.
	 *
	 *  When #prim_copy and #prim_swap are not provided, then the
	 *  #SK_BoxIntf implementation should use generic byte copying /
	 *  swapping functions instead.
	 *
	 *  @warning
	 *  It is recommended to use SK_BoxStatImpl_sel_prim_flag () to
	 *  select this value.
	 *
	 *  FIXME : Should this flag be a hint that can optionally be ignored,
	 *  or a mandatory parameter? What ignoring this flag would mean is
	 *  using a generic element copying function all the time regardless of
	 *  #prim_flag.
	 */

	int prim_flag;




	/*!
	 *  A "primitive copy" function, which copies #elem_size number of
	 *  bytes from #src to #dst. The implementation of #SK_BoxIntf
	 *  will use this function to provide much more complex element
	 *  manipulation functions.
	 *
	 *  The reason this field exists is because when #elem_size is
	 *  hard-coded in the provided function, that is theoretically faster
	 *  to execute than running a for-loop copying byte-by-byte up to
	 *  #elem_size. That is why this function does NOT take in #elem_size
	 *  as an argument.
	 *
	 *  @warning
	 *  It is the responsibility of whoever instantiates #SK_BoxStatReqs to
	 *  ensure that #elem_size and #prim_copy are consistent, otherwise
	 *  memory access errors can occur.
	 *
	 *  @warning
	 *  It is recommended to use SK_BoxStatImpl_sel_prim_copy () to
	 *  select this value.
	 *
	 *  FIXME : It is theoretically possible for a
	 *  SK_BoxStatReqs_error_check () function to check if this
	 *  consistent. i.e., if #elem_size is N, then it checks that this
	 *  function copies exactly N byte. Maybe write such a function?
	 */

	SK_BoxIntf_prim_copy_func prim_copy;




	/*!
	 *  A "primitive swap" function, which swaps #elem_size number of bytes
	 *  between the memory regions starting at #ptr_0 and #ptr_1. The
	 *  implementation of #SK_BoxIntf will use this function to provide
	 *  much more complex element manipulation functions.
	 *
	 *  This function exists for essentially the same reasons as
	 *  #prim_copy. If #elem_size is hard-coded in the provided function,
	 *  that is theoretically faster to execute than running a for-loop
	 *  that swaps byte-by-byte between 2 elements.
	 */

	SK_BoxIntf_prim_swap_func prim_swap;




	//FIXME : Is it useful to have a #prim_byte () function that sets all
	//of the bytes of the element to the same byte value? This may be
	//useful, for example, to zero out elements
}

SK_BoxStatImpl;




/*!
 *  Used to describe a set of desired dynamic parameters for a storage object
 *  implementing the interface #SK_BoxIntf. These parameters are intended to be
 *  modifiable in the lifetime of the storage object.
 *
 *  While these parameters can change, there is no requirement to do so in any
 *  particular circumstance.
 *
 *  @warning
 *  The fields in this data structure CAN change during the lifetime of the
 *  storage data structure instance.
 *
 *  @warning
 *  If any change is made to this data structure, then SK_BoxReqs_prep ()
 *  MUST be called once again on the associated #SK_BoxReqs.
 *
 *  @warning
 *  If a change is made to this data structure, it must be done at a time where
 *  no function is accessing it (usually done via #SK_BoxReqs). In
 *  multi-threaded scenarios, that typically means some form of external
 *  locking is required if modification of this data structure is desired.
 */

typedef struct SK_BoxDynaReqs
{

	/*!
	 *  A hint describing the minimum number of blocks (minus 1) that must
	 *  be unused before automatically "minifying" the storage data
	 *  structure, which means deallocating blocks that are not currently
	 *  used to store elements. The number of elements in each block is set
	 *  by #hint_elems_per_block in the #SK_BoxStatReqs associated with the
	 *  storage object.
	 *
	 *  So, if this value is (x), and there is (x + 1) empty blocks in the
	 *  storage data structure instance, then it is considered a candidate
	 *  for minification.
	 *
	 *  This should be referenced whenever the number of elements currently
	 *  being stored in the storage data structure instance is decremented.
	 *
	 *  This hint helps prevent spurious memory deallocations when a
	 *  storage data structure instance is frequently resized. Consider the
	 *  case of constantly adding / removing an element from the end of a
	 *  set of elements.
	 *
	 *  @warning
	 *  It is recommended that this value always be greater than or equal
	 *  to #hint_pos_slack, so that brand new allocated blocks are not
	 *  removed when the size of the storage object is decremented by 1
	 *  right after. Otherwise, the memory pre-allocated as specified by
	 *  #hint_pos_slack would always get deallocated before it is used in
	 *  the case of alternating size-increases / size-decreases, which
	 *  would harm performance.
	 *
	 *  If uncertain, leave this value as 0.
	 *
	 *  @warning
	 *  Since this is a hint, it CAN be ignored by the storage data
	 *  structure.
	 */

	size_t hint_neg_slack;




	/*!
	 *  A hint describing the number of extra blocks to allocate whenever
	 *  a memory allocation is necessary to store more elements in the
	 *  storage data structure. The number of elements in each block is set
	 *  by #hint_elems_per_block in the #SK_BoxStatReqs associated with the
	 *  storage object.
	 *
	 *  By allocating extra blocks before they are actually needed, the
	 *  allocations of multiple blocks can be grouped together, and thus
	 *  finish quicker. Note, #elems_per_block can be increased to get a
	 *  similar effect, except that also has an effect on how much memory
	 *  gets deallocated at a time.
	 *
	 *  @warning
	 *  It is recommended that this value be lesser than or equal to
	 *  #hint_neg_slack. Otherwise, the memory pre-allocated as specified
	 *  by #hint_pos_slack would always get deallocated before it is used
	 *  in the case of alternating size-increases / size-decreases, which
	 *  would harm performance.
	 *
	 *  If uncertain, leave this value as 0.
	 *
	 *  @warning
	 *  Since this is a hint, it CAN be ignored by the storage data
	 *  structure.
	 */

	size_t hint_pos_slack;




	/*!
	 *  If this is '1', this indicates that #hint_max_num_dyna_blocks
	 *  should be respected.
	 *
	 *  If this is '0', this indicates that #hint_max_num_dyna_blocks
	 *  should be ignored.
	 *
	 *  @warning
	 *  Since this is a hint, it CAN be ignored by the storage data
	 *  structure.
	 */

	//TODO : Choose a shorter name for this; it is a bit obnoxiously named
	//as it is right now.

	int hint_use_max_num_dyna_blocks;




	/*!
	 *  The maximum number of dynamically-allocated blocks that are allowed
	 *  to be allocated in the storage data structure. Note, the number of
	 *  elements in a block is described by #hint_elems_per_block in
	 *  #SK_BoxStatReqs.
	 *
	 *  To respect this hint, resize-operations should intentionally fail
	 *  if it would cause the number of dynamically-allocated blocks to
	 *  exceed this.
	 *
	 *  By "resize-operation", this means any operation that changes the
	 *  number of elements in the storage data structure. This includes
	 *  #SK_BoxIntf.init ().
	 *
	 *  @warning
	 *  Since this is a hint, it CAN be ignored by the storage data
	 *  structure.
	 */

	size_t hint_max_num_dyna_blocks;




	/*!
	 *  The "degree of a node" in a tree is the number of children it has.
	 *  Likewise, the "degree of a tree" is the degree of the root node of
	 *  the tree.
	 *
	 *  This hint indicates the _target_ degree of the nodes within a tree
	 *  storage object for an #SK_BoxIntf implementation. This is _only_
	 *  relevant for tree data structures that have a selectable degree.
	 *
	 *  So for example, a binary tree should ignore this hint, because it
	 *  can only have a degree of 2. A B-tree should try to respect this
	 *  hint, because it can made with any degree.
	 *
	 *  The storage object has a great deal of freedom in how to interpret
	 *  this argument. It can be interpreted as simply the degree of the
	 *  root node, or it can be interpreted as the degree that all nodes
	 *  must exactly have, or it can be interpreted as a loose target for
	 *  the degree of each node.
	 *
	 *  At any rate, a lower average degree is associated with easier
	 *  storage object resizes at the expense of having to follow more
	 *  links to access elements. A higher average degree is associated
	 *  with less link-following at the expense of having to move more
	 *  elements during resizes.
	 *
	 *  Note that this hint is inside #SK_BoxDynaReqs, so it can change
	 *  dynamically. It is worth repeating that this hint is a _target_ and
	 *  not an exact demand. The storage object is NOT required to reshape
	 *  trees if this value changes over time in order to respect this
	 *  hint. In fact, because it is a hint, it is not required to respect
	 *  this field at all (even though doing so is beneficial).
	 */

	size_t hint_tree_avg_degree;




	//FIXME : Should there be a function pointer field here for an "element
	//array printer"? This would be a printing function that can be used on
	//an array of elements so that they can be represented textually inside
	//of a string. (Maybe even use #SK_Str here?). This would allow for a
	//high-level SK_Box_* () function to provide a generic printing
	//function for ranges of elements.
	//
	//Here's a suggested function pointer :
	//
	//	#str is a pointer to an #SK_Str that should be made to
	//	represent the contents of #array
	//
	//	#array is pointer to an array of elements
	//
	//	#array_len is the number of elements contained in #array
	//
	//
	//	SlyDr (* printer)
	//	(
	//		const SK_BoxReqs *	req,
	//		SK_Box *		box,
	//
	//	 	SK_Str *		str,
	//		void *			array,
	//		size_t			array_len
	//	);
	//
	//
	//Note the #array argument instead of a singular #elem argument, higher
	//level #SK_BoxIntf functions would desire passing this function arrays
	//instead of singular elements at a time, because printing
	//1-element-at-a-time is typically very slow.
	//
	//My reservation for adding this function pointer is that since
	//printing functions often need extraneous arguments, maybe it is a
	//GOOD thing that users of #SK_BoxIntf are expected to write their own
	//element printing functions that operate on given storage objects?
	//Then again, maybe if #printer is seen as the "default printer" for an
	//element type, then formatting arguments are not necessary?
	//
	//ALSO : If this function pointer is NULL, then a default element
	//printer that prints elements as bytes can always be used
	//
	//ALSO : If this function pointer is added here, it may be desirable to
	//add a function pointer field for an element parser that interprets an
	//#SK_Str as a set of elements, and then sets the contents of a storage
	//object accordingly (or maybe adds it to an existing storage object?).
	//This function pointer could possibly be used for this purpose
	//
	//
	//	SlyDr (* parser)
	//	(
	//		const SK_BoxReqs *	req,
	//		SK_Box *		box,
	//
	//		SK_Str *		str,
	//		void *			array,
	//		size_t			array_len
	//	);
	//
	//
	//I don't think it is terribly high-priority for this field to be added
	//ASAP, because I don't believe its addition would be a source of
	//code-rewrites for #SK_BoxIntf users / implementers. So spending a
	//good deal of time to think about this is probably a good idea. Being
	//able to inform #SK_BoxIntf users / implementers how to interpret the
	//elements as text does seem highly convenient at first glance.

}

SK_BoxDynaReqs;




/*!
 *  Used to describe a set of IMPLIED dynamic parameters for a storage object
 *  implementing the interface #SK_BoxIntf. These parmaeters CAN change during
 *  the lifetime of the storage object that uses them. The values in this
 *  struct are derived from values contained in #SK_BoxDynaReqs.
 *
 *  Unlike #SK_BoxDynaReqs, these values are not permitted to be changed
 *  manually. These values MUST get set using SK_BoxReqs_prep (). Otherwise,
 *  the values in this struct are not guaranteed to be usable, and may cause
 *  storage object operations to exhibit memory access errors.
 *
 *  @warning
 *  If a change is made to this data structure, it must be done at a time where
 *  no function is accessing it (usually done via #SK_BoxReqs). In
 *  multi-threaded scenarios, that typically means some form of external
 *  locking is required if modification of this data structure is desired.

 */

typedef struct SK_BoxDynaImpl
{
	/*!
	 *  A direct copy of the #SK_BoxDynaReqs associated with this data
	 *  structure as it appeared when SK_BoxDynaImpl_prep () was called.
	 *
	 *  This exists to prevent the #dyna_r and #dyna_i from getting
	 *  out-of-sync inside of the #SK_BoxReqs data structure. This is
	 *  useful because #dyna_r is allowed to be changed, and the use of
	 *  SK_BoxReqs_prep () may be forgotten.
	 *
	 *  FIXME : Should a similar field exist in #SK_BoxStatImpl? Consider
	 *  that #stat_r in #SK_BoxReqs MUST be constant during the usage of
	 *  the #SK_BoxReqs instance.
	 *
	 *  FIXME : Although I currently believe this field is a good idea, is
	 *  it really? Its only use is to prevent errors from users forgetting
	 *  to use SK_BoxReqs_prep (), but is altering the design for probable
	 *  errors like that good design?
	 */

	SK_BoxDynaReqs		r;
}

SK_BoxDynaImpl;




/*!
 *  Simply contains an #SK_BoxStatReqs, #SK_BoxStatImpl, #SK_BoxDynaReqs, and
 *  #SK_BoxDynaImpl.
 *
 *  Typically when one of the above data structures is instantiated, it's
 *  useful to instantiate all of them.
 *
 *  Note that this is NOT the data structure that is used to configure
 *  #SK_BoxIntf storage object instances, that is #SK_BoxReqs. #SK_BoxReqs
 *  contains pointers to the sub-structures instead of the sub-structures
 *  themselves. See #SK_BoxReqs's documentation as to why this is.
 */

typedef struct SK_BoxAllReqs
{
	SK_BoxStatReqs		stat_r;

	SK_BoxStatImpl		stat_i;

	SK_BoxDynaReqs		dyna_r;

	SK_BoxDynaImpl		dyna_i;
}

SK_BoxAllReqs;




/*!
 *  Contains both the parameters that are constant and the parameters that are
 *  dynamic for the lifetime of a storage object that implements #SK_BoxIntf.
 *
 *  The reason that these parameters are not stored in the #SK_BoxIntf storage
 *  objects themselves is that this reduces the per-instance cost of those
 *  objects. 1 #SK_BoxReqs can describe the parameters for 1000's of
 *  #SK_BoxIntf-implementing objects.
 *
 *  @warning
 *  The data structure MUST be initialized using SK_BoxReqs_prep (), otherwise
 *  it is not guaranteed to be in a coherent state.
 *
 *  The one exception is if all the sub-structures are known to be initialized,
 *  and then the pointers to the sub-structures can be set manually.
 *
 *  Note that SK_BoxReqs_prep () deterministically succeeds because it does not
 *  require dynamic resource allocation, therefore there is currently no need
 *  for #SK_BoxReqs to be deinitialized to deallocate resources.
 *
 *  @note
 *  Since #SK_BoxReqs can be used by mutliple storage objects at once, it is
 *  considered acceptable if this data structure has a large size compared to a
 *  singular "small" storage object.
 *
 *  @note
 *  Note how this data structure contains pointers to the sub-structures
 *  instead of the sub-structures themselves. This allows for a higher-level
 *  data structure (SK_Thing) to also have (SK_ThingStatReqs, SK_ThingStatImpl,
 *  SK_ThingDynaReqs, SK_ThingDynaImpl), allow for (SK_BoxStatReqs,
 *  SK_BoxStatImpl, SK_BoxDynaReqs, SK_BoxDynaImpl) to be included in those
 *  data structures respectively, and then still allow for a proper #SK_BoxReqs
 *  and #SK_ThingReqs to be formed independently. I.e. the functions that
 *  accept #SK_BoxReqs are easily usable by #SK_Thing related functions.
 */

typedef struct SK_BoxReqs
{

	/*!
	 *  The parameters of the storage object that MUST stay static during
	 *  its lifetime.
	 */

	const SK_BoxStatReqs *	stat_r;




	/*!
	 *  The parameters automatically implied by #stat_r.
	 *
	 *  This MUST be set by SK_BoxReqs_prep ().
	 */

	SK_BoxStatImpl *	stat_i;




	/*!
	 *  The parameters of the storage object that are allowed to change
	 *  during its lifetime.
	 */

	const SK_BoxDynaReqs *	dyna_r;




	/*!
	 *  The parameters automatically implied by #dyna_r.
	 *
	 *  This MUST be set by SK_BoxReqs_prep ().
	 */

	SK_BoxDynaImpl *	dyna_i;

}

SK_BoxReqs;




/*!
 *  Function pointer type used in #SK_BoxIntf as an "element reader / writer
 *  function". As the name suggests, this can be used to read or write elements
 *  inside of a storage object that implements #SK_BoxIntf.
 *
 *  Notably, this is used in #SK_BoxIntf.init () to initialize elements.
 *
 *  @param req			A pointer to the same #SK_BoxReqs that was used
 *				to initialize the storage object where the
 *				element is contained. Generally, this argument
 *				is most useful for determining the size of
 *				elements and accessing #prim_flag, #prim_copy,
 *				and #prim_swap.
 *
 *				The #SK_BoxIntf_elem_rw_func is NOT permitted
 *				to modify #req in any way, it is only allowed
 *				to read it.
 *
 *				This must NOT be NULL.
 *
 *  @param elem_ptr		A pointer to the current element inside of a
 *  				storage object that is being accessed.
 *
 *				This must NOT be NULL.
 *
 *				Furthermore, this must NOT be the same pointer
 *				as #arb_arg.
 *
 *  @param index		For the element pointed to by #elem_ptr, this
 *				represents its index within its storage object.
 *
 *  @param arb_arg		A pointer to a completely arbitrary argument.
 *  				It is up to the specific #SK_BoxIntf_elem_rw_func
 *  				to determine what #arb_arg will be used for.
 *
 *  				The only restriction on #arb_arg is that it
 *  				must NOT be the same pointer as #elem_ptr.
 *
 *  @return			Standard status code
 */

typedef

SlyDr (* SK_BoxIntf_elem_rw_func)
(
	const SK_BoxReqs * restrict	req,

	void * restrict			elem_ptr,

	size_t				index,

	void * restrict			arb_arg
);




/*!
 *  A type (WITHOUT a definition) that represents storage objects created via
 *  an #SK_BoxIntf.
 *
 *  This type declaration exists so that pointers like (SK_Box *) can be made
 *  and be differentiated from normal (void *) pointers. The memory layout of
 *  storage objects created via #SK_BoxIntf is determined totally by the
 *  #SK_BoxIntf implementation, so there is no definition associated with the
 *  #SK_Box anywhere.
 */

typedef struct SK_Box SK_Box;




/*!
 *  A type (WITHOUT a definition) that represents anchors that can be used in
 *  conjunction with #SK_BoxIntf.
 */

typedef struct SK_BoxAnc SK_BoxAnc;




/*!
 *  Defines a generic interface for data structures that store elements.
 *  Restated, this provides an interface that generalizes array-like data
 *  structures.
 *
 *  Obvious candidates to implement this interface would be data structures
 *  that implement arrays, vectors, linked-lists, trees, etc.
 *
 *  While all of the functions specified by this interface are typically easy
 *  to implement for a given storage data structure, they often come with
 *  drastically different computational complexities. As a result, there are
 *  numerous cases where only 1 storage type performs acceptably for a given
 *  task.
 *
 *  This is the foremost benefit of #SK_BoxIntf. A calling function can program
 *  against #SK_BoxIntf, and select the implementation with the most
 *  appropriate run-time complexity during execution. When programmed against
 *  #SK_BoxIntf, the same functions can make use of arrays, vectors,
 *  linked-lists, trees, etc. without any code modifications whatsoever.
 *
 *  There are many naunces that went into the design of this interface which
 *  are not at all obvious at first sight. Before disregarding this interface
 *  in lieu of an alternative, consider if the alternative handles all of these
 *  nuances as well.
 *
 *
 *
 *
 *  <b> MAJOR BENEFITS OF #SK_BoxIntf </b>
 *
 *  (An instance of the data structure that implements #SK_BoxIntf will simply
 *  be referred to as a "storage object" here.)
 *
 *
 *  A benefit of #SK_BoxIntf is that it...
 *
 *  1)
 *
 *  	Generalizes array-like storage objects so that other data structures do
 *  	not have to implement ANY changes in order to swap between using
 *  	arrays, vectors, linked-lists, trees, b-trees, doubly-linked-lists,
 *  	etc.
 *
 *
 *  2)
 *
 *  	Supplies an interface that provides all common storage object
 *  	operations (setting, getting, resizing, insertion, removal, etc.) in an
 *  	oragnized fashion.
 *
 *
 *  3)
 *
 *  	Allows for storage objects to be configured via the usage of
 *  	#SK_BoxReqs, which makes it possible to optimize them for a given task
 *  	at run-time.
 *
 *
 *  4)
 *
 *      Allows for a just-in-time compiled version of the interface
 *      (#SK_BoxJit) to be used everywhere that #SK_BoxIntf is accepted.
 *
 *      (Note : No #SK_BoxJit generator has been implemented yet, however, it
 *      is planned and many design decisions have been made in preparation for
 *      it.)
 *
 *
 *  5)
 *
 *  	Makes use of #SlySr and #SlyDr to distinguish between functions that
 *  	succeed stochastically / deterministically, so that which functions
 *  	need to have their return values examined is immediately obvious.
 *
 *
 *  6)
 *
 *  	Uses an initialization / deinitialization scheme that makes use of the
 *  	function #SK_BoxIntf.inst_stat_mem (). This function informs the user
 *  	about how much static memory the configured storage object instance
 *  	will require during its lifetime, before it is actually instantiated.
 *
 *  	This memory can then either be allocated on the stack or on the heap;
 *  	#SK_BoxIntf can be used the same regardless. This allows for storage
 *  	objects to be accessed via 1 less memory-dereference operation than
 *  	most other storage object implementations in C, which often have
 *  	*_create / *_destroy () functions that perform the static memory
 *  	allocation / deallocation themselves.
 *
 *  	#SK_BoxIntf's scheme in this matter generally benefits cache locality
 *  	wherever possible.
 *
 *
 *  7)
 *
 *  	Does NOT make any assumptions regarding the type / size of the elements
 *  	being stored in storage objects other than that...
 *
 *	  1)
 *
 *		The size of the elements are non-zero.
 *
 *	  2)
 *
 *		The elements are "directly movable data".
 *
 *		(See the foot-note "Directly Movable Data" for what this means.)
 *
 *
 *  8)
 *
 *  	Despite not making any assumptions about the size of elements (other
 *  	than being non-zero size), #SK_BoxIntf keeps element reads / writes
 *  	fast by providing functions that facilitate accessing elements in as
 *  	large of contiguous memory blocks as possible. By performing accesses
 *  	with the largest available contiguous memory blocks, good cache
 *  	locality can be maintained.
 *
 *
 *  9)
 *
 *  	Defines the concept of an "anchor", which is an arbitrary piece of data
 *  	defined by the #SK_BoxIntf implementation that allows for a
 *  	previously-accessed element to be accessed quicker than without it.
 *
 *  	For example, a linked-list implementation may use an anchor to skip
 *  	"walking the list" for a given element. This can have a huge beneficial
 *  	impact on the access time of elements in a storage object, especially
 *  	those that are implemented with numerous dynamic memory allocations.
 *
 *
 *  10)
 *
 *	Allows for the storage objects to be configured such that some portion
 *	of the storage object's elements are located in its static memory
 *	rather than its dynamic memory.
 *
 *	This allows for that portion of elements to be accessed far quicker
 *	than other elements in the storage object, at the cost of raising the
 *	static memory usage of said object.
 *
 *
 *  11)
 *
 *  	Allows for the storage object to be configured to use a given
 *  	"block-size", which specifies the smallest number of elements that
 *	resize operations should allocate at a time.
 *
 *  	This has the benefit of transparently reducing the number of memory
 *  	allocations / deallocations that must occur when elements are added /
 *  	removed from the storage object.
 *
 *  	This can also be used to increase the size of contiguous memory regions
 *  	used by the storage object, which can benefit the access time of
 *  	elements (especially for linked-lists).
 *
 *
 *  12)
 *
 *	Allows for a given amount of "negative-slack" and "positive-slack" to
 *	be specified.
 *
 *	The values for "negative-slack" and "positive-slack" are used during
 *	memory allocation operations to determine how many "extra" blocks
 *	should be left behind when elements are removed, or how many "extra"
 *	blocks should be created when elements are added.
 *
 *	This allows for large numbers of small memory allocation operations to
 *	be transparently reduced into a smaller number of large memory
 *	allocations. Since memory allocation operations usually incur numerous
 *	cache misses, the smaller number of large memory allocations typically
 *	performs far better. Additionally, the combined allocations help
 *	increase how "contiguous" the storage object is in memory.
 *
 *
 *  13)
 *
 *  	Provides a method to automatically iterate through a range of elements
 *  	and apply a function to them via an #SK_BoxIntf_elem_rw_func.
 *
 *
 *  14)
 *
 *  	Provides high-level functions to convert a storage object from using
 *  	one #SK_BoxIntf implementation to another (see SK_Box_assign ()). For
 *  	example, this could be used to convert a vector to a tree
 *  	automatically.
 *
 *
 *  15)
 *
 *  	Provides a high-level function to serialize / deserialize the elements
 *	of a given storage object so that they can easily be saved /
 *	transmitted.
 *
 *	(Note : SK_Box_ser () / SK_Box_deser () are still under development,
 *	but planned.)
 *
 *
 *
 *
 *  To provide many of the design benefits listed above, it required making the
 *  following assumptions regarding the usage of #SK_BoxIntf, which both users
 *  and implementers are expected to understand.
 *
 *
 *
 *
 *  <b> GENERAL REQUIREMENTS OF #SK_BoxIntf </b>
 *
 *  (An instance of the data structure that implements #SK_BoxIntf will
 *  simply be referred to as a "storage object" here.)
 *
 *
 *  Users / implementers of #SK_BoxIntf must be aware that...
 *
 *  1)
 *
 *	The storage objects have no "holes" in their indexing.
 *
 *	That means if get_num_elems () reports the storage object has N
 *	elements, then the values [0, (N - 1)] are valid indices to access
 *	elements in the storage object.
 *
 *	(See the foot-note "GR 1 Addendum")
 *
 *
 *  2)
 *
 *  	The storage objects are organized into a "static memory portion" and a
 *  	"dynamic memory portion".
 *
 *	The static memory portion has a size that remains static for the entire
 *	lifetime of the storage object, and the user of the #SK_BoxIntf is
 *	responsible for allocating this memory region. The user can determine
 *	this size by calling inst_stat_mem ().
 *
 *	The dynamic memory portion is managed by the #SK_BoxIntf implementation
 *	itself, and has a size that may change freely throughout the lifetime
 *	of the storage object. If this region is present, it must be
 *	automatically deallocated by deinit ().
 *
 *	Note that the above definitions are against the "lifetime of the
 *	storage object". The size of the static memory portion is not a
 *	compile-time constant, rather it is determined at run-time via
 *	inst_stat_mem (). Further, it is a different size for different
 *	#SK_BoxReqs configurations.
 *
 *
 *  3)
 *
 *	"Anchors" are values that help access a previously accessed element (or
 *	"nearby" elements) in a storage object quicker than without the use of
 *	the anchor. Anchors are used by the *_w_anc () and *_by_anc () versions
 *	of functions in #SK_BoxIntf.
 *
 *	The contents of an anchor are determined by the #SK_BoxIntf
 *	implementation, and are opaque to the user of the #SK_BoxIntf.
 *
 *	The size of the anchor is reported via anc_size (), and whether an
 *	implementation supports anchors is reported via anc_support ().
 *
 *	To make use of anchors, the caller is responsible for obtaining a
 *	memory region with the appropriate size (whether that is on the stack,
 *	heap, or elsewhere) so that it may be used as a source / destination
 *	argument.
 *
 * 	Regardless if an #SK_BoxIntf implementation supports anchors, users are
 * 	NOT required to use them (though encouraged when possible).
 *
 * 	(See the foot-note "GR 3 Addendum")
 *
 *
 *  4)
 *
 *	Anytime elements are added / removed from the storage object, the
 *	storage object may change the memory addresses associated with every
 *	element in a completely opaque manner.
 *
 *	(See the foot-note "GR 4 Addendum")
 *
 *
 *  5)
 *
 *	As a result of 4), pointers / anchors associated with elements of the
 *	storage object become INVALIDATED after any other function in
 *	#SK_BoxIntf is used on that storage object _by default_.
 *
 *	There are very notable functions that are marked as exceptions in their
 *	documentation: get_elem (), set_elem (), get_elem_ptr (),
 *	get_num_elems (), among others.
 *
 *	(See the foot-note "GR 5 Addendum")
 *
 *
 *  6)
 *
 *	Also as a result of 4), the elements that are stored in storage objects
 *	MUST be classified as directly movable data (DMD).
 *
 *	If a storage object is used to store non-DMD elements, then pointers
 *	associated with that element may become invalid, potentially leading to
 *	memory access errors.
 *
 *	See the foot-note "Directly Movable Data" for what this means.
 *
 *	Also see the foot-note "GR6 Addendum"
 *
 *
 *  7)
 *
 *	The static memory portion of storage objects MUST be possible to store
 *	within other storage objects as elements.
 *
 *	Therefore, as a result of 6), the static memory portion of storage
 *	objects MUST be DMD.
 *
 *	This has 2 consequences :
 *
 *	  1)
 *
 *		The static memory portion of storage objects must NOT contain
 *		pointers to itself.
 *
 *		(Though it CAN contain offsets relative to itself.)
 *
 *
 *	  2)
 *
 *		The dynamic memory portion of storage objects must NOT contain
 *		pointers to the static memory portion.
 *
 *		(However, the static memory portion CAN contain pointers to the
 *		dynamic memory portion.)
 *
 *
 *  8)
 *
 *	(FIXME : This requirement needs more consideration. It's probably a
 *	good idea to revisit this one after making a few #SK_BoxIntf
 *	implementations, even if that's really painful.)
 *
 *	Storage data structures that implement #SK_BoxIntf are expected to
 *	maintain these promises for multi-threading :
 *
 *	  1)
 *
 *		It IS safe for multiple threads to read elements simultaneously
 *		from the storage object using get_elem (), even if they access
 *		the same indices.
 *
 *
 *	  2)
 *
 *		It IS safe for multiple threads to simultaneosuly read / write
 *		elements inside of the storage object using
 *		get_elem () / set_elem (), but ONLY if the threads do not
 *		access the same element indices simultaneously.
 *
 *		In the case that threads might access the same element index,
 *		an external locking mechanism is necessary.
 *
 *
 *	  3)
 *
 * 		It is NOT safe for any thread to add / remove elements from the
 *		storage object while any other thread is accessing the storage
 *		object.
 *
 *		In order to add / remove elements from the storage object while
 *		other threads are accessing it, an external locking mechanism
 *		is necessary.
 *
 *
 *        4)
 *
 *		It IS safe for multiple threads to read the same anchor when
 *		accessing elements.
 *
 *		It is NOT safe for multiple threads to write to the same anchor
 *		without an external locking mechanism.
 *
 *
 *		Example :
 *
 *		This call IS thread-safe :
 *
 *		  box_intf->get_elem_w_anc (req, box, ind, anc, NULL, elem);
 *
 *
 *		This call is NOT thread-safe :
 *
 *		  box_intf->get_elem_w_anc (req, box, ind, anc, anc, elem);
 *
 *
 *		The difference between these 2 calls is that in the thread-safe
 *		variant, #anc is NOT updated, only read.
 *
 * 		(It is recommended that #SK_BoxIntf implementations use
 * 		"modification numbers" in their anchors to help users debug
 * 		anchor corruption. However, this is not required.)
 *
 *
 *  9)
 *
 *	Anchors must behave as a plain-old data (POD) type, and do not have
 *	initialization / deinitialization functions.
 *
 *
 *  10)
 *
 *	An anchor associated with any element index in a storage object can be
 *	used to access any element in the storage object via the *_w_anc ()
 *	functions.
 *
 *	It is understood, however, that anchors will generally make element
 *	accesses quicker the closer its associated index is to the requested
 *	element index.
 *
 *
 *  11)
 *
 *	A storage data structure implementing #SK_BoxIntf MUST implement all
 *	the functions in the interface, EXCEPT for those that explicitly have a
 *	warning in the documentation stating that they may unimplemented.
 *
 *	Even for those exceptions to this rule, the implementation should at
 *	least provide stub functions that return #SLY_NO_IMPL.
 *
 *
 *  12)
 *
 *  	If the storage data structure does not support anchors according to
 *  	anc_support (), implementations should still provide the *_w_anc ()
 *  	versions of functions, but without accessing the #cur_anc or #new_anc
 *  	arguments.
 *
 *  	However, the *_by_anc () versions of functions may be totally
 *  	unimplemented, and instead be stub functions that return #SLY_NO_IMPL.
 *
 *
 *  13)
 *
 *	When a function in #SK_BoxIntf provides an argument for the index of
 *	an element, by default it is the responsibility of the CALLER to ensure
 *	that the index is below the number of elements in the storage object.
 *
 *	The number of elements in the storage object can be obtained using
 *	get_num_elems (). If a given index is greater than or equal
 *	to the reported number of elements, memory access errors may occur.
 *
 *	(See the foot-note "GR 13 Addendum")
 *
 *
 *  14)
 *
 *  	#SK_BoxIntf implementations must respect the distinction between #SlyDr
 *  	values and #SlySr values.
 *
 *  	When a function returns a #SlyDr value, that means the function is
 *  	guaranteed to succeed if given valid arguments. Therefore, users are
 *  	NOT expected to inspect these return values (though they may freely
 *  	choose to do so).
 *
 *  	When a function returns a #SlySr value, that means the function is NOT
 *  	guaranteed to succeed even if given valid arguments. Users ARE expected
 *  	to inspect these return values and react appropriately to their
 *  	failure.
 *
 *
 *  15)
 *
 *	If a function that returns #SlySr in this interface experiences a
 *	failure, it MUST leave the storage object in a valid state. This means
 *	that the functions in #SK_BoxIntf may still be used on the storage
 *	object after the failure.
 *
 *	Further, the values of elements MUST remain the same after such a
 *	failure.
 *
 *	Internal data to the storage object that is not exposed via #SK_BoxIntf
 *	may change freely in response to such failures.
 *
 *	init () represents an exception to this rule, because prior to the use
 *	of init (), the storage object is not in a valid state to begin with.
 *
 *	(FIXME : I am considering adding a dynamic hint "sr_safe" that toggles
 *	this behavior, because it generally makes resize operations consume
 *	more memory).
 *
 *
 *  16)
 *
 *	Users are expected to NOT call init () on storage objects that are
 *	already initialized, and are exptected to NOT call deinit () on storage
 *	objects that are already deinitialized.
 *
 *	If a storage object is passed to deinit (), the user is ALLOWED to
 *	reinitialize it using init () though.
 *
 *
 *  FIXME : Should there be a design assumption regarding #size_t overflows?
 *  What should be this interface's policy for handling numbers of elements
 *  that would require more than #SIZE_MAX number of bytes?
 *
 *  TODO : Make an implementation of #SK_BoxJit, which is a just-in-time
 *  compiled variant of #SK_BoxIntf.
 *
 *  TODO : This is one of the more easy-to-get-wrong aspects of this interface,
 *  ensure that that the use of (const void *) is correct in this interface.
 *  Essentially, const-qualified pointers need to be carefully thought about to
 *  take into that many arguments may actually want to be modified in what is
 *  otherwise a read-operation, for the sake of allowing statistics-tracking by
 *  #SK_BoxIntf implementations. Technically, the implementations could just
 *  cast to (void *) to remove the const-qualification, but requiring
 *  implementations to do this is bad form. A similar process must done with
 *  the (restrict) keyword.
 */

typedef struct SK_BoxIntf
{




	/*!
	 *  Calculates the static memory cost of an instance of the data
	 *  structure implementing #SK_BoxIntf.
	 *
	 *  Generally speaking, if the storage object can contain
	 *  statically-allocated elements, then the memory cost of those
	 *  elements will be be taken into account by #num_bytes.
	 *
	 *  @param req			The #SK_BoxReqs describing
	 *  				the parameters of the storage data
	 *  				structure.
	 *
	 *  				Note that SK_BoxReqs_prep () must be
	 *  				used on #req before it used by
	 *  				functions in this interface.
	 *
	 *  @param size			Where to place the calculated static
	 *				memory cost of the storage object, in
	 *				units of bytes
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* inst_stat_mem)
	(
		const SK_BoxReqs *		req,

		size_t *			size
	);




	/*!
	 *  Reports the amount of dynamically-allocated memory the storage
	 *  object is currently consuming.
	 *
	 *  @warning
	 *  Note how inst_stat_mem () does NOT have an #box argument; this is
	 *  because the current state of the object is irrelevant to the amount
	 *  of static memory it consumes. That is NOT true with dynamic memory.
	 *  With that in mind, this should only be used on #box if #box has
	 *  already been initialized with init ().
	 *
	 *  @warning
	 *  Implementations of this function MUST include the cost of elements
	 *  allocated as slack when calculating #size. Every byte that is not
	 *  accounted for by inst_stat_mem () should be accounted for by this
	 *  function.
	 *
	 *  @warning
	 *  If the dynamic memory cost would overflow #size, it should be set
	 *  to #SIZE_MAX and #SLY_SUCCESS should NOT be used as a return value.
	 *
	 *  (Would this rule make this function require this function to return
	 *  a #SlySr instead? Should this function even attempt to handle
	 *  #size_t overflows?)
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param size			Where to place the calculated dynamic
	 *				memory cost of the storage object, in
	 *				units of bytes
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* inst_dyna_mem)
	(
		const SK_BoxReqs *	req,

		SK_Box *		box,

		size_t *		size
	);




	//FIXME : Should there be a function in this interface to interrogate
	//the #SK_BoxIntf as to whether it performs safety-checks on index
	//arguments or not? To ask about specific functions, should it accept
	//function pointer argument, or a (void *) that can be casted to any
	//function pointer?




	/*!
	 *  Indicates whether or not the field (req->stat_r->req_regi) needs to
	 *  point to a valid #SK_BoxReqsRegi (i.e. be non-NULL) for storage
	 *  objects to be initialized with this interface.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param need_req_regi	If this is set to 0, then
	 *  				(req->stat_r->req_regi) CAN be NULL and
	 *  				this #SK_BoxIntf can still successfully
	 *  				create storage-objects.
	 *
	 *				If this is set to 1, then
	 *				(req->stat_r->req_regi) MUST be
	 *				NON-NULL for this #SK_BoxIntf to
	 *				successfully create storage-objects.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* need_req_regi)
	(
		const SK_BoxReqs *	req,

		int *			need_req_regi
	);




	/*!
	 *  Initializes the storage object, which has had its static memory
	 *  allocated for it but has not had any of its fields initialized.
	 *
	 *  The function inst_stat_mem () can be used to determine how much
	 *  memory needs to be allocated for this instance before using
	 *  init ().
	 *
	 *  @warning
	 *  If this function is successful, the caller of this function MUST
	 *  call #deinit on #box sometime afterwards to avoid resource leaks.
	 *
	 *  @warning
	 *  Using init () on a storage object twice without using deinit () in
	 *  between will generally cause memory access errors.
	 *
	 *  @param req			The #SK_BoxReqs describing
	 *  				the parameters of the storage data
	 *  				structure. This should be the same #req
	 *  				argument as what was used in
	 *  				inst_stat_mem ().
	 *
	 *  				Note that init () is NOT permitted to
	 *  				modify #req in any way, it is only
	 *  				permitted to read it.
	 *
	 *  				Note that SK_BoxReqs_prep () must be
	 *  				used on #req before it used by
	 *  				functions in this interface.
	 *
	 *  @param box			The storage object to initialize.
	 *
	 *  				Essentially, this should point to a
	 *  				memory region with the size reported by
	 *  				inst_stat_mem ().
	 *
	 *  @param num_elems		The initial number of elements to store
	 *				inside of the storage object.
	 *
	 *				This CAN be 0 if no elements are
	 *				desired to be stored initially.
	 *
	 *				Note, get_resize_limits () will report
	 *				if there any limits for this value.
	 *
	 *  @param elem_init_func	The function that should be used to
	 *  				initialize the individual elements in
	 *  				the storage object.
	 *
	 *				The purpose of #elem_init_func is that
	 *				it allows the caller to directly
	 *				initialize the storage object with
	 *				useful values. This can be more
	 *				efficient than initializing every
	 *				element to 0, and THEN initializing
	 *				every element to something useful.
	 *
	 *				Note that this function must succeed
	 *				deterministically; its success will NOT
	 *				be checked by init ().
	 *
	 *				If the use of this function is not
	 *				desired, then this argument CAN be
	 *				NULL, in which case the elements of the
	 *				newly created storage data structure
	 *				instance will be set to all 0's.
	 *
	 *				See #SK_BoxIntf_elem_rw_func for
	 *				details about its parameters.
	 *
	 *				The #req, #elem_ptr, and #index
	 *				arguments will provided by init ()
	 *				itself, while #arb_arg will be
	 *				passed-through to #elem_init_func ().
	 *
	 *  @param arb_arg		The #arb_arg parameter, which will
	 *				be passed to each call of
	 *				elem_init_func () as it is initializing
	 *				individual elements.
	 *
	 *  @return			Standard status code
	 *
	 */

	SlySr (* init)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		SK_BoxIntf_elem_rw_func		elem_init_func,

		void *				arb_arg
	);




	/*!
	 *  Version of init () that does NOT initialize the elements contained
	 *  in the storage object after space is allocated for them. Therefore,
	 *  the elements will contain values of whatever happened to be in
	 *  memory at allocation, which obviously, is UNSAFE.
	 *
	 *  Generally, this version of the function should only be used if
	 *  it can be guaranteed that something else will initialize the
	 *  elements right afterwards.
	 *
	 *  @warning
	 *  Since there is no guarantee on what the value of elements will be
	 *  in the initialized storage object, implementations CAN choose to
	 *  intialize elements with values like 0 or 0x0BADBEEF, though doing
	 *  so reduces the utility of this function.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			See #box in init ()
	 *
	 *  @param num_elems		See #num_elems in init ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* init_unsafe)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems
	);




	/*!
	 *  Deinitializes the object using this interface, which means
	 *  deallocating all resources (memory, files, etc.) the object uses.
	 *
	 *  @warning
	 *  This should ONLY be called on objects that were previously
	 *  initialized with #init () or #init_unsafe ().
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The object to deinitialize
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* deinit)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box
	);




	//FIXME : Should there be a #copy () function for making a duplicate of
	//the #SK_Box, or is SK_Box_assign () sufficient? Note that
	//SK_Box_assign () operates at a high-level, and does not preserve
	//information that is not exposed by #SK_BoxIntf. Ergo, a #copy ()
	//function could preserve blocks used for slack, while SK_Box_assign ()
	//would not do so.
	//
	//Note that even if #copy () is added here, SK_Box_assign () is still
	//very useful, because it can copy elements between different
	//#SK_BoxIntf types.




	/*!
	 *  Gets the number of elements currently stored in the storage object.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param num_elems		Where to place the number of elements
	 *				currently stored in #box
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_num_elems)
	(
		const SK_BoxReqs *		req,

		const SK_Box *			box,

		size_t *			num_elems
	);




	/*!
	 *  Determines whether or not a storage data structure will have any
	 *  limitations for the number of elements it can store, based on the
	 *  #SK_BoxReqs it is configured with. If such limitations exist, it
	 *  indicates the minimum and maximum number of elements that the
	 *  storage object can be resized to.
	 *
	 *  Technically speaking, the number of elements a storage data
	 *  structure can store is constrained by the bit-width of the #size_t
	 *  arguments in this interface. However, this is not the limit that
	 *  this function is intrested in, So, if a storage data structure can
	 *  theoretically be used to store #SIZE_MAX number of elements (or
	 *  more somehow), then #limits_exist will be set to 0. (Note that
	 *  "#SIZE_MAX number of elements" was what was specified, not
	 *  "#SIZE_MAX number of bytes".)
	 *
	 *  It may not be obvious why these limitations would exist in the
	 *  first place, so consider these example implementations of
	 *  #SK_BoxIntf.
	 *
	 *    1)
	 *
	 *	An array composed entirely out of N statically-allocated
	 *  	elements, WITH a singular length parameter.
	 *
	 *  	This would have [min_num_elems, max_num_elems] = [0, N], where
	 *  	N is a constant. The N elements always exist in memory, but the
	 *  	length parameter can be used to represent how many of the
	 *  	currently allocated elements are actually used. Since only
	 *  	static memory is used, having more than N elements is
	 *  	impossible.
	 *
	 *    2)
	 *
	 *	An array composed entirely out of N statically-allocated
	 *	elements, with NO length parameter.
	 *
	 *	This would have [min_num_elems, max_num_elems] = [N, N]. The N
	 *	elements always exist in memory, and without a length parameter
	 *	there's no way to tell how many are actually used (assuming
	 *	there's no other mechanisms).
	 *
	 *    3)
	 *
	 *	A quadtree where for whatever reason, a maximum depth of 2 is
	 *	assummed throughout the implementation.
	 *
	 *	Assuming only 1 element in layer 0 is allowed, then at most
	 *	there are 4 elements in layer 1, and 16 elements at most in
	 *	layer 2. Therefore, this would have a
	 *	[min_num_elems, max_num_elems] = [0, 21], because
	 *	21 = 1 + 4 + 16.
	 *
	 *  @warning
	 *  These limitations are in regards to an #SK_BoxIntf implementation
	 *  given a specific #req, they are NOT in reference to the resources
	 *  of the running platform. This function does NOT guarantee that the
	 *  running platform has enough memory to allocate #min_num_elems or
	 *  #max_num_elems.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param limits_exist		Where to place a value indicating
	 *				whether or not limitations for resize
	 *				operations exist for the storage
	 *				object(s) associated with #req.
	 *
	 *				If this is 0, then the limitations do
	 *				NOT exist.
	 *
	 *				If this is 1, then the limitations DO
	 *				exist, and #min_num_elems and
	 *				#max_num_elems will be set accordingly.
	 *
	 *				This CAN be NULL if this value is not
	 *				desired.
	 *
	 *  @param min_num_elems	The minimum number of elements that
	 *  				the storage object can be set to.
	 *
	 *				This will be set to 0 if #limits_exist
	 *				is set to 0.
	 *
	 *				This CAN be NULL if this value is not
	 *				desired.
	 *
	 *  @param max_num_elems	The maximum number of elements that
	 *  				the storage object can be set to.
	 *
	 *  				This will be set to #SIZE_MAX if
	 *  				#limits_exist is set to 0.
	 *
	 *				This CAN be NULL if this value is not
	 *				desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_resize_limits)
	(
		const SK_BoxReqs *		req,

		int *				limits_exist,

		size_t *			min_num_elems,
		size_t *			max_num_elems
	);




	/*!
	 *  Resizes the storage object so that it can store a different number
	 *  elements.
	 *
	 *  This function should make a best-effort attempt at respecting the
	 *  parameters (req->dyna.hint_neg_slack) and
	 *  (req->dyna.hint_pos_slack), but no guarantee is made that it will
	 *  by this interface. Specific implementations of this interface may
	 *  instead establish that guarantee.
	 *
	 *  @warning
	 *  Assume that the storage object #box can already store N elements.
	 *  If (num_elems > N), the elements with indices [N, num_elems - 1]
	 *  will be initialized using #elem_init_func. If (num_elems < N), the
	 *  elements with indices [num_elems, N - 1] will be removed from the
	 *  storage object. All other elements will preserve their previous
	 *  value.
	 *
	 *  @note
	 *  ins_range () and rem_range () can be used an alternative to this
	 *  function. Notably, rem_range () operates deterministically and
	 *  returns a #SlyDr instead.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to resize so that it
	 *				can store a different number of
	 *				elements.
	 *
	 *  @param num_elems		The new number of elements that the
	 *				storage object should be able to store.
	 *
	 *				Note, get_resize_limits () will report
	 *				if there any limits for this value.
	 *
	 *  @param elem_init_func	See #elem_init_func in init ().
	 *
	 *  				Note that this function will only be
	 *  				used on newly allocated elements in
	 *  				#box (if any).
	 *
	 *  @param arb_arg		See #arb_arg in init ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* resize)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		SK_BoxIntf_elem_rw_func		elem_init_func,

		void *				arb_arg
	);




	/*!
	 *  Version of resize () that does NOT initialize any newly-allocated
	 *  elements in the storage object if they occur.
	 *
	 *  @warning
	 *  Since there is no guarantee on what the value of newly-allocated
	 *  elements will be in the resized storage object, implementations CAN
	 *  choose to intiialize elements with values like 0 or 0x0BADBEEF,
	 *  though doing so reduces the utility of this function.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			See #box in resize ()
	 *
	 *  @param num_elems		See #num_elems in resize ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* resize_unsafe)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems
	);




	/*!
	 *  Minifies a storage object, meaning it should deallocate memory that
	 *  is not currently necessary to store elements.
	 *
	 *  Ultimately, the behavior of this function is highly
	 *  implementation-dependent, because what is "not necessary" is
	 *  determined by the implementation.
	 *
	 *  In general, well-behaved implementations should use this function
	 *  as an opportunity to deallocate unused "slack".
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to minify
	 *
	 *  @return			Standard status code
	 */

	SlySr (* minify)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box
	);




	/*!
	 *  Optimizes a storage object, which may involve reallocating memory
	 *  or moving elements around in memory. The implementation of this
	 *  function gets to choose what exactly "optimize" means.
	 *
	 *  This function is generally most useful for storage objects that
	 *  collect statistics regarding it usage as functions are executed on
	 *  it.
	 *
	 *  For example, for a tree data structure, this function might move
	 *  the most frequently accessed elements closer to the root of the
	 *  tree.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to optimize
	 *
	 *  @param size_worth		An arbitrary score representing how
	 *				much reducing the memory usage by 50%
	 *				is worth. A higher value implies more
	 *				worth.
	 *
	 *				This value is only useful given its
	 *				relation to #speed_worth.
	 *
	 *				This is a HINT, there is no guarantee
	 *				that this function will interpret this
	 *				value in any particular way.
	 *
	 *  @param speed_worth		An arbitrary score representing how
	 *				much reducing the execution time of
	 *				setting / getting elements by 50% is
	 *				worth. A higher value implies more
	 *				worth.
	 *
	 *				This value is only useful given its
	 *				relation to #size_worth.
	 *
	 *				This is a HINT, there is no guarantee
	 *				that this function will interpret this
	 *				value in any particular way.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* optimize)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		double				size_worth,

		double				speed_worth
	);




	/*!
	 *  Informs the caller about whether "anchors" are supported by this
	 *  #SK_BoxIntf implementation with the given #req.
	 *
	 *  An "anchor" is an arbitrary piece of data associated with an
	 *  element that in some way helps access other elements at neighboring
	 *  indices in the same storage object. Typically, the anchor will take
	 *  the form of a pointer, but it may also be a (pointer + index), or
	 *  something entirely different. This is particularly useful for
	 *  linked-lists, because it lets a linked-list traversal begin from
	 *  the middle of the list rather than the beginning.
	 *
	 *  Note that it is always possible to access an element in the storage
	 *  object without using an anchor, it's just that it may be
	 *  substantially less efficient.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param support		Where to place a flag that will be 1 if
	 *				the use of anchors is supported, and
	 *				0 otherwise
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* anc_support)
	(
		const SK_BoxReqs *		req,

		int *				support
	);




	/*!
	 *  Informs the caller about whether the use of "anchors" are
	 *  recommended for the given storage object.
	 *
	 *  The difference between this function and anc_support () is that
	 *  this function indicates whether or not the use of anchors is
	 *  recommended for an individual instance of the storage data
	 *  structure. This is why this function has an #box argument; whether
	 *  or not anchors are recommended can be dependent on the current
	 *  state of #box.
	 *
	 *  As one may expect, anchors being supported according to
	 *  anc_support () is a necessary pre-requisite for them being
	 *  recommended.
	 *
	 *  Note that if anchors are supported but not recommended, it is still
	 *  always possible to use an anchor to access elements. It just may be
	 *  slightly less efficient to do so.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param recommend		Where to place a flag that will be 1 if
	 *				the use of anchors is recommended, and
	 *				0 otherwise
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* anc_recommend)
	(
		const SK_BoxReqs *		req,

		const SK_Box *			box,

		int *				recommend
	);




	/*!
	 *  Gets the size (in bytes) of an anchor associated with this
	 *  #SK_BoxIntf implementation with the given #req.
	 *
	 *  @warning
	 *  The size of the anchor must be constant for the lifetime of the
	 *  storage object.
	 *
	 *  @warning
	 *  If the storage object does not support anchors according to
	 *  anc_support (), then #size will be set to 0.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param size			Where to place the size (in bytes) of
	 *				anchors for this storage data structure
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* anc_size)
	(
		const SK_BoxReqs *		req,

		size_t *			size
	);




	/*!
	 *  Determines whether an anchor is valid or invalid. An anchor becomes
	 *  invalid when specific functions in #SK_BoxIntf are used to operate
	 *  on a storage object.
	 *
	 *  @warning
	 *  This function should be considered HEAVY-WEIGHT. Generally
	 *  speaking, the operations needed to validate an anchor are the same
	 *  operations needed to produce the anchor "from scratch".
	 *
	 *  Bear in mind that this function largely exists for the purposes of
	 *  debugging.
	 *
	 *  @warning
	 *  It is possible for an invalidated anchor to have contents that by
	 *  chance happen to match an otherwise valid anchor, similar to how a
	 *  corrupted pointer may point to a non-corrupted object.
	 *
	 *  In this event, #is_valid will still be set to true. For this
	 *  reason, it is beneficial to check #ind to see if #anc is associated
	 *  with the intended element.
	 *
	 *  @warning
	 *  If anchors are not supported by the #SK_BoxIntf implementation,
	 *  then this function should return #SK_NO_IMPL. Also, it should set
	 *  #ind to 0 and #is_valid to 0 as well.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate other existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param anc			The anchor to examine
	 *
	 *  @param ind			Where to place the element index
	 *				associated with #anc.
	 *
	 *				This argument CAN be NULL if this
	 *				information is not desired.
	 *
	 *  @param is_valid		Where to place a flag indicating that
	 *				the anchor is valid or not.
	 *
	 *				If this is set to true, #anc was valid.
	 *
	 *				If this is set to false, #anc was
	 *				invalid.
	 *
	 *				This CAN be NULL if this information is
	 *				not desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* anc_validate)
	(
		const SK_BoxReqs *		req,

		const SK_Box *			box,

		const SK_BoxAnc *		anc,

		size_t *			ind,

		int *				is_valid
	);




	/*!
	 *  Determines the element index associated with an anchor.
	 *
	 *  @warning
	 *  Implementations should take into consideration that users are
	 *  expected to view this operation as "light-weight".
	 *
	 *  While this will almost always be a light-weight operation due to
	 *  most anchors storing the element index inside of the anchor itself,
	 *  they are NOT required to do so.
	 *
	 *  For example, it is theoretically possible for a linked-list
	 *  implementation of #SK_BoxIntf to determine the index associated
	 *  with an anchor by walking the list. However, this would generally
	 *  be considered flawed design, because that would not be a
	 *  "light-weight" operation.
	 *
	 *  @warning
	 *  If #anc is an invalid anchor, this function may cause memory access
	 *  errors or report incorrect information. While most implementations
	 *  will probably set #ind to 0 in this event, this behavior is not
	 *  guaranteed.
	 *
	 *  @warning
	 *  If anchors are not supported by the #SK_BoxIntf implementation,
	 *  then this function should return #SK_NO_IMPL. Also, it should set
	 *  #ind to 0.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate other existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param anc			The anchor to examine
	 *
	 *  @param ind			Where to place the element index
	 *  				associated with #anc.
	 *
	 *  				This argument can NOT be NULL.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* anc_ind)
	(
		const SK_BoxReqs *		req,

		const SK_Box *			box,

		const SK_BoxAnc *		anc,

		size_t *			ind
	);




	/*!
	 *  INCREASES the element index associated with an anchor by +1.
	 *
	 *  So, for example, if an anchor was in reference to index 10, it will
	 *  then be in reference to index 11 after applying this function.
	 *
	 *  @warning
	 *  In the interest of speed, the CALLER is responsible for ensuring
	 *  that the new index that will be associated with the anchor is
	 *  actually present in #box. If the new index is not in the range
	 *  [0, num_elems - 1] (where #num_elems is reported by
	 *  #get_num_elems ()), then the behavior of this function is undefined
	 *  and may cause memory access errors.
	 *
	 *  (FIXME : Is this reasonable?)
	 *
	 *  @warning
	 *  Also in the interest of speed, the CALLER is responsible for making
	 *  sure that the #cur_anc argument is valid. In general, this means
	 *  that #get_anc () was used to retrieve the anchor AND no operation
	 *  has been performed on #box since then that would invalidate it.
	 *
	 *  @warning
	 *  Generally speaking, this function is most useful when it is faster
	 *  to execute than #get_anc (). For some storage objects types,
	 *  however, the speed benefit may be very small.
	 *
	 *  @warning
	 *  If anchors are not supported by the #SK_BoxIntf implementation,
	 *  then this function should return #SK_NO_IMPL.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate other existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param cur_anc		The current anchor, which will have the
	 *  				index associated with it increaased by
	 *  				1. This MUST point to a valid anchor.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* anc_inc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		SK_BoxAnc *			cur_anc
	);




	/*!
	 *  DECREASES the element index associated with an anchor by +1.
	 *
	 *  So, for example, if an anchor was in reference to index 10, it will
	 *  then be in reference to index 9 after applying this function.
	 *
	 *  @warning
	 *  In the interest of speed, the CALLER is responsible for ensuring
	 *  that the new index that will be associated with the anchor is
	 *  actually present in #box. If the new index is not in the range
	 *  [0, num_elems - 1] (where #num_elems is reported by
	 *  #get_num_elems ()), then the behavior of this function is undefined
	 *  and may cause memory access errors.
	 *
	 *  @warning
	 *  Also in the interest of speed, the CALLER is responsible for making
	 *  sure that the #cur_anc argument is valid. In general, this means
	 *  that #get_anc () was used to retrieve the anchor AND no operation
	 *  has been performed on #box since then that would invalidate it.
	 *
	 *  @warning
	 *  Generally speaking, this function is most useful when it is faster
	 *  to execute than #get_anc (). For some storage objects types,
	 *  however, the speed benefit may be very small.
	 *
	 *  @warning
	 *  If anchors are not supported by the #SK_BoxIntf implementation,
	 *  then this function should return #SK_NO_IMPL.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate other existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param cur_anc		The current anchor, which will have the
	 *				index associated with it reduced by 1.
	 *				This MUST point to a valid anchor.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* anc_dec)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		SK_BoxAnc *			cur_anc
	);




	/*!
	 *  Gets an "anchor" for an element at a given index of the storage
	 *  object.
	 *
	 *  See anc_support () for information about anchors.
	 *
	 *  @warning
	 *  If the storage object does not support anchors, this function
	 *  should NOT access / modify the contents at #anchor at all, and it
	 *  should return something other than #SLY_SUCCESS.
	 *
	 *  @warning
	 *  Using "anchors" are completely optional for a storage object that
	 *  supports them. It just _potentially_ provides efficiency benefits
	 *  for storage objects like linked-lists.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate other existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect.
	 *
	 *  @param index		The element index to determine the
	 *				"anchor" for
	 *
	 *  @param anchor		Where to place the value of the
	 *  				"anchor".
	 *
	 *  				This argument can NOT be NULL
	 *				(unless anchors are not supported,
	 *				which makes this function useless).
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				index,

		SK_BoxAnc *			anchor
	);




	/*!
	 *  Reports the average length (in number of elements) of memory
	 *  regions that can be obtained with get_region_ptr ().
	 *
	 *  @warning
	 *  The value that is reported for #avg_len is a best-effort estimate.
	 *  Implementations are not expected to precisely calculate this value,
	 *  though the more accurately they report this value, the better that
	 *  SK_Box_assign () can determine whether or not it should use
	 *  get_region_ptr (). The implementation of this function is expected
	 *  to execute very quickly, with many implementations just reporting
	 *  back the value of (req->stat_r->hint_elems_per_block).
	 *
	 *  @note
	 *  "Region pointers" are used whenever useful in SK_Box_assign () as
	 *  an optimization to make element copies faster.
	 *
	 *  However, if most of the pointed regions have only 1 element, then
	 *  attempting to use region pointers can potentially make element
	 *  copies SLOWER compared to basic element-by-element copies.
	 *
	 *  By providing this function, it allows SK_Box_assign () to make a
	 *  better determination if it should make use of get_region_ptr () or
	 *  not.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param avg_len		Where to place the average length (in
	 *  				number of elements) of memory regions
	 *  				that can be obtained by
	 *  				get_region_ptr ()
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* region_avg_len)
	(
		const SK_BoxReqs *		req,

		const SK_Box *			box,

		double *			avg_len
	);




	/*!
	 *  Gets a pointer to the largest contiguous memory region containing a
	 *  requested element that is present in a storage object.
	 *
	 *  In the worst-case scenario, the largest contiguous memory region
	 *  only contains the element at #ind.
	 *
	 *  To be explicit about what a "contiguous memory region" means in
	 *  this context, it means a memory region in which every byte is used
	 *  to store an element of size (req->stat_r->elem_size) by the storage
	 *  object #box. Incrementing an address by (req->stat_r->elem_size)
	 *  within this memory region obtains the address for the element at
	 *  the index +1 higher.
	 *
	 *  This function is intended to be used in conjunction with
	 *  #get_array () by SK_Box_assign () to perform efficient element
	 *  copies in between storage objects that implement #SK_BoxIntf.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param ind			The index of the element for which the
	 *				largest contiguous memory region
	 *				containing it should be found
	 *
	 *  @param reg_ptr		Where to place a pointer to start of
	 *				the memory region in which the element
	 *				at #ind is located
	 *
	 *				This argument CAN be NULL.
	 *
	 *  @param reg_beg		Where to place the index of the element
	 *  				at the start of the memory region which
	 *  				contains the element with index #ind.
	 *
	 *  				This argument CAN be NULL.
	 *
	 *  @param reg_end		Where to place the index of the element
	 *  				at the end of the memory region which
	 *  				contains the element with index #ind.
	 *
	 *  				This argument CAN be NULL.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_region_ptr)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		void **				reg_ptr,

		size_t *			reg_beg,

		size_t *			reg_end
	);




	/*!
	 *  Gets a pointer to an element at a specific index inside of the
	 *  storage object.
	 *
	 *  @warning
	 *  Pointers to elements in a storage object are considered INVALID
	 *  after any other function is used on the storage object, unless the
	 *  function states otherwise.
	 *
	 *  @warning
	 *  No guarantee is made whatsoever about the relative position of
	 *  other elements in relation to #elem_ptr. In other words, you can
	 *  NOT use (elem_ptr + elem_size) to get the pointer for the next
	 *  element.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param ind			The index of the element to get a
	 *				pointer to
	 *
	 *  @param elem_ptr		Where to place the element pointer
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_elem_ptr)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		void **				elem_ptr
	);




	/*!
	 *  Gets an element at a specific index inside the storage object.
	 *
	 *  @warning
	 *  For the purposes of accessing a large range of elements, it is
	 *  strongly recommended to use #get_array () or SK_Box_assign ()
	 *  instead of using this function in loop.
	 *
	 *  #get_array () / SK_Box_assign () can be up to 80x FASTER than this
	 *  function in a loop, due to using as large as possible contiguous
	 *  memory regions during the reading / writing process.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  				(Note, this is not a (const SK_Box *)
	 *  				because the object may be tracking the
	 *  				most frequently accessed element).
	 *
	 *  @param ind			The index of the element to retrieve
	 *
	 *  @param elem			Where to place the value of the element
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_elem)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		void *				elem
	);




	/*!
	 *  Sets an element at a specific index inside the storage object.
	 *
	 *  @warning
	 *  For the purposes of accessing a large range of elements, it is
	 *  strongly recommended to use #set_array () or SK_Box_assign ()
	 *  instead of using this function in loop.
	 *
	 *  #set_array () / SK_Box_assign () can be up to 80x FASTER than this
	 *  function in a loop, due to using as large as possible contiguous
	 *  memory regions during the reading / writing process.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to modify
	 *
	 *  @param ind			The index of the element to set
	 *
	 *  @param elem			The new value for the selected element
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* set_elem)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		const void *			elem
	);




	/*!
	 *  Gets a range of elements and places it into a "plain" array.
	 *
	 *  If the range [beg_ind, end_ind] includes (N) number of
	 *  elements, then that range will be saved in the range [0, N - 1]
	 *  inside of #array.
	 *
	 *  @warning
	 *  If there are not enough elements at #array to hold the range of
	 *  elements described by [beg_ind, end_ind], then the elements closer
	 *  to #end_ind will not be present in the #array.
	 *
	 *  @warning
	 *  If trying to copy from one storage object that implements
	 *  #SK_BoxIntf to another, it is better use SK_Box_assign (), which is
	 *  a higher level function.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to access
	 *
	 *  @param beg_ind		The index of the 1st element to access
	 *  				from #box
	 *
	 *  @param end_ind		The index of the last element to access
	 *				from #box
	 *
	 *  @param array_len		The length of #array measured in the
	 *				number of elements of size
	 *				(req->stat.elem_size) it can store
	 *
	 *  @param array		The array that elements should be
	 *				copied into
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_array)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				beg_ind,

		size_t				end_ind,

		size_t				array_len,

		void *				array
	);




	/*!
	 *  Uses a "plain" array to set a range of elements.
	 *
	 *  If the range [beg_ind, end_ind] includes (N) number of
	 *  elements, then that range will be read from the range [0, N - 1]
	 *  inside of #array.
	 *
	 *  @warning
	 *  If there are not enough elements at #array to supply the range of
	 *  elements described by [beg_ind, end_ind], then this function will
	 *  fail and should return #SLY_BAD_ARG.
	 *
	 *  @warning
	 *  If trying to copy from one storage object that implements
	 *  #SK_BoxIntf to another, it is better use SK_Box_assign (), which is
	 *  a higher level function.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to access
	 *
	 *  @param beg_ind		The index of the 1st element to access
	 *  				from #box
	 *
	 *  @param end_ind		The index of the last element to access
	 *				from #box
	 *
	 *  @param array_len		The length of #array measured in the
	 *				number of elements of size
	 *				(req->stat.elem_size) it can store
	 *
	 *  @param array		The array that elements should be
	 *				read from
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* set_array)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				beg_ind,

		size_t				end_ind,

		size_t				array_len,

		void *				array
	);




	/*!
	 *  Executes #rw_func on the elements inside the range of indices
	 *  [beg_ind, end_ind].
	 *
	 *  Depending on which function is supplied as #rw_func, this function
	 *  can be used to read elements, write elements, add an offset to
	 *  elements, etc.
	 *
	 *  This function is actually quite powerful because the behavior of
	 *  #rw_func is intentionally left very open-ended.
	 *
	 *  @warning
	 *  This accesses elements in the range [beg_ind, end_ind] using
	 *  #rw_func. However, if #beg_ind is greater than #end_ind, that is
	 *  considered to mean that no elements need to be set. In that event,
	 *  #SLY_NO_WORK should be returned.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to access the
	 *				elements of
	 *
	 *  @param beg_ind		The index of the 1st element to access
	 *				with #rw_func
	 *
	 *  @param end_ind		The index of the last element to access
	 *  				with #rw_func
	 *
	 *  @param rw_func		An #SK_BoxIntf_elem_rw_func that will
	 *				be applied to every element inside the
	 *				range [beg_ind, end_ind].
	 *
	 *				This can NOT be NULL.
	 *
	 *  @parma arb_arg		An arbitrary argument that will be
	 *				forwarded to #rw_func.
	 *
	 *				The meaning of #arb_arg is totally
	 *				dependent on the function selected as
	 *				#rw_func.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rw_elems)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				beg_ind,

		size_t				end_ind,

		SK_BoxIntf_elem_rw_func		rw_func,

		void *				arb_arg
	);




	/*!
	 *  Swaps the position of 2 elements in a storage object.
	 *
	 *  If elements [elem_0, elem_1] are located at (ind_0, ind_1), then
	 *  they will be located at (ind_1, ind_0) after this function
	 *  completes.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage to swap the object inside
	 *				of
	 *
	 *  @param ind_0		The index of the 1st element to swap
	 *				the position of
	 *
	 *  @param ind_1		The index of the 2nd element to swap
	 *				the position of
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* swap)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind_0,

		size_t				ind_1
	);




	/*!
	 *  Swaps the position of elements in 2 ranges of indices in a storage
	 *  object.
	 *
	 *  If the groups of elements [elems_0, elems_1] are located at
	 *  [[ind_a, ind_b], [ind_c, ind_d]], then they will be located at
	 *  [[ind_c, ind_d], [ind_a, ind_b]]  after this function completes.
	 *
	 *  @warning
	 *  The ranges of elements [ind_0, ind_0 + num_elems - 1] and
	 *  [ind_1, ind_1 + num_elems - 1] must NOT overlap. An implementation
	 *  can optionally return failure in this event.
	 *
	 *  @warning
	 *  If (ind_0 + (num_elems - 1)) or (ind_1 + (num_elems - 1)) are
	 *  greater than or equal to the number of elements in #box, memory
	 *  access errors may occur. An implementation can optionally return
	 *  failure in this event.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to swap the elements
	 *				inside of
	 *
	 *  @param num_elems		The number of elements located in the
	 *				ranges starting at #ind_0 and #ind_1
	 *
	 *  @param ind_0		Provides the index #ind_a for the range
	 *				of elements in [ind_a, ind_b]
	 *
	 *  @param ind_1		Provides the index #ind_c for the range
	 *				of elements in [ind_c, ind_d]
	 *
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* swap_range)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		size_t				ind_0,

		size_t				ind_1
	);




	/*!
	 *  Inserts an element into the storage object at the index #ind.
	 *
	 *  The element that was previously at index #ind will be moved to
	 *  (ind + 1).
	 *
	 *  If #ind is equal to the number of elements in the storage object,
	 *  then it will be appended to the end of the storage object.
	 *
	 *  @warning
	 *  If #ind is not in the range [0, num_elems], then this function will
	 *  fail.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to insert the
	 *				element into
	 *
	 *  @param ind			The index of where to insert the
	 *				element
	 *
	 *  @param elem			The value of the element to insert
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		const void *			elem
	);




	/*!
	 *  Adds a new element to the storage object at index 0, incrementing
	 *  the index of all the other elements by +1.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to insert the
	 *				element into
	 *
	 *  @param elem			The value of the element to insert
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_beg)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const void *			elem
	);




	/*!
	 *  Adds a new element to the storage object at the highest index,
	 *  leaving the index of all the other elements the same as they were
	 *  before.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to insert the
	 *				element into
	 *
	 *  @param elem			The value of the element to insert
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_end)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const void *			elem
	);




	/*!
	 *  Adds a range of new elements to the storage object starting at
	 *  #ind.
	 *
	 *  The element that was previously at index #ind will be moved to
	 *  (ind + num_elems).
	 *
	 *  If #ind is equal to the number of elements in the storage object,
	 *  then it will be appended to the end of the storage object.
	 *
	 *  (See the foot-note "Motivation for ins_range ()")
	 *
	 *  @warning
	 *  If #ind is not in the range [0, num_elems], then this function will
	 *  fail.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to insert the range
	 *				of element into
	 *
	 *  @param num_elems		The number of elements to insert into
	 *				#box
	 *
	 *  @param ind			The index of where to insert the range
	 *				of elements at
	 *
	 *  @param elem_init_func	See #elem_init_func in init ().
	 *
	 *  				Note that this function will only be
	 *  				used on the newly inserted elements in
	 *  				#box.
	 *
	 *  @param arb_arg		See #arb_arg in init ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_range)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		size_t				ind,

		SK_BoxIntf_elem_rw_func		elem_init_func,

		void *				arb_arg
	);




	/*!
	 *  Version of ins_range () that does NOT initialize the newly-inserted
	 *  elements in the storage object.
	 *
	 *  @warning
	 *  Since there is no guarantee on what the value of newly-inserted
	 *  elements will be in the storage object, implementations CAN choose
	 *  to initialize elements with values like 0 or 0x0BADBEEF, though
	 *  doing so reduces the utility of this function.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to insert the range
	 *				of element into
	 *
	 *  @param num_elems		The number of elements to insert into
	 *				#box
	 *
	 *  @param ind			The index of where to insert the range
	 *				of elements at
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_range_unsafe)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		size_t				ind
	);




	//TODO : Should there be an explicit ins_array () function to insert an
	//array, or is having both ins_range_unsafe () and set_array () already
	//provided sufficient?




	/*!
	 *  Removes an element from the storage object at the index #ind
	 *
	 *  The element that was previously at index (ind + 1) will be moved to
	 *  (ind).
	 *
	 *  @warning
	 *  If #ind is not in the range [0, (num_elems - 1)], then this
	 *  function will fail.
	 *
	 *  @note
	 *  Take notice that this function returns a #SlyDr instead of a #SlySr
	 *  like ins ().
	 *
	 *  The reasons for this are :
	 *
	 *    1)
	 *
	 *    	This design is better aligned with the pattern that
	 *    	initializations can be stochastic while deinitializations
	 *    	should always be deterministic.
	 *
	 *    2)
	 *
	 *      Consider the scenario where there are 2 #SK_Box's that MUST be
	 *      kept in sync with each other, and the same indexed element
	 *      must be removed from both. What should happen if rem ()
	 *      succeeds for 1 #SK_Box and fails for the other #SK_Box? Keep in
	 *      mind that ins () is stochastic, so putting the element back
	 *      into the #SK_Box from the one it was removed is not possible to
	 *      _guarantee_ as a fallback.
	 *
	 *  The fact that this function is deterministic does NOT mean that a
	 *  memory reallocation can not be _attempted_ on #box. However, it
	 *  DOES mean that if that memory reallocation fails, the element at
	 *  #ind must still be considered "removed" from the #SK_Box, by
	 *  somehow marking it as "unused" or removing it in some other
	 *  fashion.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to remove the
	 *				element from
	 *
	 *  @param ind			The index of the element to remove
	 *
	 *  @param elem			Where to place the value of the element
	 *				as it was right before it was removed.
	 *
	 *				This CAN be NULL if this is not
	 *				desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rem)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		void *				elem
	);




	/*!
	 *  Removes the element from the storage object at index 0,
	 *  decrementing the index of all the other elements by 1.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to remove the
	 *				element from
	 *
	 *  @param elem			Where to place the value of the element
	 *				as it was right before it was removed.
	 *
	 *				This CAN be NULL if this is not
	 *				desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rem_beg)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		void *				elem
	);




	/*!
	 *  Removes the element from the storage object at the highest index,
	 *  leaving the index of all the other elements the same as they were
	 *  before.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to remove the
	 *				element from
	 *
	 *  @param elem			Where to place the value of the element
	 *				as it was right before it was removed.
	 *
	 *				This CAN be NULL if this is not
	 *				desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rem_end)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		void *				elem
	);




	/*!
	 *  Removes a range of elements from the storage object.
	 *
	 *  The elements that were previously at index (ind + num_elems) will
	 *  be moved to (ind).
	 *
	 *  @warning
	 *  If the range [ind, (ind + num_elems - 1)] is not in the range
	 *  [0, (num_elems - 1)], then this function will fail.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to remove the
	 *				elements from
	 *
	 *  @param num_elems		The number of elements to remove from
	 *				#box
	 *
	 *  @param ind			The index of where to remove elements
	 *				from
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rem_range)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		size_t				ind
	);




	/*!
	 *  Gets an "anchor" for an element at a given index of the storage
	 *  object, but while using a separate anchor to help.
	 *
	 *  See anc_support () for information about anchors.
	 *
	 *  (See the foot-note "Const-qualification of #cur_anc")
	 *
	 *  @warning
	 *  If the storage object does not support anchors, this function
	 *  should NOT access / modify the contents at #new_anc at all, and
	 *  it should return something other than #SLY_SUCCESS.
	 *
	 *  @warning
	 *  Using "anchors" are completely optional for a storage data
	 *  structure that supports them. It just _potentially_ provides
	 *  efficiency benefits for storage objects like linked-lists.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect.
	 *
	 *  @param ind			The element index to determine the
	 *				"anchor" for
	 *
	 *  @param cur_anc		The anchor to use when accessing the
	 *				element at #ind.
	 *
	 *				This CAN be NULL, in which case no
	 *				anchor will be used.
	 *
	 *  @param new_anc		Where to place the value of the anchor
	 *				associated with the element at #ind.
	 *
	 *				In the event this is the same pointer
	 *				as #cur_anc, then this memory will be
	 *				overwritten AFTER #cur_anc has already
	 *				been used.
	 *
	 *  				This argument can NOT be NULL.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_anc_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc
	);




	/*!
	 *  Version of get_region_ptr () that accepts anchor arguments.
	 *
	 *  @note
	 *  See the foot-note "Motivation for #prev_anc / #next_anc".
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param ind			The index of the element for which the
	 *				largest contiguous memory region
	 *				containing it should be found
	 *
	 *  @param cur_anc		The anchor to use when accessing the
	 *				element at #ind.
	 *
	 *				This CAN be NULL, in which case no
	 *				anchor will be used
	 *
	 *  @param prev_anc		Where to place the value of the anchor
	 *				associated with the element at
	 *				(reg_beg - 1). If that value is lesser
	 *				than or equal to 0, then it will
	 *				instead be associated with the element
	 *				at #reg_beg.
	 *
	 *				In the event this is the same pointer
	 *				as #cur_anc, then this memory will be
	 *				overwritten AFTER #cur_anc has already
	 *				been used.
	 *
	 *				This argument CAN be NULL, in which
	 *				case this anchor will not be produced.
	 *
	 *  @param next_anc		Where to place the value of the anchor
	 *				associated with the element at
	 *				(reg_end + 1). If that value is greater
	 *				than or equal to the number of elements
	 *				in #box, then it will instead be
	 *				associated with the element at
	 *				#reg_end.
	 *
	 *				In the event this is the same pointer
	 *				as #cur_anc, then this memory will be
	 *				overwritten AFTER #cur_anc has already
	 *				been used.
	 *
	 *				In the event this is the same pointer
	 *				as #prev_anc, the argument of will take
	 *				precedence.
	 *
	 *				This argument CAN be NULL, in which
	 *				case this anchor will not be produced.
	 *
	 *  @param reg_ptr		Where to place a pointer to start of
	 *				the memory region in which the element
	 *				at #ind is located
	 *
	 *				This argument CAN be NULL.
	 *
	 *  @param reg_beg		Where to place the index of the element
	 *  				at the start of the memory region which
	 *  				contains the element with index #ind.
	 *
	 *  				This argument CAN be NULL.
	 *
	 *  @param reg_end		Where to place the index of the element
	 *  				at the end of the memory region which
	 *  				contains the element with index #ind.
	 *
	 *  				This argument CAN be NULL.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_region_ptr_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			prev_anc,

		SK_BoxAnc *			next_anc,

		void **				reg_ptr,

		size_t *			reg_beg,

		size_t *			reg_end
	);




	/*!
	 *  Version of get_elem_ptr () that accepts anchor arguments.
	 *
	 *  @warning
	 *  Pointers to elements in a storage object are considered INVALID
	 *  after any other function is used on the storage object, unless the
	 *  function states otherwise.
	 *
	 *  @warning
	 *  No guarantee is made whatsoever about the relative position of
	 *  other elements in relation to #elem_ptr. In other words, you can
	 *  NOT use (elem_ptr + elem_size) to get the pointer for the next
	 *  element.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param ind			The index of the element to get a
	 *				pointer for
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  @param elem_ptr		Where to place the element pointer
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_elem_ptr_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		void **				elem_ptr
	);




	/*!
	 *  Version of get_elem () that accepts anchor arguments.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  				(Note, this is not a (const void *)
	 *  				because the object may be tracking the
	 *  				most frequently accessed element).
	 *
	 *  @param ind			The index of the element to retrieve
	 *
	 *  @param cur_anc		The anchor to use when accessing the
	 *				element at #ind.
	 *
	 *				This CAN be NULL, in which case no
	 *				anchor will be used
	 *
	 *  @param new_anc		Where to place the value of the anchor
	 *				associated with the element at #ind.
	 *
	 *				In the event this is the same pointer
	 *				as #cur_anc, then this memory will be
	 *				overwritten AFTER #cur_anc has already
	 *				been used.
	 *
	 *				Unlike in get_anc_w_anc (), this
	 *				argument CAN be NULL, in which case no
	 *				new anchor will be produced.
	 *
	 *  @param elem			Where to place the value of the element
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_elem_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		void *				elem
	);




	/*!
	 *  Version of set_elem () that accepts anchor arguments.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to modify
	 *
	 *  @param ind			The index of the element to set
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  @param elem			The new value for the selected element
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* set_elem_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		const void *			elem
	);




	/*!
	 *  Version of #get_array () that accepts additional anchor arguments.
	 *
	 *  @warning
	 *  If there are not enough elements at #array to hold the range of
	 *  elements described by [beg_ind, end_ind], then the elements closer
	 *  to #end_ind will not be present in the #array.
	 *
	 *  @warning
	 *  If trying to copy from one storage object that implements
	 *  #SK_BoxIntf to another, it is better use SK_Box_assign (), which is
	 *  a higher level function.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to access
	 *
	 *  @param beg_ind		The index of the 1st element to access
	 *  				from #box
	 *
	 *  @param end_ind		The index of the last element to access
	 *				from #box
	 *
	 *  @param new_anc_ind		The element index of the element that
	 *				#new_anc should be in reference to.
	 *
	 *				This MAY be outside the range
	 *				[beg_ind, end_ind], but if so,
	 *				retrieving the anchor should not be
	 *				considered an efficient operation.
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *  				This anchor will generally only be
	 *  				useful if it is in reference to an
	 *  				element "near" the range
	 *  				[beg_ind, end_ind], preferably near
	 *  				#beg_ind in particular.
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  @param array_len		The length of #array measured in the
	 *				number of elements of size
	 *				(req->stat.elem_size) it can store
	 *
	 *  @param array		The array that elements should be
	 *				copied into
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_array_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				beg_ind,

		size_t				end_ind,

		size_t				new_anc_ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		size_t				array_len,

		void *				array
	);




	/*!
	 *  Version of #set_array () that accepts additional anchor arguments.
	 *
	 *  If the range [beg_ind, end_ind] includes (N) number of
	 *  elements, then that range will be read from the range [0, N - 1]
	 *  inside of #array.
	 *
	 *  @warning
	 *  If there are not enough elements at #array to supply the range of
	 *  elements described by [beg_ind, end_ind], then this function will
	 *  fail and should return #SLY_BAD_ARG.
	 *
	 *  @warning
	 *  If trying to copy from one storage object that implements
	 *  #SK_BoxIntf to another, it is better use SK_Box_assign (), which is
	 *  a higher level function.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to access
	 *
	 *  @param beg_ind		The index of the 1st element to access
	 *  				from #box
	 *
	 *  @param end_ind		The index of the last element to access
	 *				from #box
	 *
	 *  @param new_anc_ind		The element index of the element that
	 *				#new_anc should be in reference to.
	 *
	 *				This MAY be outside the range
	 *				[beg_ind, end_ind], but if so,
	 *				retrieving the anchor should not be
	 *				considered an efficient operation.
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *  				This anchor will generally only be
	 *  				useful if it is in reference to an
	 *  				element "near" the range
	 *  				[beg_ind, end_ind], preferably near
	 *  				#beg_ind in particular.
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  @param array_len		The length of #array measured in the
	 *				number of elements of size
	 *				(req->stat.elem_size) it can store
	 *
	 *  @param array		The array that elements should be
	 *				read from
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* set_array_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				beg_ind,

		size_t				end_ind,

		size_t				new_anc_ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		size_t				array_len,

		const void *			array
	);




	/*!
	 *  Version of #rw_elems () that accepts additional anchor arguments.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to access the
	 *				elements of
	 *
	 *  @param beg_ind		The index of the 1st element to access
	 *				with #rw_func
	 *
	 *  @param end_ind		The index of the last element to access
	 *  				with #rw_func
	 *
	 *  @param new_anc_ind		The element index of the element that
	 *				#new_anc should be in reference to.
	 *
	 *				This MAY be outside the range
	 *				[beg_ind, end_ind], but if so,
	 *				retrieving the anchor should not be
	 *				considered an efficient operation.
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *  				This anchor will generally only be
	 *  				useful if it is in reference to an
	 *  				element "near" the range
	 *  				[beg_ind, end_ind], preferably near
	 *  				#beg_ind in particular.
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  @param rw_func		An #SK_BoxIntf_elem_rw_func that will
	 *				be applied to every element inside the
	 *				range [beg_ind, end_ind].
	 *
	 *				This can NOT be NULL.
	 *
	 *  @parma arb_arg		An arbitrary argument that will be
	 *				forwarded to #rw_func.
	 *
	 *				The meaning of #arb_arg is totally
	 *				dependent on the function selected as
	 *				#rw_func.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rw_elems_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				beg_ind,

		size_t				end_ind,

		size_t				new_anc_ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		SK_BoxIntf_elem_rw_func		rw_func,

		void *				arb_arg
	);




	/*!
	 *  Version of swap () that accepts anchor arguments
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to swap the elements
	 *				inside of
	 *
	 *  @param ind_0		The index of the 1st element to swap
	 *				the position of
	 *
	 *  @param cur_anc_0		See #cur_anc in get_elem_w_anc ()
	 *
	 *  				This is in reference to the element at
	 *  				#ind_0.
	 *
	 *  @param new_anc_0		See #new_anc get_elem_w_anc ()
	 *
	 *  				This is in reference to the element at
	 *  				#ind_0.
	 *
	 *  @param ind_1		The index of the 2nd element to swap
	 *				the position of
	 *
	 *  @param cur_anc_1		See #cur_anc get_elem_w_anc ()
	 *
	 *  				This is in reference to the element at
	 *  				#ind_1.
	 *
	 *  @param new_anc_1		See #new_anc get_elem_w_anc ()
	 *
	 *  				This is in reference to the element at
	 *  				#ind_1.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* swap_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind_0,

		const SK_BoxAnc *		cur_anc_0,

		SK_BoxAnc *			new_anc_0,

		size_t				ind_1,

		const SK_BoxAnc *		cur_anc_1,

		SK_BoxAnc *			new_anc_1
	);




	/*!
	 *  Version of swap_range () that accepts anchor arguments
	 *
	 *  @warning
	 *  The ranges of elements [ind_0, ind_0 + num_elems - 1] and
	 *  [ind_1, ind_1 + num_elems - 1] must NOT overlap. An implementation
	 *  can optionally return failure in this event.
	 *
	 *  @warning
	 *  If (ind_0 + (num_elems - 1)) or (ind_1 + (num_elems - 1)) are
	 *  greater than the number of elements in #box, memory access errors
	 *  may occur. An implementation can optionally return failure in this
	 *  event instead.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to swap the elements
	 *				inside of
	 *
	 *  @param num_elems		The number of elements contained in the
	 *				element ranges starting at #ind_0 and
	 *				#ind_1
	 *
	 *  @param ind_0		Provides the index #ind_a for the range
	 *				of elements in [ind_a, ind_b]
	 *
	 *  @param new_anc_0_ind	The index of the element that
	 *				#new_anc_0 should be made to reference.
	 *
	 *				Preferably, this will have a value in
	 *				between [ind_0, ind_0 + num_elems - 1].
	 *
	 *
	 *  @param cur_anc_0		See #cur_anc in get_elem_w_anc ()
	 *
	 *  				This is in reference to the element at
	 *  				#ind_0.
	 *
	 *  @param new_anc_0		See #new_anc get_elem_w_anc ()
	 *
	 *  				This is in reference to the element at
	 *  				#ind_0.
	 *
	 *  @param ind_1		Provides the index #ind_c for the range
	 *				of elements in [ind_c, ind_d]
	 *
	 *  @param new_anc_1_ind	The index of the element that
	 *				#new_anc_1 should be made to reference.
	 *
	 *				Preferably, this will have a value in
	 *				between [ind_1, ind_1 + num_elems - 1].
	 *
	 *  @param cur_anc_1		See #cur_anc in get_elem_w_anc ()
	 *
	 *  				This is in reference to the element at
	 *  				#ind_1.
	 *
	 *  @param new_anc_1		See #new_anc get_elem_w_anc ()
	 *
	 *  				This is in reference to the element at
	 *  				#ind_1.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* swap_range_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		size_t				ind_0,

		size_t				new_anc_0_ind,

		const SK_BoxAnc *		cur_anc_0,

		SK_BoxAnc *			new_anc_0,

		size_t				ind_1,

		size_t				new_anc_1_ind,

		const SK_BoxAnc *		cur_anc_1,

		SK_BoxAnc *			new_anc_1
	);




	/*!
	 *  Version of ins () that accepts anchor arguments.
	 *
	 *  The element that was previously at index #ind will be moved to
	 *  (ind + 1).
	 *
	 *  If #ind is equal to the number of elements in the storage object,
	 *  then it will be appended to the end of the storage object.
	 *
	 *  @warning
	 *  If #ind is not in the range [0, num_elems], then this function will
	 *  fail.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to insert the
	 *				element into
	 *
	 *  @param ind			The index of where to insert the
	 *				element
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *				Note, this anchor should be in
	 *				reference to an element "near" #ind.
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  				Note, this anchor will be in reference
	 *  				to the new element at #ind.
	 *
	 *  @param elem			Pointer to the element value to insert.
	 *
	 *				If this is NULL, then the bytes of the
	 *				newly inserted element will be set to
	 *				0.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		const void *			elem
	);




	/*!
	 *  Version of ins_beg () that accepts anchor arguments.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *  				Note, this anchor should be in
	 *  				reference to an element near index 0.
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  				Note, this anchor will be in reference
	 *  				to the new element at index 0.
	 *
	 *  @param box			The storage object to insert the
	 *				element into
	 *
	 *  @param elem			Pointer to the element value to insert.
	 *
	 *				If this is NULL, then the bytes of the
	 *				newly inserted element will be set to
	 *				0.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_beg_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		const void *			elem
	);




	/*!
	 *  Version of ins_end () that accepts anchor arguments.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *  				Note, this anchor should be in
	 *  				reference to an element "near" the
	 *  				highest index.
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  				Note, this anchor will be in reference
	 *  				to the element at the highest index
	 *  				(which will be the newly appended
	 *  				element).
	 *
	 *  @param box			The storage object to insert the
	 *				element into
	 *
	 *  @param elem			Pointer to the element value to insert.
	 *
	 *				If this is NULL, then the bytes of the
	 *				newly inserted element will be set to
	 *				0.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_end_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		const void *			elem
	);




	/*!
	 *  Version of ins_range () that accepts additional anchor arguments.
	 *
	 *  @warning
	 *  If #ind is not in the range [0, num_elems], then this function will
	 *  fail.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to insert the range
	 *				of element into
	 *
	 *  @param num_elems		The number of elements to insert into
	 *				#box
	 *
	 *  @param ind			The index of where to insert the range
	 *				of elements at
	 *
	 *  @param new_anc_ind		The index of the element that #new_anc
	 *				should be made to reference.
	 *
	 *				Preferably, this will have a value in
	 *				the range [ind, (ind + num_elems - 1)].
	 *
	 *  @param cur_anc		See #cur_anc in get_elem_w_anc ().
	 *
	 *  @param new_anc		See #new_anc in get_elem_w_anc ().
	 *
	 *  @param elem_init_func	See #elem_init_func in init ().
	 *
	 *  				Note that this function will only be
	 *  				used on the newly inserted elements in
	 *  				#box.
	 *
	 *  @param arb_arg		See #arb_arg in init ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_range_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		size_t				ind,

		size_t				new_anc_ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		SK_BoxIntf_elem_rw_func		elem_init_func,

		void *				arb_arg
	);




	/*!
	 *  Version of ins_range_unsafe () that accepts additional anchor
	 *  arguments.
	 *
	 *  @warning
	 *  If #ind is not in the range [0, num_elems], then this function will
	 *  fail.
	 *
	 *  @warning
	 *  Since there is no guarantee on what the value of newly-inserted
	 *  elements will be in the storage object, implementations CAN choose
	 *  to initialize elements with values like 0 or 0x0BADBEEF, though
	 *  doing so reduces the utility of this function.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to insert the range
	 *				of element into
	 *
	 *  @param num_elems		The number of elements to insert into
	 *				#box
	 *
	 *  @param ind			The index of where to insert the range
	 *				of elements at
	 *
	 *  @param new_anc_ind		The index of the element that #new_anc
	 *				should be made to reference.
	 *
	 *				Preferably, this will have a value in
	 *				the range [ind, (ind + num_elems - 1)].
	 *
	 *  @param cur_anc		See #cur_anc in get_elem_w_anc ().
	 *
	 *  @param new_anc		See #new_anc in get_elem_w_anc ().
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_range_unsafe_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		size_t				ind,

		size_t				new_anc_ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc
	);




	/*!
	 *  Version of rem () that accepts anchor arguments.
	 *
	 *  The element that was previously at index (ind + 1) will be moved to
	 *  #ind.
	 *
	 *  @warning
	 *  If #ind is not in the range [0, (num_elems - 1)], then this
	 *  function will fail.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to remove the
	 *				element from
	 *
	 *  @param ind			The index of the element to remove
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *				Note, this anchor should be in
	 *				reference to an element "near" #ind.
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  				Note, this anchor will be in reference
	 *  				to the element moved to #ind.
	 *
	 *  @param elem			Where to place the value of the element
	 *				as it was right before it was removed.
	 *
	 *				This CAN be NULL if this is not
	 *				desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rem_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		void *				elem
	);




	/*!
	 *  Version of rem_beg () that accepts anchor arguments.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to remove the
	 *				element from
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *				Note, this anchor should be in
	 *				reference to an element "near" index 0.
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  				Note, this anchor will be in reference
	 *  				to the element moved to index 0.
	 *
	 *  @param elem			Where to place the value of the element
	 *				as it was right before it was removed.
	 *
	 *				This CAN be NULL if this is not
	 *				desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rem_beg_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		void *				elem
	);




	/*!
	 *  Version of rem_end () that accepts anchor arguments.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to remove the
	 *				element from
	 *
	 *  @param cur_anc		See get_elem_w_anc ()
	 *
	 *				Note, this anchor should be in
	 *				reference to an element "near" the
	 *				highest index.
	 *
	 *  @param new_anc		See get_elem_w_anc ()
	 *
	 *  				Note, this anchor will be in reference
	 *  				to the element moved to the highest
	 *  				index.
	 *
	 *  @param elem			Where to place the value of the element
	 *				as it was right before it was removed.
	 *
	 *				This CAN be NULL if this is not
	 *				desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rem_end_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc,

		void *				elem
	);




	/*!
	 *  Version of rem_range () that accepts anchor arguments.
	 *
	 *  @warning
	 *  If the range [ind, (ind + num_elems - 1)] is not in the range
	 *  [0, (num_elems - 1)], then this function will fail.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to remove the
	 *				elements from
	 *
	 *  @param num_elems		The number of elements to remove from
	 *				#box
	 *
	 *  @param ind			The index of where to remove elements
	 *				from
	 *
	 *  @param new_anc_ind		The index of the element that #new_anc
	 *				should be made to reference.
	 *
	 *				Preferably, this will have a value in
	 *				the range [ind, (ind + num_elems - 1)].
	 *
	 *  @param cur_anc		See #cur_anc in get_elem_w_anc ().
	 *
	 *  @param new_anc		See #new_anc in get_elem_w_anc ().
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rem_range_w_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		size_t				ind,

		size_t				new_anc_ind,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			new_anc
	);




	/*!
	 *  Note, the following *_by_anc () functions are very similar to the
	 *  *_w_anc () functions. Instead of using anchors in conjunction with
	 *  indices, however, only the anchors are used to access elements.
	 */




	/*!
	 *  Version of get_region_ptr () that accepts an anchor instead of an
	 *  index.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param cur_anc		The anchor to use when accessing the
	 *				element at #ind.
	 *
	 *				This CAN be NULL, in which case no
	 *				anchor will be used
	 *
	 *  @param prev_anc		Where to place the value of the anchor
	 *				associated with the element at
	 *				(reg_beg - 1). If that value is lesser
	 *				than or equal to 0, then it will
	 *				instead be associated with the element
	 *				at #reg_beg.
	 *
	 *				In the event this is the same pointer
	 *				as #cur_anc, then this memory will be
	 *				overwritten AFTER #cur_anc has already
	 *				been used.
	 *
	 *				This argument CAN be NULL, in which
	 *				case this anchor will not be produced.
	 *
	 *  @param next_anc		Where to place the value of the anchor
	 *				associated with the element at
	 *				(reg_end + 1). If that value is greater
	 *				than or equal to the number of elements
	 *				in #box, then it will instead be
	 *				associated with the element at
	 *				#reg_end.
	 *
	 *				In the event this is the same pointer
	 *				as #cur_anc, then this memory will be
	 *				overwritten AFTER #cur_anc has already
	 *				been used.
	 *
	 *				In the event this is the same pointer
	 *				as #prev_anc, the argument of will take
	 *				precedence.
	 *
	 *				This argument CAN be NULL, in which
	 *				case this anchor will not be produced.
	 *
	 *  @param reg_ptr		Where to place a pointer to start of
	 *				the memory region in which the element
	 *				at #ind is located
	 *
	 *				This argument CAN be NULL.
	 *
	 *  @param reg_beg		Where to place the index of the element
	 *  				at the start of the memory region which
	 *  				contains the element with index #ind.
	 *
	 *  				This argument CAN be NULL.
	 *
	 *  @param reg_end		Where to place the index of the element
	 *  				at the end of the memory region which
	 *  				contains the element with index #ind.
	 *
	 *  				This argument CAN be NULL.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_region_ptr_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		cur_anc,

		SK_BoxAnc *			prev_anc,

		SK_BoxAnc *			next_anc,

		void **				reg_ptr,

		size_t *			reg_beg,

		size_t *			reg_end
	);




	/*!
	 *  Version of get_elem_ptr () that accepts an anchor instead of an
	 *  index.
	 *
	 *  @warning
	 *  Pointers to elements in a storage object are considered INVALID
	 *  after any other function is used on the storage object, unless the
	 *  function states otherwise.
	 *
	 *  @warning
	 *  No guarantee is made whatsoever about the relative position of
	 *  other elements in relation to #elem_ptr. In other words, you can
	 *  NOT use (elem_ptr + elem_size) to get the pointer for the next
	 *  element.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  @param anc			An anchor associated with the selected
	 *				element
	 *
	 *  @param elem_ptr		Where to place the element pointer
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_elem_ptr_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		anc,

		void **				elem_ptr
	);




	/*!
	 *  Version of get_elem () that accepts an anchor instead of an index.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to inspect
	 *
	 *  				(Note, this is not a (const void *)
	 *  				because the object may be tracking the
	 *  				most frequently accessed element).
	 *
	 *  @param anc			An anchor associated with the selected
	 *				element
	 *
	 *  @param elem			Where to place the value of the element
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_elem_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		anc,

		void *				elem
	);




	/*!
	 *  Version of set_elem () that accepts an anchor instead of an index.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to modify
	 *
	 *  @param anc			An anchor associated with the selected
	 *				element
	 *
	 *  @param elem			The new value for the selected element
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* set_elem_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		anc,

		const void *			elem
	);




	/*!
	 *  Version of #get_array () that accepts achors instead of indices.
	 *
	 *  @warning
	 *  If there are not enough elements at #array to hold the range of
	 *  elements described by [beg_anc, end_anc], then the elements closer
	 *  to #end_anc will not be present in the #array.
	 *
	 *  @warning
	 *  If trying to copy from one storage object that implements
	 *  #SK_BoxIntf to another, it is better use SK_Box_assign (), which is
	 *  a higher level function.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to access
	 *
	 *  @param beg_anc		An anchor associated with the 1st
	 *				element to read
	 *
	 *  @param end_anc		An anchor associated with the last
	 *				element to read
	 *
	 *  @param array_len		The length of #array measured in the
	 *				number of elements of size
	 *				(req->stat.elem_size) it can store
	 *
	 *  @param array		The array that elements should be
	 *				copied into
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_array_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		beg_anc,

		const SK_BoxAnc *		end_anc,

		size_t				array_len,

		void *				array
	);




	/*!
	 *  Version of #set_array () that accepts achors instead of indices.
	 *
	 *  @warning
	 *  If there are not enough elements at #array to supply the range of
	 *  elements described by [beg_anc, end_anc], then this function will
	 *  fail and should return #SLY_BAD_ARG.
	 *
	 *  @warning
	 *  If trying to copy from one storage object that implements
	 *  #SK_BoxIntf to another, it is better use SK_Box_assign (), which is
	 *  a higher level function.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to access
	 *
	 *  @param beg_anc		An anchor associated with the 1st
	 *				element to write
	 *
	 *  @param end_anc		An anchor associated with the last
	 *				element to write
	 *
	 *  @param array_len		The length of #array measured in the
	 *				number of elements of size
	 *				(req->stat.elem_size) it can store
	 *
	 *  @param array		The array that elements should be
	 *				read from
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* set_array_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		beg_anc,

		const SK_BoxAnc *		end_anc,

		size_t				array_len,

		const void *			array
	);




	/*!
	 *  Version of #rw_elems () that accepts achors instead of indices.
	 *
	 *  @warning
	 *  #beg_anc must be associated with an element that has an index
	 *  lesser than or equal to the index of the element associated with
	 *  #end_anc.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to access the
	 *				elements of
	 *
	 *  @param beg_anc		An anchor associated with the 1st
	 *				element to access with #rw_func
	 *
	 *  @param end_anc		An anchor associated with the last
	 *				element to access with #rw_func
	 *
	 *  @param rw_func		An #SK_BoxIntf_elem_rw_func that will
	 *				be applied to every element inside the
	 *				range [beg_ind, end_ind].
	 *
	 *				This can NOT be NULL.
	 *
	 *  @parma arb_arg		An arbitrary argument that will be
	 *				forwarded to #rw_func.
	 *
	 *				The meaning of #arb_arg is totally
	 *				dependent on the function selected as
	 *				#rw_func.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rw_elems_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		beg_anc,

		const SK_BoxAnc *		end_anc,

		SK_BoxIntf_elem_rw_func		rw_func,

		void *				arb_arg
	);




	/*!
	 *  Version of #swap () that accepts achors instead of indices.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to swap the elements
	 *				inside of
	 *
	 *  @param anc_0		An anchor associated with the 1st
	 *				selected element
	 *
	 *  @param anc_0		An anchor associated with the 2nd
	 *				selected element
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* swap_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		const SK_BoxAnc *		anc_0,

		const SK_BoxAnc *		anc_1
	);




	/*!
	 *  Version of #swap_range () that accepts achors instead of indices.
	 *
	 *  @warning
	 *  The ranges of elements [ind_a, ind_b] and [ind_c, ind_d] must NOT
	 *  overlap. An implementation can optionally return failure in this
	 *  event.
	 *
	 *  @warning
	 *  If (ind_b) or (ind_d) are greater than the number of elements in
	 *  #box, memory access errors may occur. An implementation can
	 *  optionally return failure in this event instead.
	 *
	 *  @warning
	 *  Implementations of this function must NOT invalidate existing
	 *  element pointers or anchors.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to swap the elements
	 *				inside of
	 *
	 *  @param anc_0		An anchor associated with the element
	 *				at #ind_a for the range of elements in
	 *				[ind_a, ind_b]
	 *
	 *  @param anc_1		An anchor associated with the element
	 *				at #ind_c for the range of elements in
	 *				[ind_c, ind_d]
	 *
	 *  @param num_elems		The number of elements located in the
	 *				ranges starting at (ind_a) and (ind_c)
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* swap_range_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		size_t				num_elems,

		const SK_BoxAnc *		anc_0,

		const SK_BoxAnc *		anc_1
	);




	//FIXME : Should the below functions use a singular #anc argument or a
	//#cur_anc and #new_anc argument?




	/*!
	 *  Inserts a new element in the storage object BEFORE the element
	 *  selected by the anchor.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to insert the
	 *				element into
	 *
	 *  @param anc			An anchor to the element to insert the
	 *				new element before.
	 *
	 *				Note, this argument is a (SK_BoxAnc *)
	 *				and not a (const SK_BoxAnc *). This is
	 *				because the anchor must be updated to
	 *				be associated with the newly inserted
	 *				element.
	 *
	 *  @param elem			The value of the element to insert
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_bef_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		SK_BoxAnc *			anc,

		const void *			elem
	);




	/*!
	 *  Inserts a new element in the storage object AFTER the element
	 *  selected by the anchor.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to insert the
	 *				element into
	 *
	 *  @param anc			An anchor to the element to insert the
	 *				new element before.
	 *
	 *				Note, this argument is a (SK_BoxAnc *)
	 *				and not a (const SK_BoxAnc *). This is
	 *				because the anchor must be updated to
	 *				be associated with the newly inserted
	 *				element.
	 *
	 *  @param elem			The value of the element to insert
	 *
	 *  @return			Standard status code
	 */

	SlySr (* ins_aft_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		SK_BoxAnc *			anc,

		const void *			elem
	);




	//TODO : Consider adding ins_range_bef_by_anc () and
	//ins_range_aft_by_anc () if they are deemed useful. I am undecided as
	//to what the argument list for such functions should look like, so I
	//have opted to not add them at this time.




	/*!
	 *  Version of #rem () that accepts an achor instead of an index.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param box			The storage object to remove the
	 *				element from
	 *
	 *  @param anc			An anchor associated with the element
	 *				to remove.
	 *
	 *				Note, this argument is a (SK_BoxAnc *)
	 *				and not a (const SK_BoxAnc *). This is
	 *				because the anchor must be updated to
	 *				be associated with the element that has
	 *				the closest index to the index of the
	 *				element that was removed.
	 *
	 *				If there are no more elements left in
	 *				#req, the anchor will simply be
	 *				invalidated.
	 *
	 *  @param elem			Where to place the value of the element
	 *				as it was right before it was removed.
	 *
	 *				This CAN be NULL if this is not
	 *				desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* rem_by_anc)
	(
		const SK_BoxReqs *		req,

		SK_Box *			box,

		SK_BoxAnc *			anc,

		void *				elem
	);




	//TODO : Consider adding rem_range_by_anc () if it is deemed useful. I
	//am undecided as to what the argument list for such functions should
	//look like, so I have opted to not add them at this time.

}

SK_BoxIntf;




/*!
 *  This is a version of #SK_BoxIntf that makes use of just-in-time (JIT)
 *  compilation at run-time. Utilizing JIT'ing allows for storage objects to be
 *  optimized heavily for the static parameters of a given storage object.
 *
 *  Unlike #SK_BoxIntf, it must be initialized / deinitialized with
 *  *_init () / *_deinit () functions respectively. These functions will accept
 *  the same kind of #SK_BoxReqs as used by #SK_BoxIntf functions.
 */

typedef struct SK_BoxIntf SK_BoxJit;




SlyDr SK_BoxReqs_point_to_arq
(
	SK_BoxReqs *			req,

	const SK_BoxAllReqs *		arq
);




SlyDr SK_BoxStatImpl_prep
(
	const SK_BoxStatReqs *	stat_r,

	SK_BoxStatImpl *	stat_i
);




SlyDr SK_BoxDynaImpl_prep
(
	const SK_BoxStatReqs *	stat_r,

	const SK_BoxStatImpl *	stat_i,

	const SK_BoxDynaReqs *	dyna_r,

	SK_BoxDynaImpl *	dyna_i
);




SlyDr SK_BoxReqs_prep
(
	SK_BoxReqs *			req,

	const SK_BoxStatReqs *		stat_r,

	SK_BoxStatImpl *		stat_i,

	const SK_BoxDynaReqs *		dyna_r,

	SK_BoxDynaImpl *		dyna_i
);




SlyDr SK_BoxAllReqs_prep
(
	SK_BoxAllReqs *			arq,

	const SK_BoxStatReqs *		stat_r,

	const SK_BoxDynaReqs *		dyna_r
);




int SK_BoxStatImpl_sel_prim_flag
(
	size_t		elem_size
);




SK_BoxIntf_prim_copy_func SK_BoxStatImpl_sel_prim_copy
(
	size_t		elem_size
);




SK_BoxIntf_prim_swap_func SK_BoxStatImpl_sel_prim_swap
(
	size_t		elem_size
);




SlySr SK_BoxStatReqs_print
(
	const SK_BoxStatReqs *		stat_r,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
);




SlySr SK_BoxStatImpl_print
(
	const SK_BoxStatImpl *		stat_i,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
);




SlySr SK_BoxDynaReqs_print
(
	const SK_BoxDynaReqs *		dyna_r,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
);




SlySr SK_BoxDynaImpl_print
(
	const SK_BoxDynaImpl *		dyna_i,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
);




SlySr SK_BoxReqs_print
(
	const SK_BoxReqs *		req,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
);




SlyDr SK_BoxStatReqs_shlw_eq
(
	const SK_BoxStatReqs *	sr0,
	const SK_BoxStatReqs *	sr1,
	int *			eq
);




SlyDr SK_BoxStatImpl_shlw_eq
(
	const SK_BoxStatImpl *	si0,
	const SK_BoxStatImpl *	si1,
	int *			eq
);




SlyDr SK_BoxDynaReqs_shlw_eq
(
	const SK_BoxDynaReqs *	dr0,
	const SK_BoxDynaReqs *	dr1,
	int *			eq
);




SlyDr SK_BoxDynaImpl_shlw_eq
(
	const SK_BoxDynaImpl *	di0,
	const SK_BoxDynaImpl *	di1,
	int *			eq
);




SlyDr SK_BoxAllReqs_shlw_eq
(
	const SK_BoxAllReqs *	ar0,
	const SK_BoxAllReqs *	ar1,
	int *			eq
);




SlyDr SK_BoxReqs_shlw_eq
(
	const SK_BoxReqs *	r0,
	const SK_BoxReqs *	r1,
	int *			eq
);




SlySr SK_Box_assign
(
	const SK_BoxIntf *		dst_intf,

	const SK_BoxReqs *		dst_req,

	void *				dst_obj,

	const SK_BoxIntf *		src_intf,

	const SK_BoxReqs *		src_req,

	void *				src_obj
);




SlyDr SK_Box_elem_rw_set_zero
(
	const SK_BoxReqs * restrict	req,

	void * restrict			elem_ptr,

	size_t				index,

	void * restrict			arb_arg
);




SlyDr SK_Box_elem_rw_set_w_arb_arg
(
	const SK_BoxReqs * restrict	req,

	void * restrict			elem_ptr,

	size_t				index,

	void * restrict			arb_arg
);




SlyDr SK_Box_elem_rw_set_ind
(
	const SK_BoxReqs * restrict	req,

	void * restrict			elem_ptr,

	size_t				index,

	void * restrict			arb_arg
);




SlyDr SK_Box_elem_rw_set_f64
(
	const SK_BoxReqs * restrict	req,

	void * restrict			elem_ptr,

	size_t				index,

	void * restrict			arb_arg
);




SlyDr SK_Box_calc_new_num_blocks
(
	const SK_BoxReqs *	req,

	size_t			cur_num_blocks,
	size_t			min_num_blocks,

	size_t *		new_num_blocks
);




SlyDr SK_Box_le_cmp
(
	const SK_BoxIntf *	intf_0,
	const SK_BoxReqs *	req_0,
	SK_Box *		box_0,

	const SK_BoxIntf *	intf_1,
	const SK_BoxReqs *	req_1,
	SK_Box *		box_1,

	int *			res
);




SlyDr SK_Box_bare_le_cmp
(
	const SK_BoxIntf *	intf,
	const SK_BoxReqs *	req,
	SK_Box *		box,

	const void *		bytes,
	size_t			bytes_len,

	int *			res
);




SlySr SK_Box_bytes_print
(
	const SK_BoxIntf *	sbi,

	const SK_BoxReqs *	req,

	SK_Box *		box,

	FILE *			file,

	SK_LineDeco		deco,

	int			ind_flag,

	size_t			beg_ind,

	size_t			end_ind
);




SlySr SK_Box_le_cmp_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco
);




SlySr SK_Box_le_cmp_sub_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	size_t			max_target_bytes,

	void *			bytes_0,
	size_t			bytes_0_len,

	void *			bytes_1,
	size_t			bytes_1_len,

	const SK_BoxIntf *	intf_0,
	const SK_BoxReqs *	req_0,
	SK_Box *		box_0,

	const SK_BoxIntf *	intf_1,
	const SK_BoxReqs *	req_1,
	SK_Box *		box_1
);




SlyDr SK_Box_calc_new_num_blocks_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco
);




SlySr SK_BoxIntf_init_deinit_single_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi,

	const SK_BoxReqs *	req,

	size_t			rng_num_elems
);




SlySr SK_BoxIntf_init_deinit_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
);




SlySr SK_BoxIntf_ins_rem_single_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi,
	const SK_BoxReqs *	req,
	SK_Box *		box,

	i64			num_resizes,

	size_t			min_num_elems,
	size_t			max_num_elems
);




SlySr SK_BoxIntf_ins_rem_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
);




SlySr SK_BoxIntf_set_get_single_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi,
	SK_BoxReqs *		req,
	void *			inst,

	size_t			beg_ind,
	size_t			end_ind
);




SlySr SK_BoxIntf_set_get_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
);




SlySr SK_BoxIntf_array_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
);




SlySr SK_BoxIntf_mt_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
);




SlySr SK_BoxIntf_speed_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
);




/*!
 *  # FOOT-NOTES
 *
 *  - Directly Movable Data
 *
 *  	The term "directly movable data (DMD)" refers to a piece of data that
 *  	can be moved to another memory address WITHOUT requiring other
 *  	modifications to avoid corruption of itself or any other piece of data
 *  	in memory.
 *
 *  	For example, consider a bare #int. It can be moved or copied anywhere
 *  	and still represent the same value. It does not become corrupted simply
 *	because the memory address associated with it has changed. Therefore,
 *	an #int IS a DMD.
 *
 *	Now consider a data structure that contains pointers to itself. If such
 *	an object is moved in memory, the object becomes corrupted, because
 *	those pointers are now invalid. To properly move it in memory requires
 *	modifying its contents to avoid corruption. Therefore, this data
 *	structure is NOT a DMD.
 *
 *	(Note, however, a data structure could have pointers to OTHER objects
 *	and still be classified as DMD).
 *
 *	In essence, directly moveable data (DMD) is like a less-strict form of
 *	plain-old data (POD) as a classification.
 *
 *	POD's in general do not have required functions to use for
 *	initialization, deinitialization, copying, or movement.
 *
 *	DMD's CAN have functions required for initialization, deinitialization,
 *	and / or copying, but NOT for moving.
 *
 *	DMD's can always be moved by simply assigning one instance to another
 *	instance. (It is good form to mark the original instance as invalid in
 *	some way so that a deinitialization function is not accidentally called
 *	on it, however).
 *
 *
 *	DMD classification can be restated as 2 simpler requirements:
 *
 *	1)
 *
 * 		The DMD object does not contain pointers to itself.
 *
 * 		(It may contain offsets relative to the start / end of itself
 * 		instead.)
 *
 * 	2)
 *
 *		Pointers to DMD objects are guaranteed to only exist in-between
 *		move operations.
 *
 *
 *
 *
 *  - Consequences of #elem_size
 *
 *	Allowing #elem_size to be set at run-time has a huge number of
 *	consequences for what can actually be optimized inside of #SK_BoxIntf
 *	implementations when they are compiled. This leads to some unexpected
 *	nuances in #SK_BoxStatReqs' and #SK_BoxIntf's design.
 *
 *	Normally, because #elem_size can be any value, that means loops that
 *	copy / move elements in #SK_BoxIntf implementations must iterate
 *	byte-by-byte. Unless the compiler views #elem_size as a constant, the
 *	compiler can neither compile these byte-by-byte loops with instructions
 *	that operate on larger word-sizes, nor can it unroll the loops. While
 *	this may seem like a small detail, that can cause the execution time of
 *	element copying / moving operations to become several times slower, as
 *	in ~8x slower or more in some cases.
 *
 *	The exception here is if the program sets #elem_size to a constant and
 *	does not modify it later. Then, a compiler MIGHT be able to treat it as
 *	a regular constant via constant propagation during optimization.
 *
 *	While the above is technically possible, it is not guaranteed to be
 *	optimized, and it is also easy to defeat in the target program. I.e., a
 *	very innocent change in the target program may make it so that the
 *	compiler can not recognize #elem_size as constant even when it is. (For
 *	example, if a pointer to #elem_size is sent to a function which could
 *	conceivably write to it but never actually does.)
 *
 *	This is why #prim_flag, #prim_copy, and #prim_swap are provided. The
 *	idea is that there will be a set of "primitive" byte-copying /
 *	byte-swapping functions that are hard-coded to a particular word size,
 *	thereby allowing them to be compiled with optimal word-sizes and
 *	loop-unrolling. Using that fact, #prim_copy and #prim_swap will point
 *	at the correct byte-copying / byte-swapping function for elements with
 *	#elem_size number of bytes. Then, whenever an #SK_BoxIntf
 *	implementation needs to copy / move an element, it can use the #prim_*
 *	fields while still benefitting from optimial word sizes and loop
 *	unrolling.
 *
 *	Notably, by using the #prim_* fields in this fashion, it no longer
 *	matters whether the compiler considers #elem_size to be a constant or
 *	not; the word-size / loop-unrolling benefits are there regardless. In
 *	this specific aspect, this technique beats C++ style templates, because
 *	#elem_size does not need to be a compile-time constant in any sense.
 *	However, this has the disadvantage that #prim_copy / #prim_swap are
 *	function addresses that need to be loaded at run-time, which C++
 *	templates would not require.
 *
 *	Now, the most pressing problem has moved from "How to get #elem_size
 *	seen as constant?" to "How to remove the branch and instruction-loading
 *	operations associated with #prim_flag, #prim_copy, and #prim_swap?".
 *
 *	Very fortunately, #SK_BoxStatReqs is required to stay constant for the
 *	life-time of the associated storage object according to #SK_BoxIntf. So
 *	if the running platform's hardware performs even basic
 *	branch-prediction and instruction-caching (which, as of 2020, is all
 *	but the least-performant platforms), the branch for #prim_flag can be
 *	easily predicted, and the instructions for #prim_copy and #prim_swap
 *	should exhibit good cache-locality.
 *
 *	Still, this is not as efficient as somehow avoiding the branch and the
 *	instruction-loading altogether, but it is theoretically MUCH more
 *	preferable to losing optimal word-sizes / loop-unrolling.
 *
 *	As of right now, the only avenue I can see for optimizing this further
 *	is by utilizing just-in-time compilation, either inside specific
 *	functions in the #SK_BoxIntf implementation, or to produce the entire
 *	#SK_BoxIntf implementation itself.
 *
 *	For this reason, development on #SK_BoxJit is planned for the future.
 *
 *
 *
 *
 *  - GR 1 Addendum
 *
 *	...
 *
 *	It is important to recognize that even though the storage object is
 *	guaranteed to be contiguously-indexed, it may very well have a
 *	non-contiguous memory layout. So while the storage object can be
 *	indexed like an array, it can not be addressed like an array.
 *
 *
 *
 *
 *  - GR 3 Addendum
 *
 *  	...
 *
 *	In many storage data structures, a pointer to the start of the storage
 *	object along with an element index is not sufficient to "quickly"
 *	determine the position of an element in memory. Similarly, a pointer to
 *	the start of the storage object along with a pointer to an element is
 *	not sufficient to "quickly" determine the element index.
 *
 *	For example, if a storage object is a linked-list and an index was the
 *	only available information to access an element, then walking the list
 *	would be necessary to retrieve a pointer to that element, which can be
 *	hugely expensive. Normally, the cost of walking the list grows
 *	proportionally with the index, and each "step" typically causes a
 *	cache-miss.
 *
 *	This is the motivation for anchors in #SK_BoxIntf.
 *
 *	In the above linked-list example, assume that a (pointer, index) value
 *	pair were available instead. With this information available, walking
 *	the list would be unnecessary to access that element, whether it was
 *	by-index or by-pointer. Further, having that information would
 *	significantly reduce the cost of accessing elements in nearby indices
 *	as well, because the "steps" could start from the index in the anchor
 *	instead of the start of the list.
 *
 *
 *
 *
 *  - GR 4 Addendum
 *
 *	...
 *
 * 	The purpose of this concession is that it allows storage objects to
 * 	automatically optimize their memory layout whenever they are
 * 	allocating / deallocating memory.
 *
 * 	For example, if a storage object internally makes use of a tree of some
 * 	sort, it may decide to partially rebalance itself after every insertion
 * 	operation to optimize reads for the most frequently accessed elements.
 *
 *
 *
 *
 *  - GR 5 Addendum
 *
 *	...
 *
 *	This rule is intended to give storage objects substantial freedom to
 *	reorganize themselves, whilst not preventing the reporting of pointers
 *	to individual elements.
 *
 *	Perhaps this rule may seem inconvenient to users of #SK_BoxIntf, but it
 *	is a necessary caveat in providing element pointers while allowing
 *	storage objects to rearrange themselves.
 *
 *	The availability of element pointers is highly desirable, because a
 *	great deal of spurious element copies to get values in / out of a
 *	storage object are avoidable via the usage of pointers. Generally it
 *	allows individual elements to be used as input / output arguments in
 *	other functions quite easily.
 *
 *	Additionally, allowing storage objects to rearrange their memory is
 *	also highly desirable, because it allows storage objects to optimize
 *	themselves for access patterns that can only be determined at run-time.
 *
 *	In order to provide both features, #SK_BoxIntf must be clear / explicit
 *	regarding when storage objects are allowed to invalidate previously
 *	obtained pointers. Otherwise moving elements in memory would not be
 *	possible, or moving elements in memory would cause many flawed pointer
 *	dereferences.
 *
 *	The same arguments above apply for anchors as well.
 *
 *	Note that for purely debugging purposes, a "modification" number can
 *	be stored in the anchor representing some hash associated with the
 *	last time the storage object had an element insertion / removal, which
 *	can then be compared to a hash value maintained somewhere in the
 *	storage object.
 *
 *	This would essentially make anchor-invalidation automatic. However,
 *	this hash method is not a reliable solution to the problem on its own,
 *	because of hash value collisions. Although hash-collisions are absurdly
 *	unlikely in practice (assuming >= 64-bit hashes), a hash-collision
 *	causing invalid indexing would be similarly absurdly hard to diagnose.
 *	So relying on hashes for this would work well until it mysteriously
 *	breaks something important in a bad, unreproducible way.
 *
 *
 *
 *
 *  - GR 6 Addendum
 *
 *	There is an argument to be made for changing #SK_BoxIntf such that
 *	#SK_Box's can store non-DMD data, simply in the interest of making
 *	#SK_BoxIntf maximally generic.
 *
 *	In order to facilitate this, there would have to be a function pointer
 *	field in #SK_BoxStatReqs called #elem_move (). This function could be
 *	set to NULL for DMD data, and could be set to a function meant
 *	specifically to move the elements for non-DMD data. Then, whenever the
 *	#SK_Box needs to have elements moved, the #SK_BoxIntf implementation
 *	would call #elem_move () on each element 1-by-1. If #elem_move ()
 *	were set to a non-NULL value, that would mean that #prim_flag in
 *	#SK_BoxStatImpl would have to be set to 0, so that the #prim_* ()
 *	functions to do not get used when moving elements.
 *
 *	This theoretically works, but it raises the question of what should
 *	happen when get_elem () in #SK_BoxIntf is called?
 *
 *	For DMD data, there is no issue, both the element inside of the #SK_Box
 *	and the element that is copied to the output argument of get_elem ()
 *	are in a valid state. By virtue of being DMD, both of these elements
 *	are valid regardless of their position in memory.
 *
 *	For non-DMD data, there is a problem. If the element in the #SK_Box is
 *	moved to the output argument of #get_elem () via #elem_move (), then
 *	the element inside of the #SK_Box is now gone. Users are not required
 *	to "give back" elements after using #get_elem () (nor will such a
 *	requirement be added, as it makes using #get_elem () highly
 *	inconvenient).
 *
 *	(A similar situation exists for the input argument of #set_elem () and
 *	the element stored in the #SK_Box.)
 *
 *	The obvious solution to the above problem is to require #elem_move ()
 *	to leave behind the original copy of the element, so that it can be
 *	used as a pseudo-copy function. With that requirement added, now valid
 *	versions of the element exist within the #SK_Box and at the output
 *	argument of #get_elem (). Regardless, it does seem strange that the
 *	element in the #SK_Box and the element fetched by #get_elem () do not
 *	truly have the same contents, and this could be observed using
 *	#get_elem_ptr ().
 *
 *	The system stated above seems like it would work, but I feel there may
 *	be side-effects that I have not yet considered.
 *
 *	With this in mind, the DMD data requirement of #SK_BoxIntf will stay in
 *	place, because that is the easier decision to reverse if it ever needs
 *	to be.
 *
 *	If #SK_BoxIntf transitions from DMD-only -> non-DMD-allowed, the
 *	current users can use #SK_BoxIntf largely the same as before, but
 *	implementations will have to be altered to call #elem_move () whenever
 *	elements are moved.
 *
 *
 *
 *
 *  - GR 13 Addendum
 *
 *	...
 *
 *	Specific functions or implementations themselves may make guarantees
 *	about whether indices will be safety-checked or not, but by default, no
 *	such guarantee exists. Optionally, they may return an error-code
 *	indicating the index was invalid.
 *
 *	This requirement is necessary so that #SK_BoxIntf does not imply the
 *	cost of safety-checking every element access. That can be prohibitively
 *	slow and / or unnecessary in some applications.
 *
 *
 *
 *
 *  - Const-qualification of #cur_anc
 *
 *	Note that in get_anc_w_anc (), #cur_anc is a (const SK_BoxAnc *)
 *	instead of a (SK_BoxAnc *) argument. This convention is followed in
 *	many other *_w_anc () and *_by_anc () functions as well.
 *
 *	One motivation for the const-qualification is simply to make it clear
 *	that the #new_anc argument is the one that will be modified, which
 *	allows the caller to choose whether or not an anchor will be updated or
 *	not for a new index.
 *
 *	Another less-obvious motivation is that multiple threads can read an
 *	anchor at a time, but not write to the same anchor at a time. So to
 *	make thread-safe usage of the *_w_anc () / *_by_anc () functions
 *	possible, it needs to be made explicit which arguments will cause a
 *	thread to be read and which will cause it to be written.
 *
 *
 *
 *
 *
 *  - Motivation for #prev_anc / #next_anc
 *
 *	The reason that get_region_ptr_w_anc () uses the #prev_anc / #next_anc
 *	style arguments instead of the #new_anc_ind / #new_anc style arguments
 *	(like get_array_w_anc ()) is that the caller has no way of knowing the
 *	size of the region associated with #ind before calling this function.
 *
 *
 *
 *
 *  - Motivation for ins_range ()
 *
 *	Before the addition of ins_range () to #SK_BoxIntf, it was considered
 *	instead implementing this as a higher-level SK_Box_* () function.
 *
 *	Such a higher-level function would probably follow this process...
 *
 *		-Use resize () to add elements to the upper-indices of the
 *		#SK_Box
 *
 *		-Use get_region_ptr () to initialize the elements as quickly as
 *		possible afterwards
 *
 *		-Use swap_range () to place the elements into the correct
 *		position.
 *
 *		(Note that for some implementations (i.e. linked-lists) the
 *		swap can execute quickly as it could simply change the links
 *		between blocks)
 *
 *	However, upon further consideration, it appears that this could
 *	potentially be less efficient for some implementations compared to
 *	placing the newly allocated elements into the correct position
 *	immediately. If the position of the newly allocated elements are known
 *	up-front, then the implementation could opt to merge / split other
 *	element blocks at the target index of the insertion. This would avoid
 *	needless modification at the upper-indices of the #SK_Box.
 */




#endif //SK_BOX_INTF_H

