/*!****************************************************************************
 *
 * @file
 * SK_Vector.h
 *
 * See SK_Vector.c for details.
 *
 *****************************************************************************/




#ifndef SK_VECTOR_H
#define SK_VECTOR_H




#include "SK_Vector_opaque.h"




/*!
 *  A container for an array that will be used in #SK_Vector to store groups of
 *  elements.
 */

typedef struct SK_VectorBlock
{
	/// A pointer to an array of raw bytes

	u8 * bytes;
}

SK_VectorBlock;




/*!
 *  An array-like data structure meant for storing arbitrary data. It is slower
 *  to access an element in this than it is in an array, but its size can be
 *  incremented / decremented faster than an array's.
 *
 *  In reference to an #SK_Vector, these terms have the following meanings...
 *
 *	element index		-->	The index of an element relative to
 *					ALL the blocks contained in the
 *					#SK_Vector. For example, in an
 *					#SK_Vector where each block has 4
 *					elements, then the 3rd element in the
 *					2nd block would have element index 6.
 *					This value comes from ((4 + 3) - 1).
 *
 *
 *	block index		-->	The index of an #SK_VectorBlock
 *					contained in the #blocks memeber of
 *					this data structure. This can be found
 *					from the element index with
 *					(int (element index / elems per block)).
 *
 *
 *	block element index	-->	The index of an element contained in
 *					the #SK_Vector relative to the singular
 *					block that contains it. This can be
 *					found from the element index with
 *					((int (element index % elems per block)).
 */

typedef struct SK_Vector
{
	/// The size (in bytes) of each element stored in this vector

	size_t			elem_size;


	/// The length of the array in each #SK_VectorBlock at #blocks.
	///
	/// The higher this value is, the quicker elements are to iterate
	/// through (due to less cache misses), and the slower this #SK_Vector
	/// can be resized.

	size_t			elems_per_block;


	/// The number of #SK_VectorBlock's contained in #blocks

	size_t			num_blocks;


	/// An array of #SK_VectorBlock's that will be used to store individual
	/// elements.

	SK_VectorBlock *	blocks;


	/// The total number of elements currently stored in this #SK_Vector

	size_t			cur_num_elems;


	/// The maximum number of elements that could possibly be stored in
	/// this #SK_Vector with the number of blocks allocated at #blocks

	size_t			max_num_elems;


	/// This is the minimum number of blocks (minus 1) that must be unused
	/// before automatically "minifying" the #SK_Vector, which means
	/// deallocating all blocks that are not currently used to store
	/// elements.
	///
	/// So, if this value is (x), and there is (x + 1) empty blocks in the
	/// #SK_Vector, then the #SK_Vector is considered a candidate for
	/// minification.
	///
	/// This value will be referenced whenever #cur_num_elems is
	/// decremented to determine whether or not memory deallocations are
	/// necessary.
	///
	/// It is recommended that this value is greater than or equal to
	/// #pos_slack, so that brand new allocated blocks are not removed when
	/// #cur_num_elems is decremented.
	///
	/// If uncertain, leave this value as 0.

	size_t			neg_slack;


	/// The number of extra blocks to allocate whenever #max_num_elems
	/// needs to be increased. By allocating extra blocks before they are
	/// actually needed, the allocations of multiple blocks can be grouped
	/// together, and thus finish quicker. Note, #elems_per_block can be
	/// increased to get a similar effect, except that also has an effect
	/// on how much memory gets deallocated at a time.
	///
	/// It is recommended that this value is lesser than or equal to
	/// #neg_slack. Otherwise, the extra block allocations will immediately
	/// cause the #SK_Vector to get minified if #cur_num_elems is
	/// decremented shortly after. Constantly allocating / deallocating
	/// blocks will cause the #SK_Vector to perform quite badly.
	///
	/// If uncertain, leave this value as 0.

	size_t			pos_slack;

}

SK_Vector;




SlySr SK_Vector_init_unsafe
(
	SK_Vector *	vec,
	size_t		elem_size,
	size_t		num_elems,
	size_t		elems_per_block,
	size_t		neg_slack,
	size_t		pos_slack,
	SlyDr		(* init_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		init_func_arg
);




SlySr SK_Vector_init
(
	SK_Vector *	vec,
	size_t		elem_size,
	size_t		num_elems,
	size_t		elems_per_block,
	size_t		neg_slack,
	size_t		pos_slack,
	SlyDr		(* init_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		init_func_arg
);




SlyDr SK_Vector_deinit
(
	SK_Vector * vec
);




#endif //SK_VECTOR_H

