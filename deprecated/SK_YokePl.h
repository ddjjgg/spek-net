/*!****************************************************************************
 *
 * @file
 * SK_YokePl.h
 *
 * See SK_YokePl.c for details.
 *
 *****************************************************************************/




#ifndef SK_YOKEPL_H
#define SK_YOKEPL_H




#include "./SK_YokePl_opaque.h"




#include "../SK_Vector.h"
#include "../SK_Yoke.h"




/*!
 *  A version of #SK_Yoke capable of storing custom per-link properties.
 *
 *  The custom per-link properties allow for networks to account for states
 *  much more complex than simply "active / inactive" links. Things such as
 *  link speed, link quality, and redundant links can all be represented using
 *  #SK_YokePl. Theoretically, even things like transmission line effects can
 *  be simulated by using custom link properties.
 */

typedef struct SK_YokePl
{
	/// Used to handle the job of establishing networks.

	SK_Yoke		yoke;


	/// An #SK_Vector containing the actual link properties associated with
	/// the individual links in #yoke
	///
	/// FIXME : Is an #SK_Vector good here, or is an array of void
	/// pointer's better?

	SK_Vector *	lp;
}

SK_YokePl;




#endif //SK_YOKEPL_H

