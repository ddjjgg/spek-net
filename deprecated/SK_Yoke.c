/*!****************************************************************************
 *
 * @file
 * SK_Yoke.c
 *
 * Contains an implementation of #SK_Yoke, which is a data structure that can
 * be used to represent nodes in a network. What exactly the network represents
 * is up to the user of #SK_Yoke. An array of #SK_Yoke's could for example
 * represent a network between objects in a different array, and links in that
 * network could represent access permissions or some other property.
 *
 *****************************************************************************/




//FIXME : Make use of #SK_BoxIntf in the implementation of #SK_Yoke. In
//particular, it should be used to provide the storage space where
//SK_YokeLink's will be stored. That way, different storage data structures can
//be used when appropriate for different numbers of links and different
//reallocation frequencies.




//FIXME : This is a pretty major rewrite to #SK_Yoke, but consider making the
//SK_YokeLinks store an INDEX (into a storage object that implements
//#SK_BoxIntf) instead of a pointer to the target #SK_Yoke.
//
//What is the utility of this? Right now, #SK_Yoke's store memory addresses to
//other #SK_Yoke's as part of recording the existence of a link. The problem
//here is that if an #SK_Yoke is moved in memory, all REFERENCES TO IT MUST BE
//UPDATED TOO. Otherwise, stale links will be left behind.
//
//This makes it very difficult to place #SK_Yoke's as elements into a storage
//object that may be resized (thus moving the #SK_Yoke elements around in
//memory). Of course, opaque pointers to the #SK_Yoke's can be stored in the
//storage object instead, which solves the problem in an important sense, but
//now accessing each #SK_Yoke incurs a memory dereference.
//
//So, if #SK_Yoke links use storage object indices instead of pointers, this
//suggests SK_Yoke_link () and SK_Yoke_unlink () will require knowing which
//array all the #SK_Yoke's are stored in, so that "both ends" of links can be
//accessed and updated appropriately. Furthermore, this suggests that every
//#SK_Yoke that can be made part of the network MUST exist in the same storage
//object for the indices to be valid. Before #SK_BoxIntf was implemented,
//this would have been very worrying, because it would have very tightly
//coupled #SK_Yoke to the storage data structure containing the #SK_Yoke's. But
//instead, I can now just pass the appropriate #SK_BoxIntf to SK_Yoke_link
//() / SK_Yoke_unlink () along with a pointer to the storage object.
//
//A huge benefit of doing this is that entire networks of #SK_Yoke's can copied
//_much_ easier. Right now, to copy a network of #SK_Yoke's, it requires
//mapping all of the #SK_Yoke addresses in the source network 1-to-1 to new
//#SK_Yoke addresses in the destination network, and then recreating the links
//per #SK_Yoke.
//
//But if the #SK_Yoke's store indices instead, no mapping is necessary. The
//indices can remain exactly as they are in the source network for the new
//#SK_Yoke's in the destination network.





//FIXME : Instead of having #SK_Hoke, #SK_YokePl, #SK_HokePl, etc. as variants
//of #SK_Yoke, follow the pattern that #SK_BoxReqs / #SK_BoxIntf use.
//For example, whether or not an #SK_Yoke contains per-link properties can be
//recorded in a "SK_YokeReqs.stat.use_per_link_prop" field. Then, a pointer to
//an #SK_YokeReqs can be passed to each #SK_Yoke function.
//
//Of course, #SK_Yoke and #SK_Hoke are sized differently, but that's exactly
//why a "SK_YokeIntf" should contain an inst_stat_mem () function that accepts
//an #SK_YokeReqs as an argument, just like SK_BoxIntf.inst_stat_mem ().




#include "./SK_Yoke.h"




#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "./SK_debug.h"
#include "./SK_misc.h"
#include "./SK_numeric_types.h"
#include "./SK_Vector.h"
#include "./SK_YokeIntf.h"





//FIXME : Completely re-do this interface, need to take into account FIXME's at
//the top of this file


/*!
 *  This provides the mappings needed to use the #SK_Yoke through an
 *  #SK_YokeIntf.
 */

const SK_YokeIntf SK_Yoke_as_self =

{
	.init =	

	(SlySr (*) (void *, size_t)) SK_Yoke_init,


	.deinit =	

	(SlyDr (*) (void *)) SK_Yoke_deinit,


	.get_sizeof =

	(size_t (*) ()) SK_Yoke_get_sizeof,


	.print =

	(SlySr (*) (const void *, FILE *, SK_LineDeco)) SK_Yoke_print,


	.unlink =

	(SlyDr (*) (void *, void *)) SK_Yoke_unlink,


	.unlink_all =

	(SlyDr (*) (void *)) SK_Yoke_unlink_all,


	.link =

	(SlySr (*) (void *, size_t, void *, size_t)) SK_Yoke_link,


	.link_no_alloc =

	(SlySr (*) (void *, void *)) SK_Yoke_link_no_alloc,


	.link_exists =

	(SlyDr (*) (const void *, const void *, int *)) SK_Yoke_link_exists,


	.get_num_links =

	(SlyDr (*) (const void *, size_t *, size_t *)) SK_Yoke_get_num_links,


	.links_realloc =

	(SlySr (*) (void *, size_t, int)) SK_Yoke_links_realloc,


	.links_minify =

	(SlySr (*) (void *)) SK_Yoke_links_minify

};




/*!
 *  Prints out the contents of an #SK_YokeLink
 *
 *  @param skl			The #SK_YokeLink to print out
 *
 *  @param f_ptr		The #FILE to print to, like #stdout
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_YokeLink_print_opt
(
	SK_YokeLink	skl,
	FILE *		f_ptr,
	SK_LineDeco	deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	return

	SK_LineDeco_print_opt
	(
		f_ptr,
		deco,
		"(dst_yoke = %p,"
		" back_ind = %zu)",
		skl.dst_yoke,
		skl.back_ind
	);
}




/*!
 *  Prints out the contents of an #SK_Yoke. This is a version of
 *  SK_Yoke_print () with more options.
 *
 *  @param sky			The #SK_Yoke to print out
 *
 *  @param f_ptr		The #FILE to print to, like #stdout
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @param header_flag		Flag indicating that the information header
 *				about the #SK_Yoke should be printed out, which
 *				just contains the raw member values
 *
 *  @param active_links_flag	Flag indicating whether or not to print out the
 *				active #SK_YokeLink's contained within #sky
 *
 *  @param inactive_links_flag	Flag indicating whether or not to print out the
 *				inactive #SK_YokeLink's contained within #sky
 *
 *  @return			Standard status code
 */

SlySr SK_Yoke_print_opt
(
	const SK_Yoke *		sky,
	FILE *			f_ptr,
	SK_LineDeco		deco,
	int			header_flag,
	int			active_links_flag,
	int			inactive_links_flag
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//#SlySr values for the individual SK_LineDeco_print_opt () calls

	const size_t NUM_RES = 12;

	SlySr sr [NUM_RES];


	for (size_t sr_num = 0; sr_num < NUM_RES; sr_num += 1)
	{
		sr [sr_num] = SlySr_get (SLY_SUCCESS);
	}


	//Print out the header information for the #SK_Yoke

	if (header_flag)
	{

		sr [0] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"SK_Yoke   @ %p",
			sky
		);


		sr [1] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"links     = %p",
			sky->links
		);


		sr [2] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"links_len = %zu",
			sky->links_len
		);


		sr [3] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"links_cnt = %zu",
			sky->links_cnt
		);


		sr [4] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			""
		);
	}


	//Create a version of #deco with additional indentation

	SK_LineDeco id_deco = deco;

	id_deco.pre_str_cnt += 1;


	//Split #id_deco into its beginning and ending portions

	SK_LineDeco id_deco_beg = id_deco;

	id_deco_beg.post_str_cnt = 0;


	SK_LineDeco id_deco_end = id_deco;

	id_deco_end.pre_str_cnt = 0;


	//Print out the links of the #SK_Yoke, while respecting
	//#active_links_flag and #inactive_links_flag

	if (active_links_flag || inactive_links_flag)
	{
		sr [5] =

		SK_LineDeco_print_opt (f_ptr, deco, "Links");


		sr [6] =

		SK_LineDeco_print_opt (f_ptr, deco, "{");


		int link_printed = 0;

		for (size_t l_sel = 0; l_sel < sky->links_len; l_sel += 1)
		{
			SK_YokeLink cur_link =	sky->links [l_sel];

			int cur_active =	SK_YokeLink_is_active (cur_link);

			if
			(
				(cur_active && active_links_flag)

				||

				((! cur_active) && inactive_links_flag)
			)
			{

				SlySr sr_0 =

				SK_LineDeco_print_opt
				(
					f_ptr,
					id_deco_beg,
					"%03lu : ",
					l_sel
				);



				SlySr sr_1 =

				SK_YokeLink_print_opt
				(
					cur_link,
					f_ptr,
					id_deco_end
				);


				//If any of the printing attempts in this loop
				//fail, remember that for the rest of the loop

				if (SLY_SUCCESS != sr_0.res)
				{
					sr [7] = sr_0;
				}


				if (SLY_SUCCESS != sr_1.res)
				{
					sr [8] = sr_1;
				}


				//Indicate that the set of links is not empty

				link_printed = 1;
			}
		}


		//Handle the case where the set of links printed is empty

		if (! link_printed)
		{
			sr [9] =

			SK_LineDeco_print_opt (f_ptr, id_deco, "(No Links)");
		}


		sr [10] =

		SK_LineDeco_print_opt (f_ptr, deco, "}");


		sr [11] =

		SK_LineDeco_print_opt (f_ptr, deco, "");
	}


	//Check if the printing was malformed, and do not return success if
	//that is the case

	for (size_t sr_num = 0; sr_num < NUM_RES; sr_num += 1)
	{
		if (SLY_SUCCESS != sr [sr_num].res)
		{
			return sr [sr_num];
		}
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Prints out the contents of an #SK_Yoke
 *
 *  @param sky			The #SK_Yoke to print out
 *
 *  @param f_ptr		The #FILE to print to, like #stdout
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_Yoke_print
(
	const SK_Yoke *		sky,
	FILE *			file,
	SK_LineDeco		deco
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, file, SlySr_get (SLY_BAD_ARG));

	return SK_Yoke_print_opt
	(
		sky,
		file,
		deco,
		1,
		1,
		0
	);
}




/*!
 *  Produces an #SK_YokeLink in the default state, which is to say it has no
 *  connections assigned and upload/download are both enabled.
 *
 *  @return			An #SK_YokeLink in the default state
 */

SK_YokeLink SK_YokeLink_default ()
{
	SK_YokeLink skl;

	skl.dst_yoke =		NULL;
	skl.back_ind =		0;

	return skl;
}




/*!
 *  Determines if a particular #SK_YokeLink is ACTIVE, meaning that a valid
 *  #SK_Yoke IS assigned for #dst_yoke.
 *
 *  @param skl			The #SK_YokeLink to examine
 *
 *  @return			1 if the link is active, 0 otherwise
 */

int SK_YokeLink_is_active
(
	SK_YokeLink		skl
)
{
	return (NULL != skl.dst_yoke);
}




/*!
 *  Determines if a particular #SK_YokeLink is INACTIVE, meaning that a valid
 *  #SK_Yoke is NOT assigned for #dst_yoke.
 *
 *  @param skl			The #SK_YokeLink to examine
 *
 *  @return			1 if the link is inactive, 0 otherwise
 */

int SK_YokeLink_is_inactive
(
	SK_YokeLink		skl
)
{
	return (NULL == skl.dst_yoke);
}




/*!
 *  Returns the result of sizeof () on #SK_Yoke. This is a required function by
 *  #SK_YokeIntf.
 *
 *  @return			The result of calling sizeof () on #SK_Yoke.
 */

size_t SK_Yoke_get_sizeof ()
{
	return sizeof (SK_Yoke);
}




/*!
 *  Creates an #SK_Yoke, which initially has no active connections.
 *
 *  @warning
 *  Some time after calling this function, SK_Yoke_destroy () must be called on
 *  the returned pointer, otherwise a memory leak will occur.
 *
 *  @param links_len		The number of #SK_YokeLink's that should be
 *				allocated for connections initially. This
 *				argument CAN be 0, but in that case, the number
 *				of links will have to be resized to assign any
 *				connections.
 *
 *  @param sr_ptr		Where to place a #SlySr value indicating the
 *				success of this function. This CAN be NULL if
 *				this information is not desired.
 *
 *  @return			A pointer to the newly created #SK_Yoke
 *				structure upon success; NULL is returned
 *				otherwise.
 */

SK_Yoke * SK_Yoke_create
(
	size_t		links_len,
	SlySr *		sr_ptr
)
{

	//Allocate space for the yoke itself

	SK_Yoke * sky_ptr = malloc (sizeof (SK_Yoke));

	if (NULL == sky_ptr)
	{
		if (NULL != sr_ptr)
		{
			*sr_ptr = SlySr_get (SLY_BAD_ALLOC);
		}


		SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_ptr, NULL);
	}


	//Use SK_Yoke_init () to initialize the #SK_Yoke

	SlySr sr = SK_Yoke_init (sky_ptr, links_len);


	if (SLY_SUCCESS != sr.res)
	{
		free (sky_ptr);


		if (NULL != sr_ptr)
		{
			*sr_ptr = sr;
		}


		SK_RES_RETURN (SK_YOKE_L_DEBUG, sr.res, NULL);
	}


	//Report success and return the newly created #SK_Yoke

	if (NULL != sr_ptr)
	{
		*sr_ptr = SlySr_get (SLY_SUCCESS);
	}


	return sky_ptr;
}




/*!
 *  Initializes an #SK_Yoke which has had memory allocated for it but has not
 *  had any of its fields initialized.
 *
 *  This logic is not directly inside of SK_Yoke_create () so that it can be
 *  re-used inside of SK_Hoke_create ().
 *
 *  @param sky			The #SK_Yoke to initialize
 *
 *  @param links_len		See SK_Yoke_create ()
 *
 *  @return			Standard status code
 */

SlySr SK_Yoke_init
(
	SK_Yoke *	sky,
	size_t		links_len
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlySr_get (SLY_BAD_ARG));


	//Save the number of links to allocate in #sky, and perform that
	//allocation

	sky->links_len = links_len;

	if (0 < links_len)
	{
		SK_YokeLink * links = malloc (sizeof (SK_YokeLink) * links_len);

		sky->links = links;


		//Check for errors after allocation

		if (NULL == links)
		{
			SK_NULL_RETURN
			(
				SK_YOKE_L_DEBUG,
				links,
				SlySr_get (SLY_BAD_ALLOC)
			);
		}
	}

	else
	{
		sky->links = NULL;
	}


	//Ensure that every #SK_YokeLink is initialized to a default state

	for (size_t l_sel = 0; l_sel < sky->links_len; l_sel += 1)
	{
		sky->links [l_sel] = SK_YokeLink_default ();
	}


	//Initialize #links_cnt to indicate that no links are currently used

	sky->links_cnt = 0;


	return SlySr_get (SLY_SUCCESS);
}



/*!
 *  Deinitializes an #SK_Yoke object, meaning that all tasks needed to destroy
 *  the #SK_Yoke are performed EXCEPT for deallocating the #SK_Yoke itself.
 *
 *  This function WILL automatically unlink #sky from any other #SK_Yoke's that
 *  it is connected to.
 *
 *  This logic is separated from SK_Yoke_destroy () so that it can be re-used
 *  inside of SK_Hoke_deinit ().
 *
 *  @param sky			The #SK_Yoke to de-initialize
 *
 *  @return			Standard status code
 */

SlyDr SK_Yoke_deinit (SK_Yoke * sky)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlyDr_get (SLY_BAD_ARG));


	//BEFORE DEALLOCATING ANY RESOURCES, make sure to unlink this #SK_Yoke
	//completely so that no stale references to it will exist amongst its
	//connections!

	SK_Yoke_unlink_all (sky);


	//Free the #links array, if allocated

	if (NULL != sky->links)
	{
		free (sky->links);
	}


	//Return success WITHOUT deallocating #sky itself

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Destroys an #SK_Yoke data structure.
 *
 *  Note, this automatically takes care of unlinking #sky from any other
 *  #SK_Yoke's it is connected to. So this IS SAFE to call on #SK_Yoke's that
 *  still have active connections.
 *
 *  @param sky			The #SK_Yoke data structure to destroy
 *
 *  @return			Standard status code
 */

SlyDr SK_Yoke_destroy (SK_Yoke * sky)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlyDr_get (SLY_BAD_ARG));


	//Perform all tasks needed on #sky before destroying it, which includes
	//unlinking from all other #SK_Yoke's it is connected to.

	SK_Yoke_deinit (sky);


	//Free the #SK_Yoke data structure itself

	free (sky);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Removes the link(s) in one #SK_Yoke to another target #SK_Yoke, but WITHOUT
 *  updating the target #SK_Yoke whatsoever. If no link to #sky_1 exists inside
 *  of #sky_0, then this function has no effect on the object.
 *
 *  @warning
 *  This function leaves the "other side" of the #SK_YokeLink with stale values,
 *  and that is intentional. As a result, using this function without taking
 *  this into account can cause a set of #SK_Yoke's to enter an inconsistent
 *  state and cause memory access errors. Unless there is a good reason for
 *  doing so, you should probably use SK_Yoke_unlink_w_info () instead.
 *
 *  @warning
 *  This function only removes the first detected link in #sky_0 to #sky_1. If
 *  there are somehow duplicate links to #sky_1, only one of them will be
 *  removed. Duplicate links should be regarded as a bug.
 *
 *  @param sky_0		The #SK_Yoke that WILL be modified. If this has
 *				a link to #sky_1, it will be removed.
 *
 *  @param sky_1		The #SK_Yoke that will NOT be modified. If this
 *				has a link to #sky_0, it will NOT be removed or
 *				altered in any way.
 *
 *  @param link_existed		Where to save a flag indicating that the link
 *				from #sky_0 to #sky_1 existed. This CAN be NULL
 *				if this information is not desired.
 *
 *  @param sky_0_l_ind		Where to place the index of the disabled link
 *  				of sky_0->links []. This only has meaning if
 *  				#link_existed is 1. This argument CAN be NULL
 *  				if this information is not desired.
 *
 *  @param sky_1_l_ind		Where to save the value representing the index
 *				in sky_1->links [] that links back to #sky_0.
 *				This value can later be used in
 *				SK_Yoke_unlink_single_by_ind () to quickly
 *				unlink #sky_1 from #sky_0. This value ONLY has
 *				meaning if #link_existed is 1. This CAN be NULL
 *				if this information is not desired.
 *
 *  @return			Standard status code. #SLY_SUCCESS will be
 *				returned even if a link to #sky_1 is not found
 *				in #sky_0. The only reason failure would be
 *				returned is if the #SK_Yoke's are malformed in
 *				some way.
 */

//FIXME: The way this function works requires iterating through all the
//connections in #sky_0. While this is not a concern for most realistic yokes,
//this could be a source of slow-down if a yoke with 1000's of connections is
//ever simulated.

SlyDr SK_Yoke_unlink_single
(
	SK_Yoke *	sky_0,
	SK_Yoke *	sky_1,
	int *		link_existed,
	size_t *	sky_0_l_ind,
	size_t *	sky_1_l_ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_1, SlyDr_get (SLY_BAD_ARG));


	//These indicate whether or not the link existed, and where it was in
	//#sky_0 and #sky_1

	int l_exists =		0;

	size_t l_ind_0 =	0;

	size_t l_ind_1 =	0;


	//Attempt to disable the link to #sky_1 inside of #sky_0 (if it exists)

	for (size_t l_sel = 0; l_sel < sky_0->links_len; l_sel += 1)
	{

		//Detect the link to #sky_1

		if (sky_1 == (sky_0->links [l_sel].dst_yoke))
		{
			//Record the fact that the link existed, and where it
			//existed in #sky_0 and #sky_1.
			//
			//Take note how #l_ind_1 is actually fetched
			//from the link inside of #sky_0 that is about to be
			//disabled.

			l_exists =	1;

			l_ind_0 =	l_sel;

			l_ind_1 =	sky_0->links [l_sel].back_ind;


			//Remove the link to #sky_1

			sky_0->links [l_sel] = SK_YokeLink_default ();


			//Update the link counter

			if (0 < sky_0->links_cnt)
			{
				sky_0->links_cnt -= 1;
			}

			else
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"#sky_0->links_cnt was not properly "
					"incremented at an earlier time. "
					"sky_0 = %p",
					sky_0
				);
			}


			//Stop the search, because the link has been found and
			//broken

			break;
		}
	}


	//Record the information requested by the above arguments

	if (NULL != link_existed)
	{
		*link_existed = l_exists;
	}

	if (NULL != sky_0_l_ind)
	{
		*sky_0_l_ind = l_ind_0;
	}

	if (NULL != sky_1_l_ind)
	{
		*sky_1_l_ind = l_ind_1;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Removes the link in one #SK_Yoke to another target #SK_Yoke, but WITHOUT
 *  updating the target #SK_Yoke whatsoever. This function is very similar to
 *  #SK_Yoke_unlink_single (), except that it finds the yoke to unlink by an
 *  explicit index.
 *
 *  @warning
 *  This function leaves the "other side" of the #SK_YokeLink with stale values,
 *  and that is intentional. As a result, using this function without taking
 *  this into account can cause a set of #SK_Yoke's to enter an inconsistent
 *  state and cause memory access errors. Unless there is a good reason for
 *  doing so, you should probably use SK_Yoke_unlink_w_info () instead.
 *
 *  @param sky			The #SK_Yoke that WILL be modified.
 *
 *  @param ind			The index of the link to deactivate. To be
 *  				explicit, the link at sky->links [#ind] will be
 *  				deactivated.
 *
 *  @return			Standard status code. #SLY_SUCCESS will be
 *				returned even if a link to #sky_1 is not found
 *				in #sky. The only reason failure would be
 *				returned is if the #SK_Yoke's are malformed in
 *				some way.
 */

//FIXME : Should this take an argument for the #other_sky, so that it can be
//detected easily if #ind is incorrect?

SlyDr SK_Yoke_unlink_single_by_ind
(
	SK_Yoke *	sky,
	size_t		ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlyDr_get (SLY_BAD_ARG));

	if (ind >= sky->links_len)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"That value of #ind = %lu was outside the range "
			"[0, #sky->links_len - 1], where sky->links_len = %lu\n",
			(long unsigned) ind,
			(long unsigned) sky->links_len
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Disable the link at (sky->links [ind])

	if (SK_YokeLink_is_active (sky->links [ind]))
	{
		sky->links [ind] = SK_YokeLink_default ();

		if (0 < sky->links_cnt)
		{
			sky->links_cnt -= 1;
		}

		else
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#sky->links_cnt was not properly "
				"incremented at an earlier time. "
				"sky = %p",
				sky
			);
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Unlinks 2 connected #SK_Yoke data structures, provided that they are
 *  already linked together. Otherwise, this function does not alter the input
 *  #SK_Yoke's.
 *
 *  The suffix "_w_info" is because the #link_existed, #sky_0_l_ind, and
 *  #sky_1_l_ind arguments unlike SK_Yoke_unlink ().
 *
 *  @param sky_0		The 1st #SK_Yoke, which may or may not be
 *				linked to #sky_1
 *
 *  @param sky_1		The 2nd #SK_Yoke, which may or may not be
 *				linked to #sky_0
 *
 *  @param link_existed		Indicates whether or not the link to #sky_1
 *				actually existed or not.
 *
 *  @param sky_0_l_ind		Where to place the index of the disabled link
 *				of #sky_0->links []. This only has meaning if
 *				#link_existed is 1.
 *
 *  @param sky_1_l_ind		Where to place the index of the disabled link
 *				of #sky_1->links []. This only has meaning if
 *				#link_existed is 1.
 *
 *  @return			Standard status code. #SLY_SUCCESS will be
 *				returned even if a link to #sky_1 is not found
 *				in #sky_0. The only reason failure would be
 *				returned is if the #SK_Yoke's are malformed in
 *				some way.
 */

SlyDr SK_Yoke_unlink_w_info
(
	SK_Yoke *	sky_0,
	SK_Yoke *	sky_1,
	int *		link_existed,
	size_t *	sky_0_l_ind,
	size_t *	sky_1_l_ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_1, SlyDr_get (SLY_BAD_ARG));


	//Attempt to disable any links to #sky_1 inside of #sky_0

	int l_exists =		0;

	size_t l_ind_0 =	0;

	size_t l_ind_1 =	0;


	SK_Yoke_unlink_single
	(
		sky_0,
		sky_1,
		&l_exists,
		&l_ind_0,
		&l_ind_1
	);


	//For the sake of debugging, check if #l_ind_1 actually provides the
	//link from #sky_1 to #sky_0

	//FIXME : Disable/remove this code when the linking/unlinking process
	//of #SK_Yoke's is known to be stable

	if (l_exists)
	{
		if (sky_0 != sky_1->links [l_ind_1].dst_yoke)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"BUG : #l_ind_1 = %lu was not correct!\n"
				"sky_1->links [%lu] = %p != (#sky_0 = %p)\n"
				"A stale link must have been created at an earlier time.",
				(long unsigned) l_ind_1,
				(long unsigned) l_ind_1,
				sky_1->links [l_ind_1],
				sky_0
			);

			return SlyDr_get (SLY_UNKNOWN_ERROR);
		}
	}


	//Attempt to disable the link to #sky_0 inside of #sky_1, if they were
	//in fact linked together

	if (l_exists)
	{
		SK_Yoke_unlink_single_by_ind
		(
			sky_1,
			l_ind_1
		);
	}


	//Record the information requested by the above arguments

	if (NULL != link_existed)
	{
		*link_existed = l_exists;
	}

	if (NULL != sky_0_l_ind)
	{
		*sky_0_l_ind = l_ind_0;
	}

	if (NULL != sky_1_l_ind)
	{
		*sky_1_l_ind = l_ind_1;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 * See SK_Yoke_unlink_w_info (). This function does the same thing, it just
 * reports less information about the unlinking process.
 *
 *  @param sky_0		The 1st #SK_Yoke, which may or may not be
 *				linked to #sky_1
 *
 *  @param sky_1		The 2nd #SK_Yoke, which may or may not be
 *				linked to #sky_0
 *
 *  @return			Standard status code. #SLY_SUCCESS will be
 *				returned even if a link to #sky_1 is not found
 *				in #sky_0. The only reason failure would be
 *				returned is if the #SK_Yoke's are malformed in
 *				some way.
 */

SlyDr SK_Yoke_unlink
(
	SK_Yoke *	sky_0,

	SK_Yoke *	sky_1
)
{
	return

	SK_Yoke_unlink_w_info
	(
		sky_0,
		sky_1,
		NULL,
		NULL,
		NULL
	);
}




/*!
 *  Unlinks all of the yokes from a singular #SK_Yoke.
 *
 *  @param sky			The #SK_Yoke to unlink all of the connections
 *
 *  @return			Standard status code
 */

SlyDr SK_Yoke_unlink_all
(
	SK_Yoke *	sky
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlyDr_get (SLY_BAD_ARG));


	//Iterate through every link in #sky, and unlink them one by one

	for (size_t l_sel = 0; l_sel < sky->links_len; l_sel += 1)
	{
		//Check if the link is active

		if (NULL != sky->links [l_sel].dst_yoke)
		{
			//Unlink the "other side" of the link from #sky first.
			//
			//Note how #SK_Yoke_unlink_single_by_ind () is used for
			//quick unlinking.

			//FIXME : Should #back_ind here be checked for
			//correctness for the sake of debugging?

			SK_Yoke * other =

			sky->links [l_sel].dst_yoke;


			size_t other_link_ind =

			sky->links [l_sel].back_ind;


			SK_Yoke_unlink_single_by_ind (other, other_link_ind);


			//Set the link on "this side" to a default state

			sky->links [l_sel] = SK_YokeLink_default ();
		}
	}


	//Reset the link counter in #sky

	sky->links_cnt = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Reallocates the array of connections at #sky->links so that a different
 *  number of connections can be supported by the yoke.
 *
 *  @param sky			The #SK_Yoke data structure to modify
 *
 *  @param new_links_len	The number of connections that the #SK_Yoke
 *				should be able to store
 *
 *  @param preserve_flag	A flag indicating whether or not to save as
 *				many connections as possible after the resizing
 *				operation. If this is false, then all the links
 *				will be reset to a default state.
 *
 *  @return			Standard status code
 */

SlySr  SK_Yoke_links_realloc
(
	SK_Yoke *		sky,
	size_t			new_links_len,
	int			preserve_flag
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlySr_get (SLY_BAD_ARG));


	//Check if any reallocation is necessary

	if (new_links_len == sky->links_len)
	{
		//Even if no reallocation occurred, make sure to respect the
		//#preserve_flag

		if (! preserve_flag)
		{
			SK_Yoke_unlink_all (sky);
		}

		return SlySr_get (SLY_SUCCESS);
	}


	//Allocate an array that can store #new_links_len number of #SK_YokeLink's

	SK_YokeLink * new_links = malloc (sizeof (SK_YokeLink) * new_links_len);

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, new_links, SlySr_get (SLY_BAD_ARG));


	//If preserve flag is not true, then exit early

	if (! preserve_flag)
	{
		//Unlink all of the link previously active in #sky

		SK_Yoke_unlink_all (sky);


		//Deallocate the memory previously used to store the links

		free (sky->links);


		//Initialize #new_links

		for (size_t l_sel = 0; l_sel < new_links_len; l_sel += 1)
		{
			new_links [l_sel] = SK_YokeLink_default ();
		}


		//Update #sky->links with #new_links

		sky->links =		new_links;

		sky->links_len =	new_links_len;

		sky->links_cnt =	0;


		return SlySr_get (SLY_SUCCESS);
	}


	//Since the preserve flag is true here, then keep as many connections
	//as possible the same.

	size_t new_l_sel = 0;

	for (size_t old_l_sel = 0; old_l_sel < sky->links_len; old_l_sel += 1)
	{
		//Detect if a link was active in the #SK_Yoke

		if (SK_YokeLink_is_active (sky->links [old_l_sel]))
		{
			//Check that there is space to preserve this link

			if (new_l_sel < new_links_len)
			{
				//There is space, so save the link

				new_links [new_l_sel] = sky->links [old_l_sel];


				//Since the link has moved to a new position in
				//#new_links [l_sel], make sure the yoke on the
				//other end knows this.

				//FIXME : Should error checking be done on
				//#other_l_ind is correct? Is it reliable or
				//not?

				SK_Yoke * other_sky =	new_links [new_l_sel].dst_yoke;

				size_t other_l_ind =	new_links [new_l_sel].back_ind;


				other_sky->links [other_l_ind].back_ind =

				new_l_sel;


				//Keep track of how many links have been
				//preserved so far

				new_l_sel += 1;
			}

			else
			{
				//There is no space for this link, so it has to
				//be disabled properly

				SK_Yoke * other_sky =	sky->links [old_l_sel].dst_yoke;

				size_t other_l_ind =	sky->links [old_l_sel].back_ind;


				//FIXME : Should #other_l_ind be checked here for
				//safety?


				SK_Yoke_unlink_single_by_ind (sky, old_l_sel);

				SK_Yoke_unlink_single_by_ind (other_sky, other_l_ind);
			}
		}
	}


	//Record how many links have been preserved in #new_links

	size_t preserve_len = new_l_sel;


	//Initialize any links in #new_links that have not been used to
	//preserve a link

	for (size_t l_sel = preserve_len; l_sel < new_links_len; l_sel += 1)
	{
		new_links [l_sel] = SK_YokeLink_default ();
	}


	//Free the old array of links, and then replace it with the new array

	free (sky->links);


	sky->links =		new_links;

	sky->links_len =	new_links_len;

	sky->links_cnt =	preserve_len;


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Reallocates the number of in links in an #SK_Yoke so that the minimum
 *  number of links are allocated to support the current number of active
 *  links. This function is useful for reducing the memory usage of an
 *  #SK_Yoke.
 *
 *  @param sky			The #SK_Yoke to minimize the number of links
 *				inside
 *
 *  @return			Standard status code
 */

SlySr SK_Yoke_links_minify
(
	SK_Yoke *	sky
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlySr_get (SLY_BAD_ARG));


	return

	SK_Yoke_links_realloc
	(
		sky,
		sky->links_cnt,
		1
	);
}




/*!
 *  Creates/updates a link in one #SK_Yoke to another target #SK_Yoke, but
 *  WITHOUT updating the target #SK_Yoke whatsoever. If a link to #sky_1
 *  already exists inside of #sky_0, this will ensure that all other fields of
 *  that link are up to date.
 *
 *  @warning
 *  This function leaves the "other side" of the #SK_YokeLink with stale values,
 *  and that is intentional. As a result, using this function without taking
 *  this into account can cause a set of #SK_Yoke's to enter an inconsistent
 *  state and cause memory access errors. Unless there is a good reason for
 *  doing so, you should probably use SK_Yoke_link_no_alloc_w_info () instead.
 *
 *  Note, the memory access error in the above case would be #sky_1 getting
 *  destroyed without #sky_0 being updated, thus leaving a stale reference to a
 *  destroyed #sky_1 inside of #sky_0.
 *
 *  @warning
 *  Since only one side of link is established, there is NOT enough information
 *  to initialize #sky_0->links [sky_0_link_ind].back_ind. This MUST be set
 *  manually after the link has been established on BOTH sides!
 *
 *  @param sky_0		The #SK_Yoke that WILL be modified. A link to
 *				#sky_1 will be created in #sky_0, and if the
 *				link already exists, then all other fields it
 *				may have will be updated.
 *
 *  @param sky_1		The #SK_Yoke that will NOT be modified. A link
 *				to this yoke will be created inside of #sky_0.
 *
 *  @param need_realloc		Where to place a flag indicating that a
 *				reallocation is needed to add the link to
 *				#sky_1 inside of #sky_0. If this gets set to 1,
 *				this indicates that all the links inside of
 *				#sky_0 have been used up, and so the size needs
 *				to be increased. This can be NULL if this value
 *				is not desired.
 *
 *  @param sky_0_link_ind	Where to place the value of the index of the
 *				link in sky_0->links that was updated to
 *				connect to #sky_1. This has a default value of
 *				0 in the case that #need_realloc is set to 1.
 *				This CAN be NULL if this value is not desired.
 *				This ONLY has meaning if #need_realloc is 0.
 *				This CAN be NULL if this information is not
 *				desired.
 *
 *  @param link_existed		Where to place a flag indicating that the link
 *				from #sky_0 to #sky_1 already existed, and has
 *				been updated. This CAN be NULL if this
 *				information is not desired.
 *
 *  @return			Standard status code. The return value is only
 *				used to indicate malformed arguments. This can
 *				return SLY_SUCCESS even if the link could not
 *				be established. If this function fails, #sky_0
 *				will not be updated.
 */

SlyDr SK_Yoke_link_single
(
	SK_Yoke *	sky_0,
	SK_Yoke *	sky_1,
	int *		need_realloc,
	size_t *	sky_0_link_ind,
	int *		link_existed
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_1, SlyDr_get (SLY_BAD_ARG));

	if (sky_0 == sky_1)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"#sky_0 = %p was equal to #sky_1, link "
			"will not be formed to itself.",
			sky_0
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Note that it is not possible to exit this function early based on
	//#sky_0->links_cnt, because perhaps this function is being called to
	//determine #sky_0_link_ind and #link_existed. Those require iterating
	//through #sky_0->links to determine.


	//Iterate through all of the links in #sky_0, and determine if the link
	//to #sky_1 already exists. If it doesn't exist, determine where a link
	//to #sky_1 can be placed inside of #sky_0->links []

	int link_exists =	0;

	size_t exist_ind =	0;


	int link_avail =	0;

	size_t avail_ind =	0;


	for (size_t l_sel = 0; l_sel < sky_0->links_len; l_sel += 1)
	{
		//Detect if a link to #sky_1 exists

		if (sky_1 == sky_0->links [l_sel].dst_yoke)
		{
			link_exists = 1;

			exist_ind = l_sel;

			break;
		}


		//Detect the 1st available space for a link
		//
		//Note how this block does not contain a "break". The whole set
		//of links must still be iterated through to ensure that the
		//link does not already exist.

		if
		(
			SK_YokeLink_is_inactive (sky_0->links [l_sel])

			&&

			(0 == link_avail)

		)
		{
			link_avail = 1;

			avail_ind = l_sel;
		}
	}


	//Create the link to #sky_1 if necessary

	int link_updated = 0;

	size_t updated_ind = 0;


	if (link_exists)
	{
		//Consider the link fully updated if it already exists

		link_updated = 1;

		updated_ind = exist_ind;
	}

	else if (link_avail)
	{
		//The link does not exist, so it is necessary establish a new
		//one
		//
		//Note how the value of #back_ind can not be determined
		//right now, because the corresponding link in #sky_1 might not
		//exist right now

		sky_0->links [avail_ind].dst_yoke =		sky_1;
		sky_0->links [avail_ind].back_ind =		0;


		//Since a new link has been established, the link counter in
		//#sky_0 should be updated

		if (sky_0->links_cnt < sky_0->links_len)
		{
			sky_0->links_cnt += 1;
		}

		else
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#sky_0->links_cnt was not properly "
				"decremented at an earlier time. "
				"sky_0 = %p",
				sky_0
			);
		}


		//Indicate that a link has been updated

		link_updated = 1;

		updated_ind = avail_ind;
	}


	//If the link has not been added or updated, then raise the
	//#need_realloc flag.
	//
	//If a link has been updated, indicate the index of this link

	if (NULL != need_realloc)
	{
		*need_realloc = (! link_updated);
	}

	if (NULL != sky_0_link_ind)
	{
		*sky_0_link_ind = updated_ind;
	}

	if (NULL != link_existed)
	{
		*link_existed = link_exists;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Links together 2 #SK_Yoke data structures, provided that an #SK_YokeLink is
 *  available to use in both objects.
 *
 *  If there is not an #SK_YokeLink available in #sky_0 and #sky_1 to link
 *  together both objects, then this function will fail and no updates will be
 *  made to either object.
 *
 *  If the 2 #SK_Yoke's are already linked together, then #SLY_SUCCESS will be
 *  returned.
 *
 *  The suffix "_w_info" is because this provides more pointer arguments for
 *  where to place useful information than SK_Yoke_link ().
 *
 *  @param sky_0		The #SK_Yoke which should be linked to #sky_1
 *
 *  @param need_realloc_0	Where to place a flag indicating that
 *  				#sky_0->links will need to be reallocated in
 *  				order to establish a link to #sky_1. This can
 *  				be NULL if this value is not desired.
 *
 *  @param sky_0_l_ind		The index inside of #sky_0->links where the
 *  				link to #sky_1 was placed/updated. This is only
 *  				valid if #need_realloc_0 is 0 and
 *  				#need_realloc_1 is 0.  This CAN be NULL if this
 *  				value is not desired.
 *
 *  @param sky_1		The #SK_Yoke which should be linked to #sky_0
 *
 *  @param need_realloc_1	Where to place a flag indicating that
 *  				#sky_1->links will need to be reallocated in
 *  				order to establish a link to #sky_0. This can
 *  				be NULL if this value is not desired.
 *
 *  @param sky_1_l_ind		The index inside of #sky_1->links where the
 *  				link to #sky_0 was placed/updated. This is only
 *  				valid if #need_realloc_0 is 0 and
 *  				#need_realloc_1 is 0.  This CAN be NULL if this
 *  				value is not desired.
 *
 *  @param link_existed		Where to place a flag indicating that the link
 *				already existed (but has still been updated).
 *				This CAN be NULL if this value is not desired.
 *
 *  @return			Standard status code. This function will fail
 *				if an inactive #SK_YokeLink is not available in
 *				both #sky_0 and #sky_1 to establish a link
 *				between them.
 */

SlySr SK_Yoke_link_no_alloc_w_info
(
	SK_Yoke *	sky_0,
	int *		need_realloc_0,
	size_t *	sky_0_l_ind,

	SK_Yoke *	sky_1,
	int *		need_realloc_1,
	size_t *	sky_1_l_ind,

	int *		link_existed
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_0, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_1, SlySr_get (SLY_BAD_ARG));


	if (sky_0 == sky_1)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"#sky_0 = %p was equal to #sky_1, link "
			"will not be formed to itself.",
			sky_0
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	//Attempt to establish a link from sky_0-->sky_1 and from sky_1-->sky_0
	//
	//Note how the error-return occurs after the 2nd call to
	//SK_Yoke_link_single (); this is so that #need_realloc_1,
	//#sky_1_link_ind, and #link_existed_1 can still be returned correctly.

	int realloc_0 =		0;

	size_t sky_0_link_ind =	0;

	int link_existed_0 = 	0;


	//SlySr sr_0 =

	SK_Yoke_link_single
	(
		sky_0,
		sky_1,
		&realloc_0,
		&sky_0_link_ind,
		&link_existed_0
	);




	int realloc_1 =		0;

	size_t sky_1_link_ind =	0;

	int link_existed_1 =	0;


	//SlySr sr_1 =

	SK_Yoke_link_single
	(
		sky_1,
		sky_0,
		&realloc_1,
		&sky_1_link_ind,
		&link_existed_1
	);


	//BEFORE any error-returning occurs, provide the values requested by
	//the arguments

	if (NULL != need_realloc_0)
	{
		*need_realloc_0 = realloc_0;
	}

	if (NULL != sky_0_l_ind)
	{
		*sky_0_l_ind = sky_0_link_ind;
	}

	if (NULL != need_realloc_1)
	{
		*need_realloc_1 = realloc_1;
	}

	if (NULL != sky_1_l_ind)
	{
		*sky_1_l_ind = sky_1_link_ind;
	}

	if (NULL != link_existed)
	{
		*link_existed = (link_existed_0 || link_existed_1);
	}

	if (link_existed_0 != link_existed_1)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"BUG : A stale link existed between #sky_0 and #sky_1 "
			"previously. "
			"(link_existed_0 = %d, link_existed_1 = %d)",
			link_existed_0,
			link_existed_1
		);
	}


	//If either side of the linking process failed, unlink the two yokes
	//so that no stale links are left behind.
	//
	//Note that SK_Yoke_unlink_w_info () can NOT be used here, because the link is
	//not guaranteed to exist on both sides

	if (realloc_0 || realloc_1)
	{
		SK_Yoke_unlink_single (sky_0, sky_1, NULL, NULL, NULL);

		SK_Yoke_unlink_single (sky_1, sky_0, NULL, NULL, NULL);


		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	//Now that the link between the two yokes is known to exist in both
	//#SK_Yoke's, make sure that the #back_ind in each updated #SK_YokeLink
	//is updated properly.

	sky_0->links [sky_0_link_ind].back_ind = sky_1_link_ind;

	sky_1->links [sky_1_link_ind].back_ind = sky_0_link_ind;


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See SK_Yoke_link_no_alloc_w_info (). This function does the same thing, but
 *  provides less information about the process.
 *
 *  @param sky_0		The #SK_Yoke which should be linked to #sky_1
 *
 *  @param sky_1		The #SK_Yoke which should be linked to #sky_0
 *
 *  @return			Standard status code. This function will fail
 *				if an inactive #SK_YokeLink is not available in
 *				both #sky_0 and #sky_1 to establish a link
 *				between them.
 */

SlySr SK_Yoke_link_no_alloc
(
	SK_Yoke *	sky_0,

	SK_Yoke *	sky_1
)
{

	return

	SK_Yoke_link_no_alloc_w_info
	(
		sky_0,
		NULL,
		NULL,

		sky_1,
		NULL,
		NULL,

		NULL
	);
}




/*!
 *  This checks if a link already exists between 2 #SK_Yoke's.
 *
 *  @param sky_0		The 1st #SK_Yoke, which may or may not be
 *				linked to #sky_1
 *
 *  @param sky_1		The 2nd #SK_Yoke, which may or may not be
 *				linked to #sky_0
 *
 *  @param link_exists		If #sky_0 is linked to #sky_1, then a '1'
 *  				will be placed here. Otherwise, a '0' will be
 *  				placed here. This argument CAN be NULL if this
 *  				information is not desired.
 *
 *  @param sky_0_l_ind		Where to place the index of #sky_0->links []
 *				that holds the #SK_YokeLink to #sky_1. This
 *				argument CAN be NULL if this information is not
 *				desired.
 *
 *  @param sky_1_l_ind		Where to place the index of #sky_1->links []
 *				that holds the #SK_YokeLink to #sky_0. This
 *				argument CAN be NULL if this information is not
 *				desired.
 *
 *  @return			Standard status code
 *
 */

SlyDr SK_Yoke_link_exists
(
	SK_Yoke *	sky_0,
	SK_Yoke *	sky_1,
	int *		link_exists,
	size_t *	sky_0_l_ind,
	size_t *	sky_1_l_ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_1, SlyDr_get (SLY_BAD_ARG));


	//Inside of #sky_0, attempt to find the link to #sky_1

	int link_found = 0;

	size_t l_ind_0 = 0;
	size_t l_ind_1 = 0;


	for (size_t l_sel = 0; l_sel < sky_0->links_len; l_sel += 1)
	{

		SK_YokeLink cur_link = sky_0->links [l_sel];


		if (sky_1 == cur_link.dst_yoke)
		{
			link_found =	1;

			l_ind_0 =	l_sel;


			//Save what this link indicates is the index inside of
			//#sky_1->links [] that connects to #sky_0

			l_ind_1 =	cur_link.back_ind;


			break;
		}
	}


	//Check that #sky_1 actually links back to #sky_0 in a way consistent
	//with the #back_ind field checked earlier
	//
	//FIXME : When #SK_Yoke is reliably tested, only perform this section
	//when #SK_YOKE_L_DEBUG is '1'

	if (link_found)
	{
		if (l_ind_1 < sky_1->links_len)
		{
			if (sky_0 != sky_1->links [l_ind_1].dst_yoke)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"The link from #sky_1 -> #sky_0 at "
					"#l_ind_1 = %lu did not exist!",
					l_ind_1
				);


				return SlyDr_get (SLY_BAD_ARG);
			}
		}

		else
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"(#l_ind_1 = %lu) > (sky_1->links_len = %lu)",
				l_ind_1,
				sky_1->links_len
			);


			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Save all requested values about the existence of the link

	if (NULL != link_exists)
	{
		*link_exists = link_found;
	}

	if (NULL != sky_0_l_ind)
	{
		*sky_0_l_ind = l_ind_0;
	}

	if (NULL != sky_1_l_ind)
	{
		*sky_1_l_ind = l_ind_1;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Fetches the number of active links in an #SK_Yoke, and the number of
 *  maximum links that it can support without requiring a memory reallocation.
 *
 *  @param sky			The #SK_Yoke to report the number active /
 *				maximum links inside
 *
 *  @param num_active_links	Where to place the number of active links in the
 *  				#SK_Yoke. This CAN be NULL.
 *
 *  @param num_max_links	Where to place the maximum number of active
 *  				links that can be stored in the #SK_Yoke
 *  				without requiring a memory reallocation. This
 *  				CAN be NULL.
 *
 *  @return			Standard status code
 */

SlyDr SK_Yoke_get_num_links
(
	SK_Yoke *	sky,
	size_t *	num_active_links,
	size_t *	num_max_links
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlyDr_get (SLY_BAD_ARG));


	//Get the number of active links and maximum number of links, if
	//requested

	if (NULL != num_active_links)
	{
		*num_active_links = sky->links_cnt;
	}


	if (NULL != num_max_links)
	{
		*num_max_links = sky->links_len;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Links together 2 #SK_Yoke data structures, and automatically reallocate the
 *  links in the #SK_Yoke data structures as necessary to allow this to happen.
 *
 *  Other than automatically reallocating the links as necessary, this function
 *  is very similar to SK_Yoke_link_no_alloc_w_info ().
 *
 *  @param sky_0		See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param sky_0_l_ind		See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param sky_0_incr		In the event that #sky_0 needs to have its
 *				links reallocated, this indicates how many more
 *				links should allocated in #sky_0. This is
 *				useful for avoiding frequent reallocations.
 *
 *				If uncertain, leave this value as 1.
 *
 *  @param sky_1		See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param sky_1_l_ind		See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param sky_1_incr		In the event that #sky_1 needs to have its
 *				links reallocated, this indicates how many more
 *				links should allocated in #sky_1. This is
 *				useful for avoiding frequent reallocations.
 *
 *				If uncertain, leave this value as 1.
 *
 *  @param link_existed		See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @return			Standard status code
 */

SlySr SK_Yoke_link_w_info
(
	SK_Yoke *	sky_0,
	size_t *	sky_0_l_ind,
	size_t		sky_0_incr,

	SK_Yoke *	sky_1,
	size_t *	sky_1_l_ind,
	size_t		sky_1_incr,

	int *		link_existed
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_0, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_1, SlySr_get (SLY_BAD_ARG));


	if (sky_0 == sky_1)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"#sky_0 = %p was equal to #sky_1, link "
			"will not be formed to itself.",
			sky_0
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	//Check if there is no more space in #sky_0 and #sky_1 for a new link

	int sky_0_is_full = (sky_0->links_cnt >= sky_0->links_len);

	int sky_1_is_full = (sky_1->links_cnt >= sky_1->links_len);


	//In the event that either #sky_0 or #sky_1 is full, check if a link
	//between #sky_0 and #sky_1 already exists. This determines whether or
	//not a link between #sky_0 and #sky_1 is already accounted for by both
	//#sky_0->links_cnt and #sky_1->links_cnt.
	//
	//Note, this does not allow for early exiting, because #u_en and #d_en
	//in the links might require updating.

	//FIXME : What should be done here now that #u_en and #d_en have been
	//removed?

	int sky_0_needs_realloc = 0;

	int sky_1_needs_realloc = 0;


	if (sky_0_is_full || sky_1_is_full)
	{
		int link_exists = 0;

		SK_Yoke_link_exists (sky_0, sky_1, &link_exists, NULL, NULL);


		if ((0 == link_exists) && (sky_0_is_full))
		{
			sky_0_needs_realloc = 1;
		}


		if ((0 == link_exists) && (sky_1_is_full))
		{
			sky_1_needs_realloc = 1;
		}
	}


	//Reallocate the links in #sky_0 and #sky_1 as needed

	if (sky_0_needs_realloc)
	{
		if (0 >= sky_0_incr)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"Could not increase number of links in "
				"#sky_0 = %p, because #sky_0_incr = %zu",
				sky_0,
				sky_0_incr
			);

			return SlySr_get (SLY_BAD_ALLOC);
		}


		size_t sky_0_new_links_len = sky_0->links_len + sky_0_incr;


		SlySr sr =

		SK_Yoke_links_realloc
		(
			sky_0,
			sky_0_new_links_len,
			1
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"Could not reallocate links in #sky_0 = %p to "
				"store %zu links.",
				sky_0,
				sky_0_new_links_len
			);


			return sr;
		}
	}


	if (sky_1_needs_realloc)
	{
		if (0 >= sky_1_incr)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"Could not increase number of links in "
				"#sky_1 = %p, because #sky_1_incr = %zu",
				sky_1,
				sky_1_incr
			);

			return SlySr_get (SLY_BAD_ALLOC);
		}


		size_t sky_1_new_links_len = sky_1->links_len + sky_1_incr;


		SlySr sr =

		SK_Yoke_links_realloc
		(
			sky_1,
			sky_1_new_links_len,
			1
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"Could not reallocate links in #sky_1 = %p to "
				"store %zu links.",
				sky_1,
				sky_1_new_links_len
			);


			return sr;
		}
	}


	//Establish (or update) a link between #sky_0 and #sky_1

	SlySr sr =

	SK_Yoke_link_no_alloc_w_info
	(
		sky_0,
		NULL,
		sky_0_l_ind,

		sky_1,
		NULL,
		sky_1_l_ind,

		link_existed
	);


	return sr;
}




/*!
 *  See SK_Yoke_link_w_info (). This function does the same thing, it just
 *  provides less information about the process.
 *
 *  @param sky_0		See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param sky_0_incr		In the event that #sky_0 needs to have its
 *				links reallocated, this indicates how many more
 *				links should allocated in #sky_0. This is
 *				useful for avoiding frequent reallocations.
 *
 *				If uncertain, leave this value as 1.
 *
 *  @param sky_1		See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param sky_1_incr		In the event that #sky_1 needs to have its
 *				links reallocated, this indicates how many more
 *				links should allocated in #sky_1. This is
 *				useful for avoiding frequent reallocations.
 *
 *				If uncertain, leave this value as 1.
 *
 *  @return			Standard status code
 */

SlySr SK_Yoke_link
(
	SK_Yoke *	sky_0,
	size_t		sky_0_incr,

	SK_Yoke *	sky_1,
	size_t		sky_1_incr
)
{
	return

	SK_Yoke_link_w_info
	(
		sky_0,
		NULL,
		sky_0_incr,

		sky_1,
		NULL,
		sky_1_incr,

		NULL
	);
}




/*
 *  Checks if there is stale data left behind after a link between the 2
 *  #SK_Yoke's has been broken.
 *
 *  To be clear, a link is considered "stale" if _any_ values of the link are
 *  incorrect. So, for example an #SK_YokeLink might be considered stale if
 *  #dst_yoke is NULL when it should not be, or it might be considered stale if
 *  #back_ind does not index the correct link.
 *
 *  If this function reports stale link data between 2 #SK_Yoke's, that usually
 *  indicates a bug internal to this library, unless #SK_Yoke_unlink_single ()
 *  or #SK_Yoke_unlink_single_by_ind () was just used shortly beforehand.
 *
 *  This function is very similar to SK_Yoke_link_exists (), except that it is
 *  prepared to handle the case where #sky_0 and #sky_1 contain stale data
 *  about each other.
 *
 *  The "_single" in the name of this function refers to the fact that only the
 *  link between #sky_0 and #sky_1 is checked.
 *
 *  @warning
 *  This function must iterate entirely through #sky_0 and #sky_1, and is
 *  slower than SK_Yoke_link_exists () as a result. This function is primarily
 *  for bug-detection, it is not meant for checking the existence of a link
 *  between 2 SK_Yoke's (although it must do the latter to provide the former).
 *
 *  @param sky_0		The 1st #SK_Yoke, which may or may not be
 *				linked to #sky_1
 *
 *  @param sky_1		The 2nd #SK_Yoke, which may or may not be
 *				linked to #sky_0
 *
 *  @param link_exists		If in #sky_0 a link_exists to #sky_1, then a
 *				'1' will be placed here. Otherwise, a '0' will
 *				be placed here.
 *
 *				A 0 will be placed here if either #stale_flag_0
 *				or #stale_flag_1 is '1'.
 *
 *  				This argument CAN be NULL if this
 *  				information is not desired.
 *
 *  @param stale_flag_0		This will be set to 1 if #sky_0 contained a
 *				stale link to #sky_1. This CAN be NULL if this
 *				information is not desired.
 *
 *  @param stale_flag_1		This will be set to 1 if #sky_1 contained a
 *				stale link to #sky_0. This CAN be NULL if this
 *				information is not desired.
 *
 *  @param bad_back_l_0		Indicates that the link in #sky_0 to #sky_1 had
 *				a bad value for #back_ind. This CAN be NULL
 *				if this information is not desired.
 *
 *  @param bad_back_l_1		Indicates that the link in #sky_1 to #sky_0 had
 *				a bad value for #back_ind. This CAN be NULL
 *				if this information is not desired.
 *
 *  @param sky_0_l_ind		Where to place the index of #sky_0->links []
 *				that holds the #SK_YokeLink to #sky_1.
 *
 *				If #stale_flag_0 is '1', then this represents
 *				the index of the stale link in #sky_0->links [].
 *
 *				This argument CAN be NULL if this information
 *				is not desired.
 *
 *  @param sky_1_l_ind		Where to place the index of #sky_1->links []
 *				that holds the #SK_YokeLink to #sky_0.
 *
 *				If #stale_flag_1 is '1', then this represents
 *				the index of the stale link in #sky_1->links [].
 *
 *				This argument CAN be NULL if this information
 *				is not desired.
 *
 *  @return			Standard status code
 */

SlyDr SK_Yoke_stale_link_single
(
	SK_Yoke *	sky_0,
	SK_Yoke *	sky_1,
	int *		link_exists,
	int *		stale_flag_0,
	int *		stale_flag_1,
	int *		bad_back_l_0,
	int *		bad_back_l_1,
	size_t *	sky_0_l_ind,
	size_t *	sky_1_l_ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_1, SlyDr_get (SLY_BAD_ARG));


	//Initialize some variables that indicate the existence/position of the
	//#SK_YokeLink between #sky_0 and #sky_1

	int stale_0 =		0;
	int stale_1 =		0;

	int bad_b_l_0 =		0;
	int bad_b_l_1 =		0;

	int link_found_0 = 	0;
	int link_found_1 =	0;

	size_t l_ind_0 = 	0;
	size_t l_ind_1 = 	0;

	size_t b_l_ind_0 =	0;
	size_t b_l_ind_1 =	0;


	//Inside of #sky_0, attempt to find the link to #sky_1

	for (size_t l_sel = 0; l_sel < sky_0->links_len; l_sel += 1)
	{
		SK_YokeLink cur_link = sky_0->links [l_sel];


		if (sky_1 == cur_link.dst_yoke)
		{
			link_found_0 =	1;

			l_ind_0 =	l_sel;

			b_l_ind_0 =	cur_link.back_ind;

			break;
		}
	}


	//Inside of #sky_1, attempt to find the link to #sky_0

	for (size_t l_sel = 0; l_sel < sky_1->links_len; l_sel += 1)
	{
		SK_YokeLink cur_link = sky_1->links [l_sel];


		if (sky_0 == cur_link.dst_yoke)
		{
			link_found_1 =	1;

			l_ind_1 =	l_sel;

			b_l_ind_1 =	cur_link.back_ind;

			break;
		}
	}


	//Determine if the back-link-indices on either side of the link are bad

	if (link_found_0)
	{
		if (b_l_ind_0 < sky_1->links_len)
		{
			bad_b_l_0 = (sky_0 != sky_1->links [b_l_ind_0].dst_yoke);
		}

		else
		{
			bad_b_l_0 = 1;
		}
	}

	else
	{
		bad_b_l_0 = 0;
	}


	if (link_found_1)
	{
		if (b_l_ind_1 < sky_0->links_len)
		{
			bad_b_l_1 = (sky_1 != sky_0->links [b_l_ind_1].dst_yoke);
		}

		else
		{
			bad_b_l_1 = 1;
		}
	}

	else
	{
		bad_b_l_1 = 0;
	}


	//Determine if either side of the link is stale

	stale_0 = (link_found_0 && (! link_found_1)) || (bad_b_l_0);

	stale_1 = ((! link_found_0) && link_found_1) || (bad_b_l_1);


	//Save all requested values about the existence of the link

	if (NULL != link_exists)
	{
		*link_exists =

		(link_found_0 && link_found_1 && (! stale_0) && (! stale_1));
	}

	if (NULL != stale_flag_0)
	{
		*stale_flag_0 = stale_0;
	}

	if (NULL != stale_flag_1)
	{
		*stale_flag_1 = stale_1;
	}

	if (NULL != bad_back_l_0)
	{
		*bad_back_l_0 = bad_b_l_0;
	}

	if (NULL != bad_back_l_1)
	{
		*bad_back_l_1 = bad_b_l_1;
	}

	if (NULL != sky_0_l_ind)
	{
		*sky_0_l_ind = l_ind_0;
	}

	if (NULL != sky_1_l_ind)
	{
		*sky_1_l_ind = l_ind_1;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  This checks if any of the links contained inside of #sky_0 are stale.
 *
 *  @warning
 *  Unlike SK_Yoke_stale_link_single (), this can not detect _all_ stale links
 *  that point TO #sky_0, but it CAN detect _all_ stale links FROM #sky_0.
 *  Ergo, if this function is used on every #SK_Yoke in some collection of
 *  #SK_Yoke's, all the stale links possible in that collection will still be
 *  detected.
 *
 *  @warning
 *  This function iterates through all of the links in #sky_0, so this function
 *  may be slow for #SK_Yoke's that contain large numbers of links.
 *
 *  @param sky_0		The #SK_Yoke to examine for the presence of
 *				stale links
 *
 *  @param sky_1		Where to place a pointer to the #SK_Yoke that
 *				the 1st stale link connected to. This will be
 *				made to equal NULL if there were no detected
 *				stale links. This argument can be NULL if this
 *				information is not desired.
 *
 *  @param stale_flag_0		This will be set to 1 if #sky_0 contained a
 *				stale link to #sky_1. This CAN be NULL if this
 *				information is not desired.
 *
 *  @param stale_flag_1		This will be set to 1 if #sky_1 contained a
 *				stale link to #sky_0. This CAN be NULL if this
 *				information is not desired.
 *
 *  @param bad_back_l_0		Indicates that the link in #sky_0 to #sky_1 had
 *				a bad value for #back_ind. This CAN be NULL
 *				if this information is not desired.
 *
 *  @param bad_back_l_1		Indicates that the link in #sky_1 to #sky_0 had
 *				a bad value for #back_ind. This CAN be NULL
 *				if this information is not desired.
 *
 *  @param sky_0_l_ind		Where to place the index of #sky_0->links []
 *				that holds the #SK_YokeLink to #sky_1.
 *
 *				If #stale_flag_0 is '1', then this represents
 *				the index of the stale link in #sky_0->links [].
 *
 *				This argument CAN be NULL if this information
 *				is not desired.
 *
 *  @param sky_1_l_ind		Where to place the index of #sky_1->links []
 *				that holds the #SK_YokeLink to #sky_0.
 *
 *				If #stale_flag_1 is '1', then this represents
 *				the index of the stale link in #sky_1->links [].
 *
 *				This argument CAN be NULL if this information
 *				is not desired.
 *
 *  @return			Standard status code
 */

SlyDr SK_Yoke_stale_link_all
(
	SK_Yoke *	sky_0,

	SK_Yoke **	sky_1,

	int *		stale_flag_0,
	int *		stale_flag_1,
	int *		bad_back_l_0,
	int *		bad_back_l_1,
	size_t *	sky_0_l_ind,
	size_t *	sky_1_l_ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_0, SlyDr_get (SLY_BAD_ARG));


	//Iterate through all of the links inside of #sky_0, checking if any of
	//them contain stale data

	SK_Yoke * cur_sky = NULL;

	int stale_0 = 0;
	int stale_1 = 0;


	for (size_t l_sel = 0; l_sel < sky_0->links_len; l_sel += 1)
	{
		//Select a single yoke on the other side of a link in #sky_0,
		//and check if the link to that yoke is stale

		cur_sky = sky_0->links [l_sel].dst_yoke;


		if (NULL != cur_sky)
		{
			SK_Yoke_stale_link_single
			(
				sky_0,
				cur_sky,
				NULL,
				&stale_0,
				&stale_1,
				bad_back_l_0,
				bad_back_l_1,
				sky_0_l_ind,
				sky_1_l_ind
			);


			//Check if a stale link has been found, end the search
			//for stale links in that case

			if (stale_0 || stale_1)
			{
				break;
			}
		}
	}


	//Save the requested values that haven't already been serviced by
	//SK_Yoke_stale_link_single () in the above loop.

	if (NULL != sky_1)
	{
		if (stale_0 || stale_1)
		{
			*sky_1 = cur_sky;
		}

		else
		{
			*sky_1 = NULL;
		}
	}


	if (NULL != stale_flag_0)
	{
		*stale_flag_0 = stale_0;
	}


	if (NULL != stale_flag_1)
	{
		*stale_flag_1 = stale_1;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  This checks that there are no duplicates inside of the #links array in an
 *  #SK_Yoke.
 *
 *  Currently, #SK_Yoke is designed such that there should be no duplicates in
 *  the #links array. Ergo, if this function reports that duplicates exist,
 *  that should be considered a bug.
 *
 *  2 #SK_YokeLink's are considered duplicates if they point to the same
 *  #SK_Yoke with #dst_yoke, even if their other fields differ. Inactive links
 *  are not considered duplicates, however.
 *
 *  @param sky			The #SK_Yoke to examine for duplicate links.
 *
 *  @param dup_flag		A '1' will be placed here if a duplicate has
 *				been detected. Otherwise, a '0' will be placed
 *				here. This CAN be NULL if this information is
 *				not desired.
 *
 *  @param dup_ind_0		Where to place the 1st index of the duplicated
 *	           		link that is inside of #sky->links []. A '0'
 *	           		will placed here if there were no duplicates.
 *	           		This CAN be NULL if this information is not
 *	           		desired.
 *
 *  @param dup_ind_1		Where to place the 2nd index of the duplicated
 *				link that is inside of #sky->links []. A '0'
 *				will be paced here if there were no duplicates.
 *				This CAN be NULL if this information is not
 *				desired.
 *
 *  @return			Standard status code
 */

SlyDr SK_Yoke_detect_dup_link
(
	SK_Yoke *	sky,
	int *		dup_flag,
	size_t *	dup_ind_0,
	size_t *	dup_ind_1
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlyDr_get (SLY_BAD_ARG));


	//Iterate through all of the links inside of #sky, and check for
	//duplicates for each one

	int d_flag =		0;

	size_t d_ind_0 =	0;

	size_t d_ind_1 =	0;


	for (size_t l_sel_0 = 0; l_sel_0 < sky->links_len; l_sel_0 += 1)
	{
		if (NULL != sky->links [l_sel_0].dst_yoke)
		{
			for
			(
				size_t l_sel_1 = (l_sel_0 + 1);

				l_sel_1 < sky->links_len;

				l_sel_1 += 1
			)
			{
				if
				(
					sky->links [l_sel_0].dst_yoke

					==

					sky->links [l_sel_1].dst_yoke
				)
				{
					d_flag = 1;

					d_ind_0 = l_sel_0;

					d_ind_1 = l_sel_1;
				}
			}
		}
	}


	//Save all the requested values

	if (NULL != dup_flag)
	{
		*dup_flag = d_flag;
	}

	if (NULL != dup_ind_0)
	{
		*dup_ind_0 = d_ind_0;
	}

	if (NULL != dup_ind_1)
	{
		*dup_ind_1 = d_ind_1;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  This function checks if the #links_cnt field in an #SK_Yoke is consistent
 *  with the number of links actually allocated in #links.
 *
 *  If this function reports an inconsitency between these 2 values, that
 *  should be considered a bug.
 *
 *  @warning
 *  This function does not check for duplicates; so the value of #links_cnt can
 *  be reported as correct if duplicates make up for a bad link counter. That
 *  is why this function does not replace SK_Yoke_detect_dup_link ().
 *
 *  @param sky			The #SK_Yoke to examine
 *
 *  @param bad_cnt_flag		A '1' will be placed here if an incorrect value
 *				for #sky->links_cnt was detected. Otherwise,
 *				a '0' will be placed here. This CAN be NULL if
 *				this value is not desired.
 *
 *  @param ideal_links_cnt	Where to place the value that #links_cnt should
 *				have been equal to. This CAN be NULL if this
 *				value is not desired.
 *
 *  @return			Standard status code
 */

SlyDr SK_Yoke_verify_links_cnt
(
	SK_Yoke *	sky,
	int *		bad_cnt_flag,
	size_t *	ideal_links_cnt
)
{
	//Sanity checks on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlyDr_get (SLY_BAD_ARG));


	//Determine what #sky->links_cnt SHOULD be by individually counting
	//every link

	size_t ideal_cnt = 0;

	for (size_t l_sel = 0; l_sel < sky->links_len; l_sel += 1)
	{
		if (SK_YokeLink_is_active (sky->links [l_sel]))
		{
			ideal_cnt += 1;
		}
	}


	int bad_cnt = (ideal_cnt != sky->links_cnt);


	//Save all requested values

	if (NULL != bad_cnt_flag)
	{
		*bad_cnt_flag = bad_cnt;
	}

	if (NULL != ideal_links_cnt)
	{
		*ideal_links_cnt = ideal_cnt;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Detects multiple kinds of errors in an #SK_Yoke, and prints out relevant
 *  information when the error is detected.
 *
 *  @param sky			The #SK_Yoke which errors should be checked for
 *
 *  @param f_ptr		The #FILE to print information about errors to.
 *  				This CAN be NULL if no printing is desired.
 *
 *  @param pre_str		A string to print out before printing out an
 *				error
 *
 *  @param post_str		A string to print out after printing out an
 *				error
 *
 *  @param error_flag		If any error occurred, a '1' will be placed
 *				here. Otherwise, a '0' will be placed here.
 *				This CAN be NULL if this information is not
 *				desired.
 *
 *  @return			Standard status code.
 */

SlyDr SK_Yoke_error_check
(
	SK_Yoke *	sky,
	FILE *		f_ptr,
	const char *	pre_str,
	const char *	post_str,
	int *		error_flag
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlyDr_get (SLY_BAD_ARG));


	if (NULL == pre_str)
	{
		pre_str = "";
	}


	if (NULL == post_str)
	{
		post_str = "";
	}


	//Initialize an error detection flag

	int error_found = 0;


	//Run a series of functions on #sky that can detect errors inside of
	//#SK_Yoke's


	//Check if #sky contains any stale links

	SK_Yoke * sky_1 =	NULL;

	int stale_flag_0 = 	0;
	int stale_flag_1 = 	0;

	int bad_back_l_0 = 	0;
	int bad_back_l_1 = 	0;

	size_t sky_0_l_ind = 	0;
	size_t sky_1_l_ind = 	0;


	SK_Yoke_stale_link_all
	(
		sky,

		&sky_1,

		&stale_flag_0,
		&stale_flag_1,

		&bad_back_l_0,
		&bad_back_l_1,

		&sky_0_l_ind,
		&sky_1_l_ind
	);


	if (stale_flag_0)
	{
		error_found = 1;


		if (NULL != f_ptr)
		{
			fprintf
			(
				f_ptr,
				"%s"
				"#sky contained a stale link!\n\n"
				"sky =          %p\n"
				"stale_flag_0 = %d\n"
				"bad_back_l_0 = %lu\n"
				"sky_0_l_ind =  %lu\n"
				"%s",
				pre_str,
				sky,
				stale_flag_0,
				(long unsigned) bad_back_l_0,
				(long unsigned) sky_0_l_ind,
				post_str
			);
		}
	}


	if (stale_flag_1)
	{
		error_found = 1;


		if (NULL != f_ptr)
		{
			fprintf
			(
				f_ptr,
				"%s"
				"#A yoke connected to #sky had a stale link!\n\n"
				"sky =          %p\n"
				"sky_1 =        %p\n"
				"stale_flag_1 = %d\n"
				"bad_back_l_1 = %lu\n"
				"sky_1_l_ind =  %lu\n"
				"%s",
				pre_str,
				sky,
				sky_1,
				stale_flag_1,
				(long unsigned) bad_back_l_1,
				(long unsigned) sky_1_l_ind,
				post_str
			);
		}
	}


	//Check if #sky contained any duplcate links

	int dup_flag =     0;

	size_t dup_ind_0 = 0;
	size_t dup_ind_1 = 0;


	SK_Yoke_detect_dup_link
	(
		sky,
		&dup_flag,
		&dup_ind_0,
		&dup_ind_1
	);


	if (dup_flag)
	{
		error_found = 1;


		if (NULL != f_ptr)
		{
			fprintf
			(
				f_ptr,
				"%s"
				"#sky contained duplicate links!\n\n"
				"sky =       %p\n"
				"dup_flag =  %d\n"
				"dup_ind_0 = %lu\n"
				"dup_ind_1 = %lu\n"
				"%s",
				pre_str,
				sky,
				dup_flag,
				dup_ind_0,
				dup_ind_1,
				post_str
			);
		}
	}


	//Verify if #links_cnt in #sky is consistent with the number of active
	//links inside of it

	int bad_cnt_flag = 0;

	size_t ideal_links_cnt = 0;


	SK_Yoke_verify_links_cnt
	(
		sky,
		&bad_cnt_flag,
		&ideal_links_cnt
	);


	if (bad_cnt_flag)
	{
		error_found = 1;


		if (NULL != f_ptr)
		{
			fprintf
			(
				f_ptr,
				"%s"
				"#sky had a bad value for #links_cnt!\n\n"
				"sky =             %p\n"
				"bad_cnt_flag =    %d\n"
				"ideal_links_cnt = %lu\n"
				"sky->links_cnt =  %lu\n"
				"%s",
				pre_str,
				sky,
				bad_cnt_flag,
				ideal_links_cnt,
				sky->links_cnt,
				post_str
			);
		}
	}


	//Record whether or not an error was detected inside of #sky

	if (NULL != error_flag)
	{
		*error_flag = error_found;
	}


	return SlyDr_get (SLY_SUCCESS);
}




//FIXME : Create test-benches to test if the #SK_Yoke structure works in
//general, and also create functions to test if the linking/unlinking process
//works correctly as well




/*!
 *  Performs a simple test bench on SK_Yoke_create ()
 *
 *  @return			Standard status code
 */

SlySr SK_Yoke_create_tb ()
{
	printf ("SK_Yoke_create_tb () : BEGIN\n\n");


	const u32 NUM_TESTS =	32;

	int error_detected =	0;

	SK_Yoke * sky =		NULL;


	for (u32 test_num = 0; test_num < NUM_TESTS; test_num += 1)
	{
		printf
		(
			"SK_Yoke_create_tb () iteration #%" PRIu32 "\n\n",
			test_num
		);


		//Select some arbitrary parameters for the #SK_Yoke to create

		size_t links_len = (test_num);


		//Using the arbitrary arguments selected above, create an
		//#SK_Yoke

		SlySr sky_sr = SlySr_get (SLY_UNKNOWN_ERROR);

		sky = SK_Yoke_create (links_len, &sky_sr);


		if (SLY_SUCCESS != sky_sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"Could not create #SK_Yoke with parameters:\n"
				"links_len = %lu\n"
				"sky_sr =    %s\n\n",
				links_len,
				SlySrStr_get (sky_sr).str
			);

			error_detected = 1;

			break;
		}


		SK_Yoke_print_opt
		(
			sky,
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1
		);


		//Check that the returned pointer is consistent with the
		//returned #SlySr

		if ((NULL == sky) && (SLY_SUCCESS == sky_sr.res))
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#sky was NULL when #sky_sr = %s\n\n",
				SlySrStr_get (sky_sr).str
			);

			error_detected = 1;

			break;
		}


		//Ensure that #links_len was respected

		if (links_len != sky->links_len)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#links_len not respected.\n\n"
			);

			error_detected = 1;

			break;
		}


		//Detect a bad configuration of the #SK_Yoke

		int e_flag = 0;

		SK_Yoke_error_check
		(
			sky,
			stdout,
			"SK_Yoke_create_tb () : ",
			"\n",
			&e_flag
		);


		if (0 != e_flag)
		{
			error_detected = 1;

			break;
		}


		//Free memory allocated in this test iteration

		SK_Yoke_destroy (sky);
	}


	//Check if the test bench has succeeded or not

	if (error_detected)
	{
		//Make sure to free memory that couldn't be freed as a result
		//of the test iteration stopping prematurely

		SK_Yoke_destroy (sky);


		printf ("SK_Yoke_create_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("SK_Yoke_create_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test bench on SK_Yoke_error_check ()
 *
 *  @return			Standard status code
 */

SlySr SK_Yoke_error_check_tb ()
{
	printf ("SK_Yoke_error_check_tb () : BEGIN\n\n");


	//Create 2 #SK_Yoke's to use for intentionally creating stale links

	const size_t NUM_LINKS_PER_NODE = 2;


	SK_Yoke * sky_0 = SK_Yoke_create (NUM_LINKS_PER_NODE, NULL);

	SK_Yoke * sky_1 = SK_Yoke_create (NUM_LINKS_PER_NODE, NULL);


	if ((NULL == sky_0) || (NULL == sky_1))
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"Could not create #sky_0 / #sky_1."
		);

		SK_Yoke_destroy (sky_0);
		SK_Yoke_destroy (sky_1);

		return SlySr_get (SLY_BAD_ALLOC);
	}


	//Make sure that SK_Yoke_error_check () does not indicate an error on 2
	//newly created and unmodified #SK_Yoke's (this assumes that
	//SK_Yoke_create () works perfectly).

	printf
	(
		"Making sure false-positive errors not given for brand "
		"new #SK_Yoke's...\n\n"
	);


	int tb_fail = 0;


	int e_flag_0 = 0;

	int e_flag_1 = 0;


	SK_Yoke_error_check
	(
		sky_0,
		stdout,
		"SK_Yoke_error_check () : ",
		"\n",
		&e_flag_0
	);


	SK_Yoke_error_check
	(
		sky_1,
		stdout,
		"SK_Yoke_error_check () : ",
		"\n",
		&e_flag_1
	);


	if (e_flag_0 || e_flag_1)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"Error returned on newly created #SK_Yoke's!"
			"This either means that SK_Yoke_create () is broken "
			"or SK_Yoke_error_check () is broken.\n"
			"e_flag_0 = %d, e_flag_1 = %d",
			e_flag_0,
			e_flag_1
		);

		tb_fail = 1;
	}


	//"Manually" link together #sky_0 and #sky_1 such that it is guaranteed
	//that they are linked together correctly.
	//
	//Then, make sure that no error is reported regarding this link

	printf
	(
		"Making sure that no error is given for SK_Yoke's that are "
		"known to be linked correctly...\n\n"
	);


	sky_0->links [0].dst_yoke =	sky_1;
	sky_0->links [0].back_ind =	0;
	sky_0->links_cnt =		1;

	sky_1->links [0].dst_yoke =	sky_0;
	sky_1->links [0].back_ind =	0;
	sky_1->links_cnt =		1;


	SK_Yoke_error_check
	(
		sky_0,
		stdout,
		"SK_Yoke_error_check () : ",
		"\n",
		&e_flag_0
	);


	SK_Yoke_error_check
	(
		sky_1,
		stdout,
		"SK_Yoke_error_check () : ",
		"\n",
		&e_flag_1
	);


	if (e_flag_0 || e_flag_1)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"Error returned on manually established link!\n"
			"e_flag_0 = %d, e_flag_1 = %d",
			e_flag_0,
			e_flag_1
		);

		tb_fail = 1;
	}


	//Create a stale link in #sky_0 by linking it to #sky_1 without linking
	//#sky_1 to #sky_0

	printf
	(
		"Making sure that stale link in #sky_0 is reported correctly...\n\n"
	);


	sky_0->links [0].dst_yoke =	sky_1;
	sky_0->links [0].back_ind =	0;
	sky_0->links_cnt =		1;

	sky_1->links [0] = 		SK_YokeLink_default ();
	sky_1->links_cnt = 		0;


	SK_Yoke_error_check
	(
		sky_0,
		stdout,
		"SK_Yoke_error_check () : ",
		"\n",
		&e_flag_0
	);


	SK_Yoke_error_check
	(
		sky_1,
		stdout,
		"SK_Yoke_error_check () : ",
		"\n",
		&e_flag_1
	);


	if ((! e_flag_0) || e_flag_1)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"Stale link in #sky_0 not reported correctly!\n"
			"e_flag_0 = %d, e_flag_1 = %d",
			e_flag_0,
			e_flag_1
		);

		tb_fail = 1;
	}


	//Do the same as the above, but with #sky_1 having the stale link
	//instead of #sky_0
	//
	//FIXME : Perhaps this and the above code block should be combined in a
	//loop?

	printf
	(
		"Making sure that stale link in #sky_1 is reported correctly...\n\n"
	);


	sky_0->links [0] =		SK_YokeLink_default ();
	sky_0->links_cnt =		0;

	sky_1->links [0].dst_yoke =	sky_0;
	sky_1->links [0].back_ind =	0;
	sky_1->links_cnt =		1;


	SK_Yoke_error_check
	(
		sky_0,
		stdout,
		"SK_Yoke_error_check () : ",
		"\n",
		&e_flag_0
	);


	SK_Yoke_error_check
	(
		sky_1,
		stdout,
		"SK_Yoke_error_check () : ",
		"\n",
		&e_flag_1
	);


	if (e_flag_0 || (! e_flag_1))
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"Stale link in #sky_1 not reported correctly!\n"
			"e_flag_0 = %d, e_flag_1 = %d",
			e_flag_0,
			e_flag_1
		);

		tb_fail = 1;
	}


	//Place a duplicate link in #sky_0 and see if this is detected

	printf
	(
		"Making sure that duplicate link in #sky_0 is reported "
		"correctly...\n\n"
	);


	sky_0->links [0].dst_yoke =	sky_1;
	sky_0->links [0].back_ind =	0;
	sky_0->links [1] =		sky_0->links [0];
	sky_0->links_cnt =		2;

	sky_1->links [0].dst_yoke =	sky_0;
	sky_1->links [0].back_ind =	0;
	sky_1->links_cnt =		1;


	SK_Yoke_error_check
	(
		sky_0,
		stdout,
		"SK_Yoke_error_check () : ",
		"\n",
		&e_flag_0
	);


	if (! e_flag_0)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"Duplicate link in #sky_0 not reported correctly!\n"
			"e_flag_0 = %d",
			e_flag_0
		);

		tb_fail = 1;
	}


	sky_0->links [1] = SK_YokeLink_default ();


	//Force #sky_0->links_cnt to be incorrect and see if this is detected

	printf
	(
		"Making sure that bad value for #sky_0->links_cnt is "
		"reported correctly...\n\n"
	);


	sky_0->links [0].dst_yoke =	sky_1;
	sky_0->links [0].back_ind =	0;
	sky_0->links_cnt =		3;

	sky_1->links [0].dst_yoke =	sky_0;
	sky_1->links [0].back_ind =	0;
	sky_1->links_cnt =		1;

	SK_Yoke_error_check
	(
		sky_0,
		stdout,
		"SK_Yoke_error_check () : ",
		"\n",
		&e_flag_0
	);


	if (! e_flag_0)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"Incorrect value for #sky_0->links_cnt not "
			"reported correctly!\n"
			"e_flag_0 = %d",
			e_flag_0
		);

		tb_fail = 1;
	}


	//Destroy all of the #SK_Yoke's used in this test-bench

	SK_Yoke_destroy (sky_0);
	SK_Yoke_destroy (sky_1);


	//Check if the test had passed or not

	if (tb_fail)
	{
		printf ("SK_Yoke_error_check_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("SK_Yoke_error_check_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test bench on SK_Yoke_link_no_alloc_w_info ()
 *
 *  @return			Standard status code
 */

//FIXME: This function is somewhat long with all of the error reporting. Is it
//reasonable to split this function, despite the length being due to text
//strings?

SlySr SK_Yoke_link_no_alloc_w_info_tb ()
{
	printf ("SK_Yoke_link_no_alloc_w_info_tb () : BEGIN\n\n");


	//Create a yoke that will be used as the "extra" yoke later in the
	//test-bench

	SK_Yoke * sky_extra = SK_Yoke_create (1, NULL);

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_extra, SlySr_get (SLY_BAD_ALLOC));


	//Create some yokes to link together to form a fully-connected network

	const size_t NUM_NODES = 4;

	const size_t NUM_LINKS_PER_NODE = NUM_NODES - 1;


	SK_Yoke * sky [NUM_NODES];

	for (size_t n_sel = 0; n_sel < NUM_NODES; n_sel += 1)
	{
		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);

		sky [n_sel] = SK_Yoke_create (NUM_LINKS_PER_NODE, &sr);


		// Check that #sky was successfully created

		if (SLY_SUCCESS != sr.res)
		{
			//Deallocate any memory used in this function

			SK_Yoke_destroy (sky_extra);

			for (size_t d_sel = 0; d_sel <= n_sel; d_sel += 1)
			{
				SK_Yoke_destroy (sky [d_sel]);
			}


			SK_RES_RETURN (SK_YOKE_L_DEBUG, sr.res, sr);
		}
	}


	//Decide whether or not to attempt links from a #SK_Yoke to itself in
	//this test bench

	const int TEST_SELF_LINKS = 0;


	//In a loop, connect together each yoke to form a fully connected
	//network.
	//
	//Note, some yokes are intentionally attempted to be linked twice to
	//make sure that duplicate links are rejected.

	int tb_fail = 0;

	for (size_t sky_0_sel = 0; sky_0_sel < NUM_NODES; sky_0_sel += 1)
	{
		for (size_t sky_1_sel = 0; sky_1_sel < NUM_NODES; sky_1_sel += 1)
		{

			//Respect the option for #TEST_SELF_LINKS

			if ((0 == TEST_SELF_LINKS) && (sky_0_sel == sky_1_sel))
			{
				continue;
			}


			//Create a link between 2 #SK_Yoke's

			printf
			(
				"Linking (sky [%lu]) <-> (sky [%lu])...\n\n",
				(long unsigned) sky_0_sel,
				(long unsigned) sky_1_sel
			);


			int need_realloc_0 = 0;
			int need_realloc_1 = 0;

			size_t sky_0_l_ind = 0;
			size_t sky_1_l_ind = 0;

			int link_existed = 0;


			SlySr sr =

			SK_Yoke_link_no_alloc_w_info
			(
				sky [sky_0_sel],
				&need_realloc_0,
				&sky_0_l_ind,

				sky [sky_1_sel],
				&need_realloc_1,
				&sky_1_l_ind,

				&link_existed
			);


			//Ensure that #need_realloc_0 and #need_realloc_1 are
			//not asserted, because #NUM_LINKS_PER_NODE should be
			//large enough to implement the fully connected
			//network.

			if ((1 == need_realloc_0) || (1 == need_realloc_1))
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"need_realloc_0 = %d, need_realloc_1 = %d\n"
					"Neither should be 1 here.",
					need_realloc_0,
					need_realloc_1
				);

				tb_fail = 1;

				break;
			}


			//Ensure that the link creation _failed_ if the link
			//was from one #SK_Yoke back to itself

			if((SLY_SUCCESS == sr.res) && (sky_0_sel == sky_1_sel))
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"A link was allowed to be established "
					"from a yoke back to itself."
				);

				tb_fail = 1;

				break;
			}


			//Ensure that the link creation succeeded if the two
			//#SK_Yoke arguments were distinct

			if ((SLY_SUCCESS != sr.res) && (sky_0_sel != sky_1_sel))
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"A link was not successfully created / "
					"updated, although this attempt should "
					"have done so."
				);

				tb_fail = 1;

				break;
			}


			//Ensure that the link was established at the indices
			//reported by #sky_0_l_ind and #sky_1_l_ind

			if
			(
				(
				 	sky [sky_0_sel]->links [sky_0_l_ind].dst_yoke

				 	!=

					sky [sky_1_sel]
				)

				||

				(
					sky [sky_1_sel]->links [sky_1_l_ind].dst_yoke

					!=

					sky [sky_0_sel]
				)
			)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"The reported indices at which the "
					"links were established are incorrect. "
					"(sky_0_l_ind = %d, sky_1_l_ind = %d)",
					sky_0_l_ind,
					sky_1_l_ind
				);

				tb_fail = 1;

				break;
			}


			//Ensure that the link was reported as already existant
			//if the link should have already been created.

			if ((sky_1_sel < sky_0_sel) && (! link_existed))
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"This link was not reported as already "
					"existant even though it should have "
					"been. (link_existed = %d)",
					link_existed
				);

				tb_fail = 1;

				break;
			}


			//Ensure that both #SK_Yoke's are in a consistent state
			//whether the link creation / updating had succeeded or
			//failed.
			//
			//This includes making sure that duplicate links do not
			//exist in either #SK_Yoke after the linking process.

			int e_flag_0 = 0;
			int e_flag_1 = 0;

			SK_Yoke_error_check
			(
				sky [sky_0_sel],
				stdout,
				"SK_Yoke_link_no_alloc_w_info_tb () : ",
				"\n",
				&e_flag_0
			);

			SK_Yoke_error_check
			(
				sky [sky_1_sel],
				stdout,
				"SK_Yoke_link_no_alloc_w_info_tb () : ",
				"\n",
				&e_flag_1
			);


			if (e_flag_0 || e_flag_1)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"e_flag_0 = %d, e_flag_1 = %d",
					e_flag_0,
					e_flag_1
				);

				tb_fail = 1;

				break;
			}
		}


		//Make sure to propagate loop-breaking if an error occurred

		if (tb_fail)
		{
			break;
		}
	}


	//Attempt to link yokes in the fully connected network to an extra
	//yoke. These attempts should fail, because all of the links in all the
	//yokes in #sky should be allocated at this time.

	for (size_t sky_sel = 0; sky_sel < NUM_NODES; sky_sel += 1)
	{
		for (int dir = 0; dir < 2; dir += 1)
		{
			//Select which #SK_Yoke will act as the source and
			//destination based on the current direction

			SK_Yoke * sky_src = (dir) ? sky_extra : sky [sky_sel];

			SK_Yoke * sky_dst = (dir) ? sky [sky_sel] : sky_extra;


			//Check that connecting #sky_src -> #sky_dst fails
			//properly

			int need_realloc_0 = 0;
			int need_realloc_1 = 0;

			size_t sky_0_l_ind = 0;
			size_t sky_1_l_ind = 0;

			int link_existed = 0;


			SlySr sr =

			SK_Yoke_link_no_alloc_w_info
			(
				sky_src,
				&need_realloc_0,
				&sky_0_l_ind,

				sky_dst,
				&need_realloc_1,
				&sky_1_l_ind,

				&link_existed
			);


			//Check that the function failed whilst indicating #sky
			//[sky_sel] needs reallocation

			if (SLY_SUCCESS == sr.res)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"SK_Yoke_link_no_alloc_w_info () reported success when it "
					"should have failed."
				);

				tb_fail = 1;

				break;
			}


			//If #sky [sky_sel] was the destination, make sure only
			//that reallocation flag was raised.

			if
			(
				(sky_dst == sky [sky_sel])

				&&

				(! ((0 == need_realloc_0) && (1 == need_realloc_1)))
			)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"Wrong need_realloc_* flags. "
					"(need_realloc_0 = %d, need_realloc_1 = %d)",
					need_realloc_0,
					need_realloc_1
				);

				tb_fail = 1;

				break;
			}


			//If #sky [sky_sel] was the source, make sure only that
			//reallocation flag was raised

			if
			(
				(sky_src == sky [sky_sel])

				&&

				(! ((1 == need_realloc_0) && (0 == need_realloc_1)))
			)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"Wrong need_realloc_* flags. "
					"(need_realloc_0 = %d, need_realloc_1 = %d)",
					need_realloc_0,
					need_realloc_1
				);

				tb_fail = 1;

				break;
			}


			//Make sure that #sky [sky_sel] is still in a
			//consistent state after the failed link

			int e_flag = 0;

			SK_Yoke_error_check
			(
				sky [sky_sel],
				stdout,
				"SK_Yoke_link_no_alloc_w_info_tb () : ",
				"\n",
				&e_flag
			);


			if (e_flag)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"e_flag = %d",
					e_flag
				);

				tb_fail = 1;

				break;
			}

		}


		//Make sure to propagate loop-breaking if an error occurred

		if (tb_fail)
		{
			break;
		}

	}


	//Print out the final configuration of all the #SK_Yoke's

	printf ("Printing the final state of all the #SK_Yoke's...\n\n");

	for (size_t n_sel = 0; n_sel < NUM_NODES; n_sel += 1)
	{
		printf ("SK_Yoke #%lu\n\n", (long unsigned) n_sel);

		SK_Yoke_print_opt
		(
			sky [n_sel],
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1
		);

		printf ("\n\n");
	}


	//Deallocate all of the memory used in this test-bench

	SK_Yoke_destroy (sky_extra);

	for (size_t n_sel = 0; n_sel < NUM_NODES; n_sel += 1)
	{
		SK_Yoke_destroy (sky [n_sel]);
	}


	//Check if the test had passed or not

	if (tb_fail)
	{
		printf ("SK_Yoke_link_no_alloc_w_info_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("SK_Yoke_link_no_alloc_w_info_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test bench on SK_Yoke_unlink_w_info ()
 *
 *  @return			Standard status code
 */

SlySr SK_Yoke_unlink_w_info_tb ()
{
	printf ("SK_Yoke_unlink_w_info_tb () : BEGIN\n\n");


	//Create a yoke that will be used as the "extra" yoke later in the
	//test-bench
	//
	//This part is very similar to the beginning of
	//SK_Yoke_link_no_alloc_w_info_tb ()

	SK_Yoke * sky_extra = SK_Yoke_create (1, NULL);

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky_extra, SlySr_get (SLY_BAD_ALLOC));


	//Create some yokes to link together to form a fully-connected network

	const size_t NUM_NODES = 4;

	const size_t NUM_LINKS_PER_NODE = NUM_NODES - 1;


	SK_Yoke * sky [NUM_NODES];

	for (size_t n_sel = 0; n_sel < NUM_NODES; n_sel += 1)
	{
		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);

		sky [n_sel] = SK_Yoke_create (NUM_LINKS_PER_NODE, &sr);


		// Check that #sky was successfully created

		if (SLY_SUCCESS != sr.res)
		{
			//Deallocate any memory used in this function

			SK_Yoke_destroy (sky_extra);

			for (size_t d_sel = 0; d_sel <= n_sel; d_sel += 1)
			{
				SK_Yoke_destroy (sky [d_sel]);
			}


			SK_RES_RETURN (SK_YOKE_L_DEBUG, sr.res, sr);
		}
	}


	//Create a fully-connected network, which will be used later to test
	//unlinking

	int tb_fail = 0;


	for (size_t sky_0_sel = 0; sky_0_sel < NUM_NODES; sky_0_sel += 1)
	{
		for (size_t sky_1_sel = sky_0_sel + 1; sky_1_sel < NUM_NODES; sky_1_sel += 1)
		{

			//Select 2 #SK_Yoke's, and link them together

			printf
			(
				"Linking (sky [%lu]) <-> (sky [%lu])...\n\n",
				(long unsigned) sky_0_sel,
				(long unsigned) sky_1_sel
			);


			SK_Yoke * sky_0 = sky [sky_0_sel];

			SK_Yoke * sky_1 = sky [sky_1_sel];


			SlySr sr =

			SK_Yoke_link_no_alloc_w_info
			(
				sky_0,
				NULL,
				NULL,

				sky_1,
				NULL,
				NULL,

				NULL
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"Could not link together #sky [%lu] "
					"and #sky [%lu].",
					(long unsigned) sky_0_sel,
					(long unsigned) sky_1_sel
				);

				tb_fail = 1;

				break;
			}
		}

		//Propagate loop-breaking when an error occurs

		if (tb_fail)
		{
			break;
		}
	}


	//At first, try unlinking #sky_extra from every #SK_Yoke in #sky. All
	//of these attempts should accomplish nothing, because it was never
	//linked to any of these yokes.

	for (size_t sky_sel = 0; sky_sel < NUM_NODES; sky_sel += 1)
	{
		for (int extra_is_src = 0; extra_is_src < 2; extra_is_src += 1)
		{

			//Early exit if error detected

			if (tb_fail)
			{
				break;
			}


			//Request that #sky_extra be unlinked from #sky [sky_sel],
			//and ensure that this attempt accomplishes nothing

			SK_Yoke * src = (extra_is_src) ? sky_extra : sky [sky_sel];

			SK_Yoke * dst = (extra_is_src) ? sky [sky_sel] : sky_extra;


			int l_exists =		0;

			size_t l_ind_0 =	0;
			size_t l_ind_1 =	0;


			//SlyDr dr =

			SK_Yoke_unlink_w_info
			(
				src,
				dst,
				&l_exists,
				&l_ind_0,
				&l_ind_1
			);


			if ((0 != l_exists) || (0 != l_ind_0) || (0 != l_ind_1))
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"Performing unlink with #sky_extra did "
					"not act as a no-op."
					"sky_sel =      %lu\n"
					"extra_is_src = %d\n"
					"l_exists =     %d\n"
					"l_ind_0 =      %lu\n"
					"l_ind_1 =      %lu\n",
					sky_sel,
					extra_is_src,
					l_exists,
					l_ind_0,
					l_ind_1
				);

				tb_fail = 1;

				break;
			}


			//Check if either #SK_Yoke in the above unlinking
			//attempt have somehow entered an inconsistent state

			int e_flag_src = 0;
			int e_flag_dst = 0;

			SK_Yoke_error_check
			(
				src,
				stdout,
				"SK_Yoke_unlink_w_info_tb () : ",
				"\n",
				&e_flag_src
			);

			SK_Yoke_error_check
			(
				dst,
				stdout,
				"SK_Yoke_unlink_w_info_tb () : ",
				"\n",
				&e_flag_dst
			);

			if (e_flag_src || e_flag_dst)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"sky_sel =     	%lu\n"
					"extra_is_src =	%d\n"
					"e_flag_src =	%d\n"
					"e_flag_dst =	%d\n",
					(size_t) sky_sel,
					extra_is_src,
					e_flag_src,
					e_flag_dst
				);

				tb_fail = 1;

				break;
			}
		}


		//Propagate loop-breaking if an error occurred

		if (tb_fail)
		{
			break;
		}
	}


	//Start unlinking all the yokes in the fully-connected network
	//one-by-one. After each unlink, check that none of the yokes are in an
	//inconsistent state.

	for (size_t sky_0_sel = 0; sky_0_sel < NUM_NODES; sky_0_sel += 1)
	{
		for (size_t sky_1_sel = sky_0_sel + 1; sky_1_sel < NUM_NODES; sky_1_sel += 1)
		{

			//Early exit if error detected

			if (tb_fail)
			{
				break;
			}


			//Unlink 2 yokes inside of the fully-connected network

			printf
			(
				"Unlinking (sky [%lu]) <-> (sky [%lu])...\n\n",
				(long unsigned) sky_0_sel,
				(long unsigned) sky_1_sel
			);


			SK_Yoke * sky_0 = sky [sky_0_sel];

			SK_Yoke * sky_1 = sky [sky_1_sel];


			int l_exists =		0;

			size_t l_ind_0 =	0;
			size_t l_ind_1 =	0;


			//SlyDr dr =

			SK_Yoke_unlink_w_info
			(
				sky_0,
				sky_1,
				&l_exists,
				&l_ind_0,
				&l_ind_1
			);


			//Check that #l_exists, #l_ind_0, and #l_ind_1 were
			//reported correctly

			if (! l_exists)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"#l_exists = %0d for #sky [%lu] and #sky [%lu]",
					l_exists,
					(size_t) sky_0_sel,
					(size_t) sky_1_sel
				);

				tb_fail = 1;

				break;
			}

			if (SK_YokeLink_is_active (sky_0->links [l_ind_0]))
			{
				SK_DEBUG_PRINT
				(
				 	SK_YOKE_L_DEBUG,
					"sky [%lu]->links [%lu] was not reset "
					"properly",
					(size_t) sky_0_sel,
					(size_t) l_ind_0
				);

				tb_fail = 1;

				break;
			}

			if (SK_YokeLink_is_active (sky_1->links [l_ind_1]))
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"sky [%lu]->links [%lu] was not reset "
					"properly",
					(size_t) sky_1_sel,
					(size_t) l_ind_1
				);

				tb_fail = 1;

				break;
			}


			//Check if either #SK_Yoke in the above unlinking
			//attempt have somehow entered an inconsistent state

			int e_flag_0 = 0;
			int e_flag_1 = 0;

			SK_Yoke_error_check
			(
				sky_0,
				stdout,
				"SK_Yoke_unlink_w_info_tb () : ",
				"\n",
				&e_flag_0
			);

			SK_Yoke_error_check
			(
				sky_1,
				stdout,
				"SK_Yoke_unlink_w_info_tb () : ",
				"\n",
				&e_flag_1
			);

			if (e_flag_0 || e_flag_1)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"sky_sel_0 =    %lu\n"
					"sky_sel_1 =    %lu\n"
					"e_flag_0 =     %d\n"
					"e_flag_1 =     %d\n",
					(size_t) sky_0_sel,
					(size_t) sky_1_sel,
					e_flag_0,
					e_flag_1
				);

				tb_fail = 1;

				break;
			}
		}


		//Propagate loop-breaking if an error occurred

		if (tb_fail)
		{
			break;
		}
	}


	//Print out the final configuration of all the #SK_Yoke's

	printf ("Printing the final state of all the #SK_Yoke's...\n\n");

	for (size_t n_sel = 0; n_sel < NUM_NODES; n_sel += 1)
	{
		printf ("SK_Yoke #%lu\n\n", (long unsigned) n_sel);

		SK_Yoke_print_opt
		(
			sky [n_sel],
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1
		);

		printf ("\n\n");
	}


	//Deallocate all of the memory used in this test-bench

	SK_Yoke_destroy (sky_extra);

	for (size_t n_sel = 0; n_sel < NUM_NODES; n_sel += 1)
	{
		SK_Yoke_destroy (sky [n_sel]);
	}


	//Check if the test had passed or not

	if (tb_fail)
	{
		printf ("SK_Yoke_unlink_w_info_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("SK_Yoke_unlink_w_info_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);

}




/*!
 *  Performs a simple test bench on SK_Yoke_links_realloc ()
 *
 *  @return			Standard status code
 */

//FIXME : This function seems a bit long

SlySr SK_Yoke_links_realloc_tb ()
{
	printf ("SK_Yoke_links_realloc_tb () : BEGIN\n\n");


	//FIXME : The following code section is repeated through multiple
	//test-benches, consider making a function SK_Yoke_fully_conn_net ()
	//for establishing fully-connected networks

	//Create some yokes to link together to form a fully-connected network

	const size_t NUM_NODES = 4;

	const size_t NUM_LINKS_PER_NODE = NUM_NODES - 1;


	SK_Yoke * sky [NUM_NODES];


	for (size_t n_sel = 0; n_sel < NUM_NODES; n_sel += 1)
	{
		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);

		sky [n_sel] = SK_Yoke_create (NUM_LINKS_PER_NODE, &sr);


		// Check that #sky was successfully created

		if (SLY_SUCCESS != sr.res)
		{
			//Deallocate any memory used in this function

			for (size_t d_sel = 0; d_sel <= n_sel; d_sel += 1)
			{
				SK_Yoke_destroy (sky [d_sel]);
			}


			SK_RES_RETURN (SK_YOKE_L_DEBUG, sr.res, sr);
		}
	}


	//Create a fully-connected network, which will be used later to test
	//unlinking resulting from resizing

	int tb_fail = 0;


	for (size_t sky_0_sel = 0; sky_0_sel < NUM_NODES; sky_0_sel += 1)
	{
		for (size_t sky_1_sel = sky_0_sel + 1; sky_1_sel < NUM_NODES; sky_1_sel += 1)
		{

			//Select 2 #SK_Yoke's, and link them together

			printf
			(
				"Linking (sky [%lu]) <-> (sky [%lu])...\n\n",
				(long unsigned) sky_0_sel,
				(long unsigned) sky_1_sel
			);


			SK_Yoke * sky_0 = sky [sky_0_sel];

			SK_Yoke * sky_1 = sky [sky_1_sel];


			SlySr sr =

			SK_Yoke_link_no_alloc_w_info
			(
				sky_0,
				NULL,
				NULL,

				sky_1,
				NULL,
				NULL,

				NULL
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"Could not link together #sky [%lu] "
					"and #sky [%lu].",
					(long unsigned) sky_0_sel,
					(long unsigned) sky_1_sel
				);

				tb_fail = 1;

				break;
			}
		}

		//Propagate loop-breaking when an error occurs

		if (tb_fail)
		{
			break;
		}
	}


	//Print the state of every #SK_Yoke before reallocating their links

	printf ("Printing starting state of all the #SK_Yoke's...\n\n");

	for (size_t n_sel = 0; n_sel < NUM_NODES; n_sel += 1)
	{
		printf ("SK_Yoke #%lu\n\n", (long unsigned) n_sel);

		SK_Yoke_print_opt
		(
			sky [n_sel],
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1
		);

		printf ("\n\n");
	}


	//Test changing the number of links in the yokes while preserving as
	//many links as possible
	//
	//Make sure that increasing the size is tested FIRST, so that it can
	//easily be checked that links aren't unnecessarily removed.

	for (int decr_size = 0; decr_size < 2; decr_size += 1)
	{
		for (size_t sky_sel = 0; sky_sel < NUM_NODES; sky_sel += 1)
		{

			//Early exit if error detected

			if (tb_fail)
			{
				break;
			}


			//Print what test iteration is occurring

			printf
			(
				"Reallocating links for #sky [%lu] with "
				"(decr_size = %d)...\n\n",
				sky_sel,
				decr_size
			);


			//Before performing any resize operation, count the
			//number of unused links in #sky [sky_sel]

			size_t pre_avail_l_cnt = 0;

			for (size_t l_sel = 0; l_sel < sky [sky_sel]->links_len; l_sel += 1)
			{
				if (SK_YokeLink_is_inactive (sky [sky_sel]->links [l_sel]))
				{
					pre_avail_l_cnt += 1;
				}
			}


			//Select an #SK_Yoke and increase its size by #sky_sel

			size_t resize_len = 0;


			if (decr_size)
			{
				resize_len = NUM_LINKS_PER_NODE - 1;
			}

			else
			{
				resize_len = NUM_LINKS_PER_NODE + sky_sel;
			}


			SlySr sr =

			SK_Yoke_links_realloc
			(
				sky [sky_sel],
				resize_len,
				1
			);


			//Count the number of unused links after the
			//reallocation

			size_t post_avail_l_cnt = 0;

			for (size_t l_sel = 0; l_sel < sky [sky_sel]->links_len; l_sel += 1)
			{
				if (SK_YokeLink_is_inactive (sky [sky_sel]->links [l_sel]))
				{
					post_avail_l_cnt += 1;
				}
			}


			//Ensure that the resize operation reported success

			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"SK_Yoke_links_realloc () did not return success "
					"for resizing #sky [%lu] to have %lu links.",
					(long unsigned) sky_sel,
					(long unsigned) resize_len
				);

				tb_fail = 1;

				break;
			}


			//Ensure that the resize operation actually updated
			//#sky [sky_sel]

			if (resize_len != sky [sky_sel]->links_len)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"The size of #sky [%lu] somehow became %lu.",
					(long unsigned) sky_sel,
					(long unsigned) sky [sky_sel]->links_len
				);

				tb_fail = 1;

				break;
			}


			//Ensure that the link reallocation ended with a
			//consistent number of links being available in
			//#sky [sky_sel]

			if (decr_size)
			{
				if
				(
					(pre_avail_l_cnt <= post_avail_l_cnt)

					&&

					(pre_avail_l_cnt != 0)

				)
				{
					SK_DEBUG_PRINT
					(
						SK_YOKE_L_DEBUG,
						"Decreasing the size of #sky "
						"[%lu] somehow avoided "
						"decreasing the number of "
						"links available.\n"
						"resize_len =       %lu\n"
						"pre_avail_l_cnt =  %lu\n"
						"post_avail_l_cnt = %lu\n",
						sky_sel,
						resize_len,
						pre_avail_l_cnt,
						post_avail_l_cnt
					);

					tb_fail = 1;

					break;
				}
			}

			else
			{
				if (sky_sel != (post_avail_l_cnt - pre_avail_l_cnt))
				{
					SK_DEBUG_PRINT
					(
						SK_YOKE_L_DEBUG,
						"Increasing the size of #sky "
						"[%lu] did not produce %lu "
						"unused links.",
						sky_sel,
						sky_sel
					);

					tb_fail = 1;

					break;
				}
			}


			//Attempt to detect if SK_Yoke_links_realloc () somehow
			//placed #sky [sky_sel] into an inconsistent state

			int e_flag = 0;

			SK_Yoke_error_check
			(
				sky [sky_sel],
				stdout,
				"SK_Yoke_links_realloc_tb () : ",
				"\n",
				&e_flag
			);


			if (e_flag)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"sky_sel = %lu\n"
					"e_flag =  %lu\n",
					sky_sel,
					e_flag
				);

				tb_fail = 1;

				break;
			}

		}


		//Print the state of every #SK_Yoke after reallocating the
		//links

		printf
		(
			"Printing all the #SK_Yoke's after reallocating links "
			"with (decr_size = %d)...\n\n",
			decr_size
		);

		for (size_t n_sel = 0; n_sel < NUM_NODES; n_sel += 1)
		{
			printf ("SK_Yoke #%lu\n\n", (long unsigned) n_sel);

			SK_Yoke_print_opt
			(
				sky [n_sel],
				stdout,
				SK_LineDeco_std (),
				1,
				1,
				1
			);

			printf ("\n\n");
		}


		//Propagate loop-breaking if an error occurred

		if (tb_fail)
		{
			break;
		}
	}


	//Test resizing #SK_Yoke's _without_ preserving any existing links

	for (size_t sky_sel = 0; sky_sel < NUM_NODES; sky_sel += 1)
	{

		//Early exit if error detected

		if (tb_fail)
		{
			break;
		}


		//Print what test iteration is occurring

		printf
		(
			"Reallocating links for #sky [%lu] to %lu links "
			"WITHOUT preserving links...\n\n",
			sky_sel,
			sky_sel
		);


		//Select an #SK_Yoke and reallocate its links without
		//preserving anything

		size_t resize_len = sky_sel;


		SlySr sr =

		SK_Yoke_links_realloc
		(
			sky [sky_sel],
			resize_len,
			0
		);


		//Count the number of unused links after the reallocation

		size_t post_avail_l_cnt = 0;

		for (size_t l_sel = 0; l_sel < sky [sky_sel]->links_len; l_sel += 1)
		{
			if (SK_YokeLink_is_inactive (sky [sky_sel]->links [l_sel]))
			{
				post_avail_l_cnt += 1;
			}
		}


		//Ensure that the resize operation reported success

		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"SK_Yoke_links_realloc () did not return success "
				"for resizing #sky [%lu] to have %lu links.",
				(long unsigned) sky_sel,
				(long unsigned) resize_len
			);

			tb_fail = 1;

			break;
		}


		//Ensure that the resize operation actually updated
		//#sky [sky_sel]

		if (resize_len != sky [sky_sel]->links_len)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"The size of #sky [%lu] somehow became %lu.",
				(long unsigned) sky_sel,
				(long unsigned) sky [sky_sel]->links_len
			);

			tb_fail = 1;

			break;
		}


		//Ensure that all the links in #sky [sky_sel] are available,
		//because none have been preserved

		if (sky_sel != post_avail_l_cnt)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"sky [%lu] did not have %lu available links.",
				sky_sel,
				resize_len
			);

			tb_fail = 1;

			break;
		}


		//Attempt to detect if SK_Yoke_links_realloc () somehow
		//placed #sky [sky_sel] into an inconsistent state

		int e_flag = 0;

		SK_Yoke_error_check
		(
			sky [sky_sel],
			stdout,
			"SK_Yoke_links_realloc_tb () : ",
			"\n",
			&e_flag
		);


		if (e_flag)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"sky_sel = %lu\n"
				"e_flag =  %lu\n",
				sky_sel,
				e_flag
			);

			tb_fail = 1;

			break;
		}
	}


	//Print the state of every #SK_Yoke after reallocating the
	//links

	printf
	(
		"Printing all the #SK_Yoke's after reallocating links without "
		"preserving any links...\n\n"
	);

	for (size_t n_sel = 0; n_sel < NUM_NODES; n_sel += 1)
	{
		printf ("SK_Yoke #%lu\n\n", (long unsigned) n_sel);

		SK_Yoke_print_opt
		(
			sky [n_sel],
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1
		);

		printf ("\n\n");
	}


	//Deallocate all of the memory used in this test-bench

	for (size_t sky_sel = 0; sky_sel < NUM_NODES; sky_sel += 1)
	{
		SK_Yoke_destroy (sky [sky_sel]);
	}


	//Check if the test had passed or not

	if (tb_fail)
	{
		printf ("SK_Yoke_links_realloc_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("SK_Yoke_links_realloc_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);

}




/*!
 *  Performs a simple test-bench on SK_Yoke_link_w_info ()
 *
 *  @return			Standard status code
 */

SlySr SK_Yoke_link_w_info_tb ()
{
	printf ("SK_Yoke_link_w_info_tb () : BEGIN\n\n");


	//This test-bench will test SK_Yoke_link_w_info_tb () by using it
	//to create a fully connected network using SK_Yoke's that initially
	//have no links allocated inside of them


	//Set some constants to use throughout the test-bench

	const size_t NUM_YOKES =	16;

	const size_t MAX_INCR =		10;


	//Create a set of #SK_Yoke's that initially have no links allocated in
	//them

	SK_Yoke * sky [NUM_YOKES];


	for (size_t y_sel = 0; y_sel < NUM_YOKES; y_sel += 1)
	{
		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);

		sky [y_sel] = SK_Yoke_create (0, &sr);


		//Check that #sky [y_sel] was successfully created

		if (SLY_SUCCESS != sr.res)
		{

			//Deallocate any memory used in this function

			for (size_t d_sel = 0; d_sel <= y_sel; d_sel += 1)
			{
				SK_Yoke_destroy (sky [d_sel]);
			}


			SK_RES_RETURN (SK_YOKE_L_DEBUG, sr.res, sr);
		}
	}


	//Create a fully-connected network out of the #SK_Yoke's that initially
	//have no links allocated in them.
	//
	//Note that #sky_1_sel gets initialized with 0 instead of
	//(sky_0_sel + 1). This makes it so that testing duplicate links gets
	//tested.

	int error_detected = 0;


	for (size_t sky_0_sel = 0; sky_0_sel < NUM_YOKES; sky_0_sel += 1)
	{
		for (size_t sky_1_sel = 0; sky_1_sel < NUM_YOKES; sky_1_sel += 1)
		{
			//Skip indices that would lead to attempts to link an
			//#SK_Yoke to itself

			if (sky_0_sel == sky_1_sel)
			{
				continue;
			}


			//Record the number of links active / allocated inside
			//of both selected #SK_Yoke's

			size_t sky_0_old_links_len = sky [sky_0_sel]->links_len;

			size_t sky_1_old_links_len = sky [sky_1_sel]->links_len;

			size_t sky_0_old_links_cnt = sky [sky_0_sel]->links_cnt;

			size_t sky_1_old_links_cnt = sky [sky_1_sel]->links_cnt;


			//Determine a semi-random value to use as #sky_0_incr
			//and #sky_1_incr, which is how much the number of
			//allocated links should increase by if a reallocation
			//occurs in SK_Yoke_link_w_info ()
			//
			//Note that we avoid increments of 0, so that the size
			//is allowed to increase when needed.

			size_t sky_0_incr =

			(SK_gold_hash_u64 (sky_0_sel + sky_1_sel) % MAX_INCR)

			+

			1;


			size_t sky_1_incr =

			(SK_gold_hash_u64 (sky_1_sel + sky_0_incr) % MAX_INCR)

			+

			1;


			//Establish a link between #sky [sky_0_sel] and
			//#sky [sky_1_sel]

			size_t sky_0_l_ind =	0;

			size_t sky_1_l_ind =	0;

			int link_existed =	0;


			SlySr sr

			=

			SK_Yoke_link_w_info
			(
				sky [sky_0_sel],
				&sky_0_l_ind,
				sky_0_incr,

				sky [sky_1_sel],
				&sky_1_l_ind,
				sky_1_incr,

				&link_existed
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"For #sky_0_sel = %zu and "
					"#sky_1_sel = %zu, sr.res = %s",
					sky_0_sel,
					sky_1_sel,
					SlySrStr_get (sr).str
				);

				error_detected = 1;

				break;
			}


			//Print out the linked together #SK_Yoke's

			printf
			(
				"Linked #sky [%zu] <--> #sky [%zu]\n\n",
				sky_0_sel,
				sky_1_sel
			);


			SK_Yoke_print_opt
			(
				sky [sky_0_sel],
				stdout,
				SK_LineDeco_std (),
				1,
				1,
				1
			);


			SK_Yoke_print_opt
			(
				sky [sky_1_sel],
				stdout,
				SK_LineDeco_std (),
				1,
				1,
				1
			);


			//Check if #link_existed was reported correctly. Keep
			//in mind that this is a fully-connected network
			//created "in-order", which is why comparing to these
			//indices works.

			if
			(
				((sky_0_sel < sky_1_sel) && (link_existed))

				||

				((sky_0_sel > sky_1_sel) && (0 == link_existed))
			)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"#link_existed = %d was wrong for this linking attempt.",
					link_existed
				);

				error_detected = 1;

				break;
			}


			//Check if the number of links allocated in the linked
			//#SK_Yoke's increased correctly and only when the link
			//did not already exist

			if
			(
				(link_existed)

				&&

				(
					(sky_0_old_links_len != sky [sky_0_sel]->links_len)

					||

					(sky_1_old_links_len != sky [sky_1_sel]->links_len)
				)
			)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"#sky [%zu]->links_len = %zu, "
					"#sky [%zu]->links_len = %zu "
					"when it should have been %zu and %zu",
					sky_0_sel,
					sky [sky_0_sel]->links_len,
					sky_1_sel,
					sky [sky_1_sel]->links_len,
					sky_0_old_links_len,
					sky_1_old_links_len
				);

				error_detected = 1;

				break;
			}

			else if
			(

				(0 == link_existed)

				&&

				(sky_0_old_links_cnt == sky_0_old_links_len)

				&&

				((sky_0_old_links_len + sky_0_incr) != sky [sky_0_sel]->links_len)
			)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"#sky [%zu]->links_len = %zu, "
					"when it should have been %zu",
					sky_0_sel,
					sky [sky_0_sel]->links_len,
					sky_0_old_links_len + sky_0_incr
				);

				error_detected = 1;

				break;
			}

			else if
			(
				(0 == link_existed)

				&&

				(sky_1_old_links_cnt == sky_1_old_links_len)

				&&

				((sky_1_old_links_len + sky_1_incr) != sky [sky_1_sel]->links_len)
			)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"#sky [%zu]->links_len = %zu, "
					"when it should have been %zu",
					sky_1_sel,
					sky [sky_1_sel]->links_len,
					sky_1_old_links_len + sky_1_incr
				);

				error_detected = 1;

				break;
			}


			//Ensure that both #SK_Yoke's are in a consistent state
			//after this linking attempt.

			int e_flag_0 = 0;
			int e_flag_1 = 0;

			SK_Yoke_error_check
			(
				sky [sky_0_sel],
				stdout,
				"SK_Yoke_link_w_info_tb () : ",
				"\n",
				&e_flag_0
			);

			SK_Yoke_error_check
			(
				sky [sky_0_sel],
				stdout,
				"SK_Yoke_link_w_info_tb () : ",
				"\n",
				&e_flag_1
			);


			if (e_flag_0 || e_flag_1)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"#e_flag_0 = %d, #e_flag_1 = %d",
					e_flag_0,
					e_flag_1
				);

				error_detected = 1;

				break;
			}
		}


		//Make sure to propagate loop-breaking if an error occurred

		if (error_detected)
		{
			break;
		}
	}


	//Print out the final configuration of all the #SK_Yoke's

	printf ("Printing the final state of all the #SK_Yoke's...\n\n");

	for (size_t y_sel = 0; y_sel < NUM_YOKES; y_sel += 1)
	{
		printf ("SK_Yoke #%zu\n\n", y_sel);

		SK_Yoke_print_opt
		(
			sky [y_sel],
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1
		);

		printf ("\n\n");
	}


	//Deallocate all of the memory used in this test-bench

	for (size_t y_sel = 0; y_sel < NUM_YOKES; y_sel += 1)
	{
		SK_Yoke_destroy (sky [y_sel]);
	}


	//Check if the test had passed or not

	if (error_detected)
	{
		printf ("SK_Yoke_link_w_info_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("SK_Yoke_link_w_info_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




