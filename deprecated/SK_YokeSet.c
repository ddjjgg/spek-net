/*!****************************************************************************
 *
 * @file
 * SK_YokeSet.c
 *
 * Contains an implementation of #SK_YokeSet, which is a collection of
 * #SK_Yoke's. The benefit of having them placed into a collection is that they
 * can be updated in a multi-threaded manner.
 *
 *****************************************************************************/




#include <omp.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "./SK_debug.h"
#include "./SK_misc.h"
#include "./SK_numeric_types.h"
#include "./SK_Vector.h"
#include "./SK_Yoke.h"




#include "./SK_YokeSet.h"



//  FIXME : #SK_YokeSet code is entirely disabled until #SK_YokeIntf is
//  finished, that way #SK_YokeSet can be properly developed against that
//  interface.
//
//
//
//
//  /*!
//   *  An op-code used to represent an operation on an #SK_YokeSet.
//   */
//
//  typedef enum SK_YOKE_SET_OP
//  {
//  	SK_YOKE_SET_NO_OP,
//
//  	SK_YOKE_SET_LINK,
//
//  	SK_YOKE_SET_UNLINK,
//
//
//  	//FIXME : Should #SK_YOKE_SET_LINK_ALL be an op-code? This seems like
//  	//something that could wreak havoc on large network simulations.
//
//  	SK_YOKE_SET_LINK_ALL,
//
//
//  	SK_YOKE_SET_UNLINK_ALL
//  }
//
//  SK_YOKE_SET_OP;
//
//
//
//
//  /*!
//   *  Represents an operation to perform on 2 #SK_Yoke's contained within an
//   *  #SK_YokeSet.
//   *
//   *  In particular, this will be used to order the linking / unlinking of 2
//   *  #SK_Yoke's.
//   */
//
//  typedef struct SK_YokeSet2Op
//  {
//
//  	//Represents the operations that should be carried out on #y0
//  	//and #y1
//
//  	SK_YOKE_SET_OP op;
//
//
//  	//The index of the 0th operand in the #SK_YokeSet
//
//  	size_t y0;
//
//
//  	//The index of the 1st operand in the #SK_YokeSet
//
//  	size_t y1;
//
//  }
//
//  SK_YokeSet2Op;
//
//
//
//
//  /*!
//   *  Represents a collection of operations that should be applied to an
//   *  #SK_YokeSet in a particular order.
//   *
//   *  FIXME : Will there ever be an #SK_YokeSet operation other than
//   *  #SK_YokeSet2Op? If so, it will be necesary to use another array / vector
//   *  in this data structure to indicate the type of the Nth instruction
//   *  (SK_YokeSet1Op, SK_YokeSet2Op, SK_YokeSet3Op, etc.). Or maybe this could be
//   *  a singular #SK_Vector where the element size is (max (SK_YokeSet1Op,
//   *  SK_YokeSet2Op, SK_YokeSet3Op, ...)).
//   *
//   *  FIXME : This structure seems very simple; is it worth using the PIMPL
//   *  pattern here? Decide this matter later when this has seen some light usage.
//   */
//
//  typedef struct SK_YokeSetOpSet
//  {
//
//  	//An #SK_Vector containing #SK_YokeSet2Op's. Since this is currently
//  	//the only type of operation for an #SK_YokeSet, the order each
//  	//operation should be applied is directly equal to its index in this
//  	//#SK_Vector.
//
//  	SK_Vector * ys2op;
//  }
//
//  SK_YokeSetOpSet;
//
//
//
//
//  /*!
//   *  Represents a collection of #SK_Yoke's that can be updated via groups of
//   *  linking/unlinking operations.
//   *
//   *  The benefit of having an explicit collection of #SK_Yoke's and issuing
//   *  linking/unlinking commands in groups is that multiple threads can be used
//   *  to update the network. In this way, processing a network of #SK_Yoke's can
//   *  complete faster.
//   *
//   *  An important feature of this data structure is that when lists of
//   *  linking/unlinking operations are provided, they will leave the #SK_Yoke
//   *  network in the same final state regardless of how many threads are
//   *  operating inside the #SK_YokeSet.
//   *
//   *  @warning
//   *  There is an important distinction to understand about the mulit-threading
//   *  with this data structure. Internally, multiple threads are used to update
//   *  the #SK_Yoke's. However, THE #SK_YokeSet ITSELF STILL EXPECTS ONLY ONE
//   *  THREAD TO CALL FUNCTIONS ON IT AT A TIME.
//   *
//   *  @warning
//   *  This data structure requires that each thread "claim/unclaim" ownership of
//   *  #SK_Yoke's in a synchronized fashion, and as a result, each thread checks
//   *  what every other thread is working on before claiming an #SK_Yoke for
//   *  itself. This can lead to diminishing returns as the number of threads grows
//   *  large, as the time spent checking ownership will grow larger.
//   *
//   *  An "Async" (asynchronous) version of this data structure could avoid this
//   *  problem by having a lock per #SK_Yoke, so that a thread only has to wait
//   *  for that lock to be freed before working on it (it doesn't care if other
//   *  threads are claiming/unclaiming other #SK_Yoke's at that time). Doing locks
//   *  per #SK_Yoke like that would allow for the linking/unlinking operations to
//   *  be completed out-of-order though, because of cases where more than 1 thread
//   *  is waiting for a given #SK_Yoke to be unlocked. (Maybe sequence numbers can
//   *  be used to work-around that?). An asynchronous collection of #SK_Yoke's
//   *  could be useful for approximate networks, where most of the connections are
//   *  correctly made and some are incorrect. Perhaps that can be combined with
//   *  some form of self-correction?
//   */
//
//
//  //FIXME : Is it necessary to have an #SK_YokeSet and a #SK_YokeSetAsync; or
//  //should the synchronous/asynchronous nature of the command list processing
//  //be determined by SK_YokeSet_*_sync / SK_YokeSet_*_async () functions?
//
//
//  typedef struct SK_YokeSet
//  {
//  	/// Indicates the number of #SK_Yoke's that are stored in this
//  	/// #SK_YokeSet
//
//  	size_t			num_yokes;
//
//
//  	/// Contains pointers to the #SK_Yoke's stored in this set
//
//  	SK_Vector *		yokes;
//
//
//  	/// The minimum number of commands that should be in a
//  	/// #SK_YokeSetOpSet before deciding to use multi-threading to
//  	/// process the list.
//
//  	size_t			mt_min_op;
//
//
//
//
//  	/*
//
//
//  	Unsure right now if these members will be needed in this struct when
//  	OpenMP is used to process linking/unlinking lists.
//
//  	It is thought that instead with OpenMP, the synchronization primitives
//  	can be placed into the processing functions themselves.
//
//  	If it turns out that this structure actually does not need these to be
//  	stored in the object itself, rename this data structure to #SK_YokeSet.
//
//
//
//
//  	/// The number of threads that will be used to perform updates on
//  	/// #yokes.
//
//  	size_t			num_threads;
//
//
//  	/// A lock indicating if a thread may now claim/unclaim ownership of an
//  	/// #SK_Yoke right now.
//  	///
//  	/// This may equivalently be thought of as a lock on #thread_op_sky_0
//  	/// and thread_op_sky_1 for reading/writing.
//
//  	omp_lock_t		thread_op_lock;
//
//
//  	/// Indicates which #SK_Yoke a particular thread owns as its 1st
//  	/// operand.
//  	///
//  	/// If the #N-th thread has claimed #yokes [#i], then
//  	/// #thread_op_sky_0 [N] will be equal to #i.
//
//  	size_t *		thread_op_sky_0;
//
//
//  	/// Indicates which #SK_Yoke a particular thread owns as its 2nd
//  	/// operand. This follows the same conventions as #thread_op_sky_0.
//
//  	size_t *		thread_op_sky_1;
//
//
//  	/// Indicates if the corresponding values in #thread_op_sky_0 and
//  	/// #thread_op_sky_1 are actually in use, or are just stale values
//
//  	int *			thread_op_sky_valid;
//
//  	*/
//
//  }
//
//  SK_YokeSet;
//
//
//
//
//  /*!
//   *  Creates an #SK_YokeSet that contains a specified number of #SK_Yoke's,
//   *
//   *  @warning
//   *  Some time after calling this function, SK_YokeSet_destroy () must be
//   *  called on the returned pointer, otherwise a memory leak occur.
//   *
//   *  @param num_yokes			The number of #SK_Yoke's the
//   *					#SK_YokeSet should contain initially
//   *
//   *  @param links_per_yoke		The number of links that should be
//   *					allocated for each #SK_Yoke initially.
//   *					Normally, it is desirable to have this
//   *					be equal or greater than the average
//   *					number of links per node in the
//   *					network.
//   *
//   *  @param mt_min_op			Initial value for the minimum number
//   *  					of commands that should be inside of
//   *  					a linking / unlinking command list for
//   *  					multi-threading to be used.
//   *
//   *  @param yokes_elems_per_block	The #elems_per_block argument to use
//   *					with SK_Vector_create () when creating
//   *					#yokes
//   *
//   *  @param yokes_neg_slack		The #neg_slack argument to use with
//   *					SK_Vector_create () when creating
//   *					#yokes
//   *
//   *  @param yokes_pos_slack		The #pos_slack argument to use with
//   *					SK_Vector_create () when creating
//   *					#yokes
//   *
//   *  @param sr_ptr			Where to place a #SlySr value
//   *					indicating the success of this
//   *					function. This CAN be NULL if this
//   *					information is not desired.
//   *
//   *  @return				A pointer to the newly created
//   *					#SK_YokeSet object upon success;
//   *					NULL is returned otherwise.
//   */
//
//  SK_YokeSet * SK_YokeSet_create_opt
//  (
//  	size_t		num_yokes,
//  	size_t		links_per_yoke,
//  	size_t		mt_min_op,
//  	size_t		yokes_elems_per_block,
//  	size_t		yokes_neg_slack,
//  	size_t		yokes_pos_slack,
//  	SlySr *		sr_ptr
//  )
//  {
//
//  	//Allocate space for the #SK_YokeSet itself
//
//  	SK_YokeSet * ys = malloc (sizeof (SK_YokeSet));
//
//  	if (NULL == ys)
//  	{
//  		//Report failure
//
//  		SK_DEBUG_PRINT
//  		(
//  			SK_YOKE_SET_L_DEBUG,
//  			"Could not allocate #ys"
//  		);
//
//
//  		//Return failure
//
//  		if (NULL != sr_ptr)
//  		{
//  			*sr_ptr = SlySr_get (SLY_BAD_ALLOC);
//  		}
//
//  		return NULL;
//  	}
//
//
//  	//Create an #SK_Vector to hold pointers to the #SK_Yoke's
//  	//
//  	//FIXME : Create an initialization function for #SK_Yoke's so that
//  	//SK_Vector_init_zero () doesn't have to be used here, because it is a
//  	//slow function. Also, it makes it so that the elements in #ys->yokes
//  	//can't immediately be set to useful values.
//
//  	SlySr sr =			SlySr_get (SLY_UNKNOWN_ERROR);
//
//  	size_t init_func_elem_size =	sizeof (SK_Yoke *);
//
//
//  	ys->num_yokes =	num_yokes;
//
//
//  	ys->yokes =
//
//  	SK_Vector_create
//  	(
//  		sizeof (SK_Yoke *),
//  		num_yokes,
//  		yokes_elems_per_block,
//  		yokes_neg_slack,
//  		yokes_pos_slack,
//  		SK_Vector_init_zero,
//  		&init_func_elem_size,
//  		&sr
//  	);
//
//
//  	if (SLY_SUCCESS != sr.res)
//  	{
//  		//Report failure
//
//  		SK_DEBUG_PRINT
//  		(
//  			SK_YOKE_SET_L_DEBUG,
//  			"Failed to allocate #ys->yokes"
//  		);
//
//
//  		//Free any memory allocated in this function so far
//
//  		free (ys);
//
//
//  		//Return failure
//
//  		if (NULL != sr_ptr)
//  		{
//  			*sr_ptr = sr;
//  		}
//
//  		return NULL;
//  	}
//
//
//  	//Create #SK_Yoke's, and place them into #ys->yokes
//
//  	size_t last_created_yoke = 0;
//
//
//  	for (size_t y_sel = 0; y_sel < num_yokes; y_sel += 1)
//  	{
//
//  		//Create the #SK_Yoke
//
//  		SK_Yoke * yoke = SK_Yoke_create (links_per_yoke, &sr);
//
//  		if (SLY_SUCCESS != sr.res)
//  		{
//  			break;
//  		}
//
//
//  		//Keep track of how many #SK_Yoke's have been successfully
//  		//created
//
//  		last_created_yoke += 1;
//
//
//  		//Place the #SK_Yoke into #ys->yokes
//
//  		SK_Vector_set_elem
//  		(
//  			ys->yokes,
//  			y_sel,
//  			(u8 *) yoke,
//  			sizeof (SK_Yoke *)
//  		);
//  	}
//
//
//  	if (SLY_SUCCESS != sr.res)
//  	{
//
//  		//Report failure
//
//  		SK_DEBUG_PRINT
//  		(
//  			SK_YOKE_SET_L_DEBUG,
//  			"Could not allocate #SK_Yoke at index %zu in #ys->yokes",
//  			last_created_yoke
//  		);
//
//
//  		//Free any memory allocated in this function so far
//
//  		for (size_t y_sel = 0; y_sel < last_created_yoke; y_sel += 1)
//  		{
//  			SK_Yoke * yoke = NULL;
//
//
//  			SK_Vector_get_elem
//  			(
//  				ys->yokes,
//  				y_sel,
//  				(u8 *) yoke,
//  				sizeof (SK_Yoke *)
//  			);
//
//
//  			SK_Yoke_destroy (yoke);
//  		}
//
//  		SK_Vector_destroy (ys->yokes);
//
//  		free (ys);
//
//
//  		//Return failure
//
//  		if (NULL != sr_ptr)
//  		{
//  			*sr_ptr = sr;
//  		}
//
//  		return NULL;
//  	}
//
//
//  	//Initialize #mt_min_op in the newly created #SK_YokeSet
//
//  	ys->mt_min_op = mt_min_op;
//
//
//  	//Record the success of this function
//
//  	if (NULL != sr_ptr)
//  	{
//  		*sr_ptr = SlySr_get (SLY_SUCCESS);
//  	}
//
//
//  	return ys;
//  }
//
//
//
//
//  /*!
//   *  Destroys an #SK_YokeSet object
//   *
//   *  @param ys			The #SK_YokeSet object to destroy
//   *
//   *  @return			Standard status code
//   */
//
//  SlyDr SK_YokeSet_destroy (SK_YokeSet * ys)
//  {
//
//  	//Sanity check on arguments
//
//  	SK_NULL_RETURN (SK_YOKE_SET_L_DEBUG, ys, SlyDr_get (SLY_BAD_ARG));
//
//
//  	//Destroy all of the internal #SK_Yoke's first
//
//  	for (size_t y_sel = 0; y_sel < ys->num_yokes; y_sel += 1)
//  	{
//  		SK_Yoke * yoke = NULL;
//
//
//  		SK_Vector_get_elem
//  		(
//  			ys->yokes,
//  			y_sel,
//  			(u8 *) yoke,
//  			sizeof (SK_Yoke *)
//  		);
//
//
//  		SK_Yoke_destroy (yoke);
//  	}
//
//
//  	//Destroy the #SK_Vector used to hold pointers to the interal
//  	//#SK_Yoke's
//
//  	SK_Vector_destroy (ys->yokes);
//
//
//  	//Free the #SK_YokeSet itself
//
//  	free (ys);
//
//
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//
//
//
//  /*!
//   *  Prints out the contents of a given #SK_YokeSet
//   *
//   *  @param ys			The #SK_YokeSet to print out
//   *
//   *  @param f_ptr		The #FILE to print to, like #stdout
//   *
//   *  @param deco			An #SK_LineDeco describing how to decorate the
//   *				printed line
//   *
//   *  @param header_flag		A flag indicattin whether to print out an
//   *				informational header about #ys
//   *
//   *  @param yokes_flag		A flag indicating whether to print out the
//   *				addresses of the #SK_Yoke's in #ys
//   *
//   *  @return			Standard status code
//   */
//
//  SlyDr SK_YokeSet_print_opt
//  (
//  	SK_YokeSet *	ys,
//  	FILE *		f_ptr,
//  	SK_LineDeco	deco,
//  	int		header_flag,
//  	int		yokes_flag
//  )
//  {
//  	//Sanity check on arguments
//
//  	SK_NULL_RETURN (SK_YOKE_SET_L_DEBUG, ys, SlyDr_get (SLY_BAD_ARG));
//  	SK_NULL_RETURN (SK_YOKE_SET_L_DEBUG, f_ptr, SlyDr_get (SLY_BAD_ARG));
//
//
//  	//Print out the informational header if requested
//
//  	if (header_flag)
//  	{
//  		SK_LineDeco_print_opt
//  		(
//  			f_ptr,
//  			deco,
//  			"SK_YokeSet @ %p",
//  			ys
//  		);
//
//
//  		SK_LineDeco_print_opt
//  		(
//  			f_ptr,
//  			deco,
//  			"num_yokes  = %zu",
//  			ys->num_yokes
//  		);
//
//
//  		SK_LineDeco_print_opt
//  		(
//  			f_ptr,
//  			deco,
//  			"yokes      = %p",
//  			ys->yokes
//  		);
//
//
//  		SK_LineDeco_print_opt
//  		(
//  			f_ptr,
//  			deco,
//  			"mt_min_op  = %zu",
//  			ys->mt_min_op
//  		);
//  	}
//
//
//  	//Print out the addresses of the #SK_Yokes's contained in #ys->yokes if
//  	//requested
//  	//
//  	//FIXME : Maybe this printing is prettier if SK_Vector_get_elem () is
//  	//used and then those values are printed 1-by-1
//
//  	if (yokes_flag)
//  	{
//
//  		SK_LineDeco_print_opt (f_ptr, deco, "Contents of #yokes");
//
//  		SK_LineDeco_print_opt (f_ptr, deco, "{");
//
//
//  		SK_LineDeco id_deco = deco;
//
//  		id_deco.pre_str_cnt += 1;
//
//
//  		SK_Vector_print_opt (ys->yokes, f_ptr, id_deco, 1, 1, 1, 0);
//
//
//  		SK_LineDeco_print_opt (f_ptr, deco, "}");
//  	}
//
//
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//
//
//
//
//  /*!
//   *  Creates an #SK_YokeSetOpSet, which is a data structure that is used to
//   *  represent a collection of commands that should be performed on an
//   *  #SK_YokeSet.
//   *
//   *  @param ys2op_elems_per_block	The #elems_per_block argument to use
//   *					with SK_Vector_create () when creating
//   *					#ys2op
//   *
//   *  @param ys2op_neg_slack		The #neg_slack argument to use with
//   *					SK_Vector_create () when creating
//   *					#ys2op
//   *
//   *  @param ys2op_pos_slack		The #pos_slack argument to use with
//   *					SK_Vector_create () when creating
//   *					#ys2op
//   *
//   *  @param sr_ptr			Where to place a #SlySr value
//   *					indicating the success of this
//   *					function. This CAN be NULL if this
//   *					information is not desired.
//   *
//   *  @return				A pointer to the newly created
//   *					#SK_YokeSetOpSet object upon success;
//   *					NULL is returned otherwise.
//   */
//
//  SK_YokeSetOpSet * SK_YokeSetOpSet_create_opt
//  (
//  	size_t		ys2op_elems_per_block,
//  	size_t		ys2op_neg_slack,
//  	size_t		ys2op_pos_slack,
//  	SlySr *		sr_ptr
//  )
//  {
//  	//Reserve space for the #SK_YokeSetOpSet itself
//
//  	SK_YokeSetOpSet * ysos = malloc (sizeof (SK_YokeSetOpSet));
//
//
//  	if (NULL == ysos)
//  	{
//  		//Report failure
//
//  		SK_DEBUG_PRINT
//  		(
//  			SK_YOKE_SET_L_DEBUG,
//  			"Could not allocate #ysos"
//  		);
//
//
//  		//Return failure
//
//  		if (NULL != sr_ptr)
//  		{
//  			*sr_ptr = SlySr_get (SLY_BAD_ALLOC);
//  		}
//
//  		return NULL;
//  	}
//
//
//  	//Create the #SK_Vector that contains the operations
//
//  	SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);
//
//
//  	ysos->ys2op =
//
//  	SK_Vector_create
//  	(
//  		sizeof (SK_YokeSet2Op),
//  		0,
//  		ys2op_elems_per_block,
//  		ys2op_neg_slack,
//  		ys2op_pos_slack,
//  		SK_Vector_YokeSet2Op_init_no_op,
//  		NULL,
//  		&sr
//  	);
//
//
//  	if (SLY_SUCCESS != sr.res)
//  	{
//  		//Report failure
//
//  		SK_DEBUG_PRINT
//  		(
//  			SK_YOKE_SET_L_DEBUG,
//  			"Could not create #ysos->ys2op"
//  		);
//
//
//  		//Free any memory allocated in this function so far
//
//  		free (ysos);
//
//
//  		//Return failure
//
//  		if (NULL != sr_ptr)
//  		{
//  			*sr_ptr = sr;
//  		}
//
//  		return NULL;
//  	}
//
//
//  	//Record the success of this function, and return the newly created
//  	//#SK_YokeSetOpSet
//
//  	if (NULL != sr_ptr)
//  	{
//  		*sr_ptr = SlySr_get (SLY_SUCCESS);
//  	}
//
//
//  	return ysos;
//  }
//
//
//
//
//  /*!
//   *  Destroys an #SK_YokeSetOpSet object
//   *
//   *  @param ysos			The #SK_YokeSetOpSet object to destroy
//   *
//   *  @return			Standard status code
//   */
//
//  SlyDr SK_YokeSetOpSet_destroy
//  (
//  	SK_YokeSetOpSet *	ysos
//  )
//  {
//  	//Sanity check on the arguments
//
//  	SK_NULL_RETURN (SK_YOKE_SET_L_DEBUG, ysos, SlyDr_get (SLY_BAD_ARG));
//
//
//  	//Destroy the internal #SK_Vector, and then the #SK_YokeSetOpSet itself
//
//  	SK_Vector_destroy (ysos->ys2op);
//
//  	free (ysos);
//
//
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//
//
//
//
//  /*!
//   *  Used to initialize an #SK_Vector full of #SK_YokeSet2Op's to represent
//   *  no-op's.
//   *
//   *  @param elem_ptr		The element being modified in the #SK_Vector.
//   *  				This will be casted to a pointer to an
//   *  				#SK_YokeSet2Op
//   *
//   *  @param index		Stub argument needed for #SK_Vector_create ()
//   *
//   *  @param arg			Stub argument needed for #SK_Vector_create ()
//   *
//   *  @return			Standard status code
//   */
//
//  //FIXME : Is this function necessary? After all, it appears that
//  //SK_YokeSetOpSet_create_opt () always initially creates an #SK_Vector that has
//  //0 length, and therefore, does not really require an initialization function.
//
//  SlyDr SK_Vector_YokeSet2Op_init_no_op
//  (
//  	u8 *		elem_ptr,
//  	size_t		index,
//  	void *		arg
//  )
//  {
//  	//Sanity check on arguments
//
//  	SK_NULL_RETURN (SK_YOKE_SET_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));
//
//
//  	//Make the element at #elem_ptr represent a no-op
//
//  	SK_YokeSet2Op * ys2op = (SK_YokeSet2Op *) elem_ptr;
//
//
//  	ys2op->op = SK_YOKE_SET_NO_OP;
//
//  	ys2op->y0 = 0;
//
//  	ys2op->y1 = 0;
//
//
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//
//
//
//
//  /*!
//   *  This function is intended to be used with SK_Vector_append_cust () to
//   *  forward an element as the one appended to an #SK_Vector containing
//   *  #SK_YokeSet2Op's.
//   *
//   *  @param elem_ptr		The element being modified in the #SK_Vector.
//   *  				This will be casted to a pointer to an
//   *  				#SK_YokeSet2Op
//   *
//   *  @param index		Stub argument needed for #SK_Vector_create ()
//   *
//   *  @param src_elem		The element to forward to #elem_ptr
//   *
//   *  @return			Standard status code
//   */
//
//  SlyDr SK_Vector_YokeSet2Op_forward
//  (
//  	u8 *		elem_ptr,
//  	size_t		index,
//  	void *		src_elem
//  )
//  {
//  	//Sanity check on arguments
//
//  	SK_NULL_RETURN (SK_YOKE_SET_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));
//
//  	SK_NULL_RETURN (SK_YOKE_SET_L_DEBUG, src_elem, SlyDr_get (SLY_BAD_ARG));
//
//
//  	//Copy #src_elem to #elem_ptr as #SK_YokeSet2Op's
//
//  	* ((SK_YokeSet2Op *) elem_ptr) = * ((SK_YokeSet2Op *) src_elem);
//
//
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//
//
//
//
//  /*!
//   *  Appends an #SK_YokeSet2Op to the end of an #SK_YokeSetOpSet.
//   *
//   *  @param ysos			The #SK_YokeSetOpSet to modify
//   *
//   *  @param ys2op		The #SK_YokeSet2Op to attach after the last
//   *				operation in #ysos
//   *
//   *  @return			Standard status code
//   */
//
//  //FIXME : Is it certain that #SK_YokeSet2Op will not use the PIMPL pattern? It
//  //seems like a reasonable assumption. Give this more consideration later on.
//
//  SlySr SK_YokeSetOpSet_append_2_op
//  (
//  	SK_YokeSetOpSet *	ysos,
//
//  	SK_YokeSet2Op		ys2op
//  )
//  {
//  	//Sanity check on arguments
//
//  	SK_NULL_RETURN (SK_YOKE_SET_L_DEBUG, ysos, SlySr_get (SLY_BAD_ARG));
//
//
//  	//Append the operation to the end of the #SK_YokeSetOpSet
//
//  	SlySr sr =
//
//  	SK_Vector_append_cust
//  	(
//  		ysos->ys2op,
//  		SK_Vector_YokeSet2Op_forward,
//  		&ys2op
//  	);
//
//
//  	SK_RES_RETURN (SK_YOKE_SET_L_DEBUG, sr.res, sr);
//
//
//  	return SlySr_get (SLY_SUCCESS);
//  }
//
//
//
//
//  /*!
//   *  Performs the operations stored in an #SK_YokeSetOpSet on an #SK_YokeSet.
//   *
//   *  @warning
//   *  If any operation fails to be applied, this function will stop prematurely,
//   *  and #ys will be left in an "intermediate" state. To recover after this, it
//   *  is recommended to use #SK_YokeSet_calc_diff () to build undo-lists for #ys
//   *
//   *  TODO : Write the function SK_YokeSet_calc_diff () to help build undo-lists
//   *
//   *  TODO : Write the function SK_YokeSet_apply_op_set_safe () which makes a
//   *  copy of #ys before operating on it
//   *
//   *  @param ys			The #SK_YokeSet to modify
//   *
//   *  @param ysos			The #SK_YokeSetOpSet containing operations to
//   *				perform on #ys
//   *
//   *  @return			Standard status code
//   */
//
//  //FIXME : This function should not be concerned with making undo-lists itself.
//  //A separate function should be created to create diff's between #SK_YokeSet's,
//  //so that checkpoints could be used.
//
//  SlySr SK_YokeSet_apply_op_set
//  (
//  	SK_YokeSet *		ys,
//  	SK_YokeSetOpSet *	ysos
//  )
//  {
//
//  	//Sanity check on arguments
//
//  	SK_NULL_RETURN (SK_YOKE_SET_L_DEBUG, ys, SlySr_get (SLY_BAD_ARG));
//
//  	SK_NULL_RETURN (SK_YOKE_SET_L_DEBUG, ysos, SlySr_get (SLY_BAD_ARG));
//
//
//  	//Determine the number of operations in #ysos
//
//  	size_t num_ops = 0;
//
//  	SK_Vector_get_num_elems (ysos->ys2op, &num_ops);
//
//
//  	//Interpret the instructions in #ysos
//
//  	for (size_t op_sel = 0; op_sel < num_ops; op_sel += 1)
//  	{
//  		//Fetch the current operation
//
//  		SK_YokeSet2Op instr;
//
//  		instr.op = SK_YOKE_SET_NO_OP;
//  		instr.y0 = 0;
//  		instr.y1 = 0;
//
//  		SK_Vector_get_elem
//  		(
//  			ysos->ys2op,
//  			op_sel,
//  			(u8 *) &instr,
//  			sizeof (SK_YokeSet2Op)
//  		);
//
//
//  		//Interpret the op-code in #instr
//
//  		if (SK_YOKE_SET_NO_OP == instr.op)
//  		{
//  			//Do nothing, so just move to the next instruction
//
//  			continue;
//  		}
//
//  		else if (SK_YOKE_SET_LINK == instr.op)
//  		{
//
//  			//Check if the indices for the linking instruction are
//  			//in-range
//
//  			if
//  			(
//  				(instr.y0 >= ys->num_yokes)
//
//  				||
//
//  				(instr.y1 >= ys->num_yokes)
//  			)
//  			{
//  				SK_DEBUG_PRINT
//  				(
//  					SK_YOKE_SET_L_DEBUG,
//  					"#ys = %p, #ysos = %p\n"
//  					"For instruction #%zu, linking indices "
//  					"%zu and %zu are out-of-range. "
//  					"#ys.num_yokes = %zu",
//  					ys,
//  					ysos,
//  					op_sel,
//  					instr.y0,
//  					instr.y1,
//  					ys->num_yokes
//  				);
//
//  				return SlySr_get (SLY_BAD_ARG);
//  			}
//
//
//  			//Retrieve the #SK_Yoke's to operate on
//
//  			SK_Yoke * y0 = NULL;
//
//  			SK_Yoke * y1 = NULL;
//
//
//  			SK_Vector_get_elem
//  			(
//  				ys->yokes,
//  				instr.y0,
//  				(u8 *) &y0,
//  				sizeof (SK_Yoke *)
//  			);
//
//
//  			SK_Vector_get_elem
//  			(
//  				ys->yokes,
//  				instr.y1,
//  				(u8 *) &y1,
//  				sizeof (SK_Yoke *)
//  			);
//
//
//  			//Attempt to link together the #SK_Yoke's indicated by
//  			//the operation arguments
//
//  			//FIXME : Write SK_Yoke_link_auto () (which should
//  			//automatically resize #SK_Yoke's when needed), and use
//  			//that instead of SK_Yoke_link () here.
//
//  			SlySr sr =
//
//  			SK_Yoke_link
//  			(
//  			 	y0,
//  				1,
//  				1,
//  				NULL,
//  				NULL,
//
//  				y1,
//  				1,
//  				1,
//  				NULL,
//  				NULL,
//
//  				NULL
//  			);
//
//
//  			if (SLY_SUCCESS != sr.res)
//  			{
//  				SK_DEBUG_PRINT
//  				(
//  					SK_YOKE_SET_L_DEBUG,
//  					"In #ys = %p, could establish link "
//  					"between #SK_Yoke's %zu and %zu",
//  					ys,
//  					instr.y0,
//  					instr.y1
//  				);
//
//  				return sr;
//  			}
//  		}
//
//  		else if (SK_YOKE_SET_UNLINK == instr.op)
//  		{
//
//  			//Check if the indices for the unlinking instruction
//  			//are in-range
//
//  			if
//  			(
//  				(instr.y0 >= ys->num_yokes)
//
//  				||
//
//  				(instr.y1 >= ys->num_yokes)
//  			)
//  			{
//  				SK_DEBUG_PRINT
//  				(
//  					SK_YOKE_SET_L_DEBUG,
//  					"#ys = %p, #ysos = %p\n"
//  					"For instruction #%zu, unlinking "
//  					"indices %zu and %zu are out-of-range. "
//  					"#ys.num_yokes = %zu",
//  					ys,
//  					ysos,
//  					op_sel,
//  					instr.y0,
//  					instr.y1,
//  					ys->num_yokes
//  				);
//
//  				return SlySr_get (SLY_BAD_ARG);
//  			}
//
//
//  			//Retrieve the #SK_Yoke's to operate on
//
//  			SK_Yoke * y0 = NULL;
//
//  			SK_Yoke * y1 = NULL;
//
//
//  			SK_Vector_get_elem
//  			(
//  				ys->yokes,
//  				instr.y0,
//  				(u8 *) &y0,
//  				sizeof (SK_Yoke *)
//  			);
//
//
//  			SK_Vector_get_elem
//  			(
//  				ys->yokes,
//  				instr.y1,
//  				(u8 *) &y1,
//  				sizeof (SK_Yoke *)
//  			);
//
//
//  			//Unlink the #SK_Yoke's indicated by the operation
//  			//arguments
//
//  			SK_Yoke_unlink
//  			(
//  			 	y0,
//  				y1,
//  				NULL,
//  				NULL,
//  				NULL
//  			);
//
//  		}
//
//  		else
//  		{
//  			SK_DEBUG_PRINT
//  			(
//  				SK_YOKE_SET_L_DEBUG,
//  				"For %ysos = %p, #ysos->ys2op [%zu].op = %d, "
//  				"which is not a supported op-code (yet). "
//  				"Stopping application of operations to "
//  				"#ys = %p",
//  				ysos,
//  				op_sel,
//  				instr.op,
//  				ys
//  			);
//
//
//  			return SlySr_get (SLY_BAD_ARG);
//  		}
//
//  	}
//
//
//  	return SlySr_get (SLY_SUCCESS);
//  }
//
//
//
//
//  /*!
//   *  Performs a simple testbench on SK_YokeSet_create ()
//   *
//   *  @return			Standard status code
//   */
//
//  SlySr SK_YokeSet_create_tb ()
//  {
//
//  	printf ("SK_YokeSet_create_tb () : BEGIN\n\n");
//
//
//  	//Select constants that determine how many parameterizations to test
//
//  	const int64_t BEG_NUM_YOKES =			0;
//  	const int64_t END_NUM_YOKES =			100;
//  	const int64_t INC_NUM_YOKES =			20;
//
//  	const int64_t BEG_LINKS_PER_YOKE =		0;
//  	const int64_t END_LINKS_PER_YOKE =		10;
//  	const int64_t INC_LINKS_PER_YOKE =		5;
//
//  	const int64_t BEG_MT_MIN_OP =			0;
//  	const int64_t END_MT_MIN_OP =			8;
//  	const int64_t INC_MT_MIN_OP =			4;
//
//  	const int64_t BEG_YOKES_ELEMS_PER_BLOCK =	1;
//  	const int64_t END_YOKES_ELEMS_PER_BLOCK =	4;
//  	const int64_t INC_YOKES_ELEMS_PER_BLOCK =	1;
//
//  	const int64_t BEG_YOKES_NEG_SLACK =		0;
//  	const int64_t END_YOKES_NEG_SLACK =		1;
//  	const int64_t INC_YOKES_NEG_SLACK =		1;
//
//  	const int64_t BEG_YOKES_POS_SLACK =		0;
//  	const int64_t END_YOKES_POS_SLACK =		1;
//  	const int64_t INC_YOKES_POS_SLACK =		1;
//
//
//  	int64_t num_yokes =		BEG_NUM_YOKES;
//
//  	int64_t links_per_yoke =	BEG_LINKS_PER_YOKE;
//
//  	int64_t mt_min_op =		BEG_MT_MIN_OP;
//
//  	int64_t yokes_elems_per_block =	BEG_YOKES_ELEMS_PER_BLOCK;
//
//  	int64_t yokes_neg_slack =	BEG_YOKES_NEG_SLACK;
//
//  	int64_t yokes_pos_slack =	BEG_YOKES_POS_SLACK;
//
//
//  	//Create several different #SK_YokeSet's with different configurations
//
//  	int go =		1;
//
//  	int error_detected =	0;
//
//  	SK_YokeSet * ys =	NULL;
//
//
//  	while (go)
//  	{
//  		//Print out the parameters that will be used in this test
//  		//iteration
//
//  		printf
//  		(
//  			"\n----------\n\n"
//  			"Arguments :\n"
//  			"num_yokes =             %zu\n"
//  			"links_per_yoke =        %zu\n"
//  			"mt_min_op =             %zu\n"
//  			"yokes_elems_per_block = %zu\n"
//  			"yokes_neg_slack =       %zu\n"
//  			"yokes_pos_slack =       %zu\n\n",
//  			num_yokes,
//  			links_per_yoke,
//  			mt_min_op,
//  			yokes_elems_per_block,
//  			yokes_neg_slack,
//  			yokes_pos_slack
//  		);
//
//
//  		//Create an arbitrary #SK_YokeSet
//
//  		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);
//
//
//  		ys =
//
//  		SK_YokeSet_create_opt
//  		(
//  			num_yokes,
//  			links_per_yoke,
//  			mt_min_op,
//  			yokes_elems_per_block,
//  			yokes_neg_slack,
//  			yokes_pos_slack,
//  			&sr
//  		);
//
//  		if ((NULL == ys) || (SLY_SUCCESS != sr.res))
//  		{
//  			SK_DEBUG_PRINT
//  			(
//  				SK_VECTOR_L_DEBUG,
//  				"failed to create #SK_YokeSet"
//  			);
//
//  			error_detected = 1;
//
//  			break;
//  		}
//
//
//  		//Print out the newly createed #SK_YokeSet
//
//  		SK_YokeSet_print_opt
//  		(
//  			ys,
//  			stdout,
//  			SK_LineDeco_std (),
//  			1,
//  			1
//  		);
//
//
//
//
//  		//WAS LAST WRITING HERE
//
//
//
//
//  		//Destroy the #SK_YokeSet
//
//  		SK_YokeSet_destroy (ys);
//
//
//  	}
//
//
//  }

