# Code Style


FIXME : This code-base is currently undergoing many experiments with
interfaces, so perhaps all / most of this will change in the near future.


For spek-net, the following coding conventions are currently followed. Unless
otherwise stated, these coding conventions are in reference to C programming.

These rules are indeed up for debate, and so I will try giving as clear
justification for them where necessary.


* A very common C programming convention is used to categorize data structures
  as "opaque" or "transparent".

  A data structure is opaque if only the declaration, and NOT the defintion, of
  the struct is available to calling code. Opaque data structures are expected
  to be operated on by functions that accept a pointer to the object as an
  argument.

  A data structure is transparent if both the declaration AND the defintion of
  the struct is available to calling code. Depending on the desired level of
  coupling, transparent data structures may be operated on by functions that
  accept a pointer to the object as an input, or by directly referencing the
  members of the struct.


* For a data structure named "Thing", there may or may not be functions to
  construct a *Thing* object in memory. If such functions are present, then the
  following function signatures should be used...


		//For opaque object construction / destruction

		Thing * Thing_create (...)

		ErrorCode Thing_destroy (Thing * t)


		//For transparent object construction / destruction

		ErrorCode Thing_init (Thing * t, ...)

		ErrorCode Thing_deinit (Thing * t)


  Variants of the above function signatures are acceptable, as long as they are
  similar enough that their usage is obvious.

  For example, if Thing\_create (...) already exists, and a 2nd version of it
  that accepts more arguments is desired, then Thing\_create\_opt (...) would
  be an acceptable function signature for this new function.

  For opaque data structures, it is required to use the \*\_create () /
  \*\_destroy () function pair. For transparent data structures, it is required
  to use the \*\_init () / \*\_deinit () function pair.

  A data structure may be opaque to some code and transparent to some other
  code at the same time. In this event, both sets of \*\_create () /
  \*\_destroy () and \*\_init () / \*\_deinit () functions should be provided,
  and their declarations placed in different header files.


* If a constructor (\*\_create () or \*\_init ()) is called in a function, then
  the destructor (\*\_destroy () or \*\_deinit ()) must be called on the
  constructed object *inside that same function*. The one exception is if the
  function is a constructor or destructor itself.

  Ensuring that every constructor is paired with a destructor in the same
  function ensures that resource leaks do not occur. It also provides a clear
  guideline as to what function is responsible for what resource management.

  This guideline essentially aims to achieve the same thing as RAII in C++, but
  without the use of exceptions (and everything they imply).


* "Coupling" refers to how sensitive one piece of code is to changes in another
  piece of code. As applied to *data structures* in this project, the following
  classifications of coupling will be used:


	*Consider data structure A to be a user of data structure B*


	**Low-Coupling** :

		Data structure A only includes a pointer to data structure B.
		It is not necessary for a definition of B to be available to
		compile A. Restated, altering the definition of B does not
		induce a recompilation of A.

		Functions that operate on A must NOT directly reference members
		of B. Instead, they MUST use interfacing functions that operate
		on B by receiving a pointer to B.

		A is NOT considered responsible for B's memory management
		beyond calling a *_create () and *_destroy () function.


	**Medium-Coupling** :

		Data structure A can directly declare a member of type B. It
		is expected that changes to B will cause recompilations of A.

		Functions that operate on A must NOT directly reference members
		of B. As in the case of "Low-coupling", they MUST use
		interfacing functions that operate on B by receiving a pointer
		to B.

		A is NOT considered responsible for B's memory management
		beyond calling an *_init () and *_deinit () function. HOWEVER,
		A is expected to understand the consequences of copying or
		moving B, and should use a *_copy () or *_move () function if
		provided. (Ex : If a #SK_Yoke is moved, the pointers in the
		#SK_Yoke's linked to it must be updated to the new location.)


	**High-Coupling** :

		Data structure A can directly declare a member of type B. It
		is expected that changes to B will cause recompilations of A,
		as well as potential rewrites of A.

		Functions that operate on A are free to directly reference
		members of B. It is entirely optional to use interfacing
		functions for B if they are available.


  In general, it is good programming practice for pieces of code to be as
  lowly-coupled together as possible. This helps prevent costly rewrites from
  frequently occurring in the code-base. However, some degree of coupling is
  usually impossible to avoid due to performance or resource management issues.

  Note that opaque data types have the unfortunate side-effect of frequently
  causing CPU cache-misses. This is due to the constructor always placing the
  object B somewhere on the heap, which may be far away from where object A is.
  As a result, always using low-coupling as defined above can degrade
  performance.

  That is why for internal usage in this project, the default target level of
  coupling should be medium-coupling. The transparent usage of B inside of A
  allows for their data to be packed together in memory. Additionally, the
  exclusive use of functions to interface with B reduces the frequency of code
  rewrites compared to directly referencing B's members.

  During compilation, medium-coupling requires the same information to be
  exposed as in high-coupling, because the definition of B must be available in
  both cases. So for public interfaces accessible by external projects,
  medium-coupling is effectively impossible to guarantee. By virtue of exposing
  the same information, permitting medium-coupling is the same as permitting
  high-coupling as far as external code is concerned.

  Ultimately, achieving medium-coupling requires a promise from A's code that
  members of B will not be directly referenced. Fortunately, that promise is
  possible to maintain for code that remains internal to this project.


* Ideally, this project's public interface should help ensure that external
  projects are as lowly-coupled to this project as possible. That is why for
  the externally visible interfaces, opaque data types are *greatly* preferred.
  This prevents those external projects from having to be rebuilt every time
  this project is changed.

  An exception may be granted for a data structure if *both*...

	1) The data structure is considered very stable and unlikely to change

	2) There is a clear performance / usability benefit to making the data
	   structure transparent


* If there is a file named Thing.c, there should at least be 1 header file
  named Thing.h associated with it. All declarations that should be visible
  outside of Thing.c should be possible to retrieve by #include'ing Thing.h.

  By #include'ing Thing.h in a different file, that file is essentially asking,
  "Let me see everything about *Thing*". However, there may be times
  where this is undesirable. For example, if Thing.h contains the definition of
  the data structure #Thing, it might not be appropriate for other\_file.c to
  include it if it should not recompile everytime *Thing* changes.

  For this reason, header files that represent subsets of Thing.h are allowed.
  For example, if only the declarations necessary to treat *Thing* as an opaque
  data structure are desired, then the header file Thing\_opaque.h can be
  created.

  So far only these 4 header specializations have a specific meaning:


	*Assume there is a file named Thing.c*


	**Thing.h**

	This header file includes all declarations for Thing.c that should be
	visible to other source files. It should #include all other
	Thing\_\*.h files.

	This is the only mandatory header file associated with Thing.c.


	**Thing_constants.h**

	This header file contains only the constant values that would be
	otherwise available by #include'ing Thing.h.


	**Thing_external.h**

	Contains declarations associated with Thing.c that should be visible to
	external projects.


	**Thing_opaque.h**

	Assuming Thing.c defines a data structure *Thing*, this header file
	contains *only* the declarations necessary to use *Thing* as an opaque
	data type.


  There is no need to avoid repeating declarations between the sub-headers. It
  is acceptable if more than 1 sub-header uses a given declaration, so long as
  that declaration is available in Thing.h as well.
