/*!****************************************************************************
 *
 *  @file
 *  SK_Addr.c
 *
 *  Implements a data structure that is capable of storing a spek-net address
 *  at a given layer.
 *
 *****************************************************************************/




//FIXME : Completely re-do this data structure; this should now be
//embarrasingly simple to implement using #SK_BoxIntf. In fact, it may even be
//so simple that #SK_Addr may very well be unnecessary as a data structure
//(this needs some evaluation).




#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "./SK_debug.h"
#include "./SK_misc.h"
#include "./SK_numeric_types.h"




#include "./SK_Addr.h"








/*!
 *  A data-structure that is used to represent spek-net address, which is
 *  associated with a particular layer of the network.
 *
 *  Take note that #num_words and #layer are different members. This allows for
 *  the #layer of the #SK_Addr to be changed without having to always incur a
 *  reallocation.
 */

//FIXME : Consider using #SK_Vector inside of this data structure to handle
//resizing events

typedef struct SK_Addr
{
	// The number of words allocated at #words.
	//
	// This must NOT be lower than (layer + 1), unless it is 0. When this
	// value is 0, that indicates the #SK_Addr is uninitialized.

	size_t			num_words;


	/// The current layer that the #SK_Addr is in reference to. Any address
	/// words in #word at a higher index than this should be considered
	/// garbage data.
	///
	/// This should never be greater than or equal to #num_words, except in
	/// the case where #num_wrods is 0.

	SK_AddrLayerNum		layer;


	/// Pointer to the first element of an array of address words of the
	/// spek-net address. At any given time, the array is #num_words in
	/// length, although only words at an index less than or equal to
	/// #layer are considered valid.

	SK_AddrWordNum *	words;
}

SK_Addr;




/*!
 *  Creates an #SK_Addr data structure that can store a given number of address
 *  words.
 *
 *  This function should only be used in place of #SK_Addr_create () when
 *  explicitly controlling the number of words to allocate provides efficiency
 *  benefits.
 *
 *  @warning
 *  Some time after calling this function, SK_Addr_destroy () must be used on
 *  the returned pointer, otherwise a memory leak will occur.
 *
 *  @warning
 *  The created #SK_Addr will be set to layer 0, and all the allocated address
 *  words will be set to #reset_val.
 *
 *  The reason that a layer is not possible to specify in this function is so
 *  that cases where the layer is higher than #num_words does not have to be
 *  handled.
 *
 *  @warning
 *  A previous version of this function provided an argument 'reset_words' that
 *  acted as a flag for whether or not to reset the address words in the newly
 *  allocated #SK_Addr. This argument has been removed because it caused the
 *  tool 'valgrind' to report numerous errors when used with the '-v' option,
 *  because these words could be printed out without being initialized.
 *
 *  The lack of initialization was intentional, why waste resources
 *  initializing address words that you have already denoted are invalid with
 *  the 'num_words' member of #SK_Addr? There is a good argument to be made,
 *  though, that _any_ access to unitialized memory is a bug, even if that
 *  access is a printing statement (which even indicates that the memory is
 *  unitialized!).
 *
 *  If the cost of initialization were more severe, this argument would
 *  probably be left in. However, since this initialization is cheap for
 *  realistically sized #SK_Addr's, the benefit of not polluting valgrind's
 *  output outweighs the run-time cost here.
 *
 *  @param num_words			The number of address words to
 *					allocate for this #SK_Addr. Note that
 *					this does not set the layer of
 *					#SK_Addr; by using a higher number of
 *					words, it simply prepares an #SK_Addr
 *					to use a higher layer without needing a
 *					reallocation.
 *
 *					This argument CAN be 0, in which case
 *					no address words will be allocated yet.
 *
 *  @param reset_val			The value to set each #SK_Addr word
 *					to, provided that #reset_words is true.
 *
 *  @param sr_ptr			A pointer to where to place a #SlySr
 *					indicating the success of this
 *					function. This CAN be NULL if this
 *					information is not desired.
 *
 *  @return				A pointer to the newly created
 *					#SK_Addr structure upon success; NULL
 *					is returned otherwise.
 */

SK_Addr * SK_Addr_create_bare
(
	size_t			num_words,
	SK_AddrWordNum		reset_val,

	SlySr *			sr_ptr
)
{
	//Allocate space for the #SK_Addr itself

	SK_Addr * ska_ptr = malloc (sizeof (SK_Addr));

	if (NULL == ska_ptr)
	{
		//Report the reason for failure

		if (NULL != sr_ptr)
		{
			*sr_ptr = SlySr_get (SLY_BAD_ALLOC);
		}


		SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska_ptr, NULL);
	}


	//Save the number of words that will be allocated, and attempt that
	//allocation it is necessary

	ska_ptr->num_words = num_words;


	if (ska_ptr->num_words > 0)
	{
		SK_AddrWordNum * words = malloc (sizeof (SK_AddrWordNum) * ska_ptr->num_words);

		ska_ptr->words = words;


		if (NULL == words)
		{
			//Report the reason for failure

			if (NULL != sr_ptr)
			{
				*sr_ptr = SlySr_get (SLY_BAD_ALLOC);
			}


			//Free any memory allocated in this function

			free (ska_ptr);


			SK_NULL_RETURN (SK_ADDR_L_DEBUG, words, NULL);
		}
	}

	else
	{
		ska_ptr->words = NULL;
	}


	//Always initialize the layer to 0 here

	ska_ptr->layer = 0;


	//Set each word in the #SK_Addr to an initial value

	for (size_t word_sel = 0; word_sel < ska_ptr->num_words; word_sel += 1)
	{
		ska_ptr->words [word_sel] = reset_val;
	}


	//Report success before leaving this function

	if (NULL != sr_ptr)
	{
		*sr_ptr = SlySr_get (SLY_SUCCESS);
	}


	return ska_ptr;
}




/*!
 *  Creates an #SK_Addr data structure.
 *
 *  An #SK_Addr is intended to represent an address for a node in the network
 *  at some given layer.
 *
 *  @warning
 *  Some time after calling this function, SK_Addr_destroy () must be used on
 *  the returned pointer, otherwise a memory leak will occur.
 *
 *  @param layer			The highest layer that the #SK_Addr is
 *  					in reference to. This can be changed
 *  					later.
 *
 *  @param init_words			The initial value for the address words
 *
 *  @param sr_ptr			A pointer to where to place a #SlySr
 *					indicating the success of this
 *					function. This CAN be NULL if this
 *					information is not desired.
 *
 *  @return				A pointer to the newly created
 *					#SK_Addr structure upon success; NULL
 *					is returned otherwise.
 */

SK_Addr * SK_Addr_create
(
	SK_AddrLayerNum		layer,
	SK_AddrWordNum		init_words,

	SlySr *			sr_ptr
)
{
	//Create a #SK_Addr that can the address words up to #layer
	//
	//(Keep in mind that the layer numbering starts at 0)

	SlySr sr = SlySr_get (SLY_SUCCESS);


	SK_Addr * ska =

	SK_Addr_create_bare
	(
		((size_t) layer + 1),

		init_words,

		&sr
	);


	if (SLY_SUCCESS != sr.res)
	{
		if (NULL != sr_ptr)
		{
			*sr_ptr = sr;
		}

		SK_RES_RETURN (SK_ADDR_L_DEBUG, sr.res, NULL);
	}


	//Save the layer in #ska

	ska->layer = layer;


	//Report success (if requested)

	if (NULL != sr_ptr)
	{
		*sr_ptr = SlySr_get (SLY_SUCCESS);
	}


	return ska;
}




/*!
 *  Creates a new #SK_Addr data structure, and makes it equivalent to an
 *  existing #SK_Addr.
 *
 *  @warning
 *  Some time after calling this function, SK_Addr_destroy () must be used on
 *  the returned pointer, otherwise a memory leak will occur.
 *
 *  @param ska_src		Pointer to the #SK_Addr to copy
 *
 *  @param sr_ptr		A pointer to where to place a #SlySr indicating
 *  				the success of this function. This CAN be NULL
 *  				if this information is not desired.
 *
 *  @return			A pointer to the newly created #SK_Addr
 *  				structure upon success; NULL is returned
 *  				otherwise.
 */

SK_Addr * SK_Addr_create_copy
(
	SK_Addr *	ska_src,
	SlySr *		sr_ptr
)
{
	//Sanity check on arguments

	if (NULL == ska_src)
	{
		if (NULL != sr_ptr)
		{
			*sr_ptr = SlySr_get (SLY_BAD_ARG);
		}

		SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska_src, NULL);
	}


	//Allocate a #SK_Addr capable of storing a copy of #ska_src

	SlySr sr = SlySr_get (SLY_SUCCESS);


	SK_Addr * ska_new =

	SK_Addr_create_bare
	(
		ska_src->num_words,

		0,

		&sr
	);


	if (SLY_SUCCESS != sr.res)
	{
		if (NULL != sr_ptr)
		{
			*sr_ptr = sr;
		}

		SK_RES_RETURN (SK_ADDR_L_DEBUG, sr.res, NULL);
	}


	//Copy the contents of #ska_src over to #ska_new

	ska_new->layer = ska_src->layer;


	for (size_t l_sel = 0; l_sel < ska_src->num_words; l_sel += 1)
	{
		ska_new->words [l_sel] = ska_src->words [l_sel];
	}


	//Report success and return the newly created #SK_Addr

	if (NULL != sr_ptr)
	{
		*sr_ptr = SlySr_get (SLY_SUCCESS);
	}


	return ska_new;
}




/*!
 *  Destroys an #SK_Addr data structure
 *
 *  @param ska			Pointer to the #SK_Addr object to destroy.
 *
 *  @return			Standard status code
 */

SlyDr SK_Addr_destroy (SK_Addr * ska)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	//First, free the internal array in #ska, and the free the structure
	//itself

	free (ska->words);

	free (ska);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Changes the amount of memory allocated to a #SK_Addr so that it can store a
 *  given number of address words.
 *
 *  @warning
 *  Previously, this function provided a #reset_flag argument, but this was
 *  removed after it caused 'valgrind' to report errors. See the warning in
 *  SK_Addr_create_bare () before deciding to alter this decision.
 *
 *  @param ska			The #SK_Addr to modify. This will only be
 *				modified if this function succeeds.
 *
 *  @param num_words		The number of words that #ska should be capable
 *				of storing. This argument CAN be 0, in which
 *				case no address words will be possible to store
 *				in #ska, but #ska can be resized later on.
 *
 *  @param preserve_flag	If true, then as much of #ska as possible will
 *				be preserved when resizing it to #num_words.
 *				That means if #num_words is greater than
 *				#ska->layer, then the whole address will be
 *				preserved. If #num_words is less than or equal
 *				to #ska->layer, the address words associated
 *				with higher numbered layers will be thrown out,
 *				and #ska->layer will be set to (num_words - 1).
 *				If #num_words is 0, then #ska->layer will be
 *				set to 0.
 *
 *				If this argument is false, then ska->layer will
 *				always be set to 0 to indicate that the address
 *				contains garbage data.
 *
 *  @param reset_val		The value to initialize address words to if
 *				#reset_flag is true
 *
 *  @return			Standard status code. If this is not
 *  				#SLY_SUCCESS, #ska will not be modified.
 */

SlySr SK_Addr_resize
(
	SK_Addr *		ska,
	size_t			num_words,
	int			preserve_flag,
	SK_AddrWordNum		reset_val
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Check if any actual resizing is necessary

	if (num_words == ska->num_words)
	{
		//Even when no resizing has taken place, respect #preserve_flag

		if (! preserve_flag)
		{
			ska->layer = 0;

			for (size_t l_sel = 0; l_sel < ska->num_words; l_sel += 1)
			{
				ska->words [l_sel] = reset_val;
			}
		}

		return SlySr_get (SLY_SUCCESS);
	}


	//Attempt to allocate memory for the new array of words

	SK_AddrWordNum * new_words = malloc (sizeof (SK_AddrWordNum) * num_words);

	SK_NULL_RETURN (SK_ADDR_L_DEBUG, new_words, SlySr_get (SLY_BAD_ALLOC));


	//Calculate the maximum layer in #ska that will be preserved

	SK_AddrLayerNum num_preserve_words = 0;

	if (preserve_flag)
	{

		if ((0 != ska->num_words) && (ska->num_words <= ska->layer))
		{
			//FIXME : This condition can be removed once it is
			//confidently known that #SK_Addr will never have
			//errors where #ska->layer is greater than or equal to
			//#ska->num_words

			num_preserve_words = 0;

			SK_DEBUG_PRINT
			(
				SK_ADDR_L_DEBUG,
				"BUG : For the #SK_Addr @ %p, "
				"#layer (%lu) >= #num_words (%lu). "
				"Can not preserve any words during resize.",
				ska,
				ska->layer,
				ska->num_words
			);
		}


		else if (0 == ska->num_words)
		{
			num_preserve_words = 0;
		}

		else if (num_words > ska->layer)
		{
			num_preserve_words = ska->layer + 1;
		}

		else
		{
			num_preserve_words = num_words;
		}
	}


	//If the preserve flag is true, then attempt to preserve as much of
	//#SK_Addr as possible

	if (preserve_flag)
	{
		for (size_t l_sel = 0; l_sel < num_preserve_words; l_sel += 1)
		{
			new_words [l_sel] = ska->words [l_sel];
		}


		//Save the preserved layer back in #ska

		if (num_preserve_words > 0)
		{
			ska->layer = (num_preserve_words - 1);
		}

		else
		{
			ska->layer = 0;
		}
	}

	else
	{
		//Indicate inside of #ska that nothing has been preserved

		ska->layer = 0;
	}


	//Reset the remainder of #ska->words that has not been used to preserve
	//its previous address

	for (size_t l_sel = num_preserve_words; l_sel < num_words; l_sel += 1)
	{
		new_words [l_sel] = reset_val;
	}


	//Free the memory currently associated with ska->words, and THEN set it
	//equal to #new_words

	free (ska->words);

	ska->words = new_words;

	ska->num_words = num_words;


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Reduces the amount of memory used by an #SK_Addr so that it uses only as
 *  much memory as is necessary to represent an address at its current layer.
 *
 *  @param ska			The #SK_Addr to minify
 *
 *  @return			Standard status code. If this is not
 * 				#SLY_SUCCESS, #ska will not be modified.
 */

SlySr SK_Addr_minify
(
	SK_Addr *	ska
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Resize the current address so that it only has enough memory
	//allocated to store an address for its current layer

	return SK_Addr_resize (ska, ((size_t) ska->layer + 1), 1, 0);
}




/*!
 *  Makes an #ska_dst equal to #ska_src, resizing #ska_dst to hold as many
 *  address words as #ska_src if necessary.
 *
 *  @param ska_dst		The #SK_Addr to make equal to #ska_src
 *
 *  @param ska_src		The #SK_Addr to use a source
 *
 *  @param reset_flag		A flag indicating whether or not words of
 *				#ska_dst that have not been used to copy
 *				#ska_src should be reset
 *
 *  @param reset_val		The value to initialize address unused words to
 *				if #reset_flag is true
 *
 *  @return			Standard status code. Keep in mind, this
 *  				function can fail if resizing #ska_dst fails.
 */

SlySr SK_Addr_assign_opt
(
	SK_Addr *		ska_dst,

	const SK_Addr *		ska_src,

	int			reset_flag,
	SK_AddrWordNum		reset_val
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska_dst, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska_src, SlySr_get (SLY_BAD_ARG));


	//Check if resizing #ska_dst is necessary

	if (ska_dst->num_words < ((size_t) ska_src->layer + 1))
	{
		SlySr sr =

		SK_Addr_resize (ska_dst, ((size_t) ska_src->layer + 1), 0, 0);


		SK_RES_RETURN (SK_ADDR_L_DEBUG, sr.res, sr);
	}


	//Set the layer of #ska_dst equal to the layer of #ska_src, and copy
	//each address word over

	ska_dst->layer = ska_src->layer;

	for (SK_AddrLayerNum l_sel = 0; l_sel <= ska_src->layer; l_sel += 1)
	{
		ska_dst->words [l_sel] = ska_src->words [l_sel];
	}


	//If #reset_flag orders this function to do so, reset the unused
	//portions of #ska_dst->words to #reset_val

	if (reset_flag)
	{
		for
		(
			SK_AddrLayerNum l_sel = (ska_dst->layer + 1);

			l_sel < ska_dst->num_words;

			l_sel += 1
		)
		{
			ska_dst->words [l_sel] = reset_val;
		}
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Checks whether 2 #SK_Addr's are equal or not. If they are not equal, it
 *  reports the highest layer at which they differ.
 *
 *  This function only compares equal layers which are at or below the 'layer'
 *  field. If the #SK_Addr's do not end at the same layer, then the (lower
 *  layer + 1) will be reported and the #SK_Addr's will be considered unequal.
 *
 *  @param ska_a		The 1st #SK_Addr to compare
 *
 *  @param ska_b		The 2nd #SK_Addr to compare
 *
 *  @param eq_flag		Where to place the result. This will be 1 if
 *  				#ska_a and #ska_b are equal at each layer, and
 *  				0 if they are not. This CAN be NULL if this
 *  				value is not desired.
 *
 *  @param diff_layer		Where to place the number of the highest layer
 *				at which the #SK_Addr's differed. 0 will be
 *				placed here in cases where #ska_a and #ska_b
 *				are equal. This CAN be NULL if this value is
 *				not desired.
 *
 *  @return			Standard status code. Failure will be reported
 *				if either #ska_a or #ska_b have not been
 *				initialized.
 */

SlyDr SK_Addr_eq
(
	SK_Addr *		ska_a,
	SK_Addr *		ska_b,
	int *			eq_flag,
	SK_AddrLayerNum *	diff_layer
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska_a, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska_b, SlyDr_get (SLY_BAD_ARG));


	//Make sure that #ska_a and #ska_b are initialized

	if ((0 == ska_a->num_words) || (0 == ska_b->num_words))
	{
		if (NULL != eq_flag)
		{
			*eq_flag = 0;
		}

		if (NULL != diff_layer)
		{
			*diff_layer = 0;
		}

		SK_DEBUG_PRINT
		(
			SK_ADDR_L_DEBUG,
			"Both #ska_a and #ska_b must be initialized. "
			"ska_a->num_words = %lu, "
			"ska_b->num_words = %lu",
			ska_a->num_words,
			ska_b->num_words
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Make sure that #ska_a and #ska_b go up to the same layer

	if (ska_a->layer > ska_b->layer)
	{
		if (NULL != eq_flag)
		{
			*eq_flag = 0;
		}

		if (NULL != diff_layer)
		{
			*diff_layer = (ska_b->layer + 1);
		}

		return SlyDr_get (SLY_SUCCESS);
	}

	else if (ska_a->layer < ska_b->layer)
	{
		if (NULL != eq_flag)
		{
			*eq_flag = 0;
		}

		if (NULL != diff_layer)
		{
			*diff_layer = (ska_a->layer + 1);
		}

		return SlyDr_get (SLY_SUCCESS);
	}


	//Compare each address word from top to bottom, and if a difference is
	//detected, indicate at which layer it occurred.

	if (NULL != eq_flag)
	{
		*eq_flag = 1;
	}

	if (NULL != diff_layer)
	{
		*diff_layer = 0;
	}


	for (i64 l_sel = ska_a->layer; l_sel >= 0; l_sel -= 1)
	{
		if (ska_a->words [l_sel] != ska_b->words [l_sel])
		{
			if (NULL != eq_flag)
			{
				*eq_flag = 0;
			}

			if (NULL != diff_layer)
			{
				*diff_layer = l_sel;
			}

			break;
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the highest layer stored in a #SK_Addr, resizing it if necessary.
 *
 *  @param ska			The #SK_Addr to modify
 *
 *  @param layer		The highest layer #ska should be capable
 *				of storing
 *
 *  @return			Standard status code.
 */

SlySr SK_Addr_set_layer
(
	SK_Addr *		ska,
	SK_AddrLayerNum		layer
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Check if #ska needs to be resized to store the given layer

	if (((size_t) layer + 1) > ska->num_words)
	{
		SlySr sr = SK_Addr_resize (ska, ((size_t) layer + 1), 1, 0);

		SK_RES_RETURN (SK_ADDR_L_DEBUG, sr.res, sr);
	}


	//Set the highest layer in #ska to the desired value

	ska->layer = layer;

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Reports the highest layer stored in a #SK_Addr
 *
 *  @param ska			The #SK_Addr to examine
 *
 *  @param layer		Where to store the number of the highest layer
 *				of #ska
 *
 *  @return			Standard status code
 */

SlyDr SK_Addr_get_layer
(
	SK_Addr *		ska,
	SK_AddrLayerNum *	layer
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	//Report the highest layer in #ska

	*layer = ska->layer;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets one of the address words associated with a specified layer to a given
 *  value.
 *
 *  @param ska			The #SK_Addr to modify
 *
 *  @param layer		The number of the layer associated with the
 *  				address word to modify
 *
 *  @param word			The value to set the specified layer's address
 *  				word to
 *
 *  @return			Standard status code. This will fail if the
 *  				highest layer in #ska is less than #layer.
 */

SlyDr SK_Addr_set_word
(
	SK_Addr *		ska,
	SK_AddrLayerNum		layer,
	SK_AddrWordNum		word
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	//Check if #layer is higher than the highest layer in #ska

	if ((layer > ska->layer) || (0 == ska->num_words))
	{
		SK_DEBUG_PRINT
		(
			SK_ADDR_L_DEBUG,
			"Could not SET address word % " PRIu8
			"to %" PRIu8 " for #SK_Addr at %p",
			layer,
			word,
			ska
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Set the specified address word equal to #word

	ska->words [layer] = word;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the address word associated with a given layer from a #SK_Addr.
 *
 *  @param ska			The #SK_Addr to examine
 *
 *  @param layer		The layer to pull an address from. This
 *  				function will fail if this layer is higher than
 *  				what is stored in #ska. Ergo, this can have a
 *  				value from [0, ska->layer]
 *
 *  @param word			Where to place the address word at layer number
 *				#layer
 *
 *  @return			Standard status code.
 *
 */

SlyDr SK_Addr_get_word
(
	SK_Addr *		ska,
	SK_AddrLayerNum		layer,
	SK_AddrWordNum *	word
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_ADDR_L_DEBUG, word, SlyDr_get (SLY_BAD_ARG));


	//Check if #layer is higher than the highest layer in #ska

	*word = 0;

	if ((layer > ska->layer) || (0 == ska->num_words))
	{
		SK_DEBUG_PRINT
		(
			SK_ADDR_L_DEBUG,
			"Could not GET address word % " PRIu8
			"from #SK_Addr at %p",
			layer,
			ska
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Fetch the address word at #layer

	*word = ska->words [layer];


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Prints out the contents of an #SK_Addr
 *
 *  @param ska				The #SK_Addr to print out
 *
 *  @param f_ptr			The #FILE to print to, like #stdout
 *
 *  @param deco				An #SK_LineDeco describing how to
 *					decorate the printed lines
 *
 *  @param header_flag			A flag indicating whether or not to
 *					print out the information header above
 *					the #SK_Addr
 *
 *  @param all_words_flag		A flag indicating whether or not to
 *					print out address words at indices
 *					higher than #ska->layer, if there are
 *					any in the first place. These words are
 *					typically stale data.
 *
 *  @return				Standard status code.
 */

SlyDr SK_Addr_print_opt
(
	SK_Addr *	ska,
	FILE *		f_ptr,
	SK_LineDeco	deco,
	int		header_flag,
	int		all_words_flag
)
{
	SK_NULL_RETURN (SK_ADDR_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_ADDR_L_DEBUG, f_ptr, SlyDr_get (SLY_BAD_ARG));


	//Print out the header (if told to do so)

	if (header_flag)
	{
		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"SK_Addr   @ %p",
			ska
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"num_words = %lu",
			(long unsigned) ska->num_words
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"layer     = %" PRIu32,
			ska->layer
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"words     = %p",
			ska->words
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			""
		);
	}


	//Print out the address words that are in use

	if (0 < ska->num_words)
	{
		for (SK_AddrLayerNum l_sel = 0; l_sel <= ska->layer; l_sel += 1)
		{
			SK_LineDeco_print_opt
			(
				f_ptr,
				deco,
				"\t%03" PRIu32 " : %03" PRIu8,
				l_sel,
				ska->words [l_sel]
			);
		}
	}

	else
	{
		SK_LineDeco_print_opt (f_ptr, deco, "\t(Address Words Empty)");
	}


	//Print out the unused address words if instructed to do so

	if (all_words_flag && (0 < ska->num_words))
	{
		for (SK_AddrLayerNum l_sel = (ska->layer + 1); l_sel < ska->num_words; l_sel += 1)
		{
			SK_LineDeco_print_opt
			(
				f_ptr,
				deco,
				"\t%03" PRIu32 " : %03" PRIu8 " (unused)",
				l_sel,
				ska->words [l_sel]
			);
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




//FIXME : A standard should be decided upon for what/when test-benches print
//throughout this project




//FIXME : Many of these test-benches require manual observation of the output;
//this should be rectified if possible




/*!
 *  Performs a simple test-bench on SK_Addr_create_bare ()
 *
 *  @return		Standard status code
 */

SlySr SK_Addr_create_bare_tb ()
{
	printf ("\n\n--------SK_Addr_create_bare_tb ()--------\n\n\n");


	const u32 NUM_OBJ = 10;

	for (u32 obj_sel = 0; obj_sel < NUM_OBJ; obj_sel += 1)
	{
		SlySr sr;


		SK_Addr * ska = SK_Addr_create_bare ((size_t) obj_sel, 0, &sr);

		if (NULL == ska)
		{
			SK_DEBUG_PRINT
			(
				SK_ADDR_L_DEBUG,
				"Failed to create SK_Addr #%" PRIu32,
				obj_sel
			);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		printf
		(
			"#%" PRIu32 " : sr = %s, ska = %p\n",
			obj_sel,
			SlySrStr_get (sr).str,
			ska
		);


		SK_Addr_print_opt (ska, stdout, SK_LineDeco_std (), 1, 1);


		printf ("\n");


		SK_Addr_destroy (ska);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test-bench on SK_Addr_create ()
 *
 *  @return		Standard status code
 */

SlySr SK_Addr_create_tb ()
{
	printf ("\n\n--------SK_Addr_create_tb ()--------\n\n\n");


	const u32 NUM_OBJ = 10;

	for (u32 obj_sel = 0; obj_sel < NUM_OBJ; obj_sel += 1)
	{
		SlySr sr = SlySr_get (SLY_SUCCESS);


		SK_Addr * ska =

		SK_Addr_create
		(
			(SK_AddrLayerNum) obj_sel,
			(SK_AddrWordNum) obj_sel * 2,
			&sr
		);


		if (NULL == ska)
		{
			SK_DEBUG_PRINT
			(
				SK_ADDR_L_DEBUG,
				"Failed to create SK_Addr #%" PRIu32,
				obj_sel
			);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		printf
		(
			"#%" PRIu32 " : sr = %s, ska = %p\n",
			obj_sel,
			SlySrStr_get (sr).str,
			ska
		);


		SK_Addr_print_opt (ska, stdout, SK_LineDeco_std (), 1, 1);


		printf ("\n");


		SK_Addr_destroy (ska);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test-bench on SK_Addr_create_copy ()
 *
 *  @return		Standard status code
 */

SlySr SK_Addr_create_copy_tb ()
{
	printf ("\n\n--------SK_Addr_create_copy_tb ()--------\n\n\n");


	const u32 NUM_OBJ = 10;

	for (u32 obj_sel = 0; obj_sel < NUM_OBJ; obj_sel += 1)
	{
		SlySr sr_0 = SlySr_get (SLY_UNKNOWN_ERROR);
		SlySr sr_1 = SlySr_get (SLY_UNKNOWN_ERROR);


		//Create an initial object

		SK_Addr * ska =

		SK_Addr_create
		(
			(SK_AddrLayerNum) obj_sel,
			(SK_AddrWordNum) obj_sel * 2,
			&sr_0
		);


		if (NULL == ska)
		{
			SK_DEBUG_PRINT
			(
				SK_ADDR_L_DEBUG,
				"Failed to create SK_Addr #%" PRIu32,
				obj_sel
			);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		//Create a copy of that object

		SK_Addr * ska_copy = SK_Addr_create_copy (ska, &sr_1);

		if (NULL == ska_copy)
		{
			SK_DEBUG_PRINT
			(
				SK_ADDR_L_DEBUG,
				"Failed to copy #ska, #%" PRIu32,
				obj_sel
			);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		//Print the original #SK_Addr and the copy

		printf
		(
			"#%" PRIu32 " : "
			"sr_0 = %s, ska = %p, sr_1 = %s, ska_copy = %p\n\n",
			obj_sel,
			SlySrStr_get (sr_0).str,
			ska,
			SlySrStr_get (sr_1).str,
			ska_copy
		);


		SK_Addr_print_opt (ska, stdout, SK_LineDeco_std (), 1, 1);
		SK_Addr_print_opt (ska_copy, stdout, SK_LineDeco_std (), 1, 1);

		printf ("\n");


		//Check to see if the 2 #SK_Addr's are actually copies

		if (ska->num_words != ska_copy->num_words)
		{
			SK_DEBUG_PRINT
			(
				SK_ADDR_L_DEBUG,
				"#ska->num_words not copied properly!"
			)

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}

		if (ska->layer != ska_copy->layer)
		{
			SK_DEBUG_PRINT
			(
				SK_ADDR_L_DEBUG,
				"#ska->num_words not copied properly!"
			)

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}

		for (size_t l_sel = 0; l_sel <= ska->layer; l_sel += 1)
		{
			if (ska->words [l_sel] != ska_copy->words [l_sel])
			{
				SK_DEBUG_PRINT
				(
					SK_ADDR_L_DEBUG,
					"#ska->words [%" PRIu32 "] not copied properly!",
					(u32) l_sel
				);

				return SlySr_get (SLY_UNKNOWN_ERROR);
			}
		}


		//Free any memory allocated in this test iteration

		SK_Addr_destroy (ska);
		SK_Addr_destroy (ska_copy);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test-bench on SK_Addr_resize ()
 *
 *  @return		Standard status code
 */

SlySr SK_Addr_resize_tb ()
{
	printf ("\n\n--------SK_Addr_resize_tb ()--------\n\n\n");


	const u32 MAX_NUM_WORDS = 10;


	//Create an #SK_Addr at some initial num_words, and then resize it. At
	//each step, make sure the number of words in #ska is correct

	for (u32 start_num_words = 0; start_num_words < MAX_NUM_WORDS; start_num_words += 1)
	{
		for (u32 new_num_words = 0; new_num_words < MAX_NUM_WORDS; new_num_words += 1)
		{

			//Set some arbitrary parameters for this test
			//iteration, and then print them out

			int preserve_flag =

			(start_num_words + new_num_words) % 2;


			SK_AddrWordNum reset_val =

			(start_num_words + new_num_words);


			printf ("================\n\n");

			printf
			(
				"start_num_words = %" PRIu32 "\n"
				"new_num_words =   %" PRIu32 "\n"
				"preserve_flag =   %d\n"
				"reset_val =       %" PRIu8 "\n"
				"\n",
				start_num_words,
				new_num_words,
				preserve_flag,
				reset_val
			);


			//Create the initial #SK_Addr, and make sure it has the
			//correct number of words in it

			SlySr sr;

			SK_Addr * ska = SK_Addr_create_bare (start_num_words, 0, &sr);

			SK_RES_RETURN (SK_ADDR_L_DEBUG, sr.res, sr);

			if (start_num_words != ska->num_words)
			{
				SK_DEBUG_PRINT
				(
				 	SK_ADDR_L_DEBUG,
					"#ska was not at num_words #start_num_words"
				);

				SK_Addr_destroy (ska);

				return SlySr_get (SLY_UNKNOWN_ERROR);
			}


			//Fill the #SK_Addr with arbitrary data

			if (0 < start_num_words)
			{
				ska->layer = start_num_words - 1;

				for (size_t l_sel = 0; l_sel < ska->num_words; l_sel += 1)
				{
					ska->words [l_sel] = l_sel;
				}
			}


			//Print the original #SK_Addr

			SK_Addr_print_opt (ska, stdout, SK_LineDeco_std (), 1, 1);

			printf ("\n");


			//Resize the #SK_Addr

			sr =

			SK_Addr_resize
			(
				ska,
				new_num_words,
				preserve_flag,
				reset_val
			);

			if (SLY_SUCCESS != sr.res)
			{
				SK_Addr_destroy (ska);

				SK_RES_RETURN (SK_ADDR_L_DEBUG, sr.res, sr);
			}


			//Print out the resized #SK_Addr

			SK_Addr_print_opt (ska, stdout, SK_LineDeco_std (), 1, 1);


			//Check if the resized #SK_Addr had the correct number
			//of words inside of it

			if (new_num_words != ska->num_words)
			{
				SK_DEBUG_PRINT
				(
					SK_ADDR_L_DEBUG,
					"#ska was not at num_words #new_num_words"
				);

				SK_Addr_destroy (ska);

				return SlySr_get (SLY_UNKNOWN_ERROR);
			}



			//Destroy any objects created in this test iteration

			SK_Addr_destroy (ska);


			printf ("\n================\n\n");
		}
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test-bench on SK_Addr_assign_opt ()
 *
 *  @return		Standard status code
 */

SlySr SK_Addr_assign_opt_tb ()
{
	printf ("\n\n--------SK_Addr_assign_opt_tb ()--------\n\n\n");


	const u32 MAX_SRC_LAYER = 8;
	const u32 MAX_DST_LAYER = 8;


	for (u32 src_layer = 0; src_layer <= MAX_SRC_LAYER; src_layer += 1)
	{
		for (u32 dst_layer = 0; dst_layer <= MAX_DST_LAYER; dst_layer += 1)
		{
			//Create some arbitrary #SK_Addr's

			SlySr src_sr = SlySr_get (SLY_SUCCESS);
			SlySr dst_sr = SlySr_get (SLY_SUCCESS);


			SK_Addr * src_ska = SK_Addr_create (src_layer, src_layer * 10, &src_sr);

			SK_RES_RETURN (SK_ADDR_L_DEBUG, src_sr.res, src_sr);


			SK_Addr * dst_ska = SK_Addr_create (dst_layer, dst_layer * 1000, &dst_sr);

			if (SLY_SUCCESS != dst_sr.res)
			{
				SK_Addr_destroy (src_ska);

				SK_RES_RETURN (SK_ADDR_L_DEBUG, dst_sr.res, dst_sr);
			}


			//Assign #dst_ska to the contents of #src_ska, and then
			//print them out

			SK_Addr_assign_opt (dst_ska, src_ska, 1, 0);


			printf ("================\n\n");

			printf
			(
				"src_layer = %" PRIu32 ", dst_layer = %" PRIu32 "\n",
				src_layer,
				dst_layer
			);

			printf ("\n----src_ska----\n\n");

			SK_Addr_print_opt (src_ska, stdout, SK_LineDeco_std (), 1, 1);

			printf ("\n----dst_ska----\n\n");

			SK_Addr_print_opt (dst_ska, stdout, SK_LineDeco_std (), 1, 1);

			printf ("\n================\n\n");


			//Ensure that #dst_ska is at least initialized

			if (0 == dst_ska->num_words)
			{
				SK_DEBUG_PRINT
				(
					SK_ADDR_L_DEBUG,
					"dst_ska->num_words was 0, which is wrong."
				);

				return SlySr_get (SLY_UNKNOWN_ERROR);
			}


			//Ensure that all of the words in #src_ska are present
			//in #dst_ska

			for (size_t w_sel = 0; w_sel <= src_ska->layer; w_sel += 1)
			{
				if (dst_ska->words [w_sel] != src_ska->words [w_sel])
				{
					SK_DEBUG_PRINT
					(
						SK_ADDR_L_DEBUG,
						"dst_ska->words [%lu] = %" PRIu8 ", "
						"src_ska->words [%lu] = %" PRIu8 ". "
						"This is incorrect!",
						(long unsigned) w_sel,
						dst_ska->words [w_sel],
						(long unsigned) w_sel,
						src_ska->words [w_sel]
					);

					return SlySr_get (SLY_UNKNOWN_ERROR);
				}
			}


			//Ensure that all of the words in #dst_ska that did not
			//originate from #src_ska are properly reset

			for
			(
				size_t w_sel = (src_ska->layer + 1);

				w_sel < dst_ska->num_words;

				w_sel += 1
			)
			{
				if (0 != dst_ska->words [w_sel])
				{
					SK_DEBUG_PRINT
					(
						SK_ADDR_L_DEBUG,
						"dst_ska->words [%lu] = %" PRIu8 ", "
						"but it should be reset to 0.",
						(long unsigned) w_sel,
						dst_ska->words [w_sel]
					);

					return SlySr_get (SLY_UNKNOWN_ERROR);
				}
			}


			SK_Addr_destroy (src_ska);
			SK_Addr_destroy (dst_ska);
		}
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test-bench on SK_Addr_eq ()
 *
 *  @return		Standard status code
 */

SlySr SK_Addr_eq_tb ()
{
	printf ("\n\n--------SK_Addr_eq_tb ()--------\n\n\n");


	const u32 MAX_SKA_0_LAYER = 8;
	const u32 MAX_SKA_1_LAYER = 8;


	for (u32 ska_0_layer = 0; ska_0_layer <= MAX_SKA_0_LAYER; ska_0_layer += 1)
	{
		for (u32 ska_1_layer = 0; ska_1_layer <= MAX_SKA_1_LAYER; ska_1_layer += 1)
		{
			//Create some arbitrary #SK_Addr's

			SlySr ska_0_sr = SlySr_get (SLY_SUCCESS);
			SlySr ska_1_sr = SlySr_get (SLY_SUCCESS);

			SK_AddrWordNum ska_0_init_val = ska_0_layer % 2;
			SK_AddrWordNum ska_1_init_val = ska_1_layer % 3;



			SK_Addr * ska_0 = SK_Addr_create (ska_0_layer, ska_0_init_val, &ska_0_sr);

			SK_RES_RETURN (SK_ADDR_L_DEBUG, ska_0_sr.res, ska_0_sr);


			SK_Addr * ska_1 = SK_Addr_create (ska_1_layer, ska_1_init_val, &ska_1_sr);

			if (SLY_SUCCESS != ska_1_sr.res)
			{
				SK_Addr_destroy (ska_0);

				SK_RES_RETURN (SK_ADDR_L_DEBUG, ska_1_sr.res, ska_1_sr);
			}


			//Check the equality of #ska_0 and #ska_1

			int eq_flag = 0;

			SK_AddrLayerNum diff_layer = 0;


			SlyDr eq_dr =

			SK_Addr_eq (ska_0, ska_1, &eq_flag, &diff_layer);


			SK_RES_RETURN (SK_ADDR_L_DEBUG, eq_dr.res, SlySr_get (SLY_UNKNOWN_ERROR));


			//Print the results of the equality operation

			printf ("\n----ska_0----\n\n");

			SK_Addr_print_opt (ska_0, stdout, SK_LineDeco_std (), 1, 1);

			printf ("\n----ska_1----\n\n");

			SK_Addr_print_opt (ska_1, stdout, SK_LineDeco_std (), 1, 1);

			printf
			(
				"\n"
				"eq_flag =    %d\n"
				"diff_layer = %" PRIi64 "\n"
				"\n",
				eq_flag,
				(i64) diff_layer
			);

			printf ("\n================\n\n");


			//Check if the results of the equality make sense

			if (eq_flag && (ska_0_layer != ska_1_layer))
			{
				SK_DEBUG_PRINT
				(
					SK_ADDR_L_DEBUG,
					"#eq_flag was HIGH even though "
					"(ska_0_layer != ska_1_layer)"
				);

				return SlySr_get (SLY_UNKNOWN_ERROR);
			}


			if (eq_flag && (ska_0_init_val != ska_1_init_val))
			{
				SK_DEBUG_PRINT
				(
					SK_ADDR_L_DEBUG,
					"#eq_flag was HIGH even though "
					"(ska_0_init_val != ska_1_init_val)"
				);

				return SlySr_get (SLY_UNKNOWN_ERROR);

			}


			if
			(
				(! eq_flag)

				&&

				(ska_0_layer == ska_1_layer)

				&&

				(ska_0_init_val == ska_1_init_val)
			)
			{
				SK_DEBUG_PRINT
				(
					SK_ADDR_L_DEBUG,
					"#eq_flag was LOW even though #ska_0 "
					"should have been equal to #ska_1"
				);

				return SlySr_get (SLY_UNKNOWN_ERROR);
			}


			if
			(
				(ska_0_layer > ska_1_layer)

				&&

				((ska_1_layer + 1) != diff_layer)
			)
			{
				SK_DEBUG_PRINT
				(
					SK_ADDR_L_DEBUG,
					"#diff_layer was not equal to "
					"(ska_1_layer + 1) when it should "
					"have been"
				);

				return SlySr_get (SLY_UNKNOWN_ERROR);
			}


			if
			(
				(ska_0_layer < ska_1_layer)

				&&

				((ska_0_layer + 1) != diff_layer)
			)
			{
				SK_DEBUG_PRINT
				(
					SK_ADDR_L_DEBUG,
					"#diff_layer was not equal to "
					"(ska_0_layer + 1) when it should "
					"have been"
				);

				return SlySr_get (SLY_UNKNOWN_ERROR);
			}


			//Free any objects created in this test iteration

			SK_Addr_destroy (ska_0);
			SK_Addr_destroy (ska_1);
		}
	}


	return SlySr_get (SLY_SUCCESS);
}

