/*!****************************************************************************
 *
 *  @file
 *  SK_Addr.h
 *
 *  Implements a data structure that is capable of storing a spek-net address
 *  at a given layer.
 *
 *****************************************************************************/




#ifndef SK_ADDR_H
#define SK_ADDR_H




#include "SK_debug.h"
#include "SK_misc.h"




#define SK_ADDR_BITS_PER_ADDR_WORD		(4)




typedef u32 SK_AddrLayerNum;
typedef u8  SK_AddrWordNum;




typedef struct SK_Addr SK_Addr;




SK_Addr * SK_Addr_create
(
	SK_AddrLayerNum		layer,
	SK_AddrWordNum		init_words,

	SlySr *			sr_ptr
);




SK_Addr * SK_Addr_create_copy
(
	SK_Addr *	ska_src,
	SlySr *		sr_ptr
);




SlyDr SK_Addr_print_opt
(
	SK_Addr *	ska,
	FILE *		f_ptr,
	SK_LineDeco	deco,
	int		header_flag,
	int		all_words_flag
);




SlyDr SK_Addr_destroy (SK_Addr * ska);




SlyDr SK_Addr_eq
(
	SK_Addr *		ska_a,
	SK_Addr *		ska_b,
	int *			eq_flag,
	SK_AddrLayerNum *	diff_layer
);




SlySr SK_Addr_create_bare_tb ();




SlySr SK_Addr_create_tb ();




SlySr SK_Addr_create_copy_tb ();




SlySr SK_Addr_resize_tb ();




SlySr SK_Addr_assign_opt_tb ();




SlySr SK_Addr_eq_tb ();









#endif //SK_ADDR_H

