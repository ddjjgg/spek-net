/*!****************************************************************************
 *
 * @file
 * SK_HokePl.h
 *
 * See SK_HokePl.c for details.
 *
 *****************************************************************************/




#ifndef SK_HOKEPL_H
#define SK_HOKEPL_H




#include "./SK_HokePl_opaque.h"




#include "./SK_Hoke.h"
#include "../SK_Vector.h"




/*!
 *  A version of #SK_Hoke capable of storing custom per-link properties.
 *
 *  FIXME : Can the work for this data structure somehow re-use the work for
 *  #SK_YokePl?
 */

typedef struct SK_HokePl
{

	//Used to handle the job of establishing networks.

	SK_Hoke		hoke;


	/// An #SK_Vector containing the actual link properties associated with
	/// the individual links in #yoke
	///
	/// FIXME : Is an #SK_Vector good here, or is an array of void
	/// pointer's better?

	SK_Vector *	lp;
}

SK_HokePl;




#endif //SK_HOKEPL_H

