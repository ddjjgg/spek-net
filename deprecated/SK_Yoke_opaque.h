/*!****************************************************************************
 *
 * @file
 * SK_Yoke_opaque.h
 *
 * See SK_Yoke.c for details.
 *
 *****************************************************************************/




#ifndef SK_YOKE_OPAQUE_H
#define SK_YOKE_OPAQUE_H




#include <stdio.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "SK_misc.h"
#include "SK_numeric_types.h"




typedef struct SK_YokeLink SK_YokeLink;




typedef struct SK_Yoke SK_Yoke;




SlySr SK_YokeLink_print_opt
(
	SK_YokeLink	skl,
	FILE *		f_ptr,
	SK_LineDeco	deco
);




SlySr SK_Yoke_print_opt
(
	const SK_Yoke *		sky,
	FILE *			f_ptr,
	SK_LineDeco		deco,
	int			header_flag,
	int			active_links_flag,
	int			inactive_links_flag
);




SlySr SK_Yoke_print
(
	const SK_Yoke *		sky,
	FILE *			file,
	SK_LineDeco		deco
);




SK_Yoke * SK_Yoke_create
(
	size_t		links_len,
	SlySr *		sr_ptr
);




SlyDr SK_Yoke_destroy (SK_Yoke * sky);




SlySr  SK_Yoke_links_realloc
(
	SK_Yoke *	sky,
	size_t		new_links_len,
	int		preserve_flag
);




SlySr SK_Yoke_links_minify
(
	SK_Yoke *	sky
);




SlyDr SK_Yoke_link_exists
(
	SK_Yoke *	sky_0,
	SK_Yoke *	sky_1,
	int *		link_exists,
	size_t *	sky_0_l_ind,
	size_t *	sky_1_l_ind
);




SlyDr SK_Yoke_get_num_links
(
	SK_Yoke *	sky,
	size_t *	num_active_links,
	size_t *	num_max_links
);




SlySr SK_Yoke_link_no_alloc_w_info
(
	SK_Yoke *	sky_0,
	int *		need_realloc_0,
	size_t *	sky_0_l_ind,

	SK_Yoke *	sky_1,
	int *		need_realloc_1,
	size_t *	sky_1_l_ind,

	int *		link_existed
);




SlySr SK_Yoke_link_no_alloc
(
	SK_Yoke *	sky_0,

	SK_Yoke *	sky_1
);




SlyDr SK_Yoke_unlink_w_info
(
	SK_Yoke *	sky_0,
	SK_Yoke *	sky_1,
	int *		link_existed,
	size_t *	sky_0_l_ind,
	size_t *	sky_1_l_ind
);




SlyDr SK_Yoke_unlink
(
	SK_Yoke *	sky_0,

	SK_Yoke *	sky_1
);




SlyDr SK_Yoke_unlink_all
(
	SK_Yoke *	skn
);




int SK_YokeLink_is_active
(
	SK_YokeLink	skl
);




SlySr SK_Yoke_link_w_info
(
	SK_Yoke *	sky_0,
	size_t *	sky_0_l_ind,
	size_t		sky_0_incr,

	SK_Yoke *	sky_1,
	size_t *	sky_1_l_ind,
	size_t		sky_1_incr,

	int *		link_existed
);




SlySr SK_Yoke_link
(
	SK_Yoke *	sky_0,
	size_t		sky_0_incr,

	SK_Yoke *	sky_1,
	size_t		sky_1_incr
);




SlyDr SK_Yoke_error_check
(
	SK_Yoke *	sky,
	FILE *		f_ptr,
	const char *	pre_str,
	const char *	post_str,
	int *		error_flag
);




SlySr SK_Yoke_create_tb ();




SlySr SK_Yoke_link_no_alloc_w_info_tb ();




SlySr SK_Yoke_unlink_w_info_tb ();




SlySr SK_Yoke_error_check_tb ();




SlySr SK_Yoke_links_realloc_tb ();




SlySr SK_Yoke_link_w_info_tb ();




#endif //SK_YOKE_OPAQUE_H

