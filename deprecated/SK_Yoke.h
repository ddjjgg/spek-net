/*!****************************************************************************
 *
 * @file
 * SK_Yoke.h
 *
 * See SK_Yoke.c for details.
 *
 *****************************************************************************/




#ifndef SK_YOKE_H
#define SK_YOKE_H




#include "SK_Yoke_opaque.h"




#include "SK_YokeIntf.h"




/*!
 *  A data structure used to represent a link _to_ an #SK_Yoke.
 *
 *  This data structure is intended to be used inside of an #SK_Yoke, and in
 *  this example it represents a link from one #SK_Yoke to another #SK_Yoke.
 *
 *  @warning
 *  While it is theoretically possible to include this data structure
 *  in other types of objects, that is unlikely to be desirable. The
 *  #SK_YokeLink's inside of the destination #SK_Yoke will not be possible to
 *  properly set up in reponse, because they expect to be connected to
 *  #SK_Yoke's (which is not what the source object is). Therefore, only
 *  1-sided links can be represented by #SK_YokeLink's outside of #SK_Yoke.
 *
 *  To be explicit, if connection networks need to be represented between
 *  objects, #SK_Yoke should be used, and NOT #SK_YokeLink.
 */

typedef struct SK_YokeLink
{
	/// Pointer to an #SK_Yoke that the object containing this #SK_YokeLink
	/// is connected to.
	///
	/// If this value is ever NULL, that indicates the #SK_YokeLink is
	/// unassigned and therefore inactive.

	SK_Yoke *	dst_yoke;


	/// The index of the #SK_YokeLink inside of #dst_yoke->links that links
	/// back to the #SK_Yoke that contains this #SK_YokeLink.
	///
	/// Why is this kept track of? It makes the process of unlinking
	/// quicker, because finding the links that correspond to each other in
	/// 2 different #SK_Yoke's only requires iterating through one of the
	/// #SK_Yoke's.

	size_t		back_ind;


	//FIXME : Remove #u_en and #d_en. #SK_YokeLink should contain the bare
	//minimum information needed to implement links between #SK_Yoke's.
	//
	//If additional link properties are needed, #SK_YokePl can be used.


	// /// Flag indicating _uploading_ information over this link is enabled
	//
	// u8		u_en;


	// /// Flag indicating _downloading_ information over this link is enabled
	//
	// u8		d_en;
}

SK_YokeLink;




/*!
 *  An #SK_Yoke keeps track of links to other #SK_Yoke's.
 *
 *  Why is this useful? When this data structure is included in other objects,
 *  it allows one to form networks of those objects easily. What connetions in
 *  those networks represent is up to the object that includes the #SK_Yoke.
 *
 *  The #SK_Yoke will take care of the memory management necessary to establish
 *  / break connections in that network. Additionally, #SK_Yoke's are intended
 *  to obviate dealing with the problem of "stale links", which is when one
 *  object thinks it is connected to another object, but the other object
 *  thinks that the connection is non-existant. Stale links can lead to very
 *  subtle and hard to detect memory access bugs, because when the "other
 *  object" is destroyed the stale link will prevent it from informing the
 *  original object.
 *
 *  Metaphorically, if an object were an animal, then the #SK_Yoke is a yoke
 *  placed upon it, and this can be used to make it work with another animals.
 *
 *  In a way, #SK_Yoke's implicitly describe a sort of linked-list, but unlike
 *  most other linked-list implementations, the number of links per node is
 *  modifiable, and it does not have to be the same for every node.
 *
 *  @warning
 *  #SK_Yoke's and the functions provided to operate on them are NOT
 *  thread-safe. It is the responsibilty of some higher-level object to wrap a
 *  collection of #SK_Yoke's to provide thread-safety.
 *
 *  FIXME : Is this a good idea? Should #SK_Yoke provide thread-safety itself?
 *  How would a higher-level object provide thread-safety when
 *  #SK_Yoke_links_realloc () is called, in which case all the references to
 *  #yoke->links [n].back_ind have to be updated in all the neighbor nodes?
 *
 *  (Well, that higher level data structure could set locks associated with all
 *  of the neighbor nodes before doing that then. It seems to me that #SK_Yoke
 *  would have a harder time of avoiding dead-lock if it tried being
 *  thread-safe itself. It seems like a more appropriate job for the data
 *  structure managing the pool of #SK_Yoke's and thread resources, albeit it
 *  must have intimate knowledge of the side-effects of different #SK_Yoke
 *  functions. Hmmmmm......)
 *
 *  @warning
 *  If establishing a network between objects of different data types is
 *  desired, it is recommended to use #SK_Hoke instead of #SK_Yoke.
 */


//FIXME : Consider using hashing to place links to other #SK_Yoke's inside of
//the #links array. As it stands right now, it is thought that most realistic
//#SK_Yoke's won't have enough links to make this technique worth it.


//FIXME : This structure resembles a linked-list node, and implicitly describes
//a network layout by containing links to other nodes. How should operations on
//#SK_Yoke's be made thread-safe? Consider making a data structure #SK_YokeSet
//that contains a set of #SK_Yoke's, and issues linking/unlinking commands to
//this set of #SK_Yoke's in a thread-safe manner.


//FIXME : Consider using #SK_Vector inside of #SK_Yoke to handle the cases
//where the number of links needs to be resized


//FIXME : Should #SK_Yoke contain a small constant-sized array of
//#SK_YokeLink's so that it can be used without a memory allocations when it doesn't
//contain very many links? This does add a constant overhead to #SK_Yoke
//instantiations, however.


typedef struct SK_Yoke
{
	/// An array of #SK_YokeLink's tracking which #SK_Yoke's are connected
	/// to this yoke.
	///
	/// An important aspect to note about #links, there should be NO
	/// duplicates inside of #links. For a given destination #SK_Yoke,
	/// there should only be 1 link to it inside of #links.
	///
	/// Care should be taken such that when an #SK_Yoke on one side of a
	/// link is updated, the #SK_Yoke on the other side of the link is
	/// updated in kind.  Otherwise, memory access errors WILL occur. View
	/// #SK_YokeLink to see what information is tracked about neighbor
	/// #SK_Yoke's.

	SK_YokeLink *	links;


	/// The number of elements allocated at #links

	size_t		links_len;


	/// The number of #SK_YokeLink's inside of #links that have been
	/// assigned and are currently considered active.
	///
	/// While this counts the number of active links, it does not mean that
	/// the links from #links [0] to #links [links_cnt - 1] are necessarily
	/// the active ones. As #SK_YokeLink's are activated / deactivated in
	/// #links, they may become out-of-order and noncontiguous in #links.

	size_t		links_cnt;
}

SK_Yoke;




size_t SK_Yoke_get_sizeof ();




SlySr SK_Yoke_init
(
	SK_Yoke *	sky,
	size_t		links_len
);




SlyDr SK_Yoke_deinit (SK_Yoke * sky);




extern const SK_YokeIntf SK_Yoke_as_self;




#endif //SK_YOKE_H

