/*!****************************************************************************
 *
 * @file
 * SK_YokeSet.h
 *
 * Contains an implementation of #SK_YokeSet, which is a collection of
 * #SK_Yoke's. The benefit of having them placed into a collection is that they
 * can be updated in a thread-safe manner.
 *
 *****************************************************************************/




#ifndef SK_YOKESET_H
#define SK_YOKESET_H




SlyDr SK_Vector_YokeSet2Op_init_no_op
(
	u8 *		elem_ptr,
	size_t		index,
	void *		arg
);




SlySr SK_YokeSet_create_tb ();




#endif //SK_YOKESET_H

