/*!****************************************************************************
 *
 * @file
 * SK_Vector.c
 *
 * Contains an implementation of #SK_Vector, which is an array-like data
 * structure that is quicker to resize than an array of the same size, but
 * slower at element accesses.
 *
 * This is truly an example of re-inventing the wheel, but it's fun!
 *
 *
 * -- KNOWN ISSUES --
 *
 * 1)
 * 	The various SK_Vector_* () functions are not as fast as they could be,
 * 	because no loop-unrolling is performed in SK_Vector_set_elem () and
 * 	SK_Vector_get_elem (). No loop unrolling performed in any of the
 * 	SK_Vector_init_* functions either.
 *
 * 	As a result, the loops iterate byte-by-byte, which is substantially
 * 	slower than iterating by groups of 4 bytes or 8 bytes.
 *
 * 	As a result, #SK_Vector is slower than plain arrays at "random resizes"
 * 	until the min/max size of the array gets fairly large (2048 elements or
 * 	so), and the min/max size are in about ~25% of each other. (The
 * 	#SK_Vector still usually wins in cases element appending/truncating,
 * 	however.)
 *
 * 2)
 * 	This data-structure needs to be re-developed in order to implement
 * 	#SK_BoxIntf. In fact, doing this would make problem 1) a solved problem
 * 	due to #prim_flag, #prim_copy, and #prim_swap.
 *
 *****************************************************************************/




#include "SK_Vector.h"




#include <stdlib.h>
#include <SlyResult-0-x-x.h>
#include <time.h>




#include "./SK_debug.h"
#include "./SK_misc.h"
#include "./SK_numeric_types.h"




/*!
 *  Calculates the minimum number of blocks needed to hold a total number of
 *  elements in an #SK_Vector
 *
 *  @param elems_per_block	The number of elements in each block in the
 *				#SK_Vector
 *
 *  @param num_elems		The number of elements that need to be stored
 *
 *  @return			The minimum number of blocks that are required
 *  				to store #num_elems, if there are
 *  				#elems_per_block elements in each block
 */

size_t SK_Vector_calc_min_blocks
(
	size_t elems_per_block,
	size_t num_elems
)
{
	size_t num_blocks = (num_elems / elems_per_block);


	//Take into account integer division flooring

	if ((num_blocks * elems_per_block) < num_elems)
	{
		num_blocks += 1;
	}


	return num_blocks;
}




/*!
 *  Initializes an #SK_Vector which has had memory allocated for it but has not
 *  had any of its fields initialized.
 *
 *  Note, the suffix *_unsafe indicates that this function does not perform any
 *  sanity checking on the input arguments.
 *
 *  @param vec			The #SK_Vector to initialize
 *
 *  @param elem_size		See SK_Vector_create ()
 *
 *  @param num_elems		See SK_Vector_create ()
 *
 *  @param elems_per_block	See SK_Vector_create ()
 *
 *  @param neg_slack		See SK_Vector_create ()
 *
 *  @param pos_slack		See SK_Vector_create ()
 *
 *  @param init_func		See SK_Vector_create ()
 *
 *  @param init_func_arg	See SK_Vector_create ()
 *
 *  @param sr_ptr		See SK_Vector_create ()
 */

SlySr SK_Vector_init_unsafe
(
	SK_Vector *	vec,
	size_t		elem_size,
	size_t		num_elems,
	size_t		elems_per_block,
	size_t		neg_slack,
	size_t		pos_slack,
	SlyDr		(* init_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		init_func_arg
)
{

	//No sanity checking here, since this is the unsafe version of
	//SK_Vector_init* ()


	//Save the size of each element, the number of elements per block,
	//and the negative / positive slack

	vec->elem_size =	elem_size;

	vec->elems_per_block =	elems_per_block;

	vec->neg_slack =	neg_slack;

	vec->pos_slack =	pos_slack;


	//Determine the number of blocks that need to be allocated in order to
	//store #num_elems number of elements

	vec->num_blocks = SK_Vector_calc_min_blocks (elems_per_block, num_elems);


	//Take into account how the positive slack should affect the number of
	//blocks allocated right now.
	//
	//If no blocks would be allocated otherwise, then the positive slack
	//should not take effect here, because #vec->num_blocks will not be
	//considered "increasing" if being set to 0.

	if (0 < vec->num_blocks)
	{
		vec->num_blocks += pos_slack;
	}


	//Allocate the blocks, if any blocks should be allocated right now

	if (0 < vec->num_blocks)
	{
		vec->blocks = malloc (sizeof (SK_VectorBlock) * vec->num_blocks);

		if (NULL == vec)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Could not allocate #vec->blocks"
			);

			free (vec);

			return SlySr_get (SLY_BAD_ALLOC);
		}
	}

	else
	{
		vec->blocks = NULL;
	}


	//Allocate the arrays that should be inside each of the blocks

	int block_alloc_fail =		0;

	size_t fail_alloc_ind =		0;


	for (size_t b_sel = 0; b_sel < vec->num_blocks; b_sel += 1)
	{
		vec->blocks [b_sel].bytes = NULL;
	}


	for (size_t b_sel = 0; b_sel < vec->num_blocks; b_sel += 1)
	{
		vec->blocks [b_sel].bytes =

		malloc (sizeof (u8) * elem_size * elems_per_block);


		if (NULL == vec->blocks [b_sel].bytes)
		{
			block_alloc_fail =	1;

			fail_alloc_ind =	b_sel;

			break;
		}
	}


	if (block_alloc_fail)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Could not allocate arrays associated with each block"
		);


		for (size_t b_sel = 0; b_sel < fail_alloc_ind; b_sel += 1)
		{
			free (vec->blocks [b_sel].bytes);
		}


		free (vec->blocks);
		free (vec);

		return SlySr_get (SLY_BAD_ALLOC);
	}


	//Indicate the number of elements currently being stored in the
	//#SK_Vector, and initialize them using the provided function pointer

	vec->cur_num_elems = num_elems;

	if (num_elems > 0)
	{
		SK_Vector_init_range
		(
			vec,
			0,
			num_elems - 1,
			init_func,
			init_func_arg
		);
	}


	//Determine the maximum number of elements that could possibly be
	//stored in the #SK_Vector using the current number of allocated blocks

	vec->max_num_elems = (vec->elems_per_block * vec->num_blocks);


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Initializes an #SK_Vector which has had memory allocated for it but has not
 *  had any of its fields initialized.
 *
 *  Unlike SK_Vector_init_unsafe (), this function WILL perform sanity checks
 *  on the input arguments.
 *
 *  @param vec			The #SK_Vector to initialize
 *
 *  @param elem_size		See SK_Vector_create ()
 *
 *  @param num_elems		See SK_Vector_create ()
 *
 *  @param elems_per_block	See SK_Vector_create ()
 *
 *  @param neg_slack		See SK_Vector_create ()
 *
 *  @param pos_slack		See SK_Vector_create ()
 *
 *  @param init_func		See SK_Vector_create ()
 *
 *  @param init_func_arg	See SK_Vector_create ()
 *
 *  @param sr_ptr		See SK_Vector_create ()
 */

SlySr SK_Vector_init
(
	SK_Vector *	vec,
	size_t		elem_size,
	size_t		num_elems,
	size_t		elems_per_block,
	size_t		neg_slack,
	size_t		pos_slack,
	SlyDr		(* init_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		init_func_arg
)
{

	//Sanity check on input arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlySr_get (SLY_BAD_ARG));


	if (0 >= elem_size)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Invalid argument, #elem_size was 0."
		);


		return SlySr_get (SLY_BAD_ARG);
	}


	if (0 >= elems_per_block)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Invalid argument, #elems_per_block was 0."
		);


		return SlySr_get (SLY_BAD_ARG);
	}


	//Actually initialize #vec

	return

	SK_Vector_init_unsafe
	(
		vec,
		elem_size,
		num_elems,
		elems_per_block,
		neg_slack,
		pos_slack,
		init_func,
		init_func_arg
	);

}




/*!
 *  Creates an #SK_Vector to store elements of a given size, whilst allocating
 *  enough memory for a specified number of elements.
 *
 *  @warning
 *  Some time after calling this function, SK_Vector_destroy () must be called
 *  on the returned pointer, otherwise a memory leak will occur.
 *
 *  @warning
 *  To avoid errors related to uninitialized memory accesses, resetting the
 *  elements in the #SK_Vector at creation is mandatory. However, to avoid
 *  redundant element writes caused by writing a 0 and then actually useful
 *  values later on, a function pointer can be used to provide a function to
 *  initialize the elements immediately.
 *
 *  @param elem_size		The size of each element that will be stored in
 *				this array.
 *
 *  @param num_elems		The number of elements to prepare the
 *				#SK_Vector to store initially
 *
 *  @param elems_per_block	The number of elements that each "block"
 *				inside the #SK_Vector will be able to store.
 *				This is relevant, because memory allocations
 *				for the #SK_Vector will only occur in units of
 *				this size.
 *
 *				The larger this value is, the quicker elements
 *				will be to access for the #SK_Vector, and the
 *				slower the #SK_Vector will be to resize.
 *
 *  @param neg_slack		The threshold for how many unused blocks there
 *				should be before automatically deallocating
 *				blocks from the #SK_Vector.
 *
 *				See #SK_Vector for details.
 *
 *  @param pos_slack		The number of extra blocks that should be
 *				allocated whenever new blocks are allocated.
 *
 *				See #SK_Vector for details.
 *
 *  @param init_func		The function to use to initialize the elements
 *				in the newly created #SK_Vector.
 *
 *				Note, the return value of #init_func will not
 *				be checked by SK_Vector_create (), so it is up
 *				to the caller to ensure that it will succeed.
 *
 *				If the use of #init_func is not desired, this
 *				argument CAN be NULL, in which case the
 *				elements of the newly created #SK_Vector will
 *				be reset to 0.
 *
 *				#elem_ptr is a pointer to the beginning of the
 *				element that is being initialized. This element
 *				has a size of #elem_size. This will be supplied
 *				by SK_Vector_create () itself.
 *
 *				#index is the index of the element that is
 *				currently being reset in the #SK_Vector. This
 *				will be supplied by SK_Vector_create () itself
 *
 *				#arg is a pointer to an arbitrary argument
 *
 *  @param init_func_arg	An arbitrary argument that will be passed to
 *  				each call of #init_func as #arg.
 *
 *  				If #init_func is NULL, this argument will be
 *  				ignored.
 *
 *  @param sr_ptr		Where to place a #SlySr value indicating the
 *				success of this function. This CAN be NULL if
 *				this information is not desired.
 *
 *  @return			A pointer to the newly created #SK_Vector
 *				structure upon success; NULL is returned
 *				otherwise.
 */

SK_Vector * SK_Vector_create
(
	size_t		elem_size,
	size_t		num_elems,
	size_t		elems_per_block,
	size_t		neg_slack,
	size_t		pos_slack,
	SlyDr		(* init_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		init_func_arg,
	SlySr *		sr_ptr
)
{

	//Sanity check on arguments

	if (0 >= elem_size)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Invalid argument, #elem_size was 0."
		);

		if (NULL != sr_ptr)
		{
			*sr_ptr = SlySr_get (SLY_BAD_ARG);
		}

		return NULL;
	}


	if (0 >= elems_per_block)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Invalid argument, #elems_per_block was 0."
		);

		if (NULL != sr_ptr)
		{
			*sr_ptr = SlySr_get (SLY_BAD_ARG);
		}

		return NULL;
	}


	//Allocate space for the #SK_Vector itself

	SK_Vector * vec = malloc (sizeof (SK_Vector));

	if (NULL == vec)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Could not allocate #vec"
		);

		if (NULL != sr_ptr)
		{
			*sr_ptr = SlySr_get (SLY_BAD_ALLOC);
		}

		return NULL;
	}


	//Use SK_Vector_init_unsafe () to initialize #vec.
	//
	//The unsafe version is used because the sanity checks are already
	//performed at the beginning of this function.

	SlySr sr =

	SK_Vector_init_unsafe
	(
		vec,
		elem_size,
		num_elems,
		elems_per_block,
		neg_slack,
		pos_slack,
		init_func,
		init_func_arg
	);


	if (SLY_SUCCESS != sr.res)
	{
		free (vec);


		if (NULL != sr_ptr)
		{
			*sr_ptr = sr;
		}


		SK_RES_RETURN (SK_VECTOR_L_DEBUG, sr.res, NULL);
	}


	//Indicate that the #SK_Vector was successfully created

	if (NULL != sr_ptr)
	{
		*sr_ptr = SlySr_get (SLY_SUCCESS);
	}


	return vec;
}



/*!
 *  Deinitializes an #SK_Vector object, which prepares it for deallocation.
 *
 *  @param vec			The #SK_Vector object to deinitialize
 *
 *  @return			Standard status code
 *
 */

SlyDr SK_Vector_deinit
(
	SK_Vector * vec
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlyDr_get (SLY_BAD_ARG));


	//Make sure all the arrays in the blocks are deallocated one-by-one

	for (size_t b_sel = 0; b_sel < vec->num_blocks; b_sel += 1)
	{
		free (vec->blocks [b_sel].bytes);
	}


	//Deallocate the array of blocks itself

	free (vec->blocks);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Destroys an #SK_Vector object.
 *
 *  @param vec			The #SK_Vector object to destroy
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_destroy
(
	SK_Vector * vec
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlyDr_get (SLY_BAD_ARG));


	//Deinitialize the #SK_Vector, so that deallocating it is safe

	SK_Vector_deinit (vec);


	//Deallocate the #SK_Vector object itself

	free (vec);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Used to initialize a range of elements inside of an #SK_Vector
 *
 *  @param vec				The #SK_Vector object to modify
 *
 *  @param beg_ind			The index of the first element to
 *  					initialize
 *
 *  @param end_ind			The index of the last element to
 *					initialize
 *
 *  @param init_func			See SK_Vector_create () for details
 *
 *  @param init_func_arg		See SK_Vector_create () for details
 *
 *  @return				Standard status code
 */

SlyDr SK_Vector_init_range
(
	SK_Vector *	vec,
	size_t		beg_ind,
	size_t		end_ind,
	SlyDr		(* init_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		init_func_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlyDr_get (SLY_BAD_ARG));


	if ((beg_ind >= vec->cur_num_elems) || (end_ind >= vec->cur_num_elems))
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"[#beg_ind, #end_ind] = [%zu, %zu], which for "
			"vec = %p, #cur_num_elems = %zu, is out of range.",
			beg_ind,
			end_ind,
			vec,
			vec->cur_num_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Initialize all the elements in the range given by [#beg_ind, #end_ind]
	//using the provided #init_func ()

	if (NULL == init_func)
	{
		for (size_t elem_sel = beg_ind; elem_sel <= end_ind; elem_sel += 1)
		{
			SK_Vector_set_elem (vec, elem_sel, NULL, 0);
		}
	}

	else
	{
		for (size_t elem_sel = beg_ind; elem_sel <= end_ind; elem_sel += 1)
		{
			u8 * elem_addr = NULL;

			SK_Vector_calc_elem_addr (vec, elem_sel, &elem_addr);


			SlyDr dr = init_func (elem_addr, elem_sel, init_func_arg);

			if (SK_VECTOR_H_DEBUG)
			{
				if (SLY_SUCCESS != dr.res)
				{
					SK_DEBUG_PRINT
					(
						SK_VECTOR_H_DEBUG,
						"Failed to initialize element "
						"at index %zu in #vec = %p",
						elem_sel,
						vec
					);
				}
			}
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Calculates the memory address associated with the beginning of an element
 *  in an #SK_Vector.
 *
 *  @param vec			An #SK_Vector that contains the element that
 *				#addr will refer to
 *
 *  @param index		The element-index of the element in #vec that
 *				#addr will refer to
 *
 *  @param addr			Where to place the address associated with the
 *				given #vec and #index
 *
 *				If this function fails, then this value will
 *				not be modified.
 *
 *  @return			Standard status code
 *
 */

SlyDr SK_Vector_calc_elem_addr
(
	SK_Vector *	vec,
	size_t		index,
	u8 **		addr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, addr, SlyDr_get (SLY_BAD_ARG));


	if (index > vec->cur_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"#index = %zu was greater than vec->cur_num_elems = %zu",
			index,
			vec->cur_num_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Calculate the block-index and block-element-index associated with
	//#index

	size_t b_ind =		index / vec->elems_per_block;

	size_t b_e_ind =	index % vec->elems_per_block;


	//Calculate the memory address associated with the indexed element

	*addr = &(vec->blocks [b_ind].bytes [(vec->elem_size * b_e_ind)]);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Returns a #SK_VectorBlock in its default state, which means it has no
 *  element array allocated for it yet and it is effectively disabled.
 *
 *  @return			An #SK_Vector in its default state
 */

SK_VectorBlock SK_VectorBlock_def ()
{
	SK_VectorBlock vb;

	vb.bytes = NULL;


	return vb;
}




/*!
 *  Fetches the current number of elements inside of an #SK_Vector
 *
 *  @param vec			The #SK_Vector to examine
 *
 *  @param num_elems		Where to place a value representing the current
 *				number of elements inside of #vec
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_get_num_elems
(
	SK_Vector *	vec,
	size_t *	num_elems
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, num_elems, SlyDr_get (SLY_BAD_ARG));


	//Fetch the number of elements inside of #vec

	*num_elems = vec->cur_num_elems;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets an element inside of an #ElemSet.
 *
 *  @param vec			The #SK_Vector to modify
 *
 *  @param index		The element index of the element to modify
 *
 *  @param value_ptr		Pointer to the value to set the element to.
 *
 *  				This CAN be NULL, in which case, the element in
 *  				#vec will be set to 0.
 *
 *  @param value_size		The size in bytes of the value pointed at by
 *				#value_ptr.
 *
 *				If this is lower than the element size in #vec,
 *				then the upper bytes will be considered equal
 *				to 0.
 *
 *				If this is larger than element size in #vec,
 *				then the upper bytes will not be used.
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_set_elem
(
	SK_Vector *	vec,
	size_t		index,
	u8 *		value_ptr,
	size_t		value_size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlyDr_get (SLY_BAD_ARG));


	if (index >= vec->cur_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"For #vec = %p, #index = %zu was over "
			"#vec->cur_num_elems = %zu",
			vec,
			index,
			vec->cur_num_elems
		)

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Calculate the block-index and block-element-index associated with the
	//given element-index.

	size_t b_ind =		index / vec->elems_per_block;

	size_t b_e_ind =	index % vec->elems_per_block;


	//If #value_ptr is NULL, make sure that no bytes will attempt to be
	//read from it

	if (NULL == value_ptr)
	{
		value_size = 0;
	}


	//Find how many bytes should be copied from #value_ptr into the element
	//in #vec

	size_t num_copy_bytes = SK_MIN (value_size, vec->elem_size);


	//Copy bytes from #value_ptr into the element's bytes in #vec

	for (size_t byte = 0; byte < num_copy_bytes; byte += 1)
	{
		vec->blocks [b_ind].bytes [(vec->elem_size * b_e_ind) + byte] =

		value_ptr [byte];
	}


	//Take into account that #value_size may have been smaller than
	//#vec->elem_size, and so there are bytes that need to be set to 0.

	for (size_t byte = num_copy_bytes; byte < vec->elem_size; byte += 1)
	{
		vec->blocks [b_ind].bytes [(vec->elem_size * b_e_ind) + byte] =

		0;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets an element from an #ElemSet.
 *
 *  @param vec			The #SK_Vector to modify
 *
 *  @param index		The element index of the element to retrieve
 *
 *  @param value_ptr		Where to save the element at
 *
 *  @param value_size		The size in bytes of the value pointed at by
 *				#value_ptr.
 *
 *				If this is lower than the element size in #vec,
 *				then the upper bytes will not be used.
 *
 *				If this is larger than element size in #vec,
 *				then the upper bytes will be set to 0.
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_get_elem
(
	SK_Vector *	vec,
	size_t		index,
	u8 *		value_ptr,
	size_t		value_size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, value_ptr, SlyDr_get (SLY_BAD_ARG));


	if (index >= vec->cur_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"For #vec = %p, #index = %zu was over "
			"#vec->cur_num_elems = %zu",
			vec,
			index,
			vec->cur_num_elems
		)

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Calculate the block-index and block-element-index associated with the
	//given element-index.

	size_t b_ind =		index / vec->elems_per_block;

	size_t b_e_ind =	index % vec->elems_per_block;


	//Determine the number of bytes that will need to be copied into
	//#value_ptr

	size_t num_copy_bytes = SK_MIN (value_size, vec->elem_size);


	//Copy bytes from the element in #vec into #value_ptr

	for (size_t byte = 0; byte < num_copy_bytes; byte += 1)
	{
		value_ptr [byte] =

		vec->blocks [b_ind].bytes [(vec->elem_size * b_e_ind) + byte];
	}


	//Take into account that #value_size may have been larger than
	//#vec->elem_size, and so there are bytes that need to be set to 0.

	for (size_t byte = vec->elem_size; byte < value_size; byte += 1)
	{
		value_ptr [byte] = 0;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  A harder-to-use but potentially faster-to-execute version of
 *  SK_Vector_set_elem () / SK_Vector_get_elem ().
 *
 *  This is because SK_Vector_set_elem () / SK_Vector_get_elem () access
 *  elements byte-by-byte, where as #rw_func in this function may use larger
 *  word sizes or loop unrolling.
 *
 *  The reason this function serves as a replacement for both
 *  SK_Vector_set_elem () and SK_Vector_get_elem () is that #rw_func determines
 *  whether or not this function executes a write, a read, or both in some
 *  order. Basically, #rw_func has free reign over the element that it
 *  accesses, which makes this function both versatile, but also hard to use.
 *
 *  @param vec			The #SK_Vector to set the element inside of
 *
 *  @param index		The element index of the element to modify in
 *  				#vec
 *
 *  @param rw_func		A function used to set/get the value of the new
 *				element in #vec. This follows the same
 *				conventions as #init_func in
 *				SK_Vector_create ().
 *
 *  @param rw_func_arg		This will be passed to #rw_func as an argument.
 *  				Usually, this will be a pointer to the value
 *  				that you want to store IN #vec, or a pointer to
 *  				a place to store a value FROM #vec. This
 *  				argument follows the same conventions as
 *  				#init_func_arg in SK_Vector_create ().
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_rw_cust
(
	SK_Vector *	vec,
	size_t		index,
	SlyDr		(* rw_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		rw_func_arg
)
{

	//Sanity check on argumnets

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, rw_func, SlyDr_get (SLY_BAD_ARG));


	if (index >= vec->cur_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"For #vec = %p, #index = %zu was over "
			"#vec->cur_num_elems = %zu",
			vec,
			index,
			vec->cur_num_elems
		)

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Calculate the block-index and block-element-index associated with the
	//given element-index.

	size_t b_ind =		index / vec->elems_per_block;

	size_t b_e_ind =	index % vec->elems_per_block;


	//Get a pointer to the 1st byte of the element that needs setting /
	//getting

	u8 * elem_ptr =

	& (vec->blocks [b_ind].bytes [vec->elem_size * b_e_ind]);


	//Pass the pointer to the element to #rw_func, as well as the element
	//index and the arbitrary #rw_func_arg.
	//
	//Note that errors are not checked here, because #rw_func has a
	//deterministic return value (#SlyDr).

	rw_func (elem_ptr, index, rw_func_arg);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Reallocates blocks in an existing #SK_Vector object so that it can hold a
 *  different number of elements. This can be used to both decrease and
 *  increase the size of the #SK_Vector.
 *
 *  As many elements as possible will be preserved when decreasing the size of
 *  the #SK_Vector. The elements with the highest element-indices will be
 *  truncated first during size decreases.
 *
 *  If this function fails, the input #SK_Vector will not be modified.
 *
 *  @warning
 *  This function modifies #vec to contain the minimum number of blocks needed
 *  to store #num_elems. It does not alter #vec->cur_num_elems, unless it needs
 *  to be decreased so that it is not higher than #vec->max_num_elems.
 *
 *  @warning
 *  This function is intended to be used by other functions that resize
 *  #SK_Vector's. When considering execution speed, it is not always optimal
 *  to resize vectors to the bare minimum size needed to store the bare minimum
 *  of needed elements. This is because allocating some blocks as "slack"
 *  can help reduce the number of future block allocations / deallocations,
 *  which are expensive. It is recommended that some other function (which
 *  makes use of this one) handle the logic for allocating "slack".
 *
 *  @warning
 *  As implied by the previous warnings, this function totally ignores
 *  #vec->neg_slack and #vec->pos_slack.
 *
 *  @param vec				The #SK_Vector to reallocate blocks
 *					for. This will not be modified if this
 *					function fails.
 *
 *  @param num_elems			The number of elements that #vec should
 *					be made capable of storing
 *
 *  @return				Standard status code
 */

//FIXME : Maybe this function take the number of blocks to resize to as an
//argument instead of the number of elements? Keep in mind, this function is
//mainly called by SK_Vector_min_resize () and SK_Vector_resize ()

SlySr SK_Vector_realloc_b
(
	SK_Vector *	vec,
	size_t		num_elems
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlySr_get (SLY_BAD_ARG));


	//Determine the number of blocks that would be necessary to store
	//#num_elems

	size_t new_num_blocks =

	SK_Vector_calc_min_blocks (vec->elems_per_block, num_elems);


	//If the new number of elements still requires the same number of
	//blocks as what is currently available in #vec, go ahead and exit
	//early

	if (new_num_blocks == vec->num_blocks)
	{
		return SlySr_get (SLY_SUCCESS);
	}


	//Allocate an array to hold the new number of #SK_VectorBlock's needed.
	//
	//Also, make sure to account for the case where #new_num_blocks is 0.

	SK_VectorBlock * new_blocks = NULL;


	if (0 < new_num_blocks)
	{
		new_blocks = malloc (sizeof (SK_VectorBlock) * num_elems);

		if (NULL == new_blocks)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Could not allocate blocks to resize "
				"#vec = %p to have %zu elements",
				vec,
				num_elems
			);

			return SlySr_get (SLY_BAD_ALLOC);
		}
	}


	//Copy the old block pointers from #vec->blocks to #new_blocks, but
	//without modifying the elements in #vec->blocks [].bytes [].
	//
	//The fact that the elements themselves are not moved / modified here
	//is what makes #SK_Vector's fast to resize.

	for (size_t b_sel = 0; b_sel < new_num_blocks; b_sel += 1)
	{
		if (b_sel < vec->num_blocks)
		{
			new_blocks [b_sel] = vec->blocks [b_sel];
		}

		else
		{
			new_blocks [b_sel] = SK_VectorBlock_def ();
		}
	}


	//In the event that the size of the #SK_Vector has been increased, make
	//sure that the element arrays associated with the new blocks get
	//allocated.
	//
	//Make sure to record if these allocations succeed or fail.

	int block_alloc_fail =		0;

	size_t fail_alloc_ind =		0;


	for (size_t b_sel = vec->num_blocks; b_sel < new_num_blocks; b_sel += 1)
	{
		new_blocks [b_sel].bytes =

		malloc (sizeof (u8) * vec->elem_size * vec->elems_per_block);


		if (NULL == new_blocks [b_sel].bytes)
		{
			block_alloc_fail =	1;

			fail_alloc_ind =	b_sel;

			break;
		}
	}


	//If allocating any of the new element arrays failed, make sure to
	//deallocate all of those newly created element arrays before returning
	//an error

	if (block_alloc_fail)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Could not allocate element arrays to resize "
			"#vec = %p to %zu elements",
			vec,
			num_elems
		);


		for (size_t b_sel = vec->num_blocks; b_sel < fail_alloc_ind; b_sel += 1)
		{
			free (new_blocks [b_sel].bytes);
		}

		free (new_blocks);


		return SlySr_get (SLY_BAD_ALLOC);
	}


	//In the event that the size of the #SK_Vector has been decreased, make
	//sure that the element arrays in old blocks that have been truncated
	//off will get properly deallocated

	for (size_t b_sel = new_num_blocks; b_sel < vec->num_blocks; b_sel += 1)
	{
		free (vec->blocks [b_sel].bytes);
	}


	//Deallocate the old #blocks array in #vec, and then replace it with
	//#new_blocks

	free (vec->blocks);

	vec->blocks = new_blocks;


	//Update #vec with new parameters relevant for its new size

	vec->num_blocks =	new_num_blocks;

	vec->max_num_elems =	new_num_blocks * vec->elems_per_block;


	if (vec->cur_num_elems > vec->max_num_elems)
	{
		vec->cur_num_elems = vec->max_num_elems;
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Resizes an #SK_Vector, but without creating/leaving any blocks for "slack"
 *  (unlike SK_Vector_resize ()).
 *
 *  This function is very similar to SK_Vector_realloc_b (), except that it
 *  DOES set #vec->cur_num_elems to #num_elems.
 *
 *  This will preserve as many elements as possible during the resize
 *  operation. If the size is increased, those elements will only be reset via
 *  usage of #init_func ().
 *
 *  @param vec				The #SK_Vector to resize
 *
 *  @param num_elems			The number of elements that #vec should
 *					be able to store
 *
 *  @param init_func			See SK_Vector_create () for details
 *
 *  @param init_func_arg		See SK_Vector_create () for details
 *
 */

SlySr SK_Vector_min_resize
(
	SK_Vector *	vec,
	size_t		num_elems,
	SlyDr		(* init_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		init_func_arg

)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlySr_get (SLY_BAD_ARG));


	//Note, an early exit is not performed here if #num_elems is equal to
	//#vec->cur_num_elems. The input #SK_Vector may have blocks allocated
	//for slack, which will be removed by this function.


	//Save the old number of elements in #vec

	size_t old_num_elems = vec->cur_num_elems;


	//Attempt to resize the #vec

	SlySr sr = SK_Vector_realloc_b (vec, num_elems);

	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Could not reallocate blocks to resize #vec = %p",
			vec
		);

		return sr;
	}


	//Unlike SK_Vector_realloc_b (), make sure that #vec->cur_num_elems is
	//actually updated

	vec->cur_num_elems = num_elems;


	//Make sure to initialize any newly allocated elements in #vec

	if ((num_elems > old_num_elems) && (num_elems > 0))
	{
		SK_Vector_init_range
		(
			vec,
			old_num_elems,
			num_elems - 1,
			init_func,
			init_func_arg
		);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Resizes an #SK_Vector.
 *
 *  Unlike SK_Vector_min_resize (), this function WILL create/leave "slack",
 *  which helps reduce the number of memory allocations needed in the future.
 *
 *  Unlike SK_Vector_realloc_b (), this WILL set the value of
 *  #vec->cur_num_elems.
 *
 *  @param vec				The #SK_Vector to resize
 *
 *  @param num_elems			The number of elements that #vec should
 *					be able to store
 *
 *  @param init_func			See SK_Vector_create () for details
 *
 *  @param init_func_arg		See SK_Vector_create () for details
 *
 */

SlySr SK_Vector_resize
(
	SK_Vector *	vec,
	size_t		num_elems,
	SlyDr		(* init_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		init_func_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlySr_get (SLY_BAD_ARG));


	//Note, an early exit is not performed here if #num_elems is equal to
	//#vec->cur_num_elems. That way, #neg_slack and #pos_slack can be
	//considered against the current number of blocks in #vec, which may
	//have extra blocks allocated for slack.


	//Save the old number of elements

	size_t old_num_elems = vec->cur_num_elems;


	//Determine the minimum number of blocks needed to store #num_elems

	size_t min_num_blocks =

	SK_Vector_calc_min_blocks (vec->elems_per_block, num_elems);


	//Determine if the size of #vec either needs to be increased or
	//decreased

	if (min_num_blocks > vec->num_blocks)
	{
		//Calculate the number of extra elements that would have to be
		//allocated as slack since the size of #vec is increasing

		size_t calc_num_blocks =

		min_num_blocks + vec->pos_slack;


		size_t calc_num_elems =

		vec->elems_per_block * calc_num_blocks;


		//Attempt to resize #vec

		SlySr sr = SK_Vector_realloc_b (vec, calc_num_elems);

		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Could not reallocate blocks to resize #vec = %p",
				vec
			);

			return sr;
		}
	}

	else if (min_num_blocks < vec->num_blocks)
	{
		//Calculate exactly how many blocks the size of #vec will
		//decrease by

		size_t num_block_diff =

		vec->num_blocks - min_num_blocks;


		//Only resize #vec if the number of blocks that will be removed
		//is greater than #neg_slack

		if (num_block_diff > vec->neg_slack)
		{
			size_t min_num_elems =

			vec->elems_per_block * min_num_blocks;


			//Attempt to resize #vec

			SlySr sr = SK_Vector_realloc_b (vec, min_num_elems);

			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT
				(
					SK_VECTOR_L_DEBUG,
					"Could not reallocate blocks to "
					"resize #vec = %p",
					vec
				);

				return sr;
			}
		}
	}


	//Unlike SK_Vector_realloc_b (), make sure that #vec->cur_num_elems is
	//actually updated

	vec->cur_num_elems = num_elems;


	//If requested, make sure to reset any newly allocated elements in #vec

	if ((num_elems > old_num_elems) && (num_elems > 0))
	{
		SK_Vector_init_range
		(
			vec,
			old_num_elems,
			num_elems - 1,
			init_func,
			init_func_arg
		);
	}


	return SlySr_get (SLY_SUCCESS);
}




//FIXME : Write a test-bench for these append functions

/*!
 *  Appends an element to the end of an #SK_Vector. Precisely stated, the
 *  element at #elem_ptr is copied to the element at index
 *  (vec->num_elems - 1).
 *
 *  @warning
 *  As a result of byte-by-byte copying in SK_Vector_set_elem (), the function
 *  SK_Vector_append_cust () can perform better because it can provide custom
 *  element setting functions. Obviously, that is more work to use however.
 *
 *  @param vec			The #SK_Vector to modify by appending #elem
 *
 *  @param elem_ptr		The element to append to #vec. This CAN be
 *  				NULL, in which case an element in which all
 *  				bytes are 0 will be appended to #vec.
 *
 *  @param elem_size		The size of the element at #elem_ptr.
 *
 *				If this is lower than the element size in #vec,
 *				then the upper bytes will be considered equal
 *				to 0.
 *
 *				If this is larger than element size in #vec,
 *				then the upper bytes will not be used.
 *
 *  @return			Standard status code
 */

SlySr SK_Vector_append
(
	SK_Vector *	vec,
	u8 *		elem_ptr,
	size_t		elem_size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlySr_get (SLY_BAD_ARG));


	//Attempt to resize #vec to contain 1 more element

	size_t new_num_elems =	vec->cur_num_elems + 1;

	size_t vec_elem_size =	vec->elem_size;


	SlySr sr =

	SK_Vector_resize
	(
		vec,
		new_num_elems,
		SK_Vector_init_zero,
		&vec_elem_size
	);


	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Could not append to #vec = %p to contain %zu elements",
			vec,
			new_num_elems
		);


		return sr;
	}


	//Attempt to set the newly added element to be equal to the value at
	//#elem_ptr

	SK_Vector_set_elem
	(
		vec,
		new_num_elems - 1,
		elem_ptr,
		elem_size
	);


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  A harder-to-use but potentially faster-to-execute version of
 *  SK_Vector_append ().
 *
 *  The reason that this may be faster is that SK_Vector_append () uses
 *  SK_Vector_set_elem (), which has to copy new elements byte-by-byte. This is
 *  quite slow on larger elements, as this does not take advantage of larger
 *  word sizes or loop-unrolling.
 *
 *  This function uses the fact that SK_Vector_resize () only calls its
 *  #init_func argument on newly created elements. If the #init_func is used as
 *  a custom element-setting function, then this can be faster than using
 *  SK_Vector_resize () and then SK_Vector_set_elem (). The reason is 2-fold:
 *
 *  1)
 *  	The custom element-setting function can know the element size as a
 *  	constant, which allows the compiler to use loop-unrolling / larger
 *  	words.
 *
 *  2)
 *  	SK_Vector_resize () demands that elements be initialized after it is
 *  	done being executed. If SK_Vector_set_elem () is called right after
 *  	SK_Vector_reisze (), then essentially that element initialization was
 *  	not truly needed.
 *
 *
 *  @param vec				The #SK_Vector to append an element to
 *
 *  @param set_func			A function used to set the value of the
 *					new_element in #vec. This follows the
 *					same conventions as #init_func in
 *					SK_Vector_create ().
 *
 *  @param set_func_arg			See SK_Vector_create () for details.
 *					This will be passed to #set_func as an
 *					argument, which is very useful for
 *					forwarding the value of some element to
 *					the new element in #vec
 *
 *  @return				Standard status code
 *
 */

SlySr SK_Vector_append_cust
(
	SK_Vector *	vec,
	SlyDr		(* set_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		set_func_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlySr_get (SLY_BAD_ARG));


	//Use SK_Vector_resize () to append a new element to #vec

	size_t new_num_elems = vec->cur_num_elems + 1;


	SlySr sr =

	SK_Vector_resize
	(
		vec,
		new_num_elems,
		set_func,
		set_func_arg
	);


	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Could not append to #vec = %p to contain %zu elements",
			vec,
			new_num_elems
		);


		return sr;
	}


	return sr;
}




/*!
 *  Removes any unused blocks in an #SK_Vector that are not being used to store
 *  elements.
 *
 *  In other words, this potentially reduces the amount of memory that #vec
 *  uses by deallocating any blocks meant for "slack".
 *
 *  @warning
 *  Calling this function unnecessarily essentially makes allocating slack in
 *  the first place useless. If this function is called very frequently,
 *  consider using SK_Vector_min_resize () instead of SK_Vector_resize ().
 *
 *  @param vec				The #SK_Vector to minify
 *
 *  @return				Standard status code
 */

SlySr SK_Vector_minify
(
	SK_Vector *	vec
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlySr_get (SLY_BAD_ARG));


	//Attempt to resize the #vec to use only the number of blocks needed
	//to store #vec->cur_num_elems number of elements

	SlySr sr = SK_Vector_realloc_b (vec, vec->cur_num_elems);

	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Could not minify #vec = %p",
			vec
		);

		return sr;
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Prints out the contents of a given #SK_Vector.
 *
 *  @param vec				The #SK_Vector to print out
 *
 *  @param f_ptr			The #FILE to print to, like #stdout
 *
 *  @param deco				An #SK_LineDeco describing how to
 *					decorate the printed line
 *
 *  @param header_flag			A flag indicating whether or not to
 *					print out an informational header above
 *					the elements of the #SK_Vector. This
 *					contains all the information about the
 *					#SK_Vector that is not covered by the
 *					other flags.
 *
 *  @param blocks_flag			A flag indicating whether to print out
 *  					the location of the #bytes associated
 *  					with each block
 *
 *  @param elems_flag			A flag indicating whether to print out
 *					the raw binary of the elements or not
 *
 *  @param stale_flag			A flag indicating whether to print out
 *					stub text for unused parts of allocated
 *					blocks in the #SK_Vector or not.
 *
 *					To avoid accessing uninitialized data,
 *					the contents of these unused parts will
 *					not actually be printed out.
 *
 *  @return				Standard status code
 */

SlyDr SK_Vector_print_opt
(
	SK_Vector *	vec,
	FILE *		f_ptr,
	SK_LineDeco	deco,
	int		header_flag,
	int		blocks_flag,
	int		elems_flag,
	int		stale_flag
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, f_ptr, SlyDr_get (SLY_BAD_ARG));


	//Print out the informational header if requested

	if (header_flag)
	{
		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"SK_Vector        @ %p",
			vec
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"elem_size        = %zu",
			vec->elem_size
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"elems_per_block  = %zu",
			vec->elems_per_block
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"num_blocks       = %zu",
			vec->num_blocks
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"blocks           = %p",
			vec->blocks
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"cur_num_elems    = %zu",
			vec->cur_num_elems
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"max_num_elems    = %zu",
			vec->max_num_elems
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"neg_slack        = %zu",
			vec->neg_slack
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"pos_slack        = %zu",
			vec->pos_slack
		);


		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			""
		);
	}


	//Create version of #deco with additional indentation, and then split
	//it into a beginning and ending poriton

	SK_LineDeco id_deco = deco;

	id_deco.pre_str_cnt += 1;


	SK_LineDeco id_deco_beg = id_deco;

	id_deco_beg.post_str_cnt = 0;


	SK_LineDeco id_deco_end = id_deco;

	id_deco_end.pre_str_cnt = 0;


	//Print the pointers for the #bytes associated with each block, if
	//requested

	if (blocks_flag)
	{
		SK_LineDeco_print_opt (f_ptr, deco, "Block Array Addresses");

		SK_LineDeco_print_opt (f_ptr, deco, "{");


		for (size_t b_sel = 0; b_sel < vec->num_blocks; b_sel += 1)
		{
			SK_LineDeco_print_opt
			(
				f_ptr,
				id_deco,
				"blocks [%03zu].bytes = %p",
				b_sel,
				vec->blocks [b_sel].bytes
			);
		}


		if (0 >= vec->num_blocks)
		{
			SK_LineDeco_print_opt
			(
				f_ptr,
				id_deco,
				"(Empty)"
			);
		}


		SK_LineDeco_print_opt (f_ptr, deco, "}");

		SK_LineDeco_print_opt (f_ptr, deco, "");
	}


	//Take into account whether or not printing stale elements was
	//requested when determining how many elements to print in total

	size_t total_print_elems =

	(stale_flag) ? (vec->max_num_elems) : (vec->cur_num_elems);



	//Print out each element stored in the #SK_Vector, if requested

	if (elems_flag)
	{

		SK_LineDeco_print_opt (f_ptr, deco, "Block Contents");

		SK_LineDeco_print_opt (f_ptr, deco, "{");


		for (size_t e_sel = 0; e_sel < total_print_elems; e_sel += 1)
		{
			size_t b_sel =		e_sel / vec->elems_per_block;

			size_t b_e_sel =	e_sel % vec->elems_per_block;


			size_t beg_byte_ind =	(b_e_sel * vec->elem_size);

			size_t end_byte_ind =	((b_e_sel + 1) * vec->elem_size) - 1;


			SK_LineDeco_print_opt
			(
				f_ptr,
				id_deco_beg,
				"%04zu ---- (blocks [%04zu].bytes [%04zu - %04zu]) : ",
				e_sel,
				b_sel,
				beg_byte_ind,
				end_byte_ind
			);


			//Take into account if this element is actually used
			//right now or not, and if it is not stale, print out
			//its bytes.

			//FIXME : Converting from #size_t to #i64 is
			//technically not safe here, because if #size_t is
			//64-bits, the #i64 value will have 1/2 the range on
			//the positive side. Find a safer way to rewrite the
			//loop below.

			if (e_sel < vec->cur_num_elems)
			{
				for (i64 byte = (i64) end_byte_ind; byte >= (i64) beg_byte_ind; byte -= 1)
				{
					fprintf
					(
						f_ptr,
						"%02" PRIx8,
						vec->blocks [b_sel].bytes [byte]
					);
				}
			}

			else if (stale_flag)
			{
				fprintf (f_ptr, "(unused)");
			}


			SK_LineDeco_print_opt
			(
				f_ptr,
				id_deco_end,
				""
			);
		}



		//If there were no elements to print, indicate that

		if (0 >= total_print_elems)
		{

			SK_LineDeco_print_opt (f_ptr, id_deco, "(Empty)");
		}


		SK_LineDeco_print_opt (f_ptr, deco, "}");

		SK_LineDeco_print_opt (f_ptr, deco, "");
	}




	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Used to initialize #SK_Vector elements such that each element is equal to
 *  0.
 *
 *  @param elem_ptr		Pointer to the element inside of the #SK_Vector
 *				to initialize
 *
 *  @param index		The index of the element being initialized
 *				within the #SK_Vector
 *
 *  @param size_t_elem_size	Pointer to a #size_t value representing the
 *				size of the element at #elem_ptr
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_init_zero
(
	u8 *		elem_ptr,
	size_t		index,
	void *		size_t_elem_size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, size_t_elem_size, SlyDr_get (SLY_BAD_ARG));


	//Zero out the element byte-by-byte

	for (size_t b_sel = 0; b_sel < * ((size_t *) size_t_elem_size); b_sel += 1)
	{

		elem_ptr [b_sel] = 0;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Used to initialize #SK_Vector elements such that each element is equal to
 *  0 but ONLY when the elements have a size equal to (sizeof (int)).
 *
 *  @warning
 *  In the interest of speed, this function does NOT check the element size of
 *  #elem_ptr. So using this with an #SK_Vector with elements of a size
 *  different from (sizeof (int)) will likely cause a segfault.
 *
 *  @param elem_ptr		Pointer to the element inside of the #SK_Vector
 *				to initialize, which must have a size of
 *				(sizeof (int))
 *
 *  @param index		The index of the element being initialized
 *				within the #SK_Vector
 *
 *  @param stub			Unused argument (required to be used with
 *				SK_Vector_create ())
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_init_int_zero
(
	u8 *		elem_ptr,
	size_t		index,
	void *		stub
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));


	//Zero out the element, which has a size equal to (sizeof (int)).

	*((int *) elem_ptr) = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Used to initialize #SK_Vector elements in a sequential manner inside of an
 *  #SK_Vector
 *
 *  @param elem_ptr		Pointer to the element inside of the #SK_Vector
 *				to initialize
 *
 *  @param index		The index of the element being initialized
 *				within the #SK_Vector
 *
 *  @param size_t_elem_size	Pointer to a #size_t value representing the
 *				size of the element at #elem_ptr
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_init_sequential
(
	u8 *		elem_ptr,
	size_t		index,
	void *		size_t_elem_size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, size_t_elem_size, SlyDr_get (SLY_BAD_ARG));


	//Copy the index into the element byte-by-byte

	for (size_t b_sel = 0; b_sel < * ((size_t *) size_t_elem_size); b_sel += 1)
	{
		if (b_sel < sizeof (index))
		{
			elem_ptr [b_sel] = ((u8 *) &index) [b_sel];
		}

		else
		{
			elem_ptr [b_sel] = 0;
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Used to initialize #SK_Vector elements such that even-indexed elements are
 *  equal to 0 and odd-indexed elements are equal to 1.
 *
 *  @param elem_ptr		Pointer to the element inside of the #SK_Vector
 *				to initialize
 *
 *  @param index		The index of the element being initialized
 *				within the #SK_Vector
 *
 *  @param size_t_elem_size	Pointer to a #size_t value representing the
 *				size of the element at #elem_ptr
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_init_even_odd
(
	u8 *		elem_ptr,
	size_t		index,
	void *		size_t_elem_size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, size_t_elem_size, SlyDr_get (SLY_BAD_ARG));


	//Calulate whether this element has an even or odd index

	int parity = index % 2;


	//Copy the index into the element byte-by-byte

	for (size_t b_sel = 0; b_sel < * ((size_t *) size_t_elem_size); b_sel += 1)
	{
		if (b_sel < sizeof (parity))
		{
			elem_ptr [b_sel] = ((u8 *) &parity) [b_sel];
		}

		else
		{
			elem_ptr [b_sel] = 0;
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Used to SET #f64 elements inside of an #SK_Vector. This function is
 *  intended to be used with SK_Vector_rw_elem () as the #rw_func argument.
 *
 *  @warning
 *  In the interest of speed, this function does NOT check the element size of
 *  #elem_ptr. So using this with an #SK_Vector with elements of a size
 *  different from (sizeof (f64)) will likely cause a segfault.
 *
 *  @param elem_ptr		Pointer to the element inside of the #SK_Vector
 *				to SET, which must have a size of
 *				(sizeof (f64))
 *
 *  @param index		The index of the element being set
 *				within the #SK_Vector
 *
 *  @param f64_ptr		Pointer to the source #f64 value which will be
 *				copied to #elem_ptr
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_rw_cust_set_f64
(
	u8 *		elem_ptr,
	size_t		index,
	void *		f64_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, f64_ptr, SlyDr_get (SLY_BAD_ARG));


	//Set the element in the #SK_Vector equal to the value at #f64_ptr

	*((f64 *) elem_ptr) = *((f64 *) f64_ptr);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Used to GET an #f64 element from an #SK_Vector. This function is intended
 *  to be used with SK_Vector_rw_elem () as the #rw_func argument.
 *
 *  @warning
 *  In the interest of speed, this function does NOT check the element size of
 *  #elem_ptr. So using this with an #SK_Vector with elements of a size
 *  different from (sizeof (f64)) will likely cause a segfault.
 *
 *  @param elem_ptr		Pointer to the element inside of the #SK_Vector
 *				to GET, which must have a size of
 *				(sizeof (f64))
 *
 *  @param index		The index of the element being retrieved
 *				from the #SK_Vector
 *
 *  @param f64_ptr		Pointer to the source #f64 value which will be
 *				copied to #elem_ptr
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_rw_cust_get_f64
(
	u8 *		elem_ptr,
	size_t		index,
	void *		f64_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, f64_ptr, SlyDr_get (SLY_BAD_ARG));


	//Set the element in the #SK_Vector equal to the value at #f64_ptr

	*((f64 *) f64_ptr) = *((f64 *) elem_ptr);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple testbench on SK_Vector_create ()
 *
 *  @return			Standard status code
 */

SlySr SK_Vector_create_tb ()
{

	printf ("SK_Vector_create_tb () : BEGIN\n\n");


	//Select constants that determine how many parameterizations to test

	const int64_t BEG_USE_INIT_FUNC =	0;
	const int64_t END_USE_INIT_FUNC =	1;
	const int64_t INC_USE_INIT_FUNC =	1;

	const int64_t BEG_ELEM_SIZE =		1;
	const int64_t END_ELEM_SIZE =		4;
	const int64_t INC_ELEM_SIZE =		1;

	const int64_t BEG_NUM_ELEMS =		0;
	const int64_t END_NUM_ELEMS =		20;
	const int64_t INC_NUM_ELEMS =		5;

	const int64_t BEG_ELEMS_PER_BLOCK =	1;
	const int64_t END_ELEMS_PER_BLOCK =	4;
	const int64_t INC_ELEMS_PER_BLOCK =	1;

	const int64_t NEG_SLACK =		1;

	const int64_t POS_SLACK =		1;


	int64_t use_init_func =			BEG_USE_INIT_FUNC;

	int64_t elem_size =			BEG_ELEM_SIZE;

	int64_t num_elems =			BEG_NUM_ELEMS;

	int64_t elems_per_block =		BEG_ELEMS_PER_BLOCK;


	//Create several different #SK_Vector's with different configurations

	int go =		1;

	int error_detected =	0;

	SK_Vector * vec =	NULL;


	while (go)
	{

		//Print out the parameters that will be used in this test
		//iteration

		printf
		(
		 	"\n----------\n\n"
			"Arguments:\n"
			"elem_size       = %zu\n"
			"elems_per_block = %zu\n"
			"num_elems       = %zu\n\n",
			elem_size,
			elems_per_block,
			num_elems
		);


		//Create the #SK_Vector, while choosing whether or not to use
		//NULL for the #init_func argument

		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);


		if (use_init_func)
		{
			size_t init_func_elem_size = elem_size;


			vec =

			SK_Vector_create
			(
				elem_size,
				num_elems,
				elems_per_block,
				NEG_SLACK,
				POS_SLACK,
				SK_Vector_init_sequential,
				&init_func_elem_size,
				&sr
			);
		}

		else
		{
			vec =

			SK_Vector_create
			(
				elem_size,
				num_elems,
				elems_per_block,
				NEG_SLACK,
				POS_SLACK,
				NULL,
				NULL,
				&sr
			);
		}


		if ((NULL == vec) || (SLY_SUCCESS != sr.res))
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Failed to create #SK_Vector"
			);

			error_detected = 1;

			break;
		}


		SK_Vector_print_opt
		(
			vec,
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1,
			1
		);


		//Ensure that the parameters that need to be saved directly got
		//saved

		if
		(
			(elem_size != vec->elem_size)

			||

			(elems_per_block != vec->elems_per_block)

			||

			(NEG_SLACK != vec->neg_slack)

			||

			(POS_SLACK != vec->pos_slack)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Did not save some direct parameters "
				"correctly for #SK_Vector"
			);

			error_detected = 1;

			break;
		}


		//Ensure that enough space was created to save a number of
		//elements equal to #num_elems

		if
		(
			(vec->max_num_elems < num_elems)

			||

			((double) vec->num_blocks < ((double) num_elems / elems_per_block))
		)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Not enough space allocated for #SK_Vector"
			);

			error_detected = 1;

			break;
		}


		//Ensure that no extra blocks have been allocated

		if
		(
			(double) vec->num_blocks

			>

			(((double) num_elems / elems_per_block) + POS_SLACK + 1.0)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Too much space allocated for #SK_Vector"
			);

			error_detected = 1;

			break;
		}


		//Check that initially there are #num_elems considered stored
		//in the #SK_Vector

		if (num_elems != vec->cur_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"#cur_num_elems != %zu in #SK_Vector",
				num_elems
			);

			error_detected = 1;

			break;
		}


		//Check that the elements in #vec were initialized properly

		for (size_t e_ind = 0; e_ind < vec->cur_num_elems; e_ind += 1)
		{
			//Calculate what the ideal element in #vec would be at
			//this index

			size_t init_func_elem_size = elem_size;

			size_t good_elem = 0;

			if (use_init_func)
			{
				SK_Vector_init_sequential
				(
					(u8 *) &good_elem,
					e_ind,
					(void *) &init_func_elem_size
				);
			}

			else
			{
				good_elem = 0;
			}


			//Fetch the actual element from #vec

			size_t real_elem = 0;

			SK_Vector_get_elem
			(
				vec,
				e_ind,
				(u8 *) &real_elem,
				sizeof (size_t)
			);


			if (real_elem != good_elem)
			{
				SK_DEBUG_PRINT
				(
					SK_VECTOR_L_DEBUG,
					"#vec = %p did not have elements set "
					"correctly. "
					"#good_elem = %zu, #real_elem = %zu",
					vec,
					good_elem,
					real_elem
				);

				error_detected = 1;

				break;
			}
		}


		//Propagate loop-breaking if an error occurred in the above
		//loop

		if (error_detected)
		{
			break;
		}


		//Free memory allocated in this test iteration

		SK_Vector_destroy (vec);


		//Update the parameters to test in the next test-iteration with
		//a "pseudo-loop"

		int looped = 0;

		SK_inc_chain
		(
			&use_init_func,
			1,
			BEG_USE_INIT_FUNC,
			END_USE_INIT_FUNC,
			INC_USE_INIT_FUNC,
			&looped
		);

		SK_inc_chain
		(
			&num_elems,
			looped,
			BEG_NUM_ELEMS,
			END_NUM_ELEMS,
			INC_NUM_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&elems_per_block,
			looped,
			BEG_ELEMS_PER_BLOCK,
			END_ELEMS_PER_BLOCK,
			INC_ELEMS_PER_BLOCK,
			&looped
		);

		SK_inc_chain
		(
			&elem_size,
			looped,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);


		//End the testing if the "outer-most" parameter has looped

		if (looped)
		{
			go = 0;
		}

	}


	//Check if the test bench has succeeded or not

	if (error_detected)
	{
		//Make sure to free memory that couldn't be freed as a result
		//of the test iteration stopping prematurely

		SK_Vector_destroy (vec);


		printf ("\n\nSK_Vector_create_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("\n\nSK_Vector_create_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple testbench on SK_Vector_realloc_b_tb ()
 *
 *  @return			Standard status code
 */

SlySr SK_Vector_realloc_b_tb ()
{

	printf ("SK_Vector_realloc_b_tb () : BEGIN\n\n");


	//Select constants that determine how many parameterizations to test

	const int64_t BEG_ELEM_SIZE =		1;
	const int64_t END_ELEM_SIZE =		3;
	const int64_t INC_ELEM_SIZE =		1;

	const int64_t BEG_ELEMS_PER_BLOCK =	1;
	const int64_t END_ELEMS_PER_BLOCK =	3;
	const int64_t INC_ELEMS_PER_BLOCK =	1;

	const int64_t BEG_OLD_NUM_ELEMS =	0;
	const int64_t END_OLD_NUM_ELEMS =	7;
	const int64_t INC_OLD_NUM_ELEMS =	1;

	const int64_t BEG_NEW_NUM_ELEMS =	0;
	const int64_t END_NEW_NUM_ELEMS =	7;
	const int64_t INC_NEW_NUM_ELEMS =	1;

	const int64_t NEG_SLACK =		1;

	const int64_t POS_SLACK =		1;


	int64_t elem_size =			BEG_ELEM_SIZE;

	int64_t elems_per_block =		BEG_ELEMS_PER_BLOCK;

	int64_t old_num_elems =			BEG_OLD_NUM_ELEMS;

	int64_t new_num_elems =			BEG_NEW_NUM_ELEMS;


	//Begin the test iterations in which an #SK_Vector is created at an
	//initial size, and then resized to a different size

	int go =		1;

	int error_detected =	0;

	SK_Vector * vec =	NULL;


	while (go)
	{

		//Print out the parameters that will be used in this test
		//iteration

		printf
		(
		 	"\n----------\n\n"
			"Arguments:\n"
			"elem_size =       %zu\n"
			"elems_per_block = %zu\n"
			"old_num_elems =   %zu\n"
			"new_num_elems =   %zu\n\n",
			elem_size,
			elems_per_block,
			old_num_elems,
			new_num_elems
		);


		//Create an arbitrary #SK_Vector that will be resized later on

		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);


		size_t init_func_elem_size = elem_size;


		vec =

		SK_Vector_create
		(
			elem_size,
			old_num_elems,
			elems_per_block,
			NEG_SLACK,
			POS_SLACK,
			SK_Vector_init_sequential,
			&init_func_elem_size,
			&sr
		);


		if ((NULL == vec) || (SLY_SUCCESS != sr.res))
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Failed to create the #SK_Vector"
			);

			error_detected = 1;

			break;
		}



		//Print out #vec before resizing it

		printf ("-----Original-----\n\n");

		SK_Vector_print_opt
		(
			vec,
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1,
			1
		);


		//Ensure that the starting number of elements is correct

		if (old_num_elems != vec->cur_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"#vec does not have the correct number of "
				"starting elements."
			);

			error_detected = 1;

			break;
		}


		//Attempt to resize the #SK_Vector to a new size

		sr =

		SK_Vector_realloc_b
		(
			vec,
			new_num_elems
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"SK_Vector_realloc_b () reported failure."
			);


			error_detected = 1;

			break;
		}


		printf ("-----Resized-----\n\n");

		SK_Vector_print_opt
		(
			vec,
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1,
			1
		);


		//Make sure that SK_Vector_realloc_b () used the minimum number
		//of blocks to achieve storing #cur_num_elems.
		//
		//Keep in mind that SK_Vector_realloc_b () should ignore
		//#vec->neg_slack and #vec->pos_slack.

		size_t ideal_num_blocks =

		SK_Vector_calc_min_blocks (elems_per_block, new_num_elems);


		size_t ideal_max_num_elems =

		ideal_num_blocks * elems_per_block;


		if
		(
			(ideal_num_blocks != vec->num_blocks)

			||

			(ideal_max_num_elems != vec->max_num_elems)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"SK_Vector_realloc_b () placed wrong number of "
				"blocks in #vec"
			);

			error_detected = 1;

			break;
		}


		//Check that #vec->cur_num_elems is only updated if absolutely
		//necessary.
		//
		//This means if the old value of #vec->cur_num_elems is higher
		//than the new value of #vec->max_num_elems, then it will be
		//updated to reflect that.

		if (old_num_elems > vec->max_num_elems)
		{
			if (vec->cur_num_elems != vec->max_num_elems)
			{
				SK_DEBUG_PRINT
				(
					SK_VECTOR_L_DEBUG,
					"#vec->cur_num_elems not reduced when "
					"necessary"
				);

				error_detected = 1;

				break;
			}
		}

		else
		{
			if (vec->cur_num_elems != old_num_elems)
			{
				SK_DEBUG_PRINT
				(
					SK_VECTOR_L_DEBUG,
					"#vec->cur_num_elems was changed when "
					"NOT necessary"
				);

				error_detected = 1;

				break;
			}
		}


		//Check that the elements in #vec under index
		//#vec->cur_num_elems were not modified from their old values

		for (size_t e_ind = 0; e_ind < vec->cur_num_elems; e_ind += 1)
		{
			//Calculate what the ideal element in #vec would be at
			//this index

			size_t init_func_elem_size = elem_size;

			size_t good_elem = 0;

			SK_Vector_init_sequential
			(
				(u8 *) &good_elem,
				e_ind,
				(void *) &init_func_elem_size
			);


			//Fetch the actual element from #vec

			size_t real_elem = 0;

			SK_Vector_get_elem
			(
				vec,
				e_ind,
				(u8 *) &real_elem,
				sizeof (size_t)
			);


			if (real_elem != good_elem)
			{
				SK_DEBUG_PRINT
				(
					SK_VECTOR_L_DEBUG,
					"#vec = %p did not have elements set "
					"correctly. "
					"#good_elem = %zu, #real_elem = %zu",
					vec,
					good_elem,
					real_elem
				);

				error_detected = 1;

				break;
			}
		}


		//Propagate loop-breaking if error occurred in above loop

		if (error_detected)
		{
			break;
		}


		//Destroy the #SK_Vector created in this test iteration

		SK_Vector_destroy (vec);


		//Update the parameters to test in the next test-iteration with
		//a "pseudo-loop"

		int looped = 0;

		SK_inc_chain
		(
			&new_num_elems,
			1,
			BEG_NEW_NUM_ELEMS,
			END_NEW_NUM_ELEMS,
			INC_NEW_NUM_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&old_num_elems,
			looped,
			BEG_OLD_NUM_ELEMS,
			END_OLD_NUM_ELEMS,
			INC_OLD_NUM_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&elems_per_block,
			looped,
			BEG_ELEMS_PER_BLOCK,
			END_ELEMS_PER_BLOCK,
			INC_ELEMS_PER_BLOCK,
			&looped
		);

		SK_inc_chain
		(
			&elem_size,
			looped,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);


		//End the testing if the "outer-most" parameter has looped

		if (looped)
		{
			go = 0;
		}
	}


	//Check if the test bench succeeded or not

	if (error_detected)
	{
		//Make sure to free memory that couldn't be freed as a result
		//of the test iteration stopping prematurely

		SK_Vector_destroy (vec);


		printf ("\n\nSK_Vector_realloc_b_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("\n\nSK_Vector_realloc_b_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple testbench on SK_Vector_min_resize ()
 *
 *  @return			Standard status code
 */

SlySr SK_Vector_min_resize_tb ()
{

	printf ("SK_Vector_min_resize_tb () : BEGIN\n\n");


	//Select constants that determine how many parameterizations to test

	const int64_t BEG_ELEM_SIZE =		1;
	const int64_t END_ELEM_SIZE =		3;
	const int64_t INC_ELEM_SIZE =		1;

	const int64_t BEG_ELEMS_PER_BLOCK =	1;
	const int64_t END_ELEMS_PER_BLOCK =	3;
	const int64_t INC_ELEMS_PER_BLOCK =	1;

	const int64_t BEG_OLD_NUM_ELEMS =	0;
	const int64_t END_OLD_NUM_ELEMS =	7;
	const int64_t INC_OLD_NUM_ELEMS =	1;

	const int64_t BEG_NEW_NUM_ELEMS =	0;
	const int64_t END_NEW_NUM_ELEMS =	7;
	const int64_t INC_NEW_NUM_ELEMS =	1;

	const int64_t NEG_SLACK =		1;

	const int64_t POS_SLACK =		1;


	int64_t elem_size =			BEG_ELEM_SIZE;

	int64_t elems_per_block =		BEG_ELEMS_PER_BLOCK;

	int64_t old_num_elems =			BEG_OLD_NUM_ELEMS;

	int64_t new_num_elems =			BEG_NEW_NUM_ELEMS;


	//Begin the test iterations in which an #SK_Vector is created at an
	//initial size, and then resized to a different size

	int go =		1;

	int error_detected =	0;

	SK_Vector * vec =	NULL;


	while (go)
	{

		//Print out the parameters that will be used in this test
		//iteration

		printf
		(
		 	"\n----------\n\n"
			"Arguments:\n"
			"elem_size =       %zu\n"
			"elems_per_block = %zu\n"
			"old_num_elems =   %zu\n"
			"new_num_elems =   %zu\n\n",
			elem_size,
			elems_per_block,
			old_num_elems,
			new_num_elems
		);


		//Create an arbitrary #SK_Vector that will be resized later on

		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);


		size_t init_func_elem_size = elem_size;


		vec =

		SK_Vector_create
		(
			elem_size,
			old_num_elems,
			elems_per_block,
			NEG_SLACK,
			POS_SLACK,
			SK_Vector_init_sequential,
			&init_func_elem_size,
			&sr
		);


		if ((NULL == vec) || (SLY_SUCCESS != sr.res))
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Failed to create the #SK_Vector"
			);

			error_detected = 1;

			break;
		}


		//Print out #vec before resizing it

		printf ("-----Original-----\n\n");

		SK_Vector_print_opt
		(
			vec,
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1,
			1
		);


		//Ensure that the starting number of elements is correct

		if (old_num_elems != vec->cur_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"#vec does not have the correct number of "
				"starting elements."
			);

			error_detected = 1;

			break;
		}


		//Attempt to resize the #SK_Vector to a new size

		sr =

		SK_Vector_min_resize
		(
			vec,
			new_num_elems,
			SK_Vector_init_even_odd,
			&init_func_elem_size
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"SK_Vector_min_resize () reported failure."
			);


			error_detected = 1;

			break;
		}


		printf ("-----Resized-----\n\n");

		SK_Vector_print_opt
		(
			vec,
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1,
			1
		);


		//Make sure that SK_Vector_min_resize () used the minimum
		//number of blocks to achieve storing #cur_num_elems. Also
		//check that #vec->cur_num_elems and #vec->max_num_elems are
		//set correctly.
		//
		//Keep in mind that SK_Vector_min_resize () should ignore
		//#vec->neg_slack and #vec->pos_slack.

		size_t ideal_num_blocks =

		SK_Vector_calc_min_blocks (elems_per_block, new_num_elems);


		size_t ideal_max_num_elems =

		ideal_num_blocks * elems_per_block;


		if
		(
			(ideal_num_blocks != vec->num_blocks)

			||

			(ideal_max_num_elems != vec->max_num_elems)

			||

			(new_num_elems != vec->cur_num_elems)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"SK_Vector_min_resize () placed wrong number "
				"of blocks/elements in #vec.\n"
				"vec->num_blocks = %zu    (ideally %zu)\n"
				"vec->max_num_elems = %zu (ideally %zu)\n"
				"vec->cur_num_elems = %zu (ideally %zu)\n",
				vec->num_blocks,
				ideal_num_blocks,
				vec->max_num_elems,
				ideal_max_num_elems,
				vec->cur_num_elems,
				new_num_elems

			);

			error_detected = 1;

			break;
		}


		//Check that the elements in #vec under index #old_num_elems
		//were not modified from their old values. Also make sure that
		//elements above #old_num_elems were initialized properly.

		for (size_t e_ind = 0; (e_ind < old_num_elems) && (e_ind < new_num_elems); e_ind += 1)
		{
			//Calculate what the ideal element in #vec would be at
			//this index

			size_t init_func_elem_size = elem_size;

			size_t good_elem = 0;


			if (e_ind < old_num_elems)
			{
				SK_Vector_init_sequential
				(
					(u8 *) &good_elem,
					e_ind,
					(void *) &init_func_elem_size
				);
			}

			else
			{
				SK_Vector_init_even_odd
				(
					(u8 *) &good_elem,
					e_ind,
					(void *) &init_func_elem_size
				);
			}


			//Fetch the actual element from #vec

			size_t real_elem = 0;

			SK_Vector_get_elem
			(
				vec,
				e_ind,
				(u8 *) &real_elem,
				sizeof (size_t)
			);


			if (real_elem != good_elem)
			{
				SK_DEBUG_PRINT
				(
					SK_VECTOR_L_DEBUG,
					"#vec = %p did not have elements set "
					"correctly. "
					"#good_elem = %zu, #real_elem = %zu",
					vec,
					good_elem,
					real_elem
				);

				error_detected = 1;

				break;
			}
		}



		//Propagate loop-breaking if error occurred in above loop

		if (error_detected)
		{
			break;
		}


		//Destroy the #SK_Vector created in this test iteration

		SK_Vector_destroy (vec);


		//Update the parameters to test in the next test-iteration with
		//a "pseudo-loop"

		int looped = 0;


		SK_inc_chain
		(
			&new_num_elems,
			1,
			BEG_NEW_NUM_ELEMS,
			END_NEW_NUM_ELEMS,
			INC_NEW_NUM_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&old_num_elems,
			looped,
			BEG_OLD_NUM_ELEMS,
			END_OLD_NUM_ELEMS,
			INC_OLD_NUM_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&elems_per_block,
			looped,
			BEG_ELEMS_PER_BLOCK,
			END_ELEMS_PER_BLOCK,
			INC_ELEMS_PER_BLOCK,
			&looped
		);

		SK_inc_chain
		(
			&elem_size,
			looped,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);


		//End the testing if the "outer-most" parameter has looped

		if (looped)
		{
			go = 0;
		}
	}


	//Check if the test bench succeeded or not

	if (error_detected)
	{
		//Make sure to free memory that couldn't be freed as a result
		//of the test iteration stopping prematurely;

		SK_Vector_destroy (vec);


		printf ("\n\nSK_Vector_min_resize_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("\n\nSK_Vector_min_resize_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);

}




/*!
 *  Performs a simple testbench on SK_Vector_resize ()
 *
 *  @return			Standard status code
 *
 */

//FIXME : This functions seems to share a great deal of code with
//SK_Vector_min_resize_tb (); can these reasonably be combined

//FIXME : Consider making this test bench do less test iterations. It prints
//out a huge amount of text as of right now.

SlySr SK_Vector_resize_tb ()
{

	printf ("SK_Vector_resize_tb () : BEGIN\n\n");


	//Select constants that determine how many parameterizations to test

	const int64_t BEG_ELEM_SIZE = 		1;
	const int64_t END_ELEM_SIZE =		3;
	const int64_t INC_ELEM_SIZE =		1;

	const int64_t BEG_ELEMS_PER_BLOCK =	1;
	const int64_t END_ELEMS_PER_BLOCK =	4;
	const int64_t INC_ELEMS_PER_BLOCK =	1;

	const int64_t BEG_OLD_NUM_ELEMS =	0;
	const int64_t END_OLD_NUM_ELEMS =	7;
	const int64_t INC_OLD_NUM_ELEMS =	1;

	const int64_t BEG_NEW_NUM_ELEMS =	0;
	const int64_t END_NEW_NUM_ELEMS =	7;
	const int64_t INC_NEW_NUM_ELEMS =	1;

	const int64_t BEG_NEG_SLACK =		0;
	const int64_t END_NEG_SLACK =		4;
	const int64_t INC_NEG_SLACK =		1;

	const int64_t BEG_POS_SLACK =		0;
	const int64_t END_POS_SLACK =		4;
	const int64_t INC_POS_SLACK =		1;


	int64_t elem_size =			BEG_ELEM_SIZE;

	int64_t elems_per_block =		BEG_ELEMS_PER_BLOCK;

	int64_t old_num_elems =			BEG_OLD_NUM_ELEMS;

	int64_t new_num_elems =			BEG_NEW_NUM_ELEMS;

	int64_t neg_slack =			BEG_NEG_SLACK;

	int64_t pos_slack =			BEG_POS_SLACK;


	//Begin the test iterations in which an #SK_Vector is created at an
	//initial size, and then resized to a different size _with slack_.

	int go =		1;

	int error_detected =	0;

	SK_Vector * vec =	NULL;


	while (go)
	{

		//Print out the parameters that will be used in this test
		//iteration

		printf
		(
			"\n----------\n\n"
			"Arguments:\n"
			"elem_size =           %zu\n"
			"elems_per_block =     %zu\n"
			"old_num_elems =       %zu\n"
			"new_num_elems =       %zu\n"
			"neg_slack =           %zu\n"
			"pos_slack =           %zu\n"
			"\n",
			elem_size,
			elems_per_block,
			old_num_elems,
			new_num_elems,
			neg_slack,
			pos_slack
		);


		//Create an arbitrary #SK_Vector that will be resized later on

		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);


		size_t init_func_elem_size = elem_size;


		vec =

		SK_Vector_create
		(
			elem_size,
			old_num_elems,
			elems_per_block,
			neg_slack,
			pos_slack,
			SK_Vector_init_sequential,
			&init_func_elem_size,
			&sr
		);


		if ((NULL == vec) || (SLY_SUCCESS != sr.res))
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Failed to create the #SK_Vector"
			);

			error_detected = 1;

			break;
		}


		//Print out #vec before resizing it

		printf ("-----Original-----\n\n");

		SK_Vector_print_opt
		(
			vec,
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1,
			1
		);


		//Ensure that the starting number of elements is correct

		if (old_num_elems != vec->cur_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"#vec does not have the correct number of "
				"starting elements."
			);

			error_detected = 1;

			break;
		}


		//Record the number of blocks inside of #vec before resizing it

		size_t old_num_blocks = vec->num_blocks;


		//Attempt to resize the #SK_Vector to a new size

		sr =

		SK_Vector_resize
		(
			vec,
			new_num_elems,
			SK_Vector_init_even_odd,
			&init_func_elem_size
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"SK_Vector_resize () reported failure."
			);

			error_detected = 1;

			break;
		}


		printf ("-----Resized-----\n\n");

		SK_Vector_print_opt
		(
			vec,
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1,
			1
		);


		//Make sure that SK_Vector_resize () used the proper number of
		//blocks to resize #vec to store #cur_num_elems number of
		//elements.
		//
		//SK_Vector_resize () SHOULD take into account negative /
		//positive slack when doing this. This means that blocks will
		//only be deallocated when "enough" are empty when decreasing
		//an #SK_Vector's size, and extra blocks will be added when
		//increasing an #SK_Vector's size.

		size_t min_num_blocks =

		SK_Vector_calc_min_blocks (elems_per_block, new_num_elems);


		size_t ideal_num_blocks = 0;


		if
		(
			(old_num_blocks > min_num_blocks)

			&&

			(old_num_blocks - min_num_blocks) > (neg_slack)
		)
		{
			//The ideal number of blocks should be equal to
			//#min_num_blocks here, because the number of blocks
			//used in #vec was reduced and the number of empty
			//blocks was greater than #neg_slack

			ideal_num_blocks = min_num_blocks;
		}


		else if (old_num_blocks < min_num_blocks)
		{
			//The ideal number of blocks should be equal to
			//#min_num_blocks plus some blocks as slack, because the
			//number of elements added to #vec required the
			//allocation of new blocks

			ideal_num_blocks = min_num_blocks + pos_slack;
		}

		else
		{
			//The ideal number of blocks should be equal to
			//#old_num_blocks, because #vec was neither
			//decreased/increased in number of elements enough to
			//justify reallocation.

			ideal_num_blocks = old_num_blocks;
		}



		size_t ideal_max_num_elems =

		ideal_num_blocks * elems_per_block;


		if
		(
			(ideal_num_blocks != vec->num_blocks)

			||

			(ideal_max_num_elems != vec->max_num_elems)

			||

			(new_num_elems != vec->cur_num_elems)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"SK_Vector_resize () placed wrong number of "
				"blocks/elements in #vec.\n"
				"vec->num_blocks = %zu    (ideally %zu)\n"
				"vec->max_num_elems = %zu (ideally %zu)\n"
				"vec->cur_num_elems = %zu (ideally %zu)\n",
				vec->num_blocks,
				ideal_num_blocks,
				vec->max_num_elems,
				ideal_max_num_elems,
				vec->cur_num_elems,
				new_num_elems
			);

			error_detected = 1;

			break;
		}


		//Check that the elements in #vec under index #old_num_elems
		//were not modified from their old values. Also make sure that
		//elements above #old_num_elems were initialized properly.

		for (size_t e_ind = 0; (e_ind < old_num_elems) && (e_ind < new_num_elems); e_ind += 1)
		{
			//Calculate what the ideal element in #vec would be at
			//this index

			size_t init_func_elem_size = elem_size;

			size_t good_elem = 0;


			if (e_ind < old_num_elems)
			{
				SK_Vector_init_sequential
				(
					(u8 *) &good_elem,
					e_ind,
					(void *) &init_func_elem_size
				);
			}

			else
			{
				SK_Vector_init_even_odd
				(
					(u8 *) &good_elem,
					e_ind,
					(void *) &init_func_elem_size
				);
			}


			//Fetch the actual element from #vec

			size_t real_elem = 0;

			SK_Vector_get_elem
			(
				vec,
				e_ind,
				(u8 *) &real_elem,
				sizeof (size_t)
			);


			if (real_elem != good_elem)
			{
				SK_DEBUG_PRINT
				(
					SK_VECTOR_L_DEBUG,
					"#vec = %p did not have elements set "
					"correctly. "
					"#good_elem = %zu, #real_elem = %zu",
					vec,
					good_elem,
					real_elem
				);

				error_detected = 1;

				break;
			}
		}



		//Propagate loop-breaking if error occurred in above loop

		if (error_detected)
		{
			break;
		}


		//Destroy the #SK_Vector created in this test iteration

		SK_Vector_destroy (vec);


		//Update the parameters to test in the next test-iteration with
		//a "pseudo-loop"

		int looped = 0;


		SK_inc_chain
		(
			&new_num_elems,
			1,
			BEG_NEW_NUM_ELEMS,
			END_NEW_NUM_ELEMS,
			INC_NEW_NUM_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&old_num_elems,
			looped,
			BEG_OLD_NUM_ELEMS,
			END_OLD_NUM_ELEMS,
			INC_OLD_NUM_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&elems_per_block,
			looped,
			BEG_ELEMS_PER_BLOCK,
			END_ELEMS_PER_BLOCK,
			INC_ELEMS_PER_BLOCK,
			&looped
		);

		SK_inc_chain
		(
			&elem_size,
			looped,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);

		SK_inc_chain
		(
			&neg_slack,
			looped,
			BEG_NEG_SLACK,
			END_NEG_SLACK,
			INC_NEG_SLACK,
			&looped
		);

		SK_inc_chain
		(
			&pos_slack,
			looped,
			BEG_POS_SLACK,
			END_POS_SLACK,
			INC_POS_SLACK,
			&looped
		);


		//End the testing if the "outer-most" parameter has looped

		if (looped)
		{
			go = 0;
		}
	}


	//Check if the test bench succeeded or not

	if (error_detected)
	{
		//Make sure to free memory that couldn't be freed as a result
		//of the test iteration stopping prematurely;

		SK_Vector_destroy (vec);


		printf ("\n\nSK_Vector_resize_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("\n\nSK_Vector_resize_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple testbench on SK_Vector_set_elem ()
 *
 *  FIXME : Should it be standard that test-benches here come with lists of
 *  their function coverage? In a way, this can be considered a test-bench of
 *  SK_Vector_get_elem () as well.
 *
 *  @return			Standard status code
 */

//FIXME : Perhaps this test-bench is TOO simple.

SlySr SK_Vector_set_elem_tb ()
{

	printf ("SK_Vector_set_elem_tb () : BEGIN\n\n");


	int error_detected = 0;


	//TODO : Consider making this test-bench use multiple test-iterations


	//Set some arbitrary parameters for the #SK_Vector to create

	const int64_t ELEM_SIZE =	sizeof (double);

	const int64_t ELEMS_PER_BLOCK =	8;

	const int64_t NUM_ELEMS =	200;

	const int64_t NEG_SLACK =	1;

	const int64_t POS_SLACK =	1;


	//Create an arbitrary #SK_Vector

	SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);


	SK_Vector * vec =

	SK_Vector_create
	(
		ELEM_SIZE,
		NUM_ELEMS,
		ELEMS_PER_BLOCK,
		NEG_SLACK,
		POS_SLACK,
		NULL,
		NULL,
		&sr
	);


	if ((NULL == vec) || (SLY_SUCCESS != sr.res))
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Failed to create the #SK_Vector"
		);

		error_detected = 1;

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	//Print out the original state of #vec

	printf ("-----Original-----\n\n");

	SK_Vector_print_opt
	(
		vec,
		stdout,
		SK_LineDeco_std (),
		1,
		1,
		1,
		1
	);


	//Set the elements in #vec to arbitrary values

	for (size_t e_sel = 0; e_sel < NUM_ELEMS; e_sel += 1)
	{
		double value = e_sel * 1.5;


		SlyDr dr =

		SK_Vector_set_elem
		(
			vec,
			e_sel,
			(u8 *) &value,
			sizeof (double)
		);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Failed to SET element at index %zu",
				e_sel
			);

			error_detected = 1;
		}
	}


	//Print out the new state of #vec after the elements have been set

	printf ("-----New-----\n\n");

	SK_Vector_print_opt
	(
		vec,
		stdout,
		SK_LineDeco_std (),
		1,
		1,
		1,
		1
	);


	//Check if the elements in #vec match what was put in

	for (size_t e_sel = 0; e_sel < NUM_ELEMS; e_sel += 1)
	{
		double value =	0;

		double ideal =	e_sel * 1.5;


		SlyDr dr =

		SK_Vector_get_elem
		(
			vec,
			e_sel,
			(u8 *) &value,
			sizeof (double)
		);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Failed to GET element at index %zu",
				e_sel
			);

			error_detected = 1;

			break;
		}


		if (value != ideal)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Bad set and/or get at index %zu.\n\n"
				"value : %lf\n"
				"ideal : %lf",
				e_sel,
				value,
				ideal
			);

			error_detected = 1;

			break;
		}
	}


	//Destroy the #SK_Vector used in this test

	SK_Vector_destroy (vec);


	//Report the success or failure of this function

	if (error_detected)
	{
		printf ("\n\nSK_Vector_set_elem_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("\n\nSK_Vector_set_elem_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple testbench on SK_Vector_rw_cust ()
 *
 *  @return			Standard status code
 */

//FIXME : Is this function TOO similar to SK_Vector_set_elem_tb ()?

SlySr SK_Vector_rw_cust_tb ()
{

	printf ("SK_Vector_rw_cust_tb () : BEGIN\n\n");


	int error_detected = 0;


	//TODO : Consider making this test-bench use multiple test-iterations


	//Set some arbitrary parameters for the #SK_Vector to create

	const int64_t ELEM_SIZE =	sizeof (double);

	const int64_t ELEMS_PER_BLOCK =	8;

	const int64_t NUM_ELEMS =	200;

	const int64_t NEG_SLACK =	1;

	const int64_t POS_SLACK =	1;


	//Create an arbitrary #SK_Vector

	SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);


	SK_Vector * vec =

	SK_Vector_create
	(
		ELEM_SIZE,
		NUM_ELEMS,
		ELEMS_PER_BLOCK,
		NEG_SLACK,
		POS_SLACK,
		NULL,
		NULL,
		&sr
	);


	if ((NULL == vec) || (SLY_SUCCESS != sr.res))
	{
		SK_DEBUG_PRINT
		(
			SK_VECTOR_L_DEBUG,
			"Failed to create the #SK_Vector"
		);

		error_detected = 1;

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	//Print out the original state of #vec

	printf ("-----Original-----\n\n");

	SK_Vector_print_opt
	(
		vec,
		stdout,
		SK_LineDeco_std (),
		1,
		1,
		1,
		1
	);


	//Set the elements in #vec to arbitrary values

	for (size_t e_sel = 0; e_sel < NUM_ELEMS; e_sel += 1)
	{
		double value = e_sel * 1.5;


		SlyDr dr =

		SK_Vector_rw_cust
		(
			vec,
			e_sel,
			SK_Vector_rw_cust_set_f64,
			&value
		);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Failed to SET element at index %zu",
				e_sel
			);

			error_detected = 1;
		}
	}


	//Print out the new state of #vec after the elements have been set

	printf ("-----New-----\n\n");

	SK_Vector_print_opt
	(
		vec,
		stdout,
		SK_LineDeco_std (),
		1,
		1,
		1,
		1
	);


	//Check if the elements in #vec match what was put in

	for (size_t e_sel = 0; e_sel < NUM_ELEMS; e_sel += 1)
	{
		double value =	0;

		double ideal =	e_sel * 1.5;


		SlyDr dr =

		SK_Vector_rw_cust
		(
			vec,
			e_sel,
			SK_Vector_rw_cust_get_f64,
			&value
		);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Failed to GET element at index %zu",
				e_sel
			);

			error_detected = 1;

			break;
		}


		if (value != ideal)
		{
			SK_DEBUG_PRINT
			(
				SK_VECTOR_L_DEBUG,
				"Bad set and/or get at index %zu.\n\n"
				"value : %lf\n"
				"ideal : %lf",
				e_sel,
				value,
				ideal
			);

			error_detected = 1;

			break;
		}
	}


	//Destroy the #SK_Vector used in this test

	SK_Vector_destroy (vec);


	//Report the success or failure of this function

	if (error_detected)
	{
		printf ("\n\nSK_Vector_rw_cust_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("\n\nSK_Vector_rw_cust_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a speed test between a plain array and an #SK_Vector undergoing
 *  several resize operations.
 *
 *  @return			Standard status code. This test will return
 *				#SK_UNKNOWN_ERROR if the #SK_Vector is measured
 *				to be slower in these resize operations.
 *
 *
 *  FIXME :
 *
 *  When an initialization function other than SK_Vector_init_int_zero () is
 *  used, the #SK_Vector often loses unless the test parameters are a bit
 *  contrived. There's always going to be cases where plain arrays win, but
 *  #SK_Vector loses in many cases where it should be possible to win.
 *
 *  For #SK_Vector to win in that case right now, the min/max size of the array
 *  have to be fairly large (equal or greater than 1024), and they have to be
 *  within ~25% of each other. Or, #USE_HASH can be set to 0 so that appending
 *  is tested, in which case #SK_Vector almost always wins.
 *
 *  The cause of this problem is that #SK_Vector requires new elements to be
 *  initialized, and almost all of the #SK_Vector_init_* () functions currently
 *  initialize byte-by-byte in a loop, which is much slower than doing it in
 *  groups of 4 or 8 bytes.
 *
 *  It is thought that as a result, #SK_Vector loses so much time during
 *  element initialization compared to the plain array, the fact is
 *  requires less elements to be calculated after resizes becomes irrelevant.
 *
 *  That said, this problem will not matter as much for SK_Vector_init_* ()
 *  functions that assume a given element size (such as SK_Vector_init_int_zero
 *  ()).
 */

SlySr SK_Vector_resize_speed_test_tb ()
{

	printf ("SK_Vector_resize_speed_test_tb () : BEGIN\n\n");


	//Set arbitrary test parameters that will be shared amongst the arrays
	//and #SK_Vectors

	const size_t MIN_SIZE =		1000;

	const size_t MAX_SIZE =		1500;

	const size_t SIZE_RANGE =	MAX_SIZE - MIN_SIZE;

	const size_t NUM_RESIZES =	100 * 1000;

	const int USE_HASH =		1;


	//Perform several resize operations with a plain array, and measure the
	//time it takes to complete

	struct timespec array_start_ts;

	clock_gettime (CLOCK_MONOTONIC, &array_start_ts);


	int * array = NULL;

	for (size_t resize_num = 0; resize_num <= NUM_RESIZES; resize_num += 1)
	{

		//Choose a value to resize to

		size_t new_size = 0;

		if (USE_HASH)
		{
			new_size =

			(SK_gold_hash_u64 (resize_num) % SIZE_RANGE) + MIN_SIZE;
		}

		else
		{
			new_size = resize_num % SIZE_RANGE;
		}


		//Perform the resize

		free (array);

		array = malloc (sizeof (int) * new_size);

		SK_NULL_RETURN (SK_VECTOR_L_DEBUG, array, SlySr_get (SLY_BAD_ARG));


		//Initialize the array
		//
		//(This has to be done to meet feature parity with the
		//#SK_Vector, which is initialized at creation and resizing.)

		for (size_t e_sel = 0; e_sel < new_size; e_sel += 1)
		{
			array [e_sel] = 0;
		}
	}


	//Get the stop time

	struct timespec array_stop_ts;

	clock_gettime (CLOCK_MONOTONIC, &array_stop_ts);


	//Clean up after the last resize above

	free (array);


	//Create an empty #SK_Vector to use in the next resize test

	const size_t ELEMS_PER_BLOCK =	128;

	const size_t NEG_SLACK =	4;

	const size_t POS_SLACK =	1;


	size_t elem_size = sizeof (int);


	SK_Vector * vec =

	SK_Vector_create
	(
		elem_size,
		0,
		ELEMS_PER_BLOCK,
		NEG_SLACK,
		POS_SLACK,
		SK_Vector_init_zero,
		&elem_size,
		NULL
	);


	SK_NULL_RETURN (SK_VECTOR_L_DEBUG, vec, SlySr_get (SLY_UNKNOWN_ERROR));


	//Perform several resize operations and measure the time it takes, but
	//instead use an #SK_Vector now

	struct timespec vec_start_ts;

	clock_gettime (CLOCK_MONOTONIC, &vec_start_ts);


	for (size_t resize_num = 0; resize_num <= NUM_RESIZES; resize_num += 1)
	{

		//Choose a value to resize to

		size_t new_size = 0;

		if (USE_HASH)
		{
			new_size =

			(SK_gold_hash_u64 (resize_num) % SIZE_RANGE) + MIN_SIZE;
		}

		else
		{
			new_size = resize_num % SIZE_RANGE;
		}


		//Resize the #SK_Vector, and make elements get initialized with
		//zeroes

		SK_Vector_resize
		(
			vec,
			new_size,
			SK_Vector_init_int_zero,
			NULL
		);
	}


	//Get the stop time

	struct timespec vec_stop_ts;

	clock_gettime (CLOCK_MONOTONIC, &vec_stop_ts);


	//Destroy the #SK_Vector used in the resizing test

	SK_Vector_destroy (vec);


	//Calculate the total time used to perform the resizing with the plain
	//array and #SK_Vector

	struct timespec array_ts;


	array_ts.tv_sec =

	array_stop_ts.tv_sec - array_start_ts.tv_sec;


	if (array_stop_ts.tv_nsec > array_start_ts.tv_nsec)
	{
		array_ts.tv_nsec =

		array_stop_ts.tv_nsec - array_start_ts.tv_nsec;
	}

	else
	{
		array_ts.tv_sec -= 1;


		array_ts.tv_nsec =

		(1000 * 1000 * 1000 + array_stop_ts.tv_nsec) - array_start_ts.tv_nsec;
	}



	struct timespec vec_ts;


	vec_ts.tv_sec =

	vec_stop_ts.tv_sec - vec_start_ts.tv_sec;


	if (vec_stop_ts.tv_nsec > vec_start_ts.tv_nsec)
	{
		vec_ts.tv_nsec =

		vec_stop_ts.tv_nsec - vec_start_ts.tv_nsec;
	}

	else
	{
		vec_ts.tv_sec -= 1;


		vec_ts.tv_nsec =

		(1000 * 1000 * 1000 + vec_stop_ts.tv_nsec) - vec_start_ts.tv_nsec;
	}


	//Determine which method was faster

	int vec_is_faster =

	(vec_ts.tv_sec < array_ts.tv_sec)

	||

	(
		(vec_ts.tv_sec == array_ts.tv_sec)

		&&

		(vec_ts.tv_nsec < array_ts.tv_nsec)
	);


	//Print out the results

	printf
	(
		"Plain Array : %" PRIu64 " sec, %" PRIu64 " nsec\n"
		"#SK_Vector  : %" PRIu64 " sec, %" PRIu64 " nsec\n\n",
		(u64) array_ts.tv_sec,
		(u64) array_ts.tv_nsec,
		(u64) vec_ts.tv_sec,
		(u64) vec_ts.tv_nsec
	);


	if (vec_is_faster)
	{
		printf ("#SK_Vector was faster\n\n");
	}

	else
	{
		printf ("#SK_Vector was equal or slower\n\n");
	}


	//Report the success or failure of this function

	if (0 == vec_is_faster)
	{
		printf ("SK_Vector_resize_speed_test_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("SK_Vector_resize_speed_test_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);

}

