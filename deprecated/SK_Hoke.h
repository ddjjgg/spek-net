/*!****************************************************************************
 *
 * @file
 * SK_Hoke.h
 *
 * See SK_Hoke.c for details.
 *
 *****************************************************************************/




#ifndef SK_HOKE_H
#define SK_HOKE_H




#include "./SK_Hoke_opaque.h"




#include "../SK_Yoke.h"




/*!
 *  Very similar to #SK_Yoke, except that it contains a #data_ptr field and a
 *  #data_opt field.
 *
 *  By including these extra fields, an #SK_Hoke is able to be "owned" by an
 *  object of a different type WITHOUT including the #SK_Hoke inside of that
 *  data structure itself.
 *
 *  In essence, #SK_Yoke's are "use-by-composition", while #SK_Hoke's are
 *  "use-by-association". In the object hierarchy, the #SK_Hoke can be
 *  horizontal to the object using it (hence the name "Hoke").
 *
 *  To be clear, there is nothing preventing an #SK_Hoke from being included in
 *  the data structure being associated with it, but it is generally not
 *  necessary.
 *
 *  A notable advantage of #SK_Hoke's is the ease of creating networks of
 *  objects with different data types. The object's in question can be
 *  associated via the #data_ptr field, and the data type can be represented
 *  via the #data_opt field.
 *
 *  Constructing networks of different data-type objects is possible with
 *  #SK_Yoke's, but not as easy. The mapping of #SK_Yoke to object would have
 *  to be calculated / stored somehow, which will most likely require an array
 *  separate from the group of #SK_Yoke's themselves.
 */

typedef struct SK_Hoke
{

	/// Used to handle the job of establishing networks.

	SK_Yoke		yoke;


	/// Pointer to the start of some data that has been associated with the
	/// #SK_Hoke.
	///
	/// The benefit of associating an object from a different data
	/// structure with an #SK_Hoke is that the #SK_Hoke can handle the
	/// job of creating networks for it, which can be quite error-prone
	/// otherwise.

	void *		data_ptr;


	/// An optional value to correspond with #data_ptr. This use of this
	/// field is not required, but it is available.
	///
	/// This field is particularly useful in the case that #data_ptr points
	/// to a plain array, in which case it can be used to represent the
	/// length of that array.
	///
	/// Or, if a network of different data types is created, this field can
	/// be used as a key for the data type of the object being pointed to.

	size_t		data_opt;

}

SK_Hoke;




size_t SK_Hoke_get_sizeof ();




SlySr SK_Hoke_init
(
	SK_Hoke *		skh,
	size_t			links_len,
	void *			data_ptr,
	size_t			data_opt
);




SlySr SK_Hoke_init_no_assoc
(
	SK_Hoke *		skh,
	size_t			links_len
);




SlyDr SK_Hoke_deinit (SK_Hoke * skh);




#endif //SK_HOKE_H

