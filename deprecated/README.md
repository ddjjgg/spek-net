This directory is for source files that are deprecated.

They are kept in the project tree as they will likely serve as a basis for
significantly refactored versions of the code that they represent.

Once such refactored versions are made, then the associated deprecated file
will be fully removed from the project.

