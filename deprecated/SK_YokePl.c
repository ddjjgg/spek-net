/*!****************************************************************************
 *
 * @file
 * SK_YokePl.c
 *
 * Contains an implementation of #SK_YokePl, which is a data structure very
 * similar to #SK_Yoke, except that it can also support per-link properties.
 *
 *****************************************************************************/




#include "./SK_YokePl.h"




/*!
 *  Initializes a specified #SK_YokePl
 *
 *  @param sky			The #SK_YokePl to initialize
 *
 *  @param links_len		See SK_Yoke_create ()
 *
 *  @return			Standard status code
 */

SlySr SK_YokePl_init
(
	SK_YokePl *		sky,
	size_t			links_len
)
{
	//Sanity check on arguments

	//FIXME : #SK_YokePl shold have its own debug flag, #SK_YOKE_PL_L_DEBUG

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, sky, SlySr_get (SLY_BAD_ARG));


	//Initialize the #SK_Yoke inside of #sky

	SlySr sr = SK_Yoke_init (&(sky->yoke), links_len);

	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"Could not initialize #sky->yoke (skh = %p)",
			sky
		);

		return sr;
	}


	//FIXME : Add code to initialize the #SK_Vector inside of #sky

	//return sr;

	return SlySr_get (SLY_BAD_ARG);
}
