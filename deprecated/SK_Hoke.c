/*!****************************************************************************
 *
 * @file
 * SK_Hoke.c
 *
 * Contains an implementation of #SK_Hoke, which is very similar to #SK_Yoke,
 * but with additional fields that make it easier to explicitly associate it
 * with a different object. This generally makes #SK_Hoke easier to use than
 * #SK_Yoke, which provides no internal means of recording which object it
 * "belongs" to. Using #SK_Hoke, it is easier to build networks of objects of
 * dissimilar data types as well.
 *
 *****************************************************************************/




#include "./SK_Hoke.h"




#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "../SK_debug.h"
#include "../SK_misc.h"
#include "../SK_numeric_types.h"
#include "../SK_Vector.h"
#include "../SK_Yoke.h"




/*!
 * This provides the mappings needed to use an #SK_Hoke as if it were an
 * #SK_Yoke.
 */

const SK_YokeIntf SK_Hoke_as_SK_Yoke =

{

	.init =

	(SlySr (*) (void *, size_t)) SK_Hoke_init_no_assoc,


	.deinit =

	(SlyDr (*) (void *)) SK_Hoke_deinit,


	.get_sizeof =

	(size_t (*) ()) SK_Hoke_get_sizeof,


	.print =

	(SlySr (*) (const void *, FILE *, SK_LineDeco)) SK_Hoke_print


	//FIXME : Write the forwarding function SK_Hoke_unlink ()

	//.unlink =
	//
	//(SlyDr (*) (void *, void *)) SK_Hoke_unlink


	//FIXME : Finish this initialization

};




/*!
 *  Returns the result of sizeof () on #SK_Hoke. This is a required function by
 *  #SK_YokeIntf.
 *
 *  @return			The result of calling sizeof () on #SK_Hoke.
 */

size_t SK_Hoke_get_sizeof ()
{
	return sizeof (SK_Hoke);
}




/*!
 *  Creates an #SK_Hoke, which initially is not associated with any object, and
 *  has no active connections.
 *
 *  @warning
 *  Some time after calling this function, SK_Hoke_destroy () must be called on
 *  the returned pointer, otherwise a memory leak will occur.
 *
 *  @param links_len		See SK_Yoke_create ()
 *
 *  @param sr_ptr		Where to place a #SlySr value indicating the
 *				success of this function. This CAN be NULL if
 *				this information is not desired.
 *
 *  @return			A pointer to the newly createed #SK_Hoke
 *				structure upon success; NULL is returned
 *				otherwise.
 */

SK_Hoke * SK_Hoke_create
(
	size_t			links_len,
	SlySr *			sr_ptr
)
{
	//Allocate space for the #SK_Hoke itself

	SK_Hoke * skh = malloc (sizeof (SK_Hoke));

	if (NULL == skh)
	{
		if (NULL != sr_ptr)
		{
			*sr_ptr = SlySr_get (SLY_BAD_ALLOC);
		}

		SK_NULL_RETURN (SK_YOKE_L_DEBUG, skh, NULL);
	}


	//Use SK_Hoke_init () to initialize #skh

	SlySr sr = SK_Hoke_init (skh, links_len, NULL, 0);


	if (SLY_SUCCESS != sr.res)
	{
		free (skh);


		if (NULL != sr_ptr)
		{
			*sr_ptr = sr;
		}


		SK_RES_RETURN (SK_YOKE_L_DEBUG, sr.res, NULL);
	}


	//Report success and return the newly created #SK_Yoke

	if (NULL != sr_ptr)
	{
		*sr_ptr = SlySr_get (SLY_SUCCESS);
	}

	return skh;
}




/*!
 *  Creates an #SK_Hoke, which initially CAN be associated with an object, and
 *  has no active connections.
 *
 *  This is very similar to SK_Hoke_create (), except that it provides
 *  arguments necessary to associate an object with the newly created #SK_Hoke.
 *
 *  @warning
 *  Some time after calling this function, SK_Hoke_destroy () must be called on
 *  the returned pointer, otherwise a memory leak will occur.
 *
 *  @param links_len		See SK_Yoke_create ()
 *
 *  @param data_ptr		Pointer to the object to associate with the
 *				created #SK_Hoke
 *
 *  @param data_opt		An optional #size_t value to store alongside
 *				#data_ptr. This is very useful for storing
 *				array lengths, for example, if #data_ptr points
 *				to an array.
 *
 *  @param sr_ptr		Where to place a #SlySr value indicating the
 *				success of this function. This CAN be NULL if
 *				this information is not desired.
 *
 *  @return			A pointer to the newly createed #SK_Hoke
 *				structure upon success; NULL is returned
 *				otherwise.
 */

SK_Hoke * SK_Hoke_create_opt
(
	size_t			links_len,
	void *			data_ptr,
	size_t			data_opt,
	SlySr *			sr_ptr
)
{
	//Allocate space for the #SK_Hoke itself

	SK_Hoke * skh = malloc (sizeof (SK_Hoke));

	if (NULL == skh)
	{
		if (NULL != sr_ptr)
		{
			*sr_ptr = SlySr_get (SLY_BAD_ALLOC);
		}

		SK_NULL_RETURN (SK_YOKE_L_DEBUG, skh, NULL);
	}


	//Use SK_Hoke_init () to initialize #skh

	SlySr sr = SK_Hoke_init (skh, links_len, data_ptr, data_opt);


	if (SLY_SUCCESS != sr.res)
	{
		free (skh);


		if (NULL != sr_ptr)
		{
			*sr_ptr = sr;
		}


		SK_RES_RETURN (SK_YOKE_L_DEBUG, sr.res, NULL);
	}


	//Report success and return the newly created #SK_Yoke

	if (NULL != sr_ptr)
	{
		*sr_ptr = SlySr_get (SLY_SUCCESS);
	}

	return skh;
}




/*!
 *  Initializes an #SK_Hoke which has had memory allocated for it but has not
 *  had any of its fields initialized.
 *
 *  This logic is not directly inside of SK_Hoke_create () so that it can be
 *  potentially re-used by other data structures that include #SK_Hoke in a
 *  non-PIMPL fashion
 *
 *  @param skh			The #SK_Hoke to initialize
 *
 *  @param links_len		See SK_Yoke_create ()
 *
 *  @param data_ptr		Pointer to the object to associate with the
 *				created #SK_Hoke
 *
 *  @param data_opt		An optional #size_t value to store alongside
 *				#data_ptr. This is very useful for storing
 *				array lengths, for example, if #data_ptr points
 *				to an array.
 *
 *  @return			Standard status code
 */

SlySr SK_Hoke_init
(
	SK_Hoke *		skh,
	size_t			links_len,
	void *			data_ptr,
	size_t			data_opt
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, skh, SlySr_get (SLY_BAD_ARG));


	//Initialize the #SK_Yoke inside of #skh

	SlySr sr = SK_Yoke_init (&(skh->yoke), links_len);

	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_YOKE_L_DEBUG,
			"Could not initialize #skh->yoke (skh = %p)",
			skh
		);

		return sr;
	}


	//Initialize #skh->data_ptr and #skh->data_opt with the given arguments

	skh->data_ptr = data_ptr;

	skh->data_opt = data_opt;


	return SlySr_get (SLY_SUCCESS);
}



/*!
 *  Does thing as SK_Hoke_init, EXCEPT the #data_ptr and #data_opt arguments
 *  are given the values NULL and 0 respectively. This makes it so that
 *  initially, the #SK_Hoke is NOT associated with any object yet.
 *
 *  This function needs to exist to provide a mapping needed to initialize
 *  #SK_Hoke_as_SK_Yoke.
 *
 *  @param skh			The #SK_Hoke to initialize
 *
 *  @param links_len		See SK_Yoke_create ()
 *
 *  @return			Standard status code
 */

SlySr SK_Hoke_init_no_assoc
(
	SK_Hoke *		skh,
	size_t			links_len
)
{
	return

	SK_Hoke_init
	(
		skh,
		links_len,
		NULL,
		0
	);
}




/*!
 *  Deinitializes an #SK_Hoke object, which means to prepare it for
 *  deallocation.
 *
 *  This WILL automatically unlink #skh from all other #SK_Hoke's it is
 *  currently linked to.
 *
 *  @param skh			The #SK_Hoke object to deinitialize
 *
 *  @return			Standard status code
 */

SlyDr SK_Hoke_deinit (SK_Hoke * skh)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, skh, SlyDr_get (SLY_BAD_ARG));


	//Deinitialize the internal #SK_Yoke in #skh. Since #skh->yoke handles
	//maintaining all of the connections for #skh, this will unlink #skh
	//from all other #SK_Hoke's.

	SK_Yoke_deinit (&(skh->yoke));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Destroys an #SK_Hoke object.
 *
 *  Very similar to SK_Yoke_destroy (), this will automatically take care of
 *  unlinking the #SK_Hoke from any other #SK_Hoke's it still has active
 *  connections to.
 *
 *  @warning
 *  This function does NOT alter the object associated with the given #SK_Hoke
 *  (i.e. the one pointed to by #skh->data_ptr). So if that object requires
 *  deallocation in some way, that should take place BEFORE calling this
 *  function, or the pointer to that object should be remembered independently
 *  from this function.
 *
 *  @param skh			The #SK_Hoke object to destroy
 *
 *  @return			Standard status code
 */

SlyDr SK_Hoke_destroy (SK_Hoke * skh)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, skh, SlyDr_get (SLY_BAD_ARG));


	//De-initialize the #SK_Hoke, which will also take care of unlinking
	//the #SK_Hoke from all other #SK_Hoke's it is connected to

	SK_Hoke_deinit (skh);


	//Free the #SK_Hoke data structure itself

	free (skh);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Prints out the contents of an #SK_Hoke
 *
 *  @param skh			The #SK_Hoke to print out
 *
 *  @param f_ptr		The #FILE to print to, like #stdout
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @param header_flag		Flag indicating that the information header
 *				about the #SK_Yoke should be printed out, which
 *				just contains the raw member values
 *
 *  @param active_links_flag	Flag indicating whether or not to print out the
 *				active links contained within #skh
 *
 *  @param inactive_links_flag	Flag indicating whether or not to print out the
 *				inactive links contained within #skh
 *
 *  @return			Standard status code
 */

SlySr SK_Hoke_print_opt
(
	const SK_Hoke *		skh,
	FILE *			f_ptr,
	SK_LineDeco		deco,
	int			header_flag,
	int			active_links_flag,
	int			inactive_links_flag
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, skh, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//#SlySr values for the individual SK_LineDeco_print_opt () calls

	const size_t NUM_RES = 14;

	SlySr sr [NUM_RES];


	for (size_t sr_num = 0; sr_num < NUM_RES; sr_num += 1)
	{
		sr [sr_num] = SlySr_get (SLY_SUCCESS);
	}


	//Print out the header information for the #SK_Yoke

	if (header_flag)
	{

		sr [0] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"SK_Hoke         @ %p",
			skh
		);


		sr [1] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"yoke.links      = %p",
			skh->yoke.links
		);


		sr [2] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"yoke.links_len  = %zu",
			skh->yoke.links_len
		);


		sr [3] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"yoke.links_cnt  = %zu",
			skh->yoke.links_cnt
		);


		sr [4] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"data_ptr        = %p",
			skh->data_ptr
		);


		sr [5] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			"data_opt        = %zu",
			skh->data_opt
		);


		sr [6] =

		SK_LineDeco_print_opt
		(
			f_ptr,
			deco,
			""
		);
	}


	//Create a version of #deco with additional indentation

	SK_LineDeco id_deco = deco;

	id_deco.pre_str_cnt += 1;


	//Split #id_deco into its beginning and ending portions

	SK_LineDeco id_deco_beg = id_deco;

	id_deco_beg.post_str_cnt = 0;


	SK_LineDeco id_deco_end = id_deco;

	id_deco_end.pre_str_cnt = 0;


	//Print out the links of the #SK_Yoke, while respecting
	//#active_links_flag and #inactive_links_flag

	if (active_links_flag || inactive_links_flag)
	{
		sr [7] =

		SK_LineDeco_print_opt (f_ptr, deco, "Links");


		sr [8] =

		SK_LineDeco_print_opt (f_ptr, deco, "{");


		int link_printed = 0;

		for (size_t l_sel = 0; l_sel < skh->yoke.links_len; l_sel += 1)
		{
			SK_YokeLink cur_link =	skh->yoke.links [l_sel];

			int cur_active =	SK_YokeLink_is_active (cur_link);

			if
			(
				(cur_active && active_links_flag)

				||

				((! cur_active) && inactive_links_flag)
			)
			{

				SlySr sr_0 =

				SK_LineDeco_print_opt
				(
					f_ptr,
					id_deco_beg,
					"%03lu : ",
					l_sel
				);


				SlySr sr_1 =

				SK_YokeLink_print_opt
				(
					cur_link,
					f_ptr,
					id_deco_end
				);


				//If any of the printing attempts in this loop
				//fail, remember that for the rest of the loop

				if (SLY_SUCCESS != sr_0.res)
				{
					sr [9] = sr_0;
				}

				if (SLY_SUCCESS != sr_1.res)
				{
					sr [10] = sr_1;
				}


				link_printed = 1;
			}
		}


		if (! link_printed)
		{
			sr [11] =

			SK_LineDeco_print_opt (f_ptr, id_deco, "(No Links)");
		}


		sr [12] =

		SK_LineDeco_print_opt (f_ptr, deco, "}");


		sr [13] =

		SK_LineDeco_print_opt (f_ptr, deco, "");
	}


	//Check if the printing was malformed, and do not return success if
	//that is the case

	for (size_t sr_num = 0; sr_num < NUM_RES; sr_num += 1)
	{
		if (SLY_SUCCESS != sr [sr_num].res)
		{
			return sr [sr_num];
		}
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Prints out the contents of an #SK_Hoke
 *
 *  @param skh			The #SK_Hoke to print out
 *
 *  @param f_ptr		The #FILE to print to, like #stdout
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_Hoke_print
(
	const SK_Hoke *		skh,
	FILE *			f_ptr,
	SK_LineDeco		deco
)
{
	return

	SK_Hoke_print_opt
	(
		skh,
		f_ptr,
		deco,
		1,
		1,
		0
	);
}




/*!
 *  Sets #data_ptr inside of an #SK_Hoke. When #data_ptr points to some given
 *  object, the #SK_Hoke is considered to be "associated" with that object. If
 *  #data_ptr is set to NULL, that indicates that no object is associated with
 *  the #SK_Hoke.
 *
 *  @param skh			The #SK_Hoke to modify
 *
 *  @param ptr			Pointer to the object to associate with #skh
 *
 *  @return			Standard status code
 */

SlyDr SK_Hoke_set_data_ptr
(
	SK_Hoke *	skh,
	void *		ptr
)
{
	//Santiy check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, skh, SlyDr_get (SLY_BAD_ARG));


	//Associate the object with #skh

	skh->data_ptr = ptr;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets #data_opt inside of an #SK_Hoke, which is a #size_t value that is
 *  considered relevant to the object the #SK_Hoke is associated with.
 *
 *  For example, if SK_Hoke_set_data_ptr () was previously used to associate an
 *  #SK_Hoke with an array, this function can be used to store the length of
 *  that array in the #SK_Hoke.
 *
 *  The use of the #data_opt field in #SK_Hoke is completely optional. The
 *  #SK_Hoke data structure itself makes no use of this value. Basically, it is
 *  up to the user of #SK_Hoke to decide how exactly #data_opt will be
 *  interpreted.
 *
 *  @param skh			The #SK_Hoke to modify
 *
 *  @param opt			An arbitrary value to store in #skh
 *
 *  @return			Standard status code
 */

SlyDr SK_Hoke_set_data_opt
(
	SK_Hoke *	skh,
	size_t		opt
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, skh, SlyDr_get (SLY_BAD_ARG));


	//Store a value in #skh that is considered related to the object
	//associated with #skh

	skh->data_opt = opt;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Fetches #data_ptr from an #SK_Hoke, which was either set at creation or
 *  with SK_Hoke_set_data_ptr ().
 *
 *  @param skh			The #SK_Hoke to examine
 *
 *  @param ptr			Where to place the value of #data_ptr
 *
 *  @return			Standard status code
 */

SlyDr SK_Hoke_get_data_ptr
(
	SK_Hoke *	skh,
	void **		ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, skh, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));


	//Retrieve the value of the #data_ptr field

	*ptr = skh->data_ptr;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Fetches #data_opt from an #SK_Hoke, which was either set at creation or
 *  with SK_Hoke_set_data_opt ().
 *
 *  @param skh			The #SK_Hoke to examine
 *
 *  @param opt			Where to place the value of #data_opt
 *
 *  @return			Standard status code
 */

SlyDr SK_Hoke_get_data_opt
(
	SK_Hoke *	skh,
	size_t *	opt
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_YOKE_L_DEBUG, skh, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_YOKE_L_DEBUG, opt, SlyDr_get (SLY_BAD_ARG));


	//Retrieve the value of the #data_opt field

	*opt = skh->data_opt;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Unlinks 2 connected #SK_Hoke data structures, provided that they are linked
 *  together. Otherwise, this function does not alter the input #SK_Hoke's.
 *
 *  This function follows the same conventions as SK_Yoke_unlink_w_info ().
 *
 *  @param skh_0		The 1st #SK_Hoke, which may or may not be
 *  				linked to #skh_1
 *
 *  @param skh_1		The 2nd #SK_Hoke, which may or may not be
 *				linked to #skh_0
 *
 *  @param link_existed		See SK_Yoke_unlink_w_info ()
 *
 *  @param skh_0_l_ind		See SK_Yoke_unlink_w_info ()
 *
 *  @param skh_1_l_ind		See SK_Yoke_unlink_w_info ()
 *
 *  @return			See SK_Yoke_unlink_w_info ()
 */

SlyDr SK_Hoke_unlink_w_info
(
	SK_Hoke *	skh_0,
	SK_Hoke *	skh_1,
	int *		link_existed,
	size_t *	skh_0_l_ind,
	size_t *	skh_1_l_ind
)
{

	return

	SK_Yoke_unlink_w_info
	(
		&(skh_0->yoke),
		&(skh_1->yoke),
		link_existed,
		skh_0_l_ind,
		skh_1_l_ind
	);

}




/*!
 *  Unlinks all of the hokes from a singular #SK_Hoke.
 *
 *  @param skh			The #SK_Hoke to have all of its connections
 *				unlinked
 *
 *  @return			See SK_Yoke_unlink_all ()
 */

SlyDr SK_Hoke_unlink_all
(
	SK_Hoke *	skh
)
{
	return SK_Yoke_unlink_all (&(skh->yoke));
}




/*!
 *  Reallocates the array of connections at #skh->yoke.links so that a
 *  different number of connections can be supported by the hoke.
 *
 *  @param skh			The #SK_Hoke to modify
 *
 *  @param new_links_len	See SK_Yoke_links_realloc ()
 *
 *  @param preserve_flag	See SK_Yoke_links_realloc ()
 */

SlySr SK_Hoke_links_realloc
(
	SK_Hoke *		skh,
	size_t			new_links_len,
	int			preserve_flag
)
{

	return

	SK_Yoke_links_realloc
	(
		&(skh->yoke),
		new_links_len,
		preserve_flag
	);

}




/*!
 *  Links together 2 #SK_Hoke data structures, provided that there is a link
 *  available to use in both objects.
 *
 *  This function follows the same conventions as SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param skh_0		The #SK_Hoke which should be linked to #skh_1
 *
 *  @param need_realloc_0	See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param skh_0_l_ind		See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param skh_1		The #SK_Hoke which should be linked to #skh_0
 *
 *  @param need_realloc_1	See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param skh_1_l_ind		See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @param link_existed		See SK_Yoke_link_no_alloc_w_info ()
 *
 *  @return			See SK_Yoke_link_no_alloc_w_info ()
 */

SlySr SK_Hoke_link_no_alloc_w_info
(
	SK_Hoke *	skh_0,
	int *		need_realloc_0,
	size_t *	skh_0_l_ind,

	SK_Hoke *	skh_1,
	int *		need_realloc_1,
	size_t *	skh_1_l_ind,

	int *		link_existed
)
{
	return

	SK_Yoke_link_no_alloc_w_info
	(
		&(skh_0->yoke),
		need_realloc_0,
		skh_0_l_ind,

		&(skh_1->yoke),
		need_realloc_1,
		skh_1_l_ind,

		link_existed
	);
}




/*!
 *  This checks if a link already exists between 2 #SK_Hoke's.
 *
 *  This follows the same conventions as SK_Yoke_link_exists ().
 *
 *  @param skh_0		The 1st #SK_Hoke, which may or may not be
 *				linked to #skh_1
 *
 *  @param skh_1		The 2nd #SK_Hoke, which may or may not be
 *				linked to #skh_0
 *
 *  @param link_exists		See SK_Yoke_link_exists ()
 *
 *  @param skh_0_l_ind		See SK_Yoke_link_exists ()
 *
 *  @param skh_1_l_ind		See SK_Yoke_link_exists ()
 *
 *  @return			See SK_Yoke_link_exists ()
 */

SlyDr SK_Hoke_link_exists
(
	SK_Hoke *	skh_0,
	SK_Hoke *	skh_1,
	int *		link_exists,
	size_t *	skh_0_l_ind,
	size_t *	skh_1_l_ind
)
{
	return

	SK_Yoke_link_exists
	(
		&(skh_0->yoke),
		&(skh_1->yoke),
		link_exists,
		skh_0_l_ind,
		skh_1_l_ind
	);
}




/*!
 *  Links together 2 #SK_Hoke data structures, and automatically reallocates
 *  the links in the #SK_Hoke data structures as necessary to allow this to
 *  happen.
 *
 *  This follows the same conventions as #SK_Yoke_link_w_info ().
 *
 *  @param skh_0		The 1st #SK_Hoke, which may or may not already
 *  				be linked to #skh_1
 *
 *  @param skh_0_l_ind		See SK_Yoke_link_w_info ()
 *
 *  @parma skh_0_incr		See SK_Yoke_link_w_info ()
 *
 *  @param skh_1		The 2nd #SK_Hoke, which may or may not already
 *				be linked to #skh_0
 *
 *  @param skh_1_l_ind		See SK_Yoke_link_w_info ()
 *
 *  @param skh_1_incr		See SK_Yoke_link_w_info ()
 *
 *  @param link_existed		See SK_Yoke_link_w_info ()
 *
 *  @return			Standard status code
 */

SlySr SK_Hoke_link_w_info
(
	SK_Hoke *	skh_0,
	size_t *	skh_0_l_ind,
	size_t		skh_0_incr,

	SK_Hoke *	skh_1,
	size_t *	skh_1_l_ind,
	size_t		skh_1_incr,

	int *		link_existed
)
{
	return

	SK_Yoke_link_w_info
	(
		&(skh_0->yoke),
		skh_0_l_ind,
		skh_0_incr,

		&(skh_1->yoke),
		skh_1_l_ind,
		skh_1_incr,

		link_existed
	);
}




/*!
 *  Performs a simple test bench on SK_Hoke_create_opt ()
 *
 *  @return			Standard status code
 */

SlySr SK_Hoke_create_opt_tb ()
{
	printf ("SK_Hoke_create_opt_tb () : BEGIN\n\n");


	const u32 NUM_TESTS =	32;

	int error_detected =	0;

	SK_Hoke * skh =		NULL;


	for (u32 test_num = 0; test_num < NUM_TESTS; test_num += 1)
	{
		printf
		(
			"SK_Hoke_create_opt_tb () iteration #%" PRIu32 "\n\n",
			test_num
		);


		//Select some arbitrary parameters for the #SK_Hoke to create

		size_t links_len =	(test_num);

		int arb_num = 		(test_num * 10);

		void * data_ptr =	&arb_num;

		size_t data_opt =	sizeof (arb_num);


		//Using the arbitrary arguments selected above, create an
		//#SK_Hoke

		SlySr skh_sr = SlySr_get (SLY_UNKNOWN_ERROR);


		skh =

		SK_Hoke_create_opt
		(
			links_len,
			data_ptr,
			data_opt,
			&skh_sr
		);


		if (SLY_SUCCESS != skh_sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"Coulc not create #SK_Hoke with parameters:\n"
				"links_len = %lu\n"
				"data_ptr =  %p\n"
				"data_opt =  %zu\n"
				"skh_sr =    %s\n\n",
				links_len,
				data_ptr,
				data_opt,
				SlySrStr_get (skh_sr).str
			);

			error_detected = 1;

			break;
		}


		SK_Hoke_print_opt
		(
			skh,
			stdout,
			SK_LineDeco_std (),
			1,
			1,
			1
		);


		//Check that the returned pointer is consistent with the
		//returned #SlySr

		if ((NULL == skh) && (SLY_SUCCESS == skh_sr.res))
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#skh was NULL when #skh_sr = %s\n\n",
				SlySrStr_get (skh_sr).str
			);

			error_detected = 1;

			break;
		}


		//Ensure that the selected parameters were actually respected

		if (links_len != skh->yoke.links_len)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#links_len was not respected.\n\n"
			);

			error_detected = 1;

			break;
		}


		if (data_ptr != skh->data_ptr)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#data_ptr was not respected.\n\n"
			);

			error_detected = 1;

			break;
		}

		if (data_opt != skh->data_opt)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#data_opt was not respected.\n\n"
			);

			error_detected = 1;

			break;
		}


		//Detect a bad configuration of #yoke inside of #skh

		int e_flag = 0;

		SK_Yoke_error_check
		(
			&(skh->yoke),
			stdout,
			"SK_Hoke_create_tb () : ",
			"\n",
			&e_flag
		);

		if (0 != e_flag)
		{
			error_detected = 1;

			break;
		}


		//Free memory that was allocated in this test iteration

		SK_Hoke_destroy (skh);
	}


	//Check if the test bench has succeeded or not

	if (error_detected)
	{
		//Make sure to free memory that couldn't be freed as a result
		//of the test iteration stopping prematurely

		SK_Hoke_destroy (skh);


		printf ("SK_Hoke_create_opt_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("SK_Hoke_create_opt_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a general test-bench of the #SK_Hoke -> #SK_Yoke forwarding
 *  functions.
 *
 *  More detailed tests of the logic behind these functions is in the #SK_Yoke
 *  test-benches. This test-bench itself is somewhat ad-hoc, but that is fine
 *  as long as it ensures the forwarding function work properly.
 *
 *  @return			Standard status code
 */

//FIXME : This test-bench is not finished. It uses copy-pasted lines at some
//points where small loops should be used instead, and it also does not check
//the return value of some functions that return a #SlySr

SlySr SK_Hoke_test_forwarding_func_tb ()
{
	printf ("SK_Hoke_test_forwarding_func_tb () : BEGIN\n\n");


	//Select some arbitrary parameters for this test-bench

	const size_t NUM_HOKES =	3;

	const size_t INIT_NUM_LINKS =	1;


	//Create some arbitrary #SK_Hoke's for linking / unlinking tests

	SK_Hoke * skh [NUM_HOKES];

	int error_detected = 0;


	for (size_t h_sel = 0; h_sel < NUM_HOKES; h_sel += 1)
	{
		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);

		skh [h_sel] = SK_Hoke_create (INIT_NUM_LINKS, &sr);


		//Checks that #skh [h_sel] was successfully created

		if (SLY_SUCCESS != sr.res)
		{

			//Deallocate any memory used in this function

			for (size_t d_sel = 0; d_sel <= h_sel; d_sel += 1)
			{
				SK_Hoke_destroy (skh [d_sel]);
			}


			SK_RES_RETURN (SK_YOKE_L_DEBUG, sr.res, sr);
		}
	}


	//Print the initial state of the #SK_Hoke's

	printf ("\n\nInitial state of #skh []\n\n");

	SK_Hoke_print_opt (skh [0], stdout, SK_LineDeco_std (), 1, 1, 1);
	SK_Hoke_print_opt (skh [1], stdout, SK_LineDeco_std (), 1, 1, 1);
	SK_Hoke_print_opt (skh [2], stdout, SK_LineDeco_std (), 1, 1, 1);


	//Check that SK_Hoke_link_exists () detects a non-existant link

	if (0 == error_detected)
	{
		int link_exists = 0;


		SK_Hoke_link_exists (skh [0], skh [1], &link_exists, NULL, NULL);


		if (link_exists)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#link_exists = %d when it should have been 0.",
				link_exists
			);

			error_detected = 1;
		}
	}


	//Check that SK_Hoke_link_no_alloc_w_info () can establish a link

	if (0 == error_detected)
	{
		//SlySr sr =

		SK_Hoke_link_no_alloc_w_info
		(
			skh [0],
			NULL,
			NULL,

			skh [1],
			NULL,
			NULL,

			NULL
		);


		int link_exists = 0;

		SK_Hoke_link_exists (skh [0], skh [1], &link_exists, NULL, NULL);


		if (0 == link_exists)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#link_exists = %d when it should have been 1.",
				link_exists
			);

			error_detected = 1;
		}


		printf ("\n\nState of #skh [] after linking 0 <-> 1\n\n");

		SK_Hoke_print_opt (skh [0], stdout, SK_LineDeco_std (), 1, 1, 1);
		SK_Hoke_print_opt (skh [1], stdout, SK_LineDeco_std (), 1, 1, 1);
		SK_Hoke_print_opt (skh [2], stdout, SK_LineDeco_std (), 1, 1, 1);
	}



	//Check that SK_Hoke_unlink_w_info () can remove a link

	if (0 == error_detected)
	{
		SK_Hoke_unlink_w_info
		(
			skh [0],
			skh [1],
			NULL,
			NULL,
			NULL
		);


		int link_exists = 0;

		SK_Hoke_link_exists (skh [0], skh [1], &link_exists, NULL, NULL);


		if (1 == link_exists)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"#link_exists = %d when it should have been 0.",
				link_exists
			);

			error_detected = 1;
		}


		printf ("\n\nState of #skh [] after unlinking 0 <-> 1\n\n");

		SK_Hoke_print_opt (skh [0], stdout, SK_LineDeco_std (), 1, 1, 1);
		SK_Hoke_print_opt (skh [1], stdout, SK_LineDeco_std (), 1, 1, 1);
		SK_Hoke_print_opt (skh [2], stdout, SK_LineDeco_std (), 1, 1, 1);
	}


	//Check that SK_Hoke_link_w_info () can establish links.
	//
	//Note, this section establishes a fully connected network between the
	//#SK_Hoke's as well.

	for (size_t h_sel_0 = 0; h_sel_0 < NUM_HOKES; h_sel_0 += 1)
	{
		//Propagate loop breaking if an error-occurs

		if (error_detected)
		{
			break;
		}


		for (size_t h_sel_1 = h_sel_0 + 1; h_sel_1 < NUM_HOKES; h_sel_1 += 1)
		{
			//SlySr sr =

			SK_Hoke_link_w_info
			(
				skh [h_sel_0],
				NULL,
				1,

				skh [h_sel_1],
				NULL,
				1,

				NULL
			);


			int link_exists = 0;

			SK_Hoke_link_exists
			(
				skh [h_sel_0],
				skh [h_sel_1],
				&link_exists,
				NULL,
				NULL
			);


			if (0 == link_exists)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"For #h_sel_0 = %zu, "
					"#h_sel_1 = %zu, the value of "
					"#link_exists = %d",
					h_sel_0,
					h_sel_1,
					link_exists
				);

				error_detected = 1;
			}


			printf
			(
				"\n\nState of #skh [] after linking %zu <-> %zu\n\n",
				h_sel_0,
				h_sel_1
			);

			SK_Hoke_print_opt (skh [0], stdout, SK_LineDeco_std (), 1, 1, 1);
			SK_Hoke_print_opt (skh [1], stdout, SK_LineDeco_std (), 1, 1, 1);
			SK_Hoke_print_opt (skh [2], stdout, SK_LineDeco_std (), 1, 1, 1);
		}
	}


	//Test that SK_Hoke_links_realloc () can reallocate links in an
	//#SK_Hoke without disturbing the already existing links

	if (0 == error_detected)
	{
		//SlySr sr =

		SK_Hoke_links_realloc
		(
			skh [0],
			2 * NUM_HOKES,
			1
		);


		if ((2 * NUM_HOKES) != skh [0]->yoke.links_len)
		{
			SK_DEBUG_PRINT
			(
				SK_YOKE_L_DEBUG,
				"Link reallocation failed, "
				"#skh [0]->yoke.links_len = %zu",
				skh [0]->yoke.links_len
			);

			error_detected = 1;
		}


		for (size_t h_sel = 1; h_sel < NUM_HOKES; h_sel += 1)
		{

			int link_exists = 0;

			SK_Hoke_link_exists
			(
				skh [0],
				skh [h_sel],
				&link_exists,
				NULL,
				NULL
			);


			if (0 == link_exists)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"#link_exists = %d, meaning that link "
					"in #skh [0] was destroyed.",
					link_exists
				);

				error_detected = 1;

				break;
			}
		}


		printf
		(
			"\n\nState of #skh [] after reallocating links in 0\n\n"
		);

		SK_Hoke_print_opt (skh [0], stdout, SK_LineDeco_std (), 1, 1, 1);
		SK_Hoke_print_opt (skh [1], stdout, SK_LineDeco_std (), 1, 1, 1);
		SK_Hoke_print_opt (skh [2], stdout, SK_LineDeco_std (), 1, 1, 1);
	}



	//Tests if #SK_Hoke_unlink_all () can remove all of the active links
	//from an #SK_Hoke

	if (0 == error_detected)
	{
		SK_Hoke_unlink_all (skh [0]);


		for (size_t h_sel = 0; h_sel < NUM_HOKES; h_sel += 1)
		{
			int link_exists = 0;

			SK_Hoke_link_exists
			(
				skh [h_sel],
				skh [0],
				&link_exists,
				NULL,
				NULL
			);


			if (1 == link_exists)
			{
				SK_DEBUG_PRINT
				(
					SK_YOKE_L_DEBUG,
					"#link_exists = %d when it should "
					"have been 0.",
					link_exists
				);

				error_detected = 1;

				break;
			}
		}


		printf
		(
			"\n\nState of #skh [] unlinking "
			"all #SK_Hoke's from 0\n\n"
		);

		SK_Hoke_print_opt (skh [0], stdout, SK_LineDeco_std (), 1, 1, 1);
		SK_Hoke_print_opt (skh [1], stdout, SK_LineDeco_std (), 1, 1, 1);
		SK_Hoke_print_opt (skh [2], stdout, SK_LineDeco_std (), 1, 1, 1);
	}


	//Deallocate all of the memory used in this test-bench

	for (size_t h_sel = 0; h_sel < NUM_HOKES; h_sel += 1)
	{
		SK_Hoke_destroy (skh [h_sel]);
	}


	//Check if the test-bench has passed or not

	if (error_detected)
	{
		printf ("SK_Hoke_test_forwarding_func_tb () : FAIL\n\n");

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("SK_Hoke_test_forwarding_func_tb () : PASS\n\n");

	return SlySr_get (SLY_SUCCESS);
}




