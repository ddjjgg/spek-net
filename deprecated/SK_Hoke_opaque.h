/*!****************************************************************************
 *
 * @file
 * SK_Hoke_opaque.h
 *
 * See SK_Hoke.c for details.
 *
 *****************************************************************************/




#ifndef SK_HOKE_OPAQUE_H
#define SK_HOKE_OPAQUE_H




#include <stdio.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "../SK_misc.h"
#include "../SK_numeric_types.h"




typedef struct SK_Hoke SK_Hoke;




SK_Hoke * SK_Hoke_create
(
	size_t			links_len,
	SlySr *			sr_ptr
);




SK_Hoke * SK_Hoke_create_opt
(
	size_t			links_len,
	void *			data_ptr,
	size_t			data_opt,
	SlySr *			sr_ptr
);




SlyDr SK_Hoke_destroy (SK_Hoke * skh);




SlySr SK_Hoke_print_opt
(
	const SK_Hoke *		skh,
	FILE *			f_ptr,
	SK_LineDeco		deco,
	int			header_flag,
	int			active_links_flag,
	int			inactive_links_flag
);




SlySr SK_Hoke_print
(
	const SK_Hoke *		skh,
	FILE *			f_ptr,
	SK_LineDeco		deco
);




SlySr SK_Hoke_create_opt_tb ();




SlySr SK_Hoke_test_forwarding_func_tb ();




#endif //SK_HOKE_OPAQUE_H

