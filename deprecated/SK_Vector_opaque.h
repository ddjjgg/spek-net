/*!****************************************************************************
 *
 * @file
 * SK_Vector_opaque.h
 *
 * See SK_Vector.c for details.
 *
 *****************************************************************************/




#ifndef SK_VECTOR_OPAQUE_H
#define SK_VECTOR_OPAQUE_H




#include "SK_debug.h"
#include "SK_misc.h"




typedef struct SK_Vector SK_Vector;




SK_Vector * SK_Vector_create
(
	size_t		elem_size,
	size_t		num_elems,
	size_t		elems_per_block,
	size_t		neg_slack,
	size_t		pos_slack,
	SlyDr		(* init_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		init_func_arg,
	SlySr *		sr_ptr
);




SlyDr SK_Vector_destroy
(
	SK_Vector * vec
);




SlyDr SK_Vector_get_num_elems
(
	SK_Vector *	vec,
	size_t *	num_elems
);




SlyDr SK_Vector_set_elem
(
	SK_Vector *	vec,
	size_t		index,
	u8 *		value_ptr,
	size_t		value_size
);




SlyDr SK_Vector_get_elem
(
	SK_Vector *	vec,
	size_t		index,
	u8 *		value_ptr,
	size_t		value_size
);




SlySr SK_Vector_append
(
	SK_Vector *	vec,
	u8 *		elem_ptr,
	size_t		elem_size
);




SlySr SK_Vector_append_cust
(
	SK_Vector *	vec,
	SlyDr		(* set_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		set_func_arg
);




SlyDr SK_Vector_calc_elem_addr
(
	SK_Vector *	vec,
	size_t		index,
	u8 **		addr
);




SlyDr SK_Vector_init_range
(
	SK_Vector *	vec,
	size_t		beg_ind,
	size_t		end_ind,
	SlyDr		(* init_func) (u8 * elem_ptr, size_t index, void * arg),
	void *		init_func_arg
);




SlyDr SK_Vector_init_zero
(
	u8 *		elem_ptr,
	size_t		index,
	void *		size_t_elem_size
);




SlySr SK_Vector_create_tb ();




SlySr SK_Vector_realloc_b_tb ();




SlySr SK_Vector_min_resize_tb ();




SlySr SK_Vector_resize_tb ();




SlySr SK_Vector_set_elem_tb ();




SlySr SK_Vector_rw_cust_tb ();




SlySr SK_Vector_resize_speed_test_tb ();




#endif //SK_VECTOR_OPAQUE_H

