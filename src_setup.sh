#!/bin/bash




#Check for correct number of arguments

if [ "$#" -ne 1 ]
then

	#Print help information

	printf "This script sets up a default *.c and *.h file pair "

	printf "for a new data structure in this project.\n\n"

	printf "Usage : $0 SK_DataStructureName\n\n"

	exit
fi




#Produce file-names and the upper-case include-guard strings

CAP_NAME="$(printf $1 | tr a-z A-Z)"


FILE_C="${1}.c"


FILE_H="${1}.h"

GUARD_FILE_H="${CAP_NAME}_H"




#Make it so the redirection operations that are about to occur do not destroy
#existing files

set -o noclobber




#Create a default *.c source file for the data structure

cat > "$FILE_C" <<EOL
/*!****************************************************************************
 *
 * @file
 * $FILE_C
 *
 * (Add comment here)
 *
 *****************************************************************************/




#include "$FILE_H"




//#include system headers here




//#include internal project headers here




//Add implementation here

EOL




#Create a default *_L0.h header for the data structure

cat > "$FILE_H" <<EOL
/*!****************************************************************************
 *
 * @file
 * $FILE_H
 *
 * See $FILE_C for details.
 *
 *****************************************************************************/




#ifndef $GUARD_FILE_H
#define $GUARD_FILE_H




//#include system headers that are depended on here




//#include project headers that are depended on here




//Add declarations here




#endif //$GUARD_FILE_H

EOL

