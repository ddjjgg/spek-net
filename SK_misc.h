/*!****************************************************************************
 *
 * @file
 * SK_misc.h
 *
 * Functions, type definitions, and constants that do not fit in well anywhere
 * else in spek-net.
 *
 * This file was based on ST_misc.h in sly-time.
 *
 *****************************************************************************/




#ifndef SK_MISC_H
#define SK_MISC_H




#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




#include "SK_debug.h"




#include "SK_numeric_types.h"




/*!
 *  Macro for an expression that returns the smallest of two numbers
 *
 *  @warning
 *  Note how statement-expressions and local variables are used in this macro
 *  to save the value of #a and #b. This is needed in case #a and #b are
 *  expressions that have side-effects, in which case, we only want those
 *  side-effects to occur once.
 *
 *  @warning
 *  Note the identifiers that start with "LOCAL___". This is used to make name
 *  collisions extremely unlikely after the macro is expanded. Keep in mind
 *  that #a and #b can be expressions that reference several identifers, and we
 *  don't want #LOCAL___a or #LOCAL___b to collide with any of them.
 *
 *  @param a	The first element to compare
 *
 *  @param b	The second element to compare
 *
 *  @return	The value of #a or #b, depending on which is smaller
 */

#define SK_MIN(a, b)							\
									\
	({								\
		typeof (a) LOCAL___a = (a);				\
									\
		typeof (b) LOCAL___b = (b);				\
									\
		((LOCAL___a < LOCAL___b) ? (LOCAL___a) : (LOCAL___b));	\
	})




/*!
 *  Macro for an expression that returns the largest of two numbers
 *
 *  @warning
 *  Note how statement-expressions and local variables are used in this macro
 *  to save the value of #a and #b. This is needed in case #a and #b are
 *  expressions that have side-effects, in which case, we only want those
 *  side-effects to occur once.
 *
 *  @warning
 *  Note the identifiers that start with "LOCAL___". This is used to make name
 *  collisions extremely unlikely after the macro is expanded. Keep in mind
 *  that #a and #b can be expressions that reference several identifers, and we
 *  don't want #LOCAL___a or #LOCAL___b to collide with any of them.
 *
 *  @param a	The first element to compare
 *
 *  @param b	The second element to compare
 *
 *  @return	The value of #a or #b, depending on which is larger
 */

#define SK_MAX(a, b)							\
									\
	({								\
		typeof (a) LOCAL___a = (a);				\
									\
		typeof (b) LOCAL___b = (b);				\
									\
		((LOCAL___a > LOCAL___b) ? (LOCAL___a) : (LOCAL___b));	\
	})




/*!
 *  Every #p_depth value (see SK_p_depth_next ()) lesser than or equal to this
 *  value will be interpreted as a request for infinite printing depth.
 */

#define SK_P_DEPTH_INF	((i64) -1)




/*!
 *  A type that represents whether or not something is ready
 */

typedef enum SK_READY_FLAG
{
	SK_NOT_READY = 0,

	SK_READY

} SK_READY_FLAG;




/*!
 *  A type that represents a basic true-or-false value
 *
 *  FIXME : Just use stdbool.h
 */

typedef enum SK_BOOL
{
	SK_FALSE = 0,

	SK_TRUE  = 1

} SK_BOOL;




/*!
 *  Determines the length the string in the #SK_ErrnoStr struct
 */

#define SK_ERRNO_STR_CHAR_LEN	(64)




/*!
 *  A type that holds a string representing the current value of #errno (as
 *  returned by the strerror_*() functions)
 */

typedef struct SK_ErrnoStr
{
	/// The actual string that represents value of #errno

	char str [SK_ERRNO_STR_CHAR_LEN];

} SK_ErrnoStr;




/*!
 *  Used to represent strings to print before / after a different line in order
 *  to decorate it. In other words, this helps with pretty printing.
 */

typedef struct SK_LineDeco
{
	/// The string to print before the contents of a text line

	const char * pre_str;


	/// The string to print after the contents of text line

	const char * post_str;


	/// The number of times to repeat #pre_str

	size_t pre_str_cnt;


	/// The number of times to repeat #post_str

	size_t post_str_cnt;

} SK_LineDeco;




//*****************************************************************************
//
//	See the Doxygen Documentation for information on items below
//
//*****************************************************************************




SK_ErrnoStr SK_ErrnoStr_get (int errnum, SlyDr * dr);




static inline u64 SK_gold_hash_u64 (u64 key);




static inline u64 SK_ceil_div_u64 (u64 a, u64 b);




i64 SK_range_oflow_i64
(
	i64	val,
	i64	beg,
	i64	end
);




SlySr SK_fprintf_cnt
(
	FILE *		f_ptr,
	u32		cnt,
	const char *	f_str,
	...
);




SK_LineDeco SK_LineDeco_std ();




SlySr SK_LineDeco_print_opt
(
	FILE *		f_ptr,
	SK_LineDeco	deco,
	const char *	f_str,
	...
);




SlySr SK_byte_print
(
	FILE *		file,
	const void *	src,
	size_t		size
);




i64 SK_p_depth_next
(
	i64	p_depth
);




static inline SlyDr SK_inc_chain
(
	int64_t *	cur_val,
	int		inc_now,
	int64_t		beg_val,
	int64_t		end_val,
	int64_t		inc_val,
	int *		looped
);




static inline SlyDr SK_byte_eq
(
	const void *	bytes_0,
	size_t		len_0,

	const void *	bytes_1,
	size_t		len_1,

	int *		eq,
	size_t *	ineq_ind
);




static inline SlyDr SK_byte_rep_eq_u8
(
	const void *	bytes,
	size_t		len,

	u8		val,

	int *		eq,
	size_t *	ineq_ind
);




static inline SlyDr SK_byte_copy
(
	void *		dst,

	const void *	src,

	size_t		size
);




static inline SlyDr SK_byte_copy_b8
(
	void *		dst,

	const void *	src
);




static inline SlyDr SK_byte_copy_b7
(
	void *		dst,

	const void *	src
);




static inline SlyDr SK_byte_copy_b6
(
	void *		dst,

	const void *	src
);




static inline SlyDr SK_byte_copy_b5
(
	void *		dst,

	const void *	src
);




static inline SlyDr SK_byte_copy_b4
(
	void *		dst,

	const void *	src
);




static inline SlyDr SK_byte_copy_b3
(
	void *		dst,

	const void *	src
);




static inline SlyDr SK_byte_copy_b2
(
	void *		dst,

	const void *	src
);




static inline SlyDr SK_byte_copy_b1
(
	void *		dst,

	const void *	src
);




static inline SlyDr SK_byte_swap
(
	void *		ptr_0,

	void *		ptr_1,

	size_t		size
);




static inline SlyDr SK_byte_swap_b8
(
	void *		ptr_0,

	void *		ptr_1
);




static inline SlyDr SK_byte_swap_b7
(
	void *		ptr_0,

	void *		ptr_1
);




static inline SlyDr SK_byte_swap_b6
(
	void *		ptr_0,

	void *		ptr_1
);




static inline SlyDr SK_byte_swap_b5
(
	void *		ptr_0,

	void *		ptr_1
);




static inline SlyDr SK_byte_swap_b4
(
	void *		ptr_0,

	void *		ptr_1
);




static inline SlyDr SK_byte_swap_b3
(
	void *		ptr_0,

	void *		ptr_1
);




static inline SlyDr SK_byte_swap_b2
(
	void *		ptr_0,

	void *		ptr_1
);




static inline SlyDr SK_byte_swap_b1
(
	void *		ptr_0,

	void *		ptr_1
);




static inline SlyDr SK_byte_set_all
(
	void *		dst,

	u8		val,

	size_t		size
);




static inline SlyDr SK_byte_le_cmp
(
	const void  *		bytes_0,
	const void  *		bytes_1,
	size_t			len,
	int *			res
);




static inline SlyDr SK_byte_le_cmp_b8
(
	const void *	b0,

	const void *	b1,

	int *		res
);




static inline SlyDr SK_byte_le_cmp_b7
(
	const void *	b0,

	const void *	b1,

	int *		res
);




static inline SlyDr SK_byte_le_cmp_b6
(
	const void *	b0,

	const void *	b1,

	int *		res
);




static inline SlyDr SK_byte_le_cmp_b5
(
	const void *	b0,

	const void *	b1,

	int *		res
);




static inline SlyDr SK_byte_le_cmp_b4
(
	const void *	b0,

	const void *	b1,

	int *		res
);




static inline SlyDr SK_byte_le_cmp_b3
(
	const void *	b0,

	const void *	b1,

	int *		res
);




static inline SlyDr SK_byte_le_cmp_b2
(
	const void *	b0,

	const void *	b1,

	int *		res
);




static inline SlyDr SK_byte_le_cmp_b1
(
	const void *	b0,

	const void *	b1,

	int *		res
);




SlyDr SK_inc_chain_tb ();




SlyDr SK_range_oflow_i64_tb ();




SlyDr SK_byte_eq_tb
(
	i64		p_depth,
	FILE *		file,
	SK_LineDeco	deco
);




SlyDr SK_byte_copy_tb ();




SlyDr SK_byte_swap_tb ();




SlyDr SK_byte_le_cmp_tb
(
	i64		p_depth,
	FILE *		file,
	SK_LineDeco	deco
);




/*!
 *  -------- Inline functions --------
 *
 *  Note, I'm aware that most compilers are capable of inlining functions even
 *  without the 'static inline' keywords. However, these functions are so basic
 *  that it is desrable to static inline them even when compiler optimizations
 *  are not requested, such as in debug builds.
 */




/*!
 *  This implements Donald Knuth's multiplicative hash function which makes use
 *  of the golden ratio and the max value of a given number of bits to map a
 *  value semi-randomly to a given bit range.
 *
 *  This is not a perfect hash function for all use cases, but it works "good
 *  enough" for a wide variety of cases.
 *
 *  @param key			A value which will be used to generate the hash
 *				value
 *
 *  @return			A hash value based off the input key
 */

static inline u64 SK_gold_hash_u64 (u64 key)
{
	//Some portions of this expression will be computed at compile-time

	const f128 GOLDEN_RATIO =	1.61803398874989484820458683436563811;

	const u64 MULT_VAL =		(((f128) UINT64_MAX) / GOLDEN_RATIO);

	return (key * MULT_VAL);
}




/*!
 *  Calculates ceil (a / b), which is a ceiling-division of the #u64 values #a
 *  and #b.
 *
 *  @param a			The dividend
 *
 *  @param b			The divisor
 *
 *  @return			The ceiling-quotient, which will be the integer
 *				ceiling of the real quotient
 */

static inline u64 SK_ceil_div_u64 (u64 a, u64 b)
{
	return (a / b) + ((a % b) != 0);
}




/*!
 *  Helps calculates values for variables that are supposed to change in
 *  lock-step as if they are the loop-variables for nested for-loops.
 *
 *  This is useful for simulating a large number of nested for-loops in which
 *  logic only occurs in the inner-most level.
 *
 *  This is very useful in test-benches, which frequently have to iterate
 *  through large sets of arbitrary combinations of values.
 *
 *
 *  <b> EXAMPLE </b>
 *
 *  @code
 *
 *	for (int64_t loop_num = 0; loop_num < 100; loop_num += 1)
 *	{
 *  		int64_t param_0 = 0;	//Should loop from 0 to 4
 *
 *		int64_t param_1 = 5;	//Should loop from 5 to 15,
 *			   		//ncrement by +1 when #param_0 loops
 *
 *		int64_t param_2 = 0;	//Should loop from 0 to 9,
 *					//increment by +3 when #param_1 loops
 *
 *		int looped = 0;		//Indicates if a value will restart at
 *					//its beginning in the next loop
 *					//iteration
 *
 *
 *		SK_inc_chain (&param_0, 1,      0, 4,  1, &looped);
 *
 *		SK_inc_chain (&param_1, looped, 5, 15, 1, &looped);
 *
 *		SK_inc_chain (&param_2, looped, 0, 9,  3, &looped);
 *
 *
 *		//Code that uses #param_0, #param_1, and #param_2 as if they
 *		//updated by separate nested for-loops goes here
 *
 *	}
 *
 *  @endcode
 *
 *
 *  @param cur_val		Pointer to the value that should be updated
 *  				as if it were a loop variable for a for-loop
 *
 *  @param inc_now		A flag indicating whether or not to increment
 *  				the loop variable now. Usually, this will
 *  				either be 1 (when this is the "first" call to
 *				SK_inc_chain () in a loop, as shown above), or
 *				the value of #looped set by previous call.
 *
 *  @param beg_val		What the value of #cur_val should be set to
 *				when the value starts incrementing for a new
 *				pseudo-loop
 *
 *  @param end_val		The end value that #cur_val should be before
 *				the pseudo-loop restarts back at #beg_val.
 *
 *  				If this is greater than #beg_val, then #cur_val
 *  				will be reset if it is greater than #end_val.
 *
 *  				If this is lesser than #beg_val, then #cur_val
 *  				will be reset if it is lesser than #end_val.
 *
 *  @param inc_val		The amount #cur_val should increment by each
 *				time the pseudo-loop iterates
 *
 *  @param looped		Where to place a flag indicating the
 *				pseudo-loop for this variable has restarted at
 *				#beg_val. This is useful to feed-back into
 *				another call to SK_inc_chain () as #inc_now.
 *
 *				This CAN be NULL if this value is not needed.
 *
 *  @return			Standard status code
 *
 */

static inline SlyDr SK_inc_chain
(
	int64_t *	cur_val,
	int		inc_now,
	int64_t		beg_val,
	int64_t		end_val,
	int64_t		inc_val,
	int     *	looped
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, cur_val, SlyDr_get (SLY_BAD_ARG));


	//If commanded to do so, add the increment to #cur_val

	if (inc_now)
	{
		*cur_val += inc_val;
	}


	//Check if the pseudo-loop should restart, due to #cur_val going beyond
	//#end_val.
	//
	//Note that "going beyond" is defined by the direction of the range
	//[beg_val, end_val]

	if
	(
		(inc_now)

		&&

		(
			((beg_val <= end_val) && (*cur_val > end_val))

			||

			((beg_val > end_val)  && (*cur_val < end_val))
		)
	)
	{

		*cur_val = beg_val;


		if (NULL != looped)
		{
			*looped = 1;
		}
	}

	else
	{
		if (NULL != looped)
		{
			*looped = 0;
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Checks if 2 contiguous memory regions contain identical bytes.
 *
 *  Note, the reason this function has both #len_0 and #len_1 as argument is
 *  because it is often very convenient to have this function compare the
 *  lengths rather than the caller.
 *
 *  @warning
 *  If #len_0 and #len_1 are not equal, this function will set #eq to 0.
 *
 *  @param bytes_0		Pointer to a contiguous memory region
 *
 *  @param len_0		The length in bytes of #bytes_0
 *
 *  @param bytes_1		Pointer to another contiguous memory region
 *  				(This CAN be equal to #bytes_0, though that
 *  				would generally be useless).
 *
 *  @param len_1		The length in bytes of #bytes_1
 *
 *  @param eq			Where to place a flag indicating if the 2
 *				memory regions were equal or not.
 *
 *				If this is set to 0, they were not equal.
 *
 *				If this is set to 1, they were equal
 *
 *				This CAN be NULL if this value is not desired.
 *
 *  @param ineq_ind		Where to place an index showing the 1st byte in
 *				which #bytes_0 and #bytes_1 differed.
 *
 *				If #bytes_0 and #bytes_1 were equal, this will
 *				be set to 0.
 *
 *				If #len_0 does not equal #len_1, then this will
 *				be set to the minimum of these 2 values.
 *
 *				This CAN be NULL if this value is not desired.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_eq
(
	const void *	bytes_0,
	size_t		len_0,

	const void *	bytes_1,
	size_t		len_1,

	int *		eq,
	size_t *	ineq_ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, bytes_0, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_MISC_L_DEBUG, bytes_1, SlyDr_get (SLY_BAD_ARG));


	//Check if the length arguments are equal or not

	if (len_0 != len_1)
	{
		if (NULL != eq)
		{
			*eq = 0;
		}

		if (NULL != ineq_ind)
		{
			*ineq_ind = SK_MIN (len_0, len_1);
		}


		return SlyDr_get (SLY_SUCCESS);
	}


	//Check the bytes for equality using larger word-sizes first, and then
	//move down to smaller word-sizes

	//TODO : This can probably be optimized further

	size_t b_sel = 0;

	size_t u64_b_max = (len_0 / sizeof (u64)) * sizeof (u64);
	size_t u32_b_max = (len_0 / sizeof (u32)) * sizeof (u32);
	size_t u16_b_max = (len_0 / sizeof (u16)) * sizeof (u16);


	while (b_sel < u64_b_max)
	{
		if ((* (u64 *) (bytes_0 + b_sel)) != (* (u64 *) (bytes_1 + b_sel)))
		{
			break;
		}

		b_sel += sizeof (u64);
	}

	while (b_sel < u32_b_max)
	{
		if ((* (u32 *) (bytes_0 + b_sel)) != (* (u32 *) (bytes_1 + b_sel)))
		{
			break;
		}

		b_sel += sizeof (u32);
	}

	while (b_sel < u16_b_max)
	{
		if ((* (u16 *) (bytes_0 + b_sel)) != (* (u16 *) (bytes_1 + b_sel)))
		{
			break;
		}

		b_sel += sizeof (u16);
	}

	while (b_sel < len_0)
	{
		if ((* (u8 *) (bytes_0 + b_sel)) != (* (u8 *) (bytes_1 + b_sel)))
		{
			if (NULL != eq)
			{
				*eq = 0;
			}

			if (NULL != ineq_ind)
			{
				*ineq_ind = b_sel;
			}

			return SlyDr_get (SLY_SUCCESS);
		}

		b_sel += sizeof (u8);
	}


	//Since none of the bytes differed, report that the 2 memory regions
	//were equal and return success

	if (NULL != eq)
	{
		*eq = 1;
	}

	if (NULL != ineq_ind)
	{
		*ineq_ind = 0;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Checks if a contiguous memory region's bytes are equivalent to repeating a
 *  given #u8 value.
 *
 *  In essence, this checks if all #u8's at #bytes are equal to #val.
 *
 *  @param bytes		Pointer to a contiguous memory region
 *
 *  @param len			The lengthin bytes of #bytes
 *
 *  @param val			The #u8 value to repeatedly compare #bytes to
 *
 *  @param eq			Where to place a flag indicating whether or not
 *				#bytes was equal to repeatedly concatenating
 *				#val.
 *
 *				If this is set to 0, they were not equal.
 *
 *				If this is set to 1, they were equal
 *
 *				This CAN be NULL if this value is not desired.
 *
 *  @param ineq_ind		Where to place an index showing the 1st byte in
 *				which #bytes differed from repeatedly
 *				concatenating #val.
 *
 *				If #eq is 1, this will be set to 0.
 *
 *				This CAN be NULL if this value is not desired.
 */

//TODO : This whole function can probably be better optimized.

static inline SlyDr SK_byte_rep_eq_u8
(
	const void *	bytes,
	size_t		len,

	u8		val,

	int *		eq,
	size_t *	ineq_ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, bytes, SlyDr_get (SLY_BAD_ARG));


	//Create a #u64 value that is simply #val repeated

	//FIXME : Is this one of those cases where it is more efficient to use
	//a union?

	u16 val_u16 = 0;

	val_u16 |= ((u16) val);
	val_u16 |= ((u16) val) << 8;


	u32 val_u32 = 0;

	val_u32 |= ((u32) val_u16);
	val_u32 |= ((u32) val_u16) << 16;


	u64 val_u64 = 0;

	val_u64 |= ((u64) val_u32);
	val_u64 |= ((u64) val_u32) << 32;


	//Check the bytes for equality to #val using larger word-sizes first,
	//and then move down to smaller word-sizes

	size_t b_sel = 0;

	size_t u64_b_max = (len / sizeof (u64)) * sizeof (u64);
	size_t u32_b_max = (len / sizeof (u32)) * sizeof (u32);
	size_t u16_b_max = (len / sizeof (u16)) * sizeof (u16);


	while (b_sel < u64_b_max)
	{
		if ((* (u64 *) (bytes + b_sel)) != val_u64)
		{
			break;
		}

		b_sel += sizeof (u64);
	}

	while (b_sel < u32_b_max)
	{
		if ((* (u32 *) (bytes + b_sel)) != val_u32)
		{
			break;
		}

		b_sel += sizeof (u32);

	}

	while (b_sel < u16_b_max)
	{
		if ((* (u16 *) (bytes + b_sel)) != val_u16)
		{
			break;
		}

		b_sel += sizeof (u16);

	}

	while (b_sel < len)
	{
		if ((* (u8 *) (bytes + b_sel)) != val)
		{
			if (NULL != eq)
			{
				*eq = 0;
			}

			if (NULL != ineq_ind)
			{
				*ineq_ind = b_sel;
			}

			return SlyDr_get (SLY_SUCCESS);
		}

		b_sel += sizeof (u8);
	}


	//Since none of the bytes differed, report that #bytes was equal to
	//repeatedly concatenating #val

	if (NULL != eq)
	{
		*eq = 1;
	}

	if (NULL != ineq_ind)
	{
		*ineq_ind = 0;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Copies a set of bytes from one memory region to another.
 *
 *  @warning
 *  Unlike SK_byte_copy (), overlapping memory regions are allowed with
 *  SK_byte_move ().
 *
 *  If the memory regions pointed to by #dst and #src overlap, the copying
 *  takes place as if the bytes at #src are copied into a temporary array (that
 *  does not overlap #src or #dst) that is then copied to #dst.
 *
 *  @param dst			Pointer to the beginning of the memory region
 *  				to write the bytes to.
 *
 *  				This can NOT be NULL.
 *
 *  @param src			Pointer to the beginning of the memory region
 *				to read the bytes from.
 *
 *				This can NOT be NULL.
 *
 *  @param size			The number of bytes to read from #src and write
 *				to #dst
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_move
(
	void *			dst,

	const void *		src,

	size_t			size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, dst, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_MISC_L_DEBUG, src, SlyDr_get (SLY_BAD_ARG));


	//Actually perform the byte-by-byte copying

	memmove (dst, src, size);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Copies a set of bytes from one memory region to another.
 *
 *  @warning
 *  The memory regions must NOT overlap.
 *
 *  @param dst			Pointer to the beginning of the memory region
 *  				to write the bytes to.
 *
 *  				This can NOT be NULL.
 *
 *  @param src			Pointer to the beginning of the memory region
 *				to read the bytes from.
 *
 *				This memory region must NOT overlap with the
 *				memory region pointed to by #dst.
 *
 *				This can NOT be NULL.
 *
 *  @param size			The number of bytes to read from #src and write
 *				to #dst
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_copy
(
	void *			dst,

	const void *		src,

	size_t			size
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, dst, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_MISC_L_DEBUG, src, SlyDr_get (SLY_BAD_ARG));


	if (SK_MISC_L_DEBUG)
	{
		//Check if the #dst and #src memory regions are overlapping

		if
		(
			((dst <= src) && ((((u8 *) dst) + size) > ((u8 *) src)))

			||

			((src <= dst) && ((((u8 *) src) + size) > ((u8 *) dst)))
		)
		{
			SK_DEBUG_PRINT
			(
				SK_MISC_L_DEBUG,
				"BUG : With the arguments...\n"
				"\n"
				" #dst =  %p\n"
				" #src =  %p\n"
				" #size = %zu\n"
				"\n"
				"...the memory regions overlap, which "
				"is UNSAFE.",
				dst,
				src,
				size
			);


			return SlyDr_get (SLY_BAD_ARG);
		}

	}


	//Actually perform the byte-by-byte copying

	memcpy (dst, src, size);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Copies 8 bytes from 1 memory region to another.
 *
 *  @warning
 *  This function performs no sanity check on #dst or #src
 *
 *  @param dst			The memory region to copy bytes to.
 *
 *  				This must NOT be NULL.
 *
 *  @param src			The memory region to copy bytes from.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

//FIXME : Considering these are now 'static inline', is it a good idea to add
//sanity checks to these functions? After all, these checks can be optimized if
//such a check occurs before the function call now.

static inline SlyDr SK_byte_copy_b8
(
	void *		dst,

	const void *	src
)
{
	(* (u64 *) dst) = (* (u64 *) src);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Copies 7 bytes from 1 memory region to another.
 *
 *  @warning
 *  This function performs no sanity check on #dst or #src
 *
 *  @param dst			The memory region to copy bytes to.
 *
 *  				This must NOT be NULL.
 *
 *  @param src			The memory region to copy bytes from.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_copy_b7
(
	void *		dst,

	const void *	src
)
{
	(* (u32 *) dst) = (* (u32 *) src);

	(* (u16 *) (dst + 4)) = (* (u16 *) (src + 4));

	(* (u8 *)  (dst + 6)) = (* (u8 *)  (src + 6));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Copies 6 bytes from 1 memory region to another.
 *
 *  @warning
 *  This function performs no sanity check on #dst or #src
 *
 *  @param dst			The memory region to copy bytes to.
 *
 *  				This must NOT be NULL.
 *
 *  @param src			The memory region to copy bytes from.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_copy_b6
(
	void *		dst,

	const void *	src
)
{
	(* (u32 *) dst) = (* (u32 *) src);

	(* (u16 *) (dst + 4)) = (* (u16 *) (src + 4));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Copies 5 bytes from 1 memory region to another.
 *
 *  @warning
 *  This function performs no sanity check on #dst or #src
 *
 *  @param dst			The memory region to copy bytes to.
 *
 *  				This must NOT be NULL.
 *
 *  @param src			The memory region to copy bytes from.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_copy_b5
(
	void *		dst,

	const void *	src
)
{
	(* (u32 *) dst) = (* (u32 *) src);

	(* (u8 *)  (dst + 4)) = (* (u8 *)  (src + 4));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Copies 4 bytes from 1 memory region to another.
 *
 *  @warning
 *  This function performs no sanity check on #dst or #src
 *
 *  @param dst			The memory region to copy bytes to.
 *
 *  				This must NOT be NULL.
 *
 *  @param src			The memory region to copy bytes from.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_copy_b4
(
	void *		dst,

	const void *	src
)
{
	(* (u32 *) dst) = (* (u32 *) src);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Copies 3 bytes from 1 memory region to another.
 *
 *  @warning
 *  This function performs no sanity check on #dst or #src
 *
 *  @param dst			The memory region to copy bytes to.
 *
 *  				This must NOT be NULL.
 *
 *  @param src			The memory region to copy bytes from.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_copy_b3
(
	void *		dst,

	const void *	src
)
{
	(* (u16 *) (dst)) = (* (u16 *) (src));

	(* (u8 *)  (dst + 2)) = (* (u8 *)  (src + 2));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Copies 2 bytes from 1 memory region to another.
 *
 *  @warning
 *  This function performs no sanity check on #dst or #src
 *
 *  @param dst			The memory region to copy bytes to.
 *
 *  				This must NOT be NULL.
 *
 *  @param src			The memory region to copy bytes from.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_copy_b2
(
	void *		dst,

	const void *	src
)
{
	(* (u16 *) dst) = (* (u16 *) src);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Copies 1 byte from 1 memory region to another.
 *
 *  @warning
 *  This function performs no sanity check on #dst or #src
 *
 *  @param dst			The memory region to copy bytes to.
 *
 *  				This must NOT be NULL.
 *
 *  @param src			The memory region to copy bytes from.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_copy_b1
(
	void *		dst,

	const void *	src
)
{
	(* (u8 *) dst) = (* (u8 *) src);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Swaps a set of bytes from one memory region with another.
 *
 *  @warning
 *  The memory regions must NOT overlap.
 *
 *  @param ptr_0		Pointer to the beginning of one of the memory
 *				regions to swap bytes with.
 *
 *  				This can NOT be NULL.
 *
 *  @param ptr_1		Pointer to the beginning of one of the memory
 *				regions to swap bytes with.
 *
 *				This memory region must NOT overlap with the
 *				memory region pointed to by #ptr_0.
 *
 *				This can NOT be NULL.
 *
 *  @param size			The number of bytes located at #ptr_0 and
 *				#ptr_1
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_swap
(
	void *		ptr_0,

	void *		ptr_1,

	size_t		size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, ptr_0, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_MISC_L_DEBUG, ptr_1, SlyDr_get (SLY_BAD_ARG));


	if (SK_MISC_L_DEBUG)
	{
		//Check if the #ptr_0 and #ptr_1 memory regions are overlapping

		if
		(
			((ptr_0 <= ptr_1) && ((((u8 *) ptr_0) + size) > ((u8 *) ptr_1)))

			||

			((ptr_1 <= ptr_0) && ((((u8 *) ptr_1) + size) > ((u8 *) ptr_0)))
		)
		{
			SK_DEBUG_PRINT
			(
				SK_MISC_L_DEBUG,
				"BUG : With the arguments...\n"
				"\n"
				" #ptr_0 = %p\n"
				" #ptr_1 = %p\n"
				" #size =  %zu\n"
				"\n"
				"...the memory regions overlap, which "
				"is UNSAFE.",
				ptr_0,
				ptr_1,
				size
			);


			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Swap the bytes starting in #u64 groupings and progressively use
	//smaller byte groupings

	size_t num_bytes = 0;


	while (sizeof (u64) <= (size - num_bytes))
	{
		u64 temp = (* (u64 *) (ptr_0 + num_bytes));

		(* (u64 *) (ptr_0 + num_bytes)) = (* (u64 *) (ptr_1 + num_bytes));

		(* (u64 *) (ptr_1 + num_bytes)) = temp;


		num_bytes += sizeof (u64);
	}


	if (sizeof (u32) <= (size - num_bytes))
	{
		u32 temp = (* (u32 *) (ptr_0 + num_bytes));

		(* (u32 *) (ptr_0 + num_bytes)) = (* (u32 *) (ptr_1 + num_bytes));

		(* (u32 *) (ptr_1 + num_bytes)) = temp;


		num_bytes += sizeof (u32);
	}


	if (sizeof (u16) <= (size - num_bytes))
	{
		u16 temp = (* (u16 *) (ptr_0 + num_bytes));

		(* (u16 *) (ptr_0 + num_bytes)) = (* (u16 *) (ptr_1 + num_bytes));

		(* (u16 *) (ptr_1 + num_bytes)) = temp;


		num_bytes += sizeof (u16);
	}


	if (sizeof (u8) <= (size - num_bytes))
	{
		u8 temp = (* (u8 *) (ptr_0 + num_bytes));

		(* (u8 *) (ptr_0 + num_bytes)) = (* (u8 *) (ptr_1 + num_bytes));

		(* (u8 *) (ptr_1 + num_bytes)) = temp;


		num_bytes += sizeof (u8);
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Swaps 8 bytes from 1 memory region with another.
 *
 *  @warning
 *  This function performs no sanity checks on #ptr_0 or #ptr_1
 *
 *  @param ptr_0		Pointer to a memory region to swap bytes with.
 *
 *				This must NOT be NULL.
 *
 *  @param ptr_1		Pointer to another memory region to swap bytes
 *  				with.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_swap_b8
(
	void *		ptr_0,

	void *		ptr_1
)
{
	u64 temp = 0;

	SK_byte_copy_b8 (&temp, ptr_0);
	SK_byte_copy_b8 (ptr_0, ptr_1);
	SK_byte_copy_b8 (ptr_1, &temp);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Swaps 7 bytes from 1 memory region with another.
 *
 *  @warning
 *  This function performs no sanity checks on #ptr_0 or #ptr_1
 *
 *  @param ptr_0		Pointer to a memory region to swap bytes with.
 *
 *				This must NOT be NULL.
 *
 *  @param ptr_1		Pointer to another memory region to swap bytes
 *  				with.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_swap_b7
(
	void *		ptr_0,

	void *		ptr_1
)
{
	u64 temp = 0;

	SK_byte_copy_b7 (&temp, ptr_0);
	SK_byte_copy_b7 (ptr_0, ptr_1);
	SK_byte_copy_b7 (ptr_1, &temp);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Swaps 6 bytes from 1 memory region with another.
 *
 *  @warning
 *  This function performs no sanity checks on #ptr_0 or #ptr_1
 *
 *  @param ptr_0		Pointer to a memory region to swap bytes with.
 *
 *				This must NOT be NULL.
 *
 *  @param ptr_1		Pointer to another memory region to swap bytes
 *  				with.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_swap_b6
(
	void *		ptr_0,

	void *		ptr_1
)
{
	u64 temp = 0;

	SK_byte_copy_b6 (&temp, ptr_0);
	SK_byte_copy_b6 (ptr_0, ptr_1);
	SK_byte_copy_b6 (ptr_1, &temp);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Swaps 5 bytes from 1 memory region with another.
 *
 *  @warning
 *  This function performs no sanity checks on #ptr_0 or #ptr_1
 *
 *  @param ptr_0		Pointer to a memory region to swap bytes with.
 *
 *				This must NOT be NULL.
 *
 *  @param ptr_1		Pointer to another memory region to swap bytes
 *  				with.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_swap_b5
(
	void *		ptr_0,

	void *		ptr_1
)
{
	u64 temp = 0;

	SK_byte_copy_b5 (&temp, ptr_0);
	SK_byte_copy_b5 (ptr_0, ptr_1);
	SK_byte_copy_b5 (ptr_1, &temp);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Swaps 4 bytes from 1 memory region with another.
 *
 *  @warning
 *  This function performs no sanity checks on #ptr_0 or #ptr_1
 *
 *  @param ptr_0		Pointer to a memory region to swap bytes with.
 *
 *				This must NOT be NULL.
 *
 *  @param ptr_1		Pointer to another memory region to swap bytes
 *  				with.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_swap_b4
(
	void *		ptr_0,

	void *		ptr_1
)
{
	u32 temp = 0;

	SK_byte_copy_b4 (&temp, ptr_0);
	SK_byte_copy_b4 (ptr_0, ptr_1);
	SK_byte_copy_b4 (ptr_1, &temp);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Swaps 3 bytes from 1 memory region with another.
 *
 *  @warning
 *  This function performs no sanity checks on #ptr_0 or #ptr_1
 *
 *  @param ptr_0		Pointer to a memory region to swap bytes with.
 *
 *				This must NOT be NULL.
 *
 *  @param ptr_1		Pointer to another memory region to swap bytes
 *  				with.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_swap_b3
(
	void *		ptr_0,

	void *		ptr_1
)
{
	u32 temp = 0;

	SK_byte_copy_b3 (&temp, ptr_0);
	SK_byte_copy_b3 (ptr_0, ptr_1);
	SK_byte_copy_b3 (ptr_1, &temp);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Swaps 2 bytes from 1 memory region with another.
 *
 *  @warning
 *  This function performs no sanity checks on #ptr_0 or #ptr_1
 *
 *  @param ptr_0		Pointer to a memory region to swap bytes with.
 *
 *				This must NOT be NULL.
 *
 *  @param ptr_1		Pointer to another memory region to swap bytes
 *  				with.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_swap_b2
(
	void *		ptr_0,

	void *		ptr_1
)
{
	u16 temp = 0;

	SK_byte_copy_b2 (&temp, ptr_0);
	SK_byte_copy_b2 (ptr_0, ptr_1);
	SK_byte_copy_b2 (ptr_1, &temp);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Swaps 1 bytes from 1 memory region with another.
 *
 *  @warning
 *  This function performs no sanity checks on #ptr_0 or #ptr_1
 *
 *  @param ptr_0		Pointer to a memory region to swap bytes with.
 *
 *				This must NOT be NULL.
 *
 *  @param ptr_1		Pointer to another memory region to swap bytes
 *  				with.
 *
 *  				This must NOT be NULL.
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_swap_b1
(
	void *		ptr_0,

	void *		ptr_1
)
{
	u64 temp = 0;

	SK_byte_copy_b1 (&temp, ptr_0);
	SK_byte_copy_b1 (ptr_0, ptr_1);
	SK_byte_copy_b1 (ptr_1, &temp);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets all the bytes in a memory region to the same value.
 *
 *  This is very useful for resetting a memory region to contain all 0's.
 *
 *  @param dst			The memory region to modify
 *
 *  @param val			The value to set all the bytes in the memory
 *				region to
 *
 *  @param size			The size of the memory region in bytes
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_set_all
(
	void *		dst,

	u8		val,

	size_t		size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, dst, SlyDr_get (SLY_BAD_ARG));


	memset (dst, val, size);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Determines if the running platform is little-endian or big-endian.
 *
 *  Little-endian systems store the least-significant byte of multi-byte values
 *  in the lowest address.
 *
 *  Big-endian systems store the least-significant byte of multi-byte values
 *  in the highest address.
 *
 *  @return			0 if the running platform is big-endian
 *
 *  				1 if the running platform is little-endian
 */

static inline int SK_plat_is_le ()
{
	if16 num = 1;

	return (1 == (* (u8 *) &num));
}




/*!
 *  Compares 2 groups of bytes in little-endian order, meaning that the
 *  lowest-addressed byte is the least-significant byte. Each individual byte
 *  is treated as if it is a #u8 value.
 *
 *  @param bytes_0		Pointer to a contiguous memory region
 *
 *  @param bytes_1		Pointer to another contiguous memory region
 *
 *  @param len			The length in bytes of #bytes_0 and #bytes_1
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (bytes_0 < bytes_1)
 *
 *  				 0 means (bytes_0 == bytes_1)
 *
 *  				+1 means (bytes_0 > bytes_1)
 *
 *  return			Standard status code
 */

static inline SlyDr SK_byte_le_cmp
(
	const void  *		bytes_0,
	const void  *		bytes_1,
	size_t			len,
	int *			res
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, bytes_0, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_MISC_L_DEBUG, bytes_1, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_MISC_L_DEBUG, res, SlyDr_get (SLY_BAD_ARG));


	if (0 == len)
	{
		SK_DEBUG_PRINT
		(
			SK_MISC_L_DEBUG,
			"BUG : len = %zu, which makes performing the "
			"comparison impossible.",
			len
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	if (bytes_0 == bytes_1)
	{
		//The pointers were the same, so there is only 1 byte region,
		//and it is of course equal to itself

		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}


	//If the running platform is big-endian, use a less efficient
	//byte-by-byte method for comparison.
	//
	//TODO : Make this comparison more efficient for big-endian systems

	if (0 == SK_plat_is_le ())
	{
		size_t b_sel = len;

		while (b_sel > sizeof (u8))
		{
			b_sel -= sizeof (u8);

			if ((* (u8 *) bytes_0) < (* (u8 *) bytes_1))
			{
				*res = -1;

				return SlyDr_get (SLY_SUCCESS);
			}

			if ((* (u8 *) bytes_0) > (* (u8 *) bytes_1))
			{
				*res = +1;

				return SlyDr_get (SLY_SUCCESS);
			}
		}

		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}


	//Compare the 2 memory regions using the largest word-size first, and
	//then use smaller words-sizes.

	size_t b_sel = len;


	while (b_sel >= sizeof (u64))
	{
		b_sel -= sizeof (u64);

		if ((* (u64 *) (bytes_0 + b_sel)) < (* (u64 *) (bytes_1 + b_sel)))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u64 *) (bytes_0 + b_sel)) > (* (u64 *) (bytes_1 + b_sel)))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}
	}


	while (b_sel >= sizeof (u32))
	{
		b_sel -= sizeof (u32);

		if ((* (u32 *) (bytes_0 + b_sel)) < (* (u32 *) (bytes_1 + b_sel)))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u32 *) (bytes_0 + b_sel)) > (* (u32 *) (bytes_1 + b_sel)))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}
	}


	while (b_sel >= sizeof (u16))
	{
		b_sel -= sizeof (u16);

		if ((* (u16 *) (bytes_0 + b_sel)) < (* (u16 *) (bytes_1 + b_sel)))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u16 *) (bytes_0 + b_sel)) > (* (u16 *) (bytes_1 + b_sel)))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}
	}


	while (b_sel >= sizeof (u8))
	{
		b_sel -= sizeof (u8);

		if ((* (u8 *) (bytes_0 + b_sel)) < (* (u8 *) (bytes_1 + b_sel)))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u8 *) (bytes_0 + b_sel)) > (* (u8 *) (bytes_1 + b_sel)))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}
	}


	*res = 0;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  TODO : The SK_byte_le_cmp_b* () functions can probably be optimized
 *  further, however they are sufficient for use in #SK_HmapStatImpl as
 *  #prim_cmp ().
 */




/*!
 *  Compares 2 memory regions of 8 bytes as if they are each unsigned values
 *  stored in little-endian order.
 *
 *  @warning
 *  This function performs no sanity check on #b0, #b1, or #res
 *
 *  @param b0			The first memory region to compare
 *
 *  @param b1			The second memory region to compare
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (bytes_0 < bytes_1)
 *
 *  				 0 means (bytes_0 == bytes_1)
 *
 *  				+1 means (bytes_0 > bytes_1)
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_le_cmp_b8
(
	const void *	b0,

	const void *	b1,

	int *		res
)
{
	if (SK_plat_is_le ())
	{
		//Compare bytes [7, 0]

		if ((* (u64 *) b0) < (* (u64 *) b1))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u64 *) b0) > (* (u64 *) b1))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}

		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}

	else
	{
		return SK_byte_le_cmp (b0, b1, 8, res);
	}
}




/*!
 *  Compares 2 memory regions of 7 bytes as if they are each unsigned values
 *  stored in little-endian order.
 *
 *  @warning
 *  This function performs no sanity check on #b0, #b1, or #res
 *
 *  @param b0			The first memory region to compare
 *
 *  @param b1			The second memory region to compare
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (bytes_0 < bytes_1)
 *
 *  				 0 means (bytes_0 == bytes_1)
 *
 *  				+1 means (bytes_0 > bytes_1)
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_le_cmp_b7
(
	const void *	b0,

	const void *	b1,

	int *		res
)
{
	if (SK_plat_is_le ())
	{
		//Compare bytes [6, 3]

		if ((* (u32 *) (((u8 *) b0) + 4)) < (* (u32 *) (((u8 *) b1) + 4)))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u32 *) (((u8 *) b0) + 4)) > (* (u32 *) (((u8 *) b1) + 4)))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		//Compare bytes [2, 1]

		if ((* (u16 *) (((u8 *) b0) + 2)) < (* (u16 *) (((u8 *) b1) + 2)))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u16 *) (((u8 *) b0) + 2)) > (* (u16 *) (((u8 *) b1) + 2)))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		//Compare byte [0]

		if ((* (u8 *) b0) < (* (u8 *) b1))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u8 *) b0) > (* (u8 *) b1))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}

	else
	{
		return SK_byte_le_cmp (b0, b1, 7, res);
	}
}




/*!
 *  Compares 2 memory regions of 6 bytes as if they are each unsigned values
 *  stored in little-endian order.
 *
 *  @warning
 *  This function performs no sanity check on #b0, #b1, or #res
 *
 *  @param b0			The first memory region to compare
 *
 *  @param b1			The second memory region to compare
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (bytes_0 < bytes_1)
 *
 *  				 0 means (bytes_0 == bytes_1)
 *
 *  				+1 means (bytes_0 > bytes_1)
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_le_cmp_b6
(
	const void *	b0,

	const void *	b1,

	int *		res
)
{
	if (SK_plat_is_le ())
	{
		//Compare bytes [5, 2]

		if ((* (u32 *) (((u8 *) b0) + 2)) < (* (u32 *) (((u8 *) b1) + 2)))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u32 *) (((u8 *) b0) + 2)) > (* (u32 *) (((u8 *) b1) + 2)))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		//Compare bytes [1, 0]

		if ((* (u16 *) b0) < (* (u16 *) b1))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u16 *) b0) > (* (u16 *) b1))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}

	else
	{
		return SK_byte_le_cmp (b0, b1, 6, res);
	}
}




/*!
 *  Compares 2 memory regions of 5 bytes as if they are each unsigned values
 *  stored in little-endian order.
 *
 *  @warning
 *  This function performs no sanity check on #b0, #b1, or #res
 *
 *  @param b0			The first memory region to compare
 *
 *  @param b1			The second memory region to compare
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (bytes_0 < bytes_1)
 *
 *  				 0 means (bytes_0 == bytes_1)
 *
 *  				+1 means (bytes_0 > bytes_1)
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_le_cmp_b5
(
	const void *	b0,

	const void *	b1,

	int *		res
)
{
	if (SK_plat_is_le ())
	{
		//Compare bytes [4, 1]

		if ((* (u32 *) (((u8 *) b0) + 1)) < (* (u32 *) (((u8 *) b1) + 1)))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u32 *) (((u8 *) b0) + 1)) > (* (u32 *) (((u8 *) b1) + 1)))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		//Compare byte [0]

		if ((* (u8 *) b0) < (* (u8 *) b1))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u8 *) b0) > (* (u8 *) b1))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}

	else
	{
		return SK_byte_le_cmp (b0, b1, 5, res);
	}
}




/*!
 *  Compares 2 memory regions of 4 bytes as if they are each unsigned values
 *  stored in little-endian order.
 *
 *  @warning
 *  This function performs no sanity check on #b0, #b1, or #res
 *
 *  @param b0			The first memory region to compare
 *
 *  @param b1			The second memory region to compare
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (bytes_0 < bytes_1)
 *
 *  				 0 means (bytes_0 == bytes_1)
 *
 *  				+1 means (bytes_0 > bytes_1)
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_le_cmp_b4
(
	const void *	b0,

	const void *	b1,

	int *		res
)
{
	if (SK_plat_is_le ())
	{
		//Compare bytes [0, 3]

		if ((* (u32 *) b0) < (* (u32 *) b1))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u32 *) b0) > (* (u32 *) b1))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		b0 = (u8 *) b0 + sizeof (u32);
		b1 = (u8 *) b1 + sizeof (u32);


		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}

	else
	{
		return SK_byte_le_cmp (b0, b1, 4, res);
	}
}




/*!
 *  Compares 2 memory regions of 3 bytes as if they are each unsigned values
 *  stored in little-endian order.
 *
 *  @warning
 *  This function performs no sanity check on #b0, #b1, or #res
 *
 *  @param b0			The first memory region to compare
 *
 *  @param b1			The second memory region to compare
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (bytes_0 < bytes_1)
 *
 *  				 0 means (bytes_0 == bytes_1)
 *
 *  				+1 means (bytes_0 > bytes_1)
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_le_cmp_b3
(
	const void *	b0,

	const void *	b1,

	int *		res
)
{
	if (SK_plat_is_le ())
	{
		//Compare bytes [2, 1]

		if ((* (u16 *) (((u8 *) b0) + 2)) < (* (u16 *) (((u8 *) b1) + 2)))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u16 *) (((u8 *) b0) + 2)) > (* (u16 *) (((u8 *) b1) + 2)))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		//Compare byte [0]

		if ((* (u8 *) b0) < (* (u8 *) b1))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u8 *) b0) > (* (u8 *) b1))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}

	else
	{
		return SK_byte_le_cmp (b0, b1, 3, res);
	}
}




/*!
 *  Compares 2 memory regions of 2 bytes as if they are each unsigned values
 *  stored in little-endian order.
 *
 *  @warning
 *  This function performs no sanity check on #b0, #b1, or #res
 *
 *  @param b0			The first memory region to compare
 *
 *  @param b1			The second memory region to compare
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (bytes_0 < bytes_1)
 *
 *  				 0 means (bytes_0 == bytes_1)
 *
 *  				+1 means (bytes_0 > bytes_1)
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_le_cmp_b2
(
	const void *	b0,

	const void *	b1,

	int *		res
)
{
	if (SK_plat_is_le ())
	{
		//Compare bytes [0, 1]

		if ((* (u16 *) b0) < (* (u16 *) b1))
		{
			*res = -1;

			return SlyDr_get (SLY_SUCCESS);
		}

		if ((* (u16 *) b0) > (* (u16 *) b1))
		{
			*res = +1;

			return SlyDr_get (SLY_SUCCESS);
		}


		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}

	else
	{
		return SK_byte_le_cmp (b0, b1, 2, res);
	}
}




/*!
 *  Compares 2 memory regions of 1 byte as if they are each unsigned values
 *  stored in little-endian order.
 *
 *  @warning
 *  This function performs no sanity check on #b0, #b1, or #res
 *
 *  @param b0			The first memory region to compare
 *
 *  @param b1			The second memory region to compare
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (bytes_0 < bytes_1)
 *
 *  				 0 means (bytes_0 == bytes_1)
 *
 *  				+1 means (bytes_0 > bytes_1)
 *
 *  @return			Standard status code
 */

static inline SlyDr SK_byte_le_cmp_b1
(
	const void *	b0,

	const void *	b1,

	int *		res
)
{
	//Compare byte [0]

	if ((* (u8 *) b0) < (* (u8 *) b1))
	{
		*res = -1;

		return SlyDr_get (SLY_SUCCESS);
	}

	if ((* (u8 *) b0) > (* (u8 *) b1))
	{
		*res = +1;

		return SlyDr_get (SLY_SUCCESS);
	}


	*res = 0;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  TODO : Make functions that compare memory regions as unsigned values with
 *  no regard for whether they are considered to be in little-endian or
 *  big-endian order. While this makes their operation platform-dependent, it
 *  does potentially allow for these functions to execute quicker.
 */




/*!
 *  TODO : Make functions to convert memory regions to / from little-endian
 *  from / to big-endian.
 */




#endif //SK_MISC_H

