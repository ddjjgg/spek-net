/*!****************************************************************************
 *
 * @file
 * SK_Array.c
 *
 * See SK_Array.h for details.
 *
 *****************************************************************************/




#include "SK_Array.h"




#include <stdio.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "SK_BoxIntf.h"
#include "SK_debug.h"
#include "SK_MemStd.h"
#include "SK_misc.h"




/*!
 *  This is the number of elements per block that #SK_Array will use whenever
 *  it uses an #SK_BoxReqs ("req") where (0 == req->stat_r->hint_elems_per_block).
 *
 *  In the case that an #SK_BoxReqs meets the above condition, that is
 *  essentially a request meaning "please choose the most convenient block size
 *  for this storage data structure".
 */

#define SK_ARRAY_DEF_ELEMS_PER_BLOCK	((size_t) 1)




/*!
 *  (This is an attempt to implement the "struct-less data structure" idea as
 *  described in "ideas.txt")
 *
 *
 *  @defgroup SK_Array
 *
 *  @{
 *
 *  The memory layout of #SK_Array is as follows:
 *
 *
 * 	-Number of Bits-		-Field-
 *
 *	sizeof (size_t)			cur_num_elems
 *
 *	sizeof (size_t)			dyna_elem_len
 *
 *	sizeof (void *)			dyna_array_ptr
 *
 *	(elem_size)			stat_elem [0]
 *
 *	(elem_size)			stat_elem [1]
 *
 *	.
 *	.
 *	.
 *
 *	(elem_size)			stat_elem [num_stat_elems - 1]
 *
 *
 *
 *
 *  @var cur_num_elems		This is used to represent how many elements are
 *				currently being used in the #SK_Array. This
 *				WILL count both the dynamically and statically
 *				allocated elements.
 *
 *				This is what will be reported as the "number of
 *				elements" inside of the #SK_Array.
 *
 *				This CAN be set to values lower than
 *				#num_stat_elems (including 0), in which
 *				case the statically allocated elements will
 *				simply be unused.
 *
 *				It is very important to understand that this
 *				does NOT have to equal (num_stat_elems +
 *				dyna_elem_len). Elements can be allocated
 *				before they are actually used in #SK_Array.
 *
 *  @var dyna_elem_len		The number of dynamically allocated elements at
 *				#dyna_elem_ptr.
 *
 *  @var dyna_elem_ptr		A pointer to a dynamically-allocated contiguous
 *				memory region used to store elements.
 *
 *				Not all of the elements allocated at
 *				#dyna_elem_ptr have to be "in use", which means
 *				counted for by #cur_num_elems.
 *
 *  @var stat_elem []		An array of statically allocated elements.
 *				These are part of the static memory of the data
 *				structure, so this array can not be resized
 *				during the lifetime of the object.
 *
 *				It is possible for #num_stat_elems to be 0,
 *				in which case, this array will not exist in the
 *				data structure.
 *
 *  @var num_stat_elems		This is a parameter that is received from the
 *				#SK_BoxReqs arguments in the #SK_BoxIntf.
 *
 *				Notably, this value is NOT stored in the data
 *				structure itself, as a way of reducing the
 *				per-instance memory cost of the data structure.
 *
 *  @}
 */




/*!
 *  See #SK_BoxIntf.inst_stat_mem
 */

SlyDr SK_Array_inst_stat_mem
(
	const SK_BoxReqs *		req,

	size_t *			num_bytes
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, num_bytes, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_L_DEBUG)
	{
		if (0 >= req->stat_r->elem_size)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#req->stat_r->elem_size = %zu, making the "
				"described #SK_Array impossible.",
				req->stat_r->elem_size
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Calculate the size of the part of #SK_Array that is not dependent on
	//the parameters in #req

	size_t inst_size = 0;


	//Size of #cur_num_elems

	inst_size += sizeof (size_t);


	//Size of #dyna_elem_len

	inst_size += sizeof (size_t);


	//Size of #dyna_elem_ptr

	inst_size += sizeof (void *);


	//Calculate the size of the part of #SK_Array that IS dependent on the
	//parameters in #req


	//Size of #stat_elem []

	inst_size += (req->stat_r->elem_size * req->stat_r->hint_num_stat_elems);


	//Save the calculated instance size

	*num_bytes = inst_size;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with #cur_num_elems in an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represents general
 *				parameters of the #SK_Array
 *
 *  @param offset		Where to place the offset associated with
 *				#cur_num_elems
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_offset_cur_num_elems
(
	const SK_BoxReqs *		req,

	size_t *			offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	*offset = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with #dyna_elem_len in an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represents general
 *				parameters of the #SK_Array
 *
 *  @param offset		Where to place the offset associated with
 *				#dyna_elem_len
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_offset_dyna_elem_len
(
	const SK_BoxReqs *		req,

	size_t *			offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	*offset = sizeof (size_t);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with #dyna_elem_ptr in an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represents general
 *				parameters of the #SK_Array
 *
 *  @param offset		Where to place the offset associated with
 *				#dyna_elem_ptr
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_offset_dyna_array_ptr
(
	const SK_BoxReqs *		req,

	size_t *			offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	*offset =

	sizeof (size_t) + sizeof (size_t);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with the START of #stat_elem [] in an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represents general
 *				parameters of the #SK_Array
 *
 *  @param offset		Where to place the offset associated with
 *				#stat_elem
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_offset_stat_elem
(
	const SK_BoxReqs *		req,

	size_t *			offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	*offset =

	sizeof (size_t) + sizeof (size_t) + sizeof (void *);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the value #cur_num_elems from an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array
 *
 *  @param ska			The #SK_Array to inspect
 *
 *  @param cur_num_elems	Where to place the value of (ska->cur_num_elems)
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_get_cur_num_elems
(
	const SK_BoxReqs *		req,

	const SK_Array *		ska,

	size_t *			cur_num_elems
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, cur_num_elems, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_Array_offset_cur_num_elems (req, &offset);


	*cur_num_elems = * ((size_t *) (((u8 *) ska) + offset));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the value #dyna_elem_len from an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array
 *
 *  @param ska			The #SK_Array to inspect
 *
 *  @param dyna_elem_len	Where to place the value of (ska->dyna_elem_len)
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_get_dyna_elem_len
(
	const SK_BoxReqs *		req,

	const SK_Array *		ska,

	size_t *			dyna_elem_len
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, dyna_elem_len, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_Array_offset_dyna_elem_len (req, &offset);


	*dyna_elem_len = * ((size_t *) (((u8 *) ska) + offset));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to (dyna_array_ptr) from an #SK_Array.
 *
 *  @warning
 *  Unlike the very similar function SK_Array_get_dyna_elem_ptr (), this
 *  function does not accept an #ind argument to get the pointer for a
 *  dynamically-allocated element at a given index. It simply returns the value
 *  of #dyna_array_ptr, which may or may not be NULL. If it is not NULL, then
 *  this will be equivalent to the pointer to the 0th dynamically-allocated
 *  element.
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array
 *
 *  @param ska			The #SK_Array to inspect
 *
 *  @param dyna_array_ptr	Where to place the value of
 *  				(ska->dyna_array_ptr)
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_get_dyna_array_ptr
(
	const SK_BoxReqs *		req,

	const SK_Array *		ska,

	void **				dyna_array_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, dyna_array_ptr, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_Array_offset_dyna_array_ptr (req, &offset);


	*dyna_array_ptr =

	*((void **) ((u8 *) ska + offset));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to (dyna_array_ptr [ind]) from an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array
 *
 *  @param ska			The #SK_Array to inspect
 *
 *  @param ind			The index of the element in
 *				(ska->dyna_array_ptr []) to get the pointer
 *				for.
 *
 *				This MUST be less than (ska->dyna_elem_len)
 *
 *  @param dyna_elem_ptr	Where to place the value of
 *  				(ska->dyna_array_ptr [ind])
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_get_dyna_elem_ptr
(
	const SK_BoxReqs *		req,

	const SK_Array *		ska,

	size_t				ind,

	void **				dyna_elem_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, dyna_elem_ptr, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_H_DEBUG)
	{
		size_t dyna_elem_len = 0;

		SK_Array_get_dyna_elem_len (req, ska, &dyna_elem_len);


		if (ind >= dyna_elem_len)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_H_DEBUG,
				"BUG : In #SK_Array at #ska = %p, attempted to "
				"access dynamic element at #ind = %zu although "
				"(ska->dyna_elem_len) = %zu. Only the internal "
				"#SK_Array functions should be able directly "
				"request dynamically-allocated elements, so "
				"this is a bug somewhere in the #SK_Array "
				"functions.",
				ska,
				ind,
				dyna_elem_len
			)

			*dyna_elem_ptr = NULL;

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	size_t offset = 0;

	SK_Array_offset_dyna_array_ptr (req, &offset);


	*dyna_elem_ptr =

	((u8 *) *((void **) ((u8 *) ska + offset))) + (ind * req->stat_r->elem_size);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to (stat_elem [ind]) from an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array
 *
 *  @param ska			The #SK_Array to inspect
 *
 *  @param ind			The index of the element in (stat_elem []) to
 *				get the pointer for.
 *
 *				This MUST be less than
 *				(req->stat_r->hint_num_stat_elems).
 *
 *  @param stat_elem_ptr	Where to place the value of
 *				(& (ska->stat_elem [ind]))
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_get_stat_elem_ptr
(
	const SK_BoxReqs *		req,

	const SK_Array *		ska,

	size_t				ind,

	void **				stat_elem_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, stat_elem_ptr, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_H_DEBUG)
	{
		if (ind >= req->stat_r->hint_num_stat_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_H_DEBUG,
				"In #SK_Array at #ska = %p, attempted to "
				"access static element at (ind = %zu) although "
				"(req->stat_r->hint_num_stat_elems = %zu). "
				"Only the internal #SK_Array functions "
				"should be able directly request "
				"statically-allocated elements, so this is "
				"either a bug somewhere in the "
				"#SK_Array functions, #req has been "
				"corrupted, or something else.",
				ska,
				ind,
				req->stat_r->hint_num_stat_elems
			);

			*stat_elem_ptr = NULL;

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	size_t offset = 0;

	SK_Array_offset_stat_elem (req, &offset);


	*stat_elem_ptr =

	((void *) (((u8 *) ska) + offset + (ind * req->stat_r->elem_size)));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to the element at index #ind from an #SK_Array.
 *
 *  Note, when viewing the #SK_Array as a whole, the statically-allocated
 *  elements and dynamically-allocated elements both make up the set of
 *  elements contained in the #SK_Array. The statically-allocated elements
 *  store the lowest-indexed elements, while the dynamically-allocated elements
 *  store the highest-indexed elements.
 *
 *  With this understood, the #ind in this function "covers" both the
 *  statically and dynamically allocated elements of the #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array
 *
 *  @param ska			The #SK_Array to inspect
 *
 *  @param ind			The index of the element in #ska to get the
 *				pointer for. This must have a value in the
 *				range [0, (ska->cur_num_elems - 1)].
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_get_elem_ptr
(
	const SK_BoxReqs *		req,

	const SK_Array *		ska,

	size_t				ind,

	void **				elem_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_H_DEBUG)
	{
		size_t cur_num_elems = 0;

		SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


		if (ind >= cur_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_H_DEBUG,
				"BUG : In #SK_Array at #ska = %p, attempted to "
				"access element at #ind = %zu although "
				"(ska->cur_num_elems) = %zu.",
				ska,
				ind,
				cur_num_elems
			)

			*elem_ptr = NULL;

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Determine whether the element indicated by #ind is in the statically
	//or dynamically allocated memory regions

	if (ind < req->stat_r->hint_num_stat_elems)
	{
		return

		SK_Array_get_stat_elem_ptr
		(
			req,
			ska,
			ind,
			elem_ptr
		);
	}

	else
	{
		size_t dyna_ind = ind - req->stat_r->hint_num_stat_elems;


		return

		SK_Array_get_dyna_elem_ptr
		(
			req,
			ska,
			dyna_ind,
			elem_ptr
		);
	}
}




/*!
 *  Sets the value #cur_num_elems in an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array
 *
 *  @param ska			The #SK_Array to modify
 *
 *  @param cur_num_elems	The new value of (ska->cur_num_elems)
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_set_cur_num_elems
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				cur_num_elems
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_Array_offset_cur_num_elems (req, &offset);


	* ((size_t *) (((u8 *) ska) + offset)) = cur_num_elems;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the value #dyna_elem_len in an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array
 *
 *  @param ska			The #SK_Array to modify
 *
 *  @param dyna_elem_len	The new value of (ska->dyna_elem_len)
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_set_dyna_elem_len
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				dyna_elem_len
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_Array_offset_dyna_elem_len (req, &offset);


	* ((size_t *) (((u8 *) ska) + offset)) = dyna_elem_len;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the value #dyna_elem_ptr in an #SK_Array.
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array
 *
 *  @param ska			The #SK_Array to modify
 *
 *  @param dyna_elem_ptr	The new value of (ska->dyna_array_ptr)
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_set_dyna_array_ptr
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	void *				dyna_elem_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_Array_offset_dyna_array_ptr (req, &offset);


	* ((void * *) (((u8 *) ska) + offset)) = dyna_elem_ptr;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.get_array
 */

//FIXME : This function needs to be tested

SlyDr SK_Array_get_array
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			beg_ind,

	size_t			end_ind,

	size_t			array_len,

	void *			array
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, array, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_L_DEBUG)
	{
		//FIXME : Maybe there should be a function, SK_Array_req_check
		//(), that can determine if a given #req argument is malformed?

		if (0 >= req->stat_r->elem_size)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#req->stat_r->elem_size = %zu, making the "
				"described #SK_Array impossible.",
				req->stat_r->elem_size
			);

			return SlyDr_get (SLY_BAD_ARG);
		}


		if (end_ind < beg_ind)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"Order of (beg_ind, end_ind) is wrong.\n"
				"#beg_ind = %zu\n"
				"#end_ind = %zu\n",
				beg_ind,
				end_ind
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Ensure that #end_ind is lower than the max index in #ska

	size_t ska_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &ska_num_elems);


	if (end_ind >= ska_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"#end_ind = %zu is too large, #ska_num_elems = %zu",
			end_ind,
			ska_num_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Modify #end_ind in the event #array_len is too small for the range
	//[beg_ind, end_ind]

	size_t num_elems =	(end_ind - beg_ind) + 1;

	num_elems =		SK_MIN (num_elems, array_len);

	end_ind =		beg_ind + (num_elems - 1);


	//Declare a pointer to keep track of the current element being accessed
	//in #array

	void * array_elem_ptr = array;


	//If necessary, iterate through the statically-allocated elements that
	//have indices in the range [beg_ind, end_ind].
	//
	//Before attempting to make changes to this section, take note of how
	//SK_Array_get_stat_elem_ptr () and SK_Array_get_dyna_elem_ptr () are
	//called only once, and the contiguousness of the arrays is exploited
	//to call as few copy operations as possible.

	if (beg_ind < req->stat_r->hint_num_stat_elems)
	{
		//Determine the indices of the statically-allocated elements
		//that should be read

		size_t start_ind =

		beg_ind;


		size_t stop_ind =

		SK_MIN ((req->stat_r->hint_num_stat_elems - 1), end_ind);


		size_t stat_num_elems =

		(stop_ind - start_ind) + 1;


		size_t num_bytes =

		stat_num_elems * req->stat_r->elem_size;


		//Get the pointer to the first statically-allocated element
		//that should be read

		void * stat_elem_ptr = NULL;

		SK_Array_get_stat_elem_ptr
		(
			req,
			ska,
			start_ind,
			&stat_elem_ptr
		);


		//Copy bytes from the statically-allocated elements to #array

		SK_byte_copy
		(
			array_elem_ptr,
			stat_elem_ptr,
			num_bytes
		);


		//Update #array_elem_ptr to the next element that should be
		//written (if there is one that should be written)

		array_elem_ptr =

		((u8 *) array_elem_ptr) + num_bytes;
	}


	//If necessary, iterate through the dynamically-allocated elements that
	//have indices in the range [beg_ind, end_ind]

	if (end_ind >= req->stat_r->hint_num_stat_elems)
	{
		//Determine the indices of the dynamically-allocated elements
		//that should be modified

		size_t start_ind =

		SK_MAX ((req->stat_r->hint_num_stat_elems), beg_ind);


		size_t stop_ind =

		end_ind;


		size_t dyna_num_elems =

		(stop_ind - start_ind) + 1;


		size_t num_bytes =

		dyna_num_elems * req->stat_r->elem_size;


		//Get the pointer to the first dynamically-allocated element
		//that should be read

		void * dyna_elem_ptr = NULL;

		SK_Array_get_dyna_elem_ptr
		(
			req,
			ska,
			(start_ind - req->stat_r->hint_num_stat_elems),
			&dyna_elem_ptr
		);


		//Copy bytes from the dynamically-allocated elements to #array

		SK_byte_copy
		(
			array_elem_ptr,
			dyna_elem_ptr,
			num_bytes
		);


		// //Update #array_elem_ptr to the next element that should be
		// //written (if there is one that should be written)
		//
		// array_elem_ptr =
		//
		// ((u8 *) array_elem_ptr) + num_bytes;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.set_array
 */

//FIXME : This function needs to be tested

SlyDr SK_Array_set_array
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			beg_ind,

	size_t			end_ind,

	size_t			array_len,

	void *			array
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, array, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_L_DEBUG)
	{
		//FIXME : Maybe there should be a function, SK_Array_req_check
		//(), that can determine if a given #req argument is malformed?

		if (0 >= req->stat_r->elem_size)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#req->stat_r->elem_size = %zu, making the "
				"described #SK_Array impossible.",
				req->stat_r->elem_size
			);

			return SlyDr_get (SLY_BAD_ARG);
		}


		if (end_ind < beg_ind)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"Order of (beg_ind, end_ind) is wrong.\n"
				"#beg_ind = %zu\n"
				"#end_ind = %zu\n",
				beg_ind,
				end_ind
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Check that #array is in fact large enough to supply the range of
	//elements [beg_ind, end_ind]

	size_t num_elems = (end_ind - beg_ind) + 1;


	if (num_elems > array_len)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"#array_len = %zu is too small for the range [%zu, %zu].",
			array_len,
			beg_ind,
			end_ind
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Ensure that #end_ind is lower than the max index in #ska

	size_t ska_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &ska_num_elems);


	if (end_ind >= ska_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"#end_ind = %zu is too large, #ska_num_elems = %zu",
			end_ind,
			ska_num_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Declare a pointer to keep track of the current element being accessed
	//in #array

	void * array_elem_ptr = array;


	//If necessary, iterate through the statically-allocated elements that
	//have indices in the range [beg_ind, end_ind].
	//
	//Before attempting to make changes to this section, take note of how
	//SK_Array_get_stat_elem_ptr () and SK_Array_get_dyna_elem_ptr () are
	//called only once, and the contiguousness of the arrays is exploited
	//to call as few copy operations as possible.

	if (beg_ind < req->stat_r->hint_num_stat_elems)
	{
		//Determine the indices of the statically-allocated elements
		//that should be read

		size_t start_ind =

		beg_ind;


		size_t stop_ind =

		SK_MIN ((req->stat_r->hint_num_stat_elems - 1), end_ind);


		size_t stat_num_elems =

		(stop_ind - start_ind) + 1;


		size_t num_bytes =

		stat_num_elems * req->stat_r->elem_size;


		//Get the pointer to the first statically-allocated element
		//that should be written

		void * stat_elem_ptr = NULL;

		SK_Array_get_stat_elem_ptr
		(
			req,
			ska,
			start_ind,
			&stat_elem_ptr
		);


		//Copy bytes from #array to the statically-allocated elements

		SK_byte_copy
		(
			stat_elem_ptr,
			array_elem_ptr,
			num_bytes
		);


		//Update #array_elem_ptr to the next element that should be
		//read (if there is one that should be read)

		array_elem_ptr =

		((u8 *) array_elem_ptr) + num_bytes;
	}


	//If necessary, iterate through the dynamically-allocated elements that
	//have indices in the range [beg_ind, end_ind]

	if (end_ind >= req->stat_r->hint_num_stat_elems)
	{
		//Determine the indices of the dynamically-allocated elements
		//that should be modified

		size_t start_ind =

		SK_MAX ((req->stat_r->hint_num_stat_elems), beg_ind);


		size_t stop_ind =

		end_ind;


		size_t dyna_num_elems =

		(stop_ind - start_ind) + 1;


		size_t num_bytes =

		dyna_num_elems * req->stat_r->elem_size;


		//Get the pointer to the first dynamically-allocated element
		//that should be written

		void * dyna_elem_ptr = NULL;

		SK_Array_get_dyna_elem_ptr
		(
			req,
			ska,
			(start_ind - req->stat_r->hint_num_stat_elems),
			&dyna_elem_ptr
		);


		//Copy bytes from the dynamically-allocated elements to #array

		SK_byte_copy
		(
			dyna_elem_ptr,
			array_elem_ptr,
			num_bytes
		);


		// //Update #array_elem_ptr to the next element that should be
		// //written (if there is one that should be read)
		//
		// array_elem_ptr =
		//
		// ((u8 *) array_elem_ptr) + num_bytes;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.rw_elems
 */

SlyDr SK_Array_rw_elems
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				beg_ind,

	size_t				end_ind,

	SK_BoxIntf_elem_rw_func		rw_func,

	void *				arb_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, rw_func, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_L_DEBUG)
	{
		//FIXME : Maybe there should be a function, SK_Array_req_check
		//(), that can determine if a given #req argument is malformed?

		if (0 >= req->stat_r->elem_size)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#req->stat_r->elem_size = %zu, making the "
				"described #SK_Array impossible.",
				req->stat_r->elem_size
			);

			return SlyDr_get (SLY_BAD_ARG);
		}


		if (end_ind < beg_ind)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"Order of (beg_ind, end_ind) is wrong.\n"
				"#beg_ind = %zu\n"
				"#end_ind = %zu\n",
				beg_ind,
				end_ind
			);

			return SlyDr_get (SLY_BAD_ARG);
		}


		size_t cur_num_elems = 0;

		SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


		if ((beg_ind >= cur_num_elems) || (end_ind >= cur_num_elems))
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"[beg_ind, end_ind] = [%zu, %zu], but "
				"#cur_num_elems = %zu",
				beg_ind,
				end_ind,
				cur_num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//If necessary, iterate through the statically-allocated elements that
	//have indices in the range [beg_ind, end_ind]
	//
	//Note, SK_Array_get_elem_ptr () is not used here because the method
	//here is faster when accessing elements sequentially.
	//
	//Before attempting to make changes to this section, take note of how
	//SK_Array_get_stat_elem_ptr () and SK_Array_get_dyna_elem_ptr () are
	//called only once, and the contiguousness of the arrays is exploited
	//to get the remaining element pointers. This is because (1 addition)
	//is faster than (1 multiplication + 1 addition).

	if (beg_ind < req->stat_r->hint_num_stat_elems)
	{
		//Determine the indices of the statically-allocated elements
		//that should be modified

		size_t start_ind =

		beg_ind;


		size_t stop_ind =

		SK_MIN ((req->stat_r->hint_num_stat_elems - 1), end_ind);


		//Get the pointer to the first statically-allocated element
		//that should be modified

		void * stat_elem_ptr = NULL;

		SK_Array_get_stat_elem_ptr
		(
			req,
			ska,
			start_ind,
			&stat_elem_ptr
		);


		//Iterate through the remaining statically-allocated elements
		//that should be modified

		for (size_t elem_sel = start_ind; elem_sel <= stop_ind; elem_sel += 1)
		{
			rw_func
			(
				req,
				stat_elem_ptr,
				elem_sel,
				arb_arg
			);


			stat_elem_ptr =

			((u8 *) stat_elem_ptr) + req->stat_r->elem_size;
		}
	}


	//If necessary, iterate through the dynamically-allocated elements that
	//have indices in the range [beg_ind, end_ind]

	if (end_ind >= req->stat_r->hint_num_stat_elems)
	{
		//Determine the indices of the dynamically-allocated elements
		//that should be modified

		size_t start_ind =

		SK_MAX ((req->stat_r->hint_num_stat_elems), beg_ind);


		size_t stop_ind =

		end_ind;


		//Get the pointer to the first dynamically-allocated element
		//that should be modified

		void * dyna_elem_ptr = NULL;

		SK_Array_get_dyna_elem_ptr
		(
			req,
			ska,
			(start_ind - req->stat_r->hint_num_stat_elems),
			&dyna_elem_ptr
		);


		//Iterate through the remaining dynamically-allocated elements
		//that should be modified

		for (size_t elem_sel = start_ind; elem_sel <= stop_ind; elem_sel += 1)
		{
			rw_func
			(
				req,
				dyna_elem_ptr,
				elem_sel,
				arb_arg
			);


			dyna_elem_ptr =

			((u8 *) dyna_elem_ptr) + req->stat_r->elem_size;
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Determines the number of elements that the dynamically allocated array of
 *  an #SK_Array should be resized to.
 *
 *  This function does NOT modify the given #SK_Array, and it does NOT allocate
 *  memory itself.
 *
 *  When calculating #new_dyna_elem_len, this function WILL take into account
 *  (req->dyna_i->r.hint_neg_slack) and (req->dyna_i->r.hint_pos_slack), as
 *  well as the current size of the dynamically allocated array already used by
 *  #ska (if any).
 *
 *  This function is intended to be used as a lower-level function in the
 *  implementations of SK_Array_ins_range () and SK_Array_resize ().
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array.
 *
 *  @param ska			The #SK_Array to inspect, which is needed to
 *				determine if a dynamic memory allocation is
 *				necessary at all.
 *
 *				#ska will NOT be modified by this function.
 *
 *  @param new_num_elems	The number of elements that the caller is
 *				targeting for #ska to store.
 *
 *				This would be the value that the caller desires
 *				setting (ska->cur_num_elems) to.
 *
 *  @param realloc_needed	Indicates if a dynamic memory reallocation is
 *				necessary for #ska to hold #new_num_elems
 *				whilst respecting the slack hints in #req.
 *
 *				If this is set to 0, then NO dynamic memory
 *				reallocation is necessary, meaning
 *				(ska->cur_num_elems) can just be updated to use
 *				a different amount of the current dynamically
 *				allocated array.
 *
 *				If this is set to 1, then a dynamic memory
 *				reallocation SHOULD occur, meaning that
 *				(ska->dyna_array_ptr) and (ska->dyna_elem_len)
 *				should be updated.
 *
 *				This argument CAN be NULL.
 *
 *  @param new_dyna_elem_len	The number of elements that should be allocated
 *				in a new dynamically allocated array.
 *
 *				If #realloc_needed gets set to 0, this will be
 *				set equal to the previous value of
 *				#dyna_elem_len in #ska.
 *
 *				If #realloc_needed gets set to 1 but this is
 *				set to 0, then that means the current
 *				dynamically allocated array,
 *				(ska->dyna_array_ptr), should be deallocated.
 *				This would mean that only the
 *				statically-allocated elements are needed to
 *				store #new_num_elems.
 *
 *				This argument CAN be NULL.
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_calc_new_dyna_elem_len
(
	const SK_BoxReqs *	req,

	const SK_Array *	ska,

	size_t			new_num_elems,

	int *			realloc_needed,

	size_t *		new_dyna_elem_len
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_L_DEBUG)
	{
		//FIXME : Maybe there should be a function,
		//SK_Array_req_check (), that can determine if a given #req
		//argument is malformed? Or should this be a higher-level
		//SK_Box_* () function?

		if (0 >= req->stat_r->elem_size)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"req->stat_r->elem_size = %zu, making the "
				"described #SK_Array impossible.",
				req->stat_r->elem_size
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Determine if the current number of elements stored in #ska matches
	//#new_num_elems to see if any reallocation is necessary.
	//
	//Note that in order to take advantage of this opportunity,
	//(req->dyna_i->r.hint_neg_slack) must be greater than or equal to
	//(req->dyna_i->r.hint_pos_slack).
	//
	//This is because when the negative-slack is less than the
	//positive-slack, then technically resizing to the same number of
	//elements causes blocks previously allocated as slack to get
	//immediately deallocated.

	size_t old_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &old_num_elems);


	size_t old_dyna_elem_len = 0;

	SK_Array_get_dyna_elem_len (req, ska, &old_dyna_elem_len);


	if
	(
		(req->dyna_i->r.hint_neg_slack >= req->dyna_i->r.hint_pos_slack)

		&&

		(new_num_elems == old_num_elems)
	)
	{
		if (NULL != realloc_needed)
		{
			*realloc_needed = 0;
		}


		if (NULL != new_dyna_elem_len)
		{
			*new_dyna_elem_len = old_dyna_elem_len;
		}


		return SlyDr_get (SLY_SUCCESS);
	}


	//Determine the number of elements per "block" that are used with #ska.
	//
	//Since #SK_Array stores elements in a contiguous array, there aren't
	//really separate blocks to speak of. So, the best way to respect the
	//(req->stat_r->hint_elems_per_block) hint is to round the number of
	//elements upwards to the nearest whole multiple of this.
	//
	//What is the value of doing that? It theoretically reduces the number
	//of resize operations needed in a similar fashion to the "slack"
	//hints.
	//
	//Note, if #hint_elems_per_block is 0, we treat it as if it is
	//#SK_ARRAY_DEF_ELEMS_PER_BLOCK.

	size_t elems_per_block = req->stat_r->hint_elems_per_block;

	if (0 >= elems_per_block)
	{
		elems_per_block = SK_ARRAY_DEF_ELEMS_PER_BLOCK;
	}


	//Determine the current number of dynamically-allocated "blocks" that
	//are used to store elements, and then determine the minimum number of
	//blocks that will be needed to store the new number of elements. Right
	//now, we only care about the number of dynamically-allocated blocks
	//because that is what will have to be reallocated if a memory
	//reallocation occurs.
	//
	//Note how #old_dyna_alloc_blocks is the result of a floored division,
	//and #new_dyna_alloc_blocks is the result of a ceiling division.
	//
	//This is because we don't want to count partial blocks in the count of
	//currently available blocks, and if #new_num_elems would require a
	//partial block to store, we would expand it to a full block.

	size_t old_dyna_alloc_blocks =

	(u64) (old_dyna_elem_len / elems_per_block);


	size_t min_new_dyna_elem_len = 0;

	if (new_num_elems > req->stat_r->hint_num_stat_elems)
	{
		min_new_dyna_elem_len =

		(new_num_elems - req->stat_r->hint_num_stat_elems);
	}


	size_t min_new_dyna_alloc_blocks =

	SK_ceil_div_u64 (min_new_dyna_elem_len, elems_per_block);


	//Based off the current number of dynamically-allocated blocks and the
	//minimum number of dynamically-allocated blocks that can possibly
	//store #new_num_elems, determine the number of dynamically-allocated
	//blocks that should actually be used

	size_t new_dyna_alloc_blocks = 0;

	SK_Box_calc_new_num_blocks
	(
		req,

		old_dyna_alloc_blocks,
		min_new_dyna_alloc_blocks,

		&new_dyna_alloc_blocks
	);


	//If the number of dynamically-allocated blocks is left unchanged,
	//report that no dynamic memory reallocation is necessary

	if (old_dyna_alloc_blocks == new_dyna_alloc_blocks)
	{
		if (NULL != realloc_needed)
		{
			*realloc_needed = 0;
		}

		if (NULL != new_dyna_elem_len)
		{
			*new_dyna_elem_len = old_dyna_elem_len;
		}


		return SlyDr_get (SLY_SUCCESS);
	}


	//If this point has been reached, then it has been determined that a
	//dynamic memory reallocation IS necessary, so report the needed size
	//of the necessary dynamically allocated array.

	if (NULL != realloc_needed)
	{
		*realloc_needed = 1;
	}

	if (NULL != new_dyna_elem_len)
	{
		*new_dyna_elem_len = (new_dyna_alloc_blocks * elems_per_block);
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Resizes an #SK_Array so that it can store a different number of elements
 *  but without any consideration for "slack".
 *
 *  The phrase "_direct_" implies that no extra space for elements gets
 *  allocated, the exact number of elements requested is what gets allocated.
 *  The phrase "_unsafe_" implies that no elements get initialized here.
 *
 *  This is a lower-level resize operation that is intended to be used by other
 *  resize operations.
 *
 *  @param req			The #SK_BoxReqs that represent general
 *				parameters of the #SK_Array
 *
 *  @param ska			The #SK_Array to inspect
 *
 *  @param new_dyna_elem_len	The new number of elements that should be
 *				allocated at (ska->dyna_elem_ptr).
 *
 *  @param new_num_elems	The new number of elements that #ska will
 *				store, which will become the new value of
 *				(ska->num_elems)
 *
 *  @return			Standard status code
 */

SlySr SK_Array_direct_resize_unsafe
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				new_dyna_elem_len,

	size_t				new_num_elems
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	size_t elem_size = req->stat_r->elem_size;

	if (SK_ARRAY_L_DEBUG)
	{
		//FIXME : Maybe there should be a function, SK_Array_req_check
		//(), that can determine if a given #req argument is malformed?

		if (0 >= elem_size)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#req->stat_r->elem_size = %zu, making the "
				"described #SK_Array impossible.",
				req->stat_r->elem_size
			);

			return SlySr_get (SLY_BAD_ARG);
		}
	}


	size_t num_stat_elems = req->stat_r->hint_num_stat_elems;

	if (SK_ARRAY_L_DEBUG)
	{
		if (new_num_elems > (new_dyna_elem_len + num_stat_elems))
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#new_num_elems too large for given "
				"#new_dyna_elem_len and #req.\n"
				"new_num_elems =     %zu\n"
				"new_dyna_elem_len = %zu\n"
				"num_stat_elems =    %zu\n",
				new_num_elems,
				new_dyna_elem_len,
				num_stat_elems
			);

			return SlySr_get (SLY_BAD_ARG);
		}
	}


	//Determine the old number of dynamically-allocated elements

	size_t old_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &old_num_elems);


	size_t old_dyna_elem_len = 0;

	SK_Array_get_dyna_elem_len (req, ska, &old_dyna_elem_len);


	//Determine where the old dynamically-allocated elements are stored

	void * old_dyna_elem_ptr = NULL;

	if (0 < old_dyna_elem_len)
	{
		SK_Array_get_dyna_elem_ptr (req, ska, 0, &old_dyna_elem_ptr);
	}


	//Check if a memory allocation is actually necessary to complete the
	//requested resize operation

	if (old_dyna_elem_len == new_dyna_elem_len)
	{
		//Simply record the new number of elements used and complete

		SK_Array_set_cur_num_elems (req, ska, new_num_elems);


		return SlySr_get (SLY_SUCCESS);
	}


	//Allocate space for the new number of dynamically-allocated elements

	void * new_dyna_elem_ptr = NULL;


	if (0 < new_dyna_elem_len)
	{
		//FIXME : Consider using mem->retake () here instead

		SlySr sr =

		req->stat_r->mem->take
		(
			&new_dyna_elem_ptr,

			new_dyna_elem_len,

			elem_size
		);


		SK_RES_RETURN (SK_ARRAY_L_DEBUG, sr.res, sr);
	}


	//Copy the old dynamically-allocated elements that will need to be
	//preserved over to the new dynamically-allocated memory region.
	//
	//Note that by taking advantage of the fact that #old_dyna_elem_ptr and
	//#new_dyna_elem_ptr point to separate but contiguous memory regions,
	//the copying of elements can be done in 1 SK_byte_copy () call.

	size_t old_used_dyna_elems =

	(old_num_elems > num_stat_elems) ? (old_num_elems - num_stat_elems) : 0;


	size_t new_used_dyna_elems =

	(new_num_elems > num_stat_elems) ? (new_num_elems - num_stat_elems) : 0;


	size_t num_copy_elems =

	SK_MIN (old_used_dyna_elems, new_used_dyna_elems);


	if (num_copy_elems > 0)
	{
		SK_byte_copy
		(
			new_dyna_elem_ptr,
			old_dyna_elem_ptr,
			(num_copy_elems * elem_size)
		);
	}


	//Now that the old dynamically-allocated elements have been saved into
	//the new dynamically-allocated storage space, deallocate the old space
	//and make #ska use this new space

	if (NULL != old_dyna_elem_ptr)
	{
		req->stat_r->mem->free (old_dyna_elem_ptr);
	}


	SK_Array_set_dyna_array_ptr (req, ska, new_dyna_elem_ptr);

	SK_Array_set_dyna_elem_len (req, ska, new_dyna_elem_len);


	//Make sure to record the number of elements actually used

	SK_Array_set_cur_num_elems (req, ska, new_num_elems);


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.resize_unsafe
 */

SlySr SK_Array_resize_unsafe
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				new_num_elems
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Determine if reallocating (ska->dyna_array_ptr) is necessary.
	//
	//Note that SK_Array_calc_new_dyna_elem_len () takes into account the
	//hints for slack contained in #req, and this affects the value of
	//#new_dyna_elem_len.

	int realloc_needed =		0;

	size_t new_dyna_elem_len =	0;


	SK_Array_calc_new_dyna_elem_len
	(
		req,
		ska,
		new_num_elems,
		&realloc_needed,
		&new_dyna_elem_len
	);


	//Respect (req->dyna_i->r.hint_use_max_num_dyna_blocks) by
	//intentionally failing if #new_dyna_elem_len contains too many blocks.

	size_t elems_per_block = req->stat_r->hint_elems_per_block;

	if (0 >= elems_per_block)
	{
		elems_per_block = SK_ARRAY_DEF_ELEMS_PER_BLOCK;
	}


	size_t num_dyna_blocks =

	SK_ceil_div_u64 (new_dyna_elem_len, elems_per_block);


	if
	(
		(req->dyna_i->r.hint_use_max_num_dyna_blocks)

		&&

		(num_dyna_blocks > req->dyna_i->r.hint_max_num_dyna_blocks)
	)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"Resize failed, #new_num_elems = %zu would cause "
			"#num_dyna_blocks = %zu, and "
			"#req->dyna_i->r.hint_max_num_dyna_blocks = %zu",
			new_num_elems,
			num_dyna_blocks,
			req->dyna_i->r.hint_max_num_dyna_blocks
		);


		//FIXME : Is this the best #SlySr value to use?

		return SlySr_get (SLY_BAD_ALLOC);
	}


	//Command SK_Array_direct_resize_unsafe () to perform a resize
	//operation if necessary.
	//
	//If it is not necessary, simply mark that #new_num_elems number of
	//elements are being used in #ska.

	if (realloc_needed)
	{
		return

		SK_Array_direct_resize_unsafe
		(
			req,
			ska,
			new_dyna_elem_len,
			new_num_elems
		);
	}

	else
	{
		SK_Array_set_cur_num_elems (req, ska, new_num_elems);

		return SlySr_get (SLY_SUCCESS);
	}
}




/*!
 *  See #SK_BoxIntf.resize
 */

SlySr SK_Array_resize
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				new_num_elems,

	SK_BoxIntf_elem_rw_func		elem_init_func,

	void *				arb_arg
)
{

	//Determine how many elements are currently in #ska

	size_t old_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &old_num_elems);


	//Attempt to resize the #SK_Array

	SlySr sr = SK_Array_resize_unsafe (req, ska, new_num_elems);


	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"Failed to resize #ska = %p to store %zu elements.",
			ska,
			new_num_elems
		);

		return sr;
	}


	//In the event that #new_num_elems is greater than #old_num_elems,
	//these new elements will have to be initialized

	if (new_num_elems > old_num_elems)
	{
		//Take into account when #elem_init_func is NULL

		if (NULL == elem_init_func)
		{
			elem_init_func = SK_Box_elem_rw_set_zero;
		}


		//Execute #elem_init_func on the newly allocated elements in
		//#ska

		SK_Array_rw_elems
		(
			req,
			ska,
			old_num_elems,
			(new_num_elems - 1),
			elem_init_func,
			arb_arg
		);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.init_unsafe
 */

SlySr SK_Array_init_unsafe
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				num_elems
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Initially make #ska represent an empty (yet valid) #SK_Array, that
	//way it can be safely passed to other SK_Array_* () functions

	SK_Array_set_cur_num_elems (req, ska, 0);

	SK_Array_set_dyna_elem_len (req, ska, 0);

	SK_Array_set_dyna_array_ptr (req, ska, NULL);


	//Use SK_Array_resize_unsafe () to take care of the element allocation

	return SK_Array_resize_unsafe (req, ska, num_elems);

}




/*!
 *  See #SK_BoxIntf.init
 */

SlySr SK_Array_init
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				num_elems,

	SK_BoxIntf_elem_rw_func		elem_init_func,

	void *				arb_arg
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Use SK_Array_init_unsafe () to perform the initialization of #ska's
	//fields and the allocation of elements. Note that the elements
	//themselves will still be uninitialized at this point, however.

	SlySr sr = SK_Array_init_unsafe (req, ska, num_elems);

	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"Failed to initialize #ska = %p to store %zu "
			"elements.",
			ska,
			num_elems
		);

		return sr;
	}


	//Initialize all of the elements now contained in #ska, if there are
	//any

	if (0 < num_elems)
	{
		//Take into account when #elem_init_func is NULL

		if (NULL == elem_init_func)
		{
			elem_init_func = SK_Box_elem_rw_set_zero;
		}


		//Execute #elem_init_func on all of the elements in #ska

		SK_Array_rw_elems
		(
			req,
			ska,
			0,
			(num_elems - 1),
			elem_init_func,
			arb_arg
		);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.deinit
 */

SlyDr SK_Array_deinit
(
	const SK_BoxReqs *		req,

	SK_Array *			ska
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	//Deallocate the dynamically allocated elements

	void * dyna_elem_ptr = NULL;

	SK_Array_get_dyna_array_ptr (req, ska, &dyna_elem_ptr);

	return req->stat_r->mem->free (dyna_elem_ptr);

}




/*!
 *  See #SK_BoxIntf.get_resize_limits
 */

SlyDr SK_Array_get_resize_limits
(
	const SK_BoxReqs *		req,

	int *				limits_exist,

	size_t *			min_num_elems,
	size_t *			max_num_elems
)
{

	//#SK_Array can theoretically be used to store #SIZE_MAX number of
	//elements, so this function reports that no limits exist UNLESS a
	//limit has been requested with
	//#req->dyna_i->r.hint_use_max_num_dyna_blocks.

	if (req->dyna_i->r.hint_use_max_num_dyna_blocks)
	{
		//Determine the number of elements per block

		size_t elems_per_block = 0;

		if (0 == req->stat_r->hint_elems_per_block)
		{

			elems_per_block = SK_ARRAY_DEF_ELEMS_PER_BLOCK;

		}

		else
		{
			elems_per_block = req->stat_r->hint_elems_per_block;
		}


		size_t max_num_dyna_blocks =

		req->dyna_i->r.hint_max_num_dyna_blocks;


		//Determine the maximum number of elements that could be stored
		//in dynamically-allocated blocks, and then check for overflow

		size_t calc_max =

		elems_per_block * max_num_dyna_blocks;


		if
		(
			(0 != elems_per_block)

			&&

			((calc_max / elems_per_block) != max_num_dyna_blocks)
		)
		{

			calc_max = SIZE_MAX;
		}


		//Now take into account the number of elements that could be
		//stored outside of the dynamically-allocated blocks

		size_t num_stat_elems = req->stat_r->hint_num_stat_elems;


		if ((calc_max + num_stat_elems) >= calc_max)
		{
			calc_max = (calc_max + num_stat_elems);
		}

		else
		{
			calc_max = SIZE_MAX;
		}


		//Report the size limits associated with #SK_Array's configured
		//by #req

		if (NULL != limits_exist)
		{
			*limits_exist = 1;
		}

		if (NULL != min_num_elems)
		{
			*min_num_elems = 0;
		}

		if (NULL != max_num_elems)
		{
			*max_num_elems = calc_max;
		}
	}

	else
	{
		if (NULL != limits_exist)
		{
			*limits_exist = 0;
		}

		if (NULL != min_num_elems)
		{
			*min_num_elems = 0;
		}

		if (NULL != max_num_elems)
		{
			*max_num_elems = SIZE_MAX;
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.inst_dyna_mem
 */

SlyDr SK_Array_inst_dyna_mem
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t *		size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, size, SlyDr_get (SLY_BAD_ARG));


	//Determine the number of dynamically-allocated elements, including
	//ones allocated as slack

	size_t dyna_elem_len = 0;

	SK_Array_get_dyna_elem_len (req, ska, &dyna_elem_len);


	//Calculate the dynamic memory usage
	//
	//FIXME : Should there be some overflow detection here? (Most likely
	//yes)

	*size = (req->stat_r->elem_size * dyna_elem_len);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.need_req_regi ()
 */

SlyDr SK_Array_need_req_regi
(
	const SK_BoxReqs *	req,

	int *			need_req_regi
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, need_req_regi, SlyDr_get (SLY_BAD_ARG));


	*need_req_regi = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.minify ()
 */


SlySr SK_Array_minify
(
	const SK_BoxReqs *	req,

	SK_Array *		ska
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Determine the number of elements currently used in #ska

	size_t cur_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


	//Determine the number of elements stored per "block".
	//
	//Note, if #hint_elems_per_block is 0, we treat it as if it is
	//#SK_ARRAY_DEF_ELEMS_PER_BLOCK.

	size_t elems_per_block = req->stat_r->hint_elems_per_block;

	if (0 >= elems_per_block)
	{
		elems_per_block = SK_ARRAY_DEF_ELEMS_PER_BLOCK;
	}


	//Determine the minimum number of blocks that are needed to store the
	//CURRENT number of elements. Right now, we only care about the number
	//of dynamically-allocated blocks because that is what will have to be
	//reallocated if a memory reallocation occurs.

	size_t min_new_dyna_elem_len = 0;

	if (cur_num_elems > req->stat_r->hint_num_stat_elems)
	{
		min_new_dyna_elem_len =

		(cur_num_elems - req->stat_r->hint_num_stat_elems);
	}


	size_t min_new_dyna_alloc_blocks =

	SK_ceil_div_u64 (min_new_dyna_elem_len, elems_per_block);


	//Adjust the value of #min_new_dyna_elem_len so that it obeys
	//#elems_per_block

	min_new_dyna_elem_len = min_new_dyna_alloc_blocks * elems_per_block;


	//Remove all of the extra slack from #ska

	return

	SK_Array_direct_resize_unsafe
	(
		req,
		ska,
		min_new_dyna_elem_len,
		cur_num_elems
	);

}




/*!
 *  See #SK_BoxIntf.optimize ()
 */

SlySr SK_Array_optimize
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	double			size_worth,

	double			speed_worth
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//The memory layout of #SK_Array is very straightforward, so there
	//really is no room for optimization of #SK_Array (except arguably
	//calling SK_Array_minify (), but keep in mind that it removes slack as
	//well).

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.anc_support ()
 */

SlyDr SK_Array_anc_support
(
	const SK_BoxReqs *		req,

	int *				support
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, support, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not useful for #SK_Array in particular, because the
	//position of every element IS known from the information in the
	//statically-allocated memory of #SK_Array.

	*support = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.anc_recommend ()
 */

SlyDr SK_Array_anc_recommend
(
	const SK_BoxReqs *		req,

	const SK_Array *		ska,

	int *				recommend
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, recommend, SlyDr_get (SLY_BAD_ARG));


	//See note in SK_Array_anc_support () about why anchors are not used
	//in #SK_Array

	*recommend = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.anc_size ()
 */

SlyDr SK_Array_anc_size
(
	const SK_BoxReqs *		req,

	size_t *			size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, size, SlyDr_get (SLY_BAD_ARG));


	//#SK_Array does not support anchors, so this is reflected in #size

	*size = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.anc_validate ()
 */

SlyDr SK_Array_anc_validate
(
	const SK_BoxReqs *		req,

	const SK_Array *		ska,

	const SK_BoxAnc *		anc,

	size_t *			ind,

	int *				is_valid
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));


	//Since #SK_Array does not support anchors, simply report that any
	//anchor given is invalid

	if (NULL != ind)
	{
		*ind = 0;
	}

	if (NULL != is_valid)
	{
		*is_valid = 0;
	}


	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, anchors are not supported by #SK_Array."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.anc_ind ()
 */

SlyDr SK_Array_anc_ind
(
	const SK_BoxReqs *		req,

	const SK_Array *		ska,

	const SK_BoxAnc *		anc,

	size_t *			ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ind, SlyDr_get (SLY_BAD_ARG));


	//Since #SK_Array does not support anchors, simply report that any
	//anchor given is invalid

	if (NULL != ind)
	{
		*ind = 0;
	}


	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, anchors are not supported by #SK_Array."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.anc_inc ()
 */

SlyDr SK_Array_anc_inc
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	SK_BoxAnc *			cur_anc
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, cur_anc, SlyDr_get (SLY_BAD_ARG));


	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, anchors are not supported by #SK_Array."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.anc_dec ()
 */

SlyDr SK_Array_anc_dec
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	SK_BoxAnc *			cur_anc
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, cur_anc, SlyDr_get (SLY_BAD_ARG));


	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, anchors are not supported by #SK_Array."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.get_anc ()
 */

SlyDr SK_Array_get_anc
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				ind,

	SK_BoxAnc *			anc
)
{

	//Sanity check on arguments
	//
	//This may seem useless for this function, since it will just return
	//#SLY_NO_IMPL anyway, but it can help catch bugs when a caller is
	//using this interface and then switches it out with another
	//#SK_BoxIntf.

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));


	//Check if #ind is out of range or not

	size_t cur_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


	if (ind >= cur_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"#ind = %zu is out of range, "
			"cur_num_elems = %zu",
			ind,
			cur_num_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//#SK_Array does not support anchors, so the anchor arguments will not
	//get modified here at all.


	//Just return #SLY_NO_IMPL since #SK_Array does not support anchors

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, anchors are not supported by #SK_Array."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.region_avg_len ()
 */

SlyDr SK_Array_region_avg_len
(
	const SK_BoxReqs *		req,

	const SK_Array *		ska,

	double *			avg_len
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, avg_len, SlyDr_get (SLY_BAD_ARG));


	//Report the value of (cur_num_elems / 2) as the average region length,
	//because in most cases there will be a static region and a dynamic
	//region.
	//
	//It would be more accurate to determine if only static elements are
	//used or dynamic elements are used as well, but this is slightly
	//faster.

	size_t cur_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


	*avg_len = ((double) cur_num_elems) / 2;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.get_region_ptr ()
 */

SlyDr SK_Array_get_region_ptr
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				ind,

	void **				reg_ptr,

	size_t *			reg_beg,

	size_t *			reg_end
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_L_DEBUG)
	{
		//Check if #ind is out of range or not

		size_t cur_num_elems = 0;

		SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


		if (ind >= cur_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#ind = %zu is out of range, "
				"cur_num_elems = %zu",
				ind,
				cur_num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Select the reported region depending on whether #ind refers to an
	//element that is statically-allocated or dynamically-allocated

	if (ind < req->stat_r->hint_num_stat_elems)
	{

		size_t cur_num_elems = 0;

		SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


		void * elem_ptr = NULL;

		SK_Array_get_elem_ptr (req, ska, 0, &elem_ptr);


		if (NULL != reg_ptr)
		{
			*reg_ptr = elem_ptr;
		}

		if (NULL != reg_beg)
		{
			*reg_beg = 0;
		}

		if (NULL != reg_end)
		{
			*reg_end =

			SK_MIN
			(
				cur_num_elems,
				(req->stat_r->hint_num_stat_elems - 1)
			);
		}
	}

	else
	{

		size_t cur_num_elems = 0;

		SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


		void * elem_ptr = NULL;

		SK_Array_get_elem_ptr
		(
			req,
			ska,
			req->stat_r->hint_num_stat_elems,
			&elem_ptr
		);


		if (NULL != reg_ptr)
		{
			*reg_ptr = elem_ptr;
		}

		if (NULL != reg_beg)
		{
			*reg_beg = req->stat_r->hint_num_stat_elems;
		}

		if (NULL != reg_end)
		{
			*reg_end = cur_num_elems - 1;
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.get_elem ()
 */

SlyDr SK_Array_get_elem
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				ind,

	void *				elem
)
{
	//Sanity check on arguments
	//
	//FIXME : This is pretty expensive to do per-element. Consider making
	//these sanity checks *_H_DEBUG instead of *_L_DEBUG

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_L_DEBUG)
	{
		//Check if #ind is out of range or not

		size_t cur_num_elems = 0;

		SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


		if (ind >= cur_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#ind = %zu is out of range, "
				"cur_num_elems = %zu",
				ind,
				cur_num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}



	//Get a pointer to the element at #ind in #ska

	void * src_elem_ptr = NULL;

	SK_Array_get_elem_ptr (req, ska, ind, &src_elem_ptr);


	//Copy from the element in #ska to #elem, using #req->stat_i->prim_copy ()
	//if it is available

	if (req->stat_i->prim_flag)
	{
		return req->stat_i->prim_copy (elem, src_elem_ptr);
	}

	else
	{
		return SK_byte_copy (elem, src_elem_ptr, req->stat_r->elem_size);
	}
}




/*!
 *  See SK_BoxIntf.set_elem ()
 */

SlyDr SK_Array_set_elem
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				ind,

	const void *			elem
)
{
	//Sanity check on arguments
	//
	//FIXME : This is pretty expensive to do per-element. Consider making
	//these sanity checks *_H_DEBUG instead of *_L_DEBUG

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_L_DEBUG)
	{
		//Check if #ind is out of range or not

		size_t cur_num_elems = 0;

		SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


		if (ind >= cur_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#ind = %zu is out of range, "
				"cur_num_elems = %zu",
				ind,
				cur_num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}



	//Get a pointer to the element at #ind in #ska

	void * dst_elem_ptr = NULL;

	SK_Array_get_elem_ptr (req, ska, ind, &dst_elem_ptr);


	//Copy from #elem to the element in #ska, using #req->stat_i->prim_copy ()
	//if it is available

	if (req->stat_i->prim_flag)
	{
		return req->stat_i->prim_copy (dst_elem_ptr, elem);
	}

	else
	{
		return SK_byte_copy (dst_elem_ptr, elem, req->stat_r->elem_size);
	}
}




/*!
 *  See #SK_BoxIntf.swap ()
 */

SlyDr SK_Array_swap
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				ind_0,

	size_t				ind_1
)
{
	//Sanity check on arguments
	//
	//FIXME : This is pretty expensive to do per-swap. Consider making
	//these sanity checks *_H_DEBUG instead of *_L_DEBUG

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	if (SK_ARRAY_L_DEBUG)
	{
		size_t num_elems = 0;

		SK_Array_get_cur_num_elems (req, ska, &num_elems);


		if ((ind_0 >= num_elems) || (ind_1 >= num_elems))
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#ind_0 = %zu and #ind_1 = %zu were not "
				"both below #num_elems = %zu",
				ind_0,
				ind_1,
				num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Get pointers to the elements that have to be swapped.

	void * elem_0_ptr = NULL;
	void * elem_1_ptr = NULL;

	SK_Array_get_elem_ptr (req, ska, ind_0, &elem_0_ptr);
	SK_Array_get_elem_ptr (req, ska, ind_1, &elem_1_ptr);


	//Swap the elements locations, using #req->stat_i->prim_swap () if it is
	//available

	if (req->stat_i->prim_flag)
	{
		return req->stat_i->prim_swap (elem_0_ptr, elem_1_ptr);
	}

	else
	{
		return SK_byte_swap (elem_0_ptr, elem_1_ptr, req->stat_r->elem_size);
	}
}




/*!
 *  See #SK_BoxIntf.swap_range ()
 */

SlyDr SK_Array_swap_range
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			num_elems,

	size_t			ind_0,

	size_t			ind_1
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	if (0 >= num_elems)
	{
		//If #num_elems is 0, then there is no work to perform

		return SlyDr_get (SLY_SUCCESS);
	}


	if (SK_ARRAY_L_DEBUG)
	{
		size_t cur_num_elems = 0;

		SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


		if
		(
			((ind_0 + num_elems - 1) >= cur_num_elems)

			||

			((ind_1 + num_elems - 1) >= cur_num_elems)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#num_elems = %zu, #ind_0 = %zu, #ind_1 = %zu "
				"were not both below #cur_num_elems = %zu",
				num_elems,
				ind_0,
				ind_1,
				cur_num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}


		//FIXME : Consider making a function SK_range_overlap () that
		//can detect range overlaps

		if
		(
			((ind_0 + num_elems - 1) >= ind_1)

			||

			((ind_1 + num_elems - 1) >= ind_0)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#num_elems = %zu, #ind_0 = %zu, #ind_1 = %zu "
				"described overlapping ranges, can not "
				"perform swap operation.",
				num_elems,
				ind_0,
				ind_1
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Swap all of the elements in the ranges [ind_0, ind_0 + num_elems - 1]
	//and [ind_1, ind_1 + num_elems - 1]

	//FIXME : This can be made more efficient by calling SK_byte_swap () on
	//as large as possible of memory regions within #ska.
	//
	//See SK_Array_ins_range_unsafe () and SK_Array_rem_range () for an
	//example of how this can be implemented, both of which feed as large
	//as possible memory regions to SK_byte_copy ().

	for (size_t elem_sel = 0; elem_sel < num_elems; elem_sel += 1)
	{
		SK_Array_swap
		(
			req,
			ska,
			ind_0 + elem_sel,
			ind_1 + elem_sel
		);
	}

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.ins_range_unsafe ().
 */

//TODO : All of the SK_Array_ins_* () functions need to be tested. The indexing
//in this function is likely flawed in some way until testing completes.

SlySr SK_Array_ins_range_unsafe
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				num_ins_elems,

	size_t				ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//If #num_ins_elems is 0, exit early because no elements need to be
	//inserted then

	if (0 >= num_ins_elems)
	{
		return SlySr_get (SLY_SUCCESS);
	}


	//Take note of how this function differs from
	//SK_Array_direct_resize_unsafe (). That function always adds / removes
	//elements from the top-indices of #ska, while this functions adds
	//elements starting at an arbitrary index.
	//
	//Therefore, the manner in which elements are copied from old
	//dynamically allocated arrays to new dynamically allocated arrays in
	//this function is performed with this in mind.


	//Determine the number of elements in #ska

	size_t old_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &old_num_elems);


	//Check if #ind is in the range [0, old_num_elems] or not.
	//
	//Note, #ind CAN equal #old_num_elems for this function, because that
	//means inserting the new element to the end of the #SK_Array.

	if (ind > old_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"#ind = %zu is out of range, "
			"old_num_elems = %zu",
			ind,
			old_num_elems
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	//Determine the new number of elements that should be stored in #ska

	size_t new_num_elems = old_num_elems + num_ins_elems;

	if (new_num_elems < old_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"(old_num_elems + num_ins_elems) = (%zu + %zu) "
			"overflows to %zu. Can not perform insertion "
			"operation.",
			old_num_elems,
			num_ins_elems,
			new_num_elems
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	//Determine the highest index associated with the element range to be
	//inserted

	size_t ind_end = ind + (num_ins_elems - 1);


	//Determine if a dynamic memory reallocation is actually necessary to
	//store #new_num_elems number of elements

	int realloc_needed =		0;

	size_t new_dyna_elem_len =	0;


	SK_Array_calc_new_dyna_elem_len
	(
		req,
		ska,
		new_num_elems,
		&realloc_needed,
		&new_dyna_elem_len
	);


	//Respect (req->dyna_i->r.hint_use_max_num_dyna_blocks) by
	//intentionally failing if #new_dyna_elem_len contains too many blocks.

	size_t elems_per_block = req->stat_r->hint_elems_per_block;

	if (0 >= elems_per_block)
	{
		elems_per_block = SK_ARRAY_DEF_ELEMS_PER_BLOCK;
	}


	size_t num_dyna_blocks =

	SK_ceil_div_u64 (new_dyna_elem_len, elems_per_block);


	if
	(
		(req->dyna_i->r.hint_use_max_num_dyna_blocks)

		&&

		(num_dyna_blocks > req->dyna_i->r.hint_max_num_dyna_blocks)
	)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"Resize failed, #new_num_elems = %zu would cause "
			"#num_dyna_blocks = %zu, and "
			"#req->dyna_i->r.hint_max_num_dyna_blocks = %zu",
			new_num_elems,
			num_dyna_blocks,
			req->dyna_i->r.hint_max_num_dyna_blocks
		);


		//FIXME : Is this the best #SlySr value to use?

		return SlySr_get (SLY_BAD_ALLOC);
	}


	//Retrieve pointers for the statically / dynamically allocated arrays
	//associated with #ska

	void * stat_array_ptr = NULL;

	if (0 < req->stat_r->hint_num_stat_elems)
	{
		SK_Array_get_stat_elem_ptr (req, ska, 0, &stat_array_ptr);
	}


	void * dyna_array_ptr =	NULL;

	size_t dyna_elem_len =	0;


	SK_Array_get_dyna_array_ptr (req, ska, &dyna_array_ptr);

	SK_Array_get_dyna_elem_len (req, ska, &dyna_elem_len);


	//If a dynamic memory reallocation is necessary, attempt to resize the
	//dynamically allocated array.
	//
	//Note that there are two obvious ways to perform the memory
	//reallocation in this section:
	//
	//  1)
	//
	//	Use mem->take () to allocate a new block, move the elements
	//	over to it within this function, and then deallocate the old
	//	block
	//
	//  2)
	//  	Use mem->retake () to resize (or allocate) the current memory
	//  	block in #ska, which may or may not move all the elements in
	//  	the current block to a new memory location. Then, in the newly
	//  	resized block, adjust the position of each element to make way
	//  	for the newly inserted elements.
	//
	//Both methods have their merits. Technique 1) temporarily has 2 full
	//size memory blocks allocated simultaneously, but it only has to move
	//each element once to put it in the correct final position. Technique
	//2) may trasparently avoid expensive memory allocations by simply
	//adjusting the size of the current block allocation; but if it fails
	//to do so it will move all the elements to a new block while
	//maintaining their same index, after which this function will move the
	//elements to the appropriate index for the insertion.
	//
	//In effect, technique 1) is less memory efficient, but it only
	//accesses each element once. Technique 2) may have to access some
	//elements twice, but it also has the opportunity to completely avoid
	//most element movements and even entire memory allocations.
	//
	//With these design considerations taken into account, technique 2) is
	//the one selected for this implementation.
	//
	//TODO : If there were a version of #SK_MemIntf.retake () that allowed
	//for the newly inserted memory to be placed somewhere in the middle of
	//a memory block, the elements would only have to be accessed once, and
	//allocations can still be opportunistically avoided transparently.
	//That would make the above discussion obsolete, and be all-around the
	//better choice.
	//
	//(However, it does not seem possible to make such a request to realloc
	//() or reallocarray () in POSIX, raising the question of how such a
	//version of the function should be implemented).
	//
	//TODO : Since the dynamically allocated array portion of #ska is a
	//singular contiguous block at any given time, and this block contains
	//the unused positive slack (if there is any), mem->retake () will
	//needlessly copy the data in this unused positive slack to the new
	//memory allocation (if a new block is allocated). This exacerbates the
	//redundant element movements that occur when a reallocation actually
	//occurs using mem->retake ().

	size_t elem_size = req->stat_r->elem_size;


	if (realloc_needed)
	{
		//Perform the reallocation

		SlySr sr =

		req->stat_r->mem->retake
		(
			&dyna_array_ptr,

			new_dyna_elem_len,

			elem_size
		);


		SK_RES_RETURN (SK_ARRAY_L_DEBUG, sr.res, sr);


		//If the reallocation was successful, set #dyna_elem_ptr in
		//#ska (in case #dyna_array_ptr has been modified), and record
		//the new number of elements in the dynamically allocated
		//array.

		SK_Array_set_dyna_array_ptr (req, ska, dyna_array_ptr);


		dyna_elem_len = new_dyna_elem_len;

		SK_Array_set_dyna_elem_len (req, ska, dyna_elem_len);


		//Note, if (0 >= new_dyna_elem_len) is true, then the
		//reallocation calls for deallocating #dyna_array_ptr in #ska.
		//
		//Such a scenario can occur even though this is an insertion
		//operation that only adds elements. #dyna_array_ptr may have
		//been allocated followed later by a reduction in the number of
		//elements used in #ska, causing #dyna_array_ptr to become
		//unused. Also consider that the slack parameters contained in
		//#req may be changed over time.
		//
		//In this event, the above call to #req-stat_r->mem->retake ()
		//will safely deallocate the memory block.
	}


	//Read some values that will be frequently accessed in the next
	//sections

	size_t num_stat_elems =	req->stat_r->hint_num_stat_elems;

	size_t num_used_stat =	SK_MIN (num_stat_elems, old_num_elems);

	size_t num_used_dyna =	old_num_elems - num_used_stat;


	//Take notice of how this function is designed to move the
	//largest possible groups of elements at once, with as few separate
	//copy operations as possible.
	//
	//While this causes the indexing to be far more convoluted than a
	//simpler implementation that iterates element-by-element, this is far
	//more efficient in general. The technique used in this function allows
	//for the copying to take place with the largest possible word-sizes,
	//irrespective of the size of each element contained in #ska.


	//Determine the index ranges which will be inserted into the
	//statically / dynamically allocated arrays of #ska

	size_t stat_ins_beg =	0;

	size_t stat_ins_end =	0;

	size_t num_stat_ins =	0;

	//size_t flag_stat_ins = 0;

	if (ind < num_stat_elems)
	{
		stat_ins_beg =	ind;

		stat_ins_end =	SK_MIN (ind_end, num_stat_elems - 1);

		num_stat_ins =	(stat_ins_end - stat_ins_beg) + 1;

		//flag_stat_ins = 1;
	}


	size_t dyna_ins_beg =	0;

	size_t dyna_ins_end =	0;

	size_t num_dyna_ins =	0;

	size_t flag_dyna_ins =	0;

	if (ind_end >= num_stat_elems)
	{
		dyna_ins_beg =	SK_MAX (ind, num_stat_elems) - num_stat_elems;

		dyna_ins_end =	ind_end - num_stat_elems;

		num_dyna_ins =	(dyna_ins_end - dyna_ins_beg) + 1;

		flag_dyna_ins =	1;
	}


	// printf
	// (
	// 	"ind =           %zu\n"
	// 	"ind_end =       %zu\n"
	// 	"num_ins_elems = %zu\n",
	// 	ind,
	// 	ind_end,
	// 	num_ins_elems
	// );


	// printf
	// (
	// 	"stat_ins_beg = %zu\n"
	// 	"stat_ins_end = %zu\n"
	// 	"num_stat_ins = %zu\n",
	// 	stat_ins_beg,
	// 	stat_ins_end,
	// 	num_stat_ins
	// );

	// printf
	// (
	// 	"dyna_ins_beg = %zu\n"
	// 	"dyna_ins_end = %zu\n"
	// 	"num_dyna_ins = %zu\n",
	// 	dyna_ins_beg,
	// 	dyna_ins_end,
	// 	num_dyna_ins
	// );


	//Determine how many elements in the statically / dynamically allocated
	//arrays will have to be moved to a different position in the
	//statically / dynamically allocated arrays

	size_t num_dyna_to_dyna = 0;

	if (ind < old_num_elems)
	{
		num_dyna_to_dyna =

		SK_MIN ((old_num_elems - ind), num_used_dyna);
	}


	size_t num_stat_to_dyna = 0;

	if ((num_used_stat + num_stat_ins) > num_stat_elems)
	{
		num_stat_to_dyna =

		(num_used_stat + num_stat_ins) - num_stat_elems;
	}


	size_t num_stat_to_stat = 0;

	if (0 < num_stat_ins)
	{
		num_stat_to_stat =

		SK_MIN
		(
			(num_used_stat - stat_ins_beg),

			(num_stat_elems - (stat_ins_end + 1))
		);
	}


	// printf
	// (
	// 	"num_dyna_to_dyna = %zu\n"
	// 	"num_stat_to_dyna = %zu\n"
	// 	"num_stat_to_stat = %zu\n",
	// 	num_dyna_to_dyna,
	// 	num_stat_to_dyna,
	// 	num_stat_to_stat
	// );


	//Move elements in the dynamically allocated array that need a new
	//position within it.
	//
	//Note how SK_byte_move () is used instead of SK_byte_copy (), because
	//the index range for the final position may overlap the original index
	//range.

	if (num_dyna_to_dyna > 0)
	{
		SK_byte_move
		(
			((u8 *) dyna_array_ptr) + ((dyna_ins_end + num_stat_to_dyna + flag_dyna_ins) * elem_size),
			((u8 *) dyna_array_ptr) + (dyna_ins_beg * elem_size),
			(num_dyna_to_dyna * elem_size)
		);
	}


	//Move elements in the statically allocated array that need to go into
	//the dynamically allocated array.

	if (num_stat_to_dyna > 0)
	{
		SK_byte_move
		(
			((u8 *) dyna_array_ptr) + (num_dyna_ins * elem_size),
			((u8 *) stat_array_ptr) + ((stat_ins_beg + num_stat_to_stat) * elem_size),
			(num_stat_to_dyna * elem_size)
		);
	}


	//Move elements in the statically allocated array that need a new
	//position within it.

	if (num_stat_to_stat > 0)
	{
		SK_byte_move
		(
			((u8 *) stat_array_ptr) + ((stat_ins_end + 1) * elem_size),
			((u8 *) stat_array_ptr) + (stat_ins_beg * elem_size),
			(num_stat_to_stat * elem_size)
		);
	}


	//Indicate that #new_num_elems number of elements is now stored in #ska

	SK_Array_set_cur_num_elems (req, ska, new_num_elems);


	//At this point, all the elements in #ska have been properly resized
	//and the elements have been moved out of the way for the index range
	//[ind, (ind + num_ins_elems - 1)].
	//
	//Note that the elements in the index range
	//[ind, (ind + num_ins_elems - 1)] are still uninitialized at this
	//point in time.
	//
	//Since this is the *_unsafe () version of ins_range* (), the function
	//will now end without initializing these elements.


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.ins_range ()
 */

SlySr SK_Array_ins_range
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				num_elems,

	size_t				ind,

	SK_BoxIntf_elem_rw_func		elem_init_func,

	void *				arb_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Insert the element range [ind, (ind + (num_elems - 1)]

	SlySr sr =

	SK_Array_ins_range_unsafe (req, ska, num_elems, ind);


	SK_RES_RETURN (SK_ARRAY_L_DEBUG, sr.res, sr);


	//If #num_elems is 0, return early because there are no newly inserted
	//elements to initialize

	if (0 >= num_elems)
	{
		return SlySr_get (SLY_SUCCESS);
	}


	//Now it is necessary to initialize the newly inserted range of
	//elements, [ind, ind + (num_elems - 1)]

	if (NULL == elem_init_func)
	{
		elem_init_func = SK_Box_elem_rw_set_zero;
	}

	SK_Array_rw_elems
	(
		req,
		ska,
		ind,
		ind + (num_elems - 1),
		elem_init_func,
		arb_arg
	);


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.ins ()
 */

SlySr SK_Array_ins
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				ind,

	const void *			elem
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Insert the element at #ind

	SlySr sr =

	SK_Array_ins_range_unsafe (req, ska, 1, ind);


	SK_RES_RETURN (SK_ARRAY_L_DEBUG, sr.res, sr);


	//Set the value of the new element located at #ind

	if (NULL != elem)
	{
		SK_Array_set_elem (req, ska, ind, elem);
	}

	else
	{
		void * ins_elem = NULL;

		SK_Array_get_elem_ptr (req, ska, ind, &ins_elem);


		SK_byte_set_all (ins_elem, 0, req->stat_r->elem_size);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.ins_beg ()
 */

SlySr SK_Array_ins_beg
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	const void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Insert #elem at index 0 of #ska

	return SK_Array_ins (req, ska, 0, elem);
}




/*!
 *  See #SK_BoxIntf.ins_end ()
 */

SlySr SK_Array_ins_end
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	const void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));


	//Insert #elem at 1 above the highest index in #ska (which would be
	//equal to the number of elements)

	size_t cur_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


	return SK_Array_ins (req, ska, cur_num_elems, elem);
}




/*!
 *  See #SK_BoxIntf.rem_range ()
 */

SlyDr SK_Array_rem_range
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				num_rem_elems,

	size_t				ind
)
{
	//Sanity checks on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	//If #num_rem_elems is 0, exit early because no elements need to be
	//removed then

	if (0 >= num_rem_elems)
	{
		return SlyDr_get (SLY_SUCCESS);
	}


	//Determine the number of elements in #ska

	size_t old_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &old_num_elems);


	//Determine the index of the highest-indexed element being removed

	size_t ind_end = ind + (num_rem_elems - 1);

	if (ind_end < ind)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"(ind + (num_rem_elems - 1)) = (%zu + (%zu - 1)) "
			"overflows to %zu. Can not perform removal operation.",
			ind,
			num_rem_elems,
			ind_end
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Check if #ind_end is in the range [0, (old_num_elems - 1)] or not.

	if (ind_end > (old_num_elems - 1))
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"#ind_end = %zu is out of range.\n"
			"old_num_elems = %zu\n"
			"ind =           %zu\n"
			"num_rem_elems = %zu\n",
			ind_end,
			old_num_elems,
			ind,
			num_rem_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Determine the new number of elements that should be stored in #ska
	//
	//Note, this calculation should not overflow if the above argument
	//checks were performed correctly.

	size_t new_num_elems = old_num_elems - num_rem_elems;


	//Determine if a dynamic memory reallocation is actually necessary to
	//store #new_num_elems number of elements

	int realloc_needed =		0;

	size_t new_dyna_elem_len =	0;


	SK_Array_calc_new_dyna_elem_len
	(
		req,
		ska,
		new_num_elems,
		&realloc_needed,
		&new_dyna_elem_len
	);


	//Retrieve pointers for the statically / dynamically allocated arrays
	//associated with #ska.
	//
	//FIXME : Can be optimized by only retrieving pointers that will be
	//used, which is determined after the next sections

	void * stat_array_ptr = NULL;

	if (0 < req->stat_r->hint_num_stat_elems)
	{
		SK_Array_get_stat_elem_ptr (req, ska, 0, &stat_array_ptr);
	}


	void * dyna_array_ptr =	NULL;

	size_t dyna_elem_len =	0;


	SK_Array_get_dyna_array_ptr (req, ska, &dyna_array_ptr);

	SK_Array_get_dyna_elem_len (req, ska, &dyna_elem_len);


	//Take notice of how this function is designed to move the
	//largest possible groups of elements at once, with as few separate
	//copy operations as possible.
	//
	//While this causes the indexing to be far more convoluted than a
	//simpler implementation that iterates element-by-element, this is far
	//more efficient in general. The technique used in this function allows
	//for the copying to take place with the largest possible word-sizes,
	//irrespective of the size of each element contained in #ska.


	//Read some values that will be frequently accessed in the next
	//sections

	size_t elem_size =	req->stat_r->elem_size;

	size_t num_stat_elems =	req->stat_r->hint_num_stat_elems;

	size_t num_used_stat =	SK_MIN (num_stat_elems, old_num_elems);

	size_t num_used_dyna =	old_num_elems - num_used_stat;


	//Determine which index ranges will be removed from the statically /
	//dynamically allocated arrays of #ska.

	size_t stat_rem_beg =	0;

	size_t stat_rem_end =	0;

	size_t num_stat_rem =	0;

	//size_t flag_stat_rem = 0;

	if (ind < num_used_stat)
	{
		stat_rem_beg =	ind;

		stat_rem_end =	SK_MIN (ind_end, num_used_stat - 1);

		num_stat_rem =	(stat_rem_end - stat_rem_beg) + 1;

		//flag_stat_rem = 1;
	}


	size_t dyna_rem_beg =	0;

	size_t dyna_rem_end =	0;

	size_t num_dyna_rem =	0;

	size_t flag_dyna_rem =	0;

	if (ind_end >= num_stat_elems)
	{
		dyna_rem_beg =	SK_MAX (ind, num_stat_elems) - num_stat_elems;

		dyna_rem_end =	ind_end - num_stat_elems;

		num_dyna_rem =	(dyna_rem_end - dyna_rem_beg) + 1;

		flag_dyna_rem =	1;
	}


	// printf
	// (
	// 	"ind =           %zu\n"
	// 	"ind_end =       %zu\n"
	// 	"num_rem_elems = %zu\n",
	// 	ind,
	// 	ind_end,
	// 	num_rem_elems
	// );


	// printf
	// (
	// 	"stat_rem_beg = %zu\n"
	// 	"stat_rem_end = %zu\n"
	// 	"num_stat_rem = %zu\n",
	// 	stat_rem_beg,
	// 	stat_rem_end,
	// 	num_stat_rem
	// );

	// printf
	// (
	// 	"dyna_rem_beg = %zu\n"
	// 	"dyna_rem_end = %zu\n"
	// 	"num_dyna_rem = %zu\n",
	// 	dyna_rem_beg,
	// 	dyna_rem_end,
	// 	num_dyna_rem
	// );


	//Determine how many elements in the statically / dynamically allocated
	//arrays will have to be moved to a different position in the
	//statically / dynamically allocated arrays

	size_t num_stat_to_stat = 0;

	if (0 < num_stat_rem)
	{
		num_stat_to_stat = num_used_stat - (stat_rem_end + 1);
	}


	size_t num_dyna_to_stat = 0;

	if (0 < num_stat_rem)
	{
		num_dyna_to_stat =

		SK_MIN
		(
			(num_used_dyna - num_dyna_rem),

			(num_stat_elems - (num_used_stat - num_stat_rem))
		);
	}


	size_t num_dyna_to_dyna =

	SK_MIN
	(
		(num_used_dyna - num_dyna_to_stat),

		old_num_elems - (ind_end + 1)
	);


	// printf
	// (
	// 	"num_stat_to_stat = %zu\n"
	// 	"num_dyna_to_stat = %zu\n"
	// 	"num_dyna_to_dyna = %zu\n",
	// 	num_stat_to_stat,
	// 	num_dyna_to_stat,
	// 	num_dyna_to_dyna
	// );


	//Move elements in the statically allocated array that need a new
	//position within it.
	//
	//Note how SK_byte_move () is used instead of SK_byte_copy (), because
	//the index range for the final position may overlap the original index
	//range.

	if (num_stat_to_stat > 0)
	{
		SK_byte_move
		(
			((u8 *) stat_array_ptr) + (stat_rem_beg * elem_size),
			((u8 *) stat_array_ptr) + ((stat_rem_end + 1) * elem_size),
			(num_stat_to_stat * elem_size)
		);
	}


	//Move elements from the dynamically allocated array that need to go
	//into the statically allocated array.

	if (num_dyna_to_stat > 0)
	{
		SK_byte_move
		(
			((u8 *) stat_array_ptr) + ((stat_rem_beg + num_stat_to_stat) * elem_size),
			((u8 *) dyna_array_ptr) + (num_dyna_rem * elem_size),
			(num_dyna_to_stat * elem_size)
		);
	}


	//Move elements in the dynamically allocated array that need a new
	//position within it.

	if (num_dyna_to_dyna > 0)
	{
		SK_byte_move
		(
			((u8 *) dyna_array_ptr) + (dyna_rem_beg * elem_size),
			((u8 *) dyna_array_ptr) + ((dyna_rem_end + num_dyna_to_stat + flag_dyna_rem) * elem_size),
			(num_dyna_to_dyna * elem_size)
		);
	}


	//If a dynamic memory reallocation is necessary, attempt to perform it.
	//
	//(See the section in SK_Array_ins_range_unsafe () where the dynamic
	//memory reallocation is performed, as the same considerations apply
	//here.)

	if (realloc_needed)
	{
		//The reallocation calls for allocating a different sized
		//dynamically allocated array

		SlySr sr =

		req->stat_r->mem->retake
		(
			&dyna_array_ptr,

			new_dyna_elem_len,

			elem_size
		);

		//Note that SK_Array_rem_range () is deterministic and returns
		//a #SlyDr. In the event the above memory reallocation failed,
		//#mem->retake () will leave the dynamically allocated array at
		//#dyna_array_ptr unmodified.

		//If the reallocation was successful, set #dyna_elem_ptr in
		//#ska (in case #dyna_array_ptr has been modified), and record
		//the new number of elements in the dynamically allocated
		//array.

		if (SLY_SUCCESS == sr.res)
		{
			SK_Array_set_dyna_array_ptr (req, ska, dyna_array_ptr);


			dyna_elem_len = new_dyna_elem_len;

			SK_Array_set_dyna_elem_len (req, ska, dyna_elem_len);
		}


		//Note, if (0 >= new_dyna_elem_len) is true, then the
		//reallocation called for deallocating #dyna_array_ptr. In this
		//event, the above call to #req->stat_r->mem->retake () will
		//safely deallocate the memory block.
	}


	//Indicate that #new_num_elems number of elements is now stored in #ska

	SK_Array_set_cur_num_elems (req, ska, new_num_elems);


	//Note, unlike SK_Array_ins_range (), this removed elements. So there
	//are no new elements in need of initialization after this operation.

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.rem ()
 */

SlyDr SK_Array_rem
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				ind,

	void *				elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	//Determine the number of elements in #ska

	size_t cur_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


	if (SK_ARRAY_L_DEBUG)
	{
		//Check if #ind is in the range [0, (cur_num_elems - 1)] or not

		if (ind >= cur_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"#ind = %zu is out of range, "
				"#cur_num_elems = %zu",
				ind,
				cur_num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//If #elem is not NULL, retrieve the element at #ind before removing it

	if (NULL != elem)
	{
		SK_Array_get_elem (req, ska, ind, elem);
	}


	//Remove the element with index #ind from #ska

	SK_Array_rem_range (req, ska, 1, ind);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.rem_beg ()
 */

SlyDr SK_Array_rem_beg
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	void *			elem
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	size_t cur_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


	if (0 >= cur_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"Attempted to remove element from #ska when it "
			"contained no elements."
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	return

	SK_Array_rem
	(
		req,
		ska,
		0,
		elem
	);
}




/*!
 *  See #SK_BoxIntf.rem_end ()
 */

SlyDr SK_Array_rem_end
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));


	size_t cur_num_elems = 0;

	SK_Array_get_cur_num_elems (req, ska, &cur_num_elems);


	if (0 >= cur_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"Attempted to remove element from #ska when it "
			"contained no elements."
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	return

	SK_Array_rem
	(
		req,
		ska,
		(cur_num_elems - 1),
		elem
	);
}




/*!
 *  See SK_BoxIntf.get_anc_w_anc ()
 */

SlyDr SK_Array_get_anc_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			ind,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc
)
{
	//Anchors are not supported by #SK_Array. So here, #cur_anc is ignored
	//and the same actions as #SK_Array_get_anc () are taken.

	return SK_Array_get_anc (req, ska, ind, new_anc);
}




/*!
 *  See #SK_BoxIntf.get_region_ptr_w_anc ()
 */

SlyDr SK_Array_get_region_ptr_w_anc
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			prev_anc,

	SK_BoxAnc *			next_anc,

	void **				reg_ptr,

	size_t *			reg_beg,

	size_t *			reg_end
)
{

	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_get_region_ptr
	(
		req,
		ska,
		ind,
		reg_ptr,
		reg_beg,
		reg_end
	);
}




/*!
 *  See #SK_BoxIntf.get_elem_w_anc ()
 */

SlyDr SK_Array_get_elem_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			ind,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc,

	void *			elem
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_get_elem
	(
		req,
		ska,
		ind,
		elem
	);
}




/*!
 *  See #SK_BoxIntf.get_elem_ptr_w_anc ()
 */

SlyDr SK_Array_get_elem_ptr_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			ind,

	const SK_BoxAnc *	cur_anc,

	const SK_BoxAnc *	new_anc,

	void **			elem_ptr
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_get_elem_ptr
	(
		req,
		ska,
		ind,
		elem_ptr
	);
}




/*!
 *  See #SK_BoxIntf.set_elem_w_anc ()
 */

SlyDr SK_Array_set_elem_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			ind,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc,

	void *			elem
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_set_elem
	(
		req,
		ska,
		ind,
		elem
	);
}




/*!
 *  See #SK_BoxIntf.get_array_w_anc ()
 */

SlyDr SK_Array_get_array_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			beg_ind,

	size_t			end_ind,

	size_t			new_anc_ind,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc,

	size_t			array_len,

	void *			array
)
{

	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_get_array
	(
		req,
		ska,
		beg_ind,
		end_ind,
		array_len,
		array
	);
}




/*!
 *  See #SK_BoxIntf.set_array_w_anc ()
 */

SlyDr SK_Array_set_array_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			beg_ind,

	size_t			end_ind,

	size_t			new_anc_ind,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc,

	size_t			array_len,

	void *			array
)
{

	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_set_array
	(
		req,
		ska,
		beg_ind,
		end_ind,
		array_len,
		array
	);
}




/*!
 *  See #SK_BoxIntf.rw_elems_w_anc ()
 */

SlyDr SK_Array_rw_elems_w_anc
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				beg_ind,

	size_t				end_ind,

	size_t				new_anc_ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	SK_BoxIntf_elem_rw_func		rw_func,

	void *				arb_arg
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_rw_elems
	(
		req,
		ska,
		beg_ind,
		end_ind,
		rw_func,
		arb_arg
	);
}




/*!
 *  See #SK_BoxIntf.swap_w_anc ()
 */

SlyDr SK_Array_swap_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			ind_0,

	const SK_BoxAnc *	cur_anc_0,

	SK_BoxAnc *		new_anc_0,

	size_t			ind_1,

	const SK_BoxAnc *	cur_anc_1,

	SK_BoxAnc *		new_anc_1
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_swap
	(
		req,
		ska,
		ind_0,
		ind_1
	);
}




/*!
 *  See #SK_BoxIntf.swap_range_w_anc ()
 */

SlyDr SK_Array_swap_range_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			num_elems,

	size_t			ind_0,

	size_t			new_anc_0_ind,

	const SK_BoxAnc *	cur_anc_0,

	SK_BoxAnc *		new_anc_0,

	size_t			ind_1,

	size_t			new_anc_1_ind,

	const SK_BoxAnc *	cur_anc_1,

	SK_BoxAnc *		new_anc_1
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_swap_range
	(
		req,
		ska,
		num_elems,
		ind_0,
		ind_1
	);
}




/*!
 *  See #SK_BoxIntf.ins_w_anc ()
 */

SlySr SK_Array_ins_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			ind,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc,

	const void *		elem
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_ins
	(
		req,
		ska,
		ind,
		elem
	);
}




/*!
 *  See #SK_BoxIntf.ins_beg_w_anc ()
 */

SlySr SK_Array_ins_beg_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc,

	const void *		elem
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_ins_beg
	(
		req,
		ska,
		elem
	);
}




/*!
 *  See #SK_BoxIntf.ins_end_w_anc ()
 */

SlySr SK_Array_ins_end_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc,

	const void *		elem
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_ins_end
	(
		req,
		ska,
		elem
	);
}




/*!
 *  See #SK_BoxIntf.ins_range_w_anc ()
 */

SlySr SK_Array_ins_range_w_anc
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				num_elems,

	size_t				ind,

	size_t				new_anc_ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	SK_BoxIntf_elem_rw_func		elem_init_func,

	void *				arb_arg
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_ins_range
	(
		req,
		ska,
		num_elems,
		ind,
		elem_init_func,
		arb_arg
	);
}




/*!
 *  See #SK_BoxIntf.ins_range_unsafe_w_anc ()
 */

SlySr SK_Array_ins_range_unsafe_w_anc
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	size_t				num_elems,

	size_t				ind,

	size_t				new_anc_ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_ins_range_unsafe
	(
		req,
		ska,
		num_elems,
		ind
	);
}




/*!
 *  See #SK_BoxIntf.rem_w_anc ()
 */

SlyDr SK_Array_rem_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			ind,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc,

	void *			elem
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_rem
	(
		req,
		ska,
		ind,
		elem
	);
}




/*!
 *  See #SK_BoxIntf.rem_beg_w_anc ()
 */

SlyDr SK_Array_rem_beg_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc,

	void *			elem
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_rem_beg
	(
		req,
		ska,
		elem
	);
}




/*!
 *  See #SK_BoxIntf.rem_end_w_anc ()
 */

SlyDr SK_Array_rem_end_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc,

	void *			elem
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_rem_end
	(
		req,
		ska,
		elem
	);
}




/*!
 *  See #SK_BoxIntf.rem_range_w_anc ()
 */

SlyDr SK_Array_rem_range_w_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			num_elems,

	size_t			ind,

	size_t			new_anc_ind,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		new_anc
)
{
	//Anchors are not supported by #SK_Array, so use anchor-less version

	return

	SK_Array_rem_range
	(
		req,
		ska,
		num_elems,
		ind
	);
}




/*!
 *  See #SK_BoxIntf.get_region_ptr_by_anc ()
 */

SlyDr SK_Array_get_region_ptr_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	cur_anc,

	SK_BoxAnc *		prev_anc,

	SK_BoxAnc *		next_anc,

	void **			reg_ptr,

	size_t *		reg_beg,

	size_t *		reg_end
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, cur_anc, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.get_elem_ptr_by_anc ()
 */

SlyDr SK_Array_get_elem_ptr_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	anc,

	void **			elem_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.get_elem_by_anc ()
 */

SlyDr SK_Array_get_elem_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	anc,

	void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.set_elem_by_anc ()
 */

SlyDr SK_Array_set_elem_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	anc,

	const void *		elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.get_array_by_anc ()
 */

SlyDr SK_Array_get_array_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	beg_anc,

	const SK_BoxAnc *	end_anc,

	size_t			array_len,

	void *			array
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, beg_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, end_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, array, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.set_array_by_anc ()
 */

SlyDr SK_Array_set_array_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	beg_anc,

	const SK_BoxAnc *	end_anc,

	size_t			array_len,

	const void *		array
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, beg_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, end_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, array, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.rw_elems_by_anc ()
 */

SlyDr SK_Array_rw_elems_by_anc
(
	const SK_BoxReqs *		req,

	SK_Array *			ska,

	const SK_BoxAnc *		beg_anc,

	const SK_BoxAnc *		end_anc,

	SK_BoxIntf_elem_rw_func		rw_func,

	void *				arb_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, beg_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, end_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, rw_func, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.swap_by_anc ()
 */

SlyDr SK_Array_swap_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	const SK_BoxAnc *	anc_0,

	const SK_BoxAnc *	anc_1
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc_0, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc_1, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.swap_range_by_anc ()
 */

SlyDr SK_Array_swap_range_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	size_t			num_elems,

	const SK_BoxAnc *	anc_0,

	const SK_BoxAnc *	anc_1
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc_0, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc_1, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.ins_bef_by_anc ()
 */

SlySr SK_Array_ins_bef_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	SK_BoxAnc *		anc,

	const void *		elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, elem, SlySr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlySr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.ins_aft_by_anc ()
 */

SlySr SK_Array_ins_aft_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	SK_BoxAnc *		anc,

	const void *		elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, elem, SlySr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlySr_get (SLY_NO_IMPL);
}




/*!
 *  See #SK_BoxIntf.rem_by_anc ()
 */

SlyDr SK_Array_rem_by_anc
(
	const SK_BoxReqs *	req,

	SK_Array *		ska,

	SK_BoxAnc *		anc,

	void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, ska, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_Array, so the *_by_anc () functions
	//are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  Performs a simple test-bench on the #SK_Array set / get functions.
 *
 *  @return			Standard status code
 */

SlySr SK_Array_set_get_tb ()
{

	//Announce the test-bench starting

	SK_TB_BEG_PRINT (stdout);


	//Select constants that determine how many parameterizations to test

	const i64 BEG_DYNA_ELEM_LEN =				1;
	const i64 END_DYNA_ELEM_LEN =				4;
	const i64 INC_DYNA_ELEM_LEN =				1;

	const i64 BEG_HINT_NEG_SLACK =				0;
	const i64 END_HINT_NEG_SLACK =				2;
	const i64 INC_HINT_NEG_SLACK =				1;

	const i64 BEG_HINT_POS_SLACK =				0;
	const i64 END_HINT_POS_SLACK =				2;
	const i64 INC_HINT_POS_SLACK =				1;

	const i64 BEG_ELEM_SIZE =				1;
	const i64 END_ELEM_SIZE =				4;
	const i64 INC_ELEM_SIZE =				1;

	const i64 BEG_HINT_NUM_STAT_ELEMS =			0;
	const i64 END_HINT_NUM_STAT_ELEMS =			4;
	const i64 INC_HINT_NUM_STAT_ELEMS =			2;

	const i64 BEG_HINT_ELEMS_PER_BLOCK =			0;
	const i64 END_HINT_ELEMS_PER_BLOCK =			2;
	const i64 INC_HINT_ELEMS_PER_BLOCK =			1;

	const i64 BEG_HINT_USE_MAX_NUM_DYNA_BLOCKS =		0;
	const i64 END_HINT_USE_MAX_NUM_DYNA_BLOCKS =		1;
	const i64 INC_HINT_USE_MAX_NUM_DYNA_BLOCKS =		1;

	const i64 BEG_HINT_MAX_NUM_DYNA_BLOCKS =		0;
	const i64 END_HINT_MAX_NUM_DYNA_BLOCKS =		200;
	const i64 INC_HINT_MAX_NUM_DYNA_BLOCKS =		100;


	i64 dyna_elem_len =			BEG_DYNA_ELEM_LEN;

	i64 hint_neg_slack =			BEG_HINT_NEG_SLACK;

	i64 hint_pos_slack =			BEG_HINT_POS_SLACK;

	i64 elem_size =				BEG_ELEM_SIZE;

	i64 hint_num_stat_elems =		BEG_HINT_NUM_STAT_ELEMS;

	i64 hint_elems_per_block =		BEG_HINT_ELEMS_PER_BLOCK;

	i64 hint_max_num_dyna_blocks =		BEG_HINT_MAX_NUM_DYNA_BLOCKS;

	i64 hint_use_max_num_dyna_blocks =	BEG_HINT_USE_MAX_NUM_DYNA_BLOCKS;


	//Enter a test-loop

	int go =		1;

	int error_detected =	0;


	while (go)
	{

		//Break the loop in-between test iterations if a previous
		//iteration of this loop reported an error

		if (error_detected)
		{
			break;
		}


		//Announce the test iteration

		printf ("\n--------\n\n");


		//Choose arbitrary parameters for #SK_BoxReqs that will
		//describe the hints for the #SK_Array

		SK_BoxStatReqs srq =

		{
			.mem =			&SK_MemStd,

			.elem_size =		elem_size,

			.hint_num_stat_elems =	hint_num_stat_elems,

			.hint_elems_per_block =	hint_elems_per_block
		};


		SK_BoxDynaReqs drq =

		{
			.hint_neg_slack =		hint_neg_slack,

			.hint_pos_slack =		hint_pos_slack,

			.hint_use_max_num_dyna_blocks =	hint_use_max_num_dyna_blocks,

			.hint_max_num_dyna_blocks =	hint_max_num_dyna_blocks
		};


		SK_BoxStatImpl sim;

		SK_BoxDynaImpl dim;

		SK_BoxReqs req;


		SK_BoxReqs_prep (&req, &srq, &sim, &drq, &dim);


		SK_BoxReqs_print (&req, 2, stdout, SK_LineDeco_std ());

		printf ("\n\n");


		//The functions being tested in this test-bench are considered
		//prerequisites for implementing SK_Array_init (), so instead
		//of using that function to generate #SK_Array's, they will
		//have to be "manually" created here.


		//Determine the size of the #SK_Array

		size_t inst_stat_mem = 0;

		SK_Array_inst_stat_mem (&req, &inst_stat_mem);


		printf ("inst_stat_mem = %zu\n\n", inst_stat_mem);


		//Allocate memory for the #SK_Array

		SK_Array * ska = malloc (inst_stat_mem);

		if (NULL == ska)
		{

			SK_DEBUG_PRINT (1, "Failed to allocate #ska\n");

			error_detected = 1;

			break;
		}


		//Allocate memory for the dynamically-allocated elements in the
		//#SK_Array

		printf ("dyna_elem_len = %" PRIi64 "\n\n", dyna_elem_len);


		void * dyna_elem_ptr =

		malloc (dyna_elem_len * req.stat_r->elem_size);


		if (NULL == dyna_elem_ptr)
		{
			//Dellocate previously allocated memory

			free (ska);


			SK_DEBUG_PRINT (1, "Failed to allocate #dyna_elem_ptr\n");

			error_detected = 1;

			break;
		}


		//Determine an arbitrary value for the number of currently used
		//elements in

		size_t cur_num_elems =

		dyna_elem_len + req.stat_r->hint_num_stat_elems;


		//Set all the fields of the #SK_Array to arbitrary values while
		//respecting #req

		SK_Array_set_cur_num_elems (&req, ska, cur_num_elems);

		SK_Array_set_dyna_elem_len (&req, ska, dyna_elem_len);

		SK_Array_set_dyna_array_ptr (&req, ska, dyna_elem_ptr);


		//Attempt to retrieve all the fields in the #SK_Array that were
		//just set

		size_t fetched_cur_num_elems = 0;

		size_t fetched_dyna_elem_len = 0;

		void * fetched_dyna_elem_ptr = NULL;


		SK_Array_get_cur_num_elems (&req, ska, &fetched_cur_num_elems);

		SK_Array_get_dyna_elem_len (&req, ska, &fetched_dyna_elem_len);

		SK_Array_get_dyna_elem_ptr (&req, ska, 0, &fetched_dyna_elem_ptr);


		printf
		(
			"cur_num_elems = %15zu,    got %15zu\n"
			"dyna_elem_len = %15zu,    got %15zu\n"
			"dyna_elem_ptr = %15p,    got %15p\n",

			cur_num_elems,
			fetched_cur_num_elems,

			dyna_elem_len,
			fetched_dyna_elem_len,

			dyna_elem_ptr,
			fetched_dyna_elem_ptr
		);


		if
		(
			(fetched_cur_num_elems != cur_num_elems) ||
			(fetched_dyna_elem_len != dyna_elem_len) ||
			(fetched_dyna_elem_ptr != dyna_elem_ptr)
		)
		{
			error_detected = 1;
		}



		//Test if retrieving statically-allocated elements from the
		//#SK_Array works

		size_t stat_elem_offset = 0;

		SK_Array_offset_stat_elem (&req, &stat_elem_offset);


		void * stat_single_elem_ptr = NULL;


		for
		(
			size_t stat_elem_sel = 0;

			(stat_elem_sel < req.stat_r->hint_num_stat_elems);

			stat_elem_sel += 1
		)
		{

			//Get a pointer to a single statically-allocated
			//element

			SK_Array_get_stat_elem_ptr
			(
				&req,
				ska,
				stat_elem_sel,
				&stat_single_elem_ptr
			);


			//Check if the returned pointer is correct

			size_t elem_offset =

			(size_t) (stat_single_elem_ptr - (void *) ska);


			size_t calc_stat_elem_sel =

			(elem_offset - stat_elem_offset) / req.stat_r->elem_size;


			//printf
			//(
			//	"stat_elem_sel =      %zu\n"
			//	"calc_stat_elem_sel = %zu\n\n",
			//	stat_elem_sel,
			//	calc_stat_elem_sel
			//);


			if (stat_elem_sel != calc_stat_elem_sel)
			{

				SK_DEBUG_PRINT
				(
					1,
					"Failed to index statically-"
					"allocated elements correctly.\n"
				);

				error_detected = 1;
			}


			//Check if this element is accessible when the
			//#SK_Array is "viewed" as a contiguous array with
			//SK_Array_get_elem_ptr ()

			void * contig_single_elem_ptr = NULL;


			SK_Array_get_elem_ptr
			(
				&req,
				ska,
				stat_elem_sel,
				&contig_single_elem_ptr
			);


			if (contig_single_elem_ptr != stat_single_elem_ptr)
			{
				SK_DEBUG_PRINT
				(
					1,
					"Could not access statically-"
					"allocated element at %p via "
					"SK_Array_get_elem_ptr () correctly.\n"
					"contig_single_elem_ptr = %p",
					stat_single_elem_ptr,
					contig_single_elem_ptr
				);

				error_detected = 1;
			}
		}


		//Tests if retrieving dynamically-allocated elements from the
		//#SK_Array works

		void * dyna_single_elem_ptr = NULL;


		for
		(
			size_t dyna_elem_sel = 0;

			(dyna_elem_sel < dyna_elem_len);

			dyna_elem_sel += 1
		)
		{
			//Get a pointer to a single dynamically-allocated
			//element

			SK_Array_get_dyna_elem_ptr
			(
				&req,
				ska,
				dyna_elem_sel,
				&dyna_single_elem_ptr
			);


			//Check if the returned pointer is correct

			size_t elem_offset =

			(size_t) (dyna_single_elem_ptr - (void *) dyna_elem_ptr);


			size_t calc_dyna_elem_sel =

			elem_offset / req.stat_r->elem_size;


			//printf
			//(
			//	"dyna_elem_sel =      %zu\n"
			//	"calc_dyna_elem_sel = %zu\n\n",
			//	dyna_elem_sel,
			//	calc_dyna_elem_sel
			//);


			if (dyna_elem_sel != calc_dyna_elem_sel)
			{
				SK_DEBUG_PRINT
				(
					1,
					"Failed to index dynamically-"
					"allocated elements correctly.\n"
				);

				error_detected = 1;
			}


			//Check if this element is accessible when the
			//#SK_Array is "viewed" as a contiguous array with
			//SK_Array_get_elem_ptr ()

			void * contig_single_elem_ptr = NULL;


			SK_Array_get_elem_ptr
			(
				&req,
				ska,
				(dyna_elem_sel + req.stat_r->hint_num_stat_elems),
				&contig_single_elem_ptr
			);


			if (contig_single_elem_ptr != dyna_single_elem_ptr)
			{
				SK_DEBUG_PRINT
				(
				 	1,
					"Could not access statically-"
					"allocated element at %p via "
					"SK_Array_get_elem_ptr () correctly.\n"
					"contig_single_elem_ptr = %p",
					dyna_single_elem_ptr,
					contig_single_elem_ptr
				);

				error_detected = 1;
			}
		}


		//printf ("\n\n");


		//Deallocate the dynamically-allocated array in the #SK_Array

		free (dyna_elem_ptr);


		//Dellocate the #SK_Array

		free (ska);


		//Update the parameters to test in the next test-iteration with
		//a "pseudo-loop"

		int looped = 0;

		SK_inc_chain
		(
			&dyna_elem_len,
			1,
			BEG_DYNA_ELEM_LEN,
			END_DYNA_ELEM_LEN,
			INC_DYNA_ELEM_LEN,
			&looped
		);


		SK_inc_chain
		(
			&hint_neg_slack,
			looped,
			BEG_HINT_NEG_SLACK,
			END_HINT_NEG_SLACK,
			INC_HINT_NEG_SLACK,
			&looped
		);


		SK_inc_chain
		(
			&hint_pos_slack,
			looped,
			BEG_HINT_POS_SLACK,
			END_HINT_POS_SLACK,
			INC_HINT_POS_SLACK,
			&looped
		);


		SK_inc_chain
		(
			&elem_size,
			looped,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);


		SK_inc_chain
		(
			&hint_num_stat_elems,
			looped,
			BEG_HINT_NUM_STAT_ELEMS,
			END_HINT_NUM_STAT_ELEMS,
			INC_HINT_NUM_STAT_ELEMS,
			&looped
		);


		SK_inc_chain
		(
			&hint_elems_per_block,
			looped,
			BEG_HINT_ELEMS_PER_BLOCK,
			END_HINT_ELEMS_PER_BLOCK,
			INC_HINT_ELEMS_PER_BLOCK,
			&looped
		);


		SK_inc_chain
		(
			&hint_use_max_num_dyna_blocks,
			looped,
			BEG_HINT_USE_MAX_NUM_DYNA_BLOCKS,
			END_HINT_USE_MAX_NUM_DYNA_BLOCKS,
			INC_HINT_USE_MAX_NUM_DYNA_BLOCKS,
			&looped
		);


		SK_inc_chain
		(
			&hint_max_num_dyna_blocks,
			looped,
			BEG_HINT_MAX_NUM_DYNA_BLOCKS,
			END_HINT_MAX_NUM_DYNA_BLOCKS,
			INC_HINT_MAX_NUM_DYNA_BLOCKS,
			&looped
		);


		//End the if the "outer-most" parameter has looped

		if (looped)
		{
			go = 0;
		}
	}


	printf ("\n--------\n\n");


	SK_TB_END_PRINT
	(
		stdout,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Given an already initialized #SK_Array, performs an arbitrary series of
 *  resize operations on it for the purposes of testing. This is intended to be
 *  used inside of SK_Array_resize_tb ().
 *
 *  @param deco			An #SK_LineDeco that describes how to decorate
 *				what will be printed during this test-bench
 *
 *  @param req			The #SK_BoxReqs associated with #ska
 *
 *  @param ska			The #SK_Array to perform an arbitrary series of
 *				resizing operations on.
 *
 *				@warning
 *				#ska MUST already be initialized before being
 *				sent to this function!
 *
 *  @param num_elems_beg	The lowest number of elements that #ska should
 *				be resized to during this test-bench.
 *
 *  @param num_elems_end	The highest number of elements that #ska should
 *				be resized to during this test-bench.
 *
 *  @param num_elems_inc	The amount the number of elements should change
 *				by when it is updated for a new test iteration.
 *
 *  				Note that in this test bench, every number of
 *  				elements described by the arguments
 *  				(num_elems_beg, num_elems_end, num_elems_inc)
 *  				will be resized to / from every other number of
 *  				elements in that range, provided that
 *  				#use_rand_vals is false.
 *
 *  @param neg_slack_beg	The lowest value for negative slack that should
 *				be tested
 *
 *  @parma neg_slack_end	The highest value for negative slack that
 *  				should be tested
 *
 *  @param neg_slack_inc	The amount the negative slack should change by
 *				when it is updated for a new test iteration
 *
 *  @param pos_slack_beg	The lowest value for positive slack that should
 *				be tested
 *
 *  @parma pos_slack_end	The highest value for positive slack that
 *  				should be tested
 *
 *  @param pos_slack_inc	The amount the positive slack should change by
 *				when it is updated for a new test iteration
 *
 *  @param use_rand_vals	If this is true, then the above *_inc arguments
 *				will be ignored. Instead, values from the range
 *				described by the arguments [*_beg, *_end] will
 *				be used for the test parameters instead.
 *
 *  @param num_rand_tests	The number of randomly selected test iterations
 *				to perform. Note that that this value is only
 *				used when #use_rand_vals is true.
 *
 *  @return			Standard status code
 */

SlySr SK_Array_single_arb_resize_tb
(
	SK_LineDeco		deco,

	SK_BoxReqs *		req,
	SK_Array *		ska,

	i64			num_elems_beg,
	i64			num_elems_end,
	i64			num_elems_inc,

	i64			neg_slack_beg,
	i64			neg_slack_end,
	i64			neg_slack_inc,

	i64			pos_slack_beg,
	i64			pos_slack_end,
	i64			pos_slack_inc,

	bool			use_rand_vals,

	size_t			num_rand_tests
)
{

	//Announce the test-bench starting

	SK_TB_BEG_PRINT_DECO (stdout, deco);


	//Initialize the test parameterizations

	i64 num_elems_0 =			num_elems_beg;

	i64 num_elems_1 =			num_elems_beg;

	i64 neg_slack =				neg_slack_beg;

	i64 pos_slack =				pos_slack_beg;

	size_t test_num =			0;


	//This generates some contiguous sets of bytes that will be used by the
	//element initialization functions that will be passed into
	//SK_Array_resize ().

	const u8 ARB_ELEM_0_BYTE = 5;

	const u8 ARB_ELEM_1_BYTE = 13;


	u8 * arb_elem_0 = malloc (req->stat_r->elem_size);

	SK_NULL_RETURN (SK_ARRAY_L_DEBUG, arb_elem_0, SlySr_get (SLY_BAD_ALLOC));


	u8 * arb_elem_1 = malloc (req->stat_r->elem_size);

	if (NULL == arb_elem_1)
	{
		free (arb_elem_0);

		SK_NULL_RETURN
		(
			SK_ARRAY_L_DEBUG,
			arb_elem_1,
			SlySr_get (SLY_BAD_ALLOC)
		);
	}


	SK_byte_set_all (arb_elem_0, ARB_ELEM_0_BYTE, req->stat_r->elem_size);

	SK_byte_set_all (arb_elem_1, ARB_ELEM_1_BYTE, req->stat_r->elem_size);


	//Enter the test loop, in which a singular #SK_Array will be
	//continually resized

	int go =		1;

	int error_detected =	0;

	SK_BoxReqs new_req = *req;


	while (go)
	{

		//Make a version of #req that has a modified negative-slack /
		//positive-slack to match the current test parameters

		new_req.dyna_i->r.hint_neg_slack =			neg_slack;

		new_req.dyna_i->r.hint_pos_slack =			pos_slack;

		new_req.dyna_i->r.hint_use_max_num_dyna_blocks =	false;

		new_req.dyna_i->r.hint_max_num_dyna_blocks =		0;


		//Enter an inner-loop so that the test 1st runs with
		//#num_elems_0, and then runs with #num_elems_1 2nd.
		//
		//What is the purpose of this? By keeping #num_elems_0 separate
		//from #num_elems_1, the pattern of resize operations looks
		//like this...
		//
		// A_0 --> A_1 --> A_0 --> B_1 --> A_0 --> C_1 --> ...
		//
		//This helps ensure that every pair of (start_num_elems,
		//final_num_elems) in a given range is tested in this
		//test-bench.
		//
		//I.e., we don't just want to test resizing from A elements to
		//B elements, we also want to test resizing from B elements to
		//A elements.
		//
		//Why does the order of resize operations matter? Because is
		//negative-slack and positive-slack is respected, the number of
		//currently allocated blocks is taken into consideration for
		//how many blocks will be allocated next.

		const size_t NUM_SEL_NUM_ELEMS = 2;

		for
		(
			size_t num_elems_sel = 0;

			num_elems_sel < NUM_SEL_NUM_ELEMS;

			num_elems_sel += 1
		)
		{

			//Select the number of elements to resize to what to
			//use as the "arbitrary element" to pass to
			//SK_Box_elem_rw_set_w_arb_arg ()

			size_t num_elems =

			(num_elems_sel) ? (num_elems_0) : (num_elems_1);


			u8 * arb_elem =

			(num_elems_sel) ? (arb_elem_0) : (arb_elem_1);


			//Gather information about how many elements are stored
			//in #ska before resizing it

			size_t start_dyna_elem_len = 0;

			SK_Array_get_dyna_elem_len
			(
				&new_req,
				ska,
				&start_dyna_elem_len
			);


			size_t start_num_elems = 0;

			SK_Array_get_cur_num_elems
			(
				&new_req,
				ska,
				&start_num_elems
			);


			//Reset all of the elements in the #SK_Array to 0's, so
			//that it is easier to determine which elements are
			//modified by SK_Array_resize ()

			if (0 < start_num_elems)
			{
				SK_Array_rw_elems
				(
					&new_req,
					ska,

					0,
					(start_num_elems - 1),

					SK_Box_elem_rw_set_zero,
					NULL
				);
			}


			//Resize the #SK_Array to #num_elems

			SK_LineDeco_print_opt
			(
				stdout,
				deco,
				"--------\n"
			);


			SK_LineDeco_print_opt
			(
			 	stdout,
				deco,
				"Resizing #ska = %p from %zu "
				"elements to %zu\n",
				ska,
				start_num_elems,
				num_elems
			);


			SK_LineDeco_print_opt
			(
				stdout,
				deco,
				"neg_slack = %zu",
				neg_slack
			);


			SK_LineDeco_print_opt
			(
				stdout,
				deco,
				"pos_slack = %zu\n",
				pos_slack
			);


			SlySr sr =

			SK_Array_resize
			(
				&new_req,
				ska,

				num_elems,

				SK_Box_elem_rw_set_w_arb_arg,
				arb_elem
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT
				(
					SK_ARRAY_L_DEBUG,
					"Failed to resize #ska = %p "
					"to %zu elements.",
					ska,
					num_elems
				);

				error_detected = 1;

				break;
			}


			//Check if #cur_num_elems was changed correctly

			size_t cur_dyna_elem_len = 0;

			SK_Array_get_dyna_elem_len
			(
				&new_req,
				ska,
				&cur_dyna_elem_len
			);


			size_t cur_num_elems = 0;

			SK_Array_get_cur_num_elems
			(
				&new_req,
				ska,
				&cur_num_elems
			);


			if (cur_num_elems != num_elems)
			{
				SK_DEBUG_PRINT
				(
					SK_ARRAY_L_DEBUG,
					"#cur_num_elems = %zu when "
					"it should have been %zu",
					cur_num_elems,
					num_elems
				);

				error_detected = 1;

				break;
			}


			//Calculate the minimum number of dynamically-allocated
			//element blocks that could be used to store the number
			//of elements that #ska currently stores

			size_t elems_per_block =

			new_req.stat_r->hint_elems_per_block;


			if (0 >= elems_per_block)
			{
				elems_per_block = SK_ARRAY_DEF_ELEMS_PER_BLOCK;
			}


			size_t min_dyna_elem_len = 0;

			if (cur_num_elems > new_req.stat_r->hint_num_stat_elems)
			{
				min_dyna_elem_len =

				cur_num_elems - new_req.stat_r->hint_num_stat_elems;
			}


			size_t min_num_dyna_blocks =

			SK_ceil_div_u64
			(
				min_dyna_elem_len,
				elems_per_block
			);


			//Check if the number of dynamically-allocated blocks
			//matches what SK_Box_calc_new_blocks () would
			//select

			size_t start_num_dyna_blocks =

			SK_ceil_div_u64
			(
				start_dyna_elem_len,
				elems_per_block
			);


			size_t cur_num_dyna_blocks =

			SK_ceil_div_u64
			(
				cur_dyna_elem_len,
				elems_per_block
			);


			size_t correct_cur_num_dyna_blocks = 0;

			SK_Box_calc_new_num_blocks
			(
				&new_req,

				start_num_dyna_blocks,
				min_num_dyna_blocks,

				&correct_cur_num_dyna_blocks
			);


			if (correct_cur_num_dyna_blocks != cur_num_dyna_blocks)
			{
				SK_DEBUG_PRINT
				(
					SK_ARRAY_L_DEBUG,
					"#cur_num_dyna_blocks = %zu when "
					"it should have been %zu\n"
					"start_dyna_elem_len %zu\n"
					"cur_dyna_elem_len %zu\n",
					cur_num_dyna_blocks,
					correct_cur_num_dyna_blocks,
					start_dyna_elem_len,
					cur_dyna_elem_len
				);

				error_detected = 1;

				break;
			}


			//Check that all the elements that should have been
			//modified by SK_Array_resize () have been in fact
			//modified correctly.
			//
			//Note that these modified elements will only be
			//present in the case that #ska is resized to have a
			//larger number of elements

			for
			(
				size_t elem_sel = start_num_elems;

				elem_sel < cur_num_elems;

				elem_sel += 1
			)
			{

				//Get a pointer to the currently selected
				//element

				void * elem_ptr = NULL;

				SK_Array_get_elem_ptr
				(
					&new_req,
					ska,
					elem_sel,
					&elem_ptr
				);


				//Check if the element matches the "arbitrary
				//elemenet value" supplied to SK_Array_resize
				//()

				int is_equal =		0;

				size_t ineq_ind =	0;


				SK_byte_eq
				(
					arb_elem,
					new_req.stat_r->elem_size,

					elem_ptr,
					new_req.stat_r->elem_size,

					&is_equal,
					&ineq_ind
				);


				if (0 == is_equal)
				{
					SK_DEBUG_PRINT
					(
						SK_ARRAY_L_DEBUG,
						"Element #%zu in #ska = %p "
						"was set wrongly.\n"
						"arb_elem [%zu] = %" PRIu8 "\n"
						"elem_ptr [%zu] = %" PRIu8 "\n",
						elem_sel,
						ska,
						ineq_ind,
						* (((u8 *) arb_elem) + ineq_ind),
						ineq_ind,
						* (((u8 *) elem_ptr) + ineq_ind)
					);

					error_detected = 1;

					break;
				}

			}

		}

		//Propagate loop-breaking from the inner-loop if an error
		//occurred

		if (error_detected)
		{
			break;
		}


		//Keep count of how many tests have been performed

		test_num += 1;


		//Update the test parameters in a way that respects the
		//#use_rand_vals argument

		if (use_rand_vals)
		{
			num_elems_0 =

			SK_range_oflow_i64
			(
				SK_gold_hash_u64 ((test_num + 1) * 10),
				num_elems_beg,
				num_elems_end
			);


			num_elems_1 =

			SK_range_oflow_i64
			(
				SK_gold_hash_u64 ((test_num + 1) * 11),
				num_elems_beg,
				num_elems_end
			);


			neg_slack =

			SK_range_oflow_i64
			(
				SK_gold_hash_u64 ((test_num + 1) * 12),
				neg_slack_beg,
				neg_slack_end
			);


			pos_slack =

			SK_range_oflow_i64
			(
				SK_gold_hash_u64 ((test_num + 1) * 13),
				pos_slack_beg,
				pos_slack_end
			);


			//End the loop if enough random tests have been
			//performed

			if (test_num >= num_rand_tests)
			{
				go = 0;
			}

		}

		else
		{

			//Update the parameters to test in the next
			//test-iteration with a "pseudo-loop"

			int looped = 0;


			SK_inc_chain
			(
				&num_elems_0,
				1,
				num_elems_beg,
				num_elems_end,
				num_elems_inc,
				&looped
			);


			SK_inc_chain
			(
				&num_elems_1,
				looped,
				num_elems_beg,
				num_elems_end,
				num_elems_inc,
				&looped
			);


			SK_inc_chain
			(
				&neg_slack,
				looped,
				neg_slack_beg,
				neg_slack_end,
				neg_slack_inc,
				&looped
			);


			SK_inc_chain
			(
				&pos_slack,
				looped,
				pos_slack_beg,
				pos_slack_end,
				pos_slack_inc,
				&looped
			);


			//End the loop if the "outer-most" parameter has looped

			if (looped)
			{
				go = 0;
			}
		}
	}


	SK_LineDeco_print_opt
	(
		stdout,
		deco,
		"--------\n\n"
	);


	//Make sure that #hint_use_max_num_dyna_blocks and
	//#hint_max_num_dyna_blocks are respected by SK_Array_resize ().
	//
	//In the above test-loop, these hints were left unused.


	SK_LineDeco_print_opt
	(
		stdout,
		deco,
		"Intentionally attempting to allocate number of elements that "
		"cause too many blocks to be dynamically allocated. This "
		"SHOULD cause SK_Array_resize () to fail, so expect error "
		"messages here!\n\n\n\n"
	);


	new_req.dyna_i->r.hint_use_max_num_dyna_blocks =	true;


	size_t elems_per_block =

	new_req.stat_r->hint_elems_per_block;


	if (0 >= elems_per_block)
	{
		elems_per_block = SK_ARRAY_DEF_ELEMS_PER_BLOCK;
	}


	size_t cur_num_elems = 0;

	SK_Array_get_cur_num_elems (&new_req, ska, &cur_num_elems);


	new_req.dyna_i->r.hint_max_num_dyna_blocks =

	SK_ceil_div_u64 (cur_num_elems, elems_per_block);


	size_t over_max_num_elems =

	((new_req.dyna_i->r.hint_max_num_dyna_blocks +  1) * elems_per_block)

	+

	new_req.stat_r->hint_num_stat_elems;


	SlySr sr =

	SK_Array_resize
	(
		&new_req,
		ska,

		over_max_num_elems,

		SK_Box_elem_rw_set_w_arb_arg,
		arb_elem_0
	);


	if (SLY_SUCCESS == sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_ARRAY_L_DEBUG,
			"Resize operation succeeded when it should have "
			"failed!\n"
			"over_max_num_elems = %zu",
			over_max_num_elems
		);

		error_detected = 1;
	}


	//FIXME : Some code should be added here to ensure that after the
	//intentionally failed resize operation, the #SK_Array has not been
	//modified and furthermore it is still a valid #SK_Array.

	SK_LineDeco_print_opt
	(
		stdout,
		deco,
		"Intentionally failed resize operation complete.\n\n\n\n"
	);


	//Free the arbitrary elements that were created for initialization
	//purposes earlier in this function

	free (arb_elem_0);

	free (arb_elem_1);


	//Announce the test-bench ending

	SK_TB_END_PRINT_DECO
	(
		stdout,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Performs a simple test-bench on #SK_Array resizing.
 *
 *  Note that this test-bench initializes #SK_Array's in such a manner where
 *  SK_Array_init is not used, because SK_Array_init () uses SK_Array_resize ()
 *  interanlly.
 *
 *  @return			Standard status code
 */

SlySr SK_Array_resize_tb ()
{

	//Announce the test-bench starting

	SK_TB_BEG_PRINT (stdout);


	//Select constants that determine how many parameterizations to test

	const i64 BEG_ELEM_SIZE =				1;
	const i64 END_ELEM_SIZE =				4;
	const i64 INC_ELEM_SIZE =				1;

	const i64 BEG_HINT_NUM_STAT_ELEMS =			0;
	const i64 END_HINT_NUM_STAT_ELEMS =			4;
	const i64 INC_HINT_NUM_STAT_ELEMS =			2;

	const i64 BEG_HINT_ELEMS_PER_BLOCK =			0;
	const i64 END_HINT_ELEMS_PER_BLOCK =			2;
	const i64 INC_HINT_ELEMS_PER_BLOCK =			1;


	i64 elem_size =				BEG_ELEM_SIZE;

	i64 hint_num_stat_elems =		BEG_HINT_NUM_STAT_ELEMS;

	i64 hint_elems_per_block =		BEG_HINT_ELEMS_PER_BLOCK;


	//Select constants that will be used in the calls to
	//SK_Array_single_arb_resize_tb ()

	const i64 BEG_RESIZE_NUM_ELEMS =			0;
	const i64 END_RESIZE_NUM_ELEMS =			128;
	const i64 INC_RESIZE_NUM_ELEMS =			8;

	const i64 BEG_HINT_NEG_SLACK =				0;
	const i64 END_HINT_NEG_SLACK =				2;
	const i64 INC_HINT_NEG_SLACK =				1;

	const i64 BEG_HINT_POS_SLACK =				0;
	const i64 END_HINT_POS_SLACK =				2;
	const i64 INC_HINT_POS_SLACK =				1;


	//Enter the test loop, in which a singular #SK_Array will be
	//"manually" created with different static parameters, and then sent to
	//a test-bench for SK_Array_resize ()

	int go = 		1;

	int error_detected =	0;


	while (go)
	{

		//Break the loop in-between test iterations if a previous
		//iteration of this loop reported an error

		if (error_detected)
		{
			break;
		}


		//Print out information regarding this test iteration

		printf
		(
			"--------\n\n"
			"elem_size =            %zu\n"
			"hint_num_stat_elems =  %zu\n"
			"hint_elems_per_block = %zu\n\n",
			elem_size,
			hint_num_stat_elems,
			hint_elems_per_block
		);


		//Create a #SK_BoxReqs describing the parameters of the
		//#SK_Array

		SK_BoxStatReqs srq =

		{
			.mem =				&SK_MemStd,

			.elem_size =			elem_size,

			.hint_num_stat_elems =		hint_num_stat_elems,

			.hint_elems_per_block =		hint_elems_per_block
		};


		SK_BoxDynaReqs drq =

		{
			.hint_neg_slack =			0,

			.hint_pos_slack =			0,

			.hint_use_max_num_dyna_blocks =		0,

			.hint_max_num_dyna_blocks =		0
		};


		SK_BoxStatImpl sim;

		SK_BoxDynaImpl dim;

		SK_BoxReqs req;


		SK_BoxReqs_prep (&req, &srq, &sim, &drq, &dim);


		//Determine how much static memory this #SK_Array
		//parameterization would require, and then create the #SK_Array
		//"manually"

		size_t ska_inst_stat_mem = 0;

		SK_Array_inst_stat_mem (&req, &ska_inst_stat_mem);


		SK_Array * ska = malloc (ska_inst_stat_mem);

		if (NULL == ska)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"Failed to allocate #ska with size %zu",
				ska_inst_stat_mem
			);

			error_detected = 1;

			break;
		}


		SK_Array_set_cur_num_elems
		(
			&req,
			ska,
			0
		);


		SK_Array_set_dyna_elem_len
		(
			&req,
			ska,
			0
		);


		SK_Array_set_dyna_array_ptr
		(
			&req,
			ska,
			NULL
		);


		//Perform a test-bench for SK_Array_resize () with this
		//instantiation of an #SK_Array

		SK_LineDeco deco = SK_LineDeco_std ();

		deco.pre_str_cnt += 1;


		SlySr sr =

		SK_Array_single_arb_resize_tb
		(
			deco,

			&req,
			ska,

			BEG_RESIZE_NUM_ELEMS,
			END_RESIZE_NUM_ELEMS,
			INC_RESIZE_NUM_ELEMS,

			BEG_HINT_NEG_SLACK,
			END_HINT_NEG_SLACK,
			INC_HINT_NEG_SLACK,

			BEG_HINT_POS_SLACK,
			END_HINT_POS_SLACK,
			INC_HINT_POS_SLACK,

			0,
			0
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"Inner test-bench failed!"
			);

			error_detected = 1;
		}


		//Since #ska was "manually" constructed in this test, it is now
		//necessary to "manually" deconstruct, which includes
		//dellocating all the memory for elements it uses.

		void * elem_ptr = NULL;

		SK_Array_get_dyna_elem_ptr (&req, ska, 0, &elem_ptr);


		if (NULL != elem_ptr)
		{
			free (elem_ptr);
		}



		free (ska);


		//Update the parameters to test in the next test-iteration with
		//a "pseudo-loop"

		int looped = 0;

		SK_inc_chain
		(
			&elem_size,
			1,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);


		SK_inc_chain
		(
			&hint_num_stat_elems,
			looped,
			BEG_HINT_NUM_STAT_ELEMS,
			END_HINT_NUM_STAT_ELEMS,
			INC_HINT_NUM_STAT_ELEMS,
			&looped
		);


		SK_inc_chain
		(
			&hint_elems_per_block,
			looped,
			BEG_HINT_ELEMS_PER_BLOCK,
			END_HINT_ELEMS_PER_BLOCK,
			INC_HINT_ELEMS_PER_BLOCK,
			&looped
		);


		//End the loop if the "outer-most" parameter has looped

		if (looped)
		{
			go = 0;
		}
	}


	printf ("--------\n\n");


	//Announce the test-bench ending

	SK_TB_END_PRINT
	(
		stdout,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);


}




/*!
 *  Performs a simple test-bench on #SK_Array initialization /
 *  deinitialization.
 *
 *  @return			Standard status code
 */

SlySr SK_Array_init_tb ()
{

	//Announce the test-bench starting

	SK_TB_BEG_PRINT (stdout);


	//Select constants that determine how many parameterizations to test

	const i64 BEG_INIT_ELEM_INIT_FUNC_SEL =			0;
	const i64 END_INIT_ELEM_INIT_FUNC_SEL =			2;
	const i64 INC_INIT_ELEM_INIT_FUNC_SEL =			1;

	const i64 BEG_START_NUM_ELEMS =				0;
	const i64 END_START_NUM_ELEMS =				150;
	const i64 INC_START_NUM_ELEMS =				50;

	const i64 BEG_HINT_NEG_SLACK =				0;
	const i64 END_HINT_NEG_SLACK =				2;
	const i64 INC_HINT_NEG_SLACK =				1;

	const i64 BEG_HINT_POS_SLACK =				0;
	const i64 END_HINT_POS_SLACK =				2;
	const i64 INC_HINT_POS_SLACK =				1;

	const i64 BEG_ELEM_SIZE =				1;
	const i64 END_ELEM_SIZE =				4;
	const i64 INC_ELEM_SIZE =				1;

	const i64 BEG_HINT_NUM_STAT_ELEMS =			0;
	const i64 END_HINT_NUM_STAT_ELEMS =			4;
	const i64 INC_HINT_NUM_STAT_ELEMS =			2;

	const i64 BEG_HINT_ELEMS_PER_BLOCK =			0;
	const i64 END_HINT_ELEMS_PER_BLOCK =			2;
	const i64 INC_HINT_ELEMS_PER_BLOCK =			1;

	const i64 BEG_HINT_USE_MAX_NUM_DYNA_BLOCKS =		0;
	const i64 END_HINT_USE_MAX_NUM_DYNA_BLOCKS =		1;
	const i64 INC_HINT_USE_MAX_NUM_DYNA_BLOCKS =		1;

	const i64 BEG_HINT_MAX_NUM_DYNA_BLOCKS =		0;
	const i64 END_HINT_MAX_NUM_DYNA_BLOCKS =		200;
	const i64 INC_HINT_MAX_NUM_DYNA_BLOCKS =		100;


	i64 init_elem_init_func_sel =		BEG_INIT_ELEM_INIT_FUNC_SEL;

	i64 start_num_elems =			BEG_START_NUM_ELEMS;

	i64 hint_neg_slack =			BEG_HINT_NEG_SLACK;

	i64 hint_pos_slack =			BEG_HINT_POS_SLACK;

	i64 elem_size =				BEG_ELEM_SIZE;

	i64 hint_num_stat_elems =		BEG_HINT_NUM_STAT_ELEMS;

	i64 hint_elems_per_block =		BEG_HINT_ELEMS_PER_BLOCK;

	i64 hint_max_num_dyna_blocks =		BEG_HINT_MAX_NUM_DYNA_BLOCKS;

	i64 hint_use_max_num_dyna_blocks =	BEG_HINT_USE_MAX_NUM_DYNA_BLOCKS;


	//Select constants that will be used in the calls to
	//SK_Array_single_arb_resize_tb ()

	const i64 BEG_RESIZE_NUM_ELEMS =			0;
	const i64 END_RESIZE_NUM_ELEMS =			12;
	const i64 INC_RESIZE_NUM_ELEMS =			3;


	//Enter the test loop, in which #SK_Array's with different parameters
	//will be initialized and resized

	int go =		1;

	int error_detected =	0;


	while (go)
	{

		//Break the loop in-between test iterations if a previous
		//iteration of this loop reported an error

		if (error_detected)
		{
			break;
		}


		//Print out all the parameters that will be used in this test
		//iteration

		printf
		(
			"\n--------\n\n"
			"init_elem_init_func_sel =      %" PRIi64 "\n"
			"start_num_elems =              %" PRIi64 "\n"
			"hint_neg_slack =               %" PRIi64 "\n"
			"hint_pos_slack =               %" PRIi64 "\n"
			"elem_size =                    %" PRIi64 "\n"
			"hint_num_stat_elems =          %" PRIi64 "\n"
			"hint_elems_per_block =         %" PRIi64 "\n"
			"hint_max_num_dyna_blocks =     %" PRIi64 "\n"
			"hint_use_max_num_dyna_blocks = %" PRIi64 "\n\n",
			init_elem_init_func_sel,
			start_num_elems,
			hint_neg_slack,
			hint_pos_slack,
			elem_size,
			hint_num_stat_elems,
			hint_elems_per_block,
			hint_max_num_dyna_blocks,
			hint_use_max_num_dyna_blocks
		);


		//Create a #SK_BoxReqs describing the #SK_Array that
		//should created based on the parameters in this test iteration

		SK_BoxStatReqs srq =

		{
			.mem =			&SK_MemStd,

			.elem_size =		elem_size,

			.hint_num_stat_elems =	hint_num_stat_elems,

			.hint_elems_per_block =	hint_elems_per_block
		};


		SK_BoxDynaReqs drq =

		{
			.hint_neg_slack =		hint_neg_slack,

			.hint_pos_slack =		hint_pos_slack,

			.hint_use_max_num_dyna_blocks =	hint_use_max_num_dyna_blocks,

			.hint_max_num_dyna_blocks =	hint_max_num_dyna_blocks
		};


		SK_BoxStatImpl sim;

		SK_BoxDynaImpl dim;

		SK_BoxReqs req;


		SK_BoxReqs_prep (&req, &srq, &sim, &drq, &dim);


		//Select the function that will be used to initialize elements
		//during initialization of the #SK_Array

		SlyDr (* init_elem_init_func)
		(
			const SK_BoxReqs *	req,

			void *			elem_ptr,

			size_t			index,

			void *			arb_arg
		);


		if (0 == init_elem_init_func_sel)
		{
			init_elem_init_func = NULL;
		}

		else if (1 == init_elem_init_func_sel)
		{
			init_elem_init_func = SK_Box_elem_rw_set_zero;
		}

		else if (2 == init_elem_init_func_sel)
		{
			init_elem_init_func = SK_Box_elem_rw_set_w_arb_arg;
		}

		else
		{
			init_elem_init_func = NULL;
		}


		//The only element initialization function where #arb_arg
		//matters is SK_BasicsetIntf_elem_init_w_arb_arg (). So make
		//sure in these cases that the #arb_arg arguments point to
		//appropriate values.

		u8 arb_elem_0 [END_ELEM_SIZE];


		for (size_t byte_sel = 0; byte_sel < END_ELEM_SIZE; byte_sel += 1)
		{
			arb_elem_0 [byte_sel] = byte_sel;
		}


		void * init_arb_arg = NULL;

		if (SK_Box_elem_rw_set_w_arb_arg == init_elem_init_func)
		{
			init_arb_arg = arb_elem_0;
		}


		//Determine how much static memory needs to be allocated for
		//the #SK_Array instance, and then allocate it

		size_t ska_size = 0;

		SK_Array_inst_stat_mem (&req, &ska_size);


		SK_Array * ska = malloc (ska_size);


		if (NULL == ska)
		{
			SK_DEBUG_PRINT
			(
				1,
				"Could not allocate #ska.\n #ska_size = %zu",
				ska_size
			);

			error_detected = 1;

			break;
		}


		//Initialize the #SK_Array.
		//
		//Note how when if it successfully initialized,
		//#hint_use_max_num_dyna_blocks is compared to #start_num_elems
		//to see if it SHOULD have failed.

		SlySr sr =

		SK_Array_init
		(
			&req,
			ska,
			start_num_elems,
			init_elem_init_func,
			init_arb_arg
		);


		size_t max_allowed_num_elems =

		hint_max_num_dyna_blocks

		*

		(
			(hint_elems_per_block > 0)

			?

			hint_elems_per_block

			:

			SK_ARRAY_DEF_ELEMS_PER_BLOCK
		);


		if
		(
			hint_use_max_num_dyna_blocks

			&&

			(start_num_elems > max_allowed_num_elems)

			&&

			(SLY_SUCCESS != sr.res)
		)
		{
			//If reached here, this is actually an example of the
			//test-case succeeding. More elements than what were
			//allowable were requested, so SK_Array_init () failed.


			printf
			(
				"\n\n"
				"SK_Array_init () was CORRECT to report an "
				"error for this test iteration, because "
				"more dynamically-allocated blocks than "
				"#hint_max_num_dyna_blocks would have been "
				"required.\n"
			);


			SK_Array_deinit (&req, ska);

			free (ska);

			break;
		}

		else if
		(
			hint_use_max_num_dyna_blocks

			&&

			(start_num_elems > max_allowed_num_elems)

			&&

			(SLY_SUCCESS == sr.res)
		)
		{
			SK_DEBUG_PRINT
			(
				1,
				"#SK_Array was successfully created when it "
				"should not have been, because too many "
				"elements were requested.\n"
				"start_num_elems =       %zu\n"
				"max_allowed_num_elems = %zu\n",
				start_num_elems,
				max_allowed_num_elems
			);


			SK_Array_deinit (&req, ska);

			free (ska);


			error_detected = 1;

			break;
		}

		else if
		(
			(start_num_elems <= max_allowed_num_elems)

			&&

			(SLY_SUCCESS != sr.res)
		)
		{
			SK_DEBUG_PRINT
			(
				1,
				"Could not initialize the #SK_Array."
			);


			free (ska);


			error_detected = 1;

			break;
		}


		//Check if #cur_num_elems in the #SK_Array has been set
		//correctly

		size_t cur_num_elems = 0;

		SK_Array_get_cur_num_elems (&req, ska, &cur_num_elems);


		if (cur_num_elems != start_num_elems)
		{
			SK_DEBUG_PRINT
			(
				1,
				"#cur_num_elems is wrong.\n"
				"#cur_num_elems =   %zu\n"
				"#start_num_elems = %zu\n",
				cur_num_elems,
				start_num_elems
			);

			error_detected = 1;
		}


		//Check if the dynamically allocated array in the #SK_Array has
		//been allocated correctly

		size_t dyna_elem_len = 0;

		void * dyna_elem_ptr = NULL;


		SK_Array_get_dyna_elem_len (&req, ska, &dyna_elem_len);


		if (0 < dyna_elem_len)
		{
			SK_Array_get_dyna_elem_ptr (&req, ska, 0, &dyna_elem_ptr);
		}


		if (start_num_elems > hint_num_stat_elems)
		{

			//If reached here, then it was necessary to allocate
			//the dynamically-allocated array.
			//
			//Note how when the length of the dynamically-allocated
			//array is checked, the positive slack hint is taken
			//into account.

			size_t min_dyna_elem_len =

			(start_num_elems - hint_num_stat_elems);


			size_t correct_dyna_elem_len =

			min_dyna_elem_len;


			if (min_dyna_elem_len > 0)
			{
				//If #hint_elems_per_block happens to be 0,
				//treat it as if it were
				//#SK_ARRAY_DEF_ELEMS_PER_BLOCK

				if (0 >= hint_elems_per_block)
				{
					correct_dyna_elem_len +=

					SK_ARRAY_DEF_ELEMS_PER_BLOCK

					*

					hint_pos_slack;
				}

				else
				{
					correct_dyna_elem_len +=

					(hint_elems_per_block * hint_pos_slack);
				}
			}


			if (dyna_elem_len != correct_dyna_elem_len)
			{
				SK_DEBUG_PRINT
				(
					1,
					"#dyna_elem_len is wrong.\n"
					"#dyna_elem_len =         %zu\n"
					"#correct_dyna_elem_len = %zu\n",
					dyna_elem_len,
					correct_dyna_elem_len
				);

				error_detected = 1;
			}


			if (NULL == dyna_elem_ptr)
			{
				SK_DEBUG_PRINT
				(
					1,
					"#dyna_elem_ptr was NULL when it "
					"should have been allocated."
				);

				error_detected = 1;
			}
		}

		else
		{
			if ((0 != dyna_elem_len) || (NULL != dyna_elem_ptr))
			{
				SK_DEBUG_PRINT
				(
					1,
					"Dynamically-allocated array should "
					"not have been made."
					"dyna_elem_len = %zu\n"
					"dyna_elem_ptr = %p\n",
					dyna_elem_len,
					dyna_elem_ptr
				);
			}
		}


		//Check that the elements in #SK_Array were initialized
		//correctly

		for (size_t elem_sel = 0; elem_sel < cur_num_elems; elem_sel += 1)
		{

			//Get the current element

			void * elem_ptr = NULL;

			SK_Array_get_elem_ptr
			(
				&req,
				ska,
				elem_sel,
				&elem_ptr
			);


			//Determine what the correct element would have been

			u8 correct_bytes [END_ELEM_SIZE];

			if (NULL == init_elem_init_func)
			{
				SK_byte_set_all
				(
					correct_bytes,
					0,
					elem_size
				);
			}

			else
			{
				init_elem_init_func
				(
					&req,
					correct_bytes,
					elem_sel,
					init_arb_arg
				);
			}


			//Check if the current element matches what it actually
			//should have been

			int is_equal =		0;

			size_t ineq_ind =	0;


			SK_byte_eq
			(
				elem_ptr,
				elem_size,

				correct_bytes,
				elem_size,

				&is_equal,
				&ineq_ind
			);


			if (0 == is_equal)
			{
				SK_DEBUG_PRINT
				(
					1,
					"Element at index %zu was incorrect.",
					ineq_ind
				);


				//FIXME : Use a function to print byte-strings

				printf ("elem =          ");

				for (size_t b_sel = 0; b_sel < elem_size; b_sel += 1)
				{
					printf
					(
						"%" PRIu8,
						*(((u8 *) elem_ptr) + (elem_size - (b_sel + 1)))
					);
				}

				printf ("\n");


				printf ("correct_bytes = ");

				for (size_t b_sel = 0; b_sel < elem_size; b_sel += 1)
				{
					printf
					(
						"%" PRIu8,
						*(((u8 *) correct_bytes) + (elem_size - (b_sel + 1)))
					);
				}

				printf ("\n");


				error_detected = 1;


				break;
			}
		}

		if (error_detected)
		{
			break;
		}


		//Perform a small test-bench on #ska to make sure it can be
		//resized correctly

		SK_LineDeco deco = SK_LineDeco_std ();

		deco.pre_str_cnt += 1;


		sr =

		SK_Array_single_arb_resize_tb
		(
			deco,

			&req,
			ska,

			BEG_RESIZE_NUM_ELEMS,
			END_RESIZE_NUM_ELEMS,
			INC_RESIZE_NUM_ELEMS,

			req.dyna_i->r.hint_neg_slack,
			req.dyna_i->r.hint_neg_slack,
			1,

			req.dyna_i->r.hint_pos_slack,
			req.dyna_i->r.hint_pos_slack,
			1,

			0,
			0
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_ARRAY_L_DEBUG,
				"Inner test-bench failed!"
			);

			error_detected = 1;
		}


		//Deinitialize the #SK_Array, which will deallocate any memory
		//it "owns"

		SK_Array_deinit (&req, ska);


		//Deallocate the static memory used for the #SK_Array instance

		free (ska);

		//Update the parameters to test in the next test-iteration with
		//a "pseudo-loop"

		int looped = 0;


		SK_inc_chain
		(
			&init_elem_init_func_sel,
			1,
			BEG_INIT_ELEM_INIT_FUNC_SEL,
			END_INIT_ELEM_INIT_FUNC_SEL,
			INC_INIT_ELEM_INIT_FUNC_SEL,
			&looped
		);


		SK_inc_chain
		(
			&start_num_elems,
			looped,
			BEG_START_NUM_ELEMS,
			END_START_NUM_ELEMS,
			INC_START_NUM_ELEMS,
			&looped
		);


		SK_inc_chain
		(
			&hint_neg_slack,
			looped,
			BEG_HINT_NEG_SLACK,
			END_HINT_NEG_SLACK,
			INC_HINT_NEG_SLACK,
			&looped
		);


		SK_inc_chain
		(
			&hint_pos_slack,
			looped,
			BEG_HINT_POS_SLACK,
			END_HINT_POS_SLACK,
			INC_HINT_POS_SLACK,
			&looped
		);


		SK_inc_chain
		(
			&elem_size,
			looped,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);


		SK_inc_chain
		(
			&hint_num_stat_elems,
			looped,
			BEG_HINT_NUM_STAT_ELEMS,
			END_HINT_NUM_STAT_ELEMS,
			INC_HINT_NUM_STAT_ELEMS,
			&looped
		);


		SK_inc_chain
		(
			&hint_elems_per_block,
			looped,
			BEG_HINT_ELEMS_PER_BLOCK,
			END_HINT_ELEMS_PER_BLOCK,
			INC_HINT_ELEMS_PER_BLOCK,
			&looped
		);


		SK_inc_chain
		(
			&hint_use_max_num_dyna_blocks,
			looped,
			BEG_HINT_USE_MAX_NUM_DYNA_BLOCKS,
			END_HINT_USE_MAX_NUM_DYNA_BLOCKS,
			INC_HINT_USE_MAX_NUM_DYNA_BLOCKS,
			&looped
		);


		SK_inc_chain
		(
			&hint_max_num_dyna_blocks,
			looped,
			BEG_HINT_MAX_NUM_DYNA_BLOCKS,
			END_HINT_MAX_NUM_DYNA_BLOCKS,
			INC_HINT_MAX_NUM_DYNA_BLOCKS,
			&looped
		);


		//End the loop if the "outer-most" parameter has looped

		if (looped)
		{
			go = 0;
		}
	}


	printf ("\n--------\n\n");


	//Announce the test-bench ending

	SK_TB_END_PRINT
	(
		stdout,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




//FIXME : Is there a shorter way to instantiate this?

const SK_BoxIntf SK_Array_intf =

{
	.inst_stat_mem =

	(SlyDr (*) (const SK_BoxReqs *, size_t *)) SK_Array_inst_stat_mem,


	.inst_dyna_mem =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t *))

	SK_Array_inst_dyna_mem,


	.need_req_regi =

	(SlyDr (*) (const SK_BoxReqs *, int *))

	SK_Array_need_req_regi,


	.init =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_Array_init,


	.init_unsafe =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t
		)
	)

	SK_Array_init_unsafe,


	.deinit =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *)) SK_Array_deinit,


	.get_num_elems =

	(SlyDr (*) (const SK_BoxReqs *, const SK_Box *, size_t *))

	SK_Array_get_cur_num_elems,


	.get_resize_limits =

	(SlyDr (*) (const SK_BoxReqs *, int *, size_t *, size_t *))

	SK_Array_get_resize_limits,


	.resize =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_Array_resize,


	.resize_unsafe =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t
		)
	)

	SK_Array_resize_unsafe,


	.minify =

	(SlySr (*) (const SK_BoxReqs *, SK_Box *))

	SK_Array_minify,


	.optimize =

	(SlySr (*) (const SK_BoxReqs *, SK_Box *, double, double))

	SK_Array_optimize,


	.anc_support =

	(SlyDr (*) (const SK_BoxReqs *, int *))

	SK_Array_anc_support,


	.anc_recommend =

	(SlyDr (*) (const SK_BoxReqs *, const SK_Box *, int *))

	SK_Array_anc_recommend,


	.anc_size =

	(SlyDr (*) (const SK_BoxReqs *, size_t *))

	SK_Array_anc_size,


	.anc_validate =

	(SlyDr (*) (const SK_BoxReqs *, const SK_Box *, const SK_BoxAnc *, size_t *, int *))

	SK_Array_anc_validate,


	.anc_ind =

	(SlyDr (*) (const SK_BoxReqs *, const SK_Box *, const SK_BoxAnc *, size_t *))

	SK_Array_anc_ind,


	.anc_inc =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, SK_BoxAnc *))

	SK_Array_anc_inc,


	.anc_dec =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, SK_BoxAnc *))

	SK_Array_anc_dec,


	.get_anc =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, SK_BoxAnc *))

	SK_Array_get_anc,


	.region_avg_len =

	(SlyDr (*) (const SK_BoxReqs *, const SK_Box *, double *))

	SK_Array_region_avg_len,


	.get_region_ptr =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			void **,

			size_t *,

			size_t *
		)
	)

	SK_Array_get_region_ptr,


	.get_elem_ptr =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, void **))

	SK_Array_get_elem_ptr,


	.get_elem =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, void *))

	SK_Array_get_elem,


	.set_elem =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, const void *))

	SK_Array_set_elem,


	.get_array =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, size_t, size_t, void *))

	SK_Array_get_array,


	.set_array =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, size_t, size_t, void *))

	SK_Array_set_array,


	.rw_elems =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_Array_rw_elems,


	.swap =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t
		)
	)

	SK_Array_swap,


	.swap_range =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t
		)
	)


	SK_Array_swap_range,


	.ins =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const void *
		)
	)

	SK_Array_ins,


	.ins_beg =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const void *
		)
	)

	SK_Array_ins_beg,


	.ins_end =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const void *
		)
	)

	SK_Array_ins_end,


	.ins_range =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_Array_ins_range,


	.ins_range_unsafe =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t
		)
	)

	SK_Array_ins_range_unsafe,


	.rem =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			void *
		)
	)

	SK_Array_rem,


	.rem_beg =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			void *
		)
	)

	SK_Array_rem_beg,


	.rem_end =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			void *
		)
	)

	SK_Array_rem_end,


	.rem_range =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t
		)
	)

	SK_Array_rem_range,


	.get_anc_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *
		)
	)

	SK_Array_get_anc_w_anc,


	.get_region_ptr_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			SK_BoxAnc *,

			void **,

			size_t *,

			size_t *
		)
	)

	SK_Array_get_region_ptr_w_anc,


	.get_elem_ptr_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			void **
		)
	)

	SK_Array_get_elem_ptr_w_anc,


	.get_elem_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			void *
		)
	)

	SK_Array_get_elem_w_anc,


	.set_elem_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_Array_set_elem_w_anc,


	.get_array_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			size_t,

			void *
		)
	)

	SK_Array_get_array_w_anc,


	.set_array_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			size_t,

			const void *
		)
	)

	SK_Array_set_array_w_anc,


	.rw_elems_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_Array_rw_elems_w_anc,


	.swap_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *
		)
	)

	SK_Array_swap_w_anc,


	.swap_range_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *
		)
	)


	SK_Array_swap_range_w_anc,


	.ins_w_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_Array_ins_w_anc,


	.ins_beg_w_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_Array_ins_beg_w_anc,


	.ins_end_w_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_Array_ins_end_w_anc,


	.ins_range_w_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_Array_ins_range_w_anc,


	.ins_range_unsafe_w_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *
		)
	)

	SK_Array_ins_range_unsafe_w_anc,


	.rem_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			void *
		)
	)

	SK_Array_rem_w_anc,


	.rem_beg_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			void *
		)
	)

	SK_Array_rem_beg_w_anc,


	.rem_end_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			void *
		)
	)

	SK_Array_rem_end_w_anc,


	.rem_range_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *
		)
	)

	SK_Array_rem_range_w_anc,


	.get_region_ptr_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			SK_BoxAnc *,

			void **,

			size_t *,

			size_t *
		)
	)

	SK_Array_get_region_ptr_by_anc,


	.get_elem_ptr_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			void **
		)
	)

	SK_Array_get_elem_ptr_by_anc,


	.get_elem_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			void *
		)
	)

	SK_Array_get_elem_by_anc,


	.set_elem_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			const void *
		)
	)

	SK_Array_set_elem_by_anc,


	.get_array_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			const SK_BoxAnc *,

			size_t,

			void *
		)
	)

	SK_Array_get_array_by_anc,


	.set_array_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			const SK_BoxAnc *,

			size_t,

			const void *
		)
	)

	SK_Array_set_array_by_anc,


	.rw_elems_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			const SK_BoxAnc *,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_Array_rw_elems_by_anc,


	.swap_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			const SK_BoxAnc *
		)
	)

	SK_Array_swap_by_anc,


	.swap_range_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			const SK_BoxAnc *
		)
	)

	SK_Array_swap_range_by_anc,


	.ins_bef_by_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_Array_ins_bef_by_anc,


	.ins_aft_by_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_Array_ins_aft_by_anc,


	.rem_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			SK_BoxAnc *,

			void *
		)
	)

	SK_Array_rem_by_anc
};




//FIXME : Turn this into a template for test-benches of some sort


// /*!
//  *  Performs a simple test-bench on #SK_Array initialization /
//  *  deinitialization
//  *
//  *  @return			Standard status code
//  */
//
// SlySr SK_Array_init_tb ()
// {
//
// 	//Announce the test-bench starting
//
// 	SK_TB_BEG_PRINT ();
//
//
// 	//Select constants that determine how many parameterizations to test
//
// 	const i64 BEG_INIT_ELEM_INIT_FUNC_SEL =			0;
// 	const i64 END_INIT_ELEM_INIT_FUNC_SEL =			2;
// 	const i64 INC_INIT_ELEM_INIT_FUNC_SEL =			1;
//
// 	const i64 BEG_RESIZE_ELEM_INIT_FUNC_SEL =		0;
// 	const i64 END_RESIZE_ELEM_INIT_FUNC_SEL =		2;
// 	const i64 INC_RESIZE_ELEM_INIT_FUNC_SEL =		1;
//
// 	const i64 BEG_START_NUM_ELEMS =				0;
// 	const i64 END_START_NUM_ELEMS =				150;
// 	const i64 INC_START_NUM_ELEMS =				50;
//
// 	const i64 BEG_RESIZE_NUM_ELEMS =			0;
// 	const i64 END_RESIZE_NUM_ELEMS =			150;
// 	const i64 INC_RESIZE_NUM_ELEMS =			50;
//
// 	const i64 BEG_HINT_NEG_SLACK =				0;
// 	const i64 END_HINT_NEG_SLACK =				2;
// 	const i64 INC_HINT_NEG_SLACK =				1;
//
// 	const i64 BEG_HINT_POS_SLACK =				0;
// 	const i64 END_HINT_POS_SLACK =				2;
// 	const i64 INC_HINT_POS_SLACK =				1;
//
// 	const i64 BEG_ELEM_SIZE =				1;
// 	const i64 END_ELEM_SIZE =				4;
// 	const i64 INC_ELEM_SIZE =				1;
//
// 	const i64 BEG_HINT_NUM_STAT_ELEMS =			0;
// 	const i64 END_HINT_NUM_STAT_ELEMS =			4;
// 	const i64 INC_HINT_NUM_STAT_ELEMS =			2;
//
// 	const i64 BEG_HINT_ELEMS_PER_BLOCK =			0;
// 	const i64 END_HINT_ELEMS_PER_BLOCK =			2;
// 	const i64 INC_HINT_ELEMS_PER_BLOCK =			1;
//
// 	const i64 BEG_HINT_USE_MAX_NUM_DYNA_BLOCKS =		0;
// 	const i64 END_HINT_USE_MAX_NUM_DYNA_BLOCKS =		1;
// 	const i64 INC_HINT_USE_MAX_NUM_DYNA_BLOCKS =		1;
//
// 	const i64 BEG_HINT_MAX_NUM_DYNA_BLOCKS =		0;
// 	const i64 END_HINT_MAX_NUM_DYNA_BLOCKS =		200;
// 	const i64 INC_HINT_MAX_NUM_DYNA_BLOCKS =		100;
//
//
// 	i64 init_elem_init_func_sel =		BEG_INIT_ELEM_INIT_FUNC_SEL;
//
// 	i64 resize_elem_init_func_sel =		BEG_RESIZE_ELEM_INIT_FUNC_SEL;
//
// 	i64 start_num_elems =			BEG_START_NUM_ELEMS;
//
// 	i64 resize_num_elems =			BEG_RESIZE_NUM_ELEMS;
//
// 	i64 hint_neg_slack =			BEG_HINT_NEG_SLACK;
//
// 	i64 hint_pos_slack =			BEG_HINT_POS_SLACK;
//
// 	i64 elem_size =				BEG_ELEM_SIZE;
//
// 	i64 hint_num_stat_elems =		BEG_HINT_NUM_STAT_ELEMS;
//
// 	i64 hint_elems_per_block =		BEG_HINT_ELEMS_PER_BLOCK;
//
// 	i64 hint_max_num_dyna_blocks =		BEG_HINT_MAX_NUM_DYNA_BLOCKS;
//
// 	i64 hint_use_max_num_dyna_blocks =	BEG_HINT_USE_MAX_NUM_DYNA_BLOCKS;
//
//
// 	//Enter the test loop, in which #SK_Array's with different parameters
// 	//will be initialized and resized
//
// 	int go =		1;
//
// 	int error_detected =	0;
//
//
// 	while (go)
// 	{
//
// 		//Break the loop in-between test iterations if a previous
// 		//iteration of this loop reported an error
//
// 		if (error_detected)
// 		{
// 			break;
// 		}
//
//
// 		//Print out all the parameters that will be used in this test
// 		//iteration
//
// 		printf
// 		(
// 			"\n--------\n\n"
// 			"init_elem_init_func_sel =      %" PRIi64 "\n"
// 			"resize_elem_init_func_sel =    %" PRIi64 "\n"
// 			"start_num_elems =              %" PRIi64 "\n"
// 			"resize_num_elems =             %" PRIi64 "\n"
// 			"hint_neg_slack =               %" PRIi64 "\n"
// 			"hint_pos_slack =               %" PRIi64 "\n"
// 			"elem_size =                    %" PRIi64 "\n"
// 			"hint_num_stat_elems =          %" PRIi64 "\n"
// 			"hint_elems_per_block =         %" PRIi64 "\n"
// 			"hint_max_num_dyna_blocks =     %" PRIi64 "\n"
// 			"hint_use_max_num_dyna_blocks = %" PRIi64 "\n",
// 			init_elem_init_func_sel,
// 			resize_elem_init_func_sel,
// 			start_num_elems,
// 			resize_num_elems,
// 			hint_neg_slack,
// 			hint_pos_slack,
// 			elem_size,
// 			hint_num_stat_elems,
// 			hint_elems_per_block,
// 			hint_max_num_dyna_blocks,
// 			hint_use_max_num_dyna_blocks
// 		);
//
//
// 		//FIXME : WAS LAST WRITING HERE
//
//
// 		//Update the parameters to test in the next test-iteration with
// 		//a "pseudo-loop"
//
// 		int looped = 0;
//
//
// 		SK_inc_chain
// 		(
// 			&init_elem_init_func_sel,
// 			1,
// 			BEG_INIT_ELEM_INIT_FUNC_SEL,
// 			END_INIT_ELEM_INIT_FUNC_SEL,
// 			INC_INIT_ELEM_INIT_FUNC_SEL,
// 			&looped
// 		);
//
//
// 		SK_inc_chain
// 		(
// 			&resize_elem_init_func_sel,
// 			looped,
// 			BEG_RESIZE_ELEM_INIT_FUNC_SEL,
// 			END_RESIZE_ELEM_INIT_FUNC_SEL,
// 			INC_RESIZE_ELEM_INIT_FUNC_SEL,
// 			&looped
// 		);
//
//
// 		SK_inc_chain
// 		(
// 			&start_num_elems,
// 			looped,
// 			BEG_START_NUM_ELEMS,
// 			END_START_NUM_ELEMS,
// 			INC_START_NUM_ELEMS,
// 			&looped
// 		);
//
//
// 		SK_inc_chain
// 		(
// 			&resize_num_elems,
// 			looped,
// 			BEG_RESIZE_NUM_ELEMS,
// 			END_RESIZE_NUM_ELEMS,
// 			INC_RESIZE_NUM_ELEMS,
// 			&looped
// 		);
//
//
// 		SK_inc_chain
// 		(
// 			&hint_neg_slack,
// 			looped,
// 			BEG_HINT_NEG_SLACK,
// 			END_HINT_NEG_SLACK,
// 			INC_HINT_NEG_SLACK,
// 			&looped
// 		);
//
//
// 		SK_inc_chain
// 		(
// 			&hint_pos_slack,
// 			looped,
// 			BEG_HINT_POS_SLACK,
// 			END_HINT_POS_SLACK,
// 			INC_HINT_POS_SLACK,
// 			&looped
// 		);
//
//
// 		SK_inc_chain
// 		(
// 			&elem_size,
// 			looped,
// 			BEG_ELEM_SIZE,
// 			END_ELEM_SIZE,
// 			INC_ELEM_SIZE,
// 			&looped
// 		);
//
//
// 		SK_inc_chain
// 		(
// 			&hint_num_stat_elems,
// 			looped,
// 			BEG_HINT_NUM_STAT_ELEMS,
// 			END_HINT_NUM_STAT_ELEMS,
// 			INC_HINT_NUM_STAT_ELEMS,
// 			&looped
// 		);
//
//
// 		SK_inc_chain
// 		(
// 			&hint_elems_per_block,
// 			looped,
// 			BEG_HINT_ELEMS_PER_BLOCK,
// 			END_HINT_ELEMS_PER_BLOCK,
// 			INC_HINT_ELEMS_PER_BLOCK,
// 			&looped
// 		);
//
//
// 		SK_inc_chain
// 		(
// 			&hint_use_max_num_dyna_blocks,
// 			looped,
// 			BEG_HINT_USE_MAX_NUM_DYNA_BLOCKS,
// 			END_HINT_USE_MAX_NUM_DYNA_BLOCKS,
// 			INC_HINT_USE_MAX_NUM_DYNA_BLOCKS,
// 			&looped
// 		);
//
//
// 		SK_inc_chain
// 		(
// 			&hint_max_num_dyna_blocks,
// 			looped,
// 			BEG_HINT_MAX_NUM_DYNA_BLOCKS,
// 			END_HINT_MAX_NUM_DYNA_BLOCKS,
// 			INC_HINT_MAX_NUM_DYNA_BLOCKS,
// 			&looped
// 		);
//
//
// 		//End the if the "outer-most" parameter has looped
//
// 		if (looped)
// 		{
// 			go = 0;
// 		}
// 	}
//
//
// 	//Announce the test-bench ending
//
// 	SK_TB_END_PRINT
// 	(
// 		error_detected,
// 		SlySr_get (SLY_UNKNOWN_ERROR),
// 		SlySr_get (SLY_SUCCESS)
// 	);
//
// }
//
