/*!****************************************************************************
 *
 * @file
 * SK_MemStd.h
 *
 * The reference implementation of #SK_MemIntf.
 *
 *****************************************************************************/




#ifndef SK_MEMSTD_H
#define SK_MEMSTD_H




#include "SK_MemIntf.h"




extern const SK_MemIntf SK_MemStd;




/*!
 *  These declarations are present here for use in initializing #SK_MemDebug.
 *
 *  Otherwise, the SK_MemStd_* () functions should be accessed through the
 *  global constant #SK_MemStd.
 */

SlyDr SK_MemStd_free
(
	void *	addr
);




SlyDr SK_MemStd_list_impl
(
	SK_MemIntf *	intf
);




SlyDr SK_MemStd_set_conf
(
	const SK_MemIntfConf *	conf
);




#endif //SK_MEMSTD_H

