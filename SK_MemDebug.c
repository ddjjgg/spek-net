/*!****************************************************************************
 *
 * @file
 * SK_MemDebug.c
 *
 * See SK_MemDebug.h for details.
 *
 *****************************************************************************/




#include "SK_MemDebug.h"




#include "SK_MemStd.h"




/*!
 *  If this is non-zero, then the functions take (), retake (), take_near (),
 *  take_soft (), and take_hard () in #SK_MemDebug will always return failure
 *  (SLY_BAD_ALLOC).
 */

int SK_MemDebug_break_allocators = 0;




/*!
 *  If this is non-zero, then the function take () in #SK_MemDebug will always
 *  return failure (SLY_BAD_ALLOC).
 */

int SK_MemDebug_break_take = 0;




/*!
 *  If this is non-zero, then the function retake () in #SK_MemDebug will
 *  always return failure (SLY_BAD_ALLOC).
 */

int SK_MemDebug_break_retake = 0;




/*!
 *  If this is non-zero, then the function take_near () in #SK_MemDebug will
 *  always return failure (SLY_BAD_ALLOC).
 */

int SK_MemDebug_break_take_near = 0;




/*!
 *  If this is non-zero, then the function take_soft () in #SK_MemDebug will
 *  always return failure (SLY_BAD_ALLOC).
 */

int SK_MemDebug_break_take_soft = 0;




/*!
 *  If this is non-zero, then the function take_hard () in #SK_MemDebug will
 *  always return failure (SLY_BAD_ALLOC).
 */

int SK_MemDebug_break_take_hard = 0;




/*!
 *  See #SK_MemIntf.take ()
 */

SlySr SK_MemDebug_take
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size
)
{
	//React to the #*_break_* constants for this function

	if (SK_MemDebug_break_allocators || SK_MemDebug_break_take)
	{
		return SlySr_get (SLY_BAD_ALLOC);
	}


	//In the event that this function is not intentionally failing, then
	//behave like the associated function in #SK_MemStd.

	return SK_MemStd.take (addr, num_elems, elem_size);
}




/*!
 *  See #SK_MemIntf.retake ()
 */

SlySr SK_MemDebug_retake
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size
)
{
	//React to the #*_break_* constants for this function

	if (SK_MemDebug_break_allocators || SK_MemDebug_break_retake)
	{
		return SlySr_get (SLY_BAD_ALLOC);
	}


	//In the event that this function is not intentionally failing, then
	//behave like the associated function in #SK_MemStd.

	return SK_MemStd.retake (addr, num_elems, elem_size);
}




/*!
 *  See #SK_MemIntf.take_near ()
 */

SlySr SK_MemDebug_take_near
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		near_addr
)
{
	//React to the #*_break_* constants for this function

	if (SK_MemDebug_break_allocators || SK_MemDebug_break_take_near)
	{
		return SlySr_get (SLY_BAD_ALLOC);
	}


	//In the event that this function is not intentionally failing, then
	//behave like the associated function in #SK_MemStd.

	return SK_MemStd.take_near (addr, num_elems, elem_size, near_addr);
}




/*!
 *  See #SK_MemIntf.take_soft ()
 */

SlySr SK_MemDebug_take_soft
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		min_addr,

	void *		max_addr
)
{
	//React to the #*_break_* constants for this function

	if (SK_MemDebug_break_allocators || SK_MemDebug_break_take_soft)
	{
		return SlySr_get (SLY_BAD_ALLOC);
	}


	//In the event that this function is not intentionally failing, then
	//behave like the associated function in #SK_MemStd.

	return SK_MemStd.take_soft (addr, num_elems, elem_size, min_addr, max_addr);
}




/*!
 *  See #SK_MemIntf.take_hard ()
 */

SlySr SK_MemDebug_take_hard
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		min_addr,

	void *		max_addr
)
{
	//React to the #*_break_* constants for this function

	if (SK_MemDebug_break_allocators || SK_MemDebug_break_take_hard)
	{
		return SlySr_get (SLY_BAD_ALLOC);
	}


	//In the event that this function is not intentionally failing, then
	//behave like the associated function in #SK_MemStd.

	return SK_MemStd.take_hard (addr, num_elems, elem_size, min_addr, max_addr);
}




/*!
 *  An implementation of #SK_MemIntf where individual functions can be made to
 *  intentionally fail.
 */

const SK_MemIntf SK_MemDebug =

{
	.take =			SK_MemDebug_take,

	.retake =		SK_MemDebug_retake,

	.take_near =		SK_MemDebug_take_near,

	.take_soft =		SK_MemDebug_take_soft,

	.take_hard =		SK_MemDebug_take_hard,

	.free =			SK_MemStd_free,

	.mem_avail =		SK_MemIntf_no_impl_mem_avail,

	.list_impl =		SK_MemStd_list_impl,

	.set_conf =		SK_MemStd_set_conf
};

