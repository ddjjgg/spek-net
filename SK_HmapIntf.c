/*!****************************************************************************
 *
 * @file
 * SK_HmapIntf.c
 *
 * See SK_HmapIntf.h for details.
 *
 *****************************************************************************/




#include "SK_HmapIntf.h"




#include <SlyResult-0-x-x.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>




#include "SK_Array.h"
#include "SK_HmapIntf.h"
#include "SK_misc.h"




/*!
 *  Makes an #SK_HmapReqs have all its member pointers point to the contents
 *  of an #SK_HmapAllReqs.
 *
 *  @warning
 *  SK_HmapReqs_prep () will still be necessary to call on the resulting #req
 *  UNLESS SK_HmapAllReqs_prep () has already been used on #arq.
 *
 *  @param req			The #SK_HmapReqs that should have all its
 *  				fields point to fields in #arq
 *
 *  @param arq			The #SK_HmapAllReqs that should have all
 *				of its fields pointed to by #req
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapReqs_point_to_arq
(
	SK_HmapReqs *		req,

	const SK_HmapAllReqs *	arq
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, arq, SlyDr_get (SLY_BAD_ARG));

	req->stat_r = (const SK_HmapStatReqs *)		&(arq->stat_r);
	req->stat_i = (SK_HmapStatImpl *)		&(arq->stat_i);
	req->dyna_r = (const SK_HmapDynaReqs *)		&(arq->dyna_r);
	req->dyna_i = (SK_HmapDynaImpl *)		&(arq->dyna_i);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Performs a sanity check on an initialized #SK_HmapStatReqs to ensure that
 *  it is in a logical state.
 *
 *  @param stat_r		The #SK_HmapStatReqs to perform a sanity check
 *				on.
 *
 *				This #SK_HmapStatReqs MUST already be
 *				initialized.
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStatReqs_sanity_check
(
	const SK_HmapStatReqs * stat_r
)
{
	//Sanity check on argument (i.e. is #stat_r a NULL pointer)

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));


	//This sanity check ensures that the #SK_BoxStatReqs / #SK_BoxStatImpl
	//fields in #stat_r match what is produced by
	//#SK_HmapStatReqs_box_calc (), without actually modifying #stat_r
	//itself.

	//FIXME : Is there a way to make this sanity-check lighter-weight?
	//Perhaps by storing some hash values after
	//#SK_HmapStatReqs_box_calc ()?

	SK_HmapStatReqs	test_stat_r =		*stat_r;


	SK_BoxStatReqs test_key_group_stat_r =	(const SK_BoxStatReqs) {0};

	SK_BoxStatImpl test_key_group_stat_i =	(const SK_BoxStatImpl) {0};


	SK_BoxStatReqs test_val_group_stat_r =	(const SK_BoxStatReqs) {0};

	SK_BoxStatImpl test_val_group_stat_i =	(const SK_BoxStatImpl) {0};


	SK_BoxStatReqs test_key_bytes_stat_r =	(const SK_BoxStatReqs) {0};

	SK_BoxStatImpl test_key_bytes_stat_i =	(const SK_BoxStatImpl) {0};


	SK_BoxStatReqs test_val_bytes_stat_r =	(const SK_BoxStatReqs) {0};

	SK_BoxStatImpl test_val_bytes_stat_i =	(const SK_BoxStatImpl) {0};


	test_stat_r.key_group_stat_r = &test_key_group_stat_r;

	test_stat_r.key_group_stat_i = &test_key_group_stat_i;


	test_stat_r.val_group_stat_r = &test_val_group_stat_r;

	test_stat_r.val_group_stat_i = &test_val_group_stat_i;


	test_stat_r.key_bytes_stat_r = &test_key_bytes_stat_r;

	test_stat_r.key_bytes_stat_i = &test_key_bytes_stat_i;


	test_stat_r.val_bytes_stat_r = &test_val_bytes_stat_r;

	test_stat_r.val_bytes_stat_i = &test_val_bytes_stat_i;


	SlyDr dr = SK_HmapStatReqs_box_calc (&test_stat_r);

	if (SLY_SUCCESS != dr.res)
	{

		//Note, if SK_HmapStatReqs_box_calc () failed, that means the
		//contents of #test_stat_r were invalid, and therefore
		//initializing the #*_stat_r / #*_stat_i fields within it were
		//impossible

		SK_DEBUG_PRINT
		(
			SK_HMAP_INTF_L_DEBUG,
			"Improper initialization of #stat_r = %p detected.",
			stat_r
		);

		return dr;
	}


	int key_group_stat_r_shlw_eq = 0;
	int key_group_stat_i_shlw_eq = 0;

	int val_group_stat_r_shlw_eq = 0;
	int val_group_stat_i_shlw_eq = 0;

	int key_bytes_stat_r_shlw_eq = 0;
	int key_bytes_stat_i_shlw_eq = 0;

	int val_bytes_stat_r_shlw_eq = 0;
	int val_bytes_stat_i_shlw_eq = 0;


	SK_BoxStatReqs_shlw_eq
	(
		test_stat_r.key_group_stat_r,
		stat_r->key_group_stat_r,
		&key_group_stat_r_shlw_eq
	);

	SK_BoxStatImpl_shlw_eq
	(
		test_stat_r.key_group_stat_i,
		stat_r->key_group_stat_i,
		&key_group_stat_i_shlw_eq
	);

	SK_BoxStatReqs_shlw_eq
	(
		test_stat_r.val_group_stat_r,
		stat_r->val_group_stat_r,
		&val_group_stat_r_shlw_eq
	);

	SK_BoxStatImpl_shlw_eq
	(
		test_stat_r.val_group_stat_i,
		stat_r->val_group_stat_i,
		&val_group_stat_i_shlw_eq
	);

	SK_BoxStatReqs_shlw_eq
	(
		test_stat_r.key_bytes_stat_r,
		stat_r->key_bytes_stat_r,
		&key_bytes_stat_r_shlw_eq
	);

	SK_BoxStatImpl_shlw_eq
	(
		test_stat_r.key_bytes_stat_i,
		stat_r->key_bytes_stat_i,
		&key_bytes_stat_i_shlw_eq
	);

	SK_BoxStatReqs_shlw_eq
	(
		test_stat_r.val_bytes_stat_r,
		stat_r->val_bytes_stat_r,
		&val_bytes_stat_r_shlw_eq
	);

	SK_BoxStatImpl_shlw_eq
	(
		test_stat_r.val_bytes_stat_i,
		stat_r->val_bytes_stat_i,
		&val_bytes_stat_i_shlw_eq
	);


	if
	(
		(0 == key_group_stat_r_shlw_eq) ||
		(0 == key_group_stat_i_shlw_eq)
	)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_INTF_L_DEBUG,
			"In stat_r = %p, detected incorretly initialized "
			"#key_group_stat_r / #key_group_stat_i",
			stat_r
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	if (0 == stat_r->key_val_concat)
	{
		if
		(
			(0 == val_group_stat_r_shlw_eq) ||
			(0 == val_group_stat_i_shlw_eq)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"In stat_r = %p, detected incorretly "
				"initialized #val_group_stat_r / "
				"#val_group_stat_i",
				stat_r
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	if (stat_r->key_dyna_size_en)
	{
		if
		(
			(0 == key_bytes_stat_r_shlw_eq) ||
			(0 == key_bytes_stat_i_shlw_eq)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"In stat_r = %p, detected incorretly "
				"initialized #key_bytes_stat_r / "
				"#key_bytes_stat_i",
				stat_r
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	if (stat_r->val_dyna_size_en)
	{
		if
		(
			(0 == val_bytes_stat_r_shlw_eq) ||
			(0 == val_bytes_stat_i_shlw_eq)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"In stat_r = %p, detected incorretly "
				"initialized #val_bytes_stat_r / "
				"#val_bytes_stat_i",
				stat_r
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}




	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Performs a sanity check on an initialized #SK_HmapDynaReqs to ensure that
 *  it is in a logical state.
 *
 *  @param stat_r		The #SK_HmapStatReqs that was referenced during
 *				the intialization of #dyna_r.
 *
 *				I.e., the same #SK_HmapStatReqs that was used
 *				in the call of SK_HmapDynaReqs_box_calc () on
 *				#dyna_r.
 *
 *				This #SK_HmapStatReqs MUST already be
 *				initialized, and SHOULD be sanity-checked
 *				itself before calling this function.
 *
 *  @param dyna_r		The #SK_HmapDynaReqs to perform a sanity check
 *				on.
 *
 *				This #SK_HmapDynaReqs MUST already be
 *				initialized.
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapDynaReqs_sanity_check
(
	const SK_HmapStatReqs * stat_r,

	const SK_HmapDynaReqs * dyna_r
)
{
	//Sanity check on argument (i.e. is #dyna_r a NULL pointer)

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, dyna_r, SlyDr_get (SLY_BAD_ARG));


	//This sanity check ensures that the #SK_BoxDynaReqs / #SK_BoxDynaImpl
	//fields in #dyna_r match what is produced by
	//#SK_HmapDynaReqs_box_calc (), without actually modifying #dyna_r
	//itself.

	//FIXME : Is there a way to make this sanity-check lighter-weight?
	//Perhaps by storing some hash values after
	//#SK_HmapDynaReqs_box_calc ()?

	SK_HmapDynaReqs	test_dyna_r =		*dyna_r;


	SK_BoxDynaReqs test_key_group_dyna_r =	(const SK_BoxDynaReqs) {0};

	SK_BoxDynaImpl test_key_group_dyna_i =	(const SK_BoxDynaImpl) {0};


	SK_BoxDynaReqs test_val_group_dyna_r =	(const SK_BoxDynaReqs) {0};

	SK_BoxDynaImpl test_val_group_dyna_i =	(const SK_BoxDynaImpl) {0};


	SK_BoxDynaReqs test_key_bytes_dyna_r =	(const SK_BoxDynaReqs) {0};

	SK_BoxDynaImpl test_key_bytes_dyna_i =	(const SK_BoxDynaImpl) {0};


	SK_BoxDynaReqs test_val_bytes_dyna_r =	(const SK_BoxDynaReqs) {0};

	SK_BoxDynaImpl test_val_bytes_dyna_i =	(const SK_BoxDynaImpl) {0};


	test_dyna_r.key_group_dyna_r = &test_key_group_dyna_r;

	test_dyna_r.key_group_dyna_i = &test_key_group_dyna_i;


	test_dyna_r.val_group_dyna_r = &test_val_group_dyna_r;

	test_dyna_r.val_group_dyna_i = &test_val_group_dyna_i;


	test_dyna_r.key_bytes_dyna_r = &test_key_bytes_dyna_r;

	test_dyna_r.key_bytes_dyna_i = &test_key_bytes_dyna_i;


	test_dyna_r.val_bytes_dyna_r = &test_val_bytes_dyna_r;

	test_dyna_r.val_bytes_dyna_i = &test_val_bytes_dyna_i;


	SlyDr dr = SK_HmapDynaReqs_box_calc (stat_r, &test_dyna_r);

	if (SLY_SUCCESS != dr.res)
	{
		//Note, if SK_HmapDynaReqs_box_calc () failed, that means the
		//contents of either #stat_r or #test_dyna_r were invalid, and
		//therefore initializing the #*_dyna_r / #*_dyna_i fields
		//within #dyna_r were impossible

		SK_DEBUG_PRINT
		(
			SK_HMAP_INTF_L_DEBUG,
			"Improper initialization of #dyna_r = %p detected.",
			dyna_r
		);

		return dr;
	}


	int key_group_dyna_r_shlw_eq = 0;
	int key_group_dyna_i_shlw_eq = 0;

	int val_group_dyna_r_shlw_eq = 0;
	int val_group_dyna_i_shlw_eq = 0;

	int key_bytes_dyna_r_shlw_eq = 0;
	int key_bytes_dyna_i_shlw_eq = 0;

	int val_bytes_dyna_r_shlw_eq = 0;
	int val_bytes_dyna_i_shlw_eq = 0;


	SK_BoxDynaReqs_shlw_eq
	(
		test_dyna_r.key_group_dyna_r,
		dyna_r->key_group_dyna_r,
		&key_group_dyna_r_shlw_eq
	);

	SK_BoxDynaImpl_shlw_eq
	(
		test_dyna_r.key_group_dyna_i,
		dyna_r->key_group_dyna_i,
		&key_group_dyna_i_shlw_eq
	);

	SK_BoxDynaReqs_shlw_eq
	(
		test_dyna_r.val_group_dyna_r,
		dyna_r->val_group_dyna_r,
		&val_group_dyna_r_shlw_eq
	);

	SK_BoxDynaImpl_shlw_eq
	(
		test_dyna_r.val_group_dyna_i,
		dyna_r->val_group_dyna_i,
		&val_group_dyna_i_shlw_eq
	);

	SK_BoxDynaReqs_shlw_eq
	(
		test_dyna_r.key_bytes_dyna_r,
		dyna_r->key_bytes_dyna_r,
		&key_bytes_dyna_r_shlw_eq
	);

	SK_BoxDynaImpl_shlw_eq
	(
		test_dyna_r.key_bytes_dyna_i,
		dyna_r->key_bytes_dyna_i,
		&key_bytes_dyna_i_shlw_eq
	);

	SK_BoxDynaReqs_shlw_eq
	(
		test_dyna_r.val_bytes_dyna_r,
		dyna_r->val_bytes_dyna_r,
		&val_bytes_dyna_r_shlw_eq
	);

	SK_BoxDynaImpl_shlw_eq
	(
		test_dyna_r.val_bytes_dyna_i,
		dyna_r->val_bytes_dyna_i,
		&val_bytes_dyna_i_shlw_eq
	);


	if
	(
		(0 == key_group_dyna_r_shlw_eq) ||
		(0 == key_group_dyna_i_shlw_eq)
	)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_INTF_L_DEBUG,
			"In dyna_r = %p, detected incorretly initialized "
			"#key_group_dyna_r / #key_group_dyna_i",
			dyna_r
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	if (0 == stat_r->key_val_concat)
	{
		if
		(
			(0 == val_group_dyna_r_shlw_eq) ||
			(0 == val_group_dyna_i_shlw_eq)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"In dyna_r = %p, detected incorretly "
				"initialized #val_group_dyna_r / "
				"#val_group_dyna_i",
				dyna_r
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	if (stat_r->key_dyna_size_en)
	{
		if
		(
			(0 == key_bytes_dyna_r_shlw_eq) ||
			(0 == key_bytes_dyna_i_shlw_eq)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"In dyna_r = %p, detected incorretly "
				"initialized #key_bytes_dyna_r / "
				"#key_bytes_dyna_i",
				dyna_r
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	if (stat_r->val_dyna_size_en)
	{
		if
		(
			(0 == val_bytes_dyna_r_shlw_eq) ||
			(0 == val_bytes_dyna_i_shlw_eq)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"In dyna_r = %p, detected incorretly "
				"initialized #val_bytes_dyna_r / "
				"#val_bytes_dyna_i",
				dyna_r
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}




	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Calculates the values within an #SK_HmapStatImpl based off of the values
 *  contained within an #SK_HmapStatReqs.
 *
 *  @note
 *  Note how #SK_HmapDynaReqs and #SK_HmapDynaImpl are not referenced in
 *  this function. This is because letting a dynamic parameter determine a
 *  static parameter is dangerous, though the reverse is acceptable.
 *
 *  @warning
 *  If #stat_r is detected as representing an impossible configuration, this
 *  function will fail and return without modifiying #stat_i.
 *
 *  @param stat_r		The #SK_HmapStatReqs that represents the
 *  				requested static parameters of an #SK_Hmap.
 *
 *				This MUST be already initialized.
 *
 * 				@warning
 *				If the #SK_BoxStatReqs / #SK_BoxStatImpl fields
 *				of #stat_r do not match what is produced by
 *				SK_HmapStatReqs_box_calc (), then this function
 *				will fail.
 *
 *  @param stat_i		Where to place the #SK_HmapStatImpl that
 *  				will be calculated based off of #stat_r.
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStatImpl_prep
(
	const SK_HmapStatReqs *	stat_r,

	SK_HmapStatImpl *	stat_i
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_i, SlyDr_get (SLY_BAD_ARG));


	SlyDr dr = SK_HmapStatReqs_sanity_check (stat_r);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, dr.res, dr);


	//If static-sized keys are used, then #prim_key_cmp can be set, and
	//later used to compare keys of a static size

	SK_HmapStatImpl_sel_prim_flag (stat_r, &(stat_i->prim_flag));

	SK_HmapStatImpl_sel_prim_key_cmp (stat_r, &(stat_i->prim_key_cmp));


	//These values used to temporarily form whole #SK_BoxReqs despite the
	//fact that the dynamic portions are not calculated in this function

	SK_BoxReqs brq = {0};

	SK_BoxAllReqs arq = {0};


	//Calculate the #*_inst_stat_mem fields in #stat_i.
	//
	//Note how #brq is initialized in such a manner that contents of
	//#stat_r are not modified.

	arq.stat_r = *(stat_r->key_group_stat_r);

	SK_BoxReqs_prep
	(
		&brq,
		&(arq.stat_r),
		&(arq.stat_i),
		&(arq.dyna_r),
		&(arq.dyna_i)
	);

	stat_r->key_group_intf->inst_stat_mem
	(
		&brq,
		&(stat_i->key_group_inst_stat_mem)
	);

	stat_r->key_group_intf->anc_support
	(
		&brq,
		&(stat_i->key_group_anc_support)
	);

	stat_r->key_group_intf->anc_size
	(
		&brq,
		&(stat_i->key_group_anc_size)
	);




	if (0 == stat_r->key_val_concat)
	{
		arq.stat_r = *(stat_r->val_group_stat_r);

		SK_BoxReqs_prep
		(
			&brq,
			&(arq.stat_r),
			&(arq.stat_i),
			&(arq.dyna_r),
			&(arq.dyna_i)
		);

		stat_r->val_group_intf->inst_stat_mem
		(
			&brq,
			&(stat_i->val_group_inst_stat_mem)
		);

		stat_r->val_group_intf->anc_support
		(
			&brq,
			&(stat_i->val_group_anc_support)
		);

		stat_r->val_group_intf->anc_size
		(
			&brq,
			&(stat_i->val_group_anc_size)
		);
	}

	else
	{
		stat_i->val_group_inst_stat_mem =	0;
		stat_i->val_group_anc_support =		0;
		stat_i->val_group_anc_size =		0;
	}




	if (stat_r->key_dyna_size_en)
	{
		arq.stat_r = *(stat_r->key_bytes_stat_r);

		SK_BoxReqs_prep
		(
			&brq,
			&(arq.stat_r),
			&(arq.stat_i),
			&(arq.dyna_r),
			&(arq.dyna_i)
		);

		stat_r->key_bytes_intf->inst_stat_mem
		(
			&brq,
			&(stat_i->key_bytes_inst_stat_mem)
		);

		stat_r->key_bytes_intf->anc_support
		(
			&brq,
			&(stat_i->key_bytes_anc_support)
		);

		stat_r->key_bytes_intf->anc_size
		(
			&brq,
			&(stat_i->key_bytes_anc_size)
		);
	}

	else
	{
		stat_i->key_bytes_inst_stat_mem =	0;
		stat_i->key_bytes_anc_support =		0;
		stat_i->key_bytes_anc_size =		0;
	}




	if (stat_r->val_dyna_size_en)
	{
		arq.stat_r = *(stat_r->val_bytes_stat_r);

		SK_BoxReqs_prep
		(
			&brq,
			&(arq.stat_r),
			&(arq.stat_i),
			&(arq.dyna_r),
			&(arq.dyna_i)
		);

		stat_r->val_bytes_intf->inst_stat_mem
		(
			&brq,
			&(stat_i->val_bytes_inst_stat_mem)
		);

		stat_r->val_bytes_intf->anc_support
		(
			&brq,
			&(stat_i->val_bytes_anc_support)
		);

		stat_r->val_bytes_intf->anc_size
		(
			&brq,
			&(stat_i->val_bytes_anc_size)
		);
	}

	else
	{
		stat_i->val_bytes_inst_stat_mem =	0;
		stat_i->val_bytes_anc_support =		0;
		stat_i->val_bytes_anc_size =		0;
	}




	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Calculates the values within an #SK_HmapDynaImpl based off of the values
 *  contained within an #SK_HmapDynaReqs.
 *
 *  @warning
 *  If #stat_r, #stat_i, or #dyna_r, are detected as representing an impossible
 *  configuration, this function will fail and return without modifiying
 *  #dyna_i.
 *
 *  @param stat_r		The #SK_HmapStatReqs that represents the
 *  				requested static parameters of an #SK_Hmap.
 *
 *				This MUST be already initialized.
 *
 *  @param stat_i		The #SK_HmapStatImpl that was previously
 *				calculated using SK_HmapStatImpl_prep ().
 *
 *				This MUST be already initialized.
 *
 *  @param dyna_r		The #SK_HmapDynaReqs that represents the
 *				requested dynamic parameters of an #SK_Hmap.
 *
 *				This MUST be already initialized.
 *
 * 				@warning
 *				If the #SK_BoxStatReqs / #SK_BoxStatImpl fields
 *				of #stat_r do not match what is produced by
 *				SK_HmapStatReqs_box_calc (), then this function
 *				will fail.
 *
 *  @param dyna_i		Where to place the #SK_HmapDynaImpl that
 *  				will be calculated based off of #stat_r,
 *  				#stat_i, and #dyna_r.
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapDynaImpl_prep
(
	const SK_HmapStatReqs *	stat_r,

	const SK_HmapStatImpl *	stat_i,

	const SK_HmapDynaReqs *	dyna_r,

	SK_HmapDynaImpl *	dyna_i
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_i, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, dyna_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, dyna_i, SlyDr_get (SLY_BAD_ARG));


	SlyDr dr = SK_HmapDynaReqs_sanity_check (stat_r, dyna_r);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, dr.res, dr);


	dyna_i->key_group_req.stat_r = stat_r->key_group_stat_r;
	dyna_i->key_group_req.stat_i = stat_r->key_group_stat_i;
	dyna_i->key_group_req.dyna_r = dyna_r->key_group_dyna_r;
	dyna_i->key_group_req.dyna_i = dyna_r->key_group_dyna_i;


	dyna_i->val_group_req.stat_r = stat_r->val_group_stat_r;
	dyna_i->val_group_req.stat_i = stat_r->val_group_stat_i;
	dyna_i->val_group_req.dyna_r = dyna_r->val_group_dyna_r;
	dyna_i->val_group_req.dyna_i = dyna_r->val_group_dyna_i;


	dyna_i->key_bytes_req.stat_r = stat_r->key_bytes_stat_r;
	dyna_i->key_bytes_req.stat_i = stat_r->key_bytes_stat_i;
	dyna_i->key_bytes_req.dyna_r = dyna_r->key_bytes_dyna_r;
	dyna_i->key_bytes_req.dyna_i = dyna_r->key_bytes_dyna_i;


	dyna_i->val_bytes_req.stat_r = stat_r->val_bytes_stat_r;
	dyna_i->val_bytes_req.stat_i = stat_r->val_bytes_stat_i;
	dyna_i->val_bytes_req.dyna_r = dyna_r->val_bytes_dyna_r;
	dyna_i->val_bytes_req.dyna_i = dyna_r->val_bytes_dyna_i;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Prepares an #SK_HmapReqs to be used by setting its pointers and
 *  initializing the "implied" values.
 *
 *  @warning
 *  If the pointers #stat_r, #stat_i, #dyna_r, or #dyna_i become invalid while
 *  #req is being used, then memory access errors WILL occur.
 *
 *  @warning
 *  If #stat_r or #dyna_r are detected as requesting an impossible
 *  configuration, this function will fail and return without modifiying #req,
 *  #stat_i, or #dyna_i.
 *
 *  @param req			Pointer to the #SK_HmapReqs to prepare,
 *				which is NOT expected to be initialized.
 *
 *  				(Regardless if the pointers inside of it are
 *  				already set, they will be set to (stat_r,
 *  				stat_i, dyna_r, dyna_i) respectively.)
 *
 *				This CAN be NULL if this argument is not
 *				desired, in which case the other arguments will
 *				simply be prepared.
 *
 *  @param stat_r		Pointer to the #SK_HmapStatReqs to use,
 *				which IS expected to be manually initialized.
 *
 *  @param stat_i		Pointer to the #SK_HmapStatImpl to use, which
 *  				is NOT expected to be intialized, but will be
 *  				initalized after this function completes.
 *
 *  @param dyna_r		Pointer to the #SK_HmapDynaReqs to use, which
 *  				IS expected to be manually initialized.
 *
 *  @param dyna_i		Pointer to the #SK_HmapDynaImpl to use, which
 *				is NOT expected to be intialized, but will be
 *				initalized after this function completes.
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapReqs_prep
(
	SK_HmapReqs *			req,

	const SK_HmapStatReqs *		stat_r,

	SK_HmapStatImpl *		stat_i,

	const SK_HmapDynaReqs *		dyna_r,

	SK_HmapDynaImpl *		dyna_i
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_i, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, dyna_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, dyna_i, SlyDr_get (SLY_BAD_ARG));


	//Calculate #stat_i from #stat_r

	SlyDr dr = SK_HmapStatImpl_prep (stat_r, stat_i);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, dr.res, dr);


	//Calculate #dyna_i from #stat_r, #stat_i, and #dyna_r

	dr = SK_HmapDynaImpl_prep (stat_r, stat_i, dyna_r, dyna_i);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, dr.res, dr);


	//Set the pointers inside of #req

	if (NULL != req)
	{
		req->stat_r = stat_r;

		req->stat_i = stat_i;

		req->dyna_r = dyna_r;

		req->dyna_i = dyna_i;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Prepares an #SK_HmapAllReqs by initializing the "implied" values.
 *
 *  This makes it so that if #SK_HmapReqs_point_to_arq () is used to make an
 *  #SK_HmapReqs point to the contents of #arq, then the resulting
 *  #SK_HmapReqs will already be prepared.
 *
 *  @param arq			The #SK_HmapAllReqs to prepare. This is NOT
 *				expected to be initialized yet.
 *
 *  @param stat_r		Pointer to the #SK_HmapStatReqs to use, which
 *				IS expected to be initialized.
 *
 *				(arq->stat_r) will be set equal to this.
 *
 *				This argument CAN be equal to &(arq->stat_r),
 *				provided that it is initialized.
 *
 *  @param dyna_r		Pointer to the #SK_HmapDynaReqs to use, which
 *				IS expected to be initialized.
 *
 *				(arq->dyna_r) will be set equal to this.
 *
 *				This argument CAN be equal to &(arq->dyna_r),
 *				provided that it is intiialized.
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapAllReqs_prep
(
	SK_HmapAllReqs *		arq,

	const SK_HmapStatReqs *		stat_r,

	const SK_HmapDynaReqs *		dyna_r
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, arq, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, dyna_r, SlyDr_get (SLY_BAD_ARG));


	//Copy the value (arq->stat_r) and (arq->dyna_r), if necessary

	if (&(arq->stat_r) != stat_r)
	{
		arq->stat_r = *stat_r;
	}


	if (&(arq->dyna_r) != dyna_r)
	{
		arq->dyna_r = *dyna_r;
	}


	//Make use of SK_HmapReqs_prep () to prepare #arq

	return

	SK_HmapReqs_prep
	(
		NULL,
		&(arq->stat_r),
		&(arq->stat_i),
		&(arq->dyna_r),
		&(arq->dyna_i)
	);
}




/*!
 *  Selects what value #SK_HmapStatImpl.prim_flag should have for an associated
 *  #SK_HmapStatReqs.
 *
 *  @param stat_r		The #SK_HmapStatReqs associated with the
 *				#SK_HmapStatImpl in which #prim_flag is being
 *				calculated
 *
 *  @param prim_flag		Where to place the value of prim_flag
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStatImpl_sel_prim_flag
(
	const SK_HmapStatReqs *		stat_r,

	int *				prim_flag
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, prim_flag, SlyDr_get (SLY_BAD_ARG));


	if (0 >= stat_r->key_stat_size)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_INTF_L_DEBUG,
			"Could not properly set #prim_flag because "
			"#stat_r->key_stat_size was 0."
		);

		*prim_flag = 0;

		return SlyDr_get (SLY_BAD_ARG);
	}


	if
	(
		(0 == stat_r->key_dyna_size_en)

		&&

		(8 >= stat_r->key_stat_size)
	)
	{
		*prim_flag = 1;
	}

	else
	{
		*prim_flag = 0;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Selects what value #SK_HmapStatImpl.prim_key_cmp should have for an
 *  associated #SK_HmapStatReqs.
 *
 *  @param stat_r		The #SK_HmapStatReqs associated with the
 *				#SK_HmapStatImpl in which #prim_key_cmp is
 *				being calculated
 *
 *  @param prim_key_cmp		Where to place the value of prim_key_cmp
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStatImpl_sel_prim_key_cmp
(
	const SK_HmapStatReqs *		stat_r,

	SK_HmapIntf_prim_cmp *		prim_key_cmp
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, prim_key_cmp, SlyDr_get (SLY_BAD_ARG));


	if (0 >= stat_r->key_stat_size)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_INTF_L_DEBUG,
			"Could not properly set #prim_key_cmp because "
			"#stat_r->key_stat_size was 0."
		);

		*prim_key_cmp = NULL;

		return SlyDr_get (SLY_BAD_ARG);
	}


	if
	(
		(0 == stat_r->key_dyna_size_en)

		&&

		(8 >= stat_r->key_stat_size)
	)
	{

		SK_HmapIntf_prim_cmp funcs [8] =

		{
			SK_byte_le_cmp_b1,
			SK_byte_le_cmp_b2,
			SK_byte_le_cmp_b3,
			SK_byte_le_cmp_b4,
			SK_byte_le_cmp_b5,
			SK_byte_le_cmp_b6,
			SK_byte_le_cmp_b7,
			SK_byte_le_cmp_b8
		};


		*prim_key_cmp = funcs [stat_r->key_stat_size - 1];
	}

	else
	{
		*prim_key_cmp = NULL;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Within an #SK_HmapStatReqs, initializes the #*_stat_r and #*_stat_i fields
 *  such that they are consistent with the rest of the contents of the
 *  #SK_HmapStatReqs.
 *
 *  @note
 *  The reason that this is a separate function from SK_HmapReqs_prep () is
 *  that doing so assists in sharing the #*_stat_r and #*_stat_i pointers
 *  amongst multiple #SK_HmapStatReqs structures. This is the function that
 *  modifies those particular fields, while SK_HmapReqs_prep () merely checks
 *  if they are correctly set. So, SK_HmapReqs_prep () can be called on the
 *  other #SK_HmapReqs pointing to the #SK_HmapStatReqs that re-uses the
 *  #*_stat_r / #*_stat_i pointers.
 *
 *  @param req			The #SK_HmapStatReqs requiring initialization
 *				of the #*_stat_r and #*_stat_i fields.
 *
 *				@warning
 *				#req must be fully initialized before calling
 *				this function.
 *
 *				This implies that the (req->*_stat_r) and
 *				(req->*_stat_i) fields must point to
 *				#SK_BoxStatReqs / #SK_BoxStatImpl (which may /
 *				may not be initialized, but will be initialized
 *				after this function completes).
 *
 *				Otherwise, this function will fail.
 *
 *				@warning
 *				As stated in the documentation of
 *				#SK_HmapStatReqs, some of the #*_stat_r and
 *				#*_stat_i fields of #req may be ignored
 *				depending on the if the value of other fields
 *				in #req.
 *
 *				For the fields of #req that will be ignored,
 *				if they have non-null values, they will be set
 *				to a default value.
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStatReqs_box_calc
(
	SK_HmapStatReqs *	req
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));


	//TODO : Perhaps move this portion of the sanity check to a separate
	//function?

	if
	(
		(NULL == req->key_group_intf)	||
		(NULL == req->key_group_stat_r)	||
		(NULL == req->key_group_stat_i)
	)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_INTF_L_DEBUG,
			"Invalid NULL value in #req = %p\n"
			"req->key_group_intf =   %p\n"
			"req->key_group_stat_r = %p\n"
			"req->key_group_stat_i = %p",
			req,
			req->key_group_intf,
			req->key_group_stat_r,
			req->key_group_stat_i
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	if (0 == req->key_val_concat)
	{
		if
		(
			(NULL == req->val_group_intf)	||
			(NULL == req->val_group_stat_r)	||
			(NULL == req->val_group_stat_i)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #req = %p\n"
				"req->key_val_concat =   %d\n"
				"req->val_group_intf =   %p\n"
				"req->val_group_stat_r = %p\n"
				"req->val_group_stat_i = %p",
				req,
				req->key_val_concat,
				req->val_group_intf,
				req->val_group_stat_r,
				req->val_group_stat_i
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	if (req->key_dyna_size_en)
	{
		if
		(
			(NULL == req->key_bytes_intf)	||
			(NULL == req->key_bytes_stat_r)	||
			(NULL == req->key_bytes_stat_i)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #req = %p\n"
				"req->key_dyna_size_en = %d\n"
				"req->key_bytes_intf =   %p\n"
				"req->key_bytes_stat_r = %p\n"
				"req->key_bytes_stat_i = %p",
				req,
				req->key_dyna_size_en,
				req->key_bytes_intf,
				req->key_bytes_stat_r,
				req->key_bytes_stat_i
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}

	else
	{
		if (0 == req->key_stat_size)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #req = %p\n"
				"req->key_dyna_size_en = %d\n"
				"req->key_stat_size =    %zu\n",
				req,
				req->key_dyna_size_en,
				req->key_stat_size
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	if (req->val_dyna_size_en)
	{
		if
		(
			(NULL == req->val_bytes_intf)	||
			(NULL == req->val_bytes_stat_r)	||
			(NULL == req->val_bytes_stat_i)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #req = %p\n"
				"req->val_dyna_size_en = %d\n"
				"req->val_bytes_intf =   %p\n"
				"req->val_bytes_stat_r = %p\n"
				"req->val_bytes_stat_i = %p",
				req,
				req->val_dyna_size_en,
				req->val_bytes_intf,
				req->val_bytes_stat_r,
				req->val_bytes_stat_i
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}

	else
	{
		if (0 == req->val_stat_size)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #req = %p\n"
				"req->val_dyna_size_en = %d\n"
				"req->val_stat_size =    %zu\n",
				req,
				req->val_dyna_size_en,
				req->val_stat_size
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//If the #*_stat_r_basis fields in #req were used, initially copy them
	//over every associated #*_stat_r field.
	//
	//If no basis is given for a particular field, then instead initialize
	//these structs with default values.

	if (NULL != req->key_group_stat_r_basis)
	{
		*(req->key_group_stat_r) = *(req->key_group_stat_r_basis);
	}

	else
	{
		*(req->key_group_stat_r) =(const SK_BoxStatReqs) {0};
	}


	if (0 == req->key_val_concat)
	{
		if (NULL != req->val_group_stat_r_basis)
		{
			*(req->val_group_stat_r) = *(req->val_group_stat_r_basis);
		}

		else
		{
			*(req->val_group_stat_r) = (const SK_BoxStatReqs) {0};
		}
	}

	else
	{
		if (NULL != req->val_group_stat_r)
		{
			*(req->val_group_stat_r) = (const SK_BoxStatReqs) {0};
		}

		if (NULL != req->val_group_stat_i)
		{
			*(req->val_group_stat_i) = (const SK_BoxStatImpl) {0};
		}
	}


	if (req->key_dyna_size_en)
	{
		if (NULL != req->key_bytes_stat_r_basis)
		{
			*(req->key_bytes_stat_r) = *(req->key_bytes_stat_r_basis);
		}

		else
		{
			*(req->key_bytes_stat_r) = (const SK_BoxStatReqs) {0};
		}
	}

	else
	{
		if (NULL != req->key_bytes_stat_r)
		{
			*(req->key_bytes_stat_r) = (const SK_BoxStatReqs) {0};
		}

		if (NULL != req->key_bytes_stat_i)
		{
			*(req->key_bytes_stat_i) = (const SK_BoxStatImpl) {0};
		}
	}


	if (req->val_dyna_size_en)
	{
		if (NULL != req->val_bytes_stat_r_basis)
		{
			*(req->val_bytes_stat_r) = *(req->val_bytes_stat_r_basis);
		}

		else
		{
			*(req->val_bytes_stat_r) = (const SK_BoxStatReqs) {0};
		}
	}

	else
	{
		if (NULL != req->val_bytes_stat_r)
		{
			*(req->val_bytes_stat_r) = (const SK_BoxStatReqs) {0};
		}

		if (NULL != req->val_bytes_stat_i)
		{
			*(req->val_bytes_stat_i) = (const SK_BoxStatImpl) {0};
		}
	}


	//Ensure that every #*_stat_r->mem field is set to something valid

	if (NULL == req->key_group_stat_r->mem)
	{
		req->key_group_stat_r->mem = &SK_MemStd;
	}

	if (0 == req->key_val_concat)
	{
		if (NULL == req->val_group_stat_r->mem)
		{
			req->val_group_stat_r->mem = &SK_MemStd;
		}
	}

	if (req->key_dyna_size_en)
	{
		if (NULL == req->key_bytes_stat_r->mem)
		{
			req->key_bytes_stat_r->mem = &SK_MemStd;
		}
	}

	if (req->val_dyna_size_en)
	{
		if (NULL == req->val_bytes_stat_r->mem)
		{
			req->val_bytes_stat_r->mem = &SK_MemStd;
		}
	}


	//These values are used to temporarily form whole #SK_BoxReqs despite
	//the fact that the dynamic portions are not calculated in this
	//function

	SK_BoxDynaReqs temp_bdr = {0};

	SK_BoxDynaImpl temp_bdi = {0};


	//This section sets the contents of #val_bytes_stat_r and
	//#val_bytes_stat_i consistently with the remainder of #req

	if (req->val_dyna_size_en)
	{
		req->val_bytes_stat_r->elem_size = 1;


		//This initializes #val_bytes_stat_i

		SK_BoxStatImpl_prep
		(
			req->val_bytes_stat_r,
			req->val_bytes_stat_i
		);
	}


	//This section sets the contents of #key_bytes_stat_r and
	//#key_bytes_stat_i consistently with the remainder of #req

	if (req->key_dyna_size_en)
	{
		req->key_bytes_stat_r->elem_size = 1;


		//This initializes #key_bytes_stat_i

		SK_BoxStatImpl_prep
		(
			req->key_bytes_stat_r,
			req->key_bytes_stat_i
		);
	}


	//This section sets the contents of #key_bytes_stat_r and
	//#key_bytes_stat_i consistently with the remainder of #req

	if (0 == req->key_val_concat)
	{
		if (req->val_dyna_size_en)
		{
			//Temporarily make a full #SK_BoxReqs that will be used
			//in conjunction with #req->val_bytes_intf.
			//
			//Note that this #SK_BoxReqs is already prepared due to
			//the earlier sections of code.
			//
			//Also note that the #dyna_r and #dyna_i fields here
			//are largely unimportant, because only static values
			//are being calculated at this time, and static values
			//do not have data dependencies on dynamic values in
			//#SK_BoxReqs.

			SK_BoxReqs brq =

			{
				.stat_r = req->val_bytes_stat_r,
				.stat_i = req->val_bytes_stat_i,
				.dyna_r = &temp_bdr,
				.dyna_i = &temp_bdi
			};


			//The size of each element within the boxes created
			//with #val_group_intf will each be the size reported
			//by #val_bytes_intf->inst_stat_mem ()

			size_t inst_stat_mem = 0;

			req->val_bytes_intf->inst_stat_mem
			(
				&brq,
				&inst_stat_mem
			);


			req->val_group_stat_r->elem_size = inst_stat_mem;
		}

		else
		{
			req->val_group_stat_r->elem_size = req->val_stat_size;
		}


		//This initializes #val_group_stat_i

		SK_BoxStatImpl_prep
		(
			req->val_group_stat_r,
			req->val_group_stat_i
		);
	}


	//This section sets the contents of #key_group_stat_r and
	//#key_group_stat_i consistently with the remainder of #req

	if (req->key_dyna_size_en)
	{
		//Temporarily make a full #SK_BoxReqs that will be used in
		//conjunction with #req->key_bytes_intf.
		//
		//Note that this #SK_BoxReqs is already prepared due to the
		//earlier sections of code.

		SK_BoxReqs brq =

		{
			.stat_r = req->key_bytes_stat_r,
			.stat_i = req->key_bytes_stat_i,
			.dyna_r = &temp_bdr,
			.dyna_i = &temp_bdi
		};


		//The size of each element within the boxes created with
		//#key_group_intf will each be the size reported by
		//#key_bytes_intf->inst_stat_mem ()

		size_t inst_stat_mem = 0;

		req->key_bytes_intf->inst_stat_mem
		(
			&brq,
			&inst_stat_mem
		);


		req->key_group_stat_r->elem_size = inst_stat_mem;
	}

	else
	{
		req->key_group_stat_r->elem_size = req->key_stat_size;
	}


	if (req->key_val_concat)
	{

		if (req->val_dyna_size_en)
		{
			//Since #req->key_val_concat and #req->val_dyna_size_en
			//are true, add the per-instance static memory cost of
			//the boxes created with #req->val_bytes_intf to
			//#req->key_group_stat_r to the element size of
			//#key_group_stat_r

			SK_BoxReqs brq =

			{
				.stat_r = req->val_bytes_stat_r,
				.stat_i = req->val_bytes_stat_i,
				.dyna_r = &temp_bdr,
				.dyna_i = &temp_bdi
			};


			size_t inst_stat_mem = 0;

			req->val_bytes_intf->inst_stat_mem
			(
				&brq,
				&inst_stat_mem
			);


			req->key_group_stat_r->elem_size += inst_stat_mem;
		}

		else
		{
			//Since #req->key_val_concat is true and
			//#req->val_dyna_size_en is false, add the value of
			//#req->val_stat_size to the element size of
			//#key_group_stat_r

			req->key_group_stat_r->elem_size += req->val_stat_size;
		}
	}


	//This initializes #key_group_stat_i

	SK_BoxStatImpl_prep
	(
		req->key_group_stat_r,
		req->key_group_stat_i
	);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Within an #SK_HmapDynaReqs, initializes the #*_dyna_r and #*_dyna_i fields
 *  such that they are consistent with the rest of the contents of the
 *  #SK_HmapDynaReqs.
 *
 *  @note
 *  The reason that this is a separate function from SK_HmapReqs_prep () is
 *  that doing so assists in sharing the #*_dyna_r and #*_dyna_i pointers
 *  amongst multiple #SK_HmapDynaReqs structures. This is the function that
 *  modifies those particular fields, while SK_HmapReqs_prep () merely checks
 *  if they are correctly set. So, SK_HmapReqs_prep () can be called on the
 *  other #SK_HmapReqs pointing to the #SK_HmapDynaReqs that re-uses the
 *  #*_dyna_r / #*_dyna_i pointers.
 *
 *  @param sreq			The #SK_HmapStatReqs that will be associated
 *				with #dreq inside of an #SK_HmapReqs.
 *
 *				SK_HmapStatReqs_box_calc () MUST have already
 *				been used on #sreq before calling this
 *				function.
 *
 *				@warning
 *				If SK_HmapStatReqs_box_calc () has not been
 *				used on #sreq before using this function,
 *				then #dreq may end up in an invalid state.
 *
 *  @param dreq			The #SK_HmapDynaReqs requiring initialization
 *				of the #*_dyna_r and #*_dyna_i fields.
 *
 *				@warning
 *				#dreq must be fully initialized before calling
 *				this function.
 *
 *				This implies that the (dreq->*_dyna_r) and
 *				(dreq->*_dyna_i) fields must point to
 *				#SK_BoxDynaReqs / #SK_BoxStatImpl (which may /
 *				may not be initialized, but will be initialized
 *				after this function completes).
 *
 *				Otherwise, this function will fail.
 *
 *				@warning
 *				As stated in the documentation of
 *				#SK_HmapDynaReqs, some of the #*_dyna_r and
 *				#*_dyna_i fields of #dreq may be ignored
 *				depending on the if the value of other fields
 *				in #dreq.
 *
 *				For the fields of #req that will be ignored,
 *				if they have non-null values, they will be set
 *				to a default value.
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapDynaReqs_box_calc
(
	const SK_HmapStatReqs *	sreq,

	SK_HmapDynaReqs *	dreq
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, sreq, SlyDr_get (SLY_BAD_ARG));


	//TODO : Perhaps move these next sections of the sanity check to their
	//own separate functions?


	//Perform a sanity check on #sreq, which perhaps is "overbearing" as
	//this sanity check is also in SK_HmapStatReqs_box_calc ()

	if
	(
		(NULL == sreq->key_group_intf)		||
		(NULL == sreq->key_group_stat_r)	||
		(NULL == sreq->key_group_stat_i)
	)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_INTF_L_DEBUG,
			"Invalid NULL value in #sreq = %p\n"
			"sreq->key_group_intf =   %p\n"
			"sreq->key_group_stat_r = %p\n"
			"sreq->key_group_stat_i = %p",
			sreq,
			sreq->key_group_intf,
			sreq->key_group_stat_r,
			sreq->key_group_stat_i
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	if (0 == sreq->key_val_concat)
	{
		if
		(
			(NULL == sreq->val_group_intf)		||
			(NULL == sreq->val_group_stat_r)	||
			(NULL == sreq->val_group_stat_i)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #sreq = %p\n"
				"sreq->key_val_concat =   %d\n"
				"sreq->val_group_intf =   %p\n"
				"sreq->val_group_stat_r = %p\n"
				"sreq->val_group_stat_i = %p",
				sreq,
				sreq->key_val_concat,
				sreq->val_group_intf,
				sreq->val_group_stat_r,
				sreq->val_group_stat_i
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	if (sreq->key_dyna_size_en)
	{
		if
		(
			(NULL == sreq->key_bytes_intf)		||
			(NULL == sreq->key_bytes_stat_r)	||
			(NULL == sreq->key_bytes_stat_i)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #sreq = %p\n"
				"sreq->key_dyna_size_en = %d\n"
				"sreq->key_bytes_intf =   %p\n"
				"sreq->key_bytes_stat_r = %p\n"
				"sreq->key_bytes_stat_i = %p",
				sreq,
				sreq->key_dyna_size_en,
				sreq->key_bytes_intf,
				sreq->key_bytes_stat_r,
				sreq->key_bytes_stat_i
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}

	else
	{
		if (0 == sreq->key_stat_size)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #sreq = %p\n"
				"sreq->key_dyna_size_en = %d\n"
				"sreq->key_stat_size =    %zu\n",
				sreq,
				sreq->key_dyna_size_en,
				sreq->key_stat_size
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	if (sreq->val_dyna_size_en)
	{
		if
		(
			(NULL == sreq->val_bytes_intf)		||
			(NULL == sreq->val_bytes_stat_r)	||
			(NULL == sreq->val_bytes_stat_i)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #sreq = %p\n"
				"sreq->val_dyna_size_en = %d\n"
				"sreq->val_bytes_intf =   %p\n"
				"sreq->val_bytes_stat_r = %p\n"
				"sreq->val_bytes_stat_i = %p",
				sreq,
				sreq->val_dyna_size_en,
				sreq->val_bytes_intf,
				sreq->val_bytes_stat_r,
				sreq->val_bytes_stat_i
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}

	else
	{
		if (0 == sreq->val_stat_size)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #sreq = %p\n"
				"sreq->val_dyna_size_en = %d\n"
				"sreq->val_stat_size =    %zu\n",
				sreq,
				sreq->val_dyna_size_en,
				sreq->val_stat_size
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Perform a sanity check on #dreq

	if
	(
		(NULL == dreq->key_group_dyna_r)	||
		(NULL == dreq->key_group_dyna_i)
	)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_INTF_L_DEBUG,
			"Invalid NULL value in #dreq = %p\n"
			"dreq->key_group_dyna_r = %p\n"
			"dreq->key_group_dyna_i = %p",
			dreq,
			dreq->key_group_dyna_r,
			dreq->key_group_dyna_i
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	if (0 == sreq->key_val_concat)
	{
		if
		(
			(NULL == dreq->val_group_dyna_r)	||
			(NULL == dreq->val_group_dyna_i)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #dreq = %p\n"
				"sreq->key_val_concat =   %d\n"
				"dreq->val_group_dyna_r = %p\n"
				"dreq->val_group_dyna_i = %p",
				dreq,
				sreq->key_val_concat,
				dreq->val_group_dyna_r,
				dreq->val_group_dyna_i
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	if (sreq->key_dyna_size_en)
	{
		if
		(
			(NULL == dreq->key_bytes_dyna_r)	||
			(NULL == dreq->key_bytes_dyna_i)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #dreq = %p\n"
				"sreq->key_dyna_size_en = %d\n"
				"dreq->key_bytes_dyna_r = %p\n"
				"dreq->key_bytes_dyna_i = %p",
				dreq,
				sreq->key_dyna_size_en,
				dreq->key_bytes_dyna_r,
				dreq->key_bytes_dyna_i
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}

	if (sreq->val_dyna_size_en)
	{
		if
		(
			(NULL == dreq->val_bytes_dyna_r)	||
			(NULL == dreq->val_bytes_dyna_i)
		)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_INTF_L_DEBUG,
				"Invalid field combination in #dreq = %p\n"
				"sreq->val_dyna_size_en = %d\n"
				"dreq->val_bytes_dyna_r = %p\n"
				"dreq->val_bytes_dyna_i = %p",
				dreq,
				sreq->val_dyna_size_en,
				dreq->val_bytes_dyna_r,
				dreq->val_bytes_dyna_i
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//If the #*_dyna_r_basis fields in #dreq were used, initially copy them
	//over every associated #*_dyna_r field.
	//
	//If no basis is given for a particular field, then instead initialize
	//these structs with default values.

	if (NULL != dreq->key_group_dyna_r_basis)
	{
		*(dreq->key_group_dyna_r) = *(dreq->key_group_dyna_r_basis);
	}

	else
	{
		*(dreq->key_group_dyna_r) =(const SK_BoxDynaReqs) {0};
	}


	if (0 == sreq->key_val_concat)
	{
		if (NULL != dreq->val_group_dyna_r_basis)
		{
			*(dreq->val_group_dyna_r) = *(dreq->val_group_dyna_r_basis);
		}

		else
		{
			*(dreq->val_group_dyna_r) = (const SK_BoxDynaReqs) {0};
		}
	}

	else
	{
		if (NULL != dreq->val_group_dyna_r)
		{
			*(dreq->val_group_dyna_r) = (const SK_BoxDynaReqs) {0};
		}

		if (NULL != dreq->val_group_dyna_i)
		{
			*(dreq->val_group_dyna_i) = (const SK_BoxDynaImpl) {0};
		}
	}


	if (sreq->key_dyna_size_en)
	{
		if (NULL != dreq->key_bytes_dyna_r_basis)
		{
			*(dreq->key_bytes_dyna_r) = *(dreq->key_bytes_dyna_r_basis);
		}

		else
		{
			*(dreq->key_bytes_dyna_r) = (const SK_BoxDynaReqs) {0};
		}
	}

	else
	{
		if (NULL != dreq->key_bytes_dyna_r)
		{
			*(dreq->key_bytes_dyna_r) = (const SK_BoxDynaReqs) {0};
		}

		if (NULL != dreq->key_bytes_dyna_i)
		{
			*(dreq->key_bytes_dyna_i) = (const SK_BoxDynaImpl) {0};
		}
	}


	if (sreq->val_dyna_size_en)
	{
		if (NULL != dreq->val_bytes_dyna_r_basis)
		{
			*(dreq->val_bytes_dyna_r) = *(dreq->val_bytes_dyna_r_basis);
		}

		else
		{
			*(dreq->val_bytes_dyna_r) = (const SK_BoxDynaReqs) {0};
		}
	}

	else
	{
		if (NULL != dreq->val_bytes_dyna_r)
		{
			*(dreq->val_bytes_dyna_r) = (const SK_BoxDynaReqs) {0};
		}

		if (NULL != dreq->val_bytes_dyna_i)
		{
			*(dreq->val_bytes_dyna_i) = (const SK_BoxDynaImpl) {0};
		}
	}


	//There are no fields in #SK_BoxDynaReqs that must be set to particular
	//values to ensure proper operation of #SK_HmapIntf, so unlike
	//SK_HmapStatReqs_box_calc (), none of the #*_dyna_r fields need any
	//further calculation here.


	//Now that all the #*_dyna_r fields are properly set, all of the
	//#*_dyna_i fields can be computed now.

	SK_BoxDynaImpl_prep
	(
		sreq->key_group_stat_r,
		sreq->key_group_stat_i,
		dreq->key_group_dyna_r,
		dreq->key_group_dyna_i
	);

	SK_BoxDynaImpl_prep
	(
		sreq->val_group_stat_r,
		sreq->val_group_stat_i,
		dreq->val_group_dyna_r,
		dreq->val_group_dyna_i
	);

	SK_BoxDynaImpl_prep
	(
		sreq->key_bytes_stat_r,
		sreq->key_bytes_stat_i,
		dreq->key_bytes_dyna_r,
		dreq->key_bytes_dyna_i
	);

	SK_BoxDynaImpl_prep
	(
		sreq->val_bytes_stat_r,
		sreq->val_bytes_stat_i,
		dreq->val_bytes_dyna_r,
		dreq->val_bytes_dyna_i
	);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Prints out the contents of an #SK_HmapStatReqs to a given file.
 *
 *  @param stat_r		The #SK_HmapStatReqs to print
 *
 *  @param p_depth		How many layers deep should sub-structures be
 *				printed, if there are any.
 *
 *				If this is 0 or less, than this function will
 *				not print out anything.
 *
 *  @param file			FILE * for the file to print to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_HmapStatReqs_print
(
	const SK_HmapStatReqs *	stat_r,
	i64			p_depth,
	FILE *			f_ptr,
	SK_LineDeco		deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_r, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//Respect the requested print depth

	if (0 == p_depth)
	{
		return SlySr_get (SLY_SUCCESS);
	}

	i64 next_depth = SK_p_depth_next (p_depth);


	SK_LineDeco inner_deco = deco;

	inner_deco.pre_str_cnt += 1;


	SlySr sr = SlySr_get (SLY_SUCCESS);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_val_concat =           %d", stat_r->key_val_concat);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_dyna_size_en =         %d", stat_r->key_dyna_size_en);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_dyna_size_en =         %d", stat_r->val_dyna_size_en);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_stat_size =            %zu", stat_r->key_stat_size);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_stat_size =            %zu", stat_r->val_stat_size);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_zero_extend =          %d", stat_r->key_zero_extend);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_intf =           %p", stat_r->key_group_intf);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_intf =           %p", stat_r->val_group_intf);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_intf =           %p", stat_r->key_bytes_intf);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_intf =           %p", stat_r->val_bytes_intf);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_stat_r_basis =   %p", stat_r->key_group_stat_r_basis);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	if (NULL != stat_r->key_group_stat_r_basis)
	{
		SK_BoxStatReqs_print (stat_r->key_group_stat_r_basis, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);
	}


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_stat_r_basis =   %p", stat_r->val_group_stat_r_basis);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	if (NULL != stat_r->val_group_stat_r_basis)
	{
		SK_BoxStatReqs_print (stat_r->val_group_stat_r_basis, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);
	}


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_stat_r_basis =   %p", stat_r->key_bytes_stat_r_basis);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	if (NULL != stat_r->key_bytes_stat_r_basis)
	{
		SK_BoxStatReqs_print (stat_r->key_bytes_stat_r_basis, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);
	}


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_stat_r_basis =   %p", stat_r->val_bytes_stat_r_basis);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	if (NULL != stat_r->val_bytes_stat_r_basis)
	{
		SK_BoxStatReqs_print (stat_r->val_bytes_stat_r_basis, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);
	}


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_stat_r =         %p", stat_r->key_group_stat_r);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxStatReqs_print (stat_r->key_group_stat_r, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_stat_i =         %p", stat_r->key_group_stat_i);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxStatImpl_print (stat_r->key_group_stat_i, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_stat_r =         %p", stat_r->val_group_stat_r);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxStatReqs_print (stat_r->val_group_stat_r, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_stat_i =         %p", stat_r->val_group_stat_i);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxStatImpl_print (stat_r->val_group_stat_i, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_stat_r =         %p", stat_r->key_bytes_stat_r);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxStatReqs_print (stat_r->key_bytes_stat_r, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_stat_i =         %p", stat_r->key_bytes_stat_i);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxStatImpl_print (stat_r->key_bytes_stat_i, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_stat_r =         %p", stat_r->val_bytes_stat_r);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxStatReqs_print (stat_r->val_bytes_stat_r, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_stat_i =         %p", stat_r->val_bytes_stat_i);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxStatImpl_print (stat_r->val_bytes_stat_i, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	return sr;
}




/*!
 *  Prints out the contents of an #SK_HmapStatImpl to a given file.
 *
 *  @param stat_i		The #SK_HmapStatImpl to print
 *
 *  @param p_depth		How many layers deep should sub-structures be
 *				printed, if there are any.
 *
 *				If this is 0 or less, than this function will
 *				not print out anything.
 *
 *  @param file			FILE * for the file to print to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_HmapStatImpl_print
(
	const SK_HmapStatImpl *	stat_i,
	i64			p_depth,
	FILE *			f_ptr,
	SK_LineDeco		deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, stat_i, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//Respect the requested print depth

	if (0 == p_depth)
	{
		return SlySr_get (SLY_SUCCESS);
	}

	//i64 next_depth = SK_p_depth_next (p_depth);


	SlySr sr = SlySr_get (SLY_SUCCESS);


	//sr = SK_LineDeco_print_opt (f_ptr, deco, "invalid_stat_r =           %d", stat_i->invalid_stat_r);
	//
	//SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_inst_stat_mem =  %zu", stat_i->key_group_inst_stat_mem);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_inst_stat_mem =  %zu", stat_i->val_group_inst_stat_mem);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_inst_stat_mem =  %zu", stat_i->key_bytes_inst_stat_mem);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_inst_stat_mem =  %zu", stat_i->val_bytes_inst_stat_mem);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_anc_support =    %d", stat_i->key_group_anc_support);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_anc_support =    %d", stat_i->val_group_anc_support);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_anc_support =    %d", stat_i->key_bytes_anc_support);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_anc_support =    %d", stat_i->val_bytes_anc_support);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_anc_size =       %zu", stat_i->key_group_anc_size);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_anc_size =       %zu", stat_i->val_group_anc_size);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_anc_size =       %zu", stat_i->key_bytes_anc_size);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_anc_size =       %zu", stat_i->val_bytes_anc_size);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	return sr;
}




/*!
 *  Prints out the contents of an #SK_HmapDynaReqs to a given file.
 *
 *  @param dyna_r		The #SK_HmapDynaReqs to print
 *
 *  @param p_depth		How many layers deep should sub-structures be
 *				printed, if there are any.
 *
 *				If this is 0 or less, than this function will
 *				not print out anything.
 *
 *  @param file			FILE * for the file to print to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_HmapDynaReqs_print
(
	const SK_HmapDynaReqs *	dyna_r,
	i64			p_depth,
	FILE *			f_ptr,
	SK_LineDeco		deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, dyna_r, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//Respect the requested print depth

	if (0 == p_depth)
	{
		return SlySr_get (SLY_SUCCESS);
	}

	i64 next_depth = SK_p_depth_next (p_depth);


	SK_LineDeco inner_deco = deco;

	inner_deco.pre_str_cnt += 1;


	SlySr sr = SlySr_get (SLY_SUCCESS);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_zero_extend =          %d", dyna_r->val_zero_extend);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	//sr = SK_LineDeco_print_opt (f_ptr, deco, "dyna_key_dest_reset =      %d", dyna_r->dyna_key_dest_reset);

	//SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	//sr = SK_LineDeco_print_opt (f_ptr, deco, "dyna_val_dest_reset =      %d", dyna_r->dyna_val_dest_reset);

	//SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_dyna_r_basis =   %p", dyna_r->key_group_dyna_r_basis);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	if (NULL != dyna_r->key_group_dyna_r_basis)
	{
		SK_BoxDynaReqs_print (dyna_r->key_group_dyna_r_basis, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);
	}


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_dyna_r_basis =   %p", dyna_r->val_group_dyna_r_basis);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	if (NULL != dyna_r->val_group_dyna_r_basis)
	{
		SK_BoxDynaReqs_print (dyna_r->val_group_dyna_r_basis, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);
	}


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_dyna_r_basis =   %p", dyna_r->key_bytes_dyna_r_basis);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	if (NULL != dyna_r->key_bytes_dyna_r_basis)
	{
		SK_BoxDynaReqs_print (dyna_r->key_bytes_dyna_r_basis, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);
	}


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_dyna_r_basis =   %p", dyna_r->val_bytes_dyna_r_basis);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	if (NULL != dyna_r->val_bytes_dyna_r_basis)
	{
		SK_BoxDynaReqs_print (dyna_r->val_bytes_dyna_r_basis, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);
	}


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_dyna_r =         %p", dyna_r->key_group_dyna_r);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxDynaReqs_print (dyna_r->key_group_dyna_r, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_dyna_i =         %p", dyna_r->key_group_dyna_i);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxDynaImpl_print (dyna_r->key_group_dyna_i, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_dyna_r =         %p", dyna_r->val_group_dyna_r);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxDynaReqs_print (dyna_r->val_group_dyna_r, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_dyna_i =         %p", dyna_r->val_group_dyna_i);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxDynaImpl_print (dyna_r->val_group_dyna_i, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_dyna_r =         %p", dyna_r->key_bytes_dyna_r);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxDynaReqs_print (dyna_r->key_bytes_dyna_r, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_dyna_i =         %p", dyna_r->key_bytes_dyna_i);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxDynaImpl_print (dyna_r->key_bytes_dyna_i, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_dyna_r =         %p", dyna_r->val_bytes_dyna_r);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxDynaReqs_print (dyna_r->val_bytes_dyna_r, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_dyna_i =         %p", dyna_r->val_bytes_dyna_i);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	SK_BoxDynaImpl_print (dyna_r->val_bytes_dyna_i, next_depth, f_ptr, inner_deco);

	SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


	return sr;
}




/*!
 *  Prints out the contents of an #SK_HmapDynaImpl to a given file.
 *
 *  @param dyna_i		The #SK_HmapDynaImpl to print
 *
 *  @param p_depth		How many layers deep should sub-structures be
 *				printed, if there are any.
 *
 *				If this is 0 or less, than this function will
 *				not print out anything.
 *
 *  @param file			FILE * for the file to print to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_HmapDynaImpl_print
(
	const SK_HmapDynaImpl *	dyna_i,
	i64			p_depth,
	FILE *			f_ptr,
	SK_LineDeco		deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, dyna_i, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//Respect the requested print depth

	if (0 == p_depth)
	{
		return SlySr_get (SLY_SUCCESS);
	}

	i64 next_depth = SK_p_depth_next (p_depth);


	SK_LineDeco inner_deco = deco;

	inner_deco.pre_str_cnt += 1;


	SlySr sr = SlySr_get (SLY_SUCCESS);


	if (0 != next_depth)
	{
		sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_req =");

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


		sr = SK_BoxReqs_print (&(dyna_i->key_group_req), next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_req =");

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


		sr = SK_BoxReqs_print (&(dyna_i->val_group_req), next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_req =");

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


		sr = SK_BoxReqs_print (&(dyna_i->key_bytes_req), next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_req =");

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


		sr = SK_BoxReqs_print (&(dyna_i->val_bytes_req), next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);
	}

	else
	{
		sr = SK_LineDeco_print_opt (f_ptr, deco, "key_group_req =            {...}");

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, deco, "val_group_req =            {...}");

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, deco, "key_bytes_req =            {...}");

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, deco, "val_bytes_req =            {...}");

		SK_RES_RETURN (SK_HMAP_INTF_L_DEBUG, sr.res, sr);
	}


	return sr;
}




/*!
 *  Prints out the contents of an #SK_HmapReqs to a given file.
 *
 *  @param req			The #SK_HmapReqs to print
 *
 *  @param p_depth		How many layers deep should sub-structures be
 *				printed, if there are any.
 *
 *				If this is 0 or less, than this function will
 *				not print out anything.
 *
 *  @param file			FILE * for the file to print to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

//FIXME : Similar to what needs to be done for SK_BoxReqs_print (), later make
//a version of this function that prints to #SK_Str

SlySr SK_HmapReqs_print
(
	const SK_HmapReqs *	req,
	i64			p_depth,
	FILE *			f_ptr,
	SK_LineDeco		deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_HMAP_INTF_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//Respect the requested print depth

	if (0 == p_depth)
	{
		return SlySr_get (SLY_SUCCESS);
	}

	i64 next_depth = SK_p_depth_next (p_depth);


	//Create #SK_LineDeco's for the different indentation levels needed
	//here

	SK_LineDeco outer_deco = deco;


	SK_LineDeco inner_deco = deco;

	inner_deco.pre_str_cnt += 1;


	//Print the contents of the #SK_HmapReqs

	SlySr sr = SlySr_get (SLY_SUCCESS);


	if (0 != next_depth)
	{
		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "stat_r @ %p =", req->stat_r);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "{");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_HmapStatReqs_print (req->stat_r, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "}");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "stat_i @ %p =", req->stat_i);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "{");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_HmapStatImpl_print (req->stat_i, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "}");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "dyna_r @ %p =", req-> dyna_r);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "{");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_HmapDynaReqs_print (req->dyna_r, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "}");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "dyna_i @ %p =", req->dyna_i);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "{");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_HmapDynaImpl_print (req->dyna_i, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "}");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);
	}

	else
	{
		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "stat_r @ %p = {...}", req->stat_r);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "stat_i @ %p = {...}", req->stat_i);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "dyna_r @ %p = {...}", req->dyna_r);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "dyna_i @ %p = {...}", req->dyna_i);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);
	}


	return sr;
}




/*!
 *  Performs a simple test-bench on SK_HmapStatReqs_box_calc ().
 *
 *  @warning
 *  This test-bench requires manual verification
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStatReqs_box_calc_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco
)
{

	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);


	//Sanity checks on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlyDr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//These #SK_BoxStatReqs and #SK_BoxStatImpl instances will be used in
	//#SK_HmapStatReqs instances

	SK_BoxStatReqs key_group_stat_r_basis = (const SK_BoxStatReqs) {0};

	SK_BoxStatReqs val_group_stat_r_basis = (const SK_BoxStatReqs) {0};

	SK_BoxStatReqs key_bytes_stat_r_basis = (const SK_BoxStatReqs) {0};

	SK_BoxStatReqs val_bytes_stat_r_basis = (const SK_BoxStatReqs) {0};


	SK_BoxStatReqs key_group_stat_r = (const SK_BoxStatReqs) {0};

	SK_BoxStatReqs val_group_stat_r = (const SK_BoxStatReqs) {0};

	SK_BoxStatReqs key_bytes_stat_r = (const SK_BoxStatReqs) {0};

	SK_BoxStatReqs val_bytes_stat_r = (const SK_BoxStatReqs) {0};


	SK_BoxStatImpl key_group_stat_i = (const SK_BoxStatImpl) {0};

	SK_BoxStatImpl val_group_stat_i = (const SK_BoxStatImpl) {0};

	SK_BoxStatImpl key_bytes_stat_i = (const SK_BoxStatImpl) {0};

	SK_BoxStatImpl val_bytes_stat_i = (const SK_BoxStatImpl) {0};


	//Initialize some fields of the #*_basis instances to non-zero values,
	//to see if they are respected by SK_HmapStatReqs_box_calc ()

	key_group_stat_r_basis.mem = 				&SK_MemStd;
	key_group_stat_r_basis.hint_elems_per_block =		0;
	key_group_stat_r_basis.hint_num_stat_elems =		3;

	val_group_stat_r_basis.mem = 				&SK_MemStd;
	val_group_stat_r_basis.hint_elems_per_block =		1;
	val_group_stat_r_basis.hint_num_stat_elems =		2;

	key_bytes_stat_r_basis.mem = 				&SK_MemStd;
	key_bytes_stat_r_basis.hint_elems_per_block =		2;
	key_bytes_stat_r_basis.hint_num_stat_elems =		1;

	val_bytes_stat_r_basis.mem = 				&SK_MemStd;
	val_bytes_stat_r_basis.hint_elems_per_block =		3;
	val_bytes_stat_r_basis.hint_num_stat_elems =		0;


	//Associate all of the previous created #SK_BoxStatReqs and
	//#SK_BoxStatImpl instances to an #SK_HmapStatReqs

	SK_HmapStatReqs req = (const SK_HmapStatReqs) {0};


	req.key_group_stat_r_basis = &key_group_stat_r_basis;

	req.val_group_stat_r_basis = &val_group_stat_r_basis;

	req.key_bytes_stat_r_basis = &key_bytes_stat_r_basis;

	req.val_bytes_stat_r_basis = &val_bytes_stat_r_basis;


	req.key_group_stat_r = &key_group_stat_r;

	req.val_group_stat_r = &val_group_stat_r;

	req.key_bytes_stat_r = &key_bytes_stat_r;

	req.val_bytes_stat_r = &val_bytes_stat_r;


	req.key_group_stat_i = &key_group_stat_i;

	req.val_group_stat_i = &val_group_stat_i;

	req.key_bytes_stat_i = &key_bytes_stat_i;

	req.val_bytes_stat_i = &val_bytes_stat_i;


	//Set all of the #SK_BoxIntf fields in #req to #SK_Array_intf

	req.key_group_intf = &SK_Array_intf;

	req.val_group_intf = &SK_Array_intf;

	req.key_bytes_intf = &SK_Array_intf;

	req.val_bytes_intf = &SK_Array_intf;


	//Make it so that #req would require the usage of ALL of its
	//#SK_BoxStatReqs and #SK_BoxStatImpl fields, and then attempt to use
	//SK_HmapStatReqs_box_calc ()

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "Test 0 - All fields required");
		SK_LineDeco_print_opt (file, deco, "");
	}


	req.key_val_concat =	0;
	req.key_dyna_size_en =	1;
	req.val_dyna_size_en =	1;
	req.key_stat_size =	0;
	req.val_stat_size =	0;


	SlyDr dr = SK_HmapStatReqs_box_calc (&req);


	if (print_flag)
	{
		SK_HmapStatReqs_print (&req, SK_P_DEPTH_INF, file, deco);

		SK_LineDeco_print_opt (file, deco, "");
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
		SK_LineDeco_print_opt (file, deco, "");
	}


	//Reset all of the modified #*_stat_r and #*_stat_i fields,
	//make it so that #req would NOT require the usage of #val_group_intf,
	//and then attempt to use SK_HmapStatReqs_box_calc ()

	key_group_stat_r = (const SK_BoxStatReqs) {0};

	val_group_stat_r = (const SK_BoxStatReqs) {0};

	key_bytes_stat_r = (const SK_BoxStatReqs) {0};

	val_bytes_stat_r = (const SK_BoxStatReqs) {0};


	key_group_stat_i = (const SK_BoxStatImpl) {0};

	val_group_stat_i = (const SK_BoxStatImpl) {0};

	key_bytes_stat_i = (const SK_BoxStatImpl) {0};

	val_bytes_stat_i = (const SK_BoxStatImpl) {0};


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "Test 1 - #val_group_intf ignored");
		SK_LineDeco_print_opt (file, deco, "");
	}


	req.key_val_concat =	1;
	req.key_dyna_size_en =	1;
	req.val_dyna_size_en =	1;
	req.key_stat_size =	0;
	req.val_stat_size =	0;


	dr = SK_HmapStatReqs_box_calc (&req);


	if (print_flag)
	{
		SK_HmapStatReqs_print (&req, SK_P_DEPTH_INF, file, deco);

		SK_LineDeco_print_opt (file, deco, "");
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
		SK_LineDeco_print_opt (file, deco, "");
	}


	//Reset all of the modified #*_stat_r and #*_stat_i fields, make it so
	//that #req would NOT require the usage of #val_group_intf or
	//#key_bytes_intf, and then attempt to use SK_HmapStatReqs_box_calc ()

	key_group_stat_r = (const SK_BoxStatReqs) {0};

	val_group_stat_r = (const SK_BoxStatReqs) {0};

	key_bytes_stat_r = (const SK_BoxStatReqs) {0};

	val_bytes_stat_r = (const SK_BoxStatReqs) {0};


	key_group_stat_i = (const SK_BoxStatImpl) {0};

	val_group_stat_i = (const SK_BoxStatImpl) {0};

	key_bytes_stat_i = (const SK_BoxStatImpl) {0};

	val_bytes_stat_i = (const SK_BoxStatImpl) {0};


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "Test 2 - #val_group_intf and #key_bytes_intf ignored");
		SK_LineDeco_print_opt (file, deco, "");
	}


	req.key_val_concat =	1;
	req.key_dyna_size_en =	0;
	req.val_dyna_size_en =	1;
	req.key_stat_size =	100;
	req.val_stat_size =	0;


	dr = SK_HmapStatReqs_box_calc (&req);


	if (print_flag)
	{
		SK_HmapStatReqs_print (&req, SK_P_DEPTH_INF, file, deco);

		SK_LineDeco_print_opt (file, deco, "");
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
		SK_LineDeco_print_opt (file, deco, "");
	}


	//Reset all of the modified #*_stat_r and #*_stat_i fields, make it so
	//that #req would NOT require the usage of #val_group_intf,
	//#key_bytes_intf, or #val_bytes_intf, and then attempt to use
	//SK_HmapStatReqs_box_calc ()

	key_group_stat_r = (const SK_BoxStatReqs) {0};

	val_group_stat_r = (const SK_BoxStatReqs) {0};

	key_bytes_stat_r = (const SK_BoxStatReqs) {0};

	val_bytes_stat_r = (const SK_BoxStatReqs) {0};


	key_group_stat_i = (const SK_BoxStatImpl) {0};

	val_group_stat_i = (const SK_BoxStatImpl) {0};

	key_bytes_stat_i = (const SK_BoxStatImpl) {0};

	val_bytes_stat_i = (const SK_BoxStatImpl) {0};


	if (print_flag)
	{
		SK_LineDeco_print_opt
		(
			file,
			deco,
			"Test 3 - #val_group_intf, #key_bytes_intf, and #val_bytes_intf ignored"
		);

		SK_LineDeco_print_opt (file, deco, "");
	}


	req.key_val_concat =	1;
	req.key_dyna_size_en =	0;
	req.val_dyna_size_en =	0;
	req.key_stat_size =	100;
	req.val_stat_size =	200;


	dr = SK_HmapStatReqs_box_calc (&req);


	if (print_flag)
	{
		SK_HmapStatReqs_print (&req, SK_P_DEPTH_INF, file, deco);

		SK_LineDeco_print_opt (file, deco, "");
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
		SK_LineDeco_print_opt (file, deco, "");
	}


	//Reset all of the modified #*_stat_r and #*_stat_i fields, make it so
	//that #req is INVALID because #req->val_stat_size is (0) when
	//#req->val_dyna_size is (0), and then attempt to use
	//SK_HmapStatReqs_box_calc ()

	key_group_stat_r = (const SK_BoxStatReqs) {0};

	val_group_stat_r = (const SK_BoxStatReqs) {0};

	key_bytes_stat_r = (const SK_BoxStatReqs) {0};

	val_bytes_stat_r = (const SK_BoxStatReqs) {0};


	key_group_stat_i = (const SK_BoxStatImpl) {0};

	val_group_stat_i = (const SK_BoxStatImpl) {0};

	key_bytes_stat_i = (const SK_BoxStatImpl) {0};

	val_bytes_stat_i = (const SK_BoxStatImpl) {0};


	if (print_flag)
	{
		SK_LineDeco_print_opt
		(
			file,
			deco,
			"Test 4 - Invalid #val_stat_size"
		);

		SK_LineDeco_print_opt (file, deco, "");
	}


	req.key_val_concat =	1;
	req.key_dyna_size_en =	0;
	req.val_dyna_size_en =	0;
	req.key_stat_size =	100;
	req.val_stat_size =	0;


	dr = SK_HmapStatReqs_box_calc (&req);


	if (print_flag)
	{
		SK_HmapStatReqs_print (&req, SK_P_DEPTH_INF, file, deco);

		SK_LineDeco_print_opt (file, deco, "");
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
		SK_LineDeco_print_opt (file, deco, "");
	}


	//Reset all of the modified #*_stat_r and #*_stat_i fields, make it so
	//that #req is INVALID because #req->key_val_concat is (0) when
	//#req->val_group_intf is (NULL), and then attempt to use
	//SK_HmapStatReqs_box_calc ()

	key_group_stat_r = (const SK_BoxStatReqs) {0};

	val_group_stat_r = (const SK_BoxStatReqs) {0};

	key_bytes_stat_r = (const SK_BoxStatReqs) {0};

	val_bytes_stat_r = (const SK_BoxStatReqs) {0};


	key_group_stat_i = (const SK_BoxStatImpl) {0};

	val_group_stat_i = (const SK_BoxStatImpl) {0};

	key_bytes_stat_i = (const SK_BoxStatImpl) {0};

	val_bytes_stat_i = (const SK_BoxStatImpl) {0};


	if (print_flag)
	{
		SK_LineDeco_print_opt
		(
			file,
			deco,
			"Test 5 - Invalid #*_intf values"
		);

		SK_LineDeco_print_opt (file, deco, "");
	}


	req.key_val_concat =	0;
	req.key_dyna_size_en =	1;
	req.val_dyna_size_en =	1;
	req.key_stat_size =	0;
	req.val_stat_size =	0;


	req.val_group_intf =	NULL;
	req.key_bytes_intf =	NULL;
	req.val_bytes_intf =	NULL;


	dr = SK_HmapStatReqs_box_calc (&req);


	if (print_flag)
	{
		SK_HmapStatReqs_print (&req, SK_P_DEPTH_INF, file, deco);

		SK_LineDeco_print_opt (file, deco, "");
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
		SK_LineDeco_print_opt (file, deco, "");
	}


	//If this section was reached, then the test-bench was successful

	int error_detected = 0;

	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlyDr_get (SLY_UNKNOWN_ERROR),
		SlyDr_get (SLY_SUCCESS)
	);
}



