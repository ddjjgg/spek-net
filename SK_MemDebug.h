/*!****************************************************************************
 *
 * @file
 * SK_MemDebug.h
 *
 * An implementation of #SK_MemIntf very similar to #SK_MemStd, except that the
 * memory allocating functions within it can be made to fail intentionally.
 *
 * This can be useful for debugging code paths where memory allocations fail
 * WITHOUT having to actually consume all available memory.
 *
 *****************************************************************************/




#ifndef SK_MEMDEBUG_H
#define SK_MEMDEBUG_H




#include "SK_MemIntf.h"




extern int SK_MemDebug_break_allocators;

extern int SK_MemDebug_break_take;

extern int SK_MemDebug_break_retake;

extern int SK_MemDebug_break_take_near;

extern int SK_MemDebug_break_take_soft;

extern int SK_MemDebug_break_take_hard;




extern const SK_MemIntf SK_MemDebug;




#endif //SK_MEMDEBUG_H

