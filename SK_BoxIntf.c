/*!****************************************************************************
 *
 * @file
 * SK_BoxIntf.c
 *
 * See SK_BoxIntf.h for details.
 *
 *****************************************************************************/




#include <omp.h>
#include <time.h>




#include "SK_BoxIntf.h"




#include "./SK_Array.h"
#include "./SK_debug.h"
#include "./SK_MemDebug.h"
#include "./SK_misc.h"




//FIXME : Added a function that can check if a #SK_BoxReqs makes sense; namely,
//if #prim_flag is 1, are either #prim_copy or #prim_swap NULL?




//FIXME : Add a function to generate a "standard" #SK_BoxReqs based off a given
//element size




//FIXME : Update this file to use (SK_Box *) pointers instead of (void *)
//pointers where appropriate




/*!
 *  Makes an #SK_BoxReqs have all its member pointers point to the contents
 *  of an #SK_BoxAllReqs.
 *
 *  @warning
 *  SK_BoxReqs_prep () will still be necessary to call on the resulting #req
 *  UNLESS SK_BoxAllReqs_prep () has already been used on #arq.
 *
 *  @param req			The #SK_BoxReqs that should have all its
 *  				fields point to fields in #arq
 *
 *  @param arq			The #SK_BoxAllReqs that should have all
 *				of its fields pointed to by #req
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxReqs_point_to_arq
(
	SK_BoxReqs *		req,

	const SK_BoxAllReqs *	arq
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, arq, SlyDr_get (SLY_BAD_ARG));

	req->stat_r = (const SK_BoxStatReqs *)		&(arq->stat_r);
	req->stat_i = (SK_BoxStatImpl *)		&(arq->stat_i);
	req->dyna_r = (const SK_BoxDynaReqs *)		&(arq->dyna_r);
	req->dyna_i = (SK_BoxDynaImpl *)		&(arq->dyna_i);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Calculates the values within an #SK_BoxStatImpl based off of the values
 *  contained within an #SK_BoxStatReqs.
 *
 *  @note
 *  Note how #SK_BoxDynaReqs and #SK_BoxDynaImpl are not referenced in
 *  this function. This is because letting a dynamic parameter determine a
 *  static parameter is dangerous, though the reverse is acceptable.
 *
 *  @warning
 *  If #stat_r is detected as representing an impossible configuration, this
 *  function will fail and return without modifiying #stat_i.
 *
 *  @param stat_r		The #SK_BoxStatReqs that represents the
 *  				requested static parameters of an #SK_Box.
 *
 *				This MUST be already initialized.
 *
 *  @param stat_i		Where to place the #SK_BoxStatImpl that
 *  				will be calculated based off of #stat_r.
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxStatImpl_prep
(
	const SK_BoxStatReqs *	stat_r,

	SK_BoxStatImpl *	stat_i
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, stat_i, SlyDr_get (SLY_BAD_ARG));


	stat_i->prim_flag = SK_BoxStatImpl_sel_prim_flag (stat_r->elem_size);

	stat_i->prim_copy = SK_BoxStatImpl_sel_prim_copy (stat_r->elem_size);

	stat_i->prim_swap = SK_BoxStatImpl_sel_prim_swap (stat_r->elem_size);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Calculates the values within an #SK_BoxDynaImpl based off of the values
 *  contained within an #SK_BoxDynaReqs.
 *
 *  @warning
 *  If #stat_r, #stat_i, or #dyna_r, are detected as representing an impossible
 *  configuration, this function will fail and return without modifiying
 *  #dyna_i.
 *
 *  @param stat_r		The #SK_BoxStatReqs that represents the
 *  				requested static parameters of an #SK_Box.
 *
 *				This MUST be already initialized.
 *
 *  @param stat_i		The #SK_BoxStatImpl that was previously
 *				calculated using SK_BoxStatImpl_prep ().
 *
 *				This MUST be already initialized.
 *
 *  @param dyna_r		The #SK_BoxDynaReqs that represents the
 *				requested dynamic parameters of an #SK_Box.
 *
 *				This MUST be already initialized.
 *
 *  @param dyna_i		Where to place the #SK_BoxDynaImpl that
 *  				will be calculated based off of #stat_r,
 *  				#stat_i, and #dyna_r.
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxDynaImpl_prep
(
	const SK_BoxStatReqs *	stat_r,

	const SK_BoxStatImpl *	stat_i,

	const SK_BoxDynaReqs *	dyna_r,

	SK_BoxDynaImpl *	dyna_i
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, stat_i, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, dyna_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, dyna_i, SlyDr_get (SLY_BAD_ARG));


	dyna_i->r = *dyna_r;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Prepares an #SK_BoxReqs to be used by setting its pointers and
 *  initializing the "implied" values.
 *
 *  @warning
 *  If the pointers #stat_r, #stat_i, #dyna_r, or #dyna_i become invalid while
 *  #req is being used, then memory access errors WILL occur.
 *
 *  @warning
 *  If #stat_r or #dyna_r are detected as requesting an impossible
 *  configuration, this function will fail and return without modifiying #req,
 *  #stat_i, or #dyna_i.
 *
 *  @param req			Pointer to the #SK_BoxReqs to prepare,
 *				which is NOT expected to be initialized.
 *
 *  				(Regardless if the pointers inside of it are
 *  				already set, they will be set to (stat_r,
 *  				stat_i, dyna_r, dyna_i) respectively.)
 *
 *				This CAN be NULL if this argument is not
 *				desired, in which case the other arguments will
 *				simply be prepared.
 *
 *  @param stat_r		Pointer to the #SK_BoxStatReqs to use,
 *				which IS expected to be manually initialized.
 *
 *  @param stat_i		Pointer to the #SK_BoxStatImpl to use, which
 *  				is NOT expected to be intialized, but will be
 *  				initalized after this function completes.
 *
 *  @param dyna_r		Pointer to the #SK_BoxDynaReqs to use, which
 *  				IS expected to be manually initialized.
 *
 *  @param dyna_i		Pointer to the #SK_BoxDynaImpl to use, which
 *				is NOT expected to be intialized, but will be
 *				initalized after this function completes.
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxReqs_prep
(
	SK_BoxReqs *			req,

	const SK_BoxStatReqs *		stat_r,

	SK_BoxStatImpl *		stat_i,

	const SK_BoxDynaReqs *		dyna_r,

	SK_BoxDynaImpl *		dyna_i
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, stat_i, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, dyna_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, dyna_i, SlyDr_get (SLY_BAD_ARG));


	//Calculate #stat_i from #stat_r

	SlyDr dr = SK_BoxStatImpl_prep (stat_r, stat_i);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


	//Calculate #dyna_i from #stat_r, #stat_i, and #dyna_r

	dr = SK_BoxDynaImpl_prep (stat_r, stat_i, dyna_r, dyna_i);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


	//Set the pointers inside of #req

	if (NULL != req)
	{
		req->stat_r = stat_r;

		req->stat_i = stat_i;

		req->dyna_r = dyna_r;

		req->dyna_i = dyna_i;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Prepares an #SK_BoxAllReqs by initializing the "implied" values.
 *
 *  This makes it so that if #SK_BoxReqs_point_to_arq () is used to make an
 *  #SK_BoxReqs point to the contents of #arq, then the resulting
 *  #SK_BoxReqs will already be prepared.
 *
 *  @param arq			The #SK_BoxAllReqs to prepare. This is NOT
 *				expected to be initialized yet.
 *
 *  @param stat_r		Pointer to the #SK_BoxStatReqs to use, which
 *				IS expected to be initialized.
 *
 *				(arq->stat_r) will be set equal to this.
 *
 *				This argument CAN be equal to &(arq->stat_r),
 *				provided that it is initialized.
 *
 *  @param dyna_r		Pointer to the #SK_BoxDynaReqs to use, which
 *				IS expected to be initialized.
 *
 *				(arq->dyna_r) will be set equal to this.
 *
 *				This argument CAN be equal to &(arq->dyna_r),
 *				provided that it is intiialized.
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxAllReqs_prep
(
	SK_BoxAllReqs *			arq,

	const SK_BoxStatReqs *		stat_r,

	const SK_BoxDynaReqs *		dyna_r
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, arq, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, dyna_r, SlyDr_get (SLY_BAD_ARG));


	//Copy the value (arq->stat_r) and (arq->dyna_r), if necessary

	if (&(arq->stat_r) != stat_r)
	{
		arq->stat_r = *stat_r;
	}


	if (&(arq->dyna_r) != dyna_r)
	{
		arq->dyna_r = *dyna_r;
	}


	//Make use of SK_BoxReqs_prep () to prepare #arq

	return

	SK_BoxReqs_prep
	(
		NULL,
		&(arq->stat_r),
		&(arq->stat_i),
		&(arq->dyna_r),
		&(arq->dyna_i)
	);
}




/*!
 *  Selects what value #SK_BoxStatReqs.prim_flag should have for a given
 *  #elem_size.
 *
 *  @param elem_size		The value of elem_size in the same
 *				#SK_BoxStatReqs as #prim_flag
 *
 *  @return			What the value of #prim_flag should be
 */

int SK_BoxStatImpl_sel_prim_flag
(
	size_t		elem_size
)
{

	//This comes from the fact that the largest-word version of
	//SK_byte_copy_b* () is SK_byte_copy_b8 ()

	if (elem_size > 8)
	{
		return 0;
	}


	{
		return 1;
	}
}




/*!
 *  Selects what value #SK_BoxStatReqs.prim_copy should have for a givem
 *  #elem_size.
 *
 *  @param elem_size		The value of elem_size in the same
 *				#SK_BoxStatReqs as #prim_copy
 *
 *  @return			What the value of #prim_copy should be
 */

SK_BoxIntf_prim_copy_func SK_BoxStatImpl_sel_prim_copy
(
	size_t		elem_size
)
{
	if ((0 >= elem_size) || (8 < elem_size))
	{
		return NULL;
	}


	SK_BoxIntf_prim_copy_func funcs [8]

	=

	{
		SK_byte_copy_b1,
		SK_byte_copy_b2,
		SK_byte_copy_b3,
		SK_byte_copy_b4,
		SK_byte_copy_b5,
		SK_byte_copy_b6,
		SK_byte_copy_b7,
		SK_byte_copy_b8
	};


	return funcs [elem_size - 1];
}




/*!
 *  Selects what value #SK_BoxStatReqs.prim_swap should have for a givem
 *  #elem_size.
 *
 *  @param elem_size		The value of elem_size in the same
 *				#SK_BoxStatReqs as #prim_swap
 *
 *  @return			What the value of #prim_swap should be
 */

SK_BoxIntf_prim_swap_func SK_BoxStatImpl_sel_prim_swap
(
	size_t		elem_size
)
{
	if ((0 >= elem_size) || (8 < elem_size))
	{
		return NULL;
	}


	SK_BoxIntf_prim_swap_func funcs [8]

	=

	{
		SK_byte_swap_b1,
		SK_byte_swap_b2,
		SK_byte_swap_b3,
		SK_byte_swap_b4,
		SK_byte_swap_b5,
		SK_byte_swap_b6,
		SK_byte_swap_b7,
		SK_byte_swap_b8
	};


	return funcs [elem_size - 1];
}




/*!
 *  Prints out the contents of an #SK_BoxStatReqs to a given file.
 *
 *  @param stat_r		The #SK_BoxStatReqs to print
 *
 *  @param p_depth		How many layers deep should sub-structures be
 *				printed, if there are any.
 *
 *				If this is 0 or less, than this function will
 *				not print out anything, and return
 *				#SLY_SUCCESS.
 *
 *  @param file			FILE * for the file to print to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_BoxStatReqs_print
(
	const SK_BoxStatReqs *		stat_r,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, stat_r, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//Respect the requested print depth

	if (0 == p_depth)
	{
		return SlySr_get (SLY_SUCCESS);
	}


	SlySr sr = SlySr_get (SLY_SUCCESS);


	//FIXME : Should #mem be printed out in detail here? It would only be
	//filled with function pointers.

	sr = SK_LineDeco_print_opt (f_ptr, deco, "mem @                  %p", stat_r->mem);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "elem_size =            %zu", stat_r->elem_size);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "hint_num_stat_elems =  %zu", stat_r->hint_num_stat_elems);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "hint_elems_per_block = %zu", stat_r->hint_elems_per_block);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	return sr;
}




/*!
 *  Prints out the contents of an #SK_BoxStatImpl to a given file.
 *
 *  @param bsim			The #SK_BoxStatImpl to print
 *
 *  @param p_depth		How many layers deep should sub-structures be
 *				printed, if there are any.
 *
 *				If this is 0 or less, than this function will
 *				not print out anything.
 *
 *  @param file			FILE * for the file to print to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_BoxStatImpl_print
(
	const SK_BoxStatImpl *		bsim,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, bsim, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//Respect the requested print depth

	if (0 == p_depth)
	{
		return SlySr_get (SLY_SUCCESS);
	}


	SlySr sr = SlySr_get (SLY_SUCCESS);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "prim_flag =            %d", bsim->prim_flag);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "prim_copy @            %p", bsim->prim_copy);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco, "prim_swap @            %p", bsim->prim_swap);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	return sr;
}




/*!
 *  Prints out the contents of an #SK_BoxDynaReqs to a given file.
 *
 *  @param dyna_r		The #SK_BoxDynaReqs to print
 *
 *  @param p_depth		How many layers deep should sub-structures be
 *				printed, if there are any.
 *
 *				If this is 0 or less, than this function will
 *				not print out anything.
 *
 *  @param file			FILE * for the file to print to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_BoxDynaReqs_print
(
	const SK_BoxDynaReqs *		dyna_r,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, dyna_r, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//Respect the requested print depth

	if (0 == p_depth)
	{
		return SlySr_get (SLY_SUCCESS);
	}


	SlySr sr = SlySr_get (SLY_SUCCESS);


	sr = SK_LineDeco_print_opt (f_ptr, deco,"hint_neg_slack =               %zu", dyna_r->hint_neg_slack);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco,"hint_pos_slack =               %zu", dyna_r->hint_pos_slack);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco,"hint_use_max_num_dyna_blocks = %d", (int) dyna_r->hint_use_max_num_dyna_blocks);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	sr = SK_LineDeco_print_opt (f_ptr, deco,"hint_max_num_dyna_blocks =     %zu", dyna_r->hint_max_num_dyna_blocks);

	SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


	return sr;
}




/*!
 *  Prints out the contents of an #SK_BoxDynaImpl to a given file.
 *
 *  @param dyna_i		The #SK_BoxDynaImpl to print
 *
 *  @param p_depth		How many layers deep should sub-structures be
 *				printed, if there are any.
 *
 *				If this is 0 or less, than this function will
 *				not print out anything.
 *
 *  @param file			FILE * for the file to print to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_BoxDynaImpl_print
(
	const SK_BoxDynaImpl *		dyna_i,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, dyna_i, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//Respect the requested print depth

	if (0 == p_depth)
	{
		return SlySr_get (SLY_SUCCESS);
	}

	i64 next_depth = SK_p_depth_next (p_depth);


	//Create #SK_LineDeco's for the different indentation levels needed
	//here

	SK_LineDeco outer_deco = deco;


	SK_LineDeco inner_deco = deco;

	inner_deco.pre_str_cnt += 1;


	//Print the contents of #bsim

	SlySr sr = SlySr_get (SLY_SUCCESS);


	if (0 != next_depth)
	{
		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "r =");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "{");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_BoxDynaReqs_print (&(dyna_i->r), next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "}");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);

	}

	else
	{
		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "r = {...}");
	}


	return sr;
}




/*!
 *  Prints out the contents of an #SK_BoxReqs to a given file.
 *
 *  @param req			The #SK_BoxReqs to print
 *
 *  @param p_depth		How many layers deep should sub-structures be
 *				printed, if there are any.
 *
 *				If this is 0 or less, than this function will
 *				not print out anything.
 *
 *  @param file			FILE * for the file to print to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *				printed lines
 *
 *  @return			Standard status code
 */

//FIXME : For the printing functions in general, consider a design where the
//string is generated 1st, the #SK_LineDeco is applied to the generated
//string 2nd, and then the decorated string is printed out 3rd. This will allow
//the re-use of logic for general purpose string generation.
//
//Right now, this print-directly-to-the-file-for-every-line works okay, but it
//is annoying to read / write AND it is inefficient with the file access split
//over multiple calls.
//
//In other words, finish #SK_StrIntf, and print to #SK_Str's.


//FIXME : Update this function for the recent updates to #SK_BoxReqs

SlySr SK_BoxReqs_print
(
	const SK_BoxReqs *		req,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));


	//Respect the requested print depth

	if (0 == p_depth)
	{
		return SlySr_get (SLY_SUCCESS);
	}

	i64 next_depth = SK_p_depth_next (p_depth);


	//Create #SK_LineDeco's for the different indentation levels needed
	//here

	SK_LineDeco outer_deco = deco;


	SK_LineDeco inner_deco = deco;

	inner_deco.pre_str_cnt += 1;


	//Print the contents of the #SK_BoxReqs

	SlySr sr = SlySr_get (SLY_SUCCESS);


	if (0 != next_depth)
	{
		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "stat_r @ %p =", req->stat_r);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "{");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_BoxStatReqs_print (req->stat_r, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "}");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "stat_i @ %p =", req->stat_i);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "{");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_BoxStatImpl_print (req->stat_i, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "}");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "dyna_r @ %p =", req-> dyna_r);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "{");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_BoxDynaReqs_print (req->dyna_r, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "}");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "dyna_i @ %p =", req->dyna_i);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "{");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_BoxDynaImpl_print (req->dyna_i, next_depth, f_ptr, inner_deco);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (f_ptr, outer_deco, "}");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);
	}

	else
	{
		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "stat_r @ %p = {...}", req->stat_r);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "stat_i @ %p = {...}", req->stat_i);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "dyna_r @ %p = {...}", req->dyna_r);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr =  SK_LineDeco_print_opt (f_ptr, outer_deco, "dyna_i @ %p = {...}", req->dyna_i);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);
	}


	return sr;
}




//FIXME : Is there really no better way of checking if 2 structs are equal to
//one another in C? Note that memcmp () does not always work for 2 structs on
//the stack, due to memory padding for alignment reasons.

/*!
 *  Simply checks if 2 #SK_BoxStatReqs are equal to each other.
 *
 *  @warning
 *  This is a "shallow" comparison; any pointer sub-fields will simply be
 *  checked if they point to the same memory region, not whether if the
 *  contents at both regions are the same.
 *
 *  @param sr0			The first #SK_BoxStatReqs to compare
 *
 *  @param sr1			The second #SK_BoxStatReqs to compare
 *
 *  @param eq			Where to place a flag that will be 1 if
 *  				#sr0 and #sr1 are equal, and 0
 *  				otherwise
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxStatReqs_shlw_eq
(
	const SK_BoxStatReqs *	sr0,

	const SK_BoxStatReqs *	sr1,

	int *			eq
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, sr0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, sr1, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, eq, SlyDr_get (SLY_BAD_ARG));


	//Compare #sr0 and #sr1 field-by-field

	if
	(
		(sr0->mem !=			sr1->mem)			||
		(sr0->elem_size !=		sr1->elem_size)			||
		(sr0->hint_num_stat_elems !=	sr1->hint_num_stat_elems)	||
		(sr0->hint_elems_per_block !=	sr1->hint_elems_per_block)
		//(sr0->req_reg !=		sr1->req_reg)
	)
	{
		*eq = 0;
	}

	else
	{
		*eq = 1;
	}

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Simply checks if 2 #SK_BoxStatImpl are equal to each other.
 *
 *  @warning
 *  This is a "shallow" comparison; any pointer sub-fields will simply be
 *  checked if they point to the same memory region, not whether if the
 *  contents at both regions are the same.
 *
 *  @param si0			The first #SK_BoxStatImpl to compare
 *
 *  @param si1			The second #SK_BoxStatReqs to compare
 *
 *  @param eq			Where to place a flag that will be 1 if
 *  				#si0 and #si1 are equal, and 0
 *  				otherwise
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxStatImpl_shlw_eq
(
	const SK_BoxStatImpl *	si0,

	const SK_BoxStatImpl *	si1,

	int *			eq
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, si0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, si1, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, eq, SlyDr_get (SLY_BAD_ARG));


	//Compare #si0 and #si1 field-by-field

	if
	(
		(si0->prim_flag != si1->prim_flag) ||
		(si0->prim_copy != si1->prim_copy) ||
		(si0->prim_swap != si1->prim_swap)
	)
	{
		*eq = 0;
	}

	else
	{
		*eq = 1;
	}

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Simply checks if 2 #SK_BoxDynaReqs are equal to each other.
 *
 *  @warning
 *  This is a "shallow" comparison; any pointer sub-fields will simply be
 *  checked if they point to the same memory region, not whether if the
 *  contents at both regions are the same.
 *
 *  @param dr0			The first #SK_BoxDynaReqs to compare
 *
 *  @param dr1			The second #SK_BoxDynaReqs to compare
 *
 *  @param eq			Where to place a flag that will be 1 if
 *  				#dr0 and #dr1 are equal, and 0
 *  				otherwise
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxDynaReqs_shlw_eq
(
	const SK_BoxDynaReqs *	dr0,

	const SK_BoxDynaReqs *	dr1,

	int *			eq
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, dr0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, dr1, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, eq, SlyDr_get (SLY_BAD_ARG));


	//Compare #dr0 and #dr1 field-by-field

	if
	(
		(dr0->hint_neg_slack !=			dr1->hint_neg_slack)			||
		(dr0->hint_pos_slack !=			dr1->hint_pos_slack)			||
		(dr0->hint_use_max_num_dyna_blocks !=	dr1->hint_use_max_num_dyna_blocks) 	||
		(dr0->hint_max_num_dyna_blocks !=	dr1->hint_max_num_dyna_blocks)		||
		(dr0->hint_tree_avg_degree !=		dr1->hint_tree_avg_degree)
	)
	{
		*eq = 0;
	}

	else
	{
		*eq = 1;
	}

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Simply checks if 2 #SK_BoxDynaImpl are equal to each other.
 *
 *  @warning
 *  This is a "shallow" comparison; any pointer sub-fields will simply be
 *  checked if they point to the same memory region, not whether if the
 *  contents at both regions are the same.
 *
 *  @param di0			The first #SK_BoxDynaImpl to compare
 *
 *  @param di1			The second #SK_BoxDynaImpl to compare
 *
 *  @param eq			Where to place a flag that will be 1 if
 *  				#di0 and #di1 are equal, and 0
 *  				otherwise
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxDynaImpl_shlw_eq
(
	const SK_BoxDynaImpl *	di0,

	const SK_BoxDynaImpl *	di1,

	int *			eq
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, di0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, di1, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, eq, SlyDr_get (SLY_BAD_ARG));


	//Compare #di0 and #di1 field-by-field

	int r_eq = 0;

	SK_BoxDynaReqs_shlw_eq (&(di0->r), &(di1->r), &r_eq);


	//Note, feel free to reformat this section; this written this way for
	//consistency with the other functions.

	if
	(
		(0 == r_eq)
	)
	{
		*eq = 0;
	}

	else
	{
		*eq = 1;
	}

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Simply checks if 2 #SK_BoxAllReqs are equal to each other.
 *
 *  @warning
 *  This is a "shallow" comparison; any pointer sub-fields will simply be
 *  checked if they point to the same memory region, not whether if the
 *  contents at both regions are the same.
 *
 *  @param ar0			The first #SK_BoxAllReqs to compare
 *
 *  @param ar1			The second #SK_BoxAllReqs to compare
 *
 *  @param eq			Where to place a flag that will be 1 if
 *  				#ar0 and #ar1 are equal, and 0
 *  				otherwise
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxAllReqs_shlw_eq
(
	const SK_BoxAllReqs *	ar0,

	const SK_BoxAllReqs *	ar1,

	int *			eq
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, ar0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, ar1, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, eq, SlyDr_get (SLY_BAD_ARG));


	//Compare #ar0 and #ar1 field-by-field

	int stat_r_eq = 0;
	int stat_i_eq = 0;
	int dyna_r_eq = 0;
	int dyna_i_eq = 0;

	SK_BoxStatReqs_shlw_eq (&(ar0->stat_r), &(ar1->stat_r), &stat_r_eq);
	SK_BoxStatImpl_shlw_eq (&(ar0->stat_i), &(ar1->stat_i), &stat_i_eq);
	SK_BoxDynaReqs_shlw_eq (&(ar0->dyna_r), &(ar1->dyna_r), &dyna_r_eq);
	SK_BoxDynaImpl_shlw_eq (&(ar0->dyna_i), &(ar1->dyna_i), &dyna_i_eq);

	if
	(
		(0 == stat_r_eq) ||
		(0 == stat_i_eq) ||
		(0 == dyna_r_eq) ||
		(0 == dyna_i_eq)
	)
	{
		*eq = 0;
	}

	else
	{
		*eq = 1;
	}

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Simply checks if 2 #SK_BoxReqs are equal to each other.
 *
 *  @warning
 *  This is a "shallow" comparison; any pointer sub-fields will simply be
 *  checked if they point to the same memory region, not whether if the
 *  contents at both regions are the same.
 *
 *  @warning
 *  Note that because both are shallow comparisons, SK_BoxReqs_shlw_eq () and
 *  SK_BoxAllReqs_shlw_eq () behave differently, because #SK_BoxReqs directly
 *  has pointer fields while #SK_BoxAllReqs does not.
 *
 *  @param r0			The first #SK_BoxReqs to compare
 *
 *  @param r1			The second #SK_BoxReqs to compare
 *
 *  @param eq			Where to place a flag that will be 1 if
 *  				#r0 and #r1 are equal, and 0
 *  				otherwise
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxReqs_shlw_eq
(
	const SK_BoxReqs *	r0,

	const SK_BoxReqs *	r1,

	int *			eq
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, r0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, r1, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, eq, SlyDr_get (SLY_BAD_ARG));


	//Compare #r0 and #r1 field-by-field

	if
	(
		(r0->stat_r != r1->stat_r) ||
		(r0->stat_i != r1->stat_i) ||
		(r0->dyna_r != r1->dyna_r) ||
		(r0->dyna_i != r1->dyna_i)
	)
	{
		*eq = 0;
	}

	else
	{
		*eq = 1;
	}

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Performs an element-by-element assignment from 1 storage object to another
 *  storage object. After this operation, both storage objects should have the
 *  same number of elements and the same element values at the same indices.
 *
 *  This function makes it possible to easily copy from arrays / vectors /
 *  linked-lists / etc. to arrays / vectors / linked-lists / etc. without
 *  having to worry about the specifics of the storage data structures at all.
 *
 *  Imagine there were 10 different storage data structures available. Without
 *  a function like this, there would have to be (10 x 10) = 100 functions that
 *  would have to be written to make it possible to copy from anything to
 *  anything. Even for small values, it gets out-of-hand quickly!
 *
 *  @param dst_intf		The implementation of #SK_BoxIntf to use
 *				for the destination storage object
 *
 *  @param dst_req		See init () in #SK_BoxIntf. This is for
 *				the destination storage object.
 *
 *  @param dst_obj		The destination storage object. This object
 *				MUST be already initialized with
 *				(dst->intf.init ())
 *
 *  @param src_intf		The implementation of #SK_BoxIntf to use
 *				for the source storage object
 *
 *  @param src_req		See init () in #SK_BoxIntf. This is for
 *				the source storage object.
 *
 *  @param src_obj		The source storage object. This object
 *				MUST be already initialized with
 *				(src->intf.init ())
 *
 *  @return			Standard status code
 *
 */

SlySr SK_Box_assign
(
	const SK_BoxIntf *		dst_intf,

	const SK_BoxReqs *		dst_req,

	void *				dst_obj,

	const SK_BoxIntf *		src_intf,

	const SK_BoxReqs *		src_req,

	void *				src_obj
);

//FIXME : Implement this function





/*!
 *  Reports the amount of memory needed to store the serialization of a given
 *  storage object's elements.
 */

SlyDr SK_Box_srlz_elems_mem
(
	const SK_BoxIntf *		intf,

	const SK_BoxReqs *		req,

	const void *			obj,

	size_t *			size
);

//FIXME : Implement this function




/*!
 *  Serializes a given storage object's elements.
 *
 *  FIXME : Should the serialized version of storage objects contain
 *  information other than just the number of elements and the value of the
 *  elements themselves? For example, should blocks allocated as slack be
 *  included? Perhaps there should be a separate (?) serialize function that
 *  saves all the "extra" information? Keep in mind, that information would
 *  almost necessarily be implementation specific.
 *
 *  FIXME : Should there be a version of this function that allows for
 *  serializing a subset of the elements stored in #obj?
 */

SlyDr SK_Box_srlz_elems
(
	const SK_BoxIntf *		intf,

	const SK_BoxReqs *		req,

	const void *			obj,

	size_t *			data
);

//FIXME : Implement this function




/*!
 *  Deserializes a given storage object.
 */

SlySr SK_Box_desrlz_elems
(
	const SK_BoxIntf *		intf,

	const SK_BoxReqs *		req,

	const void *			data,

	void *				obj
);

//FIXME : Implement this function




//FIXME : Should there be a function SK_Box_save_to_file () that
//handles saving #SK_BoxIntf's to files, or is that a broad enough problem
//that requires something dedicated to it (a *.c file full of functions, a
//file-saving interface)? Perhaps even if the problem is more advanced than
//that, there's no harm in providing simplistic file saving / loading
//functions. (??? is this true?)




/*!
 *  Provides an #SK_BoxIntf_elem_rw_func that writes bytes of 0's over the
 *  target element.
 *
 *  This is provided to make zero'ing out elements easier.
 *
 *  @warning
 *  Since this function performs a byte-by-byte initialization of an element,
 *  it can be much faster to use an #SK_BoxIntf_elem_rw_func that is catered to
 *  a specific data type. (Is this true? Maybe make use of #prim_copy here
 *  somehow?)
 *
 *  For information about the parameters, see #SK_BoxIntf_elem_rw_func.
 *
 *  @param arb_arg		Unused argument
 */

SlyDr SK_Box_elem_rw_set_zero
(
	const SK_BoxReqs * restrict	req,

	void * restrict			elem_ptr,

	size_t				index,

	void * restrict			arb_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));


	//Note, #arb_arg IS allowed to be NULL, so no check is performed on it


	return SK_byte_set_all (elem_ptr, 0, req->stat_r->elem_size);
}




/*!
 *  Provides an #SK_BoxIntf_elem_rw_func that copies (req->stat_r->elem_size)
 *  number of bytes located at #arb_arg into #elem_ptr.
 *
 *  @warning
 *  Since this function performs a byte-by-byte initialization of an element,
 *  it can be much faster to use an #SK_BoxIntf_elem_rw_func that is catered to
 *  a specific data type. (Is this true? Maybe make use of #prim_copy?)
 *
 *  For information about the parameters, see #SK_BoxIntf_elem_rw_func.
 *
 *  @param arb_arg		A pointer to where to copy #elem_size number of
 *				bytes from
 *
 */

SlyDr SK_Box_elem_rw_set_w_arb_arg
(
	const SK_BoxReqs * restrict	req,

	void * restrict			elem_ptr,

	size_t				index,

	void * restrict			arb_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, arb_arg, SlyDr_get (SLY_BAD_ARG));


	//Copy #elem_size number of bytes from #arb_arg to #elem_ptr

	//FIXME : Should this use the copy function referenced by #req?

	return SK_byte_copy (elem_ptr, arb_arg, req->stat_r->elem_size);
}




/*!
 *  Provides an #SK_BoxIntf_elem_rw_func that initializes the elements of the
 *  storage object to its index.
 *
 *  So, for example, the element at index 0 would have value 0, the element at
 *  index 1 would have value 1, the element at index 2 would have value 2, etc.
 *  etc.
 *
 *  If the elements do not have enough bytes for the index, the index will be
 *  truncated.
 *
 *  If the elements contain more bytes than necessary for the index, the extra
 *  bytes will be zeroed out.
 *
 *  For information about the parameters, see #SK_BoxIntf_elem_rw_func.
 *
 *  @param arb_arg			Unused argument
 */

SlyDr SK_Box_elem_rw_set_ind
(
	const SK_BoxReqs * restrict	req,

	void * restrict			elem_ptr,

	size_t				index,

	void * restrict			arb_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));


	if (sizeof (size_t) >= req->stat_r->elem_size)
	{
		size_t copy_len = SK_MIN (sizeof (size_t), req->stat_r->elem_size);

		SK_byte_copy (elem_ptr, &index, copy_len);
	}

	else
	{
		size_t zero_len = req->stat_r->elem_size - sizeof (size_t);

		SK_byte_copy (elem_ptr, &index, sizeof (size_t));

		SK_byte_set_all (((u8 *) elem_ptr) + sizeof (size_t), 0, zero_len);
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Provides an #SK_BoxIntf_elem_rw_func that sets all elements it accesses to
 *  the same #f64 value.
 *
 *  For information about the parameters, see #SK_BoxIntf_elem_rw_func.
 *
 *  @param arb_arg			Pointer to the #f64 value to set the
 *					provided element to
 */

SlyDr SK_Box_elem_rw_set_f64
(
	const SK_BoxReqs * restrict	req,

	void * restrict			elem_ptr,

	size_t				index,

	void * restrict			arb_arg
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, arb_arg, SlyDr_get (SLY_BAD_ARG));

	if (SK_BOX_INTF_L_DEBUG)
	{
		if (sizeof (f64) != req->stat_r->elem_size)
		{
			SK_DEBUG_PRINT
			(
				SK_BOX_INTF_L_DEBUG,
				"Element is not an #f64, "
				"#req->stat_r->elem_size = %zu",
				req->stat_r->elem_size
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	 * ((f64 *) elem_ptr) = * ((f64 *) arb_arg);

	 return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Calculates the number of element blocks that should be allocated inside of
 *  a storage object given the current number of element blocks and the new
 *  minimum number of blocks it should contain.
 *
 *  This function is useful in implementations of #SK_BoxIntf.resize ().
 *
 *  Importantly, this function DOES take into account negative-slack /
 *  positive-slack. These parameters enable #new_num_blocks to be made equal to
 *  #cur_num_blocks as frequently as possible, which allows for costly resize
 *  operations to be avoided as frequently as possible.
 *
 *  @param req				The #SK_BoxReqs that describes the
 *					parameters of the storage object. This
 *					is where the parameters for
 *					negative-slack and positive-slack will
 *					be taken from.
 *
 *  @param cur_num_blocks		The number of element blocks the
 *  					storage object currently contains. This
 *  					is necessary to know, because it may be
 *					acceptable to leave the number of
 *					blocks as it currently is.
 *
 *					@warning
 *					This is the number of currently
 *					allocated element BLOCKS, not the
 *					number of elements.
 *
 *  @param min_num_blocks		The minimum number of element blocks
 *					that could possibly store the desired
 *					number of elements.
 *
 *					In general, this will be equal to
 *
 *					ceil
 *					(
 *					  (desired_num_elems)
 *
 * 					  /
 *
 *					  (req->stat_r->hint_elems_per_block)
 *					)
 *
 *					Remember, if
 *					(req->stat_r->hint_elems_per_block) is 0,
 *					then the storage object should
 *					substitute whatever value it determines
 *					to be best. (Hence why this function
 *					does not take a "new_num_elems"
 *					argument, because this would not have
 *					enough information to handle that
 *					case.)
 *
 *  @param new_num_blocks		Where to place the number of blocks
 *					that the storage object should contain
 *
 *  @return				Standard status code
 */

SlyDr SK_Box_calc_new_num_blocks
(
	const SK_BoxReqs *	req,

	size_t			cur_num_blocks,
	size_t			min_num_blocks,

	size_t *		new_num_blocks
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, new_num_blocks, SlyDr_get (SLY_BAD_ARG));


	//Determine if more or less blocks need to be allocated to store
	//#new_num_elems number of elements

	if (min_num_blocks < cur_num_blocks)
	{
		//See if the amount of blocks that will be unused is large
		//enough to justify switching to a different number of blocks

		if ((cur_num_blocks - min_num_blocks) > req->dyna_i->r.hint_neg_slack)
		{
			//FIXME : Should positive-slack be added here? Is that
			//appropriate during a size decrease?

			*new_num_blocks = min_num_blocks;
		}

		else
		{
			*new_num_blocks = cur_num_blocks;
		}

	}

	else if (min_num_blocks > cur_num_blocks)
	{
		//More blocks are needed than what are currently available, so
		//use a new number of blocks (with positive slack added to it)

		*new_num_blocks = min_num_blocks + req->dyna_i->r.hint_pos_slack;
	}

	else
	{
		//The current number of blocks is sufficient to store
		//#new_num_elems number of elements

		*new_num_blocks = cur_num_blocks;

	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Performs a comparison between 2 #SK_Box's, in which the elements of each
 *  are treated as a large contiguous group of bytes in little-endian order.
 *
 *  The lowest-indexed byte of the lowest-indexed element makes the
 *  lowest-index byte when the elements are viewed as a contiguous grouping of
 *  bytes.
 *
 *  @warning
 *  If #box_0 and #box_1 have a different number of bytes in total, then the
 *  comparison will proceed as if the #SK_Box with less bytes in total has been
 *  zero-extended on the MSB-side. In little-endian ordering, these are the
 *  highest-indexed bytes.
 *
 *  Note that no elements will be added / removed from either #box_0 or #box_1,
 *  the zero extension is computed without modifiying any #SK_Box's.
 *
 *  @warning
 *  If one of the #SK_Box arguments contains 0 elements, that is logically
 *  equivalent to the value it represents being an infinite string of 0's.
 *
 *  @warning
 *  If #box_0 and #box_1 both contain 0 elements, then they will be considered
 *  equivalent to one another, as they both represent infinite strings of 0's.
 *
 *  @note
 *  While this comparison may seem strange, it finds use in the implementation
 *  of #SK_HmapStd.
 *
 *  @param intf_0		The #SK_BoxIntf to use when accessing #box_0
 *
 *  @param req_0		The #SK_BoxReqs to use when accessing #box_0
 *
 *  @param box_0		An #SK_Box to compare
 *
 *  @param intf_1		The #SK_BoxIntf to use when accessing #box_1
 *
 *  @param req_1		The #SK_BoxReqs to use when accessing #box_1
 *
 *  @param box_1		Another #SK_Box to compare
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (box_0 < box_1)
 *
 *  				 0 means (box_0 == box_1)
 *
 *  				+1 means (box_0 > box_1)
 *
 *  @return			Standard status code
 */

//TODO : Either make a version of this function that accepts anchor arguments,
//or change the implementation of this function so that it creates / uses
//anchors itself (when possible).

SlyDr SK_Box_le_cmp
(
	const SK_BoxIntf *	intf_0,
	const SK_BoxReqs *	req_0,
	SK_Box *		box_0,

	const SK_BoxIntf *	intf_1,
	const SK_BoxReqs *	req_1,
	SK_Box *		box_1,

	int *			res
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, intf_0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req_0, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, box_0, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, intf_1, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req_1, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, box_1, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, res, SlyDr_get (SLY_BAD_ARG));


	//Determine the number of elements that #box_0 and #box_1 store in total

	size_t box_0_num_elems = 0;

	intf_0->get_num_elems
	(
		req_0,
		box_0,
		&box_0_num_elems
	);


	size_t box_1_num_elems = 0;

	intf_1->get_num_elems
	(
		req_1,
		box_1,
		&box_1_num_elems
	);


	//If both #SK_Box's are empty, then indicate that they are equal and
	//return

	if ((0 == box_0_num_elems) && (0 == box_1_num_elems))
	{
		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}


	//Determine which #SK_Box stores more bytes in total, and if necessary,
	//flip the arguments so that #box_0 always represents the bigger box

	size_t box_0_num_bytes = box_0_num_elems * req_0->stat_r->elem_size;

	size_t box_1_num_bytes = box_1_num_elems * req_1->stat_r->elem_size;


	int box_flip = 0;

	if (box_1_num_bytes > box_0_num_bytes)
	{
		const SK_BoxIntf * temp_intf =	intf_0;

		const SK_BoxReqs * temp_req =	req_0;

		SK_Box * temp_box =		box_0;

		size_t temp_num_elems =		box_0_num_elems;

		size_t temp_num_bytes =		box_0_num_bytes;


		intf_0 =		intf_1;

		req_0 =			req_1;

		box_0 =			box_1;

		box_0_num_elems =	box_1_num_elems;

		box_0_num_bytes =	box_1_num_bytes;


		intf_1 =		temp_intf;

		req_1 =			temp_req;

		box_1 =			temp_box;

		box_1_num_elems =	temp_num_elems;

		box_1_num_bytes =	temp_num_bytes;


		//Remember the fact that the #SK_Box's have been swapped

		box_flip = 1;
	}


	//These variables indicate the byte regions that are currently under
	//inspection, and will be updated to reflect that at any given time

	size_t box_0_beg_byte_ind = 0;

	size_t box_0_end_byte_ind = 0;


	size_t box_1_beg_byte_ind = 0;

	size_t box_1_end_byte_ind = 0;


	//Get the contiguous memory region associated with the highest-indexed
	//bytes in #box_0 and #box_1.
	//
	//The comparison considers the byte groups in little-endian order, so
	//the highest-indexed bytes represent the MSB-side of the byte groups.

	size_t box_0_sel_ind =		0;

	size_t box_0_beg_ind =		0;

	size_t box_0_end_ind =		0;


	void * reg_0_ptr =		NULL;

	void * off_0_ptr =		NULL;


	size_t box_1_sel_ind =		0;

	size_t box_1_beg_ind =		0;

	size_t box_1_end_ind =		0;


	void * reg_1_ptr =		NULL;

	void * off_1_ptr =		NULL;


	if (0 < box_0_num_elems)
	{
		box_0_sel_ind = box_0_num_elems - 1;


		//SlyDr dr =

		intf_0->get_region_ptr
		(
			req_0,
			box_0,
			box_0_sel_ind,
			&reg_0_ptr,
			&box_0_beg_ind,
			&box_0_end_ind
		);

		//SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


		box_0_beg_byte_ind =

		box_0_beg_ind * req_0->stat_r->elem_size;


		box_0_end_byte_ind =

		((box_0_end_ind + 1) * req_0->stat_r->elem_size) - 1;
	}


	if (0 < box_1_num_elems)
	{
		box_1_sel_ind = box_1_num_elems - 1;


		//SlyDr dr =

		intf_1->get_region_ptr
		(
			req_1,
			box_1,
			box_1_sel_ind,
			&reg_1_ptr,
			&box_1_beg_ind,
			&box_1_end_ind
		);

		//SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


		box_1_beg_byte_ind =

		box_1_beg_ind * req_1->stat_r->elem_size;


		box_1_end_byte_ind =

		((box_1_end_ind + 1) * req_1->stat_r->elem_size) - 1;
	}


	//If one #SK_Box contained more bytes than the other, than it is
	//necessary to zero-extend the smaller #SK_Box with 0's on the MSB-side
	//for the comparison to be performed.
	//
	//To produce the same effect as zero-extending the smaller #SK_Box, the
	//extra bytes from the larger #SK_Box will be compared to 0. If any of
	//these extra bytes are not 0, then the larger #SK_Box automatically
	//contains the larger value, and the comparison can end.

	//Note that the #SK_Box's were flipped earlier so that #box_0 always
	//represents the larger #SK_Box by this point.

	int need_zero_extend = (box_0_num_bytes > box_1_num_bytes);

	while (need_zero_extend)
	{
		//For the region from #box_0 that was previously retrieved,
		//determine which bytes represent the "extra" bytes.
		//
		//Note that possibly the entire region represents "extra"
		//bytes.

		size_t num_extra_bytes =

		(
			box_0_end_byte_ind

			-

			SK_MAX ((box_1_end_byte_ind + 1), box_0_beg_byte_ind)
		)

		+

		1;


		off_0_ptr =

		((u8 *) reg_0_ptr)

		+

		(
			((box_0_end_byte_ind - box_0_beg_byte_ind) + 1)

			-

			num_extra_bytes
		);


		//Compare the currently selected "extra" bytes to 0

		int eq = 0;


		SK_byte_rep_eq_u8
		(
			off_0_ptr,
			num_extra_bytes,
			0,
			&eq,
			NULL
		);


		if (0 == eq)
		{
			//Some of the "extra" bytes were non-zero, therefore
			//these MSB bytes in #box_0 are automatically larger
			//than the MSB bytes in #box_1 (which is zero-extended)

			*res = 1;

			if (box_flip)
			{
				*res = -1;
			}

			return SlyDr_get (SLY_SUCCESS);
		}


		//Detect if the smaller #SK_Box (box_1) has no elements
		//whatsoever, and therefore the comparisons continued until the
		//larger #SK_Box (box_0) had all but one of its bytes compared
		//to 0.

		if ((0 >= box_0_beg_byte_ind) && (0 >= box_1_num_bytes))
		{
			if (0 == * ((u8 *) reg_0_ptr))
			{
				*res = 0;
			}

			else
			{
				if (box_flip)
				{
					*res = -1;
				}

				else
				{
					*res = +1;
				}
			}

			return SlyDr_get (SLY_SUCCESS);
		}


		//If the currently selected region did not contain all of the
		//"extra" bytes, then move unto the region underneath it.

		if (box_0_beg_byte_ind > box_1_end_byte_ind)
		{
			//Need unto the next region in #box_0

			box_0_sel_ind = box_0_beg_ind - 1;


			//SlyDr dr =

			intf_0->get_region_ptr
			(
				req_0,
				box_0,
				box_0_sel_ind,
				&reg_0_ptr,
				&box_0_beg_ind,
				&box_0_end_ind
			);

			//SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


			box_0_beg_byte_ind =

			box_0_beg_ind * req_0->stat_r->elem_size;


			box_0_end_byte_ind =

			((box_0_end_ind + 1) * req_0->stat_r->elem_size) - 1;
		}

		else
		{
			//The current region contained the last amount of
			//"extra" bytes, therefore the examination of "extra"
			//bytes end.

			// //
			// //#box_0_end_byte_ind is updated to select the bytes
			// //from the current region that have not already been
			// //examined.
			//
			// box_0_end_byte_ind = box_1_end_byte_ind;

			break;
		}
	}


	//Now that all of the "extra" bytes in #box_0 have been checked,
	//compare the bytes that overlap in #box_0 and #box_1.

	while (1)
	{
		//Note, it is necessary to take into account that the selected
		//regions from #box_0 and #box_1 might not be the same size.
		//
		//Ensuring that only the overlapping regions are compared in
		//this loop iteration is necessary.


		//Note that during any loop iteration, all the byte
		//indices above...
		//
		//	SK_MIN (box_0_end_byte_ind, box_1_end_byte_ind)
		//
		//...have already been examined, and do not need to be
		//compared.


		//Determine how much the region pointers must be offset to
		//account for any difference in size between the regions

		if (box_0_beg_byte_ind > box_1_beg_byte_ind)
		{
			//#reg_0_ptr has the smaller unexamined region

			off_0_ptr = reg_0_ptr;


			off_1_ptr =

			((u8 *) reg_1_ptr)

			+

			(box_0_beg_byte_ind - box_1_beg_byte_ind);
		}

		else
		{
			//#reg_1_ptr has the smaller unexamined region

			off_0_ptr =

			((u8 *) reg_0_ptr)

			+

			(box_1_beg_byte_ind - box_0_beg_byte_ind);


			off_1_ptr = reg_1_ptr;
		}


		//Determine the length of the regions that overlaps

		size_t cmp_byte_len =

		(
			SK_MIN (box_0_end_byte_ind, box_1_end_byte_ind)

			-

			SK_MAX (box_0_beg_byte_ind, box_1_beg_byte_ind)
		)

		+

		1;


		//Perform the comparison

		SK_byte_le_cmp
		(
			off_0_ptr,
			off_1_ptr,
			cmp_byte_len,
			res
		);


		//If it is already determined that #box_0 or #box_1 are
		//unequal, then ensure #res indicates which value is
		//smaller / larger, and return

		if (0 != *res)
		{
			//Reverse the result if the arguments were swapped
			//earlier

			if (box_flip)
			{
				*res = (*res) * -1;
			}

			return SlyDr_get (SLY_SUCCESS);
		}


		//Detect if the last possible comparison has been made, and
		//exit the loop if so

		if
		(
			(0 >= box_0_beg_ind)

			&&

			(0 >= box_1_beg_ind)
		)
		{
			break;
		}


		//For the region that had the highest #beg_byte_* index
		//associated with it, move onto the region of bytes
		//"underneath" it.
		//
		//Note that these if-blocks are intentionally written so that
		//it is possible for both to execute if
		//(box_0_beg_byte_ind == box_1_beg_byte_ind).

		if
		(
			(box_0_beg_byte_ind >= box_1_beg_byte_ind)

			&&

			(0 < box_0_beg_byte_ind)
		)
		{
			box_0_sel_ind = box_0_beg_ind - 1;


			//SlyDr dr =

			intf_0->get_region_ptr
			(
				req_0,
				box_0,
				box_0_sel_ind,
				&reg_0_ptr,
				&box_0_beg_ind,
				&box_0_end_ind
			);

			//SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


			box_0_beg_byte_ind =

			box_0_beg_ind * req_0->stat_r->elem_size;


			box_0_end_byte_ind =

			((box_0_end_ind + 1) * req_0->stat_r->elem_size) - 1;
		}

		if
		(
			(box_1_beg_byte_ind >= box_0_beg_byte_ind)

			&&

			(0 < box_1_beg_byte_ind)
		)
		{
			box_1_sel_ind = box_1_beg_ind - 1;


			//SlyDr dr =

			intf_1->get_region_ptr
			(
				req_1,
				box_1,
				box_1_sel_ind,
				&reg_1_ptr,
				&box_1_beg_ind,
				&box_1_end_ind
			);

			//SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


			box_1_beg_byte_ind =

			box_1_beg_ind * req_1->stat_r->elem_size;


			box_1_end_byte_ind =

			((box_1_end_ind + 1) * req_1->stat_r->elem_size) - 1;
		}
	}


	//If this point has been reached, that indicates that #box_0 and #box_1
	//equal according to the comparison. So, simply report that fact and
	//return.

	//Technically, setting #res here is unnecessary, but it shows what #res
	//MUST be at this point in time.

	*res = 0;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Performs a comparison between an #SK_Box and a contiguous memory region. In
 *  this comparison, the elements of the #SK_Box will be treated as a large
 *  contiguous group of bytes in little-endian order. The contiguous memory
 *  region will also be interpreted in little-endian order.
 *
 *  The lowest-indexed byte of the lowest-indexed element makes the
 *  lowest-index byte when the elements are viewed as a contiguous grouping of
 *  bytes.
 *
 *  This function is similar to SK_Box_le_cmp (), except that one of the
 *  #SK_Box's has been replaced with a contiguous memory region.
 *
 *  @warning
 *  If #box and #bytes have a different number of bytes in total, then the
 *  comparison will proceed as if the argument with less bytes in total has
 *  been zero-extended on the MSB-side. In little-endian ordering, these are
 *  the highest-indexed bytes.
 *
 *  Note that no elements will be added / removed from either #box or #bytes,
 *  the zero extension is computed without modifiying #box or #bytes.
 *
 *  @warning
 *  If either #box or #bytes contains 0 elements, that is logically equivalent
 *  to the value it represents being an infinite string of 0's.
 *
 *  @warning
 *  If #box and #bytes both contain 0 elements, then they will be considered
 *  equivalent to one another, as they both represent infinite strings of 0's.
 *
 *  @note
 *  While this comparison may seem strange, it finds use in the implementation
 *  of #SK_HmapStd.
 *
 *  @param intf			The #SK_BoxIntf to use when accessing #box
 *
 *  @param req			The #SK_BoxReqs to use when accessing #box
 *
 *  @param box			An #SK_Box to compare
 *
 *  @param bytes		A contiguous memory region to compare.
 *
 *				This argument CAN be NULL, in which case #bytes
 *				is interpreted as an infinite string of 0's.
 *
 *  @param bytes_len		The number of bytes stored at #bytes.
 *
 *  				If #bytes is NULL, then this argument will be
 *  				ignored.
 *
 *  				If this argument is 0, then #bytes will be
 *  				treated as an infinite string of 0's.
 *
 *  @param res			Where to place the result of the comparison
 *
 *  				-1 means (box < bytes)
 *
 *  				 0 means (box == bytes)
 *
 *  				+1 means (box > bytes)
 *
 *  @return			Standard status code
 */

//TODO : Either make a version of this function that accepts anchor arguments,
//or change the implementation of this function so that it creates / uses
//anchors itself (when possible).

SlyDr SK_Box_bare_le_cmp
(
	const SK_BoxIntf *	intf,
	const SK_BoxReqs *	req,
	SK_Box *		box,

	const void *		bytes,
	size_t			bytes_len,

	int *			res
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, intf, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, box, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, bytes, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, res, SlyDr_get (SLY_BAD_ARG));


	//If #bytes is NULL, that means #bytes_len should be interpreted as 0
	//regardless of whatever value it was originally passed.

	if (NULL == bytes)
	{
		bytes_len = 0;
	}


	//Determine the number of elements that #box stores in total

	size_t box_num_elems = 0;

	intf->get_num_elems
	(
		req,
		box,
		&box_num_elems
	);


	//If both #box and #bytes are empty, then indicate that they are equal
	//and return

	if ((0 == box_num_elems) && (0 == bytes_len))
	{
		*res = 0;

		return SlyDr_get (SLY_SUCCESS);
	}


	//These variables indicate the byte regions that are currently under
	//inspection, and will be updated to reflect that at any given time

	size_t reg_0_beg_byte_ind = 0;

	size_t reg_0_end_byte_ind = 0;


	size_t reg_1_beg_byte_ind = 0;

	size_t reg_1_end_byte_ind = 0;


	//Get the contiguous memory region associated with the highest-indexed
	//bytes in #box.
	//
	//The comparison considers the byte groups in little-endian order, so
	//the highest-indexed bytes represent the MSB-side of the byte groups.

	SK_Box * reg_0_box =		box;

	size_t reg_0_box_num_elems =	box_num_elems;

	size_t reg_0_box_num_bytes =	box_num_elems * req->stat_r->elem_size;

	size_t reg_0_sel_ind =		0;

	size_t reg_0_beg_ind =		0;

	size_t reg_0_end_ind =		0;

	const void * reg_0_ptr =	NULL;

	const void * off_0_ptr =	NULL;


	if (0 < reg_0_box_num_elems)
	{
		reg_0_sel_ind = reg_0_box_num_elems - 1;


		//SlyDr dr =

		intf->get_region_ptr
		(
			req,
			reg_0_box,
			reg_0_sel_ind,
			(void **) (&reg_0_ptr),
			&reg_0_beg_ind,
			&reg_0_end_ind
		);

		//SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


		reg_0_beg_byte_ind =

		reg_0_beg_ind * req->stat_r->elem_size;


		reg_0_end_byte_ind =

		((reg_0_end_ind + 1) * req->stat_r->elem_size) - 1;
	}


	//Treat #bytes as if it is a region taken from an #SK_Box.
	//
	//Why are #reg_1_box and #reg_1_box_num_elems declared here if #bytes
	//is not truly an #SK_Box? It helps "flip" the comparison later when
	//the larger region is determined.

	SK_Box * reg_1_box =		NULL;

	size_t reg_1_box_num_elems =	0;

	size_t reg_1_box_num_bytes =	0;

	size_t reg_1_sel_ind =		0;

	size_t reg_1_beg_ind =		0;

	size_t reg_1_end_ind =		0;

	const void * reg_1_ptr =	bytes;

	const void * off_1_ptr =	bytes;


	if (0 < bytes_len)
	{
		reg_1_box_num_elems =	1;

		reg_1_box_num_bytes =	bytes_len;


		reg_1_end_byte_ind =	bytes_len - 1;
	}


	//Determine which of #box and #bytes store more bytes in total, and if
	//necessary, flip the arguments so that #reg_0_box always represents
	//the bigger box

	int box_flip = 0;

	if (reg_1_box_num_bytes > reg_0_box_num_bytes)
	{
		size_t temp_beg_byte_ind =	reg_0_beg_byte_ind;

		size_t temp_end_byte_ind =	reg_0_end_byte_ind;

		SK_Box * temp_box =		reg_0_box;

		size_t temp_box_num_elems =	reg_0_box_num_elems;

		size_t temp_box_num_bytes =	reg_0_box_num_bytes;

		size_t temp_sel_ind =		reg_0_sel_ind;

		size_t temp_beg_ind =		reg_0_beg_ind;

		size_t temp_end_ind =		reg_0_end_ind;

		const void * temp_reg_ptr =	reg_0_ptr;

		const void * temp_off_ptr =	off_0_ptr;


		reg_0_beg_byte_ind =		reg_1_beg_byte_ind;

		reg_0_end_byte_ind =		reg_1_end_byte_ind;

		reg_0_box =			reg_1_box;

		reg_0_box_num_elems =		reg_1_box_num_elems;

		reg_0_box_num_bytes =	        reg_1_box_num_bytes;

		reg_0_sel_ind =                 reg_1_sel_ind;

		reg_0_beg_ind =                 reg_1_beg_ind;

		reg_0_end_ind =                 reg_1_end_ind;

		reg_0_ptr =                     reg_1_ptr;

		off_0_ptr =                     off_1_ptr;


		reg_1_beg_byte_ind =		temp_beg_byte_ind;

		reg_1_end_byte_ind =		temp_end_byte_ind;

		reg_1_box =			temp_box;

		reg_1_box_num_elems =		temp_box_num_elems;

		reg_1_box_num_bytes =	        temp_box_num_bytes;

		reg_1_sel_ind =                 temp_sel_ind;

		reg_1_beg_ind =                 temp_beg_ind;

		reg_1_end_ind =                 temp_end_ind;

		reg_1_ptr =                     temp_reg_ptr;

		off_1_ptr =                     temp_off_ptr;


		//Remember the fact that the arguments have been swapped

		box_flip = 1;
	}


	//If one argument contained more bytes than the other, than it is
	//necessary to zero-extend the smaller argument with 0's on the
	//MSB-side for the comparison to be performed.
	//
	//To produce the same effect as zero-extending the smaller argument,
	//the extra bytes from the larger argument will be compared to 0. If
	//any of these extra bytes are not 0, then the larger argument
	//automatically contains the larger value, and the comparison can end.

	//Note that the arguments were flipped earlier so that #reg_0_box
	//always represents the larger argument by this point.

	int need_zero_extend = (reg_0_box_num_bytes > reg_1_box_num_bytes);

	while (need_zero_extend)
	{
		//For the region from #reg_0_box that was previously retrieved,
		//determine which bytes represent the "extra" bytes.
		//
		//Note that possibly the entire region represents "extra"
		//bytes.

		size_t num_extra_bytes =

		(
			reg_0_end_byte_ind

			-

			SK_MAX ((reg_1_end_byte_ind + 1), reg_0_beg_byte_ind)
		)

		+

		1;


		off_0_ptr =

		((u8 *) reg_0_ptr)

		+

		(
			((reg_0_end_byte_ind - reg_0_beg_byte_ind) + 1)

			-

			num_extra_bytes
		);


		//Compare the currently selected "extra" bytes to 0

		int eq = 0;

		SK_byte_rep_eq_u8
		(
			off_0_ptr,
			num_extra_bytes,
			0,
			&eq,
			NULL
		);


		if (0 == eq)
		{
			//Some of the "extra" bytes were non-zero, therefore
			//these MSB bytes in #reg_0_box are automatically larger
			//than the MSB bytes in #reg_1_box (which is zero-extended)

			*res = 1;

			if (box_flip)
			{
				*res = -1;
			}

			return SlyDr_get (SLY_SUCCESS);
		}


		//Detect if the smaller #SK_Box (reg_1_box) has no elements
		//whatsoever, and therefore the comparisons continued until the
		//larger #SK_Box (reg_0_box) had all but one of its bytes compared
		//to 0.

		if ((0 >= reg_0_beg_byte_ind) && (0 >= reg_1_box_num_bytes))
		{
			if (0 == * ((u8 *) reg_0_ptr))
			{
				*res = 0;
			}

			else
			{
				if (box_flip)
				{
					*res = -1;
				}

				else
				{
					*res = +1;
				}
			}

			return SlyDr_get (SLY_SUCCESS);
		}


		//If the currently selected region did not contain all of the
		//"extra" bytes, then move unto the region underneath it.

		if (reg_0_beg_byte_ind > reg_1_end_byte_ind)
		{
			//Note that this section will never execute if #bytes
			//is used as #reg_0_ptr, because #reg_0_beg_byte_ind
			//will be 0 in that event.

			//Need unto the next region in #reg_0_box

			reg_0_sel_ind = reg_0_beg_ind - 1;


			//SlyDr dr =

			intf->get_region_ptr
			(
				req,
				reg_0_box,
				reg_0_sel_ind,
				(void **) &reg_0_ptr,
				&reg_0_beg_ind,
				&reg_0_end_ind
			);

			//SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


			reg_0_beg_byte_ind =

			reg_0_beg_ind * req->stat_r->elem_size;


			reg_0_end_byte_ind =

			((reg_0_end_ind + 1) * req->stat_r->elem_size) - 1;
		}

		else
		{
			//The current region contained the last amount of
			//"extra" bytes, therefore the examination of "extra"
			//bytes end.

			// //
			// //#reg_0_end_byte_ind is updated to select the bytes
			// //from the current region that have not already been
			// //examined.
			//
			// reg_0_end_byte_ind = reg_1_end_byte_ind;

			break;
		}
	}


	//Now that all of the "extra" bytes in #reg_0_box have been checked,
	//compare the bytes that overlap in #reg_0_box and #reg_1_box.

	while (1)
	{
		//Note, it is necessary to take into account that the selected
		//regions from #reg_0_box and #reg_1_box might not be the same
		//size.
		//
		//Ensuring that only the overlapping regions are compared in
		//this loop iteration is necessary.


		//Note that during any loop iteration, all the byte
		//indices above...
		//
		//	SK_MIN (reg_0_end_byte_ind, reg_1_end_byte_ind)
		//
		//...have already been examined, and do not need to be
		//compared.


		//Determine how much the region pointers must be offset to
		//account for any difference in size between the regions

		if (reg_0_beg_byte_ind > reg_1_beg_byte_ind)
		{
			//#reg_0_ptr has the smaller unexamined region

			off_0_ptr = reg_0_ptr;


			off_1_ptr =

			((u8 *) reg_1_ptr)

			+

			(reg_0_beg_byte_ind - reg_1_beg_byte_ind);
		}

		else
		{
			//#reg_1_ptr has the smaller unexamined region

			off_0_ptr =

			((u8 *) reg_0_ptr)

			+

			(reg_1_beg_byte_ind - reg_0_beg_byte_ind);


			off_1_ptr = reg_1_ptr;
		}


		//Determine the length of the regions that overlaps

		size_t cmp_byte_len =

		(
			SK_MIN (reg_0_end_byte_ind, reg_1_end_byte_ind)

			-

			SK_MAX (reg_0_beg_byte_ind, reg_1_beg_byte_ind)
		)

		+

		1;


		//Perform the comparison

		SK_byte_le_cmp
		(
			off_0_ptr,
			off_1_ptr,
			cmp_byte_len,
			res
		);


		//If it is already determined that #reg_0_box or #reg_1_box are
		//unequal, then ensure #res indicates which value is
		//smaller / larger, and return

		if (0 != *res)
		{
			//Reverse the result if the arguments were swapped
			//earlier

			if (box_flip)
			{
				*res = (*res) * -1;
			}

			return SlyDr_get (SLY_SUCCESS);
		}


		//Detect if the last possible comparison has been made, and
		//exit the loop if so

		if
		(
			(0 >= reg_0_beg_ind)

			&&

			(0 >= reg_1_beg_ind)
		)
		{
			break;
		}


		//For the region that had the highest #*_beg_byte_ind
		//associated with it, move onto the region of bytes
		//"underneath" it.
		//
		//Note that these if-blocks are intentionally written so that
		//it is possible for both to execute if
		//(reg_0_beg_byte_ind == reg_1_beg_byte_ind).
		//
		//Note, the fact that the #reg_*_beg_byte_ind associated wtih
		//#bytes is guaranteed to be 0 prevents attempts to call
		//get_region_ptr () on the #reg_*_box associated with #bytes.

		if
		(
			(reg_0_beg_byte_ind >= reg_1_beg_byte_ind)

			&&

			(0 < reg_0_beg_byte_ind)
		)
		{
			reg_0_sel_ind = reg_0_beg_ind - 1;


			//SlyDr dr =

			intf->get_region_ptr
			(
				req,
				reg_0_box,
				reg_0_sel_ind,
				(void **) &reg_0_ptr,
				&reg_0_beg_ind,
				&reg_0_end_ind
			);

			//SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


			reg_0_beg_byte_ind =

			reg_0_beg_ind * req->stat_r->elem_size;


			reg_0_end_byte_ind =

			((reg_0_end_ind + 1) * req->stat_r->elem_size) - 1;
		}

		if
		(
			(reg_1_beg_byte_ind >= reg_0_beg_byte_ind)

			&&

			(0 < reg_1_beg_byte_ind)
		)
		{
			reg_1_sel_ind = reg_1_beg_ind - 1;


			//SlyDr dr =

			intf->get_region_ptr
			(
				req,
				reg_1_box,
				reg_1_sel_ind,
				(void **) &reg_1_ptr,
				&reg_1_beg_ind,
				&reg_1_end_ind
			);

			//SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, dr.res, dr);


			reg_1_beg_byte_ind =

			reg_1_beg_ind * req->stat_r->elem_size;


			reg_1_end_byte_ind =

			((reg_1_end_ind + 1) * req->stat_r->elem_size) - 1;
		}
	}


	//If this point has been reached, that indicates that #box_0 and #box_1
	//equal according to the comparison. So, simply report that fact and
	//return.

	//Technically, setting #res here is unnecessary, but it shows what #res
	//MUST be at this point in time.

	*res = 0;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Prints out the bytes of elements associated with an index range in an
 *  #SK_Box.
 *
 *  TODO : Redesign this function after #SK_StrIntf is implemented.
 *
 *  @param sbi		The #SK_BoxIntf associated with #box
 *
 *  @param req		The #SK_BoxReqs associated with #box
 *
 *  @param box		The #SK_Box to retrieve elements from
 *
 *  @param file		The #FILE to print to
 *
 *  @param deco		An #SK_LineDeco that describes how to decorate what
 *			will be printed
 *
 *  @param ind_flag	A flag that indicates whether or not to print out the
 *			indices associated with the elements or not.
 *
 *			0 means the indices should NOT be printed.
 *
 *			1 means the indices SHOULD be printed.
 *
 *  @param beg_ind	The starting index of the elements to print the bytes
 *  			of in #box
 *
 *  @param end_ind	The ending index of the elements to print the bytes
 *  			of in #box
 *
 *  @return		Standard status code
 */

SlySr SK_Box_bytes_print
(
	const SK_BoxIntf *	sbi,

	const SK_BoxReqs *	req,

	SK_Box *		box,

	FILE *			file,

	SK_LineDeco		deco,

	int			ind_flag,

	size_t			beg_ind,

	size_t			end_ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, sbi, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, box, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_BOX_INTF_L_DEBUG, file, SlySr_get (SLY_BAD_ARG));


	if (beg_ind > end_ind)
	{
		SK_DEBUG_PRINT
		(
			SK_BOX_INTF_L_DEBUG,
			"(end_ind = %zu) was less than (beg_ind = %zu)",
			end_ind,
			beg_ind
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	size_t num_elems = 0;

	sbi->get_num_elems (req, box, &num_elems);


	if (end_ind >= num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_BOX_INTF_L_DEBUG,
			"(end_ind = %zu), even though box only has %zu "
			"elements.",
			end_ind,
			num_elems
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	//Print out the bytes of each element in the range [beg_ind, end_ind]

	//TODO : This can be made more efficient by making use of
	//get_region_ptr ()

	SlySr sr = SlySr_get (SLY_SUCCESS);


	SK_LineDeco beg_deco = deco;

	beg_deco.post_str_cnt = 0;


	SK_LineDeco end_deco = deco;

	end_deco.pre_str_cnt = 0;


	for (size_t elem_sel = beg_ind; elem_sel <= end_ind; elem_sel += 1)
	{

		if (ind_flag)
		{
			sr = SK_LineDeco_print_opt (file, beg_deco, "%08zu : ", elem_sel);

			SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);
		}

		else
		{
			sr = SK_LineDeco_print_opt (file, beg_deco, "");

			SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);
		}


		fprintf (file, "0x");


		void * elem_ptr = NULL;

		sbi->get_elem_ptr (req, box, elem_sel, &elem_ptr);


		sr = SK_byte_print (file, elem_ptr, req->stat_r->elem_size);

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);


		sr = SK_LineDeco_print_opt (file, end_deco, "");

		SK_RES_RETURN (SK_BOX_INTF_L_DEBUG, sr.res, sr);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test-bench on SK_Box_calc_new_num_blocks ().
 *
 *  @warning
 *  The results of this test-bench must be manually verified
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @return			Standard status code
 */

SlyDr SK_Box_calc_new_num_blocks_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco
)
{
	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlyDr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Select constants that determine how many parameterizations to test.
	//
	//Note, in these test cases, #neg_slack can be less than or equal to
	//#pos_slack, which as stated in #SK_BoxDynaReqs is generally
	//undesirable in practice.

	const i64 BEG_NEG_SLACK = 0;
	const i64 END_NEG_SLACK = 4;
	const i64 INC_NEG_SLACK = 1;

	const i64 BEG_POS_SLACK = 0;
	const i64 END_POS_SLACK = 4;
	const i64 INC_POS_SLACK = 1;

	const i64 BEG_CUR_NUM_BLOCKS = 0;
	const i64 END_CUR_NUM_BLOCKS = 12;
	const i64 INC_CUR_NUM_BLOCKS = 4;

	const i64 BEG_MIN_NUM_BLOCKS = 0;
	const i64 END_MIN_NUM_BLOCKS = 12;
	const i64 INC_MIN_NUM_BLOCKS = 4;


	i64 neg_slack =			BEG_NEG_SLACK;

	i64 pos_slack =			BEG_POS_SLACK;

	i64 cur_num_blocks =		BEG_CUR_NUM_BLOCKS;

	i64 min_num_blocks =		BEG_MIN_NUM_BLOCKS;


	//Enter a test loop in which moving between different numbers of blocks
	//is simulated

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//FIXME : The fprintf () statements in this test bench do not
		//respect the #deco argument

		fprintf (file, "\n--------\n\n");


		//Break the loop in-between test iterations if a previous
		//iteration of the loop reported an error

		if (error_detected)
		{
			break;
		}


		//Prepare an #SK_BoxReq's that can be fed to
		//SK_Box_calc_new_num_blocks ()
		//
		//Note that the #SK_BoxReqs given here would actually be
		//invalid; only the information necessary to calculate
		//#new_num_blocks


		SK_BoxStatReqs srq =

		{
			.mem =				NULL,

			.elem_size =			0,

			.hint_num_stat_elems =		0,

			.hint_elems_per_block = 	0
		};


		SK_BoxDynaReqs drq =

		{
			.hint_neg_slack =		neg_slack,

			.hint_pos_slack =		pos_slack,

			.hint_use_max_num_dyna_blocks =	0,

			.hint_max_num_dyna_blocks =	0
		};


		SK_BoxStatImpl sim;

		SK_BoxDynaImpl dim;

		SK_BoxReqs req;


		SK_BoxReqs_prep (&req, &srq, &sim, &drq, &dim);


		//Calculate what number of blocks should be allocated given a
		//current number of allocated blocks and the minimum number of
		//blocks needed

		size_t new_num_blocks = 0;

		SK_Box_calc_new_num_blocks
		(
			&req,

			cur_num_blocks,
			min_num_blocks,

			&new_num_blocks
		);


		//Print the results of the #new_num_blocks calculation

		printf
		(
			"neg_slack =        %zu\n"
			"pos_slack =        %zu\n"
			"cur_num_blocks =   %zu\n"
			"min_num_blocks =   %zu\n"
			"new_num_blocks =   %zu\n\n",
			neg_slack,
			pos_slack,
			cur_num_blocks,
			min_num_blocks,
			new_num_blocks
		);


		//Update the test parameters

		int looped = 0;


		SK_inc_chain
		(
			&min_num_blocks,
			1,
			BEG_MIN_NUM_BLOCKS,
			END_MIN_NUM_BLOCKS,
			INC_MIN_NUM_BLOCKS,
			&looped
		);


		SK_inc_chain
		(
			&cur_num_blocks,
			looped,
			BEG_CUR_NUM_BLOCKS,
			END_CUR_NUM_BLOCKS,
			INC_CUR_NUM_BLOCKS,
			&looped
		);


		SK_inc_chain
		(
			&pos_slack,
			looped,
			BEG_POS_SLACK,
			END_POS_SLACK,
			INC_POS_SLACK,
			&looped
		);


		SK_inc_chain
		(
			&neg_slack,
			looped,
			BEG_NEG_SLACK,
			END_NEG_SLACK,
			INC_NEG_SLACK,
			&looped
		);


		//End the loop if the "outer-most" parameter has looped

		if (looped)
		{
			go = 0;
		}
	}


	fprintf (file, "\n--------\n\n");


	//Announce the test-bench ending

	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlyDr_get (SLY_UNKNOWN_ERROR),
		SlyDr_get (SLY_SUCCESS)
	);
}




/*!
 *  Performs a simple test-bench on SK_Box_le_cmp () and SK_Box_bare_le_cmp ().
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @return			Standard status code
 */

SlySr SK_Box_le_cmp_tb
(
	i64		p_depth,
	FILE *		file,
	SK_LineDeco	deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN_FP (p_depth, stdout, file, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (p_depth, file, deco);


	//Select constants that determine how many parameterizations to test.

	const size_t MAX_TARGET_BYTES = 150;


	//FIXME : Update this array as more #SK_BoxIntf's are developed

	const size_t NUM_INTF = 1;

	const SK_BoxIntf * intfs [1] =

	{
		&SK_Array_intf
	};


	const i64 BEG_INTF_0_SEL = 		0;
	const i64 END_INTF_0_SEL = 		NUM_INTF - 1;
	const i64 INC_INTF_0_SEL = 		1;

	const i64 BEG_INTF_1_SEL = 		0;
	const i64 END_INTF_1_SEL = 		NUM_INTF - 1;
	const i64 INC_INTF_1_SEL = 		1;

	const i64 BEG_BOX_0_ELEM_SIZE =		1;
	const i64 END_BOX_0_ELEM_SIZE = 	8;
	const i64 INC_BOX_0_ELEM_SIZE = 	1;

	const i64 BEG_BOX_1_ELEM_SIZE =		1;
	const i64 END_BOX_1_ELEM_SIZE = 	8;
	const i64 INC_BOX_1_ELEM_SIZE = 	1;


	i64 intf_0_sel =			BEG_INTF_0_SEL;

	i64 intf_1_sel =			BEG_INTF_1_SEL;

	i64 box_0_elem_size =			BEG_BOX_0_ELEM_SIZE;

	i64 box_1_elem_size =			BEG_BOX_1_ELEM_SIZE;



	//Create 2 memory regions which will be filled with arbitrary bytes,
	//and be compared using SK_byte_le_cmp ().
	//
	//Note, the significance of adding on #END_BOX_*_ELEM_SIZE to the size
	//of #bytes_* is that #MAX_TARGET_BYTES is not guaranteed to be
	//divisible by #box_*_elem_size. The extra bytes assist in using
	//#bytes_0 and #bytes_1 as a source for element values, even if an
	//element value starts at byte index (MAX_TARGET_BYTES - 1).

	size_t bytes_0_len = MAX_TARGET_BYTES + END_BOX_0_ELEM_SIZE;


	u8 * bytes_0 = NULL;

	SlySr sr_0 =

	SK_MemStd.take ((void **) &bytes_0, bytes_0_len, 1);


	size_t bytes_1_len = MAX_TARGET_BYTES + END_BOX_1_ELEM_SIZE;


	u8 * bytes_1 = NULL;

	SlySr sr_1 =

	SK_MemStd.take ((void **) &bytes_1, bytes_1_len, 1);


	if ((NULL == bytes_0) || (NULL == bytes_1))
	{
		SK_DEBUG_PRINT_FP
		(
			p_depth,
			file,
			"Could not allocate space for #bytes_0 and #bytes_1"
		);

		SK_MemStd.free (bytes_0);
		SK_MemStd.free (bytes_1);

		return SlySr_get (SLY_BAD_ALLOC);
	}


	//Enter a test loop

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//Announce the parameters of this test iteration

		if (p_depth)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"intf_0_sel =        %" PRIi64,
				intf_0_sel
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"intf_1_sel =        %" PRIi64,
				intf_1_sel
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"box_0_elem_size =   %" PRIi64,
				box_0_elem_size
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"box_1_elem_size =   %" PRIi64,
				box_1_elem_size
			);
		}


		//Select #intf_0 and #intf_1

		const SK_BoxIntf * intf_0 = intfs [intf_0_sel];

		const SK_BoxIntf * intf_1 = intfs [intf_1_sel];


		//Configure #box_0 and #box_1

		//TODO : Perhaps make some of the arbitrarily sourced values
		//here iterative test values instead? Or would does this just
		//pollute the test with noise?

		//TODO : Perhaps its time to define a function / constant that
		//produces the "default" #SK_BoxStatReqs and #SK_BoxDynaReqs?
		//It will at least shorten sections like this.

		SK_BoxReqs req_0;

		SK_BoxAllReqs arq_0;


		arq_0.stat_r =

		(SK_BoxStatReqs)
		{
			.mem =				&SK_MemStd,
			.elem_size =			box_0_elem_size,
			.hint_num_stat_elems =		1,
			.hint_elems_per_block =		16
			//.req_reg =			NULL
		};


		arq_0.dyna_r =

		(SK_BoxDynaReqs)
		{
			.hint_neg_slack =		17,
			.hint_pos_slack =		16,
			.hint_use_max_num_dyna_blocks =	0,
			.hint_max_num_dyna_blocks =	0,
			.hint_tree_avg_degree =		0
		};


		SK_BoxAllReqs_prep (&arq_0, &(arq_0.stat_r), &(arq_0.dyna_r));

		SK_BoxReqs_point_to_arq (&req_0, &arq_0);


		SK_BoxReqs req_1;

		SK_BoxAllReqs arq_1;


		arq_1.stat_r =

		(SK_BoxStatReqs)
		{
			.mem =				&SK_MemStd,
			.elem_size =			box_1_elem_size,
			.hint_num_stat_elems =		1,
			.hint_elems_per_block =		4
			//.req_reg =			NULL
		};


		arq_1.dyna_r =

		(SK_BoxDynaReqs)
		{
			.hint_neg_slack =		0,
			.hint_pos_slack =		0,
			.hint_use_max_num_dyna_blocks =	0,
			.hint_max_num_dyna_blocks =	0,
			.hint_tree_avg_degree =		0
		};


		SK_BoxAllReqs_prep (&arq_1, &(arq_1.stat_r), &(arq_1.dyna_r));

		SK_BoxReqs_point_to_arq (&req_1, &arq_1);


		//Allocate the static memory of #box_0 and #box_1

		size_t box_0_stat_mem = 0;

		intf_0->inst_stat_mem (&req_0, &box_0_stat_mem);


		size_t box_1_stat_mem = 0;

		intf_1->inst_stat_mem (&req_1, &box_1_stat_mem);


		SK_Box * box_0 = NULL;

		sr_0 = SK_MemStd.take ((void **) &box_0, 1, box_0_stat_mem);


		SK_Box * box_1 = NULL;

		sr_1 = SK_MemStd.take ((void **) &box_1, 1, box_1_stat_mem);


		if ((NULL == box_0) || (NULL == box_1))
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"Could not allocate space for #box_0 and #box_1"
			);


			SK_MemStd.free (box_0);

			SK_MemStd.free (box_1);


			error_detected = 1;

			break;
		}


		//Initialize both #box_0 and #box_1

		sr_0 = intf_0->init
		(
			&req_0,
			box_0,
			0,
			NULL,
			NULL
		);

		sr_1 = intf_1->init
		(
			&req_1,
			box_1,
			0,
			NULL,
			NULL
		);


		if ((SLY_SUCCESS != sr_0.res) || (SLY_SUCCESS != sr_1.res))
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"Could not initialize #box_0 and #box_1"
			);


			if (SLY_SUCCESS == sr_0.res)
			{
				intf_0->deinit (&req_0, box_0);
			}


			if (SLY_SUCCESS == sr_1.res)
			{
				intf_1->deinit (&req_1, box_1);
			}


			SK_MemStd.free (box_0);

			SK_MemStd.free (box_1);


			error_detected = 1;

			break;
		}


		//Use an inner test-bench to test SK_Box_le_cmp () on
		//#box_0 / #box_1

		SK_LineDeco sub_deco = deco;

		sub_deco.pre_str_cnt += 1;


		i64 sub_p_depth =

		SK_p_depth_next (p_depth);


		sr_0 =

		SK_Box_le_cmp_sub_tb
		(
			sub_p_depth,
			file,
			sub_deco,

			MAX_TARGET_BYTES,

			bytes_0,
			bytes_0_len,

			bytes_1,
			bytes_1_len,

			intf_0,
			&req_0,
			box_0,

			intf_1,
			&req_1,
			box_1
		);


		if (SLY_SUCCESS != sr_0.res)
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"Sub-test-bench failed."
			);

			error_detected = 1;

			break;
		}


		//Deallocate used resources in this loop iteration

		intf_0->deinit (&req_0, box_0);
		intf_1->deinit (&req_1, box_1);

		SK_MemStd.free (box_0);
		SK_MemStd.free (box_1);


		//Increment the test parameters

		int looped = 0;


		SK_inc_chain
		(
			&box_1_elem_size,
			1,
			BEG_BOX_1_ELEM_SIZE,
			END_BOX_1_ELEM_SIZE,
			INC_BOX_1_ELEM_SIZE,
			&looped
		);

		SK_inc_chain
		(
			&box_0_elem_size,
			looped,
			BEG_BOX_0_ELEM_SIZE,
			END_BOX_0_ELEM_SIZE,
			INC_BOX_0_ELEM_SIZE,
			&looped
		);

		SK_inc_chain
		(
			&intf_1_sel,
			looped,
			BEG_INTF_1_SEL,
			END_INTF_1_SEL,
			INC_INTF_1_SEL,
			&looped
		);

		SK_inc_chain
		(
			&intf_0_sel,
			looped,
			BEG_INTF_0_SEL,
			END_INTF_0_SEL,
			INC_INTF_0_SEL,
			&looped
		);


		if (looped)
		{
			break;
		}
	}


	//Deallocate used resources

	SK_MemStd.free (bytes_0);
	SK_MemStd.free (bytes_1);


	//Announce the test-bench ending

	SK_TB_END_PRINT_STD
	(
		p_depth,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Inner test-bench that is used inside of SK_Box_le_cmp_tb ().
 *
 *  This test-bench performs several tests to ensure SK_Box_le_cmp () and
 *  SK_Box_bare_le_cmp () work correctly on the provided #SK_Box's.
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param max_target_bytes	The maximum number of bytes that #box_0 and
 *				#box_1 should target in size
 *
 *  @param bytes_0		Pointer to an arbitrary array of bytes
 *
 *  @param bytes_0_len		The length of #bytes_0
 *
 *  @param bytes_1		Pointer to another arbitrary array of bytes
 *
 *  @param bytes_1_len		The length of #bytes_1
 *
 *  @param intf_0		The #SK_BoxIntf to use when accessing #box_0
 *
 *  @param req_0		The #SK_BoxReqs to use when accessing #box_0
 *
 *  @param box_0		An #SK_Box to use in test comparisons
 *
 *  @param intf_1		The #SK_BoxIntf to use when accessing #box_1
 *
 *  @param req_1		The #SK_BoxReqs to use when accessing #box_1
 *
 *  @param box_1		Another #SK_Box to use in test comparisons
 *
 *  @return			Standard status code
 */

//TODO : This function appears to be executing very slowly; even slower than I
//would expect to be.

SlySr SK_Box_le_cmp_sub_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	size_t			max_target_bytes,

	void *			bytes_0,
	size_t			bytes_0_len,

	void *			bytes_1,
	size_t			bytes_1_len,

	const SK_BoxIntf *	intf_0,
	const SK_BoxReqs *	req_0,
	SK_Box *		box_0,

	const SK_BoxIntf *	intf_1,
	const SK_BoxReqs *	req_1,
	SK_Box *		box_1
)
{
	//Sanity check on arguments

	SK_NULL_RETURN_FP (p_depth, stdout, file, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN_FP (p_depth, file, bytes_0, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (p_depth, file, bytes_1, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (p_depth, file, intf_0, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (p_depth, file, req_0, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (p_depth, file, box_0, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (p_depth, file, intf_1, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (p_depth, file, req_1, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (p_depth, file, box_1, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (p_depth, file, deco);


	//Select constants that determine how many parameterizations to test.

	const i64 BEG_BOX_0_TARGET_BYTES =	0;
	const i64 END_BOX_0_TARGET_BYTES =	max_target_bytes;
	const i64 INC_BOX_0_TARGET_BYTES =	1;

	const i64 BEG_BOX_1_TARGET_BYTES =	0;
	const i64 END_BOX_1_TARGET_BYTES =	max_target_bytes;
	const i64 INC_BOX_1_TARGET_BYTES =	1;

	const i64 BEG_TEST_VAL_NUM =		0;
	const i64 END_TEST_VAL_NUM =		100;
	const i64 INC_TEST_VAL_NUM =		1;


	i64 box_0_target_bytes =		BEG_BOX_0_TARGET_BYTES;

	i64 box_1_target_bytes =		BEG_BOX_1_TARGET_BYTES;

	i64 test_val_num =			BEG_TEST_VAL_NUM;


	//Enter a test loop

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//Announce the parameters of this test

		if (p_depth)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"box_0_target_bytes = %zu",
				box_0_target_bytes
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"box_1_target_bytes = %zu",
				box_1_target_bytes
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"test_val_num =       %zu",
				test_val_num
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"\n",
				test_val_num
			);
		}


		//Determine the minimum number of elements in #box_0 and #box_1
		//needed to store their target number of bytes.

		size_t box_0_num_elems =

		SK_ceil_div_u64 (box_0_target_bytes, req_0->stat_r->elem_size);


		size_t box_1_num_elems =

		SK_ceil_div_u64 (box_1_target_bytes, req_1->stat_r->elem_size);


		size_t box_0_num_bytes =

		box_0_num_elems * req_0->stat_r->elem_size;


		size_t box_1_num_bytes =

		box_1_num_elems * req_1->stat_r->elem_size;


		if
		(
			(box_0_num_bytes > bytes_0_len)

			||

			(box_1_num_bytes > bytes_1_len))
		{

			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"#bytes_0 /#bytes_1 not large enough to hold "
				"equivalent data to #box_0 / #box_1. "
				"((box_0_num_bytes, box_1_num_bytes), "
				"(bytes_0_len, bytes_1_len)) = "
				"((%zu, %zu), (%zu, %zu))",
				box_0_num_bytes,
				box_1_num_bytes,
				bytes_0_len,
				bytes_1_len
			);

			error_detected = 1;

			break;
		}


		//Resize #box_0 and #box_1

		SlySr sr_0 =

		intf_0->resize
		(
			req_0,
			box_0,
			box_0_num_elems,
			NULL,
			NULL
		);


		SlySr sr_1 =

		intf_1->resize
		(
			req_1,
			box_1,
			box_1_num_elems,
			NULL,
			NULL
		);


		if ((SLY_SUCCESS != sr_0.res) || (SLY_SUCCESS != sr_1.res))
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"Could not resize #box_0 and #box_1 to the "
				"size needed for this test."
			);

			error_detected = 1;

			break;
		}


		//Fill #bytes_0 and #bytes_1 with randomly selected bytes.
		//
		//In addition, ensure that cases where #bytes_0 and #bytes_1
		//represent the same byte sequence will get tested.

		for (size_t b_sel = 0; b_sel < bytes_0_len; b_sel += 1)
		{
			if (b_sel < box_0_target_bytes)
			{
				((u8 *) bytes_0) [b_sel] =

				(u8) SK_gold_hash_u64 (box_0_num_bytes + test_val_num);
			}

			else
			{
				((u8 *) bytes_0) [b_sel] = 0;
			}
		}


		for (size_t b_sel = 0; b_sel < bytes_1_len; b_sel += 1)
		{
			if (b_sel < box_1_target_bytes)
			{
				if (0 == test_val_num % 10)
				{
					((u8 *) bytes_1) [b_sel] =

					((u8 *) bytes_0) [b_sel];
				}

				else
				{
					((u8 *) bytes_1) [b_sel] =

					(u8) SK_gold_hash_u64 (box_1_num_bytes + test_val_num);
				}
			}

			else
			{
				((u8 *) bytes_1) [b_sel] = 0;
			}
		}


		//Copy the contents of #bytes_0 / #bytes_1 into #box_0 / #box_1

		if (0 < box_0_num_elems)
		{
			intf_0->set_array
			(
				req_0,
				box_0,
				0,
				box_0_num_elems - 1,
				box_0_num_elems,
				bytes_0
			);
		}

		if (0 < box_1_num_elems)
		{
			intf_1->set_array
			(
				req_1,
				box_1,
				0,
				box_1_num_elems - 1,
				box_1_num_elems,
				bytes_1
			);
		}


		//Determine what the ideal result of comparing #box_0 and
		//#box_1 SHOULD be

		int ideal_res = 0;


		size_t cmp_len =

		SK_MAX (box_0_target_bytes, box_1_target_bytes);


		if (0 < cmp_len)
		{
			SK_byte_le_cmp
			(
				bytes_0,
				bytes_1,
				cmp_len,
				&ideal_res
			);
		}


		//Determine the result that SK_Box_le_cmp () and
		//SK_Box_bare_le_cmp () reports

		int res_0 = 0;


		SlyDr dr =

		SK_Box_le_cmp
		(
			intf_0,
			req_0,
			box_0,

			intf_1,
			req_1,
			box_1,

			&res_0
		);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"SK_Box_le_cmp returned #%s",
				SlyDrStr_get (dr).str
			);

			error_detected = 1;

			break;
		}


		int res_1 = 0;


		dr =

		SK_Box_bare_le_cmp
		(
			intf_0,
			req_0,
			box_0,

			bytes_1,
			bytes_1_len,

			&res_1
		);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"SK_Box_bare_le_cmp returned #%s",
				SlyDrStr_get (dr).str
			);

			error_detected = 1;

			break;
		}


		//Determine if SK_Box_le_cmp () and SK_Box_bare_le_cmp ()
		//functioned correctly in this test

		if ((res_0 != ideal_res) || (res_1 != ideal_res))
		{
			if (res_0 != ideal_res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"#res_0 = %d while #ideal_res = %d",
					res_0,
					ideal_res
				);
			}

			if (res_1 != ideal_res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"#res_1 = %d while #ideal_res = %d",
					res_1,
					ideal_res
				);
			}

			fprintf (file, "bytes_0 = 0x");

			SK_byte_print (file, bytes_0, bytes_0_len);

			fprintf (file, "\n\n");


			fprintf (file, "bytes_1 = 0x");

			SK_byte_print (file, bytes_1, bytes_1_len);

			fprintf (file, "\n\n");


			error_detected = 1;

			break;
		}


		//Increment the test parameters

		int looped = 0;


		SK_inc_chain
		(
			&test_val_num,
			1,
			BEG_TEST_VAL_NUM,
			END_TEST_VAL_NUM,
			INC_TEST_VAL_NUM,
			&looped
		);

		SK_inc_chain
		(
			&box_1_target_bytes,
			looped,
			BEG_BOX_1_TARGET_BYTES,
			END_BOX_1_TARGET_BYTES,
			INC_BOX_1_TARGET_BYTES,
			&looped
		);

		SK_inc_chain
		(
			&box_0_target_bytes,
			looped,
			BEG_BOX_0_TARGET_BYTES,
			END_BOX_0_TARGET_BYTES,
			INC_BOX_0_TARGET_BYTES,
			&looped
		);

		if (looped)
		{
			break;
		}
	}


	SK_TB_END_PRINT_STD
	(
		p_depth,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Given an #SK_BoxIntf (sbi) and an #SK_BoxReqs (req), performs a test-bench
 *  on initializing / de-initializing storage objects of different sizes that
 *  fit that description.
 *
 *  This is intended to be used inside of SK_BoxIntf_init_deinit_tb ().
 *
 *
 *  Sub-tests used
 *
 *  	None
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *	deinit ()
 *	get_elem ()
 *	get_num_elems ()
 *	get_resize_limits ()
 *	init ()
 *	init_unsafe ()
 *	inst_dyna_mem ()
 *	inst_stat_mem ()
 *
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param sbi			The #SK_BoxIntf to test
 *
 *  @param req			The #SK_BoxReqs that describes desired static
 *				parameters associated with the storage objects
 *				to create
 *
 *  @param range_num_elems	The range of #num_elems to test object
 *				initialization with.
 *
 *				The beginning of this range will be the minimum
 *				reported by sbi->get_resize_limits (), and the
 *				end of the range will either be (min +
 *				range_num_elems) or the maximum reported by
 *				sbi->get_resize_limits (), whichever is lower.
 *
 *				Note, the amount of time this test takes grows
 *				exponentially with this argument, because not
 *				only does it cause more initializations to be
 *				performed, those initializations will require
 *				reading #num_elems worth of elements to verify.
 *
 *  @return			Standard status code
 */

SlySr SK_BoxIntf_init_deinit_single_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi,

	const SK_BoxReqs *	req,

	size_t			range_num_elems
)
{
	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (SK_BOX_INTF_L_DEBUG, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (SK_BOX_INTF_L_DEBUG, file, sbi, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (SK_BOX_INTF_L_DEBUG, file, req, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Determines the range of how many elements the storage object
	//initialization should be tested with.
	//
	//Note that #sbi->get_resize_limits () needs to be respected here.

	int limits_exist =		0;

	size_t beg_num_elems =		0;

	size_t end_num_elems =		0;


	sbi->get_resize_limits
	(
		req,
		&limits_exist,
		&beg_num_elems,
		&end_num_elems
	);


	if (limits_exist)
	{
		end_num_elems =

		SK_MIN (end_num_elems, (beg_num_elems + range_num_elems));
	}

	else
	{
		end_num_elems = range_num_elems;
	}


	const i64 BEG_NUM_ELEMS = beg_num_elems;

	const i64 END_NUM_ELEMS = end_num_elems;

	const i64 INC_NUM_ELEMS = 1;


	i64 num_elems = BEG_NUM_ELEMS;


	//Determine other values that determine how many storage object
	//initializations to perform

	const i64 NUM_ELEM_INIT_FUNC = 3;

	const SK_BoxIntf_elem_rw_func INIT_FUNCS [3] =

	{
		(SK_BoxIntf_elem_rw_func) NULL,
		SK_Box_elem_rw_set_zero,
		SK_Box_elem_rw_set_w_arb_arg
	};


	const i64 BEG_INIT_FUNC_SEL =	0;
	const i64 END_INIT_FUNC_SEL =	NUM_ELEM_INIT_FUNC - 1;
	const i64 INC_INIT_FUNC_SEL =	1;

	const i64 BEG_USE_INIT_UNSAFE =	0;
	const i64 END_USE_INIT_UNSAFE =	1;
	const i64 INC_USE_INIT_UNSAFE =	1;


	i64 init_func_sel =	BEG_INIT_FUNC_SEL;

	i64 use_init_unsafe =	BEG_USE_INIT_UNSAFE;


	//Determine the static memory cost of the storage object instance, and
	//then allocate that memory

	size_t inst_stat_mem = 0;

	sbi->inst_stat_mem (req, &inst_stat_mem);


	void * inst = malloc (inst_stat_mem);

	SK_NULL_RETURN_FP (SK_BOX_INTF_L_DEBUG, file, inst, SlySr_get (SLY_BAD_ALLOC));


	//Create a random element to use as #arb_arg. This should be passed to
	//the selected element initialization function during the storage
	//object's initialization.

	u8 * arb_elem = malloc (req->stat_r->elem_size);

	if (NULL == arb_elem)
	{
		//Free any memory allocated up to this point

		free (inst);

		SK_NULL_RETURN_FP
		(
			SK_BOX_INTF_L_DEBUG,
			file,
			arb_elem,
			SlySr_get (SLY_BAD_ALLOC)
		);
	}


	for (size_t b_sel = 0; b_sel < req->stat_r->elem_size; b_sel += 1)
	{
		((u8 *) arb_elem) [b_sel] = SK_gold_hash_u64 (b_sel + 1);
	}


	//Create a memory space where elements fetched from the storage object
	//can be placed

	u8 * got_elem = malloc (req->stat_r->elem_size);

	if (NULL == got_elem)
	{
		//Free any memory allocated up to this point

		free (inst);

		free (arb_elem);

		SK_NULL_RETURN_FP
		(
			SK_BOX_INTF_L_DEBUG,
			file,
			got_elem,
			SlySr_get (SLY_BAD_ALLOC)
		);
	}


	//Create a memory space where the "ideal" value of elements can be
	//placed

	u8 * ideal_elem = malloc (req->stat_r->elem_size);

	if (NULL == ideal_elem)
	{
		//Free any memory allocated up to this point

		free (inst);

		free (arb_elem);

		free (got_elem);

		SK_NULL_RETURN_FP
		(
			SK_BOX_INTF_L_DEBUG,
			file,
			ideal_elem,
			SlySr_get (SLY_BAD_ALLOC)
		);
	}


	//Enter a test-loop where storage objects of different sizes will be
	//tested

	int go =		1;

	int error_detected =	0;


	while (go)
	{

		//Print out the parameters of this test iteration

		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"inst_stat_mem =   %zu",
				inst_stat_mem
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"num_elems =       %zu",
				num_elems
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"init_func_sel =   %zu",
				init_func_sel
			);


			SK_LineDeco_print_opt
			(
				file,
				deco,
				"use_init_unsafe = %zu",
				use_init_unsafe
			);


			SK_LineDeco_print_opt (file, deco, "");
		}


		//Select an element initialization function and an #arb_arg to
		//use during the storage object initialization

		SK_BoxIntf_elem_rw_func sel_init_func =

		INIT_FUNCS [init_func_sel];


		void * sel_arb_arg = NULL;


		if (2 == init_func_sel)
		{
			sel_arb_arg = arb_elem;
		}


		//Initialize the storage object

		SlySr sr = SlySr_get (SLY_UNKNOWN_ERROR);


		if (use_init_unsafe)
		{
			sr =

			sbi->init_unsafe
			(
				req,
				inst,
				num_elems
			);
		}

		else
		{
			sr =

			sbi->init
			(
				req,
				inst,
				num_elems,
				sel_init_func,
				sel_arb_arg
			);
		}


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				SK_BOX_INTF_L_DEBUG,
				file,
				"Could not initialize storage object with "
				"current parameters."
			);

			error_detected = 1;

			break;
		}


		//Check if the storage object has a correct size

		size_t cur_num_elems = 0;

		sbi->get_num_elems (req, inst, &cur_num_elems);


		if (cur_num_elems != num_elems)
		{
			SK_DEBUG_PRINT_FP
			(
				SK_BOX_INTF_L_DEBUG,
				file,
				"#inst holds incorrect number of elements. "
				"num_elems = %zu, cur_num_elems %zu",
				num_elems,
				cur_num_elems
			);

			error_detected = 1;

			break;
		}


		//Check if the total of (stat_mem + dyna_mem) is greater than
		//or equal to the total size of all the elements. It is
		//impossible for the total size of the storage object to be
		//less than (num_elems * req->stat_r->elem_size), though the
		//storage object can optionally use MORE memory for slack and
		//other parameters.

		size_t inst_dyna_mem = 0;

		sbi->inst_dyna_mem (req, inst, &inst_dyna_mem);


		size_t total_mem =	(inst_stat_mem + inst_dyna_mem);

		size_t required_mem =	(num_elems * req->stat_r->elem_size);


		if (total_mem < required_mem)
		{
			SK_DEBUG_PRINT_FP
			(
				SK_BOX_INTF_L_DEBUG,
				file,
				"#inst uses an impossibly small amount of "
				"memory."
				"num_elems =           %zu"
				"req->stat_r->elem_size = %zu"
				"inst_stat_mem =       %zu"
				"inst_dyna_mem =       %zu"
				"total_mem =           %zu"
				"required_mem =        %zu",
				num_elems,
				req->stat_r->elem_size,
				inst_stat_mem,
				inst_dyna_mem,
				total_mem,
				required_mem
			);

			error_detected = 1;

			break;
		}


		//Test that the elements in the storage object were initialized
		//correctly, provided that sbi->init () was used and not
		//sbi->init_unsafe ()

		if (0 == use_init_unsafe)
		{
			for (size_t elem_sel = 0; elem_sel < num_elems; elem_sel += 1)
			{
				sbi->get_elem
				(
					req,
					inst,
					elem_sel,
					got_elem
				);


				//Calculate the ideal element value for this
				//index with the selected element
				//initialization funtion

				if (0 == init_func_sel)
				{
					//NULL element initializtion function

					SK_byte_set_all
					(
						ideal_elem,
						0,
						req->stat_r->elem_size
					);

				}

				else if (1 == init_func_sel)
				{
					SK_Box_elem_rw_set_zero
					(
						req,
						ideal_elem,
						elem_sel,
						sel_arb_arg
					);
				}

				else if (2 == init_func_sel)
				{
					SK_Box_elem_rw_set_w_arb_arg
					(
						req,
						ideal_elem,
						elem_sel,
						sel_arb_arg
					);
				}


				//Determine if the received element actually
				//matches the ideal value

				int eq = 0;

				SK_byte_eq
				(
					got_elem,
					req->stat_r->elem_size,

					ideal_elem,
					req->stat_r->elem_size,

					&eq,
					NULL
				);


				if (0 == eq)
				{
					SK_DEBUG_PRINT_FP
					(
						SK_BOX_INTF_L_DEBUG,
						file,
						"Bad element value at index "
						"%zu",
						elem_sel
					);


					//FIXME : Shouldn't these prints use
					//#file and #deco?


					fprintf (file, "ideal_elem = 0x");

					SK_byte_print (file, ideal_elem, req->stat_r->elem_size);

					fprintf (file, "\n");


					fprintf (file, "got_elem =   0x");

					SK_byte_print (file, got_elem, req->stat_r->elem_size);

					fprintf (file, "\n");


					error_detected = 1;

					break;
				}
			}

			//Propagate error-breaking

			if (error_detected)
			{
				break;
			}
		}


		//Deinitialize the storage object

		SlyDr dr = sbi->deinit (req, inst);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				SK_BOX_INTF_L_DEBUG,
				file,
				"sbi->deinit () returned failure, which should "
				"be impossible."
			);

			error_detected = 1;

			break;
		}


		//Increment the test parameters

		int looped = 0;

		SK_inc_chain
		(
			&num_elems,
			1,
			BEG_NUM_ELEMS,
			END_NUM_ELEMS,
			INC_NUM_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&init_func_sel,
			looped,
			BEG_INIT_FUNC_SEL,
			END_INIT_FUNC_SEL,
			INC_INIT_FUNC_SEL,
			&looped
		);

		SK_inc_chain
		(
			&use_init_unsafe,
			looped,
			BEG_USE_INIT_UNSAFE,
			END_USE_INIT_UNSAFE,
			INC_USE_INIT_UNSAFE,
			&looped
		);

		if (looped)
		{
			break;
		}
	}


	//Free the memory used for the ideal element value

	free (ideal_elem);


	//Free the memory used to store fetched elements

	free (got_elem);


	//Free the memory used to store #arb_elem

	free (arb_elem);


	//Free the memory used to store the storage object instance static
	//memory

	free (inst);


	//Announce the test-bench ending

	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Given an implementation of #SK_BoxIntf, this performs a test-bench that
 *  checks if storage objects can created / destroyed correctly.
 *
 *
 *  Sub-tests used
 *
 *	SK_BoxIntf_init_deinit_single_tb ()
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *	None
 *
 *
 *  @param p_depth		The "printing depth".
 *
 *  				If (p_depth == 0), then this test-bench will
 *  				NOT print out results whatsoever to #file.
 *
 *  				If (p_depth > 0), then this test-bench WILL
 *				print out results to #file.
 *
 *				#p_depth will be decremented for inner
 *				test-benches until it reaches 0.
 *
 *				#SK_P_DEPTH_INF (and every value less than it)
 *				indicates infinite printing depth.
 *
 *  @param file			Pointer to the #FILE to print messages to
 *
 *  @parma deco			An #SK_LineDeco describing how to decorate
 *				printed lines
 *
 *  @param sbi			The #SK_BoxIntf to test
 *
 *  @return			Standard status code
 */

SlySr SK_BoxIntf_init_deinit_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
)
{
	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);

	i64 next_depth = SK_p_depth_next (p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (SK_BOX_INTF_L_DEBUG, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (SK_BOX_INTF_L_DEBUG, file, sbi, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Select constants that determine how many parameterizations to test.
	//
	//These parameters will be used to create #SK_BoxReq's, and the select
	//parameters here are the ones thought to be the most relevant to
	//initializing / deinitializing storage objects

	const size_t RNG_NUM_ELEMS =		40;

	const i64 BEG_ELEM_SIZE = 		1;
	const i64 END_ELEM_SIZE = 		10;
	const i64 INC_ELEM_SIZE = 		1;

	const i64 BEG_NUM_STAT_ELEMS =		0;
	const i64 END_NUM_STAT_ELEMS =		4;
	const i64 INC_NUM_STAT_ELEMS =		1;

	const i64 BEG_ELEMS_PER_BLOCK =		0;
	const i64 END_ELEMS_PER_BLOCK =		4;
	const i64 INC_ELEMS_PER_BLOCK =		1;

	const i64 BEG_NEG_SLACK =		0;
	const i64 END_NEG_SLACK =		2;
	const i64 INC_NEG_SLACK =		1;

	const i64 BEG_POS_SLACK =		0;
	const i64 END_POS_SLACK =		2;
	const i64 INC_POS_SLACK =		1;


	i64 elem_size =				BEG_ELEM_SIZE;

	i64 num_stat_elems =			BEG_NUM_STAT_ELEMS;

	i64 elems_per_block =			BEG_ELEMS_PER_BLOCK;

	i64 neg_slack =				BEG_NEG_SLACK;

	i64 pos_slack =				BEG_POS_SLACK;


	//Enter the test-loop, in which storage objects with different
	//parameters will be initialized and deinitialized

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//Create an #SK_BoxReqs that describes the storage object that
		//is desired to be tested

		SK_BoxStatReqs srq =

		{
			.mem =				&SK_MemStd,

			.elem_size =			elem_size,

			.hint_num_stat_elems =		num_stat_elems,

			.hint_elems_per_block =		elems_per_block
		};


		SK_BoxDynaReqs drq =

		{
			.hint_neg_slack =			neg_slack,

			.hint_pos_slack =			pos_slack,

			.hint_use_max_num_dyna_blocks =		0,

			.hint_max_num_dyna_blocks =		0
		};


		SK_BoxStatImpl sim;

		SK_BoxDynaImpl dim;

		SK_BoxReqs req;


		SK_BoxReqs_prep (&req, &srq, &sim, &drq, &dim);


		if (print_flag)
		{
			SK_LineDeco_print_opt (file, deco, "The current #SK_BoxReqs is...\n\n");

			SK_BoxReqs_print (&req, 2, file, deco);

			SK_LineDeco_print_opt (file, deco, "");
		}


		//Execute a test-bench for storage-object intiialization /
		//deinitialization using the current #SK_BoxReqs

		SK_LineDeco ind_deco = deco;

		ind_deco.pre_str_cnt += 1;


		SlySr sr =

		SK_BoxIntf_init_deinit_single_tb
		(
			next_depth,
			file,
			ind_deco,
			sbi,
			&req,
			RNG_NUM_ELEMS
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				SK_BOX_INTF_L_DEBUG,
				file,
				"Inner test-bench failed!"
			);

			error_detected = 1;

			break;
		}


		//Increment the test parameters

		int looped = 0;


		SK_inc_chain
		(
			&elem_size,
			1,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);

		SK_inc_chain
		(
			&num_stat_elems,
			looped,
			BEG_NUM_STAT_ELEMS,
			END_NUM_STAT_ELEMS,
			INC_NUM_STAT_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&elems_per_block,
			looped,
			BEG_ELEMS_PER_BLOCK,
			END_ELEMS_PER_BLOCK,
			INC_ELEMS_PER_BLOCK,
			&looped
		);

		SK_inc_chain
		(
			&neg_slack,
			looped,
			BEG_NEG_SLACK,
			END_NEG_SLACK,
			INC_NEG_SLACK,
			&looped
		);

		SK_inc_chain
		(
			&pos_slack,
			looped,
			BEG_POS_SLACK,
			END_POS_SLACK,
			INC_POS_SLACK,
			&looped
		);

		if (looped)
		{
			break;
		}
	}


	//Announce the test-bench ending

	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Given an #SK_BoxIntf storage object instance, this performs a test on the
 *  set / get functions. The versions of functions without anchors, the
 *  *_w_anc () versions, and the *_by_anc () versions are all tested.
 *
 *
 *  Sub-tests used
 *
 *  	None
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *  	get_num_elems ()
 *  	anc_recommend ()
 *  	anc_support ()
 *  	anc_size ()
 *  	anc_validate ()
 *  	get_anc ()
 *  	get_anc_w_anc ()
 *  	get_elem_ptr ()
 *  	get_elem_ptr_w_anc ()
 *  	get_elem_ptr_by_anc ()
 *  	get_elem ()
 *  	get_elem_w_anc ()
 *  	get_elem_by_anc ()
 *  	set_elem ()
 *  	set_elem_w_anc ()
 *  	set_elem_by_anc ()
 *
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param sbi			The #SK_BoxIntf to use
 *
 *  @param req			The #SK_BoxReqs associated with #inst
 *
 *  @param inst			Pointer to the storage object instance to
 *				perform the test-bench on
 *
 *  @param beg_ind		The starting index to retrieve elements from.
 *
 *  				This MUST be lower than the number of elements
 *  				in #inst.
 *
 *  @param end_ind		The ending index to retrieve elements from.
 *
 *  				This MUST be lower than the number of elements
 *  				in #inst.
 *
 *  @return			Standard status code
 */

//FIXME : This function could use some refactoring; it has a high amount of
//redundancy internally

//FIXME : Need to consistently check against #SK_BoxIntf function return values
//in this test-bench

SlySr SK_BoxIntf_set_get_single_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi,
	SK_BoxReqs *		req,
	void *			inst,

	size_t			beg_ind,
	size_t			end_ind
)
{
	//Decide on announcing the test-bench

	int print_flag = (0 != p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, req, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, inst, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Check if the values of #beg_ind and #end_ind are reasonable

	if (beg_ind > end_ind)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#beg_ind = %zu and #end_ind = %zu need to be "
			"swapped, fixing this...",
			beg_ind,
			end_ind
		);

		SK_byte_swap (&beg_ind, &end_ind, sizeof (size_t));
	}


	size_t num_elems = 0;

	sbi->get_num_elems (req, inst, &num_elems);


	if (end_ind >= num_elems)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#end_ind = %zu was too large, #num_elems = %zu",
			end_ind,
			num_elems
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	//Attempt to allocate space for elements to use as the destination /
	//source for sbi->get_elem () / sbi->set_elem ()

	//FIXME : Consider making #elem_0 and #elem_1 arguments to this
	//function so that this allocation does not need to be done everytime
	//this test-bench is executed, perhaps. Maybe it just clutters the
	//function's interface too much.

	void * elem_0 = malloc (req->stat_r->elem_size);
	void * elem_1 = malloc (req->stat_r->elem_size);

	if ((NULL == elem_0) || (NULL == elem_1))
	{
		free (elem_0);
		free (elem_1);

		SK_NULL_RETURN_FP
		(
			print_flag,
			file,
			elem_0,
			SlySr_get (SLY_BAD_ALLOC)
		);

		SK_NULL_RETURN_FP
		(
			print_flag,
			file,
			elem_1,
			SlySr_get (SLY_BAD_ALLOC)
		);
	}


	//Allocate space for the anchors to use with the *_w_anc () versions of
	//functions in this test bench.
	//
	//Note, if anchors are not supported by the current #SK_BoxIntf, then
	//it is acceptable to leave #anc_0 and #anc_1 as NULL.

	void * anc_0 = NULL;
	void * anc_1 = NULL;


	int anc_support = 0;

	sbi->anc_support (req, &anc_support);


	int anc_recommend = 0;

	sbi->anc_recommend (req, inst, &anc_recommend);


	size_t anc_size = 0;

	sbi->anc_size (req, &anc_size);


	if (anc_support)
	{
		//Make sure that anchors don't have a size of 0 if they are
		//supported

		if (0 >= anc_size)
		{
			//Free all of the memory allocated up to this point

			free (elem_0);
			free (elem_1);

			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#anc_size was 0 even though #anc_support "
				"was 1."
			);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		//Since the anchors are supported, actually allocate memory for
		//them

		anc_0 = malloc (anc_size);
		anc_1 = malloc (anc_size);

		if ((NULL == anc_0) || (NULL == anc_1))
		{
			//Free all of the memory allocated up to this point

			free (elem_0);
			free (elem_1);

			free (anc_0);
			free (anc_1);

			SK_NULL_RETURN_FP
			(
				print_flag,
				file,
				anc_0,
				SlySr_get (SLY_BAD_ALLOC)
			);

			SK_NULL_RETURN_FP
			(
				print_flag,
				file,
				anc_1,
				SlySr_get (SLY_BAD_ALLOC)
			);
		}
	}

	else
	{
		//Make sure that anchors DO have a size of 0 if they are NOT
		//supported

		if (0 != anc_size)
		{
			//Free all of the memory allocated up to this point

			free (elem_0);
			free (elem_1);

			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#anc_size = %d, but it should have been 0 "
				"because #anc_support was 0.",
				anc_size
			);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		//Make sure that anchors are not recommended if anchors are not
		//supported

		if (anc_recommend)
		{
			//Free all of the memory allocated up to this point

			free (elem_0);
			free (elem_1);

			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#anc_recommend = %d, even though anchors "
				"were not supported.",
				anc_recommend
			);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}
	}


	//Determine values that select how many tests to perform

	const i64 NORMAL_ELEM_SEL_BEG =		beg_ind;
	const i64 NORMAL_ELEM_SEL_END = 	end_ind;
	const i64 NORMAL_ELEM_SEL_INC = 	1;

	const i64 RAND_SEL_BEG = 		0;
	const i64 RAND_SEL_END = 		1;
	const i64 RAND_SEL_INC = 		1;


	i64 normal_elem_sel =	NORMAL_ELEM_SEL_BEG;

	i64 rand_sel =		RAND_SEL_BEG;


	//Enter a test-loop where values will be set/get repeatedly

	int go =		1;

	int error_detected =	0;


	while (go)
	{

		//Select an element to attempt to access.
		//
		//Either accept the normal element selection, or make a random
		//selection within the range [beg_ind, end_ind].
		//
		//Note, the purpose of selecting an element randomly is that
		//this will help identify bugs that occur when _sequences_ of
		//indices cause the storage object to get into a bad state.

		size_t elem_sel = normal_elem_sel;

		if (rand_sel)
		{
			elem_sel =

			SK_range_oflow_i64
			(
				SK_gold_hash_u64 (normal_elem_sel),
				beg_ind,
				end_ind
			);
		}


		//Print out the parameters of this test iteration

		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"normal_elem_sel = %" PRIi64,
				normal_elem_sel
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"rand_sel =        %" PRIi64,
				rand_sel
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"elem_sel =        %zu",
				elem_sel
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"",
				elem_sel
			);
		}


		//Retrieve the element at #elem_sel from the storage object,
		//modify it, and then set it back in the storage object

		//Retrieve the element

		sbi->get_elem (req, inst, elem_sel, elem_0);


		//Modify the element

		for (size_t b_sel = 0; b_sel < req->stat_r->elem_size; b_sel += 1)
		{
			((u8 *) elem_1) [b_sel] =

			((u8 *) elem_0) [b_sel] + b_sel + 1;
		}


		//Set the element

		sbi->set_elem (req, inst, elem_sel, elem_1);


		//Check the element

		sbi->get_elem (req, inst, elem_sel, elem_0);


		int eq =		0;

		size_t ineq_ind =	0;


		SK_byte_eq
		(
			elem_0,
			req->stat_r->elem_size,

			elem_1,
			req->stat_r->elem_size,

			&eq,
			&ineq_ind
		);


		if (0 == eq)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Element was not set properly\n"
				"elem_sel =   %zu\n"
				"elem_0 [%zu] = %" PRIu8 "\n"
				"elem_1 [%zu] = %" PRIu8 "\n",
				elem_sel,
				ineq_ind,
				((u8 *) elem_0) [ineq_ind],
				ineq_ind,
				((u8 *) elem_1) [ineq_ind]
			);



			//FIXME : Shouldn't these prints use
			//#file and #deco?


			fprintf (file, "elem_0 = 0x");

			SK_byte_print (file, elem_0, req->stat_r->elem_size);

			fprintf (file, "\n");


			fprintf (file, "elem_1 = 0x");

			SK_byte_print (file, elem_1, req->stat_r->elem_size);

			fprintf (file, "\n");


			error_detected = 1;

			break;
		}


		//This tests whether retrieving an element pointer using
		//get_elem_ptr () works for the selected element

		//Get the pointer

		void * elem_ptr = NULL;

		sbi->get_elem_ptr (req, inst, elem_sel, &elem_ptr);


		if (NULL == elem_ptr)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Failed to set #elem_ptr correctly."
			);

			error_detected = 1;

			break;
		}


		//Clear #elem_0

		SK_byte_set_all (elem_0, 0, req->stat_r->elem_size);


		//Copy to #elem_0 from #elem_ptr

		SK_byte_copy (elem_0, elem_ptr, req->stat_r->elem_size);


		//Check that #elem_0 is still equal to #elem_1

		SK_byte_eq
		(
			elem_0,
			req->stat_r->elem_size,

			elem_1,
			req->stat_r->elem_size,

			&eq,
			&ineq_ind
		);


		if (0 == eq)
		{

			//FIXME : This part is a copy of a previous section of
			//this test-bench; find a way to remove this redundancy

			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Element was not set properly\n"
				"elem_sel =   %zu\n"
				"elem_0 [%zu] = %" PRIu8 "\n"
				"elem_1 [%zu] = %" PRIu8 "\n",
				elem_sel,
				ineq_ind,
				((u8 *) elem_0) [ineq_ind],
				ineq_ind,
				((u8 *) elem_1) [ineq_ind]
			);


			//FIXME : Shouldn't these prints use
			//#file and #deco?


			fprintf (file, "elem_0 = 0x");

			SK_byte_print (file, elem_0, req->stat_r->elem_size);

			fprintf (file, "\n");


			fprintf (file, "elem_1 = 0x");

			SK_byte_print (file, elem_1, req->stat_r->elem_size);

			fprintf (file, "\n");


			error_detected = 1;

			break;
		}


		//Produce an anchor associated with the element at #elem_sel
		//using sbi->get_anc ()

		SlyDr dr =

		sbi->get_anc
		(
			req,
			inst,
			elem_sel,
			anc_0
		);


		if (anc_support)
		{
			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Could not use sbi->get_anc () "
					"to get the anchor associated with "
					"elem_sel = %zu "
				);

				error_detected = 1;

				break;
			}

		}

		else
		{
			if (SLY_SUCCESS == dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"sbi->get_anc () returned #SLY_SUCCESS "
					"even though #anc_support = %d",
					anc_support

				);

				error_detected = 1;

				break;
			}
		}


		//Again attempt to get an anchor associated with the element at
		//#elem_sel, except using sbi->get_anc_w_anc () instead.
		//
		//Of course, this operation in particular is useless because
		//#anc_0 is already associated with #elem_sel, but it should be
		//possible nonetheless.

		dr =

		sbi->get_anc_w_anc
		(
			req,
			inst,
			elem_sel,
			anc_0,
			anc_1
		);


		if (anc_support)
		{
			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Could not use sbi->get_anc_w_anc () "
					"to get the anchor associated with "
					"elem_sel = %zu "
				);

				error_detected = 1;

				break;
			}

		}

		else
		{
			if (SLY_SUCCESS == dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"sbi->get_anc_w_anc () returned "
					"#SLY_SUCCESS even though "
					"#anc_support = %d",
					anc_support

				);

				error_detected = 1;

				break;
			}
		}


		//Repeat the section where an element was retrieved and set,
		//but this time use the *_w_anc () versions of functions.
		//
		//Note that these functions should be available whether or not
		//anchors are supported, even though the anchor arguments will
		//be useless in that case.

		//Retrieve the element

		sbi->get_elem_w_anc (req, inst, elem_sel, anc_0, anc_1, elem_0);


		//Modify the element

		for (size_t b_sel = 0; b_sel < req->stat_r->elem_size; b_sel += 1)
		{
			((u8 *) elem_1) [b_sel] =

			((u8 *) elem_0) [b_sel] + b_sel + 1;
		}


		//Set the element

		sbi->set_elem_w_anc (req, inst, elem_sel, anc_1, anc_1, elem_1);


		//Check the element

		sbi->get_elem_w_anc (req, inst, elem_sel, anc_0, anc_0, elem_0);


		eq =		0;

		ineq_ind =	0;


		SK_byte_eq
		(
			elem_0,
			req->stat_r->elem_size,

			elem_1,
			req->stat_r->elem_size,

			&eq,
			&ineq_ind
		);


		if (0 == eq)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Element was not set properly\n"
				"elem_sel =   %zu\n"
				"elem_0 [%zu] = %" PRIu8 "\n"
				"elem_1 [%zu] = %" PRIu8 "\n",
				elem_sel,
				ineq_ind,
				((u8 *) elem_0) [ineq_ind],
				ineq_ind,
				((u8 *) elem_1) [ineq_ind]
			);


			//FIXME : Shouldn't these prints use
			//#file and #deco?


			fprintf (file, "elem_0 = 0x");

			SK_byte_print (file, elem_0, req->stat_r->elem_size);

			fprintf (file, "\n");


			fprintf (file, "elem_1 = 0x");

			SK_byte_print (file, elem_1, req->stat_r->elem_size);

			fprintf (file, "\n");


			error_detected = 1;

			break;
		}


		//Repeat the section where an element pointer was retrieved,
		//but this time use get_elem_ptr_w_anc ()

		//Get the pointer

		elem_ptr = NULL;

		sbi->get_elem_ptr_w_anc (req, inst, elem_sel, anc_0, anc_1, &elem_ptr);


		if (NULL == elem_ptr)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Failed to set #elem_ptr correctly."
			);

			error_detected = 1;

			break;
		}


		//Clear #elem_0

		SK_byte_set_all (elem_0, 0, req->stat_r->elem_size);


		//Copy to #elem_0 from #elem_ptr

		SK_byte_copy (elem_0, elem_ptr, req->stat_r->elem_size);


		//Check that #elem_0 is still equal to #elem_1

		SK_byte_eq
		(
			elem_0,
			req->stat_r->elem_size,

			elem_1,
			req->stat_r->elem_size,

			&eq,
			&ineq_ind
		);


		if (0 == eq)
		{

			//FIXME : This part is a copy of a previous section of
			//this test-bench; find a way to remove this redundancy

			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Element was not set properly\n"
				"elem_sel =   %zu\n"
				"elem_0 [%zu] = %" PRIu8 "\n"
				"elem_1 [%zu] = %" PRIu8 "\n",
				elem_sel,
				ineq_ind,
				((u8 *) elem_0) [ineq_ind],
				ineq_ind,
				((u8 *) elem_1) [ineq_ind]
			);



			//FIXME : Shouldn't these prints use
			//#file and #deco?


			fprintf (file, "elem_0 = 0x");

			SK_byte_print (file, elem_0, req->stat_r->elem_size);

			fprintf (file, "\n");


			fprintf (file, "elem_1 = 0x");

			SK_byte_print (file, elem_1, req->stat_r->elem_size);

			fprintf (file, "\n");


			error_detected = 1;

			break;
		}


		//This next portion of the test-bench repeats the above tests,
		//but instead, the *_by_anc () versions of functions are used.
		//
		//Unlike the previous versions of the functions, these function
		//DO require anchors to be supported.

		if (anc_support)
		{
			//Retrieve an element from the storage object, modify
			//it, and then set it back in the storage object

			//Retrieve the element

			sbi->get_elem_by_anc (req, inst, anc_0, elem_0);


			//Modify the element

			for
			(
				size_t b_sel = 0;

				b_sel < req->stat_r->elem_size;

				b_sel += 1
			)
			{
				((u8 *) elem_1) [b_sel] =

				((u8 *) elem_0) [b_sel] + b_sel + 1;
			}


			//Set the element
			//
			//(Note that #anc_1 should be associated with the same
			//element as #anc_0)

			sbi->set_elem_by_anc (req, inst, anc_1, elem_1);


			//Check the element

			sbi->get_elem_by_anc (req, inst, anc_0, elem_0);


			eq =		0;

			ineq_ind =	0;


			SK_byte_eq
			(
				elem_0,
				req->stat_r->elem_size,

				elem_1,
				req->stat_r->elem_size,

				&eq,
				&ineq_ind
			);


			if (0 == eq)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Element was not set properly\n"
					"elem_sel =     %zu\n"
					"elem_0 [%zu] = %" PRIu8 "\n"
					"elem_1 [%zu] = %" PRIu8 "\n",
					elem_sel,
					ineq_ind,
					((u8 *) elem_0) [ineq_ind],
					ineq_ind,
					((u8 *) elem_1) [ineq_ind]
				);


				//FIXME : Shouldn't these prints use
				//#file and #deco?


				fprintf (file, "elem_0 = 0x");

				SK_byte_print
				(
					file,
					elem_0,
					req->stat_r->elem_size
				);

				fprintf (file, "\n");


				fprintf (file, "elem_1 = 0x");

				SK_byte_print
				(
					file,
					elem_1,
					req->stat_r->elem_size
				);

				fprintf (file, "\n");


				error_detected = 1;

				break;
			}


			//This tests whether retrieving an element pointer
			//using get_elem_ptr_by_anc () works for the selected
			//element

			//Get the pointer

			elem_ptr = NULL;

			sbi->get_elem_ptr_by_anc (req, inst, anc_0, &elem_ptr);


			if (NULL == elem_ptr)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Failed to set #elem_ptr correctly."
				);

				error_detected = 1;

				break;
			}


			//Clear #elem_0

			SK_byte_set_all (elem_0, 0, req->stat_r->elem_size);


			//Copy to #elem_0 from #elem_ptr

			SK_byte_copy (elem_0, elem_ptr, req->stat_r->elem_size);


			//Check that #elem_0 is still equal to #elem_1

			SK_byte_eq
			(
				elem_0,
				req->stat_r->elem_size,

				elem_1,
				req->stat_r->elem_size,

				&eq,
				&ineq_ind
			);


			if (0 == eq)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Element was not set properly\n"
					"elem_sel =     %zu\n"
					"elem_0 [%zu] = %" PRIu8 "\n"
					"elem_1 [%zu] = %" PRIu8 "\n",
					elem_sel,
					ineq_ind,
					((u8 *) elem_0) [ineq_ind],
					ineq_ind,
					((u8 *) elem_1) [ineq_ind]
				);


				//FIXME : Shouldn't these prints use
				//#file and #deco?


				fprintf (file, "elem_0 = 0x");

				SK_byte_print
				(
					file,
					elem_0,
					req->stat_r->elem_size
				);

				fprintf (file, "\n");


				fprintf (file, "elem_1 = 0x");

				SK_byte_print
				(
					file,
					elem_1,
					req->stat_r->elem_size
				);

				fprintf (file, "\n");


				error_detected = 1;

				break;
			}
		}


		//Increment the test parameters

		int looped = 0;

		SK_inc_chain
		(
			&normal_elem_sel,
			1,
			NORMAL_ELEM_SEL_BEG,
			NORMAL_ELEM_SEL_END,
			NORMAL_ELEM_SEL_INC,
			&looped
		);

		SK_inc_chain
		(
			&rand_sel,
			looped,
			RAND_SEL_BEG,
			RAND_SEL_END,
			RAND_SEL_INC,
			&looped
		);

		if (looped)
		{
			break;
		}
	}


	//If anchors were allocated, make sure to deallocate them

	if (anc_support)
	{
		free (anc_0);
		free (anc_1);
	}


	//Free the elements allocated earlier in this function

	free (elem_0);
	free (elem_1);


	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);

}




/*!
 *  Given an implementation of #SK_BoxIntf, this performs a test-bench that
 *  checks if elements can be set / get from storage objects correctly.
 *
 *
 *  Sub-tests used
 *
 *	SK_BoxIntf_set_get_single_tb ()
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *	None
 *
 *
 *  @param p_depth		The "printing depth".
 *
 *  				If (p_depth <= 0), then this test-bench will
 *  				NOT print out results whatsoever to #file.
 *
 *  				If (p_depth > 0), then this test-bench WILL
 *				print out results to #file.
 *
 *				#p_depth will be decremented for inner
 *				test-benches until it reaches 0.
 *
 *				(-1) indicates infinite printing depth.
 *
 *  @param file			Pointer to the #FILE to print messages to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate
 *				printed lines
 *
 *  @param sbi			The #SK_BoxIntf to test
 *
 *  @return			Standard status code
 */

SlySr SK_BoxIntf_set_get_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
)
{

	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);

	i64 next_depth = SK_p_depth_next (p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Select constants that determine how many parameterizations to test.

	const i64 BEG_ELEM_SIZE = 		1;
	const i64 END_ELEM_SIZE = 		10;
	const i64 INC_ELEM_SIZE = 		1;

	const i64 BEG_NUM_STAT_ELEMS =		0;
	const i64 END_NUM_STAT_ELEMS =		4;
	const i64 INC_NUM_STAT_ELEMS =		1;

	const i64 BEG_ELEMS_PER_BLOCK =		0;
	const i64 END_ELEMS_PER_BLOCK =		4;
	const i64 INC_ELEMS_PER_BLOCK =		1;

	const i64 BEG_NEG_SLACK =		0;
	const i64 END_NEG_SLACK =		2;
	const i64 INC_NEG_SLACK =		1;

	const i64 BEG_POS_SLACK =		0;
	const i64 END_POS_SLACK =		2;
	const i64 INC_POS_SLACK =		1;


	i64 elem_size =				BEG_ELEM_SIZE;

	i64 num_stat_elems =			BEG_NUM_STAT_ELEMS;

	i64 elems_per_block =			BEG_ELEMS_PER_BLOCK;

	i64 neg_slack =				BEG_NEG_SLACK;

	i64 pos_slack =				BEG_POS_SLACK;


	//Enter the test-loop, in which storage objects with different
	//parameters will be initialized and deinitialized

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//Create an #SK_BoxReqs that describes the storage object that
		//is desired to be tested

		SK_BoxStatReqs srq =

		{
			.mem =				&SK_MemStd,

			.elem_size =			elem_size,

			.hint_num_stat_elems =		num_stat_elems,

			.hint_elems_per_block =		elems_per_block
		};


		SK_BoxDynaReqs drq =

		{
			.hint_neg_slack =			neg_slack,

			.hint_pos_slack =			pos_slack,

			.hint_use_max_num_dyna_blocks =		0,

			.hint_max_num_dyna_blocks =		0
		};


		SK_BoxStatImpl sim;

		SK_BoxDynaImpl dim;

		SK_BoxReqs req;


		SK_BoxReqs_prep (&req, &srq, &sim, &drq, &dim);


		if (print_flag)
		{
			SK_LineDeco_print_opt (file, deco, "The current #SK_BoxReqs is...\n\n");

			SK_BoxReqs_print (&req, 2, file, deco);

			SK_LineDeco_print_opt (file, deco, "");
		}


		//Determine if there are limits to how many elements the
		//storage object described by #req can store

		int lim_exist = 0;

		size_t lim_min = 0;
		size_t lim_max = 0;

		sbi->get_resize_limits (&req, &lim_exist, &lim_min, &lim_max);


		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"lim_exist = %zu",
				lim_exist
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"lim_min =   %zu",
				lim_min
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"lim_max =   %zu",
				lim_max
			);


			SK_LineDeco_print_opt (file, deco, "");
		}


		//Determine the range of elements to create storage objects
		//with

		const size_t RNG_NUM_ELEMS = 40;
		const size_t INC_NUM_ELEMS = 1;

		size_t beg_num_elems = 1;
		size_t end_num_elems = beg_num_elems + RNG_NUM_ELEMS - 1;

		if (lim_exist)
		{
			beg_num_elems =

			SK_MAX (beg_num_elems, lim_min);


			end_num_elems =

			SK_MIN ((beg_num_elems + RNG_NUM_ELEMS - 1), lim_max);
		}


		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"beg_num_elems =  %zu",
				beg_num_elems
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"end_num_elems =  %zu",
				end_num_elems
			);


			SK_LineDeco_print_opt (file, deco, "");
		}


		//Determine the amount of static memory that #req calls for

		size_t inst_stat_mem = 0;

		sbi->inst_stat_mem (&req, &inst_stat_mem);


		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"inst_stat_mem = %zu",
				inst_stat_mem
			);


			SK_LineDeco_print_opt (file, deco, "");
		}


		//Allocate the static memory for the storage object instance

		void * inst = malloc (inst_stat_mem);


		if (NULL == inst)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Could not allocate #inst, #inst_stat_mem = %zu",
				inst_stat_mem
			);

			error_detected = 1;

			break;
		}


		//Enter a loop in which storage objects with #num_elems number
		//of elements are created and then sent to
		//SK_BoxIntf_set_get_single_tb () for testing

		for
		(
			size_t num_elems = beg_num_elems;

			num_elems <= end_num_elems;

			num_elems += INC_NUM_ELEMS
		)
		{

			//Initialize #inst to hold #num_elems

			SlySr sr =

			sbi->init
			(
				&req,
				inst,
				num_elems,
				SK_Box_elem_rw_set_zero,
				NULL
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Could not initialize #inst"
				);

				error_detected = 1;

				break;
			}


			//Send #inst to #SK_BoxIntf_set_get_single_tb () for
			//testing

			size_t end_ind = 0;

			if (num_elems > 0)
			{
				end_ind = num_elems - 1;
			}


			SK_LineDeco ind_deco = deco;

			ind_deco.pre_str_cnt += 1;


			sr =

			SK_BoxIntf_set_get_single_tb
			(
				next_depth,
				file,
				ind_deco,

				sbi,
				&req,
				inst,

				0,
				end_ind
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Inner test-bench failed!"
				);

				error_detected = 1;

				break;
			}


			//Deinitialize the storage object instance

			sbi->deinit (&req, inst);

		}


		//Free the memory allocated for the storage object static
		//memory

		free (inst);


		//If an error occurred in the above test loop, propagate this
		//error. Note how this happens AFTER #inst is freed.

		if (error_detected)
		{
			break;
		}


		//Increment the test parameters

		int looped = 0;


		SK_inc_chain
		(
			&elem_size,
			1,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);

		SK_inc_chain
		(
			&num_stat_elems,
			looped,
			BEG_NUM_STAT_ELEMS,
			END_NUM_STAT_ELEMS,
			INC_NUM_STAT_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&elems_per_block,
			looped,
			BEG_ELEMS_PER_BLOCK,
			END_ELEMS_PER_BLOCK,
			INC_ELEMS_PER_BLOCK,
			&looped
		);

		SK_inc_chain
		(
			&neg_slack,
			looped,
			BEG_NEG_SLACK,
			END_NEG_SLACK,
			INC_NEG_SLACK,
			&looped
		);

		SK_inc_chain
		(
			&pos_slack,
			looped,
			BEG_POS_SLACK,
			END_POS_SLACK,
			INC_POS_SLACK,
			&looped
		);

		if (looped)
		{
			break;
		}
	}


	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  This function is used inside of #SK_BoxIntf_anc_tb () to test if the
 *  functions that anchor operations actually perform the intended operations.
 *
 *  Sub-tests used
 *
 *  	None
 *
 *  #SK_BoxIntf functions directly used
 *
 *  	anc_recommend ()
 *  	anc_support ()
 *  	anc_size ()
 *  	anc_validate ()
 *  	anc_ind ()
 *  	anc_inc ()
 *  	anc_dec ()
 *  	get_anc ()
 *  	get_anc_w_anc ()
 *
 *
 *  FIXME : This test-bench HAS NOT BEEN FULLY TESTED! It requires a
 *  #SK_BoxIntf that supports anchors to properly test.
 */

SlySr SK_BoxIntf_anc_single_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi,
	SK_BoxReqs *		req,
	void *			inst,

	size_t			beg_ind,
	size_t			end_ind
)
{
	//Decide on announcing the test-bench

	int print_flag = (0 != p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, req, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, inst, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Check if the values of #beg_ind and #end_ind are reasonable

	if (beg_ind > end_ind)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#beg_ind = %zu and #end_ind = %zu need to be "
			"swapped, fixing this...",
			beg_ind,
			end_ind
		);

		SK_byte_swap (&beg_ind, &end_ind, sizeof (size_t));
	}


	size_t num_elems = 0;

	sbi->get_num_elems (req, inst, &num_elems);


	if (end_ind >= num_elems)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#end_ind = %zu was too large, #num_elems = %zu",
			end_ind,
			num_elems
		);


		return SlySr_get (SLY_BAD_ARG);
	}


	//Determine if anchors are recommended, which can later be compared
	//against whether or not anchors are supported

	int anc_recommend = 0;

	sbi->anc_recommend (req, inst, &anc_recommend);


	//Check if anchors are actually supported by this #SK_BoxIntf

	int anc_support = 0;

	sbi->anc_support (req, &anc_support);


	if (0 == anc_support)
	{
		if (anc_recommend)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#anc_recommend = %d even though "
				"#anc_support = %d. This combination should "
				"be impossible.",
				anc_recommend,
				anc_support
			);


			return SlySr_get (SLY_UNKNOWN_ERROR);
		}

		else
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#anc_support = %d, so executing this "
				"test-bench is not necessary",
				anc_support
			);


			return SlySr_get (SLY_NO_WORK);
		}
	}


	//Determine the size of anchors

	size_t anc_size = 0;

	sbi->anc_size (req, &anc_size);


	if (0 >= anc_size)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#anc_size = %zu, which should be impossible if "
			"#anc_support = %zu",
			anc_size,
			anc_support
		);


		return SlySr_get (SLY_UNKNOWN_ERROR);
	}


	//Allocate space for anchors to use in this test-bench

	void * anc_0 = NULL;
	void * anc_1 = NULL;

	size_t anc_0_ind = 0;
	size_t anc_1_ind = 0;


	anc_0 = malloc (anc_size);
	anc_1 = malloc (anc_size);


	if ((NULL == anc_0) || (NULL == anc_1))
	{
		//Free all of the memory allocated up to this point

		free (anc_0);
		free (anc_1);

		SK_NULL_RETURN_FP
		(
			print_flag,
			file,
			anc_0,
			SlySr_get (SLY_BAD_ALLOC)
		);

		SK_NULL_RETURN_FP
		(
			print_flag,
			file,
			anc_1,
			SlySr_get (SLY_BAD_ALLOC)
		);
	}


	//Determine values that select how many tests to perform

	const i64 NORMAL_ELEM_SEL_BEG =		beg_ind;
	const i64 NORMAL_ELEM_SEL_END =		end_ind;
	const i64 NORMAL_ELEM_SEL_INC =		1;

	const i64 RAND_SEL_BEG =		0;
	const i64 RAND_SEL_END =		1;
	const i64 RAND_SEL_INC =		1;


	i64 normal_elem_sel =	NORMAL_ELEM_SEL_BEG;

	i64 rand_sel =		RAND_SEL_BEG;


	//Enter a test-loop where anchors will be repeatedly obtained for a
	//given storage object

	int go =		1;

	int error_detected = 	0;


	while (go)
	{

		//Select an element to attempt to access.
		//
		//Either accept the normal element selection, or make a random
		//selection within the range [beg_ind, end_ind].
		//
		//Note, the purpose of selecting an element randomly is that
		//this will help identify bugs that occur when _sequences_ of
		//indices cause the storage object to get into a bad state.

		size_t elem_sel = normal_elem_sel;

		if (rand_sel)
		{
			elem_sel =

			SK_range_oflow_i64
			(
				SK_gold_hash_u64 (normal_elem_sel),
				beg_ind,
				end_ind
			);
		}


		//Attempt get an anchor associated with the current index

		SlyDr dr = sbi->get_anc (req, inst, elem_sel, anc_0);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Could not get anchor for #elem_sel = %zu",
				elem_sel
			);

			error_detected = 1;

			break;
		}


		//Check whether anc_validate () reports #anc_0 as valid or not

		int is_valid = 0;

		dr = sbi->anc_validate (req, inst, anc_0, &anc_0_ind, &is_valid);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Call to anc_validate () failed."
			);

			error_detected = 1;

			break;
		}


		if (0 == is_valid)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#anc_0 appeared to be invalid, #is_valid = %d",
				is_valid
			);

			error_detected = 1;

			break;
		}


		if (anc_0_ind != elem_sel)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Somehow #anc_0_ind = %d and #elem_sel = %d "
				"did not match.",
				anc_0_ind,
				elem_sel
			);

			error_detected = 1;

			break;
		}


		//Check if the index of #anc_0 can be incremented

		if (anc_0_ind < end_ind)
		{

			//Increment the element index associated with #anc_0

			dr = sbi->anc_inc (req, inst, anc_0);

			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Incrementing the index associated "
					"with #anc_0 failed."
				);

				error_detected = 1;

				break;
			}


			//Attempt to fetch the index from the anchor, and
			//compare it with #elem_sel

			size_t anc_ind_0 = 0;

			dr = sbi->anc_ind (req, inst, anc_0, &anc_ind_0);


			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Could not get an index from #anc_0 = %p",
					anc_0
				);

				error_detected = 1;

				break;
			}


			if (anc_ind_0 != (elem_sel + 1))
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"#anc_ind_0 = %d and #elem_sel = %d, "
					"#anc_ind_0 should be +1 MORE than "
					"#elem_sel at this time."
				);

				error_detected = 1;

				break;
			}


			//Use sbi->get_anc_w_anc () to reset the index of
			//#anc_0 back to #elem_sel.
			//
			//Note how this tests when #get_anc_w_anc () is given
			//the same anchor argument twice

			dr =

			sbi->get_anc_w_anc
			(
				req,
				inst,
				elem_sel,
				anc_0,
				anc_0
			);


			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Call to sbi->get_anc_w_anc () failed."
				);

				error_detected = 1;

				break;
			}


			sbi->anc_validate
			(
				req,
				inst,
				anc_0,
				&anc_0_ind,
				&is_valid
			);


			if (0 == is_valid)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"#anc_0 appeared to be invalid, #is_valid = %d",
					is_valid
				);

				error_detected = 1;

				break;
			}


			if (anc_0_ind != elem_sel)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Somehow #anc_0_ind = %d and #elem_sel = %d "
					"did not match.",
					anc_0_ind,
					elem_sel
				);

				error_detected = 1;

				break;
			}
		}


		//Check if the index of #anc_0 can be decremented

		if (beg_ind < anc_0_ind)
		{
			//Decrement the element index associated with #anc_0

			dr = sbi->anc_dec (req, inst, anc_0);

			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Incrementing the index associated "
					"with #anc_0 failed."
				);

				error_detected = 1;

				break;
			}


			//Attempt to fetch the index from the anchor, and
			//compare it with #elem_sel

			size_t anc_ind_0 = 0;

			dr = sbi->anc_ind (req, inst, anc_0, &anc_ind_0);


			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Could not get an index from #anc_0 = %p",
					anc_0
				);

				error_detected = 1;

				break;
			}


			if (anc_ind_0 != (elem_sel - 1))
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"#anc_ind_0 = %d and #elem_sel = %d, "
					"#anc_ind_0 should be +1 LESS than "
					"#elem_sel at this time."
				);

				error_detected = 1;

				break;
			}


			//Use sbi->get_anc_w_anc () to reset the index of
			//#anc_0 back to #elem_sel.
			//
			//Note how this tests when #get_anc_w_anc () is given
			//the same anchor argument twice

			dr =

			sbi->get_anc_w_anc
			(
				req,
				inst,
				elem_sel,
				anc_0,
				anc_0
			);


			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Call to sbi->get_anc_w_anc () failed."
				);

				error_detected = 1;

				break;
			}


			sbi->anc_validate
			(
				req,
				inst,
				anc_0,
				&anc_0_ind,
				&is_valid
			);


			if (0 == is_valid)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"#anc_0 appeared to be invalid, #is_valid = %d",
					is_valid
				);

				error_detected = 1;

				break;
			}


			if (anc_0_ind != elem_sel)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Somehow #anc_0_ind = %d and #elem_sel = %d "
					"did not match.",
					anc_0_ind,
					elem_sel
				);

				error_detected = 1;

				break;
			}

		}


		//Attempt to use sbi->get_anc_w_anc () to get anchors at the
		//extremes of the data set, using an anchor at #elem_sel as the
		//basis

		//Get an anchor for the element at index #beg_ind

		dr = sbi->get_anc_w_anc (req, inst, beg_ind, anc_0, anc_1);

		if (dr.res != SLY_SUCCESS)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Call to sbi->get_anc_w_anc () failed."
			);

			error_detected = 1;

			break;
		}


		sbi->anc_validate (req, inst, anc_1, &anc_1_ind, &is_valid);


		if (0 == is_valid)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#anc_1 appeared to be invalid, #is_valid = %d",
				is_valid
			);

			error_detected = 1;

			break;
		}


		if (anc_1_ind != beg_ind)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Somehow #anc_1_ind = %d and #beg_ind = %d "
				"did not match.",
				anc_1_ind,
				beg_ind
			);

			error_detected = 1;

			break;
		}


		//Get an anchor for the element at index #end_ind

		dr = sbi->get_anc_w_anc (req, inst, end_ind, anc_0, anc_1);

		if (dr.res != SLY_SUCCESS)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Call to sbi->get_anc_w_anc () failed."
			);

			error_detected = 1;

			break;
		}


		sbi->anc_validate (req, inst, anc_1, &anc_1_ind, &is_valid);


		if (0 == is_valid)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#anc_1 appeared to be invalid, #is_valid = %d",
				is_valid
			);

			error_detected = 1;

			break;
		}


		if (anc_1_ind != end_ind)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Somehow #anc_1_ind = %d and #end_ind = %d "
				"did not match.",
				anc_1_ind,
				end_ind
			);

			error_detected = 1;

			break;
		}


		//Increment the test parameters

		int looped = 0;

		SK_inc_chain
		(
			&normal_elem_sel,
			1,
			NORMAL_ELEM_SEL_BEG,
			NORMAL_ELEM_SEL_END,
			NORMAL_ELEM_SEL_INC,
			&looped
		);

		SK_inc_chain
		(
			&rand_sel,
			looped,
			RAND_SEL_BEG,
			RAND_SEL_END,
			RAND_SEL_INC,
			&looped
		);

		if (looped)
		{
			break;
		}
	}


	//Free the anchors allocated earlier in this function

	free (anc_0);
	free (anc_1);


	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);

}




/*!
 *  This function is used inside of #SK_BoxIntf_array_tb () to test the
 *  functions that operate on arrays.
 *
 *  Sub-tests used
 *
 *	None
 *
 *  #SK_BoxIntf functions directly used
 *
 *  	region_avg_len ()
 *	get_region_ptr ()
 *  	get_region_ptr_w_anc ()
 *  	get_region_ptr_by_anc ()
 *  	get_array ()
 *  	get_array_w_anc ()
 *  	get_array_by_anc ()
 *  	set_array ()
 *  	set_array_w_anc ()
 *  	set_array_by_anc ()
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param sbi			The #SK_BoxIntf to use when accessing #inst
 *
 *  @param req			The #SK_BoxReqs to use when accessing #inst
 *
 *  @param inst			The #SK_Box instance to use in this test
 *
 *  @param beg_ind		The starting index in #inst from which arrays
 *				should be set / get.
 *
 *  @param end_ind		The ending index in #inst from which arrays
 *				should be set / get.
 *
 *  @param report_r_avg_len	Where to place the result from calling
 *  				#intf->region_avg_len () on #inst
 *
 *  @param encount_r_avg_len	Where to place the observed average number of
 *				elements associated from regions in #inst
 *
 *  @return			Standard status code
 */

SlySr SK_BoxIntf_array_single_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi,
	const SK_BoxReqs *	req,
	void *			inst,

	size_t			beg_ind,
	size_t			end_ind,

	double *		report_r_avg_len,
	double *		encount_r_avg_len
)
{
	//Decide on annoucning the test-bench

	int print_flag = (0 != p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, req, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, inst, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Check if the values of #beg_ind and #end_ind are reasonable

	if (beg_ind > end_ind)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#beg_ind = %zu and #end_ind = %zu need to be "
			"swapped, fixing this...",
			beg_ind,
			end_ind
		);

		SK_byte_swap (&beg_ind, &end_ind, sizeof (size_t));
	}


	size_t num_elems = 0;

	sbi->get_num_elems (req, inst, &num_elems);


	if (end_ind >= num_elems)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#end_ind = %zu was too large, #num_elems = %zu",
			end_ind,
			num_elems
		);


		return SlySr_get (SLY_BAD_ARG);
	}


	//Print the most relevant test parameters

	if (print_flag)
	{
		SK_LineDeco_print_opt
		(
			file,
			deco,
			"(num_elems, beg_ind, end_ind) = (%zu, %zu, %zu)",
			num_elems,
			beg_ind,
			end_ind
		);

		SK_LineDeco_print_opt (file, deco, "\n");
	}


	//Check if anchors are supported by #sbi

	int anc_support = 0;

	sbi->anc_support (req, &anc_support);


	//Determine the size of anchors, if supported

	size_t anc_size = 0;


	if (anc_support)
	{
		sbi->anc_size (req, &anc_size);


		if (0 >= anc_size)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#anc_size = %zu, which should be impossible "
				"if #anc_support = %zu",
				anc_size,
				anc_support
			);


			return SlySr_get (SLY_UNKNOWN_ERROR);
		}
	}


	//Determine if region pointers are recommended for this storage object

	double avg_len = 0;

	SlyDr dr = sbi->region_avg_len (req, inst, &avg_len);


	if (SLY_SUCCESS != dr.res)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#sbi->region_avg_len () did not report "
			"success. #sbi = %p",
			sbi
		);


		return SlySr_get (dr.res);
	}


	if (NULL != report_r_avg_len)
	{
		*report_r_avg_len = avg_len;
	}


	//Allocate a separate array that can also store the elements from the
	//index range [beg_ind, end_ind].

	size_t elem_size = req->stat_r->elem_size;

	size_t array_num_elems = (end_ind - beg_ind) + 1;

	void * array_0 = malloc (elem_size * array_num_elems);
	void * array_1 = malloc (elem_size * array_num_elems);


	if ((NULL == array_0) || (NULL == array_1))
	{
		free (array_0);
		free (array_1);

		SK_NULL_RETURN_FP
		(
			print_flag,
			file,
			array_0,
			SlySr_get (SLY_BAD_ALLOC)
		);

		SK_NULL_RETURN_FP
		(
			print_flag,
			file,
			array_1,
			SlySr_get (SLY_BAD_ALLOC)
		);
	}


	//Set the elements in #array_0 to arbitrary values, and set #array_1 to
	//all zeros

	for (size_t elem_sel = 0; elem_sel < array_num_elems; elem_sel += 1)
	{
		for (size_t byte_sel = 0; byte_sel < elem_size; byte_sel += 1)
		{
			//Get the pointer to the element in #array_0

			u8 * elem_ptr =

			((u8 *) array_0) + (elem_sel * elem_size);


			 //Set the element to an arbitrary value

			elem_ptr [byte_sel] = elem_sel + byte_sel;
		}
	}

	SK_byte_set_all (array_1, 0, array_num_elems * elem_size);


	//Try setting a range of elements inside of the storage object

	dr =

	sbi->set_array (req, inst, beg_ind, end_ind, array_num_elems, array_0);


	if (SLY_SUCCESS != dr.res)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#sbi->set_array () did not report success, #sbi = %p, dr.res = %s",
			sbi,
			SlyDrStr_get (dr).str
		);

		free (array_0);
		free (array_1);

		return SlySr_get (dr.res);
	}


	//Get the elements that were just set so that they can be checked for
	//correctness

	dr =

	sbi->get_array (req, inst, beg_ind, end_ind, array_num_elems, array_1);


	if (SLY_SUCCESS != dr.res)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#sbi->get_array () did not report success, #sbi = %p, dr.res = %s",
			sbi,
			SlyDrStr_get (dr).str
		);

		free (array_0);
		free (array_1);

		return SlySr_get (dr.res);
	}


	//Ensure that both arrays now match one another.

	int eq =		0;

	size_t ineq_ind =	0;

	SK_byte_eq
	(
		array_0,
		(elem_size * array_num_elems),

		array_1,
		(elem_size * array_num_elems),

		&eq,
		&ineq_ind
	);


	if (0 == eq)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"Either #sbi->set_array () or #sbi->get_array () "
			"not working, #array_0 did not match #array_1 "
			"byte-for-byte\n"
			"sbi = %p\n\n"
			"((u8 *) array_0) [%zu] = %" PRIu8 "\n"
			"((u8 *) array_1) [%zu] = %" PRIu8 "\n",
			sbi,
			ineq_ind,
			((u8 *) array_0) [ineq_ind],
			ineq_ind,
			((u8 *) array_1) [ineq_ind]
		);

		free (array_0);
		free (array_1);

		return SlySr_get (dr.res);
	}


	//Use region pointers to access all the elements in the range of
	//indices [beg_ind, end_ind], whilst calculating the average number of
	//elements contained in each region

	size_t num_regs =	0;
	size_t num_reg_elems =	0;

	size_t req_ind =	beg_ind;

	void * reg_ptr =	NULL;
	size_t reg_beg_ind =	0;
	size_t reg_end_ind =	0;


	while (req_ind <= end_ind)
	{
		//Request a region containing the index #req_ind

		dr =

		sbi->get_region_ptr
		(
			req,
			inst,
			req_ind,
			&reg_ptr,
			&reg_beg_ind,
			&reg_end_ind
		);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#sbi->get_region_ptr () did not report "
				"success. #sbi = %p, dr.res = %s",
				sbi,
				SlyDrStr_get (dr).str
			);


			free (array_0);
			free (array_1);


			return SlySr_get (dr.res);
		}


		//Keep count of how many regions have been encountered so far

		num_regs += 1;

		num_reg_elems += (reg_end_ind - reg_beg_ind) + 1;


		//Determine if this region contains elements that are
		//equivalent to the same region in #array_0

		size_t used_end_ind =		SK_MIN (reg_end_ind, end_ind);

		size_t used_reg_len =		(used_end_ind - req_ind) + 1;

		size_t reg_ind_offset =		req_ind - reg_beg_ind;

		size_t array_ind_offset =	req_ind - beg_ind;


		int eq =		0;

		size_t ineq_ind =	0;


		SK_byte_eq
		(
			(u8 *) reg_ptr + (reg_ind_offset * elem_size),
			used_reg_len * elem_size,

			(u8 *) array_0 + (array_ind_offset * elem_size),
			used_reg_len * elem_size,

			&eq,
			&ineq_ind
		);


		if (0 == eq)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#sbi->get_region_ptr () not working, "
				"#reg_ptr did not match #array_0 "
				"byte-for-byte for the index range "
				"[%zu, %zu].\n"
				"sbi = %p\n\n"
				"((u8 *) reg_ptr + (%zu * %zu)) [%zu] = %" PRIu8 "\n"
				"((u8 *) array_0 + (%zu * %zu)) [%zu] = %" PRIu8 "\n",
				req_ind,
				used_end_ind,
				sbi,
				reg_ind_offset,
				elem_size,
				ineq_ind,
				((u8 *) reg_ptr + (reg_ind_offset * elem_size)) [ineq_ind],
				array_ind_offset,
				elem_size,
				ineq_ind,
				((u8 *) array_0 + (array_ind_offset * elem_size)) [ineq_ind]
			);

			free (array_0);
			free (array_1);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		//Increment #req_ind for the next region

		req_ind = reg_end_ind + 1;
	}


	//Report the average number of elements in each region, if requested

	if
	(
		(NULL != encount_r_avg_len)

		&&

		(0 != num_regs)
	)
	{
		*encount_r_avg_len = ((double) num_reg_elems) / num_regs;
	}


	//If anchors are not supported, there are no more tests to run, so in
	//that event, proceed to end the test-bench.

	if (0 == anc_support)
	{
		free (array_0);
		free (array_1);


		int error_detected = 0;


		SK_TB_END_PRINT_STD
		(
			print_flag,
			file,
			deco,
			error_detected,
			SlySr_get (SLY_UNKNOWN_ERROR),
			SlySr_get (SLY_SUCCESS)
		);
	}


	//Allocate space for anchors, which will be needed for testing the
	//*_w_anc () and *_by_anc () versions of functions

	SK_BoxAnc * anc_0 = malloc (anc_size);
	SK_BoxAnc * anc_1 = malloc (anc_size);


	if ((NULL == anc_0) || (NULL == anc_1))
	{
		free (array_0);
		free (array_1);

		free (anc_0);
		free (anc_1);

		SK_NULL_RETURN_FP
		(
			print_flag,
			file,
			anc_0,
			SlySr_get (SLY_BAD_ALLOC)
		);

		SK_NULL_RETURN_FP
		(
			print_flag,
			file,
			anc_1,
			SlySr_get (SLY_BAD_ALLOC)
		);
	}


	//Initially, randomly select what elements the anchors should be
	//associated with

	//FIXME : What exactly IS the best way for anchor indices to get set in
	//this function?

	sbi->get_num_elems (req, inst, &num_elems);


	u64 hash_input =	num_elems + beg_ind + end_ind;

	size_t init_anc_0_ind =	SK_gold_hash_u64 (hash_input) % num_elems;

	size_t init_anc_1_ind = SK_gold_hash_u64 (hash_input) % num_elems;


	sbi->get_anc (req, inst, init_anc_0_ind, anc_0);

	sbi->get_anc (req, inst, init_anc_1_ind, anc_1);




	//
	//Testing the *_w_anc () versions of functions here
	//


	//FIXME : Should the elements in #inst get reset at this point?


	//Before calling the *_w_anc () functions, set all of the elements in
	//#array_1 back to 0

	SK_byte_set_all (array_1, 0, array_num_elems * elem_size);


	//Try setting a range of elements inside of the storage object

	dr =

	sbi->set_array_w_anc
	(
		req,
		inst,
		beg_ind,
		end_ind,

		beg_ind,
		anc_0,
		anc_0,

		array_num_elems,
		array_0
	);


	if (SLY_SUCCESS != dr.res)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#sbi->set_array_w_anc () did not report success, "
			"#sbi = %p, dr.res = %s",
			sbi,
			SlyDrStr_get (dr).str
		);

		free (array_0);
		free (array_1);

		free (anc_0);
		free (anc_1);

		return SlySr_get (dr.res);
	}


	//Get the elements that were just set so that they can be checked for
	//correctness

	dr =

	sbi->get_array_w_anc
	(
		req,
		inst,
		beg_ind,
		end_ind,

		end_ind,
		anc_1,
		anc_1,

		array_num_elems,
		array_1
	);


	if (SLY_SUCCESS != dr.res)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#sbi->get_array_w_anc () did not report success, "
			"#sbi = %p, dr.res = %s",
			sbi,
			SlyDrStr_get (dr).str
		);

		free (array_0);
		free (array_1);

		free (anc_0);
		free (anc_1);

		return SlySr_get (dr.res);
	}


	//Ensure that both arrays now match one another.

	eq =		0;

	ineq_ind =	0;

	SK_byte_eq
	(
		array_0,
		(elem_size * array_num_elems),

		array_1,
		(elem_size * array_num_elems),

		&eq,
		&ineq_ind
	);


	if (0 == eq)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"Either #sbi->set_array_w_anc () or "
			"#sbi->get_array_w_anc () not working, #array_0 did "
			"not match #array_1 byte-for-byte\n"
			"sbi = %p\n\n"
			"((u8 *) array_0) [%zu] = %" PRIu8 "\n"
			"((u8 *) array_1) [%zu] = %" PRIu8 "\n",
			sbi,
			ineq_ind,
			((u8 *) array_0) [ineq_ind],
			ineq_ind,
			((u8 *) array_1) [ineq_ind]
		);

		free (array_0);
		free (array_1);

		free (anc_0);
		free (anc_1);

		return SlySr_get (dr.res);
	}


	//Use region pointers to access all the elements in the range of
	//indices [beg_ind, end_ind]

	req_ind =	beg_ind;

	reg_ptr =	NULL;
	reg_beg_ind =	0;
	reg_end_ind =	0;


	while (req_ind <= end_ind)
	{
		//Request a region containing the index #req_ind

		dr =

		sbi->get_region_ptr_w_anc
		(
			req,
			inst,
			req_ind,

			anc_0,

			anc_1,
			anc_0,

			&reg_ptr,
			&reg_beg_ind,
			&reg_end_ind
		);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#sbi->get_region_ptr_w_anc () did not report "
				"success. #sbi = %p, dr.res = %s",
				sbi,
				SlyDrStr_get (dr).str
			);

			free (array_0);
			free (array_1);

			free (anc_0);
			free (anc_1);

			return SlySr_get (dr.res);
		}


		//Note, the region count has already been performed previously
		//in this test-bench, so it is unnecessary to perform again


		//Ensure that #anc_1 is now associated with the element index
		//(reg_beg_ind - 1) or the first element in #inst

		size_t anc_1_ind = 0;

		sbi->anc_ind (req, inst, anc_1, &anc_1_ind);


		if
		(
			((reg_beg_ind - 1) != anc_1_ind)

			&&

			(0 != anc_1_ind)
		)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#sbi->get_region_ptr_by_anc () did not set "
				"anc_1 to the proper index, #anc_1_ind = %zu",
				anc_1_ind
			);

			free (array_0);
			free (array_1);

			free (anc_1);
			free (anc_1);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}



		//Ensure that #anc_0 is now associated with the element index
		//(reg_end_ind + 1) or the last element in #inst

		size_t anc_0_ind = 0;

		sbi->anc_ind (req, inst, anc_0, &anc_0_ind);


		if
		(
			((reg_end_ind + 1) != anc_0_ind)

			&&

			((num_elems - 1) != anc_0_ind)
		)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#sbi->get_region_ptr_by_anc () did not set "
				"anc_0 to the proper index, #anc_0_ind = %zu",
				anc_0_ind
			);

			free (array_0);
			free (array_1);

			free (anc_0);
			free (anc_1);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		//Determine if this region contains elements that are
		//equivalent to the same region in #array_0

		size_t used_end_ind =		SK_MIN (reg_end_ind, end_ind);

		size_t used_reg_len =		(used_end_ind - req_ind) + 1;

		size_t reg_ind_offset =		req_ind - reg_beg_ind;

		size_t array_ind_offset =	req_ind - beg_ind;


		int eq =		0;

		size_t ineq_ind =	0;


		SK_byte_eq
		(
			(u8 *) reg_ptr + (reg_ind_offset * elem_size),
			used_reg_len * elem_size,

			(u8 *) array_0 + (array_ind_offset * elem_size),
			used_reg_len * elem_size,

			&eq,
			&ineq_ind
		);


		if (0 == eq)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#sbi->get_region_ptr_w_anc () not working, "
				"#reg_ptr did not match #array_0 "
				"byte-for-byte for the index range "
				"[%zu, %zu].\n"
				"sbi = %p\n\n"
				"((u8 *) reg_ptr + (%zu * %zu)) [%zu] = %" PRIu8 "\n"
				"((u8 *) array_0 + (%zu * %zu)) [%zu] = %" PRIu8 "\n",
				req_ind,
				used_end_ind,
				sbi,
				reg_ind_offset,
				elem_size,
				ineq_ind,
				((u8 *) reg_ptr + (reg_ind_offset * elem_size)) [ineq_ind],
				array_ind_offset,
				elem_size,
				ineq_ind,
				((u8 *) array_0 + (array_ind_offset * elem_size)) [ineq_ind]
			);

			free (array_0);
			free (array_1);

			free (anc_0);
			free (anc_1);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		//Increment #req_ind for the next region

		req_ind = reg_end_ind + 1;
	}




	//
	//Testing the *_by_anc () versions of functions here
	//


	//FIXME : Should the elements in #inst get reset at this point?


	//Before calling the *_by_anc () functions, set all of the elements in
	//#array_1 back to 0

	SK_byte_set_all (array_1, 0, array_num_elems * elem_size);


	//Try setting a range of elements inside of the storage object

	sbi->get_anc (req, inst, beg_ind, anc_0);
	sbi->get_anc (req, inst, end_ind, anc_1);


	dr =

	sbi->set_array_by_anc
	(
		req,
		inst,
		anc_0,
		anc_1,
		array_num_elems,
		array_0
	);


	if (SLY_SUCCESS != dr.res)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#sbi->set_array_by_anc () did not report success, "
			"#sbi = %p, dr.res = %s",
			sbi,
			SlyDrStr_get (dr).str
		);

		free (array_0);
		free (array_1);

		free (anc_0);
		free (anc_1);

		return SlySr_get (dr.res);
	}


	//Get the elements that were just set so that they can be checked for
	//correctness

	sbi->get_anc (req, inst, beg_ind, anc_0);
	sbi->get_anc (req, inst, end_ind, anc_1);


	dr =

	sbi->get_array_by_anc
	(
		req,
		inst,
		anc_0,
		anc_1,
		array_num_elems,
		array_1
	);


	if (SLY_SUCCESS != dr.res)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#sbi->get_array_by_anc () did not report success, "
			"#sbi = %p, dr.res = %s",
			sbi,
			SlyDrStr_get (dr).str
		);

		free (array_0);
		free (array_1);

		free (anc_0);
		free (anc_1);

		return SlySr_get (dr.res);
	}


	//Ensure that both arrays now match one another.

	eq =		0;

	ineq_ind =	0;

	SK_byte_eq
	(
		array_0,
		(elem_size * array_num_elems),

		array_1,
		(elem_size * array_num_elems),

		&eq,
		&ineq_ind
	);


	if (0 == eq)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"Either #sbi->set_array_by_anc () or "
			"#sbi->get_array_by_anc () not working, #array_0 did "
			"not match #array_1 byte-for-byte\n"
			"sbi = %p\n\n"
			"((u8 *) array_0) [%zu] = %" PRIu8 "\n"
			"((u8 *) array_1) [%zu] = %" PRIu8 "\n",
			sbi,
			ineq_ind,
			((u8 *) array_0) [ineq_ind],
			ineq_ind,
			((u8 *) array_1) [ineq_ind]
		);

		free (array_0);
		free (array_1);

		free (anc_0);
		free (anc_1);

		return SlySr_get (dr.res);
	}


	//Use region pointers to access all the elements in the range of
	//indices [beg_ind, end_ind]

	req_ind =	beg_ind;

	reg_ptr =	NULL;
	reg_beg_ind =	0;
	reg_end_ind =	0;


	sbi->get_anc (req, inst, req_ind, anc_0);


	while (req_ind <= end_ind)
	{
		//Request a region containing the index #req_ind

		dr =

		sbi->get_region_ptr_by_anc
		(
			req,
			inst,
			anc_0,

			anc_1,
			anc_0,

			&reg_ptr,
			&reg_beg_ind,
			&reg_end_ind
		);


		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#sbi->get_region_ptr_by_anc () did not report "
				"success. #sbi = %p, dr.res = %s",
				sbi,
				SlyDrStr_get (dr).str
			);

			free (array_0);
			free (array_1);

			free (anc_0);
			free (anc_1);

			return SlySr_get (dr.res);
		}


		//Ensure that #anc_1 is now associated with the element index
		//(reg_beg_ind - 1) or the first element in #inst

		size_t anc_1_ind = 0;

		sbi->anc_ind (req, inst, anc_1, &anc_1_ind);


		if
		(
			((reg_beg_ind - 1) != anc_1_ind)

			&&

			(0 != anc_1_ind)
		)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#sbi->get_region_ptr_by_anc () did not set "
				"anc_1 to the proper index, #anc_1_ind = %zu",
				anc_1_ind
			);

			free (array_0);
			free (array_1);

			free (anc_1);
			free (anc_1);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		//Ensure that #anc_0 is now associated with the element index
		//(reg_end_ind + 1) or the last element in #inst

		size_t anc_0_ind = 0;

		sbi->anc_ind (req, inst, anc_0, &anc_0_ind);


		if
		(
			((reg_end_ind + 1) != anc_0_ind)

			&&

			((num_elems - 1) != anc_0_ind)
		)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#sbi->get_region_ptr_by_anc () did not set "
				"anc_0 to the proper index, #anc_0_ind = %zu",
				anc_0_ind
			);

			free (array_0);
			free (array_1);

			free (anc_0);
			free (anc_1);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}


		//Note, the region count has already been performed previously
		//in this test-bench, so it is unnecessary to perform again


		//Determine if this region contains elements that are
		//equivalent to the same region in #array_0

		size_t used_end_ind =		SK_MIN (reg_end_ind, end_ind);

		size_t used_reg_len =		(used_end_ind - req_ind) + 1;

		size_t reg_ind_offset =		req_ind - reg_beg_ind;

		size_t array_ind_offset =	req_ind - beg_ind;


		int eq =		0;

		size_t ineq_ind =	0;


		SK_byte_eq
		(
			(u8 *) reg_ptr + (reg_ind_offset * elem_size),
			used_reg_len * elem_size,

			(u8 *) array_0 + (array_ind_offset * elem_size),
			used_reg_len * elem_size,

			&eq,
			&ineq_ind
		);


		if (0 == eq)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#sbi->get_region_ptr_by_anc () not working, "
				"#reg_ptr did not match #array_0 "
				"byte-for-byte for the index range "
				"[%zu, %zu].\n"
				"sbi = %p\n\n"
				"((u8 *) reg_ptr + (%zu * %zu)) [%zu] = %" PRIu8 "\n"
				"((u8 *) array_0 + (%zu * %zu)) [%zu] = %" PRIu8 "\n",
				req_ind,
				used_end_ind,
				sbi,
				reg_ind_offset,
				elem_size,
				ineq_ind,
				((u8 *) reg_ptr + (reg_ind_offset * elem_size)) [ineq_ind],
				array_ind_offset,
				elem_size,
				ineq_ind,
				((u8 *) array_0 + (array_ind_offset * elem_size)) [ineq_ind]
			);

			free (array_0);
			free (array_1);

			free (anc_0);
			free (anc_1);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}
	}


	//If this section was reached, then the test-bench was successful

	free (array_0);
	free (array_1);

	free (anc_0);
	free (anc_1);


	int error_detected = 0;


	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Given an implementation of #SK_BoxIntf, this performs a test-bench that
 *  checks if the functions that operate on entire arrays at a time work
 *  properly.
 *
 *
 *  Sub-tests used
 *
 *	SK_BoxIntf_array_single_tb ()
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *	None
 *
 *
 *  @param p_depth		The "printing depth".
 *
 *  				If (p_depth <= 0), then this test-bench will
 *  				NOT print out results whatsoever to #file.
 *
 *  				If (p_depth > 0), then this test-bench WILL
 *				print out results to #file.
 *
 *				#p_depth will be decremented for inner
 *				test-benches until it reaches 0.
 *
 *				(-1) indicates infinite printing depth.
 *
 *  @param file			Pointer to the #FILE to print messages to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate
 *				printed lines
 *
 *  @param sbi			The #SK_BoxIntf to test
 *
 *  @return			Standard status code
 */

SlySr SK_BoxIntf_array_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
)
{

	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);

	i64 next_depth = SK_p_depth_next (p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Select constants that determine how many parameterizations to test.

	const i64 BEG_ELEM_SIZE = 		1;
	const i64 END_ELEM_SIZE = 		10;
	const i64 INC_ELEM_SIZE = 		1;

	const i64 BEG_NUM_STAT_ELEMS =		0;
	const i64 END_NUM_STAT_ELEMS =		4;
	const i64 INC_NUM_STAT_ELEMS =		1;

	const i64 BEG_ELEMS_PER_BLOCK =		0;
	const i64 END_ELEMS_PER_BLOCK =		4;
	const i64 INC_ELEMS_PER_BLOCK =		1;

	const i64 BEG_NEG_SLACK =		0;
	const i64 END_NEG_SLACK =		2;
	const i64 INC_NEG_SLACK =		1;

	const i64 BEG_POS_SLACK =		0;
	const i64 END_POS_SLACK =		2;
	const i64 INC_POS_SLACK =		1;

	const i64 BEG_RAND_IND =		0;
	const i64 END_RAND_IND =		1;
	const i64 INC_RAND_IND =		1;


	i64 elem_size =				BEG_ELEM_SIZE;

	i64 num_stat_elems =			BEG_NUM_STAT_ELEMS;

	i64 elems_per_block =			BEG_ELEMS_PER_BLOCK;

	i64 neg_slack =				BEG_NEG_SLACK;

	i64 pos_slack =				BEG_POS_SLACK;

	i64 rand_ind =				BEG_RAND_IND;


	//Enter the test-loop, in which storage objects with different
	//parameters will be initialized and deinitialized

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//Create an #SK_BoxReqs that describes the storage object that
		//is desired to be tested

		SK_BoxStatReqs srq =

		{
			.mem =				&SK_MemStd,

			.elem_size =			elem_size,

			.hint_num_stat_elems =		num_stat_elems,

			.hint_elems_per_block =		elems_per_block
		};


		SK_BoxDynaReqs drq =

		{
			.hint_neg_slack =			neg_slack,

			.hint_pos_slack =			pos_slack,

			.hint_use_max_num_dyna_blocks =		0,

			.hint_max_num_dyna_blocks =		0
		};


		SK_BoxStatImpl sim;

		SK_BoxDynaImpl dim;

		SK_BoxReqs req;


		SK_BoxReqs_prep (&req, &srq, &sim, &drq, &dim);


		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"The current #SK_BoxReqs is...\n\n"
			);

			SK_BoxReqs_print (&req, 2, file, deco);

			SK_LineDeco_print_opt (file, deco, "");
		}


		//Determine if there are limits to how many elements the
		//storage object described by #req can store

		int lim_exist = 0;

		size_t lim_min = 0;
		size_t lim_max = 0;

		sbi->get_resize_limits (&req, &lim_exist, &lim_min, &lim_max);


		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"lim_exist = %zu",
				lim_exist
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"lim_min =   %zu",
				lim_min
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"lim_max =   %zu",
				lim_max
			);


			SK_LineDeco_print_opt (file, deco, "");
		}


		//Determine the range of elements to create storage objects
		//with

		const size_t RNG_NUM_ELEMS = 40;
		const size_t INC_NUM_ELEMS = 1;

		size_t beg_num_elems = 1;
		size_t end_num_elems = beg_num_elems + RNG_NUM_ELEMS - 1;

		if (lim_exist)
		{
			beg_num_elems =

			SK_MAX (beg_num_elems, lim_min);


			end_num_elems =

			SK_MIN ((beg_num_elems + RNG_NUM_ELEMS - 1), lim_max);
		}


		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"beg_num_elems =  %zu",
				beg_num_elems
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"end_num_elems =  %zu",
				end_num_elems
			);


			SK_LineDeco_print_opt (file, deco, "");
		}


		//Determine the amount of static memory that #req calls for

		size_t inst_stat_mem = 0;

		sbi->inst_stat_mem (&req, &inst_stat_mem);


		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"inst_stat_mem = %zu",
				inst_stat_mem
			);


			SK_LineDeco_print_opt (file, deco, "");
		}


		//Allocate the static memory for the storage object instance

		void * inst = malloc (inst_stat_mem);


		if (NULL == inst)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Could not allocate #inst, #inst_stat_mem = %zu",
				inst_stat_mem
			);

			error_detected = 1;

			break;
		}


		//Enter a loop in which storage objects with #num_elems number
		//of elements are created and then sent to
		//SK_BoxIntf_array_single_tb () for testing

		for
		(
			size_t num_elems = beg_num_elems;

			num_elems <= end_num_elems;

			num_elems += INC_NUM_ELEMS
		)
		{

			//Initialize #inst to hold #num_elems

			SlySr sr =

			sbi->init
			(
				&req,
				inst,
				num_elems,
				SK_Box_elem_rw_set_zero,
				NULL
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Could not initialize #inst"
				);

				error_detected = 1;

				break;
			}


			//Select the range of indices in #inst that should be
			//used as a source / destination in
			//SK_BoxIntf_array_single_tb ()

			size_t beg_ind = 0;
			size_t end_ind = 0;

			if (0 == rand_ind)
			{
				if (num_elems > 0)
				{
					end_ind = num_elems - 1;
				}
			}

			else
			{
				u64 hash_in =

				elem_size +
				num_stat_elems +
				elems_per_block +
				neg_slack +
				pos_slack +
				rand_ind +
				num_elems;


				end_ind =

				SK_gold_hash_u64 (hash_in) % num_elems;


				beg_ind =

				(SK_gold_hash_u64 (hash_in + 1) % (end_ind + 1));
			}


			//Send #inst to #SK_BoxIntf_array_single_tb () for
			//testing

			SK_LineDeco ind_deco = deco;

			ind_deco.pre_str_cnt += 1;


			double report_r_avg_len = 0;

			double encount_r_avg_len = 0;


			sr =

			SK_BoxIntf_array_single_tb
			(
				next_depth,
				file,
				ind_deco,

				sbi,
				&req,
				inst,

				beg_ind,
				end_ind,

				&report_r_avg_len,
				&encount_r_avg_len
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Inner test-bench failed!"
				);

				error_detected = 1;

				break;
			}


			if (print_flag)
			{
				SK_LineDeco_print_opt
				(
					file,
					deco,
					"report_r_avg_len =   %lf",
					report_r_avg_len
				);


				SK_LineDeco_print_opt
				(
					file,
					deco,
					"encount_r_avg_len =  %lf",
					encount_r_avg_len
				);


				SK_LineDeco_print_opt (file, deco, "");
			}


			//TODO : Decide if any further checking on
			//#report_r_avg_len and #encount_r_avg_len, such as if
			//they are a reasonable distance from one another.

			//FIXME : Should #sbi->init () and #sbi->deinit () be
			//used here, or should a resize occur repeatedly in
			//this loop instead?

			//Deinitialize the storage object instance

			sbi->deinit (&req, inst);
		}


		//Free the memory allocated for the storage object static
		//memory

		free (inst);


		//If an error occurred in the above test loop, propagate this
		//error. Note how this happens AFTER #inst is freed.

		if (error_detected)
		{
			break;
		}


		//Increment the test parameters

		int looped = 0;


		SK_inc_chain
		(
			&elem_size,
			1,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);

		SK_inc_chain
		(
			&num_stat_elems,
			looped,
			BEG_NUM_STAT_ELEMS,
			END_NUM_STAT_ELEMS,
			INC_NUM_STAT_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&elems_per_block,
			looped,
			BEG_ELEMS_PER_BLOCK,
			END_ELEMS_PER_BLOCK,
			INC_ELEMS_PER_BLOCK,
			&looped
		);

		SK_inc_chain
		(
			&neg_slack,
			looped,
			BEG_NEG_SLACK,
			END_NEG_SLACK,
			INC_NEG_SLACK,
			&looped
		);

		SK_inc_chain
		(
			&pos_slack,
			looped,
			BEG_POS_SLACK,
			END_POS_SLACK,
			INC_POS_SLACK,
			&looped
		);

		SK_inc_chain
		(
			&rand_ind,
			looped,
			BEG_RAND_IND,
			END_RAND_IND,
			INC_RAND_IND,
			&looped
		);

		if (looped)
		{
			break;
		}
	}


	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  TODO : Write a test-bench that ensures that the resize (), ins (), rem (),
 *  and related functions in #SK_BoxIntf work as expected, EVEN when memory
 *  allocations fail.
 *
 *  How can memory allocation failure be tested reliably? By making an
 *  #SK_MemIntf that can be toggled between operating normally and
 *  intentionally failing, and passing that as
 *  (SK_BoxReqs->SK_BoxStatReqs.mem).
 */




/*!
 *  TODO : Write a test-bench that checks if the *_w_anc () version of
 *  functions in an #SK_BoxIntf actually perform faster than the non-anchor
 *  versions across several realistic access patterns
 */




/*!
 *  This function is used inside of SK_BoxIntf_mt_single_tb () to simulate a
 *  singular thread that may be reading / writing elements to a storage object
 *  between the indices [beg_ind, end_ind].
 *
 *  Sub-tests used
 *
 *	None
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *	get_elem ()
 *	set_elem ()
 *	get_num_elems ()
 *
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param sbi			The #SK_BoxIntf to use
 *
 *  @param req			The #SK_BoxReqs associated with #inst
 *
 *  @param inst			Pointer to the storage object instance to
 *				perform the test-bench on
 *
 *  @param beg_ind		The starting index to access elements from.
 *
 *  				This MUST be lower than the number of elements
 *  				in #inst.
 *
 *  @param end_ind		The ending index to access elements from.
 *
 *  				This MUST be lower than the number of elements
 *  				in #inst.
 *
 *  @param elem_init		The function used to intiialize the elements in
 *				#inst. The values of the elements must be the
 *				same as what is reported by this function,
 *				otherwise this function will report an error.
 *
 *  @param arb_arg		The #arb_arg to send to each call of #elem_init
 *
 *  @param rd_flag		Indicates if this test-bench should READ
 *				elements from #inst
 *
 *  @param wr_flag		Indicates if this test-becnh should WRITE
 *				elements to #inst
 *
 *  @param rand_ind_flag	Determines whether the indices that #inst is
 *				read / written from should be randomly ordered
 *				between [beg_ind, end_ind], or should it
 *				increase by +1 increments and wrap-around.
 *
 *  @param stop_flag		When this becomes true, this function will stop
 *
 *
 *  @return			Standard status code
 */

SlySr SK_BoxIntf_mt_tb_single_rw_worker
(
	i64				p_depth,
	FILE *				file,
	SK_LineDeco			deco,

	const SK_BoxIntf *		sbi,
	SK_BoxReqs *			req,
	void *				inst,

	size_t				beg_ind,
	size_t				end_ind,

	SK_BoxIntf_elem_rw_func		elem_init,
	void *				arb_arg,

	int				rd_flag,
	int				wr_flag,

	int				rand_ind_flag,

	volatile int *			stop_flag
)
{

	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);


	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN_FP (print_flag, file, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN_FP (print_flag, file, file, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN_FP (print_flag, file, elem_init, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN_FP (print_flag, file, (void *) stop_flag, SlySr_get (SLY_BAD_ARG));


	//Check if #beg_ind and #end_ind are actually in range

	size_t num_elems = 0;

	sbi->get_num_elems (req, inst, &num_elems);


	if ((beg_ind >= num_elems) || (end_ind >= num_elems))
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"[beg_ind, end_ind] = [%zu, %zu] was not in range, "
			"num_elems = %zu",
			beg_ind,
			end_ind,
			num_elems
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	//Determine the OpenMP thread ID

	int thread_id = omp_get_thread_num ();


	//Print out some of the important parameters of this test

	if (print_flag)
	{
		SK_LineDeco_print_opt
		(
			file,
			deco,
			"thread_id =          %d\n",
			thread_id
		);

		SK_LineDeco_print_opt
		(
			file,
			deco,
			"[beg_ind, end_ind] = [%zu, %zu]\n",
			beg_ind,
			end_ind
		);

		SK_LineDeco_print_opt
		(
			file,
			deco,
			"rd_flag =            %d\n",
			rd_flag
		);

		SK_LineDeco_print_opt
		(
			file,
			deco,
			"wr_flag =            %d",
			wr_flag
		);
	}


	//Allocate some memory to store elements fetched from #inst

	void * elem_0 = malloc (req->stat_r->elem_size);

	void * elem_1 = malloc (req->stat_r->elem_size);


	if ((NULL == elem_0) || (NULL == elem_1))
	{
		free (elem_0);

		free (elem_1);


		SK_NULL_RETURN_FP (print_flag, file, elem_0, SlySr_get (SLY_BAD_ARG));

		SK_NULL_RETURN_FP (print_flag, file, elem_1, SlySr_get (SLY_BAD_ARG));
	}


	//Enter a loop where elements are read / written into #inst between the
	//indices #beg_ind

	size_t ind =			beg_ind;

	size_t error_detected =		0;


	while (0 == *stop_flag)
	{

		//Perform an OpenMP flush to make sure that #stop_flag is
		//detected as being different if it has updated since last time
		//it was checked

		#pragma omp flush


		//SK_LineDeco_print_opt
		//(
		//	file,
		//	deco,
		//	"thread_id = %d, ind = %zu",
		//	thread_id, ind
		//);


		if (rd_flag)
		{
			//Determine what the value of the element at #ind
			//should be, and then see if the value at #ind actually
			//matches it

			elem_init (req, elem_0, ind, arb_arg);

			sbi->get_elem (req, inst, ind, elem_1);


			int eq = 0;

			SK_byte_eq
			(
				elem_0,
				req->stat_r->elem_size,

				elem_1,
				req->stat_r->elem_size,

				&eq,
				NULL
			);


			if (0 == eq)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"#elem_0 and #elem_1 did not match."
				);


				//FIXME : Shouldn't these prints use
				//#file and #deco?


				fprintf (file, "elem_0 = 0x");

				SK_byte_print (file, elem_0, req->stat_r->elem_size);

				fprintf (file, "\n");


				fprintf (file, "elem_1 = 0x");

				SK_byte_print (file, elem_1, req->stat_r->elem_size);

				fprintf (file, "\n");


				error_detected = 1;

				break;
			}
		}


		if (wr_flag)
		{
			//Write a modified element to the current index, and if
			//reads are enabled, read that element back out to see
			//if it was modified properly

			sbi->get_elem (req, inst, ind, elem_0);

			for (size_t b_sel = 0; b_sel < req->stat_r->elem_size; b_sel += 1)
			{
				((u8 *) elem_0) [b_sel] =

				((u8 *) elem_0) [b_sel] + 1;
			}


			sbi->set_elem (req, inst, ind, elem_0);


			if (rd_flag)
			{
				sbi->get_elem (req, inst, ind, elem_1);


				int eq = 0;

				SK_byte_eq
				(
					elem_0,
					req->stat_r->elem_size,

					elem_1,
					req->stat_r->elem_size,

					&eq,
					NULL
				);


				if (0 == eq)
				{
					SK_DEBUG_PRINT_FP
					(
						print_flag,
						file,
						"#elem_0 and #elem_1 did not match."
					);


					//FIXME : Shouldn't these prints use
					//#file and #deco?


					fprintf (file, "elem_0 = 0x");

					SK_byte_print (file, elem_0, req->stat_r->elem_size);

					fprintf (file, "\n");


					fprintf (file, "elem_1 = 0x");

					SK_byte_print (file, elem_1, req->stat_r->elem_size);

					fprintf (file, "\n");


					error_detected = 1;

					break;
				}
			}


			//Restore the element to the value it should have been
			//initialized with.
			//
			//This is necessary for when the test-loop iterates
			//again

			elem_init (req, elem_0, ind, arb_arg);

			sbi->set_elem (req, inst, ind, elem_0);
		}


		//Update the element index.
		//
		//Note that SK_range_oflow_i64 is used to respect the range
		//[beg_ind, end_ind].

		ind += 1;


		if (rand_ind_flag)
		{
			ind = SK_gold_hash_u64 (ind);
		}


		ind =

		SK_range_oflow_i64
		(
			ind,
			beg_ind,
			end_ind
		);
	}


	//Free the memory used to store elements

	free (elem_0);

	free (elem_1);


	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Given an #SK_BoxIntf storage object instance, this performs a test-bench on
 *  the multi-threading properties of it. Namely, the multi-threading
 *  properties in '#SK_BoxIntf -> IMPORTANT_DESIGN_ASSUMPTIONS -> 3' are
 *  tested.
 *
 *
 *  Sub-tests used
 *
 *  	SK_BoxIntf_mt_tb_single_rw_worker ()
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *	get_num_elems ()
 *
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param sbi			The #SK_BoxIntf to use
 *
 *  @param req			The #SK_BoxReqs associated with #inst
 *
 *  @param inst			Pointer to the storage object instance to
 *				perform the test-bench on
 *
 *  @return			Standard status code
 */

//FIXME : Is the *_tb_single () name preferred, or is *_single_tb () preferred?

SlySr SK_BoxIntf_mt_tb_single
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi,
	SK_BoxReqs *		req,
	void *			inst
)
{

	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);

	i64 next_depth = SK_p_depth_next (p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, req, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, inst, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Create a further indented version of #deco to use for sub-tests

	SK_LineDeco ind_deco = deco;

	ind_deco.pre_str_cnt += 1;


	//Enter a test-loop that can be broken out of easily

	int error_detected = 0;

	do
	{
		//Initialize the element values inside of #inst to something
		//that is easy to verify

		size_t num_elems = 0;

		sbi->get_num_elems (req, inst, &num_elems);


		if (num_elems > 0)
		{
			sbi->rw_elems
			(
				req,
				inst,
				0,
				(num_elems - 1),
				SK_Box_elem_rw_set_ind,
				NULL
			);
		}

		else
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"#inst = %p contained %zu elements.",
				inst,
				num_elems
			)

			error_detected = 1;

			break;
		}


		//Create an OpenMP parallel region in which one thread will
		//start a timer, and the other threads will read from every
		//index inside of #inst.

		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"Executing multi-threaded read test...\n\n"
			);
		}


		int stop_flag = 0;

		#pragma omp parallel default (shared)

		{
			int thread_id =		omp_get_thread_num ();

			int num_threads =	omp_get_num_threads ();


			if (1 >= num_threads)
			{
				//This test requires more than 1 thread, so if
				//only 1 thread is being used, leave the
				//parallel region and report an error

				if (0 == thread_id)
				{
					SK_DEBUG_PRINT_FP
					(
						print_flag,
						file,
						"Only got %zu thread(s) for "
						"the OpenMP parallel region. "
						"This test needs more than 1 "
						"thread.",
						num_threads
					);

					error_detected = 1;
				}
			}

			else if (0 == thread_id)
			{
				//The 0th thread will be used set #stop_flag to
				//'1' at the end of a timer. This will stop the
				//other threads.

				//FIXME : I don't think that a timer is the
				//best technique to use here; I think that the
				//number of element accesses should be counted
				//instead and after some number of accesses,
				//then the workers should stop.


				struct timespec slp_ts =

				{
					.tv_sec =	0,
					.tv_nsec =	num_elems * 1000
				};


				struct timespec rem_ts =

				{
					.tv_sec =	0,
					.tv_nsec =	0
				};


				nanosleep (&slp_ts, &rem_ts);


				//Note, must use #OpenMP flushes here to ensure
				//that the update to #stop_flag is visible by
				//other threads
				//
				//FIXME : Is #stop_flag properly used in
				//general considering it is used by multiple
				//threads?

				#pragma omp flush

				stop_flag = 1;

				#pragma omp flush
			}

			else
			{
				//For all the other threads, turn them into
				//workers that continually read from #inst

				SlySr sr =

				SK_BoxIntf_mt_tb_single_rw_worker
				(
					next_depth,
					file,
					ind_deco,

					sbi,
					req,
					inst,

					0,
					SK_MAX ((num_elems - 1), 0),

					SK_Box_elem_rw_set_ind,
					NULL,

					1,
					0,

					1,

					&stop_flag
				);


				if (SLY_SUCCESS != sr.res)
				{
					SK_DEBUG_PRINT_FP
					(
						print_flag,
						file,
						"Worker for thread %d failed.",
						thread_id
					);

					error_detected = 1;
				}
			}
		}


		if (error_detected)
		{
			break;
		}


		//Again, create an OpenMP parallel region in which one thread
		//will start a timer, but now the other threads will read AND
		//write from index ranges reserved for them.

		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"Executing multi-threaded read / write test...\n\n"
			);
		}


		stop_flag = 0;

		#pragma omp parallel default (shared)

		{
			int thread_id =		omp_get_thread_num ();

			int num_threads =	omp_get_num_threads ();

			int worker_id =		thread_id - 1;

			int num_workers =	num_threads - 1;


			if (1 >= num_threads)
			{
				//This test requires more than 1 thread, so if
				//only 1 thread is being used, leave the
				//parallel region and report an error

				if (0 == thread_id)
				{
					SK_DEBUG_PRINT_FP
					(
						print_flag,
						file,
						"Only got %zu thread(s) for "
						"the OpenMP parallel region. "
						"This test needs more than 1 "
						"thread.",
						num_threads
					);

					error_detected = 1;
				}
			}

			else if (0 == thread_id)
			{
				//The 0th thread will be used set #stop_flag to
				//'1' at the end of a timer. This will stop the
				//other threads.

				struct timespec slp_ts =

				{
					.tv_sec =	0,
					.tv_nsec =	num_elems * 1000
				};


				struct timespec rem_ts =

				{
					.tv_sec =	0,
					.tv_nsec =	0
				};


				nanosleep (&slp_ts, &rem_ts);


				//Note, must use #OpenMP flushes here to ensure
				//that the update to #stop_flag is visible by
				//other threads
				//
				//FIXME : Is #stop_flag properly used in
				//general considering it is used by multiple
				//threads?

				#pragma omp flush

				stop_flag = 1;

				#pragma omp flush
			}

			else if (num_elems > worker_id)
			{
				//For all the other threads, turn them into
				//workers that continually read AND write from
				//index ranges that are reserved by them


				//Determine the index ranges that each worker
				//should control

				size_t ind_rng = 0;

				size_t beg_ind = 0;

				size_t end_ind = 0;


				if (num_workers < num_elems)
				{
					ind_rng = (num_elems / num_workers);

					beg_ind = worker_id * ind_rng;

					end_ind = ((worker_id + 1) * ind_rng) - 1;


					if ((num_threads - 1) == worker_id)
					{
						end_ind = num_elems - 1;
					}
				}

				else
				{
					ind_rng = 1;

					beg_ind = worker_id;

					end_ind = worker_id;
				}


				//Launch the worker

				SlySr sr =

				SK_BoxIntf_mt_tb_single_rw_worker
				(
					next_depth,
					file,
					ind_deco,

					sbi,
					req,
					inst,

					beg_ind,
					end_ind,

					SK_Box_elem_rw_set_ind,
					NULL,

					1,
					1,

					1,

					&stop_flag
				);


				if (SLY_SUCCESS != sr.res)
				{
					SK_DEBUG_PRINT_FP
					(
						print_flag,
						file,
						"Worker for thread %d failed.",
						thread_id
					);

					error_detected = 1;
				}
			}
		}


		if (error_detected)
		{
			break;
		}


	}
	while (0);


	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Given an #SK_BoxIntf, this performs a test-bench on the multi-threading
 *  properties of it. Namely, the multi-threading properties in '#SK_BoxIntf ->
 *  IMPORTANT_DESIGN_ASSUMPTIONS -> 3' are tested.
 *
 *
 *  Sub-tests used
 *
 *  	SK_BoxIntf_mt_tb_single_rw_worker ()
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *	deinit ()
 *	get_resize_limits ()
 *	init ()
 *	inst_stat_mem ()
 *
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param sbi			The #SK_BoxIntf to test
 *
 *  @return			Standard status code
 */

//FIXME : There is too much redundancy between this function and
//SK_BoxIntf_set_get_tb ()

SlySr SK_BoxIntf_mt_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
)
{

	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);

	i64 next_depth = SK_p_depth_next (p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Select constants that determine how many parameterizations to test.

	const i64 BEG_ELEM_SIZE = 		1;
	const i64 END_ELEM_SIZE = 		10;
	const i64 INC_ELEM_SIZE = 		1;

	const i64 BEG_NUM_STAT_ELEMS =		0;
	const i64 END_NUM_STAT_ELEMS =		4;
	const i64 INC_NUM_STAT_ELEMS =		1;

	const i64 BEG_ELEMS_PER_BLOCK =		0;
	const i64 END_ELEMS_PER_BLOCK =		4;
	const i64 INC_ELEMS_PER_BLOCK =		1;

	const i64 BEG_NEG_SLACK =		0;
	const i64 END_NEG_SLACK =		2;
	const i64 INC_NEG_SLACK =		1;

	const i64 BEG_POS_SLACK =		0;
	const i64 END_POS_SLACK =		2;
	const i64 INC_POS_SLACK =		1;


	i64 elem_size =				BEG_ELEM_SIZE;

	i64 num_stat_elems =			BEG_NUM_STAT_ELEMS;

	i64 elems_per_block =			BEG_ELEMS_PER_BLOCK;

	i64 neg_slack =				BEG_NEG_SLACK;

	i64 pos_slack =				BEG_POS_SLACK;


	//Enter the test-loop, in which storage objects with different
	//parameters will be initialized and deinitialized

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//Create an #SK_BoxReqs that describes the storage object that
		//is desired to be tested

		SK_BoxStatReqs srq =

		{
			.mem =				&SK_MemStd,

			.elem_size =			elem_size,

			.hint_num_stat_elems =		num_stat_elems,

			.hint_elems_per_block =		elems_per_block
		};


		SK_BoxDynaReqs drq =

		{
			.hint_neg_slack =			neg_slack,

			.hint_pos_slack =			pos_slack,

			.hint_use_max_num_dyna_blocks =		0,

			.hint_max_num_dyna_blocks =		0
		};


		SK_BoxStatImpl sim;

		SK_BoxDynaImpl dim;

		SK_BoxReqs req;


		SK_BoxReqs_prep (&req, &srq, &sim, &drq, &dim);


		SK_LineDeco_print_opt (file, deco, "The current #SK_BoxReqs is...\n\n");

		SK_BoxReqs_print (&req, 2, file, deco);

		SK_LineDeco_print_opt (file, deco, "");


		//Determine if there are limits to how many elements the
		//storage object described by #req can store

		int lim_exist = 0;

		size_t lim_min = 0;
		size_t lim_max = 0;

		sbi->get_resize_limits (&req, &lim_exist, &lim_min, &lim_max);


		SK_LineDeco_print_opt
		(
			file,
			deco,
			"lim_exist = %zu",
			lim_exist
		);

		SK_LineDeco_print_opt
		(
			file,
			deco,
			"lim_min =   %zu",
			lim_min
		);

		SK_LineDeco_print_opt
		(
			file,
			deco,
			"lim_max =   %zu",
			lim_max
		);


		SK_LineDeco_print_opt (file, deco, "");


		//Determine the range of elements to create storage objects
		//with

		const size_t RNG_NUM_ELEMS = 40;
		const size_t INC_NUM_ELEMS = 1;

		size_t beg_num_elems = 1;
		size_t end_num_elems = beg_num_elems + RNG_NUM_ELEMS - 1;

		if (lim_exist)
		{
			beg_num_elems =

			SK_MAX (beg_num_elems, lim_min);


			end_num_elems =

			SK_MIN ((beg_num_elems + RNG_NUM_ELEMS - 1), lim_max);
		}


		SK_LineDeco_print_opt
		(
			file,
			deco,
			"beg_num_elems =  %zu",
			beg_num_elems
		);

		SK_LineDeco_print_opt
		(
			file,
			deco,
			"end_num_elems =  %zu",
			end_num_elems
		);


		SK_LineDeco_print_opt (file, deco, "");


		//Determine the amount of static memory that #req calls for

		size_t inst_stat_mem = 0;

		sbi->inst_stat_mem (&req, &inst_stat_mem);


		SK_LineDeco_print_opt
		(
			file,
			deco,
			"inst_stat_mem = %zu",
			inst_stat_mem
		);


		SK_LineDeco_print_opt (file, deco, "");


		//Allocate the static memory for the storage object instance

		void * inst = malloc (inst_stat_mem);


		if (NULL == inst)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Could not allocate #inst, #inst_stat_mem = %zu",
				inst_stat_mem
			);

			error_detected = 1;

			break;
		}


		//Enter a loop in which storage objects with #num_elems number
		//of elements are created and then sent to
		//SK_BoxIntf_set_get_single_tb () for testing

		for
		(
			size_t num_elems = beg_num_elems;

			num_elems <= end_num_elems;

			num_elems += INC_NUM_ELEMS
		)
		{

			//Initialize #inst to hold #num_elems

			SlySr sr =

			sbi->init
			(
				&req,
				inst,
				num_elems,
				SK_Box_elem_rw_set_zero,
				NULL
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Could not initialize #inst"
				);

				error_detected = 1;

				break;
			}


			//Send #inst to SK_BoxIntf_mt_tb_single () for testing

			SK_LineDeco ind_deco = deco;

			ind_deco.pre_str_cnt += 1;


			sr =

			SK_BoxIntf_mt_tb_single
			(
				next_depth,
				file,
				ind_deco,

				sbi,
				&req,
				inst
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Inner test-bench failed!"
				);

				error_detected = 1;

				break;
			}


			//Deinitialize the storage object instance

			sbi->deinit (&req, inst);

		}


		//Free the memory allocated for the storage object static
		//memory

		free (inst);


		//If an error occurred in the above test loop, propagate this
		//error. Note how this happens AFTER #inst is freed.

		if (error_detected)
		{
			break;
		}


		//Increment the test parameters

		int looped = 0;


		SK_inc_chain
		(
			&elem_size,
			1,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);

		SK_inc_chain
		(
			&num_stat_elems,
			looped,
			BEG_NUM_STAT_ELEMS,
			END_NUM_STAT_ELEMS,
			INC_NUM_STAT_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&elems_per_block,
			looped,
			BEG_ELEMS_PER_BLOCK,
			END_ELEMS_PER_BLOCK,
			INC_ELEMS_PER_BLOCK,
			&looped
		);

		SK_inc_chain
		(
			&neg_slack,
			looped,
			BEG_NEG_SLACK,
			END_NEG_SLACK,
			INC_NEG_SLACK,
			&looped
		);

		SK_inc_chain
		(
			&pos_slack,
			looped,
			BEG_POS_SLACK,
			END_POS_SLACK,
			INC_POS_SLACK,
			&looped
		);

		if (looped)
		{
			break;
		}
	}




	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Given an #SK_BoxIntf and an #SK_BoxReqs, performs a test-bench on the
 *  functions that change the number of elements contained in an #SK_Box.
 *
 *  This is intended to be used inside of SK_BoxIntf_ins_rem_tb ().
 *
 *
 *  Sub-tests used
 *
 *  	None
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *	anc_size ()
 *	anc_support ()
 * 	deinit ()
 * 	get_anc ()
 * 	get_num_elems ()
 *  	get_resize_limits ()
 *	init ()
 *	inst_stat_mem ()
 *	ins ()
 *	ins_beg ()
 *	ins_end ()
 *	ins_range ()
 *	ins_range_unsafe ()
 *	ins_w_anc ()
 *	ins_beg_w_anc ()
 *	ins_end_w_anc ()
 *	ins_range_w_anc ()
 *	ins_range_unsafe_w_anc ()
 *	ins_bef_by_anc ()
 *	ins_aft_by_anc ()
 *	rem ()
 *	rem_beg ()
 *	rem_end ()
 *	rem_range ()
 *	rem_w_anc ()
 *	rem_beg_w_anc ()
 *	rem_end_w_anc ()
 *	rem_range_w_anc ()
 *	rem_by_anc ()
 *	resize ()
 *	resize_unsafe ()
 *
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param sbi			The #SK_BoxIntf to test
 *
 *  @param req			The #SK_BoxReqs that describes the desired
 *  				parameters of the storage objects to test
 *
 *  @param box			The initialized #SK_Box instance to resize
 *				multiple times
 *
 *  @param num_resizes		The number of times to add / remove elements
 *				from a storage object being tested
 *
 *  @param min_num_elems	The minimum number of elements that #box should
 *				be resized to at any time during this test
 *
 *  @param max_num_elems	The maximum number of elements that #box should
 *				be resized to at any time during this test
 *
 *  @return			Standard status code
 */

SlySr SK_BoxIntf_ins_rem_single_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi,
	const SK_BoxReqs *	req,
	SK_Box *		box,

	i64			num_resizes,

	size_t			min_num_elems,
	size_t			max_num_elems
)
{
	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, req, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, box, SlySr_get (SLY_BAD_ARG));


	if (0 >= num_resizes)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"#num_resizes = %zu, test-bench skipped.",
			num_resizes
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	if (min_num_elems > max_num_elems)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"(min_num_elems > max_num_elems) = (%zu > %zu), "
			"logically can not perform any resize operations.",
			min_num_elems,
			max_num_elems
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	if (0 >= max_num_elems)
	{
		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"#max_num_elems = %zu, no need to test any "
				"insertions / removals.",
				max_num_elems
			);
		}

		return SlySr_get (SLY_SUCCESS);
	}


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//These will be used when randomly selecting a resize operation to
	//perform

	const int SEL_INS =			0;
	const int SEL_INS_BEG =			1;
	const int SEL_INS_END =			2;
	const int SEL_INS_RANGE =		3;
	const int SEL_INS_RANGE_UNSAFE =	4;

	const int SEL_REM =			5;
	const int SEL_REM_BEG =			6;
	const int SEL_REM_END =			7;
	const int SEL_REM_RANGE =		8;

	const int SEL_RESIZE =			9;
	const int SEL_RESIZE_UNSAFE =		10;

	const int SEL_INS_W_ANC =		11;
	const int SEL_INS_BEG_W_ANC =		12;
	const int SEL_INS_END_W_ANC =		13;
	const int SEL_INS_RANGE_W_ANC =		14;
	const int SEL_INS_RANGE_UNSAFE_W_ANC =	15;

	const int SEL_REM_W_ANC =		16;
	const int SEL_REM_BEG_W_ANC =		17;
	const int SEL_REM_END_W_ANC =		18;
	const int SEL_REM_RANGE_W_ANC =		19;

	const int SEL_INS_BEF_BY_ANC =		20;
	const int SEL_INS_AFT_BY_ANC =		21;

	const int SEL_REM_BY_ANC =		22;

	const int SEL_MAX =			SEL_REM_BY_ANC;


	int sel_operation =			0;

	i64 resize_count =			0;


	//Determine if #SK_MemDebug was used for (req->stat_r->mem), which
	//would enable additional testing by this test-bench.

	int SK_MemDebug_used = (&SK_MemDebug == req->stat_r->mem);

	int old_SK_MemDebug_break_allocators = SK_MemDebug_break_allocators;


	//Allocate memory to temporarily store elements that will be used to
	//set / get values inside of #box

	size_t elem_size = req->stat_r->elem_size;

	void * elem_0 = malloc (elem_size);
	void * elem_1 = malloc (elem_size);


	if ((NULL == elem_0) || (NULL == elem_1))
	{
		free (elem_0);
		free (elem_1);

		SK_NULL_RETURN_FP
		(
			print_flag,
			file,
			elem_0,
			SlySr_get (SLY_BAD_ALLOC)
		);

		SK_NULL_RETURN_FP
		(
			print_flag,
			file,
			elem_1,
			SlySr_get (SLY_BAD_ALLOC)
		);
	}


	for (size_t b_sel = 0; b_sel < elem_size; b_sel += 1)
	{
		* ((u8 *) elem_0) = 0x55;
		* ((u8 *) elem_1) = 0xAA;
	}


	//If anchors are necessary to use in this test-bench, allocate some
	//here

	int anc_support = 0;

	sbi->anc_support (req, &anc_support);


	size_t anc_size = 0;


	SK_BoxAnc * anc_0 = NULL;
	SK_BoxAnc * anc_1 = NULL;


	if (anc_support)
	{
		sbi->anc_size (req, &anc_size);


		anc_0 = malloc (anc_size);
		anc_1 = malloc (anc_size);


		if ((NULL == anc_0) || (NULL == anc_1))
		{
			free (elem_0);
			free (elem_1);

			free (anc_0);
			free (anc_1);


			SK_NULL_RETURN_FP
			(
				print_flag,
				file,
				anc_0,
				SlySr_get (SLY_BAD_ALLOC)
			);

			SK_NULL_RETURN_FP
			(
				print_flag,
				file,
				anc_1,
				SlySr_get (SLY_BAD_ALLOC)
			);
		}
	}


	//Enter the test-loop, in which #SK_Box's will repeatedly have elements
	//inserted and removed

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//Create some hash outputs to use later for randomly selecting
		//insertion / removal indices.
		//
		//TODO : The current way hash values are calculated works, but
		//should it be replaced with a short for-loop for elegance?

		u64 hash_in_0 =

		10000 *

		(
			req->stat_r->elem_size			+
			req->stat_r->hint_num_stat_elems	+
			req->stat_r->hint_elems_per_block	+
			req->dyna_i->r.hint_neg_slack		+
			req->dyna_i->r.hint_pos_slack
		)

		+

		resize_count;


		u64 hash_out_0 = SK_gold_hash_u64 (hash_in_0);
		u64 hash_out_1 = SK_gold_hash_u64 (hash_out_0);
		u64 hash_out_2 = SK_gold_hash_u64 (hash_out_1);
		u64 hash_out_3 = SK_gold_hash_u64 (hash_out_2);
		u64 hash_out_4 = SK_gold_hash_u64 (hash_out_3);


		//Randomly select a resize operation to perform on #box

		sel_operation = hash_out_0 % (SEL_MAX + 1);


		//These are used to keep track of what index range in #box has
		//been modified, and in what way.

		const char * used_func =	"(skipped)";

		size_t old_num_elems =		0;
		size_t new_num_elems =		0;

		size_t touch_beg =		0;
		size_t touch_end =		0;

		size_t num_touch =		0;

		int touched_init =		1;

		int never_fail =		0;


		sbi->get_num_elems (req, box, &old_num_elems);

		SlySr sr = SlySr_get (SLY_SUCCESS);


		//If necessary, before performing the resize operation, reset
		//the anchors and then randomly select an element to associate
		//with #anc_0.
		//
		//TODO : Perhaps this test-bench can be improved by only
		//fetching #anc_0 when resize operation that needs it is
		//selected, though that might make this test-bench less
		//elegant.

		//Note, the reason the #stub_anc_* values are used is because
		//#box may have 0 elements inside of it during a test
		//iteration, and therefore having a valid anchor at that time
		//would be imposssible. Therefore, (NULL) should be passed as
		//the anchors to the *_w_anc () versions of the functions in
		//that event (to disable the anchor arguments), but this must
		//be done in such a way where (anc_0, anc_1) remain pointing
		//to their allocated memory space (so that they can be
		//deallocated properly later).

		SK_BoxAnc * stub_anc_0 =	NULL;
		SK_BoxAnc * stub_anc_1 =	NULL;

		int anc_0_used =		0;
		int anc_1_used =		0;

		size_t anc_0_ind =		0;
		size_t anc_1_ind =		0;


		if ((anc_support) && (old_num_elems > 0))
		{
			stub_anc_0 =	anc_0;
			stub_anc_1 =	anc_1;


			anc_0_ind =	hash_out_3 % old_num_elems;


			sbi->get_anc (req, box, anc_0_ind, anc_0);
		}


		//Before performing the selected resize operation, set all of
		//the elements in #box equal to their index. This facilitates
		//checking if the correct index ranges are modified by the
		//upcoming resize operation.

		size_t old_max_ind =

		(old_num_elems > 0) ? (old_num_elems - 1) : 0;


		if (0 < old_num_elems)
		{
			sbi->rw_elems
			(
				req,
				box,
				0,
				old_max_ind,
				SK_Box_elem_rw_set_ind,
				NULL
			);
		}


		//If #SK_MemDebug is used for (req->stat_r->mem), then take
		//advantage of the fact that allocators can be intentionally
		//disabled for debugging purposes.

		int mem_disabled = 0;


		if (SK_MemDebug_used)
		{
			mem_disabled = (0 == (resize_count % 32));

			SK_MemDebug_break_allocators = mem_disabled;


			if (mem_disabled && print_flag)
			{
				SK_LineDeco_print_opt
				(
					file,
					deco,
					"(req->stat_r->mem) has been "
					"intentionally disabled in this test. "
					"Debugging messages from the resize "
					"operation may appear."
				);

				SK_LineDeco_print_opt (file, deco, "");
			}
		}


		//Determine what index range will be affected by the selected
		//resize operation, and then actually perform the resize
		//operation.
		//
		//Note that the different resize operations have different
		//calling conventions, hence the following if-else structure.

		if (SEL_INS == sel_operation)
		{
			if (old_num_elems < max_num_elems)
			{
				used_func =	"ins";

				new_num_elems =	(old_num_elems + 1);

				touch_beg =	hash_out_1 % (old_num_elems + 1);
				touch_end =	touch_beg;

				num_touch =	1;


				sr = sbi->ins (req, box, touch_beg, elem_0);
			}
		}

		else if (SEL_INS_BEG == sel_operation)
		{
			if (old_num_elems < max_num_elems)
			{
				used_func =	"ins_beg";

				new_num_elems = (old_num_elems + 1);

				touch_beg =	0;
				touch_end =	touch_beg;

				num_touch =	1;


				sr = sbi->ins_beg (req, box, elem_0);
			}
		}

		else if (SEL_INS_END == sel_operation)
		{
			if (old_num_elems < max_num_elems)
			{
				used_func =	"ins_end";

				new_num_elems =	(old_num_elems + 1);


				touch_beg =	old_num_elems;
				touch_end =	touch_beg;


				num_touch =	1;


				sr = sbi->ins_end (req, box, elem_0);
			}
		}

		else if (SEL_INS_RANGE == sel_operation)
		{
			used_func =	"ins_range";

			num_touch =

			SK_range_oflow_i64
			(
				hash_out_2,
				0,
				max_num_elems - old_num_elems
			);


			new_num_elems = old_num_elems + num_touch;


			touch_beg = hash_out_1 % (old_num_elems + 1);
			touch_end = touch_beg + ((num_touch > 0) ? (num_touch - 1) : 0);


			sr =

			sbi->ins_range
			(
				req,
				box,
				num_touch,
				touch_beg,
				SK_Box_elem_rw_set_w_arb_arg,
				elem_0
			);
		}

		else if (SEL_INS_RANGE_UNSAFE == sel_operation)
		{
			used_func =	"ins_range_unsafe";

			num_touch =

			SK_range_oflow_i64
			(
				hash_out_2,
				0,
				max_num_elems - old_num_elems
			);


			new_num_elems = old_num_elems + num_touch;


			touch_beg = hash_out_1 % (old_num_elems + 1);
			touch_end = touch_beg + ((num_touch > 0) ? (num_touch - 1) : 0);


			touched_init = 0;


			sr =

			sbi->ins_range_unsafe
			(
				req,
				box,
				num_touch,
				touch_beg
			);
		}

		else if (SEL_REM == sel_operation)
		{
			if (0 < old_num_elems)
			{
				used_func =	"rem";

				new_num_elems =	old_num_elems - 1;

				touch_beg =	hash_out_1 % old_num_elems;
				touch_end =	touch_beg;

				num_touch =	1;

				never_fail =	1;


				SlyDr dr =

				sbi->rem (req, box, touch_beg, elem_0);


				sr = SlySr_get (dr.res);
			}
		}

		else if (SEL_REM_BEG == sel_operation)
		{
			if (0 < old_num_elems)
			{
				used_func =	"rem_beg";

				new_num_elems =	old_num_elems - 1;

				touch_beg =	0;
				touch_end =	touch_beg;

				num_touch =	1;

				never_fail =	1;


				SlyDr dr =

				sbi->rem_beg (req, box, elem_0);


				sr = SlySr_get (dr.res);
			}
		}

		else if (SEL_REM_END == sel_operation)
		{
			if (0 < old_num_elems)
			{
				used_func =	"rem_end";

				new_num_elems =	old_num_elems - 1;

				touch_beg =	old_num_elems - 1;;
				touch_end =	touch_beg;

				num_touch =	1;

				never_fail =	1;


				SlyDr dr =

				sbi->rem_end (req, box, elem_0);


				sr = SlySr_get (dr.res);
			}
		}

		else if (SEL_REM_RANGE == sel_operation)
		{
			used_func =	"rem_range";


			num_touch =

			SK_range_oflow_i64
			(
				hash_out_2,
				0,
				(old_num_elems - min_num_elems)
			);


			new_num_elems =	old_num_elems - num_touch;


			touch_beg =

			SK_range_oflow_i64
			(
				hash_out_1,
				0,
				old_num_elems - num_touch
			);


			touch_end = touch_beg + ((num_touch > 0) ? (num_touch - 1) : 0);


			never_fail =	1;


			SlyDr dr =

			sbi->rem_range
			(
				req,
				box,
				num_touch,
				touch_beg
			);


			sr = SlySr_get (dr.res);
		}

		else if (SEL_RESIZE == sel_operation)
		{
			used_func =	"resize";


			new_num_elems =

			SK_range_oflow_i64
			(
				hash_out_2,
				min_num_elems,
				max_num_elems
			);


			num_touch =

			(old_num_elems > new_num_elems) ?
			(old_num_elems - new_num_elems) :
			(new_num_elems - old_num_elems);


			touch_beg =

			(old_num_elems > new_num_elems) ?
			new_num_elems :
			old_num_elems;


			touch_end =

			(old_num_elems > new_num_elems) ?
			((old_num_elems > 0) ? (old_num_elems - 1) : 0) :
			((new_num_elems > 0) ? (new_num_elems - 1) : 0);


			sr =

			sbi->resize
			(
				req,
				box,
				new_num_elems,
				SK_Box_elem_rw_set_w_arb_arg,
				elem_0
			);
		}

		else if (SEL_RESIZE_UNSAFE == sel_operation)
		{
			used_func =	"resize_unsafe";


			new_num_elems =

			SK_range_oflow_i64
			(
				hash_out_2,
				min_num_elems,
				max_num_elems
			);


			num_touch =

			(old_num_elems > new_num_elems) ?
			(old_num_elems - new_num_elems) :
			(new_num_elems - old_num_elems);


			touch_beg =

			(old_num_elems > new_num_elems) ?
			new_num_elems :
			old_num_elems;


			touch_end =

			(old_num_elems > new_num_elems) ?
			((old_num_elems > 0) ? (old_num_elems - 1) : 0) :
			((new_num_elems > 0) ? (new_num_elems - 1) : 0);


			touched_init = 0;


			sr =

			sbi->resize_unsafe
			(
				req,
				box,
				new_num_elems
			);
		}

		else if (SEL_INS_W_ANC == sel_operation)
		{
			if (old_num_elems < max_num_elems)
			{
				used_func =	"ins_w_anc";


				new_num_elems = (old_num_elems + 1);

				touch_beg =	hash_out_1 % (old_num_elems + 1);
				touch_end =	touch_beg;

				num_touch =	1;

				anc_0_used =	1;
				anc_1_used =	1;

				anc_1_ind =	anc_0_ind;


				sr =

				sbi->ins_w_anc
				(
					req,
					box,
					touch_beg,
					stub_anc_0,
					stub_anc_1,
					elem_0
				);
			}
		}

		else if (SEL_INS_BEG_W_ANC == sel_operation)
		{
			if (old_num_elems < max_num_elems)
			{
				used_func =	"ins_beg_w_anc";


				new_num_elems =	(old_num_elems + 1);

				touch_beg =	0;
				touch_end =	touch_beg;

				num_touch =	1;

				anc_0_used =	1;
				anc_1_used =	1;

				anc_1_ind =	anc_0_ind;


				sr =

				sbi->ins_beg_w_anc
				(
					req,
					box,
					stub_anc_0,
					stub_anc_1,
					elem_0
				);
			}
		}

		else if (SEL_INS_END_W_ANC == sel_operation)
		{
			if (old_num_elems < max_num_elems)
			{
				used_func =	"ins_end_w_anc";


				new_num_elems = (old_num_elems + 1);

				touch_beg =	old_num_elems;
				touch_end =	touch_beg;

				num_touch =	1;

				anc_0_used =	1;
				anc_1_used =	1;

				anc_1_ind =	anc_0_ind;


				sr =

				sbi->ins_end_w_anc
				(
					req,
					box,
					stub_anc_0,
					stub_anc_1,
					elem_0
				);
			}
		}

		else if (SEL_INS_RANGE_W_ANC == sel_operation)
		{
			used_func =	"ins_range_w_anc";


			num_touch =

			SK_range_oflow_i64
			(
				hash_out_2,
				0,
				max_num_elems - old_num_elems
			);


			new_num_elems = old_num_elems + num_touch;


			touch_beg = hash_out_1 % (old_num_elems + 1);
			touch_end = touch_beg + ((num_touch > 0) ? (num_touch - 1) : 0);


			anc_0_used =	1;
			anc_1_used =	1;

			anc_1_ind =	hash_out_4 % (new_num_elems > 0 ? new_num_elems : 1);


			sr =

			sbi->ins_range_w_anc
			(
				req,
				box,
				num_touch,
				touch_beg,
				anc_1_ind,
				stub_anc_0,
				stub_anc_1,
				SK_Box_elem_rw_set_w_arb_arg,
				elem_0
			);
		}

		else if (SEL_INS_RANGE_UNSAFE_W_ANC == sel_operation)
		{
			used_func =	"ins_range_unsafe_w_anc";


			num_touch =

			SK_range_oflow_i64
			(
				hash_out_2,
				0,
				max_num_elems - old_num_elems
			);


			new_num_elems = old_num_elems + num_touch;


			touch_beg = hash_out_1 % (old_num_elems + 1);
			touch_end = touch_beg + ((num_touch > 0) ? (num_touch - 1) : 0);


			touched_init = 0;


			anc_0_used =	1;
			anc_1_used =	1;

			anc_1_ind =	hash_out_4 % (new_num_elems > 0 ? new_num_elems : 1);


			sr =

			sbi->ins_range_unsafe_w_anc
			(
				req,
				box,
				num_touch,
				touch_beg,
				anc_1_ind,
				stub_anc_0,
				stub_anc_1
			);
		}

		else if (SEL_REM_W_ANC == sel_operation)
		{
			if (0 < old_num_elems)
			{
				used_func =	"rem_w_anc";

				new_num_elems =	old_num_elems - 1;

				touch_beg =	hash_out_1 % old_num_elems;
				touch_end =	touch_beg;

				num_touch =	1;

				anc_0_used =	1;
				anc_1_used =	1;

				anc_1_ind =	anc_0_ind;

				never_fail =	1;


				SlyDr dr =

				sbi->rem_w_anc
				(
					req,
					box,
					touch_beg,
					stub_anc_0,
					stub_anc_1,
					elem_0
				);


				sr = SlySr_get (dr.res);
			}
		}

		else if (SEL_REM_BEG_W_ANC == sel_operation)
		{
			if (0 < old_num_elems)
			{
				used_func =	"rem_beg_w_anc";

				new_num_elems =	old_num_elems - 1;

				touch_beg =	0;
				touch_end =	touch_beg;

				num_touch =	1;

				anc_0_used =	1;
				anc_1_used =	1;

				anc_1_ind =	anc_0_ind;

				never_fail =	1;


				SlyDr dr =

				sbi->rem_beg_w_anc
				(
					req,
					box,
					stub_anc_0,
					stub_anc_1,
					elem_0
				);


				sr = SlySr_get (dr.res);
			}
		}

		else if (SEL_REM_END_W_ANC == sel_operation)
		{
			if (0 < old_num_elems)
			{
				used_func =	"rem_end_w_anc";

				new_num_elems =	old_num_elems - 1;

				touch_beg =	old_num_elems - 1;
				touch_end =	touch_beg;

				num_touch =	1;

				anc_0_used =	1;
				anc_1_used =	1;

				anc_1_ind =	anc_0_ind;

				never_fail =	1;


				SlyDr dr =

				sbi->rem_end_w_anc
				(
					req,
					box,
					stub_anc_0,
					stub_anc_1,
					elem_0
				);


				sr = SlySr_get (dr.res);
			}
		}

		else if (SEL_REM_RANGE_W_ANC == sel_operation)
		{
			used_func =	"rem_w_anc";


			num_touch =

			SK_range_oflow_i64
			(
				hash_out_2,
				0,
				old_num_elems - min_num_elems
			);


			new_num_elems = old_num_elems - num_touch;


			touch_beg =

			SK_range_oflow_i64
			(
				hash_out_1,
				0,
				old_num_elems - num_touch
			);


			touch_end =	touch_beg + ((num_touch > 0) ? (num_touch - 1) : 0);

			anc_0_used =	1;
			anc_1_used =	1;


			anc_1_ind =

			SK_range_oflow_i64
			(
				hash_out_4,
				0,
				new_num_elems
			);


			never_fail =	1;


			SlyDr dr =

			sbi->rem_range_w_anc
			(
				req,
				box,
				num_touch,
				touch_beg,
				anc_1_ind,
				stub_anc_0,
				stub_anc_1
			);


			sr = SlySr_get (dr.res);
		}

		else if (SEL_INS_BEF_BY_ANC == sel_operation)
		{
			if
			(
				anc_support			&&
				(old_num_elems > 0)		&&
				(old_num_elems < max_num_elems)
			)
			{
				used_func =	"ins_bef_by_anc";


				new_num_elems =	old_num_elems + 1;

				touch_beg =	anc_0_ind;
				touch_end =	touch_beg;

				num_touch =	1;

				anc_0_used = 	1;


				sr =

				sbi->ins_bef_by_anc
				(
					req,
					box,
					stub_anc_0,
					elem_0
				);
			}
		}

		else if (SEL_INS_AFT_BY_ANC == sel_operation)
		{
			if
			(
				anc_support			&&
				(old_num_elems > 0)		&&
				(old_num_elems < max_num_elems)
			)
			{
				used_func =	"ins_aft_by_anc";


				new_num_elems =	old_num_elems + 1;

				touch_beg =	anc_0_ind + 1;
				touch_end =	touch_beg;

				num_touch =	1;

				anc_0_used = 	1;


				sr =

				sbi->ins_aft_by_anc
				(
					req,
					box,
					stub_anc_0,
					elem_0
				);
			}
		}

		else if (SEL_REM_BY_ANC == sel_operation)
		{
			if (anc_support && (old_num_elems > 0))
			{
				used_func =	"rem_by_anc";


				new_num_elems =	old_num_elems - 1;

				touch_beg =	anc_0_ind;
				touch_end =	touch_beg;

				num_touch =	1;

				anc_0_used =	1;

				never_fail =	1;


				SlyDr dr =

				sbi->rem_by_anc
				(
					req,
					box,
					stub_anc_0,
					elem_0
				);


				sr = SlySr_get (dr.res);
			}
		}

		else
		{
			//This case should never be reached

			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"sel_operation = %zu, which should be "
				"impossible.",
				sel_operation
			);

			error_detected = 1;

			break;
		}


		//Print out the selected resize operation that was just
		//performed as well as the intended index range it was meant to
		//modify

		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"Used : %s ()",
				used_func
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"mem_disabled =  %d",
				mem_disabled
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"never_fail =    %d",
				never_fail
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"old_num_elems = %zu",
				old_num_elems
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"new_num_elems = %zu",
				new_num_elems
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"touch_beg =     %zu",
				touch_beg
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"touch_end =     %zu",
				touch_end
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"num_touch =     %zu",
				num_touch
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"touched_init =  %d",
				touched_init
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"anc_0_used =    %d",
				anc_0_used
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"anc_1_used =    %d",
				anc_1_used
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"anc_0_ind =     %zu",
				anc_0_ind
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"anc_1_ind =     %zu",
				anc_1_ind
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				""
			);
		}


		//Check if the resize operation reported failure at an
		//appropriate time.
		//
		//Note that the resize operation may have failed due to this
		//test-bench disabling the memory allocators in
		//(req->stat_r->mem), in which case it is acceptable if the
		//resize operation failed. This is done to ensure that the
		//resize operation for #box leaves the elements properly intact
		//after a failure.

		if ((0 == mem_disabled) || (never_fail))
		{
			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					print_flag,
					file,
					"Resize operation reported failure, "
					"(sr.res = %s)",
					SlySrStr_get (sr).str
				);

				error_detected = 1;

				break;
			}
		}

		else
		{
			if (SLY_SUCCESS == sr.res)
			{
				if (print_flag)
				{
					SK_LineDeco_print_opt
					(
						file,
						deco,
						"Resize operation reported "
						"success despite "
						"(req->stat_r->mem) being "
						"disabled. This may be valid "
						"due to no new memory "
						"allocations being necessary. "
						"(Ex : Made use of previously "
						"allocated slack, made use of "
						"statically allocated "
						"elements.)"
					);

					SK_LineDeco_print_opt (file, deco, "");
				}
			}

			else
			{
				if (print_flag)
				{
					SK_LineDeco_print_opt
					(
						file,
						deco,
						"(req->stat_r->mem) was "
						"intentionally disabled, and "
						"the resize operation "
						"reported failure. This is "
						"acceptable, provided that "
						"the elements in #box have "
						"been left intact. This will "
						"now be checked."
					);

					SK_LineDeco_print_opt (file, deco, "");
				}


				//Indicate that the resize operation should
				//have not effected #box

				new_num_elems =	old_num_elems;

				touch_beg =	0;
				touch_end =	0;

				num_touch =	0;

				touched_init =	0;
			}
		}


		//This is an internal sanity check for this test-bench; this
		//ensures that a resize operation that would cause the number
		//of elements in #box to exceed #max_num_elems was not
		//requested at any time.

		if
		(
			(old_num_elems > max_num_elems)

			||

			(new_num_elems > max_num_elems)
		)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Test-bench internal sanity check failed. "
				"(old_num_elems, new_num_elems) = (%zu, %zu) "
				"contains a value greater than "
				"(max_num_elems = %zu)",
				old_num_elems,
				new_num_elems,
				max_num_elems
			);

			error_detected = 1;

			break;
		}


		//Check if the expected number of elements is actually recorded
		//in #box.
		//
		//Note, this only needs to be checked in the event that the
		//resize operation was not skipped, hence the check for
		//#num_touch being non-zero.

		size_t reported_num_elems = 0;

		sbi->get_num_elems (req, box, &reported_num_elems);


		if ((0 < num_touch) && (new_num_elems != reported_num_elems))
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"The reported number of elems in #box did not "
				"match what the resize operation should have "
				"produced. "
				"(new_num_elems, reported_num_elems) = "
				"(%zu, %zu)",
				new_num_elems,
				reported_num_elems
			);

			error_detected = 1;

			break;
		}


		//TODO : Add some sort of validation to #anc_0 and #anc_1 here
		//to ensure that they are in a valid state after the above
		//resize operation completed (if anchors were in fact used.)


		//Check if the elements in #box are correct following the
		//resize operation, which is dependent on whether the operation
		//inserted elements or removed them

		size_t ind_compare_len =

		SK_MIN (sizeof (size_t), req->stat_r->elem_size);


		size_t max_ind =

		(new_num_elems > 0) ? (new_num_elems - 1) : 0;


		if (new_num_elems <= old_num_elems)
		{
			//The resize operation removed elements from #box

			size_t cur_ind = 0;


			//Check the index range BELOW the removed elements

			while (cur_ind < touch_beg)
			{
				sbi->get_elem
				(
					req,
					box,
					cur_ind,
					elem_1
				);


				int eq = 0;

				SK_byte_eq
				(
					&cur_ind,
					ind_compare_len,

					elem_1,
					ind_compare_len,

					&eq,
					NULL
				);

				if (0 == eq)
				{
					SK_DEBUG_PRINT_FP
					(
						print_flag,
						file,
						"Element at index %zu was "
						"incorrect, expected value of "
						"%zu",
						cur_ind,
						cur_ind
					);


					if (print_flag)
					{
						SK_Box_bytes_print
						(
							sbi,
							req,
							box,
							file,
							deco,
							1,
							0,
							max_ind
						);

						SK_LineDeco_print_opt
						(
							file,
							deco,
							""
						);
					}


					error_detected = 1;

					break;
				}


				cur_ind += 1;
			}

			if (error_detected)
			{
				break;
			}


			//Check the index range ABOVE the removed elements

			while (cur_ind < new_num_elems)
			{
				sbi->get_elem
				(
					req,
					box,
					cur_ind,
					elem_1
				);


				size_t calc_ind =

				cur_ind + num_touch;


				int eq = 0;

				SK_byte_eq
				(
					&calc_ind,
					ind_compare_len,

					elem_1,
					ind_compare_len,

					&eq,
					NULL
				);

				if (0 == eq)
				{
					SK_DEBUG_PRINT_FP
					(
						print_flag,
						file,
						"Element at index %zu was "
						"incorrect, expected value of "
						"%zu",
						cur_ind,
						calc_ind
					);


					if (print_flag)
					{
						SK_Box_bytes_print
						(
							sbi,
							req,
							box,
							file,
							deco,
							1,
							0,
							max_ind
						);

						SK_LineDeco_print_opt
						(
							file,
							deco,
							""
						);
					}


					error_detected = 1;

					break;
				}


				cur_ind += 1;
			}

			if (error_detected)
			{
				break;
			}
		}


		if (new_num_elems > old_num_elems)
		{
			//The resize operation inserted elements into #box

			size_t cur_ind = 0;


			//Check the index range BELOW the inserted elements

			while (cur_ind < touch_beg)
			{
				sbi->get_elem
				(
					req,
					box,
					cur_ind,
					elem_1
				);


				int eq = 0;

				SK_byte_eq
				(
					&cur_ind,
					ind_compare_len,

					elem_1,
					ind_compare_len,

					&eq,
					NULL
				);

				if (0 == eq)
				{
					SK_DEBUG_PRINT_FP
					(
						print_flag,
						file,
						"Element at index %zu was "
						"incorrect, expected value of "
						"%zu",
						cur_ind,
						cur_ind
					);


					if (print_flag)
					{
						SK_Box_bytes_print
						(
							sbi,
							req,
							box,
							file,
							deco,
							1,
							0,
							max_ind
						);

						SK_LineDeco_print_opt
						(
							file,
							deco,
							""
						);
					}


					error_detected = 1;

					break;
				}


				cur_ind += 1;
			}

			if (error_detected)
			{
				break;
			}


			//Check the index range of the inserted elements
			//themselves, provided that those values were in fact
			//initialized.

			if (0 == touched_init)
			{
				//This skips the inserted element range if it
				//is not initialized

				cur_ind = touch_end + 1;
			}

			while (cur_ind <= touch_end)
			{
				sbi->get_elem
				(
					req,
					box,
					cur_ind,
					elem_1
				);

				int eq = 0;

				SK_byte_eq
				(
					elem_0,
					elem_size,

					elem_1,
					elem_size,

					&eq,
					NULL
				);

				if (0 == eq)
				{
					SK_DEBUG_PRINT_FP
					(
						print_flag,
						file,
						"Element at index %zu was "
						"incorrect, expected value of "
						"#elem_0, which is 0x%02x"
						" repeated",
						cur_ind,
						(int) (((u8 *) elem_0) [0])
					);


					if (print_flag)
					{
						SK_Box_bytes_print
						(
							sbi,
							req,
							box,
							file,
							deco,
							1,
							0,
							max_ind
						);

						SK_LineDeco_print_opt
						(
							file,
							deco,
							""
						);
					}


					error_detected = 1;

					break;
				}


				cur_ind += 1;
			}

			if (error_detected)
			{
				break;
			}


			//Check the index range ABOVE the inserted elements

			while (cur_ind < new_num_elems)
			{
				sbi->get_elem
				(
					req,
					box,
					cur_ind,
					elem_1
				);


				size_t calc_ind =

				cur_ind - num_touch;


				int eq = 0;

				SK_byte_eq
				(
					&calc_ind,
					ind_compare_len,

					elem_1,
					ind_compare_len,

					&eq,
					NULL
				);

				if (0 == eq)
				{
					SK_DEBUG_PRINT_FP
					(
						print_flag,
						file,
						"Element at index %zu was "
						"incorrect, expected value of "
						"%zu",
						cur_ind,
						calc_ind
					);


					if (print_flag)
					{
						SK_Box_bytes_print
						(
							sbi,
							req,
							box,
							file,
							deco,
							1,
							0,
							max_ind
						);

						SK_LineDeco_print_opt
						(
							file,
							deco,
							""
						);
					}


					error_detected = 1;

					break;
				}


				cur_ind += 1;
			}

			if (error_detected)
			{
				break;
			}
		}


		//Increment the test parameters

		int looped = 0;


		SK_inc_chain
		(
			&resize_count,
			1,
			0,
			num_resizes,
			1,
			&looped
		);

		if (looped)
		{
			break;
		}
	}


	//If #SK_MemDebug was used, restore the old value of
	//#SK_MemDebug_break_allocators.

	if (SK_MemDebug_used)
	{
		SK_MemDebug_break_allocators = old_SK_MemDebug_break_allocators;
	}


	//Deallocate the memory used for the anchors (if any)

	free (anc_0);
	free (anc_1);


	//Deallocate the memory used for the temporary elements

	free (elem_0);
	free (elem_1);


	//Announce the test-bench ending

	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Given an #SK_BoxIntf, this performs a test-bench on the functions that
 *  change the number of elements contained in an #SK_Box.
 *
 *
 *  Sub-tests used
 *
 *  	SK_BoxIntf_ins_rem_single_tb ()
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *  	None
 *
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param sbi			The #SK_BoxIntf to test
 *
 *  @return			Standard status code
 */

SlySr SK_BoxIntf_ins_rem_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
)
{
	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);

	i64 next_depth = SK_p_depth_next (p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Select constants that determine how many parameterizations to test

	const i64 BEG_ELEM_SIZE =		1;
	const i64 END_ELEM_SIZE =		10;
	const i64 INC_ELEM_SIZE =		1;

	const i64 BEG_NUM_STAT_ELEMS =		0;
	const i64 END_NUM_STAT_ELEMS =		4;
	const i64 INC_NUM_STAT_ELEMS =		1;

	const i64 BEG_ELEMS_PER_BLOCK =		0;
	const i64 END_ELEMS_PER_BLOCK =		4;
	const i64 INC_ELEMS_PER_BLOCK =		1;

	const i64 BEG_NEG_SLACK =		0;
	const i64 END_NEG_SLACK =		2;
	const i64 INC_NEG_SLACK =		1;

	const i64 BEG_POS_SLACK =		0;
	const i64 END_POS_SLACK =		2;
	const i64 INC_POS_SLACK =		1;


	i64 elem_size =				BEG_ELEM_SIZE;

	i64 num_stat_elems =			BEG_NUM_STAT_ELEMS;

	i64 elems_per_block =			BEG_ELEMS_PER_BLOCK;

	i64 neg_slack =				BEG_NEG_SLACK;

	i64 pos_slack =				BEG_POS_SLACK;


	//Enter the test-loop, in which storage objects with different
	//parameters will be initialized and deinitialized

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//Create an #SK_BoxReqs that describes the storage object that
		//is desired to be tested

		SK_BoxStatReqs srq =

		{
			.mem =			&SK_MemDebug,

			.elem_size =		elem_size,

			.hint_num_stat_elems =	num_stat_elems,

			.hint_elems_per_block=	elems_per_block
		};


		SK_BoxDynaReqs drq =

		{
			.hint_neg_slack =		neg_slack,

			.hint_pos_slack =		pos_slack,

			.hint_use_max_num_dyna_blocks =	0,

			.hint_max_num_dyna_blocks =	0
		};


		SK_BoxStatImpl sim;

		SK_BoxDynaImpl dim;

		SK_BoxReqs req;


		SK_BoxReqs_prep (&req, &srq, &sim, &drq, &dim);


		if (print_flag)
		{
			SK_LineDeco_print_opt (file, deco, "The current #SK_BoxReqs is...\n\n");

			SK_BoxReqs_print (&req, 2, file, deco);

			SK_LineDeco_print_opt (file, deco, "");
		}


		//Determine if there are limits to how many elements the
		//#SK_Box's described by #req can store

		int lim_exist = 0;

		size_t lim_min = 0;
		size_t lim_max = 0;

		sbi->get_resize_limits (&req, &lim_exist, &lim_min, &lim_max);


		if (print_flag)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"lim_exist = %zu",
				lim_exist
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"lim_min =   %zu",
				lim_min
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"lim_max =   %zu",
				lim_max
			);


			SK_LineDeco_print_opt (file, deco, "");
		}


		//Allocate static memory for #SK_Box instances

		size_t inst_stat_mem = 0;

		sbi->inst_stat_mem (&req, &inst_stat_mem);


		SK_Box * box_0 = malloc (inst_stat_mem);


		if (NULL == box_0)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Could not allocate #box_0, "
				"#inst_stat_mem = %zu",
				inst_stat_mem
			);

			free (box_0);

			error_detected = 1;

			break;
		}


		//Initialize #box_0 with the minimum possible elements
		//initially

		SlySr sr =

		sbi->init
		(
			&req,
			box_0,
			lim_min,
			SK_Box_elem_rw_set_zero,
			NULL
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Could not initialize #box_0"
			);

			free (box_0);

			error_detected = 1;

			break;
		}


		//Execute a test-bench for repeatedly changing the number of
		//elements stored inside of an #SK_Box using the current
		//#SK_BoxReqs

		size_t num_resizes =	1000;
		size_t min_num_elems =	SK_MAX (0, lim_min);
		size_t max_num_elems =	SK_MIN (lim_min + 100, lim_max);


		SK_LineDeco ind_deco = deco;

		ind_deco.pre_str_cnt += 1;


		sr =

		SK_BoxIntf_ins_rem_single_tb
		(
			next_depth,
			file,
			ind_deco,

			sbi,
			&req,
			box_0,

			num_resizes,

			min_num_elems,
			max_num_elems
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				SK_BOX_INTF_L_DEBUG,
				file,
				"Inner test-bench failed!"
			);

			error_detected= 1;

			break;
		}


		//Deinitialize #box_0

		sbi->deinit (&req, box_0);


		//Free the memory allocated for #box_0's static memory

		free (box_0);


		//Increment the test parameters

		int looped = 0;


		SK_inc_chain
		(
			&elem_size,
			1,
			BEG_ELEM_SIZE,
			END_ELEM_SIZE,
			INC_ELEM_SIZE,
			&looped
		);

		SK_inc_chain
		(
			&num_stat_elems,
			looped,
			BEG_NUM_STAT_ELEMS,
			END_NUM_STAT_ELEMS,
			INC_NUM_STAT_ELEMS,
			&looped
		);

		SK_inc_chain
		(
			&elems_per_block,
			looped,
			BEG_ELEMS_PER_BLOCK,
			END_ELEMS_PER_BLOCK,
			INC_ELEMS_PER_BLOCK,
			&looped
		);

		SK_inc_chain
		(
			&neg_slack,
			looped,
			BEG_NEG_SLACK,
			END_NEG_SLACK,
			INC_NEG_SLACK,
			&looped
		);

		SK_inc_chain
		(
			&pos_slack,
			looped,
			BEG_POS_SLACK,
			END_POS_SLACK,
			INC_POS_SLACK,
			&looped
		);

		if (looped)
		{
			break;
		}
	}


	//Announce the test-bench ending

	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Given an #SK_BoxIntf, this performs a small speed test on it.
 *
 *
 *  Sub-tests used
 *
 *	None
 *
 *
 *  #SK_BoxIntf functions directly used
 *
 *	deinit ()
 *	get_resize_limits ()
 *	init ()
 *	inst_stat_mem ()
 *	get_elem ()
 *	set_elem ()
 *
 *
 *  @param p_depth		See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param file			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param deco			See SK_BoxIntf_init_deinit_tb ()
 *
 *  @param sbi			The #SK_BoxIntf to test
 *
 *  @return			Standard status code
 */

//FIXME : This test-bench is currently a stub for a more in-depth speed test.
//This test-bench needs to be completely re-made at one point!

SlySr SK_BoxIntf_speed_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	const SK_BoxIntf *	sbi
)
{

	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);

	//i64 next_depth = SK_p_depth_next (p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (print_flag, file, sbi, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Create an #SK_BoxReqs that describes the storage object that
	//is desired to be tested

	size_t elem_size =		sizeof (double);

	size_t num_stat_elems =		16;

	size_t elems_per_block =	16;

	size_t neg_slack =		2;

	size_t pos_slack =		1;



	SK_BoxStatReqs srq =

	{
		.mem =				&SK_MemStd,

		.elem_size =			elem_size,

		.hint_num_stat_elems =		num_stat_elems,

		.hint_elems_per_block =		elems_per_block
	};


	SK_BoxDynaReqs drq =

	{
		.hint_neg_slack =			neg_slack,

		.hint_pos_slack =			pos_slack,

		.hint_use_max_num_dyna_blocks =		0,

		.hint_max_num_dyna_blocks =		0
	};


	SK_BoxStatImpl sim;

	SK_BoxDynaImpl dim;

	SK_BoxReqs req;


	SK_BoxReqs_prep (&req, &srq, &sim, &drq, &dim);


	SK_LineDeco_print_opt (file, deco, "The current #SK_BoxReqs is...\n\n");

	SK_BoxReqs_print (&req, 2, file, deco);

	SK_LineDeco_print_opt (file, deco, "");


	//Determine if there are limits to how many elements the
	//storage object described by #req can store

	int lim_exist = 0;

	size_t lim_min = 0;
	size_t lim_max = 0;

	sbi->get_resize_limits (&req, &lim_exist, &lim_min, &lim_max);


	SK_LineDeco_print_opt
	(
		file,
		deco,
		"lim_exist = %zu",
		lim_exist
	);

	SK_LineDeco_print_opt
	(
		file,
		deco,
		"lim_min =   %zu",
		lim_min
	);

	SK_LineDeco_print_opt
	(
		file,
		deco,
		"lim_max =   %zu",
		lim_max
	);


	SK_LineDeco_print_opt (file, deco, "");


	//Arbitrarily select the number of elements to test with, while
	//respecting the resize limits of the storage object

	size_t num_elems = 16 * 1000 * 1000;

	if (lim_exist)
	{
		num_elems = SK_MAX (num_elems, lim_min);

		num_elems = SK_MIN (num_elems, lim_max);
	}


	SK_LineDeco_print_opt
	(
		file,
		deco,
		"num_elems =  %zu",
		num_elems
	);


	SK_LineDeco_print_opt (file, deco, "");


	//Create 2 "normal" arrays to use as a source / destination of element reads
	//and writes.
	//
	//These will be used to compare the results of #sbi against "plain"
	//in terms of execution speed.

	double * array_0 = malloc (sizeof (double) * num_elems);
	double * array_1 = malloc (sizeof (double) * num_elems);


	if ((NULL == array_0) || (NULL == array_1))
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"Could not allocate #array_0 and #array_1, num_elems = %zu",
			num_elems
		);

		return SlySr_get (SLY_BAD_ALLOC);
	}


	//Initialize #array_1 such that it can be used as a source of arbitrary
	//values

	for (size_t elem_sel = 0; elem_sel < num_elems; elem_sel += 1)
	{
		array_1 [elem_sel] = (double) elem_sel * 0.5;
	}


	//Perform the setting test on #array_0.
	//
	//Note that the casts to (volatile) are used to avoid optimizing
	//out certain statements.

	clock_t beg_time;

	clock_t end_time;


	beg_time = clock ();


	for (size_t elem_sel = 0; elem_sel < num_elems; elem_sel += 1)
	{
		((volatile double *) array_0) [elem_sel] = array_1 [elem_sel];
	}


	end_time = clock ();


	double elapsed_time = (double) (end_time - beg_time) / CLOCKS_PER_SEC;


	SK_LineDeco_print_opt
	(
		file,
		deco,
		"Setting the elements from #array_0 took %lf seconds...",
		elapsed_time
	);


	//Perform the getting test on #array_0

	beg_time = clock ();


	for (size_t elem_sel = 0; elem_sel < num_elems; elem_sel += 1)
	{
		* ((volatile double *) &array_1 [elem_sel]) = array_0 [elem_sel];
	}


	end_time = clock ();


	elapsed_time = (double) (end_time - beg_time) / CLOCKS_PER_SEC;


	SK_LineDeco_print_opt
	(
		file,
		deco,
		"Getting the elements from #array_0 took %lf seconds...",
		elapsed_time
	);


	//Determine the amount of static memory that #req calls for

	size_t inst_stat_mem = 0;

	sbi->inst_stat_mem (&req, &inst_stat_mem);


	SK_LineDeco_print_opt
	(
		file,
		deco,
		"inst_stat_mem = %zu",
		inst_stat_mem
	);


	SK_LineDeco_print_opt (file, deco, "");


	//Allocate the static memory for the storage object instance

	void * inst = malloc (inst_stat_mem);


	if (NULL == inst)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"Could not allocate #inst, #inst_stat_mem = %zu",
			inst_stat_mem
		);

		free (array_0);
		free (array_1);

		return SlySr_get (SLY_BAD_ALLOC);
	}


	//Allocate an anchor if recommeneded

	int anc_recommend = 0;

	sbi->anc_recommend (&req, inst, &anc_recommend);


	void * anc = NULL;


	if (anc_recommend)
	{
		size_t anc_size = 0;

		sbi->anc_size (&req, &anc_size);


		anc = malloc (anc_size);


		if (NULL == anc)
		{
			SK_DEBUG_PRINT_FP
			(
				print_flag,
				file,
				"Could not allocate #anc, #anc_size = %zu",
				anc_size
			);

			free (array_0);
			free (array_1);
			free (inst);

			return SlySr_get (SLY_BAD_ALLOC);
		}
	}


	//Initialize #inst to hold #num_elems

	SlySr sr =

	sbi->init
	(
		&req,
		inst,
		num_elems,
		SK_Box_elem_rw_set_zero,
		NULL
	);


	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT_FP
		(
			print_flag,
			file,
			"Could not initialize #inst"
		);

		free (array_0);
		free (array_1);
		free (inst);
		free (anc);

		return sr;
	}


	//Test the amount of time it takes to SET each element in #inst to an
	//arbitrary value

	beg_time = clock ();


	if (anc_recommend)
	{
		sbi->get_anc (&req, inst, 0, anc);

		sbi->set_array_w_anc
		(
			&req,
			inst,
			0,
			(num_elems - 1),
			0,
			anc,
			anc,
			num_elems,
			array_1
		);
	}

	else
	{
		sbi->set_array
		(
			&req,
			inst,
			0,
			(num_elems - 1),
			num_elems,
			array_1
		);
	}


	end_time = clock ();


	elapsed_time = (double) (end_time - beg_time) / CLOCKS_PER_SEC;


	SK_LineDeco_print_opt
	(
		file,
		deco,
		"Setting the elements from #inst took %lf seconds...",
		elapsed_time
	);


	//Test the amount of time it takes to GET each element in #inst

	beg_time = clock ();


	if (anc_recommend)
	{
		sbi->get_anc (&req, inst, 0, anc);

		sbi->get_array_w_anc
		(
			&req,
			inst,
			0,
			(num_elems - 1),
			0,
			anc,
			anc,
			num_elems,
			array_1
		);
	}

	else
	{
		sbi->get_array
		(
			&req,
			inst,
			0,
			(num_elems - 1),
			num_elems,
			array_1
		);
	}


	end_time = clock ();


	elapsed_time = (double) (end_time - beg_time) / CLOCKS_PER_SEC;


	SK_LineDeco_print_opt
	(
		file,
		deco,
		"Getting the elements from #inst took %lf seconds...",
		elapsed_time
	);


	//Free the "plain" arrays

	free (array_0);

	free (array_1);


	//Deinitialize the storage object instance

	sbi->deinit (&req, inst);


	//Deallocate the anchor (if allocated)

	free (anc);


	//Free the memory allocated for the storage object static
	//memory

	free (inst);


	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		0,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  # FOOT-NOTES
 *
 *  - Printing style in #SK_BoxIntf test-benches
 *
 *	The pattern in which diagnostic statements are printed out in the
 *	#SK_BoxIntf test-benches is to directly print to a file for every
 *	individual diagnostic statement.
 *
 *	This style of generating print statements is much slower than
 *	generating a string in memory, and then periodically dumping the string
 *	once it reaches a certain size. In this project, the preferred method
 *	of doing that is to create / use #SK_Str's for that purpose.
 *
 *	However, #SK_StrIntf is built on top of #SK_BoxIntf, so if there is a
 *	code change to #SK_BoxIntf that breaks it, it would probably break
 *	#SK_Str as well. Therefore, #SK_Str should not be used in the testing
 *	of #SK_BoxIntf.
 *
 *	With that in mind, printing-to-file for every diagnostic statement
 *	using SK_LineDeco_print_opt () is considered the next-most preferable
 *	alternative, with both ergonomics / speed taken into consideration.
 */

