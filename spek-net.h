/*!****************************************************************************
 *
 * @file
 * spek-net.h
 *
 *
 * Top-level header for the spek-net project.
 *
 *****************************************************************************/




#ifndef SPEK_NET_H
#define SPEK_NET_H




/// @defgroup		Spek_Net_Version_Constants
///
/// @{


//FIXME: Currently, the "makefile" references these values in this header
//by way of grep'ing for it. So, by messing up these values, it is too easy
//to break the build system. Find a better way to store the version numbers,
//such that these #define's are still available for reading, and so that the
//makefile properly labels things the same version _always_.


/// The major version of the current SpekNet header

#define SPEK_NET_MAJOR_VERSION			(0)


/// The minor version of the current SpekNet header

#define SPEK_NET_MINOR_VERSION			(0)


/// The patch version of the current SpekNet header

#define SPEK_NET_PATCH_VERSION			(0)


/// The semantic version of the current SpekNet header

#define SPEK_NET_SEM_VERSION			"0.0.0"


/// @}




#endif //SPEK_NET_H

