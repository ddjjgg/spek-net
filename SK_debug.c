/*!****************************************************************************
 *
 * @file
 * SK_debug.c
 *
 * Provides per-feature debug levels that can be checked throughout the rest of
 * the program.
 *
 * The general idea is that when a particular feature's debugging level is set
 * to #SK_NO_DEBUG, that feature should no longer have any debugging-related
 * code execute.
 *
 * Otherwise, the amount of debugging-related code to execute can be changed by
 * setting the debugging level to #SK_L_DEBUG, #SK_M_DEBUG, or #SK_H_DEBUG.
 *
 * This file was based on ST_debug.c in sly-time.
 *
 *****************************************************************************/




#include "SK_debug.h"



/*!
 *  Stores the debug level for #SK_Addr related operations in spek-net
 */

u32 SK_Addr_debug_level =		SK_NO_DEBUG;

/*!
 * Stores the debug level for #SK_Array related operations in spek-net
 */

u32 SK_Array_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_BpTree related operations in spek-net
 */

u32 SK_BpTree_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_BoxIntf related operations in spek-net
 */

u32 SK_BoxIntf_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_BoxReqsRegistry related operations in spek-net
 */

u32 SK_BoxReqsRegistry_debug_level =	SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_HmapIntf related operations in spek-net
 */

u32 SK_HmapIntf_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_HmapStd related operations in spek-net
 */

u32 SK_HmapStd_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for miscellaneous features in spek-net
 */

u32 SK_misc_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_MemIntf related operations in spek-net
 */

u32 SK_MemIntf_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_MemStd related operations in spek-net
 */

u32 SK_MemStd_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_StatArray related operations in spek-net
 */

u32 SK_StatArray_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_Vector related operations in spek-net
 */

u32 SK_Vector_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_Yoke related operations in spek-net
 */

u32 SK_Yoke_debug_level =		SK_NO_DEBUG;


/*!
 *  Stores the debug level for #SK_YokeSet related operations in spek-net
 */

u32 SK_YokeSet_debug_level =		SK_NO_DEBUG;




/*!
 *  Sets the debug level for all possible feature debug levels simultaneously
 *
 *  @param debug_level		An integer representing the debug level, which
 *				will usually be one of the macro's SK_NO_DEBUG,
 *				SK_L_DEBUG, SK_M_DEBUG, or SK_H_DEBUG
 *
 *  @return			Standard status code
 */

SlyDr SK_set_all_debug_levels (u32 debug_level)
{
	SK_Addr_debug_level =			debug_level;
	SK_Array_debug_level =			debug_level;
	SK_BpTree_debug_level =			debug_level;
	SK_BoxIntf_debug_level =		debug_level;
	SK_BoxReqsRegistry_debug_level =	debug_level;
	SK_HmapIntf_debug_level =		debug_level;
	SK_HmapStd_debug_level =		debug_level;
	SK_misc_debug_level =			debug_level;
	SK_MemIntf_debug_level =		debug_level;
	SK_MemStd_debug_level =			debug_level;
	SK_StatArray_debug_level =		debug_level;
	SK_Vector_debug_level =			debug_level;
	SK_Yoke_debug_level =			debug_level;
	SK_YokeSet_debug_level =		debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_Addr related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_Addr_set_debug_level (u32 debug_level)
{
	SK_Addr_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_Array related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_Array_set_debug_level (u32 debug_level)
{
	SK_Array_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_BpTree related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_BpTree_set_debug_level (u32 debug_level)
{
	SK_BpTree_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_BoxIntf related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxIntf_set_debug_level (u32 debug_level)
{
	SK_BoxIntf_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_BoxReqsRegistry related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_BoxReqsRegistry_set_debug_level (u32 debug_level)
{
	SK_BoxReqsRegistry_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_HmapIntf related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapIntf_set_debug_level (u32 debug_level)
{
	SK_HmapIntf_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_HmapStd related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_set_debug_level (u32 debug_level)
{
	SK_HmapStd_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for the miscellaneous features of spek-net
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_misc_set_debug_level (u32 debug_level)
{
	SK_misc_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_MemIntf related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_MemIntf_set_debug_level (u32 debug_level)
{
	SK_MemIntf_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_MemStd related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_MemStd_set_debug_level (u32 debug_level)
{
	SK_MemStd_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_StatArray related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_StatArray_set_debug_level (u32 debug_level)
{
	SK_StatArray_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_Vector related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_Vector_set_debug_level (u32 debug_level)
{
	SK_Vector_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_Yoke related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_Yoke_set_debug_level (u32 debug_level)
{
	SK_Yoke_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the debug level for #SK_YokeSet related operations
 *
 *  @param debug_level		See #debug_level in SK_set_all_debug_levels ()
 *
 *  @return			Standard status code
 */

SlyDr SK_YokeSet_set_debug_level (u32 debug_level)
{
	SK_YokeSet_debug_level = debug_level;

	return SlyDr_get (SLY_SUCCESS);
}

