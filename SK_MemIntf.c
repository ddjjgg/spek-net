/*!****************************************************************************
 *
 * @file
 * SK_MemIntf.c
 *
 * See SK_MemIntf.h for details.
 *
 *****************************************************************************/




#include "SK_MemIntf.h"




#include "SK_debug.h"




/*!
 *  @defgroup SK_MemIntf-Unimplemented-Function-Stubs
 *
 *  Functions that can be used to represent unimplemented functions in an
 *  #SK_MemIntf. To use one of these functions in #SK_MemIntf, simply set the
 *  function pointer to the associated function from this section.
 *
 *  @{
 */




SlySr SK_MemIntf_no_impl_take
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size
)
{
	SK_DEBUG_PRINT
	(
		SK_MEM_INTF_L_DEBUG,
		"%s () is not implemented in this interface!",
		((char *) __func__) + 19
	);

	return SlySr_get (SLY_NO_IMPL);
}




SlySr SK_MemIntf_no_impl_retake
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size
)
{
	SK_DEBUG_PRINT
	(
		SK_MEM_INTF_L_DEBUG,
		"%s () is not implemented in this interface!",
		((char *) __func__) + 19
	);

	return SlySr_get (SLY_NO_IMPL);
}




SlySr SK_MemIntf_no_impl_take_near
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		near_addr
)
{
	SK_DEBUG_PRINT
	(
		SK_MEM_INTF_L_DEBUG,
		"%s () is not implemented in this interface!",
		((char *) __func__) + 19
	);

	return SlySr_get (SLY_NO_IMPL);
}




SlySr SK_MemIntf_no_impl_take_soft
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		min_addr,

	void *		max_addr
)
{
	SK_DEBUG_PRINT
	(
		SK_MEM_INTF_L_DEBUG,
		"%s () is not implemented in this interface!",
		((char *) __func__) + 19
	);

	return SlySr_get (SLY_NO_IMPL);
}




SlySr SK_MemIntf_no_impl_take_hard
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		min_addr,

	void *		max_addr
)
{
	SK_DEBUG_PRINT
	(
		SK_MEM_INTF_L_DEBUG,
		"%s () is not implemented in this interface!",
		((char *) __func__) + 19
	);

	return SlySr_get (SLY_NO_IMPL);
}




SlyDr SK_MemIntf_no_impl_free
(
	void *		addr
)
{
	SK_DEBUG_PRINT
	(
		SK_MEM_INTF_L_DEBUG,
		"%s () is not implemented in this interface!",
		((char *) __func__) + 19
	);

	return SlyDr_get (SLY_NO_IMPL);
}




SlyDr SK_MemIntf_no_impl_mem_avail
(
	size_t *	avail_mem
)
{
	SK_DEBUG_PRINT
	(
		SK_MEM_INTF_L_DEBUG,
		"%s () is not implemented in this interface!",
		((char *) __func__) + 19
	);

	return SlyDr_get (SLY_NO_IMPL);
}




SlyDr SK_MemIntf_no_impl_list_impl
(
	SK_MemIntf *	intf
)
{
	SK_DEBUG_PRINT
	(
		SK_MEM_INTF_L_DEBUG,
		"%s () is not implemented in this interface!",
		((char *) __func__) + 19
	);

	return SlyDr_get (SLY_NO_IMPL);
}




SlyDr SK_MemIntf_no_impl_set_conf
(
	const SK_MemIntfConf *	conf
)
{
	SK_DEBUG_PRINT
	(
		SK_MEM_INTF_L_DEBUG,
		"%s () is not implemented in this interface!",
		((char *) __func__) + 19
	);

	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  @}
 */




/*!
 *  Performs a test-bench on a given #SK_MemIntf. This function may be split
 *  into smaller functions to provide a more detailed test-bench in the future.
 *
 *  This function will still report success even if functions in #skm are not
 *  implemented, as long as there are still stubs that properly report
 *  #SLY_NO_IMPL.
 *
 *  @param skm			The #SK_MemIntf to test
 *
 *  @return			Standard status code
 */

SlySr SK_MemIntf_tb
(
	const SK_MemIntf *	skm
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MEM_INTF_L_DEBUG, skm, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench starting

	SK_TB_BEG_PRINT (stdout);


	//This will be used to end test loops prematurely

	int error_detected = 0;


	//Test a normal memory allocation / deallocation with
	//take () / free ().
	//
	//The #min_addr and #max_addr values will be re-used later in the
	//test-bench.

	const size_t NUM_TAKE_TESTS = 100000;


	int * min_addr = NULL;

	int * max_addr = NULL;


	for (size_t test_num = 0; test_num < NUM_TAKE_TESTS; test_num += 1)
	{

		//Allocate an arbitrary array of memory

		int * int_array =	NULL;

		size_t int_array_len =  test_num + 1;


		SlySr sr =

		skm->take
		(
			(void **) &int_array,
			int_array_len,
			sizeof (int)
		);


		if (SLY_NO_IMPL == sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"take () was unimplemented"
			);

			//Note, being unimplemented is not the type of flaw we
			//are looking for, so #error_detected stays at 0

			break;
		}

		else if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"take () failed."
			);

			error_detected = 1;

			break;
		}


		//For use in a later test with address ranges, record the
		//minimum and maximum addresses produced by take ()

		if (min_addr > int_array)
		{
			min_addr = int_array;
		}


		if (max_addr < int_array)
		{
			max_addr = int_array;
		}


		//Test that it is possible to dereference the pointer and write
		//to it correctly

		*(int_array + (int_array_len - 1)) = test_num;


		if (test_num != int_array [int_array_len - 1])
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"Could not dereference pointer produced "
				"by take ()"
			);

			error_detected = 1;

			break;
		}


		//Deallocate the allocated memory

		SlyDr dr = skm->free (int_array);


		if (SLY_NO_IMPL == dr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"free () was unimplemented"
			);

			error_detected = 1;

			break;
		}
	}




	//Test memory reallocation with retake ().

	//FIXME : This section feels very redundant with the section above

	const size_t NUM_RETAKE_TESTS = 100000;


	int * retake_array =		NULL;

	size_t retake_array_len =	0;


	for (size_t test_num = 0; test_num < NUM_RETAKE_TESTS; test_num += 1)
	{

		//Reallocate the arbitrary array of memory at #retake_array

		retake_array_len = test_num + 1;

		if ((0 < test_num) && (0 == (test_num % 10)))
		{
			retake_array_len -= 5;
		}


		SlySr sr =

		skm->retake
		(
			(void **) &retake_array,
			retake_array_len,
			sizeof (int)
		);


		if (SLY_NO_IMPL == sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"retake () was unimplemented"
			);

			//Note, being unimplemented is not the type of flaw we
			//are looking for, so #error_detected stays at 0

			break;
		}

		else if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"retake () failed."
			);

			error_detected = 1;

			break;
		}


		//Test that it is possible to dereference the pointer and write
		//to it correctly

		*(retake_array + (retake_array_len - 1)) = test_num;


		if (test_num != retake_array [retake_array_len - 1])
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"Could not dereference pointer produced "
				"by take ()"
			);

			error_detected = 1;

			break;
		}
	}


	//Deallocate the allocated memory at #retake_array

	SlyDr dr = skm->free (retake_array);




	//This section tests take_near (). Since that function performs a
	//best-effort attempt at allocating close to a given pointer, this
	//particular test-bench will not check more than if it allocated memory
	//whatsoever.

	//FIXME : This section feels redundant with the section above

	const size_t NUM_TAKE_NEAR_TESTS = 100000;


	for (size_t test_num = 0; test_num < NUM_TAKE_NEAR_TESTS; test_num += 1)
	{

		//Allocate an arbitrary array of memory

		int * int_array =	NULL;

		size_t int_array_len =  test_num + 1;


		SlySr sr =

		skm->take_near
		(
			(void **) &int_array,
			int_array_len,
			sizeof (int),
			min_addr
		);


		if (SLY_NO_IMPL == sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"take_near () was unimplemented"
			);

			//Note, being unimplemented is not the type of flaw we
			//are looking for, so #error_detected stays at 0

			break;
		}

		else if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"take_near () failed."
			);

			error_detected = 1;

			break;
		}


		//Test that it is possible to dereference the pointer and write
		//to it correctly

		*(int_array + (int_array_len - 1)) = test_num;


		if (test_num != int_array [int_array_len - 1])
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"Could not dereference pointer produced "
				"by take_near ()"
			);

			error_detected = 1;

			break;
		}


		//Deallocate the allocated memory

		SlyDr dr = skm->free (int_array);


		if (SLY_NO_IMPL == dr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"free () was unimplemented"
			);

			error_detected = 1;

			break;
		}
	}




	//This section tests take_soft (). Since that function
	//performs a best-effort attempt at allocating inside of a given range,
	//this particular test-bench will not check more than if it allocated
	//memory whatsoever.

	//FIXME : This section feels very redundant with the section above

	const size_t NUM_TAKE_SOFT_RANGE_TESTS = 100000;


	for (size_t test_num = 0; test_num < NUM_TAKE_SOFT_RANGE_TESTS; test_num += 1)
	{

		//Allocate an arbitrary array of memory.
		//
		//Note how addresses recorded from earlier uses of take ()
		//are used as the range (min_addr, max_addr).

		int * int_array =	NULL;

		size_t int_array_len =  test_num + 1;


		SlySr sr =

		skm->take_soft
		(
			(void **) &int_array,
			int_array_len,
			sizeof (int),
			min_addr,
			max_addr
		);


		if (SLY_NO_IMPL == sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"take_soft () was unimplemented"
			);

			//Note, being unimplemented is not the type of flaw we
			//are looking for, so #error_detected stays at 0

			break;
		}

		else if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"take_soft () failed."
			);

			error_detected = 1;

			break;
		}


		//Test that it is possible to dereference the pointer and write
		//to it correctly

		*(int_array + (int_array_len - 1)) = test_num;


		if (test_num != int_array [int_array_len - 1])
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"Could not dereference pointer produced "
				"by take_soft ()"
			);

			error_detected = 1;

			break;
		}


		//Deallocate the allocated memory

		SlyDr dr = skm->free (int_array);


		if (SLY_NO_IMPL == dr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"free () was unimplemented"
			);

			error_detected = 1;

			break;
		}

	}




	//This section tests take_hard ().
	//
	//Unlike the above sections, this function DOES guarantee that the
	//memory allocation will be present in the provided range.

	//FIXME : This section feels very redundant with the section above

	const size_t NUM_TAKE_HARD_RANGE_TESTS = 100000;


	for (size_t test_num = 0; test_num < NUM_TAKE_HARD_RANGE_TESTS; test_num += 1)
	{

		//Allocate an arbitrary array of memory
		//
		//Note how addresses recorded from earlier uses of take ()
		//are used as the range (min_addr, max_addr).

		int * int_array =	NULL;

		size_t int_array_len =  test_num + 1;


		SlySr sr =

		skm->take_hard
		(
			(void **) &int_array,
			int_array_len,
			sizeof (int),
			min_addr,
			max_addr
		);


		if (SLY_NO_IMPL == sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"take_hard () was unimplemented"
			);

			//Note, being unimplemented is not the type of flaw we
			//are looking for, so #error_detected stays at 0

			break;
		}

		else if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"take_hard () failed."
			);

			error_detected = 1;

			break;
		}


		//Ensure that the memory allocation respected the range
		//(min_addr, max_addr)

		if ((int_array < min_addr) || (int_array > max_addr))
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"(int_array = %p) was not in the range "
				"(min_addr = %p, max_addr = %p). "
				"take_hard () is flawed.",
				int_array,
				min_addr,
				max_addr
			);

			error_detected = 1;

			break;
		}


		//Test that it is possible to dereference the pointer and write
		//to it correctly

		*(int_array + (int_array_len - 1)) = test_num;


		if (test_num != int_array [int_array_len - 1])
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"Could not dereference pointer produced "
				"by take_hard ()"
			);

			error_detected = 1;

			break;
		}


		//Deallocate the allocated memory

		SlyDr dr = skm->free (int_array);


		if (SLY_NO_IMPL == dr.res)
		{
			SK_DEBUG_PRINT
			(
				SK_MEM_INTF_L_DEBUG,
				"free () was unimplemented"
			);

			error_detected = 1;

			break;
		}

	}



	//Test if the mem_avail () function works proper.

	//FIXME : A better test needs to be made for this


	size_t avail_mem = 0;


	dr = skm->mem_avail (&avail_mem);


	if (SLY_NO_IMPL == dr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_MEM_INTF_L_DEBUG,
			"mem_avail () is unimplemented"
		);

	}

	else if (SLY_SUCCESS != dr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_MEM_INTF_L_DEBUG,
			"mem_avail () did not succeed"
		);


		error_detected = 1;
	}

	else if (0 == avail_mem)
	{
		SK_DEBUG_PRINT
		(
			SK_MEM_INTF_L_DEBUG,
			"mem_avail () reported %zu available memory",
			avail_mem
		);


		error_detected = 1;
	}




	//FIXME : Test #skm->list_impl () here




	//Test setting a configuration with set_conf ()

	//FIXME : There's almost certainly better ways to test this

	SK_MemIntfConf conf;

	conf.hint_pre_alloc_size =	1000;
	conf.hint_min_common_size = 	1;
	conf.hint_max_common_size =	10;


	dr = skm->set_conf (&conf);


	if (SLY_NO_IMPL == dr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_MEM_INTF_L_DEBUG,
			"set_conf () is unimplemented"
		);

	}

	else if (SLY_SUCCESS != dr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_MEM_INTF_L_DEBUG,
			"set_conf () did not succeed"
		);


		error_detected = 1;
	}




	//Announce the test-bench ending

	SK_TB_END_PRINT
	(
		stdout,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);

}
