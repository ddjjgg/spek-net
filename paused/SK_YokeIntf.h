/*!****************************************************************************
 *
 * @file
 * SK_YokeIntf.h
 *
 * Provides an explicit interface that a data structure must implement in order
 * to be used as an #SK_Yoke.
 *
 *****************************************************************************/




#ifndef SK_YOKEINTF_H
#define SK_YOKEINTF_H




#include <stdio.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "SK_misc.h"




//FIXME : Completely redesign this interface using "./examples/SK_FooBarIntf.h"
//and "./deprecated/SK_Yoke.c" as a basis.




/*!
 *  Defines an interface that is needed to be used like an #SK_Yoke. This
 *  interface is defined in terms of function pointers.
 *
 *  In this interface, the object that is being used _like_ an #SK_Yoke is
 *  referred to as "the object".
 */

typedef struct SK_YokeIntf
{




	//FIXME : Should there be a variant of #init that takes a function
	//pointer to a memory allocation function as an argument? By doing
	//this, it can be easier to switch from always using malloc () in the
	//future.




	/*!
	 *  Initialzes the object using this interface, which has had memory
	 *  allocated for it but has not had any of its fields initialized.
	 *
	 *  @warning
	 *  The caller of this function MUST call #deinit on #obj sometime
	 *  after using this function to avoid resource leaks.
	 *
	 *  @param obj			The object to initialize
	 *
	 *  @param links_len		The initial number of links to allocate
	 *  				inside of the object
	 *
	 *  @return			Standard status code
	 */

	SlySr (*init) (void * obj, size_t links_len);




	/*!
	 *  Deinitializes the object using this interface, which means
	 *  deallocating all resources (memory, files, etc.) the object uses.
	 *
	 *  This function MUST automatically unlink #obj from all other objects
	 *  it is linked to.
	 *
	 *  @warning
	 *  This should ONLY be called on objects that were previously
	 *  initialized with #init, NOT with #create.
	 *
	 *  @param obj			The object to deinitialize
	 *
	 *  @return			Standard status code
	 */

	SlyDr (*deinit) (void * obj);




	//FIXME : Should fields for #create and #destroy exist here, so that
	//SK_Yoke_create () and SK_Yoke_destroy () can be mapped here? Is that
	//even useful when this data structure is being accessed through this
	//data structure?




	/*!
	 *  Returns the result of using sizeof () on the object. This is needed
	 *  in order to allocate space for the object in arrays, which can then
	 *  be initialized with #init.
	 *
	 *  @return			The result of using sizeof () on the
	 *  				object
	 */

	//FIXME : Change the name of this function to be "const_part_size ()".
	//Why is this? It is theoretically possible to create data structures
	//in memory that DO NOT have a C "struct" definition associated with
	//them. For these, you would never be able to call sizeof () on such
	//data structures. However, you would always be able to provide the
	//size of the "constant part".

	size_t (*get_sizeof) ();




	//FIXME : Consider making a function that prints to a string, and then
	//re-using that in the print function. That design is much more
	//generic, and allows for the string to be used in many more places.




	/*!
	 *  Prints out relevant information regarding the object to a specified
	 *  #FILE stream.
	 *
	 *  @param obj			The object to print out
	 *
	 *  @param file			The #FILE to print to, such as #stdout
	 *
	 *  @param deco			An #SK_LineDeco describing how to
	 *				space / decorate the printed lines.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* print) (const void * obj, FILE * file, SK_LineDeco deco);




	/*!
	 *  Unlinks 2 objects that have been linked together. (The objects
	 *  are the same type as "the object").
	 *
	 *  If the objects are not linked together, then this should not have
	 *  an effect on either object.
	 *
	 *  @param obj_0		The 1st object, which may or may not be
	 *  				linked to #obj_1
	 *
	 *  @param obj_1		The 2nd object, which may or may not be
	 *  				linked to #obj_0
	 *
	 *  @return			Standard status code. Note that
	 *				#SLY_SUCCESS will still be returned
	 *				even if the link between #obj_0 and
	 *				#obj_1 does not exist.
	 */

	SlyDr (* unlink) (void * obj_0, void * obj_1);




	/*!
	 *  Unlinks all other objects from the object. (All of the objects are
	 *  the same type).
	 *
	 *  @param obj			The object which should have all other
	 *  				objects unlinked from it.
	 *
	 *  @return			Standard status code.
	 */

	SlyDr (* unlink_all) (void * obj);




	/*!
	 *  Links together 2 objects. (All of the objects are the same type).
	 *
	 *  If #obj_0 and #obj_1 are already linked, a duplicate link will
	 *  NOT be established. (#SK_Yoke does not permit storing duplicate
	 *  links).
	 *
	 *  This function SHOULD attempt to automatically reallocate memory
	 *  in either object if necessary to make the linking successful.
	 *
	 *  @param obj_0		The object to link to #obj_1
	 *
	 *  @param incr_0		The number of new links that should be
	 *				allocated in #obj_0. This will ONLY be
	 *				used if a reallocation is needed in
	 *				#obj_0. Using a value greater than 1
	 *				here can help avoid frequent costly
	 *				reallocations.
	 *
	 *  @param obj_1		The object to link to #obj_0
	 *
	 *  @param incr_1		The number of new links that should be
	 *				allocated in #obj_1. See #incr_0.
	 *
	 *  @return			Standard status code. This function
	 *				will fail if establishing the link
	 *				between #obj_0 and #obj_1 required a
	 *				memory allocation, and that allocation
	 *				failed.
	 */

	SlySr (* link) (void * obj_0, size_t incr_0, void * obj_1, size_t incr_1);




	/*!
	 *  Links together 2 objects. (All of the objects are the same type).
	 *
	 *  If #obj_0 and #obj_1 are already linked, a duplicate link will
	 *  NOT be established. (#SK_Yoke does not permit storing duplicate
	 *  links).
	 *
	 *  @warning
	 *  Since establishing a link between 2 objects may require allocating
	 *  memory, this function can fail if there is no space available to
	 *  establish a link between #obj_0 and #obj_1.
	 *
	 *  This function should NOT attempt to automatically reallocate memory
	 *  in either object to make the linking successful.
	 *
	 *  @param obj_0		The object to link to #obj_1
	 *
	 *  @param obj_1		The object to link to #obj_0
	 *
	 *  @return			Standard status code. This function
	 *				will fail if establishing the link
	 *				between #obj_0 and #obj_1 requires
	 *				memory allocation.
	 */

	SlySr (* link_no_alloc) (void * obj_0, void * obj_1);




	/*!
	 *  Checks whether a link already exists between 2 objects. (All
	 *  objects are of the same type).
	 *
	 *  @param obj_0		The object that might be linked to
	 *				#obj_1
	 *
	 *  @param obj_1		The object that might be linked to
	 *				#obj_0
	 *
	 *  @param exists		Where to place a flag that indicates if
	 *				the link exists.
	 *
	 *				This will be 0 if the link does not
	 *				exist, and 1 if it does.
	 *
	 *  @return			Standard status code.
	 */

	SlyDr (* link_exists) (const void * obj_0, const void * obj_1, int * exists);




	/*!
	 *  Gets the number of active links and maximum number of links from
	 *  the object.
	 *
	 *  The number of active links is the number of objects that the object
	 *  is linked to.  The maximum number of links in the object is the
	 *  number of links that can be established without incurring a memory
	 *  reallocation.
	 *
	 *  @param obj			The object to examine.
	 *
	 *  @param active_links		Where to place the number of active
	 *				links. This CAN be NULL.
	 *
	 *  @param max_links		Where to place the maximum number of
	 *				links. This CAN be NULL.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_num_links)
	(
		const void *	obj,
		size_t *	num_active_links,
		size_t *	num_max_links
	);




	/*!
	 *  Reallocates the memory used to store the links in the object so
	 *  that it can support a different maximum number of links.
	 *
	 *  @warning
	 *  If the new maximum number of links is smaller than its previous
	 *  value, this function WILL have to unlink as many objects as needed
	 *  to fit the number of active links in this new size. Which links
	 *  specifically will be broken will be selected by this function, with
	 *  the caller unable to influence that selection.
	 *
	 *  Therefore, if the caller wants specific links to be broken first,
	 *  it should use a combination of the #get_num_links and #unlink
	 *  functions. These can be used to determine how many links need to be
	 *  broken, and to select which links to break.
	 *
	 *  @param obj			The object to reallocate memory for
	 *				to store a new number of links
	 *
	 *  @param new_links_len	The maximum number of links that #obj
	 *				should be able to store
	 *
	 *  @param preserve		A flag indicating whether or not to
	 *				save as many connections as possible.
	 *
	 *				If this is true, then as many links as
	 *				possible will be preserved inside of
	 *				#obj.
	 *
	 *				If this is false, then all the links in
	 *				#obj will be broken.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* links_realloc) (void * obj, size_t new_links_len, int preserve);




	/*!
	 *  Minifies the number of links in the object.
	 *
	 *  In other words, this reallocates the links in #obj so that the
	 *  maximum number of links will match the number of active links.
	 *
	 *  @obj			The object to minify the number of
	 *				links within
	 *
	 *  @return			Standard status code.
	 */

	SlySr (* links_minify) (void * obj);

}

SK_YokeIntf;




#endif //SK_YOKEINTF_H

