/*!****************************************************************************
 *
 * @file
 * SK_StrIntf.h
 *
 * Provides the interface that data structures must provide in order to be used
 * like a #SK_Str.
 *
 * While normal ASCII strings associate 1 byte with 1 textual character, that
 * is obviously not true for all encodings (ex : UTF-8).
 *
 * So instead of defining #SK_Str as...
 *
 * "A null-terminated sequence of bytes where 1 byte represents 1 textual
 * character"
 *
 * ...the definition of #SK_Str in this project will be...
 *
 * "A sequence of bytes with an explicitly known encoding and an explicitly
 * known length."
 *
 * #SK_Str MUST store the encoding and length as separate fields, and it
 * MUST be able to report this information to users. "Guessing" the encoding
 * should never be necessary with #SK_Str.
 *
 * This interface should make as few assumptions about the memory layout of the
 * string as possible, preferably by storing the strings in an #SK_Box.
 * Additionally, this interface should provide a mechanism for converting
 * to/from #SK_Str's from/to regular ASCII "c-strings".
 *
 *****************************************************************************/




#ifndef SK_STRINTF_H
#define SK_STRINTF_H




#include <stdio.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




//FIXME : Completely redesign this interface using "./examples/SK_FooBarIntf.h"
//as a basis; many improvements have been made to #SK_BoxIntf-styled interfaces
//since this has started.




/*!
 *  Defines an interface that is needed to be used like an #SK_Str. This
 *  interface is defined in terms of function pointers.
 *
 *  In this interface, the data-type that is being used like an #SK_Str is
 *  referred to as "string".
 */

// typedef struct SK_StrIntf
// {
//
// 	/*!
// 	 *  Initializes the string, which has had memory allocated for it but
// 	 *  has not had any of its fields initialized.
// 	 *
// 	 *  @warning
// 	 *  The caller of this function MUST call #deinit on #str sometime
// 	 *  after using this function to avoid resource leaks.
// 	 *
// 	 *  @param str			The string to initialize
// 	 *
// 	 *  @param init_size		The number of bytes that should
// 	 *				initially be allocated inside of #str.
// 	 *				This CAN be 0.
// 	 *
// 	 *  @param max_size		The maximum number of bytes the string
// 	 *				should be capable of storing.
// 	 *
// 	 *				Any function that automatically resizes
// 	 *				the #SK_StrIntf should respect this
// 	 *				value.
// 	 *
// 	 *				This value can be changed later on with
// 	 *				#set_max_size.
// 	 *
// 	 *				(This argument is VERY useful for
// 	 *				preventing string modifications from
// 	 *				taking up huge amounts of memory when
// 	 *				you can't know the final size of the
// 	 *				string beforehand.)
// 	 *
// 	 *  @return			Standard status code
// 	 */
//
// 	SlySr (*init) (void * str, size_t init_size, size_t max_size);
//
//
//
//
// 	/*!
// 	 *  Deinitializes the string previously initialized with #init, which
// 	 *  means deallocating all resources (memory, files, etc.) the string
// 	 *  uses.
// 	 *
// 	 *  @warning
// 	 *  This should ONLY be called on objects that were previously
// 	 *  initialized with #init.
// 	 *
// 	 *  @param str			The string to deinitialize
// 	 *
// 	 *  @return			Standard status code
// 	 */
//
// 	SlyDr (*deinit) (void * str);
//
//
//
//
// 	/*!
// 	 *  Returns the result of using sizeof () on the string data-type. This
// 	 *  is needed in order to allocate space for the string, which can then
// 	 *  be intiialized with #init (which may internally perform more memory
// 	 *  allocations).
// 	 *
// 	 *  @return			The result of using sizeof () on the
// 	 *				string data-type
// 	 */
//
// 	size_t (*get_sizeof) ();
//
//
//
//
// 	/*!
// 	 *  Creates a traditional C-style-string from the string using this
// 	 *  interface. This function MUST allocate a NEW memory region for that
// 	 *  C-style-string.
// 	 *
// 	 *  This function MUST guarantee to end the generated C-style-string
// 	 *  with a null-terminator ('\0').
// 	 *
// 	 *  @warning
// 	 *  The implementation of this function must realize that the
// 	 *  generated C-style-string can modified, copied, and deleted
// 	 *  independently from the string using this interface. The lifetime of
// 	 *  the C-style-string is SEPARATE from the lifetime of #str.
// 	 *
// 	 *  In other words, if #c_str is set to a pointer to a contigious
// 	 *  region of memory used by #str, that is WRONG.
// 	 *
// 	 *  @warning
// 	 *  The caller MUST eventually call #destroy_c_str on #c_str, otherwise
// 	 *  a resource leak WILL occur. It might seem that calling free () is
// 	 *  possible, but this is wrong. There is NO guarantee that malloc ()
// 	 *  was used to allocate the memory region at #c_str.
// 	 *
// 	 *  @param str			The string that the C-style-string will
// 	 *				be generated from
// 	 *
// 	 *  @param ignore_term		Indicates if the C-style-string created
// 	 *				by this function should end at the
// 	 *				first occurrence of the null-terminator
// 	 *				('\0') in #str, or it should copy
// 	 *				the full set of bytes stored in #str
// 	 *				(which may contain multiple
// 	 *				null-terminators)
// 	 *
// 	 *  @param c_str		Where a pointer to a newly-created
// 	 *				C-style-string will be placed
// 	 *
// 	 *  @return			Standard status code.
// 	 */
//
// 	SlySr create_c_str (const void * str, int ignore_term, char ** c_str);
//
//
//
//
// 	/*!
// 	 *  Destroys a C-style-string that was previously created with
// 	 *  #create_c_str.
// 	 *
// 	 *  @warning
// 	 *  This should ONLY be called on C-style-strings that were previously
// 	 *  created with #create_c_str. Since #create_c_str does not
// 	 *  necessarily use malloc (), this does not necessarily use free ().
// 	 *
// 	 *  @param			The C-style-string to destroy
// 	 *
// 	 *  @return			Standard status code
// 	 */
//
// 	SlyDr destroy_c_str (char * c_str);
//
//
//
//
// 	/*!
// 	 *  Sets the maximum number of bytes that should be possible to store
// 	 *  in the string. This maximum size MUST be respected by any functions
// 	 *  in this interface that automatically resizes the string.
// 	 *
// 	 *  By setting this value, this prevents accidental / malicious memory
// 	 *  usage during string generation.
// 	 *
// 	 *  It can be difficult to know the final size of a string before it is
// 	 *  actually printed, so it is extremely useful to let the functions in
// 	 *  this interface automatically resize the string. But if we do that,
// 	 *  we must inform them what the upper-limit is.
// 	 *
// 	 *  @warning
// 	 *  There can be overhead to the string data type other than just the
// 	 *  number of bytes it stores. Therefore, the maximum number of
// 	 *  storable bytes should not be interpreted as the maximum size of the
// 	 *  data structure itself. (Albeit, this overhead should typically be
// 	 *  quite small compared to the size of string's storable bytes).
// 	 *
// 	 *  @param str			The string object to modify
// 	 *
// 	 *  @param max_size		The maximum number of bytes that should
// 	 *				be possible to store in the string
// 	 *
// 	 *  @return			Standard status code
// 	 */
//
// 	SlyDr set_max_size (void * str, size_t max_size);
//
//
//
//
// 	/*!
// 	 *  Sets a hint in the string object indicating the minimum amount by
// 	 *  which the number of storable bytes should increase by when a memory
// 	 *  reallocation occurs inside of the string. This is useful for
// 	 *  preventing frequent memory allocations during small string
// 	 *  modifications, such as when a string constantly has 1 byte appended
// 	 *  to it.
// 	 *
// 	 *  @warning
// 	 *  The string object is free to ignore this value. This is why this is
// 	 *  considered a "hint".
// 	 *
// 	 *  @param str			The string object to modify
// 	 *
// 	 *  @param min_inc		The minimum number of bytes that the
// 	 *				number of storable bytes should
// 	 *				increase by, whenever memory is
// 	 *				allocated to increase the number of
// 	 *				storable bytes. (What a mouthful)
// 	 *
// 	 *  @return			Standard status code
// 	 */
//
// 	SlyDr set_hint_min_inc (void * str, size_t min_inc);
//
//
//
//
//
// 	/*!
// 	 *  Resizes a string object so that it can store a specified number of
// 	 *  bytes. This number of bytes SHOULD include space for a NULL
// 	 *  terminator character, '\0'.
// 	 *
// 	 *  @warning
// 	 *  If there is a '\0' character somewhere in the string and the size
// 	 *  of the string is increased, this will not alter that '\0'
// 	 */
//
//
//
//
// 	/*!
// 	 *  Generates a string using the same calling conventions as
// 	 *  sprintf (), and stores the result in #str.
// 	 *
// 	 *  @param f_str		A C-style
// 	 */
//
// 	c_str_sprintf_set
//
//
//
//
// 	c_str_sprintf_insert
//
//
//
//
// 	c_str_sprintf_end
//
//
//
//
// 	c_str_sprintf_beg
//
//
//
//
// 	concat
//
//
//
//
//
// }



#endif //SK_STRINTF_H

