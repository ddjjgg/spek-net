/*!****************************************************************************
 *
 * @file
 * SK_YokeIntf.c
 *
 * Provides an explicit interface that a data structure must implement in order
 * to be used "like" an #SK_Yoke.
 *
 *****************************************************************************/




#include "SK_YokeIntf.h"




#include <stdlib.h>
#include <SlyResult-0-x-x.h>




/*!
 *  Performs a general test-bench on a specific instantiation of an
 *  #SK_YokeIntf.
 *
 *  @param intf			The #SK_YokeIntf to test
 *
 *  @return			Standard status code
 */

SlySr SK_YokeIntf_instance_tb (const SK_YokeIntf * intf)
{

	//FIXME : Implement this function


	return SlySr_get (SLY_SUCCESS);
}
