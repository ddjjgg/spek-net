/*!****************************************************************************
 *
 * @file
 * SK_Regi.c
 *
 * See SK_Regi.h for deatails.
 *
 *****************************************************************************/




#include "SK_Regi.h"




#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "SK_BoxIntf.h"
#include "SK_debug.h"
#include "SK_MemReqIntf.h"
#include "SK_misc.h"




//FIXME : Finish developing #SK_HmapIntf, and make at least one implementation
//of it, and then use that as the basis of #SK_Regi



//FIXME : Finish making #SK_BoxReqsRegistry into a more generic
//data-structure called #SK_Regi, and making #SK_BoxReqsRegi a specialization
//of this data structure.




/*!
 *  @defgroup SK_Regi
 *
 *  @{
 *
 *  Provides a "registry" in which unique values can be stored, and the number
 *  of users of a value can be easily tracked.
 *
 *  #SK_Regi is very similar to other dictionaries / hash-maps, except that
 *  reference counting is utilized to determine when key-value pairs should be
 *  removed from memory. This allows for multiple users to safely obtain
 *  pointers to the same piece of data by registering / unregistering their
 *  usage of the data.
 *
 *  The function SK_Regi_reg () is used to make a new entry in an #SK_Regi
 *  instance, which will make a new entry for this value (if it does not
 *  already exist). Other users can call SK_Regi_reg () or
 *  SK_Regi_reg_by_hash () to register themselves as users of this value to
 *  gain access to it. While a user is registered for a value, that value is
 *  GUARANTEED to stay to remain in the same position in memory.
 *
 *  Every registration performed via one of the SK_Regi_reg* () functions MUST
 *  eventually be followed by a call to SK_Regi_unreg () to unregister a user
 *  as being in use of a value.
 *
 *  Once there are 0 users associated with a value, that gives #SK_Regi the
 *  opportunity to remove this value from the registry. If SK_Regi_unreg () is
 *  not properly called, a memory leak may very well occur. Additionally, if
 *  SK_Regi_reg () is not used to properly gain access to a value, the
 *  referenced value may be removed from memory without notification, leading
 *  to memory access errors.
 *
 *  #SK_Regi performs locking internally, and it is built to be accessed by its
 *  functions by ANY number of threads at once, EXCEPT for the functions
 *  SK_Regi_init (), SK_Regi_deinit (), SK_Regi_create (), and
 *  SK_Regi_destroy ().
 *
 *
 *
 */




//  /*!
//   *  A bundle of information regarding an #SK_BoxReqsRegistry instance that is
//   *  used to determine the sizing of different fields within the instance.
//   *
//   *  The primary use of this struct is to shorten argument lists for various
//   *  functions in this file.
//   */
//  
//  typedef struct SK_BoxReqsRegistrySizing
//  {
//  
//  	/*!
//  	 * The value of the field #req_intf that will soon be set inside of an
//  	 * #SK_BoxReqsRegistry instance
//  	 */
//  
//  	SK_BoxIntf *	reg_intf;
//  
//  
//  	/*!
//  	 * Pointer to the #SK_BoxAllReqs that will soon be used to set #req_arg
//  	 * inside of an #SK_BoxReqsRegistry instace
//  	 */
//  
//  	SK_BoxAllReqs *	reg_arq;
//  }
//  
//  SK_BoxReqsRegistrySizing;
//  
//  
//  
//  
//  //FIXME : As it is currently designed, #SK_BoxReqsRegistry is intended to be an
//  //opaque data-type. Come up with a standard way of marking functions related to
//  //it as public / private in the documentation.
//  
//  
//  /*!
//   *  Reports the amount of static memory that an instance of #SK_BoxReqsRegistry
//   *  uses.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  This function will need to be rewritten as fields are added / removed from
//   *  #SK_BoxReqsRegistry.
//   *
//   *  @param brrs			The #SK_BoxReqsRegistrySizing with information
//   *				about the #SK_BoxReqsRegistry instance
//   *
//   *  @param num_bytes		Where to place the number of bytes
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_inst_stat_mem
//  (
//  	const SK_BoxReqsRegistrySizing *	brrs,
//  
//  	size_t *				num_bytes
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brrs, SlyDr_get (SLY_BAD_ARG));
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, num_bytes, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	//Add together the size of each field of #SK_BoxReqsRegistry
//  
//  	size_t stat_mem = 0;
//  
//  
//  	//Size of #num_rwlock
//  
//  	stat_mem += sizeof (pthread_rwlock_t);
//  
//  
//  	//Size of #num_total_users
//  
//  	stat_mem += sizeof (size_t);
//  
//  
//  	//Size of #num_unused_entries
//  
//  	stat_mem += sizeof (size_t);
//  
//  
//  	//Size of #max_unused_entries
//  
//  	stat_mem += sizeof (size_t);
//  
//  
//  	//Size of #reg_rwlock
//  
//  	stat_mem += sizeof (pthread_rwlock_t);
//  
//  
//  	//Size of #reg_intf
//  
//  	stat_mem += sizeof (SK_BoxIntf *);
//  
//  
//  	//Size of #reg_arq
//  
//  	stat_mem += sizeof (SK_BoxAllReqs);
//  
//  
//  	//Determine the size of #reg_box
//  
//  	SK_BoxReqs box_brq;
//  
//  	SK_BoxReqs_point_to_arq (&box_brq, brrs->reg_arq);
//  
//  
//  	size_t box_stat_mem = 0;
//  
//  	brrs->reg_intf->inst_stat_mem (&box_brq, &box_stat_mem);
//  
//  
//  	stat_mem += box_stat_mem;
//  
//  
//  	//Report the amount of static memory needed for an instance
//  
//  	*num_bytes = stat_mem;
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #num_rwlock in an #SK_BoxReqsRegistry
//   *  instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#num_rwlock
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_offset_num_rwlock
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset = 0;
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #num_total_users in an #SK_BoxReqsRegistry
//   *  instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#num_total_users
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_offset_num_total_users
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset =
//  
//  	sizeof (pthread_rwlock_t);
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #num_unused_entries in an #SK_BoxReqsRegistry
//   *  instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#num_unused_entries
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_offset_num_unused_entries
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset =
//  
//  	sizeof (pthread_rwlock_t)	+
//  	sizeof (size_t);
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #max_unused_entries in an #SK_BoxReqsRegistry
//   *  instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#max_unused_entries
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_offset_max_unused_entries
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset =
//  
//  	sizeof (pthread_rwlock_t)	+
//  	sizeof (size_t)			+
//  	sizeof (size_t);
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #reg_rwlock in an #SK_BoxReqsRegistry
//   *  instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#reg_rwlock
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_offset_reg_rwlock
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset =
//  
//  	sizeof (pthread_rwlock_t)	+
//  	sizeof (size_t)			+
//  	sizeof (size_t)			+
//  	sizeof (size_t);
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #reg_intf in an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#reg_intf
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_offset_reg_intf
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset =
//  
//  	sizeof (pthread_rwlock_t)	+
//  	sizeof (size_t)			+
//  	sizeof (size_t)			+
//  	sizeof (size_t)			+
//  	sizeof (pthread_rwlock_t);
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #reg_arq in an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#reg_arq
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_offset_reg_arq
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset =
//  
//  	sizeof (pthread_rwlock_t)	+
//  	sizeof (size_t)			+
//  	sizeof (size_t)			+
//  	sizeof (size_t)			+
//  	sizeof (pthread_rwlock_t)	+
//  	sizeof (SK_BoxIntf *);
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #reg_box in an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#reg_box
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_offset_reg_box
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset =
//  
//  	sizeof (pthread_rwlock_t)	+
//  	sizeof (size_t)			+
//  	sizeof (size_t)			+
//  	sizeof (size_t)			+
//  	sizeof (pthread_rwlock_t)	+
//  	sizeof (SK_BoxIntf *)		+
//  	sizeof (SK_BoxAllReqs);
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #num_rwlock within an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brr			The #SK_BoxReqsRegisry instance to examine
//   *
//   *  @param ptr			Where to place the pointer for #num_rwlock
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_get_num_rwlock_ptr
//  (
//  	const SK_BoxReqsRegistry *	brr,
//  
//  	pthread_rwlock_t **		ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistry_offset_num_rwlock (&offset);
//  
//  
//  	*ptr = ((pthread_rwlock_t *) (((u8 *) brr) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #num_total_users within an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brr			The #SK_BoxReqsRegisry instance to examine
//   *
//   *  @param ptr			Where to place the pointer for #num_total_users
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_get_num_total_users_ptr
//  (
//  	const SK_BoxReqsRegistry *	brr,
//  
//  	size_t **			ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistry_offset_num_total_users (&offset);
//  
//  
//  	*ptr = ((size_t *) (((u8 *) brr) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #num_unused_entries within an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brr			The #SK_BoxReqsRegisry instance to examine
//   *
//   *  @param ptr			Where to place the pointer for #num_unused_entries
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_get_num_unused_entries_ptr
//  (
//  	const SK_BoxReqsRegistry *	brr,
//  
//  	size_t **			ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistry_offset_num_unused_entries (&offset);
//  
//  
//  	*ptr = ((size_t *) (((u8 *) brr) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #max_unused_entries within an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brr			The #SK_BoxReqsRegisry instance to examine
//   *
//   *  @param ptr			Where to place the pointer for #max_unused_entries
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_get_max_unused_entries_ptr
//  (
//  	const SK_BoxReqsRegistry *	brr,
//  
//  	size_t **			ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistry_offset_max_unused_entries (&offset);
//  
//  
//  	*ptr = ((size_t *) (((u8 *) brr) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #reg_rwlock within an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brr			The #SK_BoxReqsRegisry instance to examine
//   *
//   *  @param ptr			Where to place the pointer for #reg_rwlock
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_get_reg_rwlock_ptr
//  (
//  	const SK_BoxReqsRegistry *	brr,
//  
//  	pthread_rwlock_t **		ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistry_offset_reg_rwlock (&offset);
//  
//  
//  	*ptr = ((pthread_rwlock_t *) (((u8 *) brr) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #reg_intf within an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brr			The #SK_BoxReqsRegisry instance to examine
//   *
//   *  @param ptr			Where to place the pointer for #reg_intf
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_get_reg_intf_ptr
//  (
//  	const SK_BoxReqsRegistry *	brr,
//  
//  	SK_BoxIntf ***			ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistry_offset_reg_intf (&offset);
//  
//  
//  	*ptr = ((SK_BoxIntf **) (((u8 *) brr) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #reg_arq within an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brr			The #SK_BoxReqsRegisry instance to examine
//   *
//   *  @param ptr			Where to place the pointer for #reg_arq
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_get_reg_arq_ptr
//  (
//  	const SK_BoxReqsRegistry *	brr,
//  
//  	SK_BoxAllReqs **		ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistry_offset_reg_arq (&offset);
//  
//  
//  	*ptr = ((SK_BoxAllReqs *) (((u8 *) brr) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #reg_box within an #SK_BoxReqsRegistry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brr			The #SK_BoxReqsRegisry instance to examine
//   *
//   *  @param ptr			Where to place the pointer for #reg_box
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistry_get_reg_box_ptr
//  (
//  	const SK_BoxReqsRegistry *	brr,
//  
//  	SK_Box **			ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistry_offset_reg_box (&offset);
//  
//  
//  	*ptr = ((SK_Box *) (((u8 *) brr) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Reports the amount of static memory that an instance of
//   *  #SK_BoxReqsRegistryEntry uses.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  This function will need to be rewritten as fields are added / removed from
//   *  #SK_BoxReqsRegistryEntry.
//   *
//   *  @param num_bytes		Where to place the number of bytes
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistryEntry_inst_stat_mem
//  (
//  	size_t *	num_bytes
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, num_bytes, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	//Add together the size of each field of #SK_BoxReqsRegistryEntry
//  
//  	size_t stat_mem = 0;
//  
//  
//  	//Size of #rwlock
//  
//  	stat_mem += sizeof (pthread_rwlock_t);
//  
//  
//  	//Size of #num_live_users
//  
//  	stat_mem += sizeof (size_t);
//  
//  
//  	//Size of #arq
//  
//  	stat_mem += sizeof (SK_BoxAllReqs *);
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #rwlock in an #SK_BoxReqsRegistryEntry
//   *  instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#rwlock
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistryEntry_offset_rwlock
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset = 0;
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #num_live_users in an
//   *  #SK_BoxReqsRegistryEntry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#num_live_users
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistryEntry_offset_num_live_users
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset =
//  
//  	sizeof (pthread_rwlock_t);
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets the offset associated with #arq in an #SK_BoxReqsRegistryEntry
//   *  instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param offset		Where to place the offset associated with
//   *  				#arq
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistryEntry_offset_arq
//  (
//  	size_t *	offset
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	*offset =
//  
//  	sizeof (pthread_rwlock_t)	+
//  	sizeof (size_t);
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #rwlock in an #SK_BoxReqsRegistryEntry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brre			The #SK_BoxReqsRegistryEntry instance to
//   *				examine
//   *
//   *  @param ptr			Where to place the pointer for #rwlock
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistryEntry_get_rwlock_ptr
//  (
//  	const SK_BoxReqsRegistryEntry *		brre,
//  
//  	pthread_rwlock_t **			ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brre, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistryEntry_offset_rwlock (&offset);
//  
//  
//  	*ptr = ((pthread_rwlock_t *) (((u8 *) brre) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #num_live_users in an #SK_BoxReqsRegistryEntry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brre			The #SK_BoxReqsRegistryEntry instance to
//   *				examine
//   *
//   *  @param ptr			Where to place the pointer for #num_live_users
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistryEntry_get_num_live_users_ptr
//  (
//  	const SK_BoxReqsRegistryEntry *		brre,
//  
//  	size_t **				ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brre, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistryEntry_offset_num_live_users (&offset);
//  
//  
//  	*ptr = ((size_t *) (((u8 *) brre) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  /*!
//   *  Gets a pointer to #arq in an #SK_BoxReqsRegistryEntry instance.
//   *
//   *  @warning
//   *  This function should stay private to SK_BoxReqsRegistry.c.
//   *
//   *  @param brre			The #SK_BoxReqsRegistryEntry instance to
//   *				examine
//   *
//   *  @param ptr			Where to place the pointer for #arq
//   *
//   *  @return			Standard status code
//   */
//  
//  static SlyDr SK_BoxReqsRegistryEntry_get_arq_ptr
//  (
//  	const SK_BoxReqsRegistryEntry *		brre,
//  
//  	SK_BoxAllReqs ***			ptr
//  )
//  {
//  	//Sanity check on arguments
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brre, SlyDr_get (SLY_BAD_ARG));
//  
//  	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, ptr, SlyDr_get (SLY_BAD_ARG));
//  
//  
//  	size_t offset = 0;
//  
//  	SK_BoxReqsRegistryEntry_offset_arq (&offset);
//  
//  
//  	*ptr = ((SK_BoxAllReqs **) (((u8 *) brre) + offset));
//  
//  
//  	return SlyDr_get (SLY_SUCCESS);
//  }
//  
//  
//  
//  
//  //    /*!
//  //     *  Initializes an #SK_BoxReqsRegistry, using the supplied #reg_intf and
//  //     *  #req_arq.
//  //     *
//  //     *  @warning
//  //     *  If this function is successful, the caller of this function MUST call
//  //     *  SK_BoxReqsRegistry_deinit () on #brr sometime after using this function to
//  //     *  avoid resource leaks.
//  //     *
//  //     *  @warning
//  //     *  This function should stay private to SK_BoxReqsRegistry.c. (Should it?)
//  //     *
//  //     *  @param brr			Pointer to a memory region with the size
//  //     *				reported by
//  //     *				SK_BoxReqsRegistry_inst_stat_mem (). This
//  //     *				memory region will be initialized as an
//  //     *				instance of #SK_BoxReqsRegistry.
//  //     *
//  //     *  @param reg_intf		The #SK_BoxIntf to use in creating the #SK_Box
//  //     *				that will be used to store registry entries
//  //     *
//  //     *  @param reg_brq		The #SK_BoxReqs which will be have its contents
//  //     *				copied into #SK_BoxReqsRegistry, and will be
//  //     *				used to configure the #SK_Box that will be used
//  //     *				to store registry entries
//  //     *
//  //     *  @return			Standard status code
//  //     */
//  //
//  //    static SlySr SK_BoxReqsRegistry_init
//  //    (
//  //    	SK_BoxReqsRegistry *	brr,
//  //
//  //    	const SK_BoxIntf *	reg_intf,
//  //
//  //    	const SK_BoxReqs *	reg_brq
//  //    )
//  //    {
//  //    	//Sanity check on arguments
//  //
//  //    	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlySr_get (SLY_BAD_ARG));
//  //
//  //    	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, reg_intf, SlySr_get (SLY_BAD_ARG));
//  //
//  //    	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, reg_brq, SlySr_get (SLY_BAD_ARG));
//  //
//  //
//  //    	//Copy #reg_intf into #brr
//  //
//  //    	SK_BoxIntf ** brr_reg_intf = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_intf_ptr (brr, &brr_reg_intf);
//  //
//  //    	*brr_reg_intf = (SK_BoxIntf *) reg_intf;
//  //
//  //
//  //    	//Copy #reg_brq into #brr as #reg_arq,
//  //
//  //    	SK_BoxAllReqs * brr_reg_arq = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_arq_ptr (brr, &brr_reg_arq);
//  //
//  //    	brr_reg_arq->stat_r = *(reg_brq->stat_r);
//  //    	brr_reg_arq->stat_i = *(reg_brq->stat_i);
//  //    	brr_reg_arq->dyna_r = *(reg_brq->dyna_r);
//  //    	brr_reg_arq->dyna_i = *(reg_brq->dyna_i);
//  //
//  //
//  //    	//Very importantly #reg_arq must be configured so that #reg_box will
//  //    	//store elements of SK_BoxReqsRegistryEntry's size, and making this
//  //    	//change will require using SK_BoxReqs_prep ()
//  //
//  //    	//FIXME : Is it more appropriate here to return failure if
//  //    	//#reg_brq->stat_r.elem_size is not equal to SK_BoxReqsRegistryEntry's
//  //    	//size instead?
//  //
//  //    	SK_BoxReqs brr_reg_brq =
//  //
//  //    	{
//  //    		.stat_r = &(brr_reg_arq->stat_r),
//  //    		.stat_i = &(brr_reg_arq->stat_i),
//  //    		.dyna_r = &(brr_reg_arq->dyna_r),
//  //    		.dyna_i = &(brr_reg_arq->dyna_i)
//  //    	};
//  //
//  //
//  //    	size_t entry_stat_mem = 0;
//  //
//  //    	SK_BoxReqsRegistryEntry_inst_stat_mem (&entry_stat_mem);
//  //
//  //
//  //    	if (entry_stat_mem != brr_reg_brq.stat_r->elem_size)
//  //    	{
//  //    		brr_reg_arq->stat_r.elem_size = entry_stat_mem;
//  //
//  //    		SK_BoxReqs_prep
//  //    		(
//  //    			&brr_reg_brq,
//  //    			&(brr_reg_arq->stat_r),
//  //    			&(brr_reg_arq->stat_i),
//  //    			&(brr_reg_arq->dyna_r),
//  //    			&(brr_reg_arq->dyna_i)
//  //    		);
//  //    	}
//  //
//  //
//  //    	//Initialize #reg_box
//  //
//  //    	SK_Box * brr_reg_box = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_box_ptr (brr, &brr_reg_box);
//  //
//  //
//  //    	SlySr sr =
//  //
//  //    	reg_intf->init
//  //    	(
//  //    		&brr_reg_brq,
//  //    		brr_reg_box,
//  //    		0,
//  //    		SK_Box_elem_rw_set_zero,
//  //    		NULL
//  //    	);
//  //
//  //
//  //    	if (SLY_SUCCESS != sr.res)
//  //    	{
//  //    		SK_DEBUG_PRINT
//  //    		(
//  //    			SK_BOX_REQS_REGISTRY_L_DEBUG,
//  //    			"Failed to initialize #reg_box, #brr = %p.",
//  //    			brr
//  //    		);
//  //
//  //    		return sr;
//  //    	}
//  //
//  //
//  //    	//Initialize #rwlock.
//  //    	//
//  //    	//Note that for purely safety purposes, #brr_rwlock is set to all 0's
//  //    	//here before being initialized. This is because I don't know if any
//  //    	//pthread implementation would detect a non-zero'd #pthread_rwlock_t as
//  //    	//already initialized (and therefore fail to initialize it).
//  //
//  //    	pthread_rwlock_t * brr_rwlock = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_rwlock_ptr (brr, &brr_rwlock);
//  //
//  //
//  //    	SK_byte_set_all (brr_rwlock, 0, sizeof (pthread_rwlock_t));
//  //
//  //
//  //    	//FIXME : Should attributes be used during the intiialization of
//  //    	//#rwlock? And if so, what IS the best attribute to initialize #rwlock
//  //    	//with? It seems to me that since writes will be rarer than reads,
//  //    	//writes should be preferred.
//  //    	//
//  //    	//pthread_rwlockattr_t brr_rwlock_attr;
//  //
//  //
//  //    	int res = pthread_rwlock_init (brr_rwlock, NULL);
//  //
//  //
//  //    	if (0 != res)
//  //    	{
//  //    		SK_DEBUG_PRINT
//  //    		(
//  //    			SK_BOX_REQS_REGISTRY_L_DEBUG,
//  //    			"Failed to initialize #rwlock, #brr = %p",
//  //    			brr
//  //    		);
//  //
//  //
//  //    		//Deallocate #brr_reg_box before returning
//  //
//  //    		reg_intf->deinit (&brr_reg_brq, brr_reg_box);
//  //
//  //
//  //    		return SlySr_get (SLY_BAD_LIB_CALL);
//  //    	}
//  //
//  //
//  //    	return SlySr_get (SLY_SUCCESS);
//  //    }
//  //
//  //
//  //
//  //
//  //    /*!
//  //     *  Determines the number of entries within an #SK_BoxReqsRegistry instance.
//  //     *
//  //     *  @warning
//  //     *  This version of the function assumes the caller has already obtained the
//  //     *  lock, (brr->rwlock), for either reading or writing.
//  //     *
//  //     *  @warning
//  //     *  This function should stay private to SK_BoxReqsRegistry.c.
//  //     *
//  //     *  @param brr			The #SK_BoxReqsRegistry instance to examine
//  //     *
//  //     *  @param num_entries		Where to place the number of entries
//  //     *
//  //     *  @return			Standard status code
//  //     */
//  //
//  //    SlyDr SK_BoxReqsRegistry_get_num_entries_no_lock
//  //    (
//  //    	const SK_BoxReqsRegistry *	brr,
//  //
//  //    	size_t *			num_entries
//  //    )
//  //    {
//  //    	//Sanity check on arguments
//  //
//  //    	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  //
//  //    	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, num_entries, SlyDr_get (SLY_BAD_ARG));
//  //
//  //
//  //    	//Fetch the necessary fields from #brr needed to interact with
//  //    	//#brr->reg_box
//  //
//  //    	SK_BoxIntf ** reg_intf_ptr = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_intf_ptr (brr, &reg_intf_ptr);
//  //
//  //
//  //    	SK_BoxAllReqs * reg_arq_ptr = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_arq_ptr (brr, &reg_arq_ptr);
//  //
//  //
//  //    	SK_BoxReqs reg_brq;
//  //
//  //    	SK_BoxReqs_point_to_arq (&reg_brq, reg_arq_ptr);
//  //
//  //
//  //    	SK_Box * reg_box_ptr = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_box_ptr (brr, &reg_box_ptr);
//  //
//  //
//  //    	//Determine the number of elements in #brr->reg_box, which stores
//  //    	//the entries
//  //
//  //    	return
//  //
//  //    	(* reg_intf_ptr)->get_num_elems
//  //    	(
//  //    		&reg_brq,
//  //    		reg_box_ptr,
//  //    		num_entries
//  //    	);
//  //    }
//  //
//  //
//  //
//  //
//  //    /*!
//  //     *  Determines the number of live users of a specific entry within an
//  //     *  #SK_BoxReqsRegistry instance.
//  //     *
//  //     *  @warning
//  //     *  This version of the function assumes the caller has already obtained the
//  //     *  lock, (brr->rwlock), for either reading or writing.
//  //     *
//  //     *  @warning
//  //     *  This function should stay private to SK_BoxReqsRegistry.c.
//  //     *
//  //     *  @param brr			The #SK_BoxReqsRegistry instance to examine
//  //     *
//  //     *  @param entry_ind		The index of the entry to examine. This must be
//  //     *  				[0, num_entries - 1], where #num_entries is
//  //     *  				determined using
//  //     *  				SK_BoxReqsRegistry_get_num_entries_no_lock ().
//  //     *
//  //     *  @param num_users		Where to place the number of live users for the
//  //     *				entry with index #entry_id.
//  //     *
//  //     *  @return			Standard status code
//  //     */
//  //
//  //    SlyDr SK_BoxReqsRegistry_get_entry_num_users_no_lock
//  //    (
//  //    	const SK_BoxReqsRegistry *	brr,
//  //
//  //    	size_t				entry_ind,
//  //
//  //    	size_t *			num_users
//  //    )
//  //    {
//  //    	//Sanity check on arguments
//  //
//  //    	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  //
//  //    	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, num_users, SlyDr_get (SLY_BAD_ARG));
//  //
//  //
//  //    	size_t num_entries = 0;
//  //
//  //    	SK_BoxReqsRegistry_get_num_entries_no_lock (brr, &num_entries);
//  //
//  //
//  //    	if (entry_ind >= num_entries)
//  //    	{
//  //    		SK_DEBUG_PRINT
//  //    		(
//  //    			SK_BOX_REQS_REGISTRY_L_DEBUG,
//  //    			"For #brr = %p, #entry_ind = %zu, but num_entries = %zu",
//  //    			brr,
//  //    			entry_ind,
//  //    			num_entries
//  //    		);
//  //
//  //    		return SlyDr_get (SLY_BAD_ARG);
//  //    	}
//  //
//  //
//  //    	//Fetch the necessary fields from #brr needed to interact with
//  //    	//#brr->reg_box
//  //
//  //    	SK_BoxIntf ** reg_intf_ptr = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_intf_ptr (brr, &reg_intf_ptr);
//  //
//  //
//  //    	SK_BoxAllReqs * reg_arq_ptr = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_arq_ptr (brr, &reg_arq_ptr);
//  //
//  //
//  //    	SK_BoxReqs reg_brq;
//  //
//  //    	SK_BoxReqs_point_to_arq (&reg_brq, reg_arq_ptr);
//  //
//  //
//  //    	SK_Box * reg_box_ptr = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_box_ptr (brr, &reg_box_ptr);
//  //
//  //
//  //    	//Fetch the entry associated with #entry_ind
//  //
//  //    	SK_BoxReqsRegistryEntry * entry_ptr = NULL;
//  //
//  //
//  //    	(* reg_intf_ptr)->get_elem_ptr
//  //    	(
//  //    		&reg_brq,
//  //    		reg_box_ptr,
//  //    		entry_ind,
//  //    		(void **) &entry_ptr
//  //    	);
//  //
//  //
//  //    	//Get the number of live users for this entry
//  //
//  //    	size_t * num_users_ptr = NULL;
//  //
//  //    	SK_BoxReqsRegistryEntry_get_num_live_users_ptr
//  //    	(
//  //    		entry_ptr,
//  //    		&num_users_ptr
//  //    	);
//  //
//  //
//  //    	//Save the number of live users
//  //
//  //    	*num_users = *num_users_ptr;
//  //
//  //
//  //    	return SlyDr_get (SLY_SUCCESS);
//  //    }
//  //
//  //
//  //
//  //
//  //    /*!
//  //     *
//  //     */
//  //
//  //
//  //
//  //
//  //    /*!
//  //     *  Performs the main tasks of deinitializing an #SK_BoxReqsRegistry without
//  //     *  obtaining the write-lock. This is meant to be used by the other
//  //     *  deinitialization functions.
//  //     *
//  //     *  Note that this function will NOT deinitialize #rwlock.
//  //     *
//  //     *  @warning
//  //     *  If this function is successful, then #SK_BoxReqsRegistry_deinit () must be
//  //     *  called on #brr sometime after using this function to avoid resource leaks.
//  //     *
//  //     *  @warning
//  //     *  This function should stay private to SK_BoxReqsRegistry.c. (Should it?)
//  //     *
//  //     *  @param brr			Pointer to a previously-initialized
//  //     *  				#SK_BoxReqsRegistry instance that should be
//  //     *  				deinitialized.
//  //     *
//  //     *  				Note, the write-lock for #brr MUST be obtained
//  //     *  				before calling this function.
//  //     *
//  //     *  @return			Standard status code
//  //     */
//  //
//  //    static SlyDr SK_BoxReqsRegistry_deinit_no_lock
//  //    (
//  //    	SK_BoxReqsRegistry *	brr
//  //    )
//  //    {
//  //    	//Sanity check on arguments
//  //
//  //    	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  //
//  //
//  //    	//Since the write-lock is already assumed to be obtained in this
//  //    	//function, this function will simply go ahead and deinitialize
//  //    	//everything but the write-lock itself.
//  //
//  //
//  //    	//Read all of fields necessary to access #reg_box
//  //
//  //    	SK_BoxIntf ** reg_intf = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_intf_ptr (brr, &reg_intf);
//  //
//  //
//  //    	SK_BoxAllReqs * reg_arq = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_arq_ptr (brr, &reg_arq);
//  //
//  //
//  //    	SK_BoxReqs reg_brq;
//  //
//  //    	SK_BoxReqs_point_to_arq (&reg_brq, reg_arq);
//  //
//  //
//  //    	SK_Box * reg_box = NULL;
//  //
//  //    	SK_BoxReqsRegistry_get_reg_box_ptr (brr, &reg_box);
//  //
//  //
//  //    	//Iterate through every entry in #brr->reg_box and deallocate each
//  //    	//#SK_BoxAllReqs associated with each entry
//  //
//  //    	size_t num_entries = 0;
//  //
//  //    	(*reg_intf)->get_num_elems (&reg_brq, reg_box, &num_entries);
//  //
//  //
//  //    	for (size_t entry_sel = 0; entry_sel < num_entries; entry_sel += 1)
//  //    	{
//  //    		//Get this particular entry
//  //
//  //    		SK_BoxReqsRegistryEntry * entry = NULL;
//  //
//  //    		(*reg_intf)->get_elem_ptr (&reg_brq, reg_box, entry_sel, (void **) &entry);
//  //
//  //
//  //    		//FIXME : Should a function SK_BoxReqsRegistryEntry_deinit ()
//  //    		//be used here instead of writing this manually?
//  //
//  //
//  //    		//Determine the number of live users of the entry
//  //
//  //    		size_t * num_live_users = 0;
//  //
//  //    		SK_BoxReqsRegistryEntry_get_num_live_users_ptr (entry, &num_live_users);
//  //
//  //
//  //    		if (*num_live_users > 0)
//  //    		{
//  //    			SK_DEBUG_PRINT
//  //    			(
//  //    				SK_BOX_REQS_REGISTRY_L_DEBUG,
//  //    				"Entry %zu of #brr = %p had %zu live users "
//  //    				"during deinitialization. Either users forgot "
//  //    				"did not correctly deregister their usage of "
//  //    				"#brr, or #brr is being deinitialized at an "
//  //    				"inappropriate time.",
//  //    				entry_sel,
//  //    				brr,
//  //    				*num_live_users
//  //    			);
//  //
//  //    			//TODO : If at a later point function pointers are
//  //    			//saved for the functions that registered their usage
//  //    			//of an #SK_BoxReqsRegistryEntry, this would be a good
//  //    			//place to print those out
//  //    		}
//  //
//  //
//  //    		//Deallocate the #SK_BoxAllReqs that was dynamically allocated
//  //    		//for this entry.
//  //    		//
//  //    		//Note how the #SK_MemReqIntf inside of (reg_brq.stat_r) was
//  //    		//what was used to dynamically allocate the #SK_BoxAllReqs.
//  //
//  //    		SK_BoxAllReqs ** entry_arq_ptr = NULL;
//  //
//  //    		SK_BoxReqsRegistryEntry_get_arq_ptr (entry, &entry_arq_ptr);
//  //
//  //
//  //    		reg_brq.stat_r->mem->mem_free (*entry_arq_ptr);
//  //    	}
//  //
//  //
//  //    	//Now that all of the entries have been been properly deinitialized,
//  //    	//#reg_box itself can be deinitialized
//  //
//  //    	(* reg_intf)->deinit
//  //    	(
//  //    		&reg_brq,
//  //    		reg_box
//  //    	);
//  //
//  //
//  //    	//As stated previously, #rwlock does NOT get deinitialized in this
//  //    	//function, because it is assumed to be in use by an outer
//  //    	//deinitialization function
//  //
//  //
//  //    	return SlyDr_get (SLY_SUCCESS);
//  //    }
//  //
//  //
//  //
//  //
//  //    /*!
//  //     *  Deinitializes an #SK_BoxReqsRegistry instance, freeing the resources
//  //     *  associated with it.
//  //     *
//  //     *  @warning
//  //     *  This deinitialization function MUST be called in a code location where it
//  //     *  is known that nothing else is using the #SK_BoxReqsRegistry instance any
//  //     *  longer.
//  //     *
//  //     *  That is true in general for deinitialization functions, but since the
//  //     *  #SK_BoxReqsRegistry tracks how many users it has for its entries, it can
//  //     *  detect if users are still recorded for the #SK_BoxReqsRegistry instance. In
//  //     *  which case, if #SK_BOX_REQS_REGISTRY_L_DEBUG is true, it will print out
//  //     *  debug messages related to this fact.
//  //     *
//  //     *  To maintain the design pattern that deinitialization functions should not
//  //     *  fail, this WILL deinitialize #SK_BoxReqsRegistry instances that still have
//  //     *  live users recorded.
//  //     *
//  //     *  If there are live users of any of the entries inside of #brr, then it is
//  //     *  likely that pointers to the #SK_AllReq's associated with those entries
//  //     *  exist elsewhere. So, de-allocating the memory associated with the entries
//  //     *  will may cause memory-access errors in that event.
//  //     *
//  //     *  HOWEVER, it is also possible that SK_BoxReqsRegistry_register () was
//  //     *  forgotten to be followed by SK_BoxReqsRegistry_deregister () by some other
//  //     *  set of functions. So, the number of live users may be reported as greater
//  //     *  than 0 even though no entries are actually in use.
//  //     *
//  //     *  (If this design guideline were not followed here, it would be possible for
//  //     *  a missing deregistration to make it impossible to destroy #brr).
//  //     *
//  //     *  That is why this function proceeds with the deinitialization regardlesss.
//  //     *
//  //     *  TODO : Should there be a version of this function
//  //     *  SK_BoxReqsRegistry_deinit_safe () that refuses to deinitialize #brr if it
//  //     *  still has live users? Or would that just encourage users to call
//  //     *  SK_BoxReqsRegistry_deinit_safe () from bad locations? To be clear,
//  //     *  SK_BoxReqsRegistry_deinit () should only be called from a code location
//  //     *  where it can be guaranteed where it no longer be used, by virtue of its
//  //     *  code location.
//  //     *
//  //     *  @warning
//  //     *  This function should stay private to SK_BoxReqsRegistry.c. (Should it?)
//  //     *
//  //     *  @param brr			Pointer to a previously-initialized
//  //     *  				#SK_BoxReqsRegistry instance that should be
//  //     *  				deinitialized.
//  //     *
//  //     *  @return			Standard status code
//  //     */
//  //
//  //    static SlyDr SK_BoxReqsRegistry_deinit
//  //    (
//  //    	SK_BoxReqsRegistry *	brr
//  //    )
//  //    {
//  //    	//Sanity check on arguments
//  //
//  //    	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr, SlyDr_get (SLY_BAD_ARG));
//  //
//  //
//  //    	//Obtain the write-lock
//  //
//  //    	pthread_rwlock_t * rwlock;
//  //
//  //    	SK_BoxReqsRegistry_get_rwlock_ptr (brr, &rwlock);
//  //
//  //
//  //    	int lock_res = pthread_rwlock_wrlock (rwlock);
//  //
//  //
//  //    	if (0 != lock_res)
//  //    	{
//  //    		SK_DEBUG_PRINT
//  //    		(
//  //    			SK_BOX_REQS_REGISTRY_L_DEBUG,
//  //    			"BUG : Failed to lock #rwlock, lock_res = %d. "
//  //    			"This has prevented #brr = %p from being "
//  //    			"deinitialized. This should not be possible.",
//  //    			lock_res,
//  //    			brr
//  //    		);
//  //
//  //    		return SlyDr_get (SLY_BAD_LIB_CALL);
//  //    	}
//  //
//  //
//  //    	//Give a warning if #brr has live users, which would normally indicate
//  //    	//that SK_BoxReqsRegistry_deinit () has been called in the wrong place
//  //
//  //    	if (SK_BOX_REQS_REGISTRY_L_DEBUG)
//  //    	{
//  //    		//FIXME : Check for live users here
//  //    	}
//  //
//  //
//  //    	//Deinitialize everything in #brr EXCEPT for #rwlock, as that is being
//  //    	//used
//  //
//  //    	SK_BoxReqsRegistry_deinit_no_lock (brr);
//  //
//  //
//  //    	//Release the write-lock
//  //
//  //    	lock_res = pthread_rwlock_unlock (rwlock);
//  //
//  //
//  //    	if (0 != lock_res)
//  //    	{
//  //    		SK_DEBUG_PRINT
//  //    		(
//  //    			SK_BOX_REQS_REGISTRY_L_DEBUG,
//  //    			"BUG : Failed to unlock #rwlock, lock_res = %d, brr = %p"
//  //    			"This should not be possible. This function will "
//  //    			"continue and attempt to destroy #rwlock regardless, "
//  //    			"as not much else can be done to resolve this. ",
//  //    			lock_res,
//  //    			brr
//  //    		);
//  //
//  //    		//Note that a return is NOT performed here
//  //    	}
//  //
//  //
//  //    	//Destroy #rwlock
//  //
//  //    	lock_res = pthread_rwlock_destroy (rwlock);
//  //
//  //
//  //    	if (0 != lock_res)
//  //    	{
//  //    		SK_DEBUG_PRINT
//  //    		(
//  //    			SK_BOX_REQS_REGISTRY_L_DEBUG,
//  //    			"BUG : Failed to destroy #rwlock, lock_res = %d, brr = %p. "
//  //    			"This should not be possible. The function will "
//  //    			"continue, as not much else can be done to resolve "
//  //    			"this.",
//  //    			lock_res,
//  //    			brr
//  //    		);
//  //
//  //    		//Note that a return is NOT performed here
//  //    	}
//  //
//  //
//  //    	//Report success
//  //
//  //    	return SlyDr_get (SLY_SUCCESS);
//  //    }
//  //
//  //
//  //
//  //
//  //    /*!
//  //     *  Determines the number of live users of an entry within an
//  //     *  #SK_BoxReqsRegistry instance.
//  //     *
//  //     *  @warning
//  //     *  This
//  //     */
//  //
//  //
//  //
//  //
//  //    //FIXME : Does there need to be an *_opt () version of this function that
//  //    //allows for selecting the values of the fields #reg_intf
//  //
//  //    /*!
//  //     *  Creates an #SK_BoxReqsRegistry.
//  //     *
//  //     *  @warning
//  //     *  SK_BoxReqRegistry_destroy () MUST be used on the created
//  //     *  #SK_BoxReqsRegistry at some point in the future, or a resource leak WILL
//  //     *  occur.
//  //     *
//  //     *  @param brr_ptr		Where to place a pointer to the created
//  //     *  				#SK_BoxReqsRegistry.
//  //     *
//  //     *  @return			Standard status code
//  //     */
//  //
//  //    // SlySr SK_BoxReqsRegistry_create
//  //    // (
//  //    // 	SK_BoxReqsRegistry **	brr_ptr
//  //    // )
//  //    // {
//  //    // 	//Sanity check on arguments
//  //    //
//  //    // 	SK_NULL_RETURN (SK_BOX_REQS_REGISTRY_L_DEBUG, brr_ptr, SlySr_get (SLY_BAD_ARG));
//  //    //
//  //    // }
//  
//  
//  
//  
//  //FIXME : Re-design this data-structure as a specialization of #SK_Regi
//  
//  /*!
//   *  @defgroup SK_BoxReqsRegistry
//   *
//   *  @{
//   *
//   *  Provides a "registry" of unique #SK_BoxReqs, which can be used to configure
//   *  #SK_BoxIntf storage-object instances.
//   *
//   *  It is important to immediately recognize this fact : it is OPTIONAL for
//   *  users / implementers of #SK_BoxIntf to make use of this data structure.
//   *
//   *  So why does this data structure exist? It exists because some #SK_BoxIntf
//   *  implementations may wish to use other #SK_BoxIntf implementations for
//   *  sub-storage-objects, and those sub-storage-objects need to have an
//   *  #SK_BoxReqs stored somewhere.
//   *
//   *  The most obvious solution for sub-storage-objects is to store their
//   *  #SK_BoxReqs within the main storage-object, but this is memory inefficient,
//   *  because typically one #SK_BoxReqs is used for far more than 1 storage
//   *  object instance. However, that method would create an #SK_BoxReqs inside of
//   *  each instance of the main storage-object, even if all those instances need
//   *  equivalent #SK_BoxReqs for the sub-storage-objects.
//   *
//   *  So, to be more efficient, there needs to be a way to store the #SK_BoxReqs
//   *  for sub-storage-objects _somewhere_ without creating duplicates. That is
//   *  what this data structure provides.
//   *
//   *  This should immediately raise some concerns; if multiple storage-object
//   *  instances reference the same #SK_BoxReqs, how can anyone determine when
//   *  they should be removed from memory? The answer is this data structure
//   *  performs reference counting for each #SK_BoxReqs.
//   *
//   *  Another immediate concern is that one thread working on one storage-object
//   *  should NOT block another thread from working on a different storage-object.
//   *  So if both threads need to read / write into this registery, won't that be
//   *  unsafe? The answer is this data structure performs read-write-locking
//   *  internally, and it is safe to be used by any number of threads (except for
//   *  the *_create () and *_destroy () functions). Reads into this registry will
//   *  not block other reads, but writes will block both reads and other writes.
//   *
//   *  Additionally, since the #SK_BoxReqs associated with each entry does not
//   *  move in memory when the entry moves in this registry, only 1 read is
//   *  necessary into this registry to fetch a pointer to an existing #SK_BoxReqs.
//   *
//   *  Perhaps somewhat confusingly, #SK_BoxReqsRegistry itself will contain an
//   *  #SK_BoxIntf storage-object, which is what will be used to store entries.
//   *
//   *
//   *  The memory layout of #SK_BoxReqsRegistry is as follows:
//   *
//   *	-Number of Bits-			-Field-
//   *
//   *	sizeof (pthread_rwlock_t)		num_rwlock
//   *
//   * 	sizeof (size_t)				num_total_users
//   *
//   *	sizeof (size_t)				num_unused_entries
//   *
//   *	sizeof (size_t)				max_unused_entries
//   *
//   *	sizeof (pthread_rwlock_t)		reg_rwlock
//   *
//   *	sizeof (SK_BoxIntf *)			reg_intf
//   *
//   *	sizeof (SK_BoxAllReqs)			reg_arq
//   *
//   *	(reg_intf.inst_stat_size ())		reg_box
//   *
//   *
//   *  FIXME : Is it necessary to re-arrange the above fields at all for
//   *  alignement reasons? Keep in mind that it is generally desirable to keep the
//   *  variable-length field(s) at the end (i.e. #reg_box) so that position of
//   *  other fields is easier to calculate.
//   *
//   *
//   *
//   *
//   *  @var num_rwlock		A read-write lock associated with
//   *				#num_total_users, #num_unused_entries, and
//   *				#max_unused_entries.
//   *
//   *				If any read / write occurs for these fields at
//   *				all, then this lock must be obtained (for
//   *				reading or writing, whichever is appropriate).
//   *
//   *				FIXME : Is there any way #rwlock can be
//   *				implemented with only OpenMP so that 2
//   *				different multi-threading libraries are not
//   *				used? It does not appear that OpenMP provides
//   *				read-write locks, though making one out of what
//   *				it provides seems very possible.
//   *
//   *  @var num_total_users	The total number of users who are making use of
//   *				an entry within #reg_box.
//   *
//   * 				This should remain equal to the value of
//   * 				#num_live_users within each entry added
//   * 				together.
//   *
//   *  @var max_unused_entries	The number of entries in #reg_box that are
//   *				allowed have 0 users before those unused
//   *				entries are removed.
//   *
//   *				This makes it so that if one entry constantly
//   *				flickers between having 0 and 1 user, it does
//   *				not cause the entry to be repeatedly removed
//   *				and added, which is wasteful.
//   *
//   *				If this is 0, then unused entries will be
//   *				removed immediately after they occur.
//   *
//   *  @var num_unused_entries	The number of entries in #reg_box that
//   *				currently have 0 users.
//   *
//   *  @var reg_rwlock		A read-write lock associated with #reg_intf,
//   *				#reg_arq, #reg_box.
//   *
//   *				If any read / write occurs for these fields at
//   *				all, then this lock must be obtained (for
//   *				reading or writing, whichever is appropriate).
//   *
//   *				Note that each entry within #reg_box has its
//   *				own lock, so this lock does NOT mean that
//   *				entries within #reg_box will not be modified.
//   *
//   *				However, this lock does mean that no operation
//   *				that invalidates anchors / element pointers
//   *				will be performed on #reg_box.
//   *
//   *  @var reg_intf		The selected #SK_BoxIntf that should be used to
//   *				access #reg_box.
//   *
//   *				@warning
//   *				If this #SK_BoxReqsRegistry instance is being
//   *				used by an #SK_BoxIntf implementation, it
//   *				should NOT provide a pointer to its own
//   *				#SK_BoxIntf as this value. That will create a
//   *				circular dependency that can never be
//   *				satisfied.
//   *
//   *				The easiest way to avoid problems here is to
//   *				just use an #SK_BoxIntf that reports via
//   *				(SK_BoxIntf.need_brq_reg ()) that it does not
//   *				utilize #SK_BoxReqsRegistry at all.
//   *
//   *				In other words, if in doubt, this value should
//   *				be set to #SK_Array_bif.
//   *
//   *  @var reg_arq		An #SK_BoxAllReqs containing the fields
//   *				necessary to generate the #SK_BoxReqs
//   *				associated with #reg_box.
//   *
//   *  @var reg_box		A storage-object that will store entries
//   *  				(of type #SK_BoxReqsRegistryEntry). These
//   *  				entries will provide pointers to usable
//   *  				#SK_BoxReqs, and also keep track of how many
//   *  				users of a particular #SK_BoxReqs are alive.
//   *
//   *  				This will be a storage-object that is
//   *  				initialized via #reg_intf, and configured
//   *				according to #reg_arq.
//   *
//   *  @warning
//   *  Since #SK_BoxReqsRegistry is a "struct-less" data structure, similar to
//   *  #SK_Box, it may be tempting to create separate *_StatReqs, *_StatImpl,
//   *  *_DynaReqs, *_DynaImpl, and *_Reqs structs to configure #SK_BoxReqsRegistry
//   *  instances in the same vein that #SK_Box instances get configured. This
//   *  *_Reqs struct would then need to be referenced each time an instance of
//   *  #SK_BoxReqsRegistry is accessed.
//   *
//   *  While doing so would work, it would require the configuration structs for
//   *  #SK_BoxReqsRegistry to be stored inside the respective configuration
//   *  structs for #SK_Box alongside #SK_BoxReqs.stat_r->brq_reg. Since part
//   *  of the configuration of #SK_BoxReqsRegistry includes providing an
//   *  #SK_BoxReqs for #reg_box, this creates somewhat of a circular dependency.
//   *  So, some method of breaking away from this infinite nesting would be
//   *  necessary.
//   *
//   *  Additionally, storing a configuration for #SK_BoxReqsRegistry inside of
//   *  #SK_BoxAllReqs / #SK_BoxReqs would further bloat it. Although, this reason
//   *  is very weak, because #SK_BoxReqsRegistry makes the fact that #SK_BoxReqs
//   *  is "heavy" substantially less important.
//   *
//   *  The current method of storing the configuration of #SK_BoxReqsRegistry
//   *  entirely within each instance is considered superior for the time being,
//   *  though this decision may change later.
//   *
//   *  Keep in mind that #SK_BoxReqsRegistry is a somewhat special data structure.
//   *  It is referenced inside of #SK_BoxStatReqs, yet it itself uses an #SK_Box
//   *  as a storage primitive. So, these design considerations will almost
//   *  certainly be irrelevant for other users of #SK_BoxIntf.
//   *
//   *  @}
//   */
//  
//  
//  
//  
//  /*!
//   *  @defgroup SK_BoxReqsRegistryEntry
//   *
//   *  @{
//   *
//   *  Represents an individual entry inside of #SK_BoxReqsRegistry.
//   *
//   *  Each unique #SK_BoxReqs stored in an #SK_BoxReqsRegistry instance should
//   *  have its own entry.
//   *
//   *
//   *  The memory layout of #SK_BoxReqsRegistryEntry is as follows:
//   *
//   *
//   *  	--Number of Bits-		-Field-
//   *
//   *	sizeof (pthread_rwlock_t)	rwlock
//   *
//   * 	sizeof (size_t)			num_live_users
//   *
//   * 	FIXME : Add value hash here
//   *
//   * 	FIXME : Change #arq into a generic pointer instead
//   *
//   * 	FIXME : Add a size field, so that each entry can have unique size if
//   * 	needed
//   *
//   *	sizeof (SK_BoxAllReqs *)	arq
//   *
//   *	FIXME : Consider adding a field here to store function pointers to each
//   *	function (or full-fledged backtraces) that registered itself as a user
//   *	of this entry. Why would this be useful? For debugging purposes, it
//   *	would identify which registration was not followed by a
//   *	de-registration.
//   *
//   *
//   *
//   *
//   *  @var num_rwlock		A read-write lock associated with
//   *  				#num_live_users and #arq.
//   *
//   *				If any read / write occurs for these fields at
//   *				all, then this lock must be obtained (for
//   *				reading or writing, whichever is appropriate).
//   *
//   *  @var num_live_users		The number of #SK_BoxIntf storage-object
//   *  				instances that are making use of the
//   *  				#SK_BoxReqs produced via this
//   *  				#SK_BoxReqsRegistryEntry.
//   *
//   *				Every time a new storage-object instance is
//   *				created that makes use the #SK_BoxReqs
//   *				generated from this entry, it should update by
//   *				+1.
//   *
//   *				Every time a storage-object instance is
//   *				destroyed that makes use of the #SK_BoxReqs
//   *				generated from this entry, it should update by
//   *				-1.
//   *
//   *				Once this number reaches 0, that indicates that
//   *				there are no users of this #SK_BoxReqs
//   *				generated from this entry, and so it is safe to
//   *				remove it from the #SK_BoxReqsRegistry that
//   *				contains it.
//   *
//   *				@warning
//   *				Stated in a different way, when this
//   *				value reaches 0, no other object should be in
//   *				possession of the pointer to #arq or a pointer
//   *				to the contents within it.
//   *
//   *  @var arq			Pointer to the #SK_BoxAllReqs which contains
//   *  				all the fields necessary to generate the
//   *  				#SK_BoxReqs associated with this
//   *  				#SK_BoxReqsRegistryEntry.
//   *
//   *				Note that this is a pointer to a dynamically
//   *				allocated #SK_BoxAllReqs, so the #SK_BoxAllReqs
//   *				instance will NOT move in memory even if the
//   *				entry moves within the #SK_BoxReqsRegistry.
//   *
//   *				Therefore, it IS SAFE to store pointers to this
//   *				#SK_BoxAllReqs outside of this entry, so long
//   *				as #num_live_users is properly updated.
//   *
//   *  @}
//   */
//  
//  typedef struct SK_BoxReqsRegistryEntry SK_BoxReqsRegistryEntry;




