/*!****************************************************************************
 *
 * @file
 * SK_BpTree.c
 *
 * A generic B+-Tree data structure that implements #SK_BoxIntf. Like
 * #SK_Array, this attempts to respect as many #SK_BoxReqs hints as possible.
 *
 *****************************************************************************/




#include "SK_BpTree.h"




#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "SK_BoxIntf.h"
#include "SK_debug.h"
#include "SK_MemReqIntf.h"
#include "SK_misc.h"




//FIXME : Once #SK_BoxReqsRegi is finished development and referenced by
//#SK_BoxReqs, restart development on #SK_BpTree. (#SK_BoxReqsRegi is necessary
//to store the #SK_BoxReqs for the sub-storage-objects that are planned for
//#SK_BpTree).




/*!
 *  The default target size (in bytes) of each block of elements in #SK_BpTree
 *  whenever it has not been otherwise specified.
 */

#define SK_BPTREE_DEF_TARGET_BLOCK_SIZE		((size_t) 4096)




// FIXME : Decide whether to keep or remove this section. The recent work
// performed on #SK_BoxReqsRegistry seems like a better direction to head in,
// but much work remains to be done.
//
//
//  //FIXME : Make use of #SK_BoxReqsRegistry here once it is complete.
//  //
//  //Furthermore, is it possible to avoid the use of global variables altogether
//  //by adding this field to #SK_BoxStatReqs:
//  //
//  //	volatile SK_BoxReqsRegistry * hint_reg;
//  //
//  //Would that be a good idea?
//
//
//
//
//  //FIXME : These global variables need to be marked volatile, make sure this is
//  //done correctly
//
//
//
//
//  /*!
//   *  A read-write lock associated with the global variables :
//   *
//   *	SK_BpTree_branch_brqs_arq_init
//   *  	SK_BpTree_branch_brqs_arq
//   *  	SK_BpTree_branch_brqs_init
//   *  	SK_BpTree_branch_brqs
//   *  	SK_BpTree_branch_intf
//   *
//   *  FIXME : Since I want to use OpenMP mainly, it is less "clean" to use
//   *  #pthread_rwlock_t here, but there doesn't appear to be a way to initialize
//   *  global variable locks in OpenMP without calling omp_init_lock ().
//   *
//   *  Every function related to #SK_BoxIntf will probably be spun off into its
//   *  own library, and I don't want there to be an SK_BoxIntf_lib_init ()
//   *  function to initialize the library.
//   *
//   *  So unless OpenMP has a way of directly initializing a global variable lock
//   *  as is done here, I will use #pthread_rwlock_t instead.
//   */
//
//  pthread_rwlock_t SK_BpTree_branch_brqs_rwlock = PTHREAD_RWLOCK_INITIALIZER;
//
//
//
//
//  /*!
//   *  Indicates if #SK_BpTree_branch_brqs_arq is initialized / deinitialized.
//   */
//
//  int SK_BpTree_branch_brqs_arq_init = 0;
//
//
//
//
//  /*!
//   *  The #SK_BoxAllReqs from which the #SK_BoxReqs associated with the storage
//   *  object #SK_BpTree_branch_brqs can be formed.
//   */
//
//  SK_BoxAllReqs SK_BpTree_branch_brqs_arq;
//
//
//
//
//  /*!
//   *  Indicates if #SK_BpTree_branch_brqs is initialized / deinitialized.
//   */
//
//  int SK_BpTree_branch_brqs_init = 0;
//
//
//
//
//  /*!
//   *  A storage object used to store #SK_BpTreeSub's which will be used to
//   *  configure the sub-storage-objects in #SK_BpTree that act as _branches_.
//   */
//
//  void * SK_BpTree_branch_brqs = NULL;
//
//
//
//
//  /*!
//   *  The #SK_BoxIntf that should be used to access #SK_BpTree_branch_brqs.
//   */
//
//  SK_BoxIntf * SK_BpTree_branch_brqs_intf = NULL;
//
//
//
//
//  /*!
//   *  A read-write lock associated with the global variables :
//   *
//   *	SK_BpTree_leaf_brqs_arq_init
//   *  	SK_BpTree_leaf_brqs_arq
//   *  	SK_BpTree_leaf_brqs_init
//   *  	SK_BpTree_leaf_brqs
//   *  	SK_BpTree_leaf_intf
//   *
//   *  FIXME : See the FIXME on #SK_BpTree_leaf_brqs_rwlock
//   */
//
//  pthread_rwlock_t SK_BpTree_leaf_brqs_rwlock = PTHREAD_RWLOCK_INITIALIZER;
//
//
//
//
//  /*!
//   *  Indicates if #SK_BpTree_leaf_brqs_arq is initialized / deinitialized.
//   */
//
//  int SK_BpTree_leaf_brqs_arq_init = 0;
//
//
//
//
//  /*!
//   *  The #SK_BoxAllReqs from which the #SK_BoxReqs associated with the storage
//   *  object #SK_BpTree_leaf_brqs can be formed.
//   */
//
//  SK_BoxAllReqs SK_BpTree_leaf_brqs_arq;
//
//
//
//
//  /*!
//   *  Indicates if #SK_BpTree_leaf_brqs is initialized / deinitialized.
//   */
//
//  int SK_BpTree_leaf_brqs_init = 0;
//
//
//
//
//  /*!
//   *  A storage object used to store #SK_BpTreeSub's which will be used to
//   *  configure the sub-storage-objects in #SK_BpTree that act as _leafes_.
//   */
//
//  void * SK_BpTree_leaf_brqs = NULL;
//
//
//
//
//  /*!
//   *  The #SK_BoxIntf that should be used to access #SK_BpTree_leaf_brqs.
//   */
//
//  SK_BoxIntf * SK_BpTree_leaf_brqs_intf = NULL;
//
//
//
//
//  /*!
//   *  #SK_BpTree will use other #SK_BoxIntf implementations for the sub-storage
//   *  objects contained in it.
//   *
//   *  In order to configure those sub-storage objects correctly, it is necessary
//   *  to have #SK_Box*Reqs's associated with them. However, storing these
//   *  #SK_Box*Reqs's in _each instance_ of every #SK_BpTree storage object would
//   *  be wasteful for memory, because many instances of #SK_BpTree are likely to
//   *  use the same configurations for the sub-storage objects.
//   *
//   *  Therefore, #SK_BpTree will keep a global record of #SK_Box*Reqs's for the
//   *  sub-storage objects inside of #SK_BpTree_branch_brqs and
//   *  #SK_BpTree_leaf_brq. It is only safe to remove #SK_Box*Reqs's from these
//   *  once no sub-storage-object instance makes use of them any longer, and so as
//   *  a result, reference-counting will be used. The reference-counts will be
//   *  updated inside of SK_BpTree_init () and SK_BpTree_deinit ().
//   *
//   *  This data structure is what each element in #SK_BpTree_branch_brqs and
//   *  #SK_BpTree_leaf_brq will contain.
//   */
//
//  typedef struct SK_BpTreeSubReqs
//  {
//
//  	/*!
//  	 *  The number of #SK_BpTree instances that are making use of this
//  	 *  #SK_BpTreeSubReqs for configuring sub-storage-objects.
//  	 *
//  	 *  Every time a new #SK_BpTree instance is created that makes use of
//  	 *  this #SK_BpTreeSubReqs, it should update by +1.
//  	 *
//  	 *  Every time an #SK_BpTree instance is destroyed that makes use of
//  	 *  this #SK_BpTreeSubReqs, it should update by -1.
//  	 *
//  	 *  Once this number reaches 0, that indicates that there are no users
//  	 *  of this #SK_BpTreeSubReqs any longer, and so it is safe to remove
//  	 *  it from
//  	 */
//
//  	size_t	num_live_users;
//
//
//
//
//  	/*!
//  	 *  The ID of this #SK_BpTreeSubReqs within the storage object that
//  	 *  contains it.
//  	 *
//  	 *  THIS value is what #SK_BpTree instances should store in order to
//  	 *  reference #SK_BpTreeSubReqs, NOT pointers to #SK_BpTreeSubReqs.
//  	 *  This is because #SK_BpTreeSubReqs will be contained in global
//  	 *  storage objects in which elements may be added / removed at any
//  	 *  time, so their indices / addresses  will not stay constant.
//  	 *
//  	 *  This value will remain static for the lifetime of the
//  	 *  #SK_BpTreeSubReqs.
//  	 */
//
//  	size_t id;
//
//
//
//
//  	/*!
//  	 *  This #SK_BoxAllReqs contains all the fields necessary to generate
//  	 *  the #SK_BoxReqs associated with the sub-storage-object.
//  	 *
//  	 *  FIXME : Make this a pointer to a dynamically-allocated
//  	 *  #SK_BoxAllReqs, that way the #SK_BoxAllReqs does not change
//  	 *  position in memory when #SK_BpTreeSubReqs changes position in one
//  	 *  of the global storage objects (a mouthful, I know). This makes #id
//  	 *  unneeded.
//  	 */
//
//  	SK_BoxAllReqs * arq;
//
//  }
//
//  SK_BpTreeSubReqs;









/*!
 *  Produces the number of elements per block that #SK_BpTree will use when
 *  encountering a given #brq.
 *
 *  Why not just directly reference (brq->stat_r->hint_elems_per_block)? It may
 *  be 0, in which case, #SK_BpTree is responsible for selecting the value it
 *  believes is best.
 *
 *  @param brq			The #SK_BoxReqs that represents the general
 *				parameters of the #SK_BpTree instance
 *
 *  @param epb			Where to place the number of elements per block
 *				that should be used for the #SK_BpTree instance
 *				described by #brq
 *
 *  @return			Standard status code
 */

SlyDr SK_BpTree_elems_per_block
(
	const SK_BoxReqs *		brq,

	size_t *			epb
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_BPTREE_L_DEBUG, brq, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_BPTREE_L_DEBUG, epb, SlyDr_get (SLY_BAD_ARG));


	if (0 == brq->stat_r->elem_size)
	{
		SK_DEBUG_PRINT
		(
			SK_BPTREE_L_DEBUG,
			"(brq->stat_r->elem_size) is 0, which is not allowed."
		);

		*epb = 0;

		return SlyDr_get (SLY_BAD_ARG);
	}


	//If #hint_elems_per_block is not 0, then we should whatever value it
	//is

	if (0 != brq->stat_r->hint_elems_per_block)
	{
		*epb = brq->stat_r->hint_elems_per_block;

		return SlyDr_get (SLY_SUCCESS);
	}


	//If we reached here, then the number of elements per block can be
	//freely chosen by this #SK_BpTree implementation.

	if (SK_BPTREE_DEF_TARGET_BLOCK_SIZE <= brq->stat_r->elem_size)
	{
		*epb = 1;
	}

	else
	{
		*epb =

		SK_ceil_div_u64
		(
			SK_BPTREE_DEF_TARGET_BLOCK_SIZE,
			brq->stat_r->elem_size
		);
	}

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  A "B-Tree" is a data structure that is organized like an M-degree tree
 *  where nodes are connected via pointers to other nodes. B-tree's are
 *  differentiated from other types of trees based on the rules for how exactly
 *  nodes get added / removed, and for how to select what nodes receive which
 *  values. These rules will soon be discussed.
 *
 *  Typical B-tree implementations do not store elements directly in the
 *  B-tree, but instead store a set of (key, element-pointer) value pairs.
 *  Note that the element-pointers are separate from the pointers that link
 *  together nodes. Also note that the set of (key) values does NOT have to
 *  form a contiguous range in a typical implementation. Ergo, if keys are
 *  unsigned integers and the keys 1 and 100 are found in the B-tree, that does
 *  NOT mean that all the keys in the range [1, 100] are also within the
 *  B-tree.
 *
 *  There are 4 rules that guide the creation of B-trees:
 *
 *  	A)
 *
 *	If a node can have N children (AKA a degree of N), then it must contain
 *	AT LEAST (ceil (N / 2)) keys before the next node is created.
 *
 *	(This rule is in place to prevent the tree from growing taller too
 *	quickly. Other thresholds than just (ceil (N / 2)) can be used.)
 *
 *	B)
 *
 *	The root node is an exception to rule A, and can have 0 children when
 *	there are not enough elements to fill a single node.
 *
 *	C)
 *
 *	All leaf nodes (nodes that do not have children) must be at the same
 *	level of the tree.
 *
 *	D)
 *
 *	The tree is built from bottom-up instead of top-down. So new nodes are
 *	created ABOVE a current node.
 *
 *
 *
 *  A "B+-Tree" is very similar to a B-tree, except for 2-major differences:
 *
 *  	1)
 *
 *	Only the bottom layer of leaf nodes contain (key, element-pointer)
 *	values. The higher level nodes instead contain (key) values.
 *
 *	Additionally, every (key) value MUST have a copy in the bottom layer of
 *	leaf nodes. This means that (key) values CAN be found more than once
 *	throughout the tree. It also means that looking at only the leaf nodes
 *	will tell you all the of the unique keys present in the tree.
 *
 *  	2)
 *
 *  	All the leaf-nodes in the lowest-level of the tree are linked together
 * 	like a linked-list.
 *
 *  Rules 1) and 2) in conjunction with one another allow for element-searching
 *  to be performed like a tree, and allow for linear-indexing to be performed
 *  like a linked-list. A B+-tree consumes slightly more memory than a B-tree,
 *  but element accesses are generally much faster in a B+-tree.
 *
 *
 *
 *
 *  #SK_BpTree is a B+-tree implementation of #SK_BoxIntf with a few major
 *  differences from a conventional B+-tree.
 *
 *	1)
 *
 *	#SK_BpTree WILL contain the elements directly and not just
 *	element-pointers.
 *
 *	This should not be confused with saying "#SK_BpTree can't store
 *	pointers". #SK_BpTree can store any type of element, including
 *	pointers. It can also store MORE than just pointers.
 *
 *	2)
 *
 *	The "keys" for this B+-tree implementation would be the element
 *	indices.
 *
 *	#SK_BoxIntf REQUIRES that all the indices from [0, num_elems - 1] are
 *	present in a storage object that contains (num_elems) number of
 *	elements. So, knowing (num_elems) means that you already know all the
 *	keys present in an #SK_BpTree instance.
 *
 *	Therefore, no keys will be stored in memory for #SK_BpTree; the "key"
 *	associated with each element is known from its position within the tree
 *	and position within the node.
 *
 *	3)
 *
 *	ALL layers of the tree are connected like a linked-list, not just the
 *	lowest-level of leaf nodes which contains elements.
 *
 *	This assists in providing more paths to choose from when accessing one
 *	element after accessing another element. Keep in mind that #SK_BpTree
 *	will support anchors, so being able to shorten the time between element
 *	accesses is very valuable.
 *
 *	4)
 *
 *	All the linked-lists that occur at each layer will be two-way AND
 *	circular. So each node will have a link for the next-node AND
 *	previous-node; and the beginning and ending nodes for each linked-list
 *	will be linked to one another.
 *
 *	Like point 3), this assists in giving more paths to go from one node to
 *	the next node.
 *
 *  #SK_BpTree can essentially be thought of as a linked-list storing elements
 *  normally, except with a massive amount of infrastructure sitting ABOVE the
 *  linked-list so that doing large jumps within the list can be quick. Even
 *  so, the way the tree will grow mimics a normal B+-tree.
 *
 *
 *  (Because of the circular nature of each linked-list, the shape of
 *  #SK_BpTree can be said to be a cone. I'm tempted to rename this data
 *  structure to #SK_ChristmasTree)
 *
 *
 *
 *
 *
 *  (This is implemented as a "struct-less data structure", as described in
 *  "ideas.txt")
 *
 *  @defgroup SK_BpTree
 *
 *  @{
 *
 *  The memory layout of #SK_BpTree is as follows:
 *
 *
 * 	-Number of Bits-			-Field-
 *
 *	u64					debug_anc_hash
 *
 *	sizeof (size_t)				cur_num_elems
 *
 *	sizeof (size_t)				tree_num_elems
 *
 *	FIXME : Is a (tree_num_layers) field necessary here?
 *
 *	SK_BpTreeBranch_inst_stat_mem ()	tree_root
 *
 *	elem_size				stat_elem [0]
 *
 *	elem_size				stat_elem [1]
 *
 *	.
 *	.
 *	.
 *
 *	elem_size				stat_elem [num_stat_elems - 1]
 *
 *  @}
 *
 *  @var debug_anc_hash		A hash value that will be updated every time an
 *				operation is performed on the #SK_BpTree that
 *				invalidates anchors occurs.
 *
 *				So, for example, if a resize operation occurs,
 *				then this hash will be updated.
 *
 *				This is very useful in debugging the usage of
 *				anchors with #SK_BpTree.
 *
 *  @var cur_num_elems		This is used to represent how many elements are
 *				currently being used in the #SK_BpTree. This
 *				WILL count both the statically-allocated
 *				elements and the elements contained in the tree
 *				starting at #tree_root_node.
 *
 *				This is what will be reported as the "number of
 *				elements" inside of the #SK_BpTree.
 *
 *				This CAN be set to values lower than
 *				#num_stat_elems_elems (including 0), which case
 *				the statically-allocated elements will simply
 *				be unused.
 *
 *				It is very important to understand that this
 *				does NOT have to equal (num_stat_elems +
 *				tree_num_elems). Elements can be allocated
 *				before they are actually used in #SK_BpTree.
 *
 *  @var tree_num_elems		The number of elements that are stored in the
 *				B+-tree that begins at #tree_root
 *
 *  @var tree_root		The root-branch of the B+-tree.
 *
 *				See the section #SK_BpTreeBranch to see what
 *				format this will take.
 *
 *  @var stat_elem		An array of statically allocated elements.
 *				These are part of the static memory of the data
 *				structure, so this array can not be resized
 *				during the lifetime of the object.
 *
 *				It is possible for #num_stat_elems to be 0, in
 *				which case, this array will not exist in the
 *				data structure.
 *
 *  @var num_stat_elems		This is a parameter that is received from the
 *				#SK_BoxReqs arguments in the #SK_BoxIntf.
 *
 *				This value is NOT stored in the data structure
 *				itself.
 *
 * @}
 *
 *
 * @defgroup SK_BpTreeBranch
 *
 * @{
 *
 * FIXME : Is it better if each #SK_BpTreeBranch is just a statically-sized
 * array, and the pointers contained within will just be allowed to be NULL as
 * necessary? Keep in mind, the number of pointers in each #SK_BpTreeBranch is
 * unlikely to ever go higher than 16, and will probably be a small value like
 * 4. So while using an #SK_Array may be more memory-efficient, it is unlikely
 * to be more processing-efficient in this particular use case.
 *
 * #SK_BpTreeBranch will be a storage object of whatever #SK_BoxIntf that
 * SK_BpTreeBranch_sel_bif () returns.
 *
 * It will contain (void *) that point to either #SK_BpTreeBranch's in the next
 * "layer" of the B+-tree, or ones that point to SK_BpTreeLeaf's that actually
 * contain elements.
 *
 * @}
 *
 *
 * @defgroup SK_BpTreeLeaf
 *
 * @{
 *
 * FIXME : Is it better if #SK_BpTreeLeaf is a storage object instace of
 * #SK_BoxIntf, or is it better as a separate statically-sized array of
 * size (elem_size * elems_per_block)? Unlike SK_BpTreeBranch, this may
 * actually contain hundreds of elements, so is it worth it?
 *
 * Even so, I am leaning towards #SK_BpTreeBranch and #SK_BpTreeLeaf both being
 * statically-sized arrays.
 *
 * @}
 *
 */




/*!
 *  Gets the offset associated with #debug_anc_hash in an #SK_BpTree.
 *
 *  @param brq			The #SK_BoxReqs that represents the general
 *				parameters of the #SK_BpTree
 *
 *  @param offset		Where to place the offset associated with
 *				#debug_anc_hash
 *
 *  @return			Standard status code
 */

SlyDr SK_BpTree_offset_debug_anc_hash
(
	const SK_BoxReqs *		brq,

	size_t *			offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_BPTREE_L_DEBUG, brq, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_BPTREE_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	*offset = 0;


	return SlyDr_get (SLY_SUCCESS);
}



//Note, this section will be needed inside of SK_BpTree_init () to properly
//intialize the global #SK_BoxReqsRegistry for #SK_BpTree, because multiple
//threads may be trying to access it at once

// SlySr SK_BpTree_init ()
// {
//
// 	//...stuff up here....
//
//
// 	//Here, the global variables #SK_BpTree_branch_brq_reg and
// 	//#SK_BpTree_leaf_brq_reg get initialized.
//
// 	if (0 == SK_BpTree_branch_brq_reg_init)
// 	{
// 		//This lock is needed because multiple threads may be calling
// 		//SK_BpTree_init () at the same time to initialize different
// 		//#SK_BpTree instances, and we don't want multiple ones trying
// 		//to initialize #SK_BpTree_branch_brq_reg at the same time.
//
// 		int res = pthread_rwlock_wrlock (&SK_BpTree_branch_brq_reg_lock);
//
//
// 		//Note how #SK_BpTree_branch_brq_reg_init is checked again,
// 		//this thread is not necessarily the first one to acquire the
// 		//lock
//
// 		if (0 == SK_BpTree_branch_brq_reg_init)
// 		{
// 			SlySr sr = SK_BoxReqsRegistry_init (SK_BpTree_branch_brq_reg);
//
// 			if (SLY_SUCCESS != sr.res)
// 			{
// 				SK_DEBUG_PRINT
// 				(
// 					SK_BPTREE_L_DEBUG,
// 					"Could not initialize #SK_BpTree_branch_brq_reg"
// 				);
//
// 				pthread_rwlock_unlock (&SK_BpTree_branch_brq_reg_lock);
//
// 				return sr;
// 			}
// 		}
//
// 		//FIXME : Does anything else need to be done to ensure the
// 		//write to #SK_BpTree_branch_brq_reg_init occurs at the end of
// 		//this block?
//
// 		SK_BpTree_branch_brq_reg_init = 1;
//
//
// 		pthread_rwlock_unlock (&SK_BpTree_branch_brq_reg_lock);
// 	}
//
//
// 	if (0 == SK_BpTree_leaf_brq_reg_init)
// 	{
// 		//This lock is needed because multiple threads may be calling
// 		//SK_BpTree_init () at the same time to initialize different
// 		//#SK_BpTree instances, and we don't want multiple ones trying
// 		//to initialize #SK_BpTree_leaf_brq_reg at the same time.
//
// 		int res = pthread_rwlock_wrlock (&SK_BpTree_leaf_brq_reg_lock);
//
//
// 		//Note how #SK_BpTree_leaf_brq_reg_init is checked again,
// 		//this thread is not necessarily the first one to acquire the
// 		//lock
//
// 		if (0 == SK_BpTree_leaf_brq_reg_init)
// 		{
// 			SlySr sr = SK_BoxReqsRegistry_init (SK_BpTree_leaf_brq_reg);
//
// 			if (SLY_SUCCESS != sr.res)
// 			{
// 				SK_DEBUG_PRINT
// 				(
// 					SK_BPTREE_L_DEBUG,
// 					"Could not initialize #SK_BpTree_leaf_brq_reg"
// 				);
//
// 				pthread_rwlock_unlock (&SK_BpTree_leaf_brq_reg_lock);
//
// 				return sr;
// 			}
// 		}
//
// 		//FIXME : Does anything else need to be done to ensure the
// 		//write to #SK_BpTree_leaf_brq_reg_init occurs at the end of
// 		//this block?
//
// 		SK_BpTree_leaf_brq_reg_init = 1;
//
//
// 		pthread_rwlock_unlock (&SK_BpTree_leaf_brq_reg_lock);
// 	}
//
//
//
//
// 	//...stuff down here....
//
// }



