/*!****************************************************************************
 *
 * @file
 * SK_misc.c
 *
 * See SK_misc.h for details.
 *
 *****************************************************************************/




#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




#include "SK_debug.h"




#include "SK_misc.h"




/*!
 *  Returns an #SK_ErrnoStr that represents the current value of #errno
 *
 *  EXAMPLE USAGE
 *
 *  @code
 *
 *  printf ("The current value of errno is : %s", SK_ErrnoStr_get (errno, NULL).str);
 *
 *  @endcode
 *
 *
 *  @param errnum		The value of #errno (either the current value
 *				or a saved value)
 *
 *  @param res			A pointer to a #SlyDr value that will hold
 *				the result of this function. If this is not
 *				#SLY_SUCCESS, then the value of #errno was not
 *				successfully retrieved.
 *
 *				This argument CAN be NULL if no error-checking
 *				is desired.
 *
 *  @return			An #SK_ErrnoStr structure representing the value
 *				of #errno
 */

SK_ErrnoStr SK_ErrnoStr_get (int errnum, SlyDr * dr)
{
	SK_ErrnoStr errno_str;


	#if ((_POSIX_C_SOURCE >= 200112L) && !  _GNU_SOURCE)

		int res = strerror_r (errnum, errno_str.str, SK_ERRNO_STR_CHAR_LEN);


		if (NULL != dr)
		{
			if (0 != res)
			{
				*dr = SlyDr_get (SLY_BAD_LIB_CALL);
			}

			else
			{
				*dr = SlyDr_get (SLY_SUCCESS);
			}
		}


	#else
		strerror_r (errnum, errno_str.str, SK_ERRNO_STR_CHAR_LEN);

		if (NULL != dr)
		{
			*dr = SlyDr_get (SLY_SUCCESS);
		}

	#endif


	return errno_str;
}





/*!
 *  Places a value into an arbitrary range by utilizing negaitve-overflow (when
 *  the value is below the range) and positive-overflow (when the value is
 *  above the range).
 *
 *  These examples illustrate the outputs that this function would produce
 *
 *
 *  <b> EXAMPLE 0 <\b>
 *
 *  beg =	-3
 *
 *  end =	+3
 *
 *  val =	[-7, -6, -5, -4, -3, -2, -1, +0, +1, +2, +3, +4, +5, +6, +7, +8]
 *
 *  return =	[+0, +1, +2, +3, -3, -2, -1, +0, +1, +2, +3, -3, -2, -1, +0, +1]
 *
 *
 *  <b> EXAMPLE 1 <\b>
 *
 *  beg =	-5
 *
 *  end =	-2
 *
 *  val =	[-7, -6, -5, -4, -3, -2, -1, +0, +1, +2, +3, +4, +5, +6, +7, +8]
 *
 *  return =	[-3, -2, -5, -4, -3, -2, -5, -4, -3, -2, -5, -4, -3, -2, -5, -4]
 *
 *
 *  <b> EXAMPLE 2 <\b>
 *
 *  beg =	+0
 *
 *  end =	+6
 *
 *  val =	[-7, -6, -5, -4, -3, -2, -1, +0, +1, +2, +3, +4, +5, +6, +7, +8]
 *
 *  return =	[+0, +1, +2, +3, +4, +5, +6, +0, +1, +2, +3, +4, +5, +6, +0, +1]
 *
 *
 *  @param val			The value to place into the range [beg, end].
 *
 *  @param beg			The lowest value of the range
 *
 *  @param end			The highest value of the range
 *
 *  @return			The value of #val placed into range [beg, end]
 *				by overflowing.
 */

i64 SK_range_oflow_i64
(
	i64	val,
	i64	beg,
	i64	end
)
{

	i64 min = SK_MIN (beg, end);

	i64 max = SK_MAX (beg, end);


	u64 len = (max - min) + 1;


	u64 dist = 0;

	if (min > val)
	{
		dist = min - val;

		return max - ((dist - 1) % len);
	}

	else if (max < val)
	{
		dist = val - max;

		return min + ((dist - 1) % len);
	}

	else
	{
		return val;
	}
}




/*!
 *  This implements a way to repeatedly print a string with fprintf (), as it
 *  turns out there are no format strings that can do this correctly.
 *
 *  @warning
 *  This function acknowledges errors from vfprintf (), and will use its return
 *  value to signify this information. However, it is not recommended to act on
 *  these failures unless the printing attempt was important. There's not much
 *  that can be done about failed printing attempts (at least not easily).
 *
 *  @param f_ptr		The #FILE to print to
 *
 *  @param cnt			The number of times to repeat the string
 *
 *  @param f_str		A format string to repeatedly print, following
 *				the same conventions as fprintf ()
 *
 *  @param ...			The variadic arguments referenced by #f_str,
 *				following the same conventions as fprintf ()
 *
 *  @return			Standard status code
 */

SlySr SK_fprintf_cnt
(
	FILE *		f_ptr,
	u32		cnt,
	const char *	f_str,
	...
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_MISC_L_DEBUG, f_str, SlySr_get (SLY_BAD_ARG));


	//Repeatedly print the format string while attempting to detect errors
	//
	//Note that va_start () and va_end () have to be called INSIDE the
	//loop, because vfprintf () invalidates #arg_list after it is used.

	int error_detected = 0;

	for (u32 i = 0; i < cnt; i += 1)
	{

		//Get the #va_list associated with the variadic arguments

		va_list arg_list;

		va_start (arg_list, f_str);


		//Print the format string WITH the variadic arguments

		int res = vfprintf (f_ptr, f_str, arg_list);


		//Make sure #arg_list is properly cleaned up

		va_end (arg_list);


		//Check for errors

		if (0 > res)
		{
			error_detected = 1;
		}
	}


	//Check whether an error needs to be reported or not

	if (error_detected)
	{
		return SlySr_get (SLY_BAD_LIB_CALL);
	}

	else
	{
		return SlySr_get (SLY_SUCCESS);
	}
}




/*!
 *  Generates a "standard" #SK_LineDeco, which is used to decorate lines of
 *  text
 *
 *  @return			An #SK_LineDeco in the "standard form"
 */

SK_LineDeco SK_LineDeco_std ()
{

	SK_LineDeco deco;


	deco.pre_str =		"\t";

	deco.post_str =		"\n";

	deco.pre_str_cnt =	0;

	deco.post_str_cnt =	1;


	return deco;

}




/*!
 *  Prints out a string with decorations specified by a #SK_LineDeco
 *
 *  @warning
 *  This checks for errors originating from vfprintf () in the same way as
 *  fprintf_cnt (). It is not recommended to attempt to check for such an error
 *  unless the printing attempt was important.
 *
 *  @param f_ptr		The #FILE to print to
 *
 *  @param deco			An #SK_LineDeco that describes how to decorate
 *  				what will be printed
 *
 *  @param f_str		A format string to print, following the same
 *				conventions as fprintf ()
 *
 *  @param ...			The variadic argumennts referenced by #f_str,
 *				following the same conventions as fprintf ()
 *
 *  @return			Standard status code
 *
 */

SlySr SK_LineDeco_print_opt
(
	FILE *		f_ptr,
	SK_LineDeco	deco,
	const char *	f_str,
	...
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, f_ptr, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_MISC_L_DEBUG, f_str, SlySr_get (SLY_BAD_ARG));


	//Print the part of the decoration before the line

	int error_detected = 0;

	SlySr sr = SlySr_get (SLY_SUCCESS);

	if (NULL != deco.pre_str)
	{
		sr = SK_fprintf_cnt (f_ptr, deco.pre_str_cnt, "%s", deco.pre_str);
	}


	if (SLY_SUCCESS != sr.res)
	{
		error_detected = 1;
	}


	//Get the #va_list associated with the variadic arguments

	va_list arg_list;

	va_start (arg_list, f_str);


	//Print the message that is being decorated

	int res = vfprintf (f_ptr, f_str, arg_list);


	if (0 > res)
	{
		error_detected = 1;
	}


	//Make sure #arg_list is properly cleaned up

	va_end (arg_list);


	//Print the part of the decoration after the line

	sr = SlySr_get (SLY_SUCCESS);

	if (NULL != deco.post_str)
	{
		sr = SK_fprintf_cnt (f_ptr, deco.post_str_cnt, "%s", deco.post_str);
	}


	if (SLY_SUCCESS != sr.res)
	{
		error_detected = 1;
	}


	//Determine whether an error should be reported or not

	if (error_detected)
	{
		return SlySr_get (SLY_UNKNOWN_ERROR);
	}

	else
	{
		return SlySr_get (SLY_SUCCESS);
	}
}




/*!
 *  Prints out an arbitrary stream of bytes to a given file.
 *
 *  @warning
 *  This will print the highest-indexed byte first.
 *
 *  So for the string of bytes
 *
 *  	(b [0] = 0x00), (b [1] = 0x10), (b [2] = 0x20), (b [3] = 0x30)
 *
 *  the byte sequence will printed out as...
 *
 *  	30201000
 *
 *  @param file			The (FILE *) to print to
 *
 *  @param src			Pointer to the array of bytes
 *
 *  @param size			The number of bytes located at #src.
 *  				This CAN be 0, in which case, nothing will be
 *  				printed.
 *
 *  @return			Standard status code. This WILL return an
 *				error if there is a failure to print the full
 *				byte string.
 */

SlySr SK_byte_print
(
	FILE *		file,
	const void *	src,
	size_t		size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MISC_L_DEBUG, file, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_MISC_L_DEBUG, src, SlySr_get (SLY_BAD_ARG));


	//Don't attempt to print anything if #size is 0

	if (0 >= size)
	{
		return SlySr_get (SLY_SUCCESS);
	}


	//Print out the bytes, starting with the highest-indexed byte first.
	//
	//Note, (size - 1) is guaranteed to not overflow, because #size was
	//checked to see if it was larger than 0 just above.

	size_t high_ind = size - 1;


	for (size_t b_sel = 0; b_sel < size; b_sel += 1)
	{
		int res =

		fprintf
		(
			file,
			"%02x",
			(int) (((u8 *) src) [high_ind - b_sel])
		);

		if (0 > res)
		{
			SK_DEBUG_PRINT
			(
				SK_MISC_L_DEBUG,
				"Failed to print out byte number %zu from the "
				"byte array at %p.",
				b_sel,
				src
			);

			return SlySr_get (SLY_UNKNOWN_ERROR);
		}
	}

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Given a #p_depth value (as it appears in SK_BoxIntf_init_deinit_tb ()),
 *  this produces the #p_depth value to use for an inner-function.
 *
 *  In effect, this means decrementing #p_depth by 1 until it reaches 0, or
 *  leaving it the same if it is already #SK_P_DEPTH_INF.
 *
 *  @param p_depth		The #p_depth value which represents the
 *				printing depth of the caller
 *
 *  @return			The next #p_depth value for a function 1 level
 *				deeper than the caller
 */

i64 SK_p_depth_next
(
	i64	p_depth
)
{
	if (0 == p_depth)
	{
		return 0;
	}

	else if (SK_P_DEPTH_INF >= p_depth)
	{
		return SK_P_DEPTH_INF;
	}

	else
	{
		return (p_depth - 1);
	}
}




/*!
 *  Performs a simple test-bench on SK_inc_chain ()
 *
 *  @return			Standard status code
 *
 */

SlyDr SK_inc_chain_tb ()
{
	printf ("SK_inc_chain_tb () : BEGIN\n\n");

	const int64_t A_MIN = 	+0;
	const int64_t A_MAX = 	+3;
	const int64_t A_INC = 	+1;

	const int64_t B_MIN = 	+0;
	const int64_t B_MAX = 	-3;
	const int64_t B_INC = 	-1;

	const int64_t C_MIN = 	-5;
	const int64_t C_MAX = 	+13;
	const int64_t C_INC = 	+4;

	const int64_t D_MIN = 	+0;
	const int64_t D_MAX = 	+3;
	const int64_t D_INC = 	+1;

	int64_t real_a =	A_MIN;
	int64_t real_b =	B_MIN;
	int64_t real_c =	C_MIN;
	int64_t real_d =	D_MIN;

	int64_t fake_a =	A_MIN;
	int64_t fake_b =	B_MIN;
	int64_t fake_c =	C_MIN;
	int64_t fake_d =	D_MIN;


	//Using a set of nested for-loops, compare the output of
	//SK_inc_chain () to what real for-loops would produce.

	int fail = 0;


	for (real_a = A_MIN; real_a <= A_MAX; real_a += A_INC)
	{
		for (real_b = B_MIN; real_b >= B_MAX; real_b += B_INC)
		{
			for (real_c = C_MIN; real_c <= C_MAX; real_c += C_INC)
			{
				for (real_d = D_MIN; real_d <= D_MAX; real_d += D_INC)
				{
					printf
					(
					 	"----------\n"
						"real_a = %" PRIi64 ", fake_a = %" PRIi64 "\n"
						"real_b = %" PRIi64 ", fake_b = %" PRIi64 "\n"
						"real_c = %" PRIi64 ", fake_c = %" PRIi64 "\n"
						"real_d = %" PRIi64 ", fake_d = %" PRIi64 "\n"
						"----------\n\n\n",
						real_a, fake_a,
						real_b, fake_b,
						real_c, fake_c,
						real_d, fake_d
					);


					//Check if the simulated loop variables
					//are matching the loop variables used
					//above

					if
					(
						(real_a != fake_a) ||
						(real_b != fake_b) ||
						(real_c != fake_c) ||
						(real_d != fake_d)
					)
					{
						SK_DEBUG_PRINT
						(
							SK_MISC_L_DEBUG,
							"Mismatch detected "
							"between \"real\" and "
							"\"fake\" loop variables"
						);

						fail = 1;

						break;
					}


					//Increment the simulated loop
					//variables

					int looped = 0;

					SK_inc_chain (&fake_d, 1,	D_MIN, D_MAX, D_INC, &looped);
					SK_inc_chain (&fake_c, looped,	C_MIN, C_MAX, C_INC, &looped);
					SK_inc_chain (&fake_b, looped, 	B_MIN, B_MAX, B_INC, &looped);
					SK_inc_chain (&fake_a, looped,	A_MIN, A_MAX, A_INC, &looped);
				}

				if (fail)
				{
					break;
				}
			}

			if (fail)
			{
				break;
			}
		}

		if (fail)
		{
			break;
		}
	}



	//Report the success of the function

	if (fail)
	{
		printf ("\n\nSK_inc_chain_tb () : FAIL\n\n");

		return SlyDr_get (SLY_UNKNOWN_ERROR);
	}


	printf ("\n\nSK_inc_chain_tb () : PASS\n\n");

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test-bench on SK_range_oflow_i64 ()
 *
 *  @warning
 *  The results of this test-bench must be manually verified.
 *
 *  @return			Standard status code
 */

SlyDr SK_range_oflow_i64_tb ()
{
	//Announce the test-bench

	SK_TB_BEG_PRINT (stdout);


	//Prepare an arbitrary set of values to process with
	//SK_range_oflow_i64 ()

	const i64 ABS_MAX_VAL =	+10;

	const size_t VAL_LEN = (2 * ABS_MAX_VAL) + 1;


	i64 val [VAL_LEN];

	for (size_t val_sel = 0; val_sel < VAL_LEN; val_sel += 1)
	{
		val [val_sel] = (-1 * ABS_MAX_VAL) + val_sel;
	}


	//Iterate through different arbitrary ranges to test SK_range_oflow_i64
	//with

	const size_t NUM_TESTS = 16;

	for (size_t test_num = 0; test_num < NUM_TESTS; test_num += 1)
	{

		//Randomly generate the range [beg, end] to use

		u64 rand_val_0 =

		SK_gold_hash_u64 (test_num * 10 + 1) % (2 * ABS_MAX_VAL);


		u64 rand_val_1 =

		SK_gold_hash_u64 (test_num * 10 + 2) % (2 * ABS_MAX_VAL);


		i64 beg =

		SK_MIN (rand_val_0, rand_val_1) - ABS_MAX_VAL;


		i64 end =

		SK_MAX (rand_val_0, rand_val_1) - ABS_MAX_VAL;


		//Print test information

		printf ("---- test #%zu ----\n\n", test_num);

		printf ("beg = %" PRIi64 "\n\n", beg);

		printf ("end = %" PRIi64 "\n\n", end);


		printf ("val = ");

		for (size_t val_sel = 0; val_sel < VAL_LEN; val_sel += 1)
		{
			printf ("%+03" PRIi64 " ", val [val_sel]);
		}

		printf ("\n\n");


		//Attempt to calculate #val "overflowed" into the range
		//[beg, end], while printing out the results

		printf ("out = ");

		for (size_t val_sel = 0; val_sel < VAL_LEN; val_sel += 1)
		{
			i64 out = SK_range_oflow_i64 (val [val_sel], beg, end);

			printf ("%+03" PRIi64 " ", out);
		}

		printf ("\n\n\n");

	}


	//End the test-bench

	SK_TB_END_PRINT
	(
		stdout,
		0,
		SlyDr_get (SLY_UNKNOWN_ERROR),
		SlyDr_get (SLY_SUCCESS)
	);
}




/*!
 *  Performs a simple test-bench on SK_byte_eq ().
 *
 *  @param p_depth		The "printing depth".
 *
 *  				If (p_depth == 0), then this test-bench will
 *  				NOT print out results whatsoever to #file.
 *
 *  				If (p_depth > 0), then this test-bench WILL
 *				print out results to #file.
 *
 *				#p_depth will be decremented for inner
 *				test-benches until it reaches 0.
 *
 *				#SK_P_DEPTH_INF (and every value less than it)
 *				indicates infinite printing depth.
 *
 *  @param file			Pointer to the #FILE to print messages to
 *
 *  @parma deco			An #SK_LineDeco describing how to decorate
 *				printed lines
 *
 *  @return			Standard status code
 *
 */

SlyDr SK_byte_eq_tb
(
	i64		p_depth,
	FILE *		file,
	SK_LineDeco	deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN_FP (SK_MISC_L_DEBUG, stdout, file, SlyDr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (p_depth, file, deco);


	//Determine the parameterizations to test

	const i64 BEG_SIZE =		1;
	const i64 END_SIZE =		128;
	const i64 INC_SIZE =		1;


	i64 size =			BEG_SIZE;


	//Enter the test-loop, in which memory regions of different sizes will
	//be tested for equality

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//Announce the test-iteration

		SK_LineDeco_print_opt
		(
			file,
			deco,
			"Testing #size = %zu...",
			size
		);


		//Make 2 contiguous memory regions that store #size number of
		//bytes

		u8 * bytes_0 = malloc (size);

		u8 * bytes_1 = malloc (size);

		if ((NULL == bytes_0) || (NULL == bytes_1))
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"Failed to allocate #bytes_0 and #bytes_1, "
				"#size = %zu.",
				size
			);

			free (bytes_0);
			free (bytes_1);

			error_detected = 1;

			break;
		}


		//Fill both regions with the same sequence of arbitrary bytes

		for (size_t b_sel = 0; b_sel < size; b_sel += 1)
		{
			bytes_0 [b_sel] = b_sel;

			bytes_1 [b_sel] = b_sel;
		}


		//Ensure that SK_byte_eq () reports that #bytes_0 and #bytes_1
		//are the same

		int eq =		0;

		size_t ineq_ind =	0;


		SlyDr dr =

		SK_byte_eq (bytes_0, size, bytes_1, size, &eq, &ineq_ind);


		if (1 != eq)
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"#eq = %d, when it should be exactly 1.",
				eq
			);

			free (bytes_0);
			free (bytes_1);

			error_detected = 1;

			break;
		}

		if (0 != ineq_ind)
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"#ineq_ind = %zu, when it should be exactly 0.",
				ineq_ind
			);

			free (bytes_0);
			free (bytes_1);

			error_detected = 1;

			break;
		}

		if (SLY_SUCCESS != dr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"dr.res = %s, when it should be #SLY_SUCCESS",
				SlyDrStr_get (dr).str
			);

			free (bytes_0);
			free (bytes_1);

			error_detected = 1;

			break;
		}


		//Alter #bytes_0 so that it differs from #bytes_1, and check if
		//SK_byte_eq () detects this appropriately

		for (size_t b_sel = 0; b_sel < size; b_sel += 1)
		{
			bytes_0 [b_sel] += 1;


			SlyDr dr =

			SK_byte_eq (bytes_0, size, bytes_1, size, &eq, &ineq_ind);


			if (0 != eq)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"#eq = %d, when it should be exactly 0.",
					eq
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}

			if (b_sel != ineq_ind)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"#ineq_ind = %zu, when it should be exactly %zu.",
					ineq_ind,
					b_sel
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}

			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"dr.res = %s, when it should be #SLY_SUCCESS",
					SlyDrStr_get (dr).str
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}


			bytes_0 [b_sel] -= 1;
		}


		//Repeat the same section, except that #bytes_1 is altered and
		//#bytes_0 stays the same.

		for (size_t b_sel = 0; b_sel < size; b_sel += 1)
		{
			bytes_1 [b_sel] += 1;


			SlyDr dr =

			SK_byte_eq (bytes_0, size, bytes_1, size, &eq, &ineq_ind);


			if (0 != eq)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"#eq = %d, when it should be exactly 0.",
					eq
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}

			if (b_sel != ineq_ind)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"#ineq_ind = %zu, when it should be exactly %zu.",
					ineq_ind,
					b_sel
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}

			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"dr.res = %s, when it should be #SLY_SUCCESS",
					SlyDrStr_get (dr).str
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}


			bytes_1 [b_sel] -= 1;
		}


		//Test if #ineq_ind is properly reported when the #len_0 and
		//#len_1 arguments of SK_byte_eq () differ

		if (size > 1)
		{
			//Test for when #len_0 is smaller than #size

			SlyDr dr =

			SK_byte_eq (bytes_0, (size - 1), bytes_1, size, &eq, &ineq_ind);


			if (0 != eq)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"#eq = %d, when it should be exactly 0.",
					eq
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}

			if ((size - 1) != ineq_ind)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"#ineq_ind = %zu, when it should be exactly %zu.",
					ineq_ind,
					(size - 1)
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}

			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"dr.res = %s, when it should be #SLY_SUCCESS",
					SlyDrStr_get (dr).str
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}


			//Test for when #len_1 is smaller than #size

			dr =

			SK_byte_eq (bytes_0, size, bytes_1, (size - 1), &eq, &ineq_ind);


			if (0 != eq)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"#eq = %d, when it should be exactly 0.",
					eq
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}

			if ((size - 1) != ineq_ind)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"#ineq_ind = %zu, when it should be exactly %zu.",
					ineq_ind,
					(size - 1)
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}

			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"dr.res = %s, when it should be #SLY_SUCCESS",
					SlyDrStr_get (dr).str
				);

				free (bytes_0);
				free (bytes_1);

				error_detected = 1;

				break;
			}
		}


		//Free memory allocated in this test iteration

		free (bytes_0);
		free (bytes_1);


		//Increment the test parameters

		int looped = 0;


		SK_inc_chain
		(
			&size,
			1,
			BEG_SIZE,
			END_SIZE,
			INC_SIZE,
			&looped
		);


		if (looped)
		{
			break;
		}
	}


	//Announce the test-bench ending

	SK_TB_END_PRINT_STD
	(
		p_depth,
		file,
		deco,
		error_detected,
		SlyDr_get (SLY_UNKNOWN_ERROR),
		SlyDr_get (SLY_SUCCESS)
	);
}




//TODO : Make the other test-benches in this file follow the same conventions
//as SK_byte_eq_tb () for #p_depth, #file, #deco, etc.




/*!
 *  Performs a simple test-bench on SK_byte_copy () and the SK_byte_copy_b* ()
 *  functions.
 *
 *  @return			Standard status code
 */

SlyDr SK_byte_copy_tb ()
{
	//Announce the test-bench

	SK_TB_BEG_PRINT (stdout);


	//Select some constant test parameters

	const size_t TEST_VALS =	100;

	const size_t NUM_COPY_B_FUNCS = 8;

	const size_t BYTE_ARRAY_SIZE =	20;


	const SlyDr (* COPY_B_FUNCS [8]) (void *, const void *)

	=

	{
		SK_byte_copy_b1,
		SK_byte_copy_b2,
		SK_byte_copy_b3,
		SK_byte_copy_b4,
		SK_byte_copy_b5,
		SK_byte_copy_b6,
		SK_byte_copy_b7,
		SK_byte_copy_b8
	};


	//Declare some arbitrary arrays that will be used as the source /
	//destination arguments for the copy functions

	u8 array_0 [BYTE_ARRAY_SIZE];

	u8 array_1 [BYTE_ARRAY_SIZE];


	//Test all of the hard-coded byte size copying functions
	//(i.e. SK_byte_copy_b8 (), SK_byte_copy_b7 (), etc.)

	int error_detected = 0;


	for (size_t func_sel = 0; func_sel < NUM_COPY_B_FUNCS; func_sel += 1)
	{

		//Propagate errors out of the test loop

		if (error_detected)
		{
			break;
		}


		//Select the current copying function

		SlyDr (* copy_func) (void *, const void *)

		=

		COPY_B_FUNCS [func_sel];


		printf ("Testing SK_byte_copy_b%zu ()\n\n", (func_sel + 1));


		//Use the copying function to copy random byte-sequencees

		for (size_t val_sel = 0; val_sel < TEST_VALS; val_sel += 1)
		{
			//Reset the both byte arrays to be full of 0's

			for (size_t b_sel = 0; b_sel < BYTE_ARRAY_SIZE; b_sel += 1)
			{
				array_0 [b_sel] = 0;
				array_1 [b_sel] = 0;
			}


			//Depending on the number of bytes that the current
			//copying function modifies, make sure that many random
			//bytes are placed in #array_0

			for (size_t b_sel = 0; b_sel <= func_sel; b_sel += 1)
			{
				array_0 [b_sel] =

				(u8) SK_gold_hash_u64 (func_sel + val_sel + b_sel);
			}


			//Copy #func_sel number of bytes from #array_0 to
			//#array_1

			copy_func (array_1, array_0);


			//Print the results

			printf ("array_0 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_0 [b_sel - 1]);
			}

			printf ("\n");


			printf ("array_1 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_1 [b_sel - 1]);
			}

			printf ("\n\n");


			//Ensure that ALL the bytes in the arrays match, not
			//just the copied ones. The purpose of this is to
			//ensure that no extra bytes were copied.

			for (size_t b_sel = 0; b_sel < BYTE_ARRAY_SIZE; b_sel += 1)
			{
				if (array_0 [b_sel] != array_1 [b_sel])
				{
					printf
					(
						"Mismatch at byte %zu\n\n",
						b_sel
					);

					error_detected = 1;

					break;
				}
			}
		}

		printf ("\n");
	}


	//This section tests the generic byte-copying function, SK_byte_copy ()

	for (size_t size_sel = 1; size_sel <= BYTE_ARRAY_SIZE; size_sel += 1)
	{

		//Propagate errors out of the test loop

		if (error_detected)
		{
			break;
		}


		printf
		(
			"Testing SK_byte_copy () when #elem_size = %zu\n\n",
			size_sel
		);


		//Use the copying function to copy random byte-sequencees

		for (size_t val_sel = 0; val_sel < TEST_VALS; val_sel += 1)
		{
			//Reset the both byte arrays to be full of 0's

			for (size_t b_sel = 0; b_sel < BYTE_ARRAY_SIZE; b_sel += 1)
			{
				array_0 [b_sel] = 0;
				array_1 [b_sel] = 0;
			}


			//Depending on the number of bytes that the current
			//copying function modifies, make sure that many random
			//bytes are placed in #array_0

			for (size_t b_sel = 0; b_sel < size_sel; b_sel += 1)
			{
				array_0 [b_sel] =

				(u8) SK_gold_hash_u64 (size_sel + val_sel + b_sel);
			}


			//Copy #size_sel number of bytes from #array_0 to
			//#array_1

			SK_byte_copy (array_1, array_0, size_sel);


			//Print the results

			printf ("array_0 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_0 [b_sel - 1]);
			}

			printf ("\n");


			printf ("array_1 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_1 [b_sel - 1]);
			}

			printf ("\n\n");


			//Ensure that ALL the bytes in the arrays match, not
			//just the copied ones. The purpose of this is to
			//ensure that no extra bytes were copied.

			for (size_t b_sel = 0; b_sel < BYTE_ARRAY_SIZE; b_sel += 1)
			{
				if (array_0 [b_sel] != array_1 [b_sel])
				{
					printf
					(
						"Mismatch at byte %zu\n\n",
						b_sel
					);

					error_detected = 1;

					break;
				}
			}
		}

		printf ("\n");
	}


	//End the test-bench

	SK_TB_END_PRINT
	(
		stdout,
		error_detected,
		SlyDr_get (SLY_UNKNOWN_ERROR),
		SlyDr_get (SLY_SUCCESS)
	);
}




/*!
 *  Performs a simple test-bench on SK_byte_swap () and the SK_byte_swap_b* ()
 *  functions.
 *
 *  @return			Standard status code
 */

SlyDr SK_byte_swap_tb ()
{
	//Announce the test-bench

	SK_TB_BEG_PRINT (stdout);


	//Select some constant test parameters

	const size_t TEST_VALS =	100;

	const size_t NUM_SWAP_B_FUNCS = 8;

	const size_t BYTE_ARRAY_SIZE =	20;


	const SlyDr (* SWAP_B_FUNCS [8]) (void *, void *)

	=

	{
		SK_byte_swap_b1,
		SK_byte_swap_b2,
		SK_byte_swap_b3,
		SK_byte_swap_b4,
		SK_byte_swap_b5,
		SK_byte_swap_b6,
		SK_byte_swap_b7,
		SK_byte_swap_b8
	};


	//Declare some arbitrary arrays that will be used as the source /
	//destination arguments for the swap functions

	u8 array_0 [BYTE_ARRAY_SIZE];

	u8 array_1 [BYTE_ARRAY_SIZE];

	u8 array_2 [BYTE_ARRAY_SIZE];

	u8 array_3 [BYTE_ARRAY_SIZE];


	//Test all of the hard-coded byte size swaping functions
	//(i.e. SK_byte_swap_b8 (), SK_byte_swap_b7 (), etc.)

	int error_detected = 0;


	for (size_t func_sel = 0; func_sel < NUM_SWAP_B_FUNCS; func_sel += 1)
	{

		//Propagate errors out of the test loop

		if (error_detected)
		{
			break;
		}


		//Select the current swaping function

		SlyDr (* swap_func) (void *, void *)

		=

		SWAP_B_FUNCS [func_sel];


		printf ("Testing SK_byte_swap_b%zu ()\n\n", (func_sel + 1));


		//Use the swaping function to swap random byte-sequencees

		for (size_t val_sel = 0; val_sel < TEST_VALS; val_sel += 1)
		{
			//Reset #array_0 and #array_1 to be full of 0's

			for (size_t b_sel = 0; b_sel < BYTE_ARRAY_SIZE; b_sel += 1)
			{
				array_0 [b_sel] = 0;
				array_1 [b_sel] = 0;
			}


			//Depending on the number of bytes that the current
			//swapping function modifies, make sure that many
			//random bytes are placed in #array_0 and #array_1

			for (size_t b_sel = 0; b_sel <= func_sel; b_sel += 1)
			{
				array_0 [b_sel] =

				(u8) SK_gold_hash_u64 (func_sel + val_sel + b_sel);


				array_1 [b_sel] =

				(u8) SK_gold_hash_u64 (func_sel + val_sel + b_sel * 10 + 1);
			}


			//Make it so that #array_2 and #array_3 hold the
			//current state of #array_0 and #array_1 respectively

			SK_byte_copy (array_2, array_0, BYTE_ARRAY_SIZE);
			SK_byte_copy (array_3, array_1, BYTE_ARRAY_SIZE);


			//Swap #func_sel number of bytes from #array_0 to
			//#array_1.
			//
			//If this completes successfully, now #array_0 should
			//match #array_3 and #array_1 should match #array_2.

			swap_func (array_1, array_0);


			//Print the results

			printf ("array_0 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_0 [b_sel - 1]);
			}

			printf ("\n");


			printf ("array_1 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_1 [b_sel - 1]);
			}

			printf ("\n");


			printf ("array_2 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_2 [b_sel - 1]);
			}

			printf ("\n");


			printf ("array_3 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_3 [b_sel - 1]);
			}

			printf ("\n\n");


			//Ensure that ALL the bytes in the swapped arrays match
			//with an eariler version of the arrays. The purpose
			//of this is to ensure that no extra bytes were
			//modified.

			for (size_t b_sel = 0; b_sel < BYTE_ARRAY_SIZE; b_sel += 1)
			{
				if (array_0 [b_sel] != array_3 [b_sel])
				{
					printf
					(
						"Mismatch at byte %zu\n\n",
						b_sel
					);

					error_detected = 1;

					break;
				}

				if (array_1 [b_sel] != array_2 [b_sel])
				{
					printf
					(
						"Mismatch at byte %zu\n\n",
						b_sel
					);

					error_detected = 1;

					break;
				}
			}
		}

		printf ("\n");
	}


	//This section tests the generic byte-swapping function,
	//SK_byte_swap ()

	for (size_t size_sel = 1; size_sel <= BYTE_ARRAY_SIZE; size_sel += 1)
	{

		//Propagate errors out of the test loop

		if (error_detected)
		{
			break;
		}


		printf
		(
			"Testing SK_byte_swap () when #elem_size = %zu\n\n",
			size_sel
		);


		//Use the swaping function to swap random byte-sequencees

		for (size_t val_sel = 0; val_sel < TEST_VALS; val_sel += 1)
		{
			//Reset #array_0 and #array_1 to be full of 0's

			for (size_t b_sel = 0; b_sel < BYTE_ARRAY_SIZE; b_sel += 1)
			{
				array_0 [b_sel] = 0;
				array_1 [b_sel] = 0;
			}


			//Depending on the number of bytes that the current
			//swapping function modifies, make sure that many
			//random bytes are placed in #array_0 and #array_1

			for (size_t b_sel = 0; b_sel < size_sel; b_sel += 1)
			{
				array_0 [b_sel] =

				(u8) SK_gold_hash_u64 (size_sel + val_sel + b_sel);


				array_1 [b_sel] =

				(u8) SK_gold_hash_u64 (size_sel + val_sel + b_sel * 10 + 1);
			}


			//Make it so that #array_2 and #array_3 hold the
			//current state of #array_0 and #array_1 respectively

			SK_byte_copy (array_2, array_0, BYTE_ARRAY_SIZE);
			SK_byte_copy (array_3, array_1, BYTE_ARRAY_SIZE);


			//Swap #size_sel number of bytes from #array_0 to
			//#array_1.
			//
			//If this completes successfully, now #array_0 should
			//match #array_3 and #array_1 should match #array_2.

			SK_byte_swap (array_1, array_0, size_sel);


			//Print the results

			printf ("array_0 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_0 [b_sel - 1]);
			}

			printf ("\n");


			printf ("array_1 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_1 [b_sel - 1]);
			}

			printf ("\n");


			printf ("array_2 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_2 [b_sel - 1]);
			}

			printf ("\n");


			printf ("array_3 [] = 0x");

			for (size_t b_sel = BYTE_ARRAY_SIZE; b_sel > 0; b_sel -= 1)
			{
				printf ("%02x", array_3 [b_sel - 1]);
			}

			printf ("\n\n");


			//Ensure that ALL the bytes in the swapped arrays match
			//with an eariler version of the arrays. The purpose
			//of this is to ensure that no extra bytes were
			//modified.

			for (size_t b_sel = 0; b_sel < BYTE_ARRAY_SIZE; b_sel += 1)
			{
				if (array_0 [b_sel] != array_3 [b_sel])
				{
					printf
					(
						"Mismatch at byte %zu\n\n",
						b_sel
					);

					error_detected = 1;

					break;
				}

				if (array_1 [b_sel] != array_2 [b_sel])
				{
					printf
					(
						"Mismatch at byte %zu\n\n",
						b_sel
					);

					error_detected = 1;

					break;
				}
			}
		}

		printf ("\n");
	}


	//End the test-bench

	SK_TB_END_PRINT
	(
		stdout,
		error_detected,
		SlyDr_get (SLY_UNKNOWN_ERROR),
		SlyDr_get (SLY_SUCCESS)
	);
}




/*!
 *  Performs a simple test-bench on SK_byte_le_cmp () and the
 *  SK_byte_le_cmp_b* () functions.
 *
 *  @param p_depth		See SK_byte_eq_tb ()
 *
 *  @param file			See SK_byte_eq_tb ()
 *
 *  @param deco			See SK_byte_eq_tb ()
 *
 *  @return			Standard status code
 */

SlyDr SK_byte_le_cmp_tb
(
	i64		p_depth,
	FILE *		file,
	SK_LineDeco	deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN_FP (SK_MISC_L_DEBUG, stdout, file, SlyDr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (p_depth, file, deco);


	//Check if the running platform is little-endian or not

	int error_detected = 0;


	if (0 == SK_plat_is_le ())
	{
		SK_DEBUG_PRINT_FP
		(
			p_depth,
			file,
			"This test is not yet implemented for big-endian "
			"systems. As a result, this test-bench will "
			"automatically fail."
		);


		error_detected = 1;
	}


	//Select some constant test parameters

	const size_t TEST_VALS =	100;

	const size_t NUM_CMP_B_FUNCS =	8;

	const size_t ARRAY_U64_LEN =	5;

	size_t ARRAY_U8_LEN =		sizeof (u64) * ARRAY_U64_LEN;


	const SlyDr (* CMP_B_FUNCS [8]) (const void *, const void *, int *)

	=

	{
		SK_byte_le_cmp_b1,
		SK_byte_le_cmp_b2,
		SK_byte_le_cmp_b3,
		SK_byte_le_cmp_b4,
		SK_byte_le_cmp_b5,
		SK_byte_le_cmp_b6,
		SK_byte_le_cmp_b7,
		SK_byte_le_cmp_b8
	};


	//Declare some arbitrary arrays that will be used as the memory regions
	//to compare within the comparison functions

	u64 array_0 [ARRAY_U64_LEN];

	u64 array_1 [ARRAY_U64_LEN];


	//Test all of the hard-coded-size byte comparison functions
	//(i.e. SK_byte_le_cmp_b8 (), SK_byte_le_cmp_b7 (), etc.)

	for (size_t func_sel = 0; func_sel < NUM_CMP_B_FUNCS; func_sel += 1)
	{
		//Propagate errors out of the test loop

		if (error_detected)
		{
			break;
		}


		//Select the current copying function

		SlyDr (* cmp_func) (const void *, const void *, int *)

		=

		CMP_B_FUNCS [func_sel];


		fprintf
		(
			file,
			"Testing SK_byte_le_cmp_b%zu ()\n\n",
			(func_sel + 1)
		);


		//Use the compare function to compare random byte sequences

		for (size_t val_sel = 0; val_sel < TEST_VALS; val_sel += 1)
		{
			//Reset both byte arrays to be full of 0's


			for (size_t b_sel = 0; b_sel < ARRAY_U8_LEN; b_sel += 1)
			{
				((u8 *) array_0) [b_sel] = 0;
				((u8 *) array_1) [b_sel] = 0;
			}


			//Depending on the number of bytes that the current
			//comparison function modifies, make sure that many
			//random bytes are placed in #array_0 and #array_1

			for (size_t b_sel = 0; b_sel <= func_sel; b_sel += 1)
			{
				((u8 *) array_0) [b_sel] =

				(u8) SK_gold_hash_u64 (func_sel + val_sel + b_sel);


				((u8 *) array_1) [b_sel] =

				(u8) SK_gold_hash_u64 (func_sel + val_sel + b_sel + ((u8 *) array_0) [b_sel]);


				//For the sake of testing equality, every 10th
				//value ensure that #array_0 and #array_1 are
				//equal

				const size_t val_sel_mod = 10;

				if (0 == val_sel % val_sel_mod)
				{
					((u8 *) array_1) [b_sel] =

					((u8 *) array_0) [b_sel];
				}
			}


			//Since...
			//
			// 1) The platform has already been tested to be
			// little-endian
			//
			// 2) #array_0 and #array_1 are larger than a #u64
			//
			// 3) All the leading bytes of #array_0 and #array_1
			// have been reset to 0
			//
			// 4) The largest SK_byte_le_cmp_b* () function works
			// on 8 bytes at a time
			//
			//...that means the result of directly comparing the
			//two values as #u64's SHOULD be the same as the result
			//as using the SK_byte_le_cmp_b* () function.

			u64 cast_0 = * (u64 *) array_0;

			u64 cast_1 = * (u64 *) array_1;

			int ideal_res = 0;

			if (cast_0 < cast_1)
			{
				ideal_res = -1;
			}

			if (cast_0 > cast_1)
			{
				ideal_res = +1;
			}


			int res = 0;

			SlyDr dr = cmp_func (array_0, array_1, &res);


			//Print the results

			fprintf (file, "array_0 [] = 0x");

			for (size_t b_sel = ARRAY_U8_LEN; b_sel > 0; b_sel -= 1)
			{
				fprintf (file, "%02x", (int) (((u8 *) array_0) [b_sel - 1]));
			}

			fprintf (file, " = %" PRIu64 "\n", * (u64 *) array_0);


			fprintf (file, "array_1 [] = 0x");

			for (size_t b_sel = ARRAY_U8_LEN; b_sel > 0; b_sel -= 1)
			{
				fprintf (file, "%02x", (int) (((u8 *) array_1) [b_sel - 1]));
			}

			fprintf (file, " = %" PRIu64 "\n", * (u64 *) array_1);


			fprintf
			(
				file,
				"ideal_res = %+d\n"
				"res =       %+d\n"
				"\n\n",
				ideal_res,
				res
			);


			//Detect if the result are wrong or not

			if (ideal_res != res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"res = %d did not match ideal_res = %d",
					res,
					ideal_res
				);

				error_detected = 1;

				break;
			}

			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"dr.res = %s when it should be "
					"#SLY_SUCCESS",
					SlyDrStr_get (dr).str
				);

				error_detected = 1;

				break;
			}
		}
	}


	//This section tests the generic little-endian comparison function,
	//SK_byte_le_cmp ().

	for (size_t size_sel = 1; size_sel < ARRAY_U8_LEN; size_sel += 1)
	{
		//Propagate errors out of the test loop

		if (error_detected)
		{
			break;
		}


		fprintf
		(
			file,
			"Testing SK_byte_le_cmp () when #size_sel = %zu\n\n",
			size_sel
		);


		//Use the compare function to compare random byte sequences

		for (size_t val_sel = 0; val_sel < TEST_VALS; val_sel += 1)
		{
			//Reset both byte arrays to be full of 0's

			size_t ARRAY_U8_LEN = sizeof (u64) * ARRAY_U64_LEN;

			for (size_t b_sel = 0; b_sel < ARRAY_U8_LEN; b_sel += 1)
			{
				((u8 *) array_0) [b_sel] = 0;
				((u8 *) array_1) [b_sel] = 0;
			}


			//Depending on the number of bytes that the current
			//comparison function modifies, make sure that many
			//random bytes are placed in #array_0 and #array_1

			for (size_t b_sel = 0; b_sel < size_sel; b_sel += 1)
			{
				((u8 *) array_0) [b_sel] =

				(u8) SK_gold_hash_u64 (size_sel + val_sel + b_sel);


				((u8 *) array_1) [b_sel] =

				(u8) SK_gold_hash_u64 (size_sel + val_sel + b_sel + ((u8 *) array_0) [b_sel]);


				//For the sake of testing equality, every 10th
				//value ensure that #array_0 and #array_1 are
				//equal

				const size_t val_sel_mod = 10;

				if (0 == val_sel % val_sel_mod)
				{
					((u8 *) array_1) [b_sel] =

					((u8 *) array_0) [b_sel];
				}
			}


			//Since...
			//
			// 1) The platform has already been tested to be
			// little-endian
			//
			// 2) #array_0 and #array_1 have a size that is
			// WHOLE MULTIPLES of a #u64's size
			//
			// 3) All the leading bytes of #array_0 and #array_1
			// have been reset to 0
			//
			//...that means the result of directly comparing the
			//two arrays as a sequence of #u64's SHOULD be the same
			//as the result as using the SK_byte_le_cmp ()
			//function.

			int ideal_res = 0;

			for (size_t u64_sel = ARRAY_U64_LEN; u64_sel > 0; u64_sel -= 1)
			{
				if (array_0 [u64_sel - 1] < array_1 [u64_sel - 1])
				{
					ideal_res = -1;

					break;
				}

				if (array_0 [u64_sel - 1] > array_1 [u64_sel - 1])
				{
					ideal_res = +1;

					break;
				}
			}


			int res = 0;

			SlyDr dr =

			SK_byte_le_cmp (array_0, array_1, size_sel, &res);


			//Print the results

			fprintf (file, "array_0 [] = 0x");

			for (size_t b_sel = ARRAY_U8_LEN; b_sel > 0; b_sel -= 1)
			{
				fprintf (file, "%02x", (int) (((u8 *) array_0) [b_sel - 1]));
			}

			//fprintf (file, " = %" PRIu64 "\n", * (u64 *) array_0);

			fprintf (file, "\n");


			fprintf (file, "array_1 [] = 0x");

			for (size_t b_sel = ARRAY_U8_LEN; b_sel > 0; b_sel -= 1)
			{
				fprintf (file, "%02x", (int) (((u8 *) array_1) [b_sel - 1]));
			}

			//fprintf (file, " = %" PRIu64 "\n", * (u64 *) array_1);

			fprintf (file, "\n");


			fprintf
			(
				file,
				"ideal_res = %+d\n"
				"res =       %+d\n"
				"\n\n",
				ideal_res,
				res
			);


			//Detect if the result are wrong or not

			if (ideal_res != res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"res = %d did not match ideal_res = %d",
					res,
					ideal_res
				);

				error_detected = 1;

				break;
			}

			if (SLY_SUCCESS != dr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"dr.res = %s when it should be "
					"#SLY_SUCCESS",
					SlyDrStr_get (dr).str
				);

				error_detected = 1;

				break;
			}
		}
	}


	//Announce the test-bench ending

	SK_TB_END_PRINT_STD
	(
		p_depth,
		file,
		deco,
		error_detected,
		SlyDr_get (SLY_UNKNOWN_ERROR),
		SlyDr_get (SLY_SUCCESS)
	);
}






