/*!
 *  @file
 *  SK_scratch.c
 *
 *  This file is a "scratchpad" for the project, a place where arbitrary
 *  code-snippets can be placed. Any function that is placed here is liable to
 *  be deleted at any arbitrary time in the future.
 *
 *  Before this file was added to this project, testing / experimentation has
 *  been exclusively done through the use of test-benches. However, this begs
 *  the question, where should code that is too emphemeral to be a test-bench
 *  be placed?
 *
 *  An example of a useful code snippet that is too temporary to be a
 *  test-bench would be code that tests an undocumented part of an external
 *  API, or code that helps calculate some math equation related to a small
 *  design aspect of the project.
 *
 *  Debatebly, this scratchpad should not be added to the git repo because it
 *  is unclean, but there are 2 benefits to doing so:
 *
 *  	1)
 *  		It can be useful to revert the scratchpad to older versions in
 *  		sync with the rest of the project.
 *
 *  	2)
 *  		It is very convenient to have the scratchpad inside of the
 *  		project infrastructure, both for function-visibility and
 *  		compilation reasons.
 */




#include <omp.h>
#include <stdio.h>
#include <stdlib.h>


#include "./SK_debug.h"
#include "./SK_misc.h"




/*!
 *  Used inside of SK_omp_4_5_0_sp () to test what happens when the number of
 *  OpenMP threads is inspected inside of a different function WITHOUT creating
 *  a new parallel region.
 *
 *  @return			Standard status code
 */

SlySr SK_omp_4_5_0_sp_stub_0 ()
{

	//Determine the number of OpenMP threads in this function WITHOUT
	//creating an OpenMP parallel region.

	size_t thread_id =	0;
	size_t num_threads =	0;


	thread_id =	omp_get_thread_num ();

	num_threads =	omp_get_num_threads ();


	SK_DEBUG_PRINT
	(
		1,
		"There are %zu threads in this OpenMP parallel "
		"region, and this is thread #%zu",
		num_threads,
		thread_id
	);


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Used inside of SK_omp_4_5_0_sp () to test what happens when the number of
 *  OpenMP threads is inspected inside of a different function WHILE creating a
 *  new parallel region.
 *
 *  @return			Standard status code
 */

SlySr SK_omp_4_5_0_sp_stub_1 ()
{

	//Determine the number of OpenMP threads in this function WHILE
	//creating an OpenMP parallel region.

	size_t thread_id =	0;
	size_t num_threads =	0;


	#pragma omp parallel default (shared) private (thread_id, num_threads)

	{
		thread_id =	omp_get_thread_num ();

		num_threads =	omp_get_num_threads ();


		SK_DEBUG_PRINT
		(
			1,
			"There are %zu threads in this OpenMP parallel "
			"region, and this is thread #%zu",
			num_threads,
			thread_id
		);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  Uses some of the macros available in the OpenMP 4.5.0 API to see if my
 *  interpretation of the documentation is correct.
 *
 *  "_sp" stands for "scratchpad".
 *
 *  @return			Standard status code
 */

SlySr SK_omp_4_5_0_sp ()
{

	//Create some arbitrary arrays for testing purposes

	size_t a0_len = 10000;
	size_t a1_len = 10000;
	size_t a2_len = 10000;

	f64 * a0 = malloc (sizeof (f64) * a0_len);
	f64 * a1 = malloc (sizeof (f64) * a1_len);
	f64 * a2 = malloc (sizeof (f64) * a2_len);


	if
	(
		(NULL == a0) ||
		(NULL == a1) ||
		(NULL == a2)
	)
	{
		free (a0);
		free (a1);
		free (a2);

		SK_DEBUG_PRINT (1, "Could not allocate #a* arrays.");

		return SlySr_get (SLY_BAD_ALLOC);
	}




	//Try creating an OpenMP parallel region with the #for directive

	#pragma omp parallel for

	for (size_t a_sel = 0; a_sel < a0_len; a_sel += 1)
	{
		a0 [a_sel] = a_sel;

		a1 [a_sel] = 2 * a_sel;

		a2 [a_sel] = 4 * a_sel;


		//Uncomment this to check what the current values of #a0, #a1,
		//and #a2 are.

		printf
		(
			"a0 [%06zu] = %06.0lf, "
			"a1 [%06zu] = %06.0lf, "
			"a2 [%06zu] = %06.0lf\n",
			a_sel,
			a0 [a_sel],
			a_sel,
			a1 [a_sel],
			a_sel,
			a2 [a_sel]
		);


		//Uncomment this to check whether or not #sly-debug prints get
		//mixed together by multiple threads attempting to print

		//SK_DEBUG_PRINT (1, "Will\nthis\nget\nmixed\nup?");


		//Uncomment this to check whether or not regular printf ()
		//calls get mixed together.
		//
		//Whether or not calls to printf () block other calls to
		//printf () is totally up to running platform I believe.

		//printf
		//(
		// 	"Will any print attempts\n"
		//	"here in this function\n"
		//	"get mixed together?\n"
		//);
	}




	//Try getting the max number of threads and the thread limit

	int max_threads =	omp_get_max_threads ();

	int thread_limit =	omp_get_thread_limit ();


	SK_DEBUG_PRINT (1, "max_threads = %d", max_threads);

	SK_DEBUG_PRINT (1, "thread_limit = %d", thread_limit);




	//Try turning on OpenMP nested threads

	int nested = omp_get_nested ();

	SK_DEBUG_PRINT (1, "nested = %d", nested);


	omp_set_nested (1);


	nested = omp_get_nested ();

	SK_DEBUG_PRINT (1, "nested = %d", nested);




	//Try getting the max number of threads and the thread limit (again)

	max_threads =	omp_get_max_threads ();

	thread_limit =	omp_get_thread_limit ();


	SK_DEBUG_PRINT (1, "max_threads = %d", max_threads);

	SK_DEBUG_PRINT (1, "thread_limit = %d", thread_limit);




	//Try using the OpenMP #parallel directive alone

	size_t thread_id =	0;
	size_t num_threads =	0;


	#pragma omp parallel default (shared) private (thread_id, num_threads)

	{
		thread_id =	omp_get_thread_num ();

		num_threads =	omp_get_num_threads ();


		SK_DEBUG_PRINT
		(
			1,
			"There are %zu threads in this OpenMP parallel "
			"region, and this is thread #%zu",
			num_threads,
			thread_id
		);


		//SK_omp_4_5_0_sp_stub_0 ();

		SK_omp_4_5_0_sp_stub_1 ();
	}

	//SK_omp_4_5_0_sp_stub_0 ();

	//SK_omp_4_5_0_sp_stub_1 ();





	//Try making an OpenMP #parallel region that only executes properly
	//with a certain number of threads

	#pragma omp parallel default (shared) private (thread_id, num_threads)

	{
		const size_t NEEDED_NUM_THREADS =	10;

		thread_id =				omp_get_thread_num ();

		num_threads =				omp_get_num_threads ();


		if (NEEDED_NUM_THREADS == num_threads)
		{
			SK_DEBUG_PRINT
			(
				1,
				"GOT %zu threads, thread_id = %zu",
				NEEDED_NUM_THREADS,
				thread_id
			);
		}

		else
		{
			SK_DEBUG_PRINT
			(
				1,
				"Did NOT get %zu threads, thread_id = %zu",
				NEEDED_NUM_THREADS,
				thread_id
			);
		}
	}




	//Free the arrays allocated earlier

	free (a0);
	free (a1);
	free (a2);




	return SlySr_get (SLY_SUCCESS);

}

