/*!****************************************************************************
 *
 * @file
 * SK_HmapStd.c
 *
 * The reference implementation of #SK_HmapIntf.
 *
 *****************************************************************************/




#include "SK_HmapStd.h"




#include <stdio.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "SK_Array.h"
#include "SK_BoxIntf.h"
#include "SK_debug.h"
#include "SK_HmapIntf.h"
#include "SK_MemIntf.h"
#include "SK_misc.h"




//FIXME : Find a way to make multi-threaded element READING thread-safe when
//anchors are supported in #SK_HmapStd. Right now, without proper locking,
//multiple threads will attempt to modify #key_anc and #val_anc at the same
//time. Perhaps make each user of the anchors make their own copy of #key_anc
//and #val_anc before using them? Which raises question, when / what should
//modify #key_anc and #val_anc. Perhaps #SK_BoxIntf should require that all
//anchors contain thread-locks, that way multiple threads can safely use the
//*_w_anc () functions at the same time safely.




/*
 *  @defgroup SK_HmapStd
 *
 *  @{
 *
 *  The memory layout of #SK_HmapStd instances are as follows:
 *
 *
 *  If (key_val_concat == 0) is true:
 *
 *
 *	-Number of Bits-			-Field-
 *
 *	(key_group_intf->inst_stat_mem ())	key_group
 *
 *	(val_group_intf->inst_stat_mem ())	val_group
 *
 *	(key_group_intf->anc_size ())		key_anc		(If anchors supported)
 *
 *	(val_group_intf->anc_size ())		val_anc		(If anchors supported)
 *
 *
 *	The memory layout of elements of #key_group are as follows:
 *
 *		-Number of Bits-			-Field-
 *
 *		(key_size)				key
 *
 *
 *		#key_size is either equal to (key_stat_size) or
 *		(key_bytes_intf->inst_stat_mem ()) depending on if #key_dyna_en
 *		is true
 *
 *
 *	The memory layout of elements of #val_group are as follows:
 *
 *		-Number of Bits-			-Field-
 *
 *		(val_size)				val
 *
 *
 *		#val_size is either equal to (val_stat_size) or
 *		(val_bytes_intf->inst_stat_mem ()) depending on if #val_dyna_en
 *		is true
 *
 *
 *  If (key_val_concat == 1) is true:
 *
 *
 *	-Number of Bits-			-Field-
 *
 *	(key_group_intf->inst_stat_mem ())	kv_group
 *
 *	(key_group_intf->anc_size ())		kv_anc		(If anchors supported)
 *
 *
 *	The memory layout of elements of #kv_group are as follows:
 *
 *		-Number of Bits-			-Field-
 *
 *		(key_size)				key
 *
 *		(val_size)				val
 *
 *
 *		#key_size is either equal to (key_stat_size) or
 *		(key_bytes_intf->inst_stat_mem ()) depending on if #key_dyna_en
 *		is true
 *
 *		#val_size is either equal to (val_stat_size) or
 *		(val_bytes_intf->inst_stat_mem ()) depending on if #val_dyna_en
 *		is true
 *
 *  @}
 */




/*!
 *  See #SK_HmapIntf.inst_stat_mem
 */

SlyDr SK_HmapStd_inst_stat_mem
(
	const SK_HmapReqs *	req,

	size_t *		num_bytes
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, num_bytes, SlyDr_get (SLY_BAD_ARG));


	size_t size = 0;

	if (0 == req->stat_r->key_val_concat)
	{
		size += req->stat_i->key_group_inst_stat_mem;

		size += req->stat_i->val_group_inst_stat_mem;


		if (req->stat_i->key_group_anc_support)
		{
			size += req->stat_i->key_group_anc_size;
		}

		if (req->stat_i->val_group_anc_support)
		{
			size += req->stat_i->key_group_anc_size;
		}
	}

	else
	{
		size += req->stat_i->key_group_inst_stat_mem;


		if (req->stat_i->key_group_anc_support)
		{
			size += req->stat_i->key_group_anc_size;
		}
	}


	*num_bytes = size;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with the #key_group field in an #SK_HmapStd
 *  instance.
 *
 *  @param req			The #SK_HmapReqs that represents general
 *				parameters of the #SK_HmapStd
 *
 *  @param offset		Where to place the offset associated with
 *  				#key_group
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_offset_key_group
(
	const SK_HmapReqs *	req,

	size_t *		offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	if (SK_HMAP_STD_H_DEBUG)
	{
		if (req->stat_r->key_val_concat)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_H_DEBUG,
				"BUG : In the #SK_HmapStd implementation, "
				"this function was called even though"
				"req->stat_r->key_val_concat = %d. This "
				"should be impossible.",
				req->stat_r->key_val_concat
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	*offset = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with the #val_group field in an #SK_HmapStd
 *  instance.
 *
 *  @param req			The #SK_HmapReqs that represents general
 *				parameters of the #SK_HmapStd
 *
 *  @param offset		Where to place the offset associated with
 *  				#val_group
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_offset_val_group
(
	const SK_HmapReqs *	req,

	size_t *		offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	if (SK_HMAP_STD_H_DEBUG)
	{
		if (req->stat_r->key_val_concat)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_H_DEBUG,
				"BUG : In the #SK_HmapStd implementation, "
				"this function was called even though"
				"req->stat_r->key_val_concat = %d. This "
				"should be impossible.",
				req->stat_r->key_val_concat
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	*offset = req->stat_i->key_group_inst_stat_mem;;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with the #key_anc field in an #SK_HmapStd
 *  instance.
 *
 *  @param req			The #SK_HmapReqs that represents general
 *				parameters of the #SK_HmapStd
 *
 *  @param offset		Where to place the offset associated with
 *  				#key_anc
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_offset_key_anc
(
	const SK_HmapReqs *	req,

	size_t *		offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	if (SK_HMAP_STD_H_DEBUG)
	{
		if (req->stat_r->key_val_concat)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_H_DEBUG,
				"BUG : In the #SK_HmapStd implementation, "
				"this function was called even though"
				"req->stat_r->key_val_concat = %d. This "
				"should be impossible.",
				req->stat_r->key_val_concat
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	*offset =

	(req->stat_i->key_group_inst_stat_mem)

	+

	(req->stat_i->val_group_inst_stat_mem);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with the #val_anc field in an #SK_HmapStd
 *  instance.
 *
 *  @param req			The #SK_HmapReqs that represents general
 *				parameters of the #SK_HmapStd
 *
 *  @param offset		Where to place the offset associated with
 *  				#val_anc
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_offset_val_anc
(
	const SK_HmapReqs *	req,

	size_t *		offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	if (SK_HMAP_STD_H_DEBUG)
	{
		if (req->stat_r->key_val_concat)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_H_DEBUG,
				"BUG : In the #SK_HmapStd implementation, "
				"this function was called even though"
				"req->stat_r->key_val_concat = %d. This "
				"should be impossible.",
				req->stat_r->key_val_concat
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	*offset =

	(req->stat_i->key_group_inst_stat_mem)

	+

	(req->stat_i->val_group_inst_stat_mem)

	+

	(req->stat_i->key_group_anc_support ? req->stat_i->key_group_anc_size : 0);


	return SlyDr_get (SLY_SUCCESS);
}




// /*!
//  *  Gets the offset associated with the #key_temp field in an #SK_HmapStd
//  *  instance.
//  *
//  *  @param req			The #SK_HmapReqs that represents general
//  *				parameters of the #SK_HmapStd
//  *
//  *  @param offset		Where to place the offset associated with
//  *  				#key_temp
//  *
//  *  @return			Standard status code
//  */
//
// SlyDr SK_HmapStd_offset_key_temp
// (
// 	const SK_HmapReqs *	req,
//
// 	size_t *		offset
// )
// {
// 	//Sanity check on arguments
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//
//
// 	if (SK_HMAP_STD_H_DEBUG)
// 	{
// 		if (req->stat_r->key_dyna_size_en)
// 		{
// 			SK_DEBUG_PRINT
// 			(
// 				SK_HMAP_STD_H_DEBUG,
// 				"BUG : In the #SK_HmapStd implementation, "
// 				"this function was called even though"
// 				"req->stat_r->key_dyna_size_en = %d. This "
// 				"should be impossible.",
// 				req->stat_r->key_dyna_size_en
// 			);
//
// 			return SlyDr_get (SLY_BAD_ARG);
// 		}
// 	}
//
//
// 	if (0 == req->stat_r->key_val_concat)
// 	{
// 		*offset =
//
// 		(req->stat_i->key_group_inst_stat_mem)
//
// 		+
//
// 		(req->stat_i->val_group_inst_stat_mem)
//
// 		+
//
// 		(req->stat_i->key_group_anc_support ? req->stat_i->key_group_anc_size : 0)
//
// 		+
//
// 		(req->stat_i->val_group_anc_support ? req->stat_i->val_group_anc_size : 0);
// 	}
//
// 	else
// 	{
// 		*offset =
//
// 		(req->stat_i->key_group_inst_stat_mem)
//
// 		+
//
// 		(req->stat_i->key_group_anc_support ? req->stat_i->key_group_anc_size : 0);
// 	}
//
//
// 	return SlyDr_get (SLY_SUCCESS);
// }
//
//
//
//
// /*!
//  *  Gets the offset associated with the #val_temp field in an #SK_HmapStd
//  *  instance.
//  *
//  *  @param req			The #SK_HmapReqs that represents general
//  *				parameters of the #SK_HmapStd
//  *
//  *  @param offset		Where to place the offset associated with
//  *  				#val_temp
//  *
//  *  @return			Standard status code
//  */
//
// SlyDr SK_HmapStd_offset_val_temp
// (
// 	const SK_HmapReqs *	req,
//
// 	size_t *		offset
// )
// {
// 	//Sanity check on arguments
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));
//
//
// 	if (SK_HMAP_STD_H_DEBUG)
// 	{
// 		if (req->stat_r->val_dyna_size_en)
// 		{
// 			SK_DEBUG_PRINT
// 			(
// 				SK_HMAP_STD_H_DEBUG,
// 				"BUG : In the #SK_HmapStd implementation, "
// 				"this function was called even though"
// 				"req->stat_r->val_dyna_size_en = %d. This "
// 				"should be impossible.",
// 				req->stat_r->val_dyna_size_en
// 			);
//
// 			return SlyDr_get (SLY_BAD_ARG);
// 		}
// 	}
//
//
// 	if (0 == req->stat_r->key_val_concat)
// 	{
// 		*offset =
//
// 		(req->stat_i->key_group_inst_stat_mem)
//
// 		+
//
// 		(req->stat_i->val_group_inst_stat_mem)
//
// 		+
//
// 		(req->stat_i->key_group_anc_support ? req->stat_i->key_group_anc_size : 0)
//
// 		+
//
// 		(req->stat_i->val_group_anc_support ? req->stat_i->val_group_anc_size : 0)
//
// 		+
//
// 		(req->stat_r->key_dyna_size_en ? req->stat_i->key_bytes_inst_stat_mem : 0);
// 	}
//
// 	else
// 	{
// 		*offset =
//
// 		(req->stat_i->key_group_inst_stat_mem)
//
// 		+
//
// 		(req->stat_i->key_group_anc_support ? req->stat_i->key_group_anc_size : 0)
//
// 		+
//
// 		(req->stat_r->key_dyna_size_en ? req->stat_i->key_bytes_inst_stat_mem : 0);
// 	}
//
//
// 	return SlyDr_get (SLY_SUCCESS);
// }




/*!
 *  Gets the offset associated with the #kv_group field in an #SK_HmapStd
 *  instance.
 *
 *  @param req			The #SK_HmapReqs that represents general
 *				parameters of the #SK_HmapStd
 *
 *  @param offset		Where to place the offset associated with
 *  				#kv_group
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_offset_kv_group
(
	const SK_HmapReqs *	req,

	size_t *		offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	if (SK_HMAP_STD_H_DEBUG)
	{
		if (0 == req->stat_r->key_val_concat)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_H_DEBUG,
				"BUG : In the #SK_HmapStd implementation, "
				"this function was called even though"
				"req->stat_r->key_val_concat = %d. This "
				"should be impossible.",
				req->stat_r->key_val_concat
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	*offset = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with the #kv_anc field in an #SK_HmapStd
 *  instance.
 *
 *  @param req			The #SK_HmapReqs that represents general
 *				parameters of the #SK_HmapStd
 *
 *  @param offset		Where to place the offset associated with
 *  				#kv_anc
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_offset_kv_anc
(
	const SK_HmapReqs *	req,

	size_t *		offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	if (SK_HMAP_STD_H_DEBUG)
	{
		if (0 == req->stat_r->key_val_concat)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_H_DEBUG,
				"BUG : In the #SK_HmapStd implementation, "
				"this function was called even though"
				"req->stat_r->key_val_concat = %d. This "
				"should be impossible.",
				req->stat_r->key_val_concat
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	*offset = req->stat_i->key_group_inst_stat_mem;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with the #key field within a single element of
 *  #kv_group
 *
 *  @param req			The #SK_HmapReqs that represents general
 *				parameters of the #SK_HmapStd
 *
 *  @param offset		Where to place the offset associated with
 *  				#key
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_offset_kv_elem_key
(
	const SK_HmapReqs *	req,

	size_t *		offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	if (SK_HMAP_STD_H_DEBUG)
	{
		if (0 == req->stat_r->key_val_concat)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_H_DEBUG,
				"BUG : In the #SK_HmapStd implementation, "
				"this function was called even though"
				"req->stat_r->key_val_concat = %d. This "
				"should be impossible.",
				req->stat_r->key_val_concat
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	*offset = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with the #val field within a single element of
 *  #kv_group
 *
 *  @param req			The #SK_HmapReqs that represents general
 *				parameters of the #SK_HmapStd
 *
 *  @param offset		Where to place the offset associated with
 *  				#val
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_offset_kv_elem_val
(
	const SK_HmapReqs *	req,

	size_t *		offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	if (SK_HMAP_STD_H_DEBUG)
	{
		if (0 == req->stat_r->key_val_concat)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_H_DEBUG,
				"BUG : In the #SK_HmapStd implementation, "
				"this function was called even though"
				"req->stat_r->key_val_concat = %d. This "
				"should be impossible.",
				req->stat_r->key_val_concat
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	if (req->stat_r->key_dyna_size_en)
	{
		*offset = req->stat_i->key_bytes_inst_stat_mem;
	}

	else
	{
		*offset = req->stat_r->key_stat_size;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to the #key_group field from an #SK_HmapStd.
 *
 *  @param req			The #SK_HmapReqs that represent general
 *  				parameters of the #SK_HmapStd
 *
 *  @param hms			The #SK_HmapStd to inspect
 *
 *  @param key_group_ptr	Where to place a pointer to the #key_group
 *				field
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_get_key_group_ptr
(
	const SK_HmapReqs *		req,

	const SK_HmapStd *		hms,

	SK_Box **			key_group_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, key_group_ptr, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_HmapStd_offset_key_group (req, &offset);


	*key_group_ptr = (SK_Box *) (((u8 *) hms) + offset);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to the #val_group field from an #SK_HmapStd.
 *
 *  @param req			The #SK_HmapReqs that represent general
 *  				parameters of the #SK_HmapStd
 *
 *  @param hms			The #SK_HmapStd to inspect
 *
 *  @param val_group_ptr	Where to place a pointer to the #val_group
 *				field
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_get_val_group_ptr
(
	const SK_HmapReqs *		req,

	const SK_HmapStd *		hms,

	SK_Box **			val_group_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, val_group_ptr, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_HmapStd_offset_val_group (req, &offset);


	*val_group_ptr = (SK_Box *) (((u8 *) hms) + offset);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to the #key_anc field from an #SK_HmapStd.
 *
 *  @param req			The #SK_HmapReqs that represent general
 *  				parameters of the #SK_HmapStd
 *
 *  @param hms			The #SK_HmapStd to inspect
 *
 *  @param key_anc_ptr		Where to place a pointer to the #key_anc
 *				field
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_get_key_anc_ptr
(
	const SK_HmapReqs *		req,

	const SK_HmapStd *		hms,

	SK_BoxAnc **			key_anc_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, key_anc_ptr, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_HmapStd_offset_key_anc (req, &offset);


	*key_anc_ptr = (SK_BoxAnc *) (((u8 *) hms) + offset);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to the #val_anc field from an #SK_HmapStd.
 *
 *  @param req			The #SK_HmapReqs that represent general
 *  				parameters of the #SK_HmapStd
 *
 *  @param hms			The #SK_HmapStd to inspect
 *
 *  @param val_anc_ptr		Where to place a pointer to the #val_anc
 *				field
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_get_val_anc_ptr
(
	const SK_HmapReqs *		req,

	const SK_HmapStd *		hms,

	SK_BoxAnc **			val_anc_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, val_anc_ptr, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_HmapStd_offset_val_anc (req, &offset);


	*val_anc_ptr = (SK_BoxAnc *) (((u8 *) hms) + offset);


	return SlyDr_get (SLY_SUCCESS);
}




// /*!
//  *  Gets a pointer to the #key_temp field from an #SK_HmapStd.
//  *
//  *  @param req			The #SK_HmapReqs that represent general
//  *  				parameters of the #SK_HmapStd
//  *
//  *  @param hms			The #SK_HmapStd to inspect
//  *
//  *  @param key_temp_ptr		Where to place a pointer to the #key_temp
//  *				field
//  *
//  *  @return			Standard status code
//  */
//
// SlyDr SK_HmapStd_get_key_temp_ptr
// (
// 	const SK_HmapReqs *		req,
//
// 	const SK_HmapStd *		hms,
//
// 	SK_Box **			key_temp_ptr
// )
// {
// 	//Sanity check on arguments
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, key_temp_ptr, SlyDr_get (SLY_BAD_ARG));
//
//
// 	size_t offset = 0;
//
// 	SK_HmapStd_offset_key_temp (req, &offset);
//
//
// 	*key_temp_ptr = (SK_Box *) (((u8 *) hms) + offset);
//
//
// 	return SlyDr_get (SLY_SUCCESS);
// }
//
//
//
//
// /*!
//  *  Gets a pointer to the #val_temp field from an #SK_HmapStd.
//  *
//  *  @param req			The #SK_HmapReqs that represent general
//  *  				parameters of the #SK_HmapStd
//  *
//  *  @param hms			The #SK_HmapStd to inspect
//  *
//  *  @param val_temp_ptr		Where to place a pointer to the #val_temp
//  *				field
//  *
//  *  @return			Standard status code
//  */
//
// SlyDr SK_HmapStd_get_val_temp_ptr
// (
// 	const SK_HmapReqs *		req,
//
// 	const SK_HmapStd *		hms,
//
// 	SK_Box **			val_temp_ptr
// )
// {
// 	//Sanity check on arguments
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, val_temp_ptr, SlyDr_get (SLY_BAD_ARG));
//
//
// 	size_t offset = 0;
//
// 	SK_HmapStd_offset_val_temp (req, &offset);
//
//
// 	*val_temp_ptr = (SK_Box *) (((u8 *) hms) + offset);
//
//
// 	return SlyDr_get (SLY_SUCCESS);
// }




/*!
 *  Gets a pointer to the #kv_group field from an #SK_HmapStd.
 *
 *  @param req			The #SK_HmapReqs that represent general
 *  				parameters of the #SK_HmapStd
 *
 *  @param hms			The #SK_HmapStd to inspect
 *
 *  @param kv_group_ptr		Where to place a pointer to the #kv_group
 *				field
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_get_kv_group_ptr
(
	const SK_HmapReqs *		req,

	const SK_HmapStd *		hms,

	SK_Box **			kv_group_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, kv_group_ptr, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_HmapStd_offset_kv_group (req, &offset);


	*kv_group_ptr = (SK_Box *) (((u8 *) hms) + offset);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to the #kv_anc field from an #SK_HmapStd.
 *
 *  @param req			The #SK_HmapReqs that represent general
 *  				parameters of the #SK_HmapStd
 *
 *  @param hms			The #SK_HmapStd to inspect
 *
 *  @param kv_anc_ptr		Where to place a pointer to the #kv_anc
 *				field
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_get_kv_anc_ptr
(
	const SK_HmapReqs *		req,

	const SK_HmapStd *		hms,

	SK_BoxAnc **			kv_anc_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, kv_anc_ptr, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_HmapStd_offset_kv_anc (req, &offset);


	*kv_anc_ptr = (SK_BoxAnc *) (((u8 *) hms) + offset);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to the #key field from a singular element of #kv_group,
 *  which itself is a field of an #SK_HmapStd instance.
 *
 *  @param req			The #SK_HmapReqs that represent general
 *  				parameters of the #SK_HmapStd
 *
 *  @param kv_elem		Pointer to a singular element from #kv_group
 *  				from an #SK_HmapStd instance
 *
 *  @param key_ptr		Where to place a pointer to the #key
 *				that is within #kv_elem
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_get_kv_elem_key_ptr
(
	const SK_HmapReqs *		req,

	const void *			kv_elem,

	void **				key_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, kv_elem, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, key_ptr, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_HmapStd_offset_kv_elem_key (req, &offset);


	*key_ptr = (void *) (((u8 *) kv_elem) + offset);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to the #val field from a singular element of #kv_group,
 *  which itself is a field of an #SK_HmapStd instance.
 *
 *  @param req			The #SK_HmapReqs that represent general
 *  				parameters of the #SK_HmapStd
 *
 *  @param kv_elem		Pointer to a singular element from #kv_group
 *  				from an #SK_HmapStd instance
 *
 *  @param val_ptr		Where to place a pointer to the #key
 *				that is within #kv_elem
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_get_kv_elem_val_ptr
(
	const SK_HmapReqs *		req,

	const void *			kv_elem,

	void **				val_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, kv_elem, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, val_ptr, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_HmapStd_offset_kv_elem_val (req, &offset);


	*val_ptr = (void *) (((u8 *) kv_elem) + offset);


	return SlyDr_get (SLY_SUCCESS);
}




// FIXME : Is this function a good idea, or is it just unneeded? Note how long
// the call to (req->stat_r->key_group_intf->get_num_elems ()) is.
//
// /*!
//  *  Determines the number of elements inside of #key_group within an
//  *  #SK_HmapStd instance.
//  *
//  *  @param req			The #SK_HmapReqs to use when accessing #hms
//  *
//  *  @param hms			The #SK_HmapStd instance to inspect
//  *
//  *  @param num_elems		Where to place the number of elements of
//  *				#key_group
//  *
//  *  @return			Standard status code
//  */
//
// SlyDr SK_HmapStd_key_group_num_elems
// (
// 	const SK_HmapReqs *		req,
//
// 	const SK_HmapStd *		hms,
//
// 	size_t *			num_elems
// )
// {
// 	//Sanity check on arguments
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
//
// 	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));
//
//
// 	SK_Box * key_group = NULL;
//
// 	SK_HmapStd_get_key_group_ptr (req, hms, &num_elems);
//
//
// 	req->stat_r->key_group_intf->get_num_elems
// 	(
// 		&(req->dyna_i->key_group_req),
// 		key_group,
// 		num_elems
// 	);
//
//
// 	return SlySr_get (SLY_SUCCESS);
// }




/*!
 *  See #SK_HmapIntf.init
 */

SlySr SK_HmapStd_init
(
	const SK_HmapReqs *		req,

	SK_HmapStd *			hms
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlySr_get (SLY_BAD_ARG));


	//Determine if the (key_group, val_group) need to be initialized or if
	//(kv_group) should be initialized

	if (req->stat_r->key_val_concat)
	{
		//Initialize #kv_group

		SK_Box * kv_group = NULL;

		SK_HmapStd_get_kv_group_ptr (req, hms, &kv_group);


		SlySr sr =

		req->stat_r->key_group_intf->init
		(
			&(req->dyna_i->key_group_req),
			kv_group,
			0,
			NULL,
			NULL
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_RES_RETURN (SK_HMAP_STD_L_DEBUG, sr.res, sr);
		}
	}

	else
	{
		//Initialize #key_group

		SK_Box * key_group = NULL;

		SK_HmapStd_get_key_group_ptr (req, hms, &key_group);


		SlySr sr =

		req->stat_r->key_group_intf->init
		(
			&(req->dyna_i->key_group_req),
			key_group,
			0,
			NULL,
			NULL
		);


		if (SLY_SUCCESS != sr.res)
		{
			SK_RES_RETURN (SK_HMAP_STD_L_DEBUG, sr.res, sr);
		}


		//Initialize #val_group

		SK_Box * val_group = NULL;

		SK_HmapStd_get_val_group_ptr (req, hms, &val_group);


		sr =

		req->stat_r->val_group_intf->init
		(
			&(req->dyna_i->val_group_req),
			val_group,
			0,
			NULL,
			NULL
		);


		if (SLY_SUCCESS != sr.res)
		{
			req->stat_r->key_group_intf->deinit
			(
				&(req->dyna_i->key_group_req),
				key_group
			);

			SK_RES_RETURN (SK_HMAP_STD_L_DEBUG, sr.res, sr);
		}
	}


	return SlySr_get (SLY_SUCCESS);
}





/*!
 *  Deinitializes all of the #SK_Box's that are used to store individual keys
 *  within the #key_group field.
 *
 *  This is used within SK_HmapStd_deinit ().
 *
 *  @param req			The #SK_HmapReqs to use when accessing #hms
 *
 *  @param hms			The #SK_HmapStd instance to modify
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_key_group_elem_deinit
(
	const SK_HmapReqs *	req,

	SK_HmapStd *		hms
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));


	if (req->stat_r->key_val_concat)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_STD_L_DEBUG,
			"BUG : This function can not be used when "
			"#key_val_concat = %d",
			req->stat_r->key_val_concat
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	if (req->stat_r->key_dyna_size_en)
	{
		//Store some variables that make accessing #key_group shorter

		const SK_BoxIntf * key_group_intf =

		req->stat_r->key_group_intf;


		const SK_BoxReqs * key_group_req =

		&(req->dyna_i->key_group_req);


		int key_group_anc_support =

		req->stat_i->key_group_anc_support;


		//Retrieve the #key_group field

		SK_Box * key_group = NULL;

		SK_HmapStd_get_key_group_ptr (req, hms, &key_group);


		//Determine the number of elements in #key_group

		size_t num_keys = 0;

		key_group_intf->get_num_elems
		(
			key_group_req,
			key_group,
			&num_keys
		);


		//Get the anchor if available.
		//
		//Note, no precaution for multi-threaded anchor use is taken
		//here, because only 1 thread may call this function at time.

		SK_BoxAnc * anc = NULL;

		if (key_group_anc_support)
		{
			SK_HmapStd_get_key_anc_ptr (req, hms, &anc);
		}


		//Iterate through all of the keys in #key_group, and
		//deinitialize every #SK_Box that is used to store a key

		for (size_t key_sel = 0; key_sel < num_keys; key_sel += 1)
		{
			void * key = NULL;


			if (key_group_anc_support)
			{
				key_group_intf->get_elem_ptr_w_anc
				(
					key_group_req,
					key_group,
					key_sel,
					anc,
					anc,
					&key
				);
			}

			else
			{
				key_group_intf->get_elem_ptr
				(
					key_group_req,
					key_group,
					key_sel,
					&key
				);
			}


			req->stat_r->key_bytes_intf->deinit
			(
				&(req->dyna_i->key_bytes_req),
				key
			);
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Deinitializes all of the #SK_Box's that are used to store individual values
 *  within the #val_group field.
 *
 *  This is used within SK_HmapStd_deinit ().
 *
 *  @param req			The #SK_HmapReqs to use when accessing #hms
 *
 *  @param hms			The #SK_HmapStd instance to modify
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_val_group_elem_deinit
(
	const SK_HmapReqs *	req,

	SK_HmapStd *		hms
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));


	if (req->stat_r->key_val_concat)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_STD_L_DEBUG,
			"BUG : This function can not be used when "
			"#val_val_concat = %d",
			req->stat_r->key_val_concat
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	if (req->stat_r->val_dyna_size_en)
	{
		//Store some variables that make accessing #val_group shorter

		const SK_BoxIntf * val_group_intf =

		req->stat_r->val_group_intf;


		const SK_BoxReqs * val_group_req =

		&(req->dyna_i->val_group_req);


		int val_group_anc_support =

		req->stat_i->val_group_anc_support;


		//Retrieve the #val_group field

		SK_Box * val_group = NULL;

		SK_HmapStd_get_val_group_ptr (req, hms, &val_group);


		//Determine the number of elements in #val_group

		size_t num_vals = 0;

		val_group_intf->get_num_elems
		(
			val_group_req,
			val_group,
			&num_vals
		);


		//Get the anchor if available
		//
		//Note, no precaution for multi-threaded anchor use is taken
		//here, because only 1 thread may call this function at time.

		SK_BoxAnc * anc = NULL;

		if (val_group_anc_support)
		{
			SK_HmapStd_get_val_anc_ptr (req, hms, &anc);
		}


		//Iterate through all of the vals in #val_group, and
		//deinitialize every #SK_Box that is used to store a val

		for (size_t val_sel = 0; val_sel < num_vals; val_sel += 1)
		{
			void * val = NULL;


			if (val_group_anc_support)
			{
				val_group_intf->get_elem_ptr_w_anc
				(
					val_group_req,
					val_group,
					val_sel,
					anc,
					anc,
					&val
				);
			}

			else
			{
				val_group_intf->get_elem_ptr
				(
					val_group_req,
					val_group,
					val_sel,
					&val
				);
			}


			req->stat_r->val_bytes_intf->deinit
			(
				&(req->dyna_i->val_bytes_req),
				val
			);
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Deinitializes all of the #SK_Box's that are used to store individual keys
 *  and / or values within the #kv_group field.
 *
 *  This is used within SK_HmapStd_deinit ().
 *
 *  @param req			The #SK_HmapReqs associated with the
 *				#SK_HmapStd instance that #kv_group is within
 *
 *  @param kv_group		The #kv_group to inspect
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_kv_group_elem_deinit
(
	const SK_HmapReqs *	req,

	SK_HmapStd *		hms
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));


	if (0 == req->stat_r->key_val_concat)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_STD_L_DEBUG,
			"BUG : This function can not be used when "
			"#val_val_concat = %d",
			req->stat_r->key_val_concat
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Determine if #SK_Box's are used to store keys and / or values inside
	//of #kv_group

	int key_dyna_size_en = req->stat_r->key_dyna_size_en;

	int val_dyna_size_en = req->stat_r->val_dyna_size_en;


	int box_used = (key_dyna_size_en || val_dyna_size_en);


	if (box_used)
	{
		//Store some variables that make accessing #kv_group shorter

		const SK_BoxIntf * kv_group_intf =

		req->stat_r->key_group_intf;


		const SK_BoxReqs * kv_group_req =

		&(req->dyna_i->key_group_req);


		int kv_group_anc_support =

		req->stat_i->key_group_anc_support;


		//Retrieve the #kv_group field

		SK_Box * kv_group = NULL;

		SK_HmapStd_get_kv_group_ptr (req, hms, &kv_group);


		//Determine the number of elements in #kv_group

		size_t num_elems = 0;

		kv_group_intf->get_num_elems
		(
			kv_group_req,
			kv_group,
			&num_elems
		);


		//Get the anchor if available
		//
		//Note, no precaution for multi-threaded anchor use is taken
		//here, because only 1 thread may call this function at time.

		SK_BoxAnc * anc = NULL;

		if (kv_group_anc_support)
		{
			SK_HmapStd_get_kv_anc_ptr (req, hms, &anc);
		}


		//Iterate through all of the elements in #kv_group, and
		//deinitialize every #SK_Box that is used to store either a key
		//or a value

		for (size_t elem_sel = 0; elem_sel < num_elems; elem_sel += 1)
		{
			void * elem = NULL;

			if (kv_group_anc_support)
			{
				kv_group_intf->get_elem_ptr_w_anc
				(
					kv_group_req,
					kv_group,
					elem_sel,
					anc,
					anc,
					&elem
				);
			}

			else
			{
				kv_group_intf->get_elem_ptr
				(
					kv_group_req,
					kv_group,
					elem_sel,
					&elem
				);
			}


			//From the element of #kv_group that was retrieved, now
			//get a pointer to the key / value, and deinitialize
			//them if they use an #SK_Box to store their bytes.

			if (key_dyna_size_en)
			{
				SK_Box * key = NULL;

				SK_HmapStd_get_kv_elem_key_ptr
				(
					req,
					elem,
					(void **) &key
				);


				req->stat_r->key_bytes_intf->deinit
				(
					&(req->dyna_i->key_bytes_req),
					key
				);
			}


			if (val_dyna_size_en)
			{
				SK_Box * val = NULL;

				SK_HmapStd_get_kv_elem_val_ptr
				(
					req,
					elem,
					(void **) &val
				);


				req->stat_r->val_bytes_intf->deinit
				(
					&(req->dyna_i->val_bytes_req),
					val
				);
			}
		}
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_HmapIntf.deinit
 */

SlyDr SK_HmapStd_deinit
(
	const SK_HmapReqs *	req,

	SK_HmapStd *		hms
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));


	//Determine if (key_group, val_group) need to be deinitialized or if
	//(kv_group) should be deinitialized

	if (req->stat_r->key_val_concat)
	{
		//Retrieve #kv_group

		SK_Box * kv_group = NULL;

		SK_HmapStd_get_kv_group_ptr (req, hms, &kv_group);


		//Deinitialize all of the #SK_Box instances stored within
		//#kv_group (if present)

		SK_HmapStd_kv_group_elem_deinit (req, hms);


		//Deinitialize #kv_group itself

		req->stat_r->key_group_intf->deinit
		(
			&(req->dyna_i->key_group_req),
			kv_group
		);
	}

	else
	{
		//Deinitialize #key_group and #val_group

		//Retrieve #key_group and #val_group

		SK_Box * key_group = NULL;

		SK_HmapStd_get_key_group_ptr (req, hms, &key_group);


		SK_Box * val_group = NULL;

		SK_HmapStd_get_val_group_ptr (req, hms, &val_group);


		//Deinitialize all ofthe #SK_Box instances stored within
		//#key_group and #val_group (if any are present)

		SK_HmapStd_key_group_elem_deinit (req, hms);

		SK_HmapStd_val_group_elem_deinit (req, hms);


		//Deinitialize #key_group and #val_group themselves

		req->stat_r->key_group_intf->deinit
		(
			&(req->dyna_i->key_group_req),
			key_group
		);


		req->stat_r->key_group_intf->deinit
		(
			&(req->dyna_i->key_group_req),
			val_group
		);
	}


	return SlyDr_get (SLY_SUCCESS);
}




//TODO : The SK_HmapStd_*_del_ind () functions need test-benches


/*!
 *  Removes an individual key (of a key-value pair) from #key_group within an
 *  #SK_HmapStd instance.
 *
 *  @warning
 *  Although similar to #SK_HmapIntf.rem_by_ind (), this function is meant to
 *  be used internally by the #SK_HmapStd implementation.
 *
 *  @param req			The #SK_HmapReqs to use when accessing #hms
 *
 *  @param hms			The #SK_HmapStd to remove a key
 *				(of a key-value pair) from
 *
 *  @param ind			The index within #key_group
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_key_group_del_ind
(
	SK_HmapReqs *	req,

	SK_HmapStd *	hms,

	size_t		ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));


	if (req->stat_r->key_val_concat)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_STD_L_DEBUG,
			"BUG : This function can not be used when "
			"#val_val_concat = %d",
			req->stat_r->key_val_concat
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Store some variables that make accessing #key_group easier

	const SK_BoxIntf * key_group_intf =

	req->stat_r->key_group_intf;


	const SK_BoxReqs * key_group_req =

	&(req->dyna_i->key_group_req);


	int key_group_anc_support =

	req->stat_i->key_group_anc_support;


	//Get a pointer to the #SK_Box used to store keys (of key-value pairs)

	SK_Box * key_group = NULL;

	SK_HmapStd_get_key_group_ptr (req, hms, &key_group);


	//Sanity check on #ind

	if (SK_HMAP_STD_L_DEBUG)
	{
		//Check that #ind is lower than the number of key-value pairs

		size_t key_group_num_elems = 0;

		key_group_intf->get_num_elems
		(
			key_group_req,
			key_group,
			&key_group_num_elems
		);

		if (ind >= key_group_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_L_DEBUG,
				"#ind = %zu, which is too large. "
				"#key_group_num_elems = %zu.",
				ind,
				key_group_num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}


		// //Check that the number of elements in #val_group is consistent
		// //with #key_group
		//
		// size_t val_group_num_elems = 0;
		//
		// val_group_intf->get_num_elems
		// (
		// 	val_group_req,
		// 	val_group,
		// 	&val_group_num_elems
		// );
		//
		// if (key_group_num_elems != val_group_num_elems)
		// {
		// 	SK_DEBUG_PRINT
		// 	(
		// 		SK_HMAP_STD_L_DEBUG,
		// 		"BUG : #key_group_num_elems = %zu and "
		// 		"#val_group_num_elems = %zu. However, "
		// 		"these 2 values should always be "
		// 		"equal."
		// 	);
		//
		// 	return SlyDr_get (SLY_BAD_ARG);
		// }
	}


	//Get the anchor if available
	//
	//Note, no precaution for multi-threaded anchor use is taken,
	//because only 1 thread may call this function at a time.

	SK_BoxAnc * key_anc = NULL;

	if (key_group_anc_support)
	{
		SK_HmapStd_get_key_anc_ptr (req, hms, &key_anc);
	}


	//If an #SK_Box is used to store the bytes of the key at #ind, then
	//make sure to properly deinitialize that #SK_Box before removing the
	//element from #key_group at #ind.

	int key_dyna_size_en = req->stat_r->key_dyna_size_en;

	if (key_dyna_size_en)
	{
		//Get the key associated with #ind

		SK_Box * key = NULL;

		if (key_group_anc_support)
		{
			key_group_intf->get_elem_ptr_w_anc
			(
				key_group_req,
				key_group,
				ind,
				key_anc,
				NULL,
				(void **) &key
			);
		}

		else
		{
			key_group_intf->get_elem_ptr
			(
				key_group_req,
				key_group,
				ind,
				(void **) &key
			);
		}


		//Deinitialze the #SK_Box used to store the bytes of the key

		req->stat_r->key_bytes_intf->deinit
		(
			&(req->dyna_i->key_bytes_req),
			key
		);
	}


	//Now that the key associated with #ind in #key_group is properly
	//deinitialized (if needed), it is safe to remove the element at #ind
	//from #key_group.

	if (key_group_anc_support)
	{
		//Note how #key_anc is updated here to ensure that it remains
		//valid after #key_group is modified.

		key_group_intf->rem_w_anc
		(
			key_group_req,
			key_group,
			ind,
			key_anc,
			key_anc,
			NULL
		);
	}

	else
	{
		key_group_intf->rem
		(
			key_group_req,
			key_group,
			ind,
			NULL
		);
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Removes an individual value (of a key-value pair) from #val_group within an
 *  #SK_HmapStd instance.
 *
 *  @warning
 *  Although similar to #SK_HmapIntf.rem_by_ind (), this function is meant to
 *  be used internally by the #SK_HmapStd implementation.
 *
 *  @param req			The #SK_HmapReqs to use when accessing #hms
 *
 *  @param hms			The #SK_HmapStd to remove a value
 *				(of a key-value pair) from
 *
 *  @param ind			The index within #val_group
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_val_group_del_ind
(
	SK_HmapReqs *	req,

	SK_HmapStd *	hms,

	size_t		ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));


	if (req->stat_r->key_val_concat)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_STD_L_DEBUG,
			"BUG : This function can not be used when "
			"#val_val_concat = %d",
			req->stat_r->key_val_concat
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Store some variables that make accessing #val_group easier

	const SK_BoxIntf * val_group_intf =

	req->stat_r->val_group_intf;


	const SK_BoxReqs * val_group_req =

	&(req->dyna_i->val_group_req);


	int val_group_anc_support =

	req->stat_i->val_group_anc_support;


	//Get a pointer to the #SK_Box used to store values

	SK_Box * val_group = NULL;

	SK_HmapStd_get_val_group_ptr (req, hms, &val_group);


	//Sanity check on #ind

	if (SK_HMAP_STD_L_DEBUG)
	{
		//Check that #ind is lower than the number of key-value pairs

		size_t val_group_num_elems = 0;

		val_group_intf->get_num_elems
		(
			val_group_req,
			val_group,
			&val_group_num_elems
		);

		if (ind >= val_group_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_L_DEBUG,
				"#ind = %zu, which is too large. "
				"#val_group_num_elems = %zu.",
				ind,
				val_group_num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}


		// //Check that the number of elements in #val_group is consistent
		// //with #val_group
		//
		// size_t val_group_num_elems = 0;
		//
		// val_group_intf->get_num_elems
		// (
		// 	val_group_req,
		// 	val_group,
		// 	&val_group_num_elems
		// );
		//
		// if (val_group_num_elems != val_group_num_elems)
		// {
		// 	SK_DEBUG_PRINT
		// 	(
		// 		SK_HMAP_STD_L_DEBUG,
		// 		"BUG : #val_group_num_elems = %zu and "
		// 		"#val_group_num_elems = %zu. However, "
		// 		"these 2 values should always be "
		// 		"equal."
		// 	);
		//
		// 	return SlyDr_get (SLY_BAD_ARG);
		// }
	}


	//Get the anchor if available
	//
	//Note, no precaution for multi-threaded anchor use is taken,
	//because only 1 thread may call this function at a time.

	SK_BoxAnc * val_anc = NULL;

	if (val_group_anc_support)
	{
		SK_HmapStd_get_val_anc_ptr (req, hms, &val_anc);
	}


	//If an #SK_Box is used to store the bytes of the value at #ind, then
	//make sure to properly deinitialize that #SK_Box before removing the
	//element from #val_group at #ind.

	int val_dyna_size_en = req->stat_r->val_dyna_size_en;

	if (val_dyna_size_en)
	{
		//Get the val associated with #ind

		SK_Box * val = NULL;

		if (val_group_anc_support)
		{
			val_group_intf->get_elem_ptr_w_anc
			(
				val_group_req,
				val_group,
				ind,
				val_anc,
				NULL,
				(void **) &val
			);
		}

		else
		{
			val_group_intf->get_elem_ptr
			(
				val_group_req,
				val_group,
				ind,
				(void **) &val
			);
		}


		//Deinitialze the #SK_Box used to store the bytes of the val

		req->stat_r->val_bytes_intf->deinit
		(
			&(req->dyna_i->val_bytes_req),
			val
		);
	}


	//Now that the value associated with #ind in #val_group are properly
	//deinitialized (if needed), it is safe to remove the element at #ind
	//from #val_group.

	if (val_group_anc_support)
	{
		//Note how #val_anc is updated here to ensure that it remains
		//valid after #val_group is modified.

		val_group_intf->rem_w_anc
		(
			val_group_req,
			val_group,
			ind,
			val_anc,
			val_anc,
			NULL
		);
	}

	else
	{
		val_group_intf->rem
		(
			val_group_req,
			val_group,
			ind,
			NULL
		);
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Removes an individual key-value pair from #kv_group within an #SK_HmapStd
 *  instance.
 *
 *  @warning
 *  Although similar to #SK_HmapIntf.rem_by_ind (), this function is meant to
 *  be used internally by the #SK_HmapStd implementation.
 *
 *  @param req			The #SK_HmapReqs to use when accessing #hms
 *
 *  @param hms			The #SK_HmapStd to remove a key-value pair from
 *
 *  @param ind			The index within #kv_group associated with the
 *				key-value pair to remove
 *
 *  @return			Standard status code
 */

SlyDr SK_HmapStd_kv_group_del_ind
(
	SK_HmapReqs *	req,

	SK_HmapStd *	hms,

	size_t		ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));


	if (0 == req->stat_r->key_val_concat)
	{
		SK_DEBUG_PRINT
		(
			SK_HMAP_STD_L_DEBUG,
			"BUG : This function can not be used when "
			"#val_val_concat = %d",
			req->stat_r->key_val_concat
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Store some variables that make accessing #kv_group shorter

	const SK_BoxIntf * kv_group_intf =

	req->stat_r->key_group_intf;


	const SK_BoxReqs * kv_group_req =

	&(req->dyna_i->key_group_req);


	int kv_group_anc_support =

	req->stat_i->key_group_anc_support;


	//Get pointers to the #SK_Box used to store keys and values

	SK_Box * kv_group = NULL;

	SK_HmapStd_get_kv_group_ptr (req, hms, &kv_group);


	//Get the anchor if available
	//
	//Note, no precaution for multi-threaded anchor use is taken, because
	//only 1 thread may call this function at a time.

	SK_BoxAnc * anc = NULL;

	if (kv_group_anc_support)
	{
		SK_HmapStd_get_kv_anc_ptr (req, hms, &anc);
	}


	//Sanity check on #ind

	if (SK_HMAP_STD_L_DEBUG)
	{
		//Check that #ind is lower than the number of key-value pairs

		size_t kv_group_num_elems = 0;

		kv_group_intf->get_num_elems
		(
			kv_group_req,
			kv_group,
			&kv_group_num_elems
		);

		if (ind >= kv_group_num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_L_DEBUG,
				"#ind = %zu, which is too large. "
				"#kv_group_num_elems = %zu.",
				ind,
				kv_group_num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//If #SK_Box's are used to store the bytes of the key / value at #ind,
	//then make sure to properly deinitialize that #SK_Box before removing
	//the element from #kv_group at #ind.

	int key_dyna_size_en = req->stat_r->key_dyna_size_en;

	int val_dyna_size_en = req->stat_r->val_dyna_size_en;


	int box_used = (key_dyna_size_en || val_dyna_size_en);


	if (box_used)
	{
		//Get the element associated with #ind, which holds both the
		//key and value

		void * elem = NULL;

		if (kv_group_anc_support)
		{
			kv_group_intf->get_elem_ptr_w_anc
			(
				kv_group_req,
				kv_group,
				ind,
				anc,
				NULL,
				&elem
			);
		}

		else
		{
			kv_group_intf->get_elem_ptr
			(
				kv_group_req,
				kv_group,
				ind,
				&elem
			);
		}


		//From the element of #kv_group that was retrieved, now
		//get a pointer to the key / value, and deinitialize them if
		//they use an #SK_Box to store their bytes.

		if (key_dyna_size_en)
		{
			SK_Box * key = NULL;

			SK_HmapStd_get_kv_elem_key_ptr
			(
				req,
				elem,
				(void **) &key
			);


			req->stat_r->key_bytes_intf->deinit
			(
				&(req->dyna_i->key_bytes_req),
				key
			);
		}


		if (val_dyna_size_en)
		{
			SK_Box * val = NULL;

			SK_HmapStd_get_kv_elem_val_ptr
			(
				req,
				elem,
				(void **) &val
			);


			req->stat_r->val_bytes_intf->deinit
			(
				&(req->dyna_i->val_bytes_req),
				val
			);
		}
	}


	//Now that the key / value associated with #ind in #kv_group are
	//properly deinitialized (if needed), it is safe to remove the element
	//at #ind from #kv_group.

	if (kv_group_anc_support)
	{
		//Note how #anc is updated here to ensure that it remains valid
		//after #kv_group is modified.

		kv_group_intf->rem_w_anc
		(
			kv_group_req,
			kv_group,
			ind,
			anc,
			anc,
			NULL
		);
	}

	else
	{
		kv_group_intf->rem
		(
			kv_group_req,
			kv_group,
			ind,
			NULL
		);
	}


	return SlyDr_get (SLY_SUCCESS);
}




// FIXME : Either finish or remove this function
//
// /*!
//  *  Removes all of the key-value pairs present within an #SK_HmapStd instance.
//  *
//  */
//
// SlySr SK_HmapStd_del_ind_range
// (
// )
// {
// 	SK_Box * key_group = NULL;
// 	SK_Box * val_group = NULL;
//
// 	if (req->stat_r->key_val_concat)
// 	{
// 		SK_HmapStd_get_kv_group_ptr (req, hms, &key_group);
//
// 		val_group = key_group;
// 	}
//
// 	else
// 	{
// 		SK_HmapStd_get_key_group_ptr (req, hms, &key_group);
//
// 		SK_HmapStd_get_val_group_ptr (req, hms, &val_group);
// 	}
//
//
// 	if (req->stat_r->key_dyna_size_en)
// 	{
// 		//This deinitializes all of the #SK_Box's that are used to
// 		//store keys in #hms
//
// 		size_t num_keys = 0;
//
// 		req->stat_r->key_group_intf->get_num_elems
// 		(
// 			req->dyna_i->key_group_req,
// 			group,
// 			&num_keys
// 		);
//
//
// 		for (size_t key_sel = 0; key_sel < num_keys; key_sel += 1)
// 		{
// 			void * elem = NULL;
//
// 			if (req->stat_i->key_group_anc_support)
// 			{
// 				SK_BoxAnc * key_anc = NULL;
//
// 				SK_HmapStd_get_key_anc_ptr (req, hms, &key_anc);
//
//
// 				req->stat_r->key_group_intf->get_elem_ptr_w_anc
// 				(
// 					req->dyna_i->key_group_req,
// 					group,
// 					key_sel,
// 					key_anc,
// 					key_anc,
// 					&elem
// 				);
// 			}
//
// 			else
// 			{
// 				req->stat_r->key_group_intf->get_elem_ptr
// 				(
// 					req->dyna_i->key_group_req,
// 					group,
// 					key_sel,
// 					&elem
// 				);
// 			}
//
//
// 			SK_Box * key = elem;
//
// 			if (req->stat_r->key_val_concat)
// 			{
// 				SK_HmapStd_get_kv_elem_key_ptr
// 				(
// 					req,
// 					elem,
// 					&key
// 				);
// 			}
//
//
// 			req->stat_r->key_bytes_intf->deinit
// 			(
// 				req->dyna_i->key_bytes_req,
// 				key
// 			);
// 		}
// 	}
//
//
// 	if (req->stat_r->val_dyna_size_en)
// 	{
// 		//This deinitializes all of the #SK_Box's that are used to
// 		//store vals in #hms
//
// 		size_t num_vals = 0;
//
// 		req->stat_r->val_group_intf->get_num_elems
// 		(
// 			req->dyna_i->val_group_req,
// 			group,
// 			&num_vals
// 		);
//
//
// 		for (size_t val_sel = 0; val_sel < num_vals; val_sel += 1)
// 		{
// 			void * elem = NULL;
//
// 			if (req->stat_i->val_group_anc_support)
// 			{
// 				SK_BoxAnc * val_anc = NULL;
//
// 				SK_HmapStd_get_val_anc_ptr (req, hms, &val_anc);
//
//
// 				req->stat_r->val_group_intf->get_elem_ptr_w_anc
// 				(
// 					req->dyna_i->val_group_req,
// 					group,
// 					val_sel,
// 					val_anc,
// 					val_anc,
// 					&elem
// 				);
// 			}
//
// 			else
// 			{
// 				req->stat_r->val_group_intf->get_elem_ptr
// 				(
// 					req->dyna_i->val_group_req,
// 					group,
// 					val_sel,
// 					&elem
// 				);
// 			}
//
//
// 			SK_Box * val = elem;
//
// 			if (req->stat_r->val_val_concat)
// 			{
// 				SK_HmapStd_get_kv_elem_val_ptr
// 				(
// 					req,
// 					elem,
// 					&val
// 				);
// 			}
//
//
// 			req->stat_r->val_bytes_intf->deinit
// 			(
// 				req->dyna_i->val_bytes_req,
// 				val
// 			);
// 		}
// 	}
//
//
// 	return SlySr_get (SLY_SUCCESS);
// }




/*!
 *  Determines the index at which #key should be placed in #key_group in #hms,
 *  and whether or not #key is already present.
 *
 *  This version of the function uses a constant-sized #key.
 *
 *  @warning
 *  This function will use a binary-search on #key_group in #hms, which
 *  REQUIRES #key_group to be sorted in order for it to work. Therefore, keys
 *  should ALWAYS be inserted into #key_group at the indices that this function
 *  reports, otherwise it is not guaranteed that #key_group will be properly
 *  sorted.
 *
 *  @param req			The #SK_HmapReqs that represent general
 *  				parameters of the #SK_HmapStd
 *
 *  @param hms			The #SK_Hmap to inspect
 *
 *  @param key			The key to inspect
 *
 *  @param ind			Where to place the index that #key should be
 *				inserted into #key_group within #hms
 *
 *				This CAN be NULL if this is not desired.
 *
 *  @param exists		Where to place a flag that will be 1 if #key is
 *				already present at #ind or not
 *
 *				This CAN be NULL if this is not desired.
 *
 *  @return			Standard status code
 */

//TODO : This function needs to be tested to ensure that it works correctly

SlyDr SK_HmapStd_c_key_pos
(
	const SK_HmapReqs *	req,

	const SK_HmapStd *	hms,

	const void *		key,

	size_t *		ind,

	int *			exists
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, key, SlyDr_get (SLY_BAD_ARG));


	if (SK_HMAP_STD_H_DEBUG)
	{
		if (1 == req->stat_r->key_dyna_size_en)
		{
			SK_DEBUG_PRINT
			(
				SK_HMAP_STD_H_DEBUG,
				"BUG : This function should not be used by "
				"the #SK_HmapStd implementation when "
				"#req->stat_r->key_dyna_size_en is 1."
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Determine the number of elements within #key_group

	SK_Box * key_group = NULL;

	if (req->stat_r->key_val_concat)
	{
		SK_HmapStd_get_kv_group_ptr (req, hms, &key_group);
	}

	else
	{
		SK_HmapStd_get_key_group_ptr (req, hms, &key_group);
	}



	size_t num_elems = 0;

	req->stat_r->key_group_intf->get_num_elems
	(
		&(req->dyna_i->key_group_req),
		key_group,
		&num_elems
	);


	//Check if there are no elements within #hms, and therefore #key should
	//automatically be placed at index zero

	if (0 >= num_elems)
	{
		if (NULL != ind)
		{
			*ind = 0;
		}

		if (NULL != exists)
		{
			*exists = 0;
		}

		return SlyDr_get (SLY_SUCCESS);
	}


	//Begin a binary-search into #key_group

	size_t min_ind =	0;

	size_t max_ind =	(num_elems - 1);

	size_t sel_ind =	((max_ind - min_ind) / 2) + min_ind;

	void * sel_key =	NULL;

	int res =		0;


	SK_BoxAnc * key_group_anc = NULL;

	if (req->stat_i->key_group_anc_support)
	{
		SK_HmapStd_get_key_anc_ptr (req, hms, &key_group_anc);


		req->stat_r->key_group_intf->get_anc
		(
			&(req->dyna_i->key_group_req),
			key_group,
			sel_ind,
			key_group_anc
		);
	}


	while (max_ind >= min_ind)
	{
		//Get the key at index #sel_ind

		if (req->stat_i->key_group_anc_support)
		{
			req->stat_r->key_group_intf->get_elem_ptr_w_anc
			(
				&(req->dyna_i->key_group_req),
				key_group,
				sel_ind,
				key_group_anc,
				key_group_anc,
				&sel_key
			);
		}

		else
		{
			req->stat_r->key_group_intf->get_elem_ptr
			(
				&(req->dyna_i->key_group_req),
				key_group,
				sel_ind,
				&sel_key
			);
		}


		if (req->stat_r->key_val_concat)
		{
			SK_HmapStd_get_kv_elem_key_ptr (req, sel_key, sel_key);
		}


		//Determine if the raw bytes of #key are lesser-than,
		//greater-than, or equal to #sel_key according to
		//SK_byte_le_cmp ()

		//TODO : Perhaps it would be slightly more efficient to move
		//the condition-checks of values that are guaranteed to be the
		//same every loop iteration outside of the loop? Of course,
		//this might mean replicating the loop multiple times with
		//slight differences

		if (req->stat_i->prim_flag)
		{
			req->stat_i->prim_key_cmp
			(
				key,
				sel_key,
				&res
			);
		}

		else
		{
			SK_byte_le_cmp
			(
				key,
				sel_key,
				req->stat_r->key_stat_size,
				&res
			);
		}


		//Check if #sel_key matches #key

		if (0 == res)
		{
			//#key was found within #key_group, therefore this
			//function can report that the key already exists, and
			//the search is over.

			if (NULL != ind)
			{
				*ind = sel_ind;
			}

			if (NULL != exists)
			{
				*exists = 1;
			}

			return SlyDr_get (SLY_SUCCESS);
		}


		//Update the search-space consistently with the comparison
		//result #res

		if (-1 == res)
		{
			//#key was lesser-than #sel_key, search space must move
			//into lower portion

			max_ind = sel_ind - 1;
		}

		else if (+1 == res)
		{
			//#key was greater-than #sel_key, search space must
			//move into higher portion

			min_ind = sel_ind + 1;
		}


		//Select the index that is in the middle of the search space

		sel_ind =

		((max_ind - min_ind) / 2) + min_ind;
	}


	//At this point, the binary search has finished with no matching key
	//found in #key_group.
	//
	//So, simply report the position at which a matching key would be
	//expected (if it were to exist), and report the fact that no key was
	//found.

	if (NULL != ind)
	{
		if (-1 == res)
		{
			*ind = sel_ind;
		}

		else if (+1 == res)
		{
			*ind = sel_ind + 1;
		}
	}

	if (NULL != exists)
	{
		*exists = 0;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Determines the index at which #key should be placed in #key_group in #hms,
 *  and whether or not #key is already present.
 *
 *  This version of the function expects the key in the form of an #SK_Box.
 *
 *  Note that #hms can store constant-sized or variable-sized keys, and still
 *  be used with this function.
 *
 *  @warning
 *  This function will use a binary-search on #key_group in #hms, which
 *  REQUIRES #key_group to be sorted in order for it to work. Therefore, keys
 *  should ALWAYS be inserted into #key_group at the indices that this function
 *  reports, otherwise it is not guaranteed that #key_group will be properly
 *  sorted.
 *
 *  @param req			The #SK_HmapReqs that represent general
 *  				parameters of the #SK_HmapStd
 *
 *  @param hms			The #SK_Hmap to inspect
 *
 *  @param key_intf		The #SK_BoxIntf to use when accessing #key
 *
 *  @param key_req		The #SK_BoxReqs to use when accessing #key
 *
 *  @param key			The key to inspect
 *
 *  @param ind			Where to place the index that #key should be
 *				inserted into #key_group within #hms
 *
 *				This CAN be NULL if this is not desired.
 *
 *  @param exists		Where to place a flag that will be 1 if #key is
 *				already present at #ind or not
 *
 *				This CAN be NULL if this is not desired.
 *
 *  @return			Standard status code
 */

//TODO : This function needs to be tested to ensure that it works correctly

SlyDr SK_HmapStd_key_pos
(
	const SK_HmapReqs *	req,

	const SK_HmapStd *	hms,

	const SK_BoxIntf *	key_intf,

	const SK_BoxReqs *	key_req,

	SK_Box *		key,

	size_t *		ind,

	int *			exists
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, hms, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, key_intf, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, key_req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_HMAP_STD_L_DEBUG, key, SlyDr_get (SLY_BAD_ARG));


	//Determine the number of elements within #key_group

	SK_Box * key_group = NULL;

	if (req->stat_r->key_val_concat)
	{
		SK_HmapStd_get_kv_group_ptr (req, hms, &key_group);
	}

	else
	{
		SK_HmapStd_get_key_group_ptr (req, hms, &key_group);
	}


	size_t num_elems = 0;

	req->stat_r->key_group_intf->get_num_elems
	(
		&(req->dyna_i->key_group_req),
		key_group,
		&num_elems
	);


	//Check if there are no elements within #hms, and therefore #key should
	//automatically be placed at index zero

	if (0 >= num_elems)
	{
		if (NULL != ind)
		{
			*ind = 0;
		}

		if (NULL != exists)
		{
			*exists = 0;
		}

		return SlyDr_get (SLY_SUCCESS);
	}


	//Begin a binary-search into #key_group

	size_t min_ind =	0;

	size_t max_ind =	(num_elems - 1);

	size_t sel_ind =	((max_ind - min_ind) / 2) + min_ind;

	void * sel_key =	NULL;

	int res =		0;


	SK_BoxAnc * key_group_anc = NULL;

	if (req->stat_i->key_group_anc_support)
	{
		SK_HmapStd_get_key_anc_ptr (req, hms, &key_group_anc);


		req->stat_r->key_group_intf->get_anc
		(
			&(req->dyna_i->key_group_req),
			key_group,
			sel_ind,
			key_group_anc
		);
	}


	while (max_ind >= min_ind)
	{
		//Get the key at index #sel_ind

		if (req->stat_i->key_group_anc_support)
		{
			req->stat_r->key_group_intf->get_elem_ptr_w_anc
			(
				&(req->dyna_i->key_group_req),
				key_group,
				sel_ind,
				key_group_anc,
				key_group_anc,
				&sel_key
			);
		}

		else
		{
			req->stat_r->key_group_intf->get_elem_ptr
			(
				&(req->dyna_i->key_group_req),
				key_group,
				sel_ind,
				&sel_key
			);
		}


		if (req->stat_r->key_val_concat)
		{
			SK_HmapStd_get_kv_elem_key_ptr (req, sel_key, sel_key);
		}


		//Determine if the raw bytes of #key are lesser-than,
		//greater-than, or equal to #sel_key according to
		//SK_byte_le_cmp ()

		//TODO : Perhaps it would be slightly more efficient to move
		//the condition-checks of values that are guaranteed to be the
		//same every loop iteration outside of the loop? Of course,
		//this might mean replicating the loop multiple times with
		//slight differences

		if (req->stat_r->key_dyna_size_en)
		{
			SK_Box_le_cmp
			(
				key_intf,
				key_req,
				key,

				req->stat_r->key_bytes_intf,
				&(req->dyna_i->key_bytes_req),
				sel_key,

				&res
			);
		}

		else
		{
			SK_Box_bare_le_cmp
			(
				key_intf,
				key_req,
				key,

				sel_key,
				req->stat_r->key_stat_size,

				&res
			);
		}


		if ((0 == res) && (0 == req->stat_r->key_zero_extend))
		{
			//Determine the size (in bytes) of both keys

			//FIXME : Maybe just make an SK_Box_num_bytes ()
			//function that determines the number of bytes stored
			//in an #SK_Box in total, and make use of it here

			size_t key_num_elems =		0;
			size_t key_size =		0;

			size_t sel_key_num_elems =	0;
			size_t sel_key_size =		0;


			key_intf->get_num_elems (key_req, key, &key_num_elems);

			key_size = key_req->stat_r->elem_size * key_num_elems;


			if (req->stat_r->key_dyna_size_en)
			{
				req->stat_r->key_bytes_intf->get_num_elems
				(
					&(req->dyna_i->key_bytes_req),
					sel_key,
					&sel_key_num_elems
				);

				sel_key_size =

				req->stat_r->key_stat_size * sel_key_num_elems;
			}

			else
			{
				sel_key_size = req->stat_r->key_stat_size;
			}


			//If the keys are different sizes, then they are
			//forbidden from being considered equal to one another
			//when (0 == req->stat_r->key_zero_extend).
			//
			//Therefore, report the larger-sized key as being the
			//larger value in this case.

			if (key_size > sel_key_size)
			{
				res = 1;
			}

			else if (key_size < sel_key_size)
			{
				res = -1;
			}
		}


		//Check if #sel_key matches #key

		if (0 == res)
		{
			//#key was found within #key_group, therefore this
			//function can report that the key already exists, and
			//the search is over.

			if (NULL != ind)
			{
				*ind = sel_ind;
			}

			if (NULL != exists)
			{
				*exists = 1;
			}

			return SlyDr_get (SLY_SUCCESS);
		}


		//Update the search-space consistently with the comparison
		//result #res

		if (-1 == res)
		{
			//#key was lesser-than #sel_key, search space must move
			//into lower portion

			max_ind = sel_ind - 1;
		}

		else if (+1 == res)
		{
			//#key was greater-than #sel_key, search space must
			//move into higher portion

			min_ind = sel_ind + 1;
		}


		//Select the index that is in the middle of the search space

		sel_ind =

		((max_ind - min_ind) / 2) + min_ind;
	}


	//At this point, the binary search has finished with no matching key
	//found in #key_group.
	//
	//So, simply report the position at which a matching key would be
	//expected (if it were to exist), and report the fact that no key was
	//found.

	if (NULL != ind)
	{
		if (-1 == res)
		{
			*ind = sel_ind;
		}

		else if (+1 == res)
		{
			*ind = sel_ind + 1;
		}
	}

	if (NULL != exists)
	{
		*exists = 0;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Performs a simple test-bench on SK_HmapStd_c_key_pos () and
 *  SK_HmapStd_key_pos ().
 *
 *  @param p_depth		The "printing depth".
 *
 *  				If (p_depth == 0), then this test-bench will
 *  				NOT print out results whatsoever to #file.
 *
 *  				If (p_depth > 0), then this test-bench WILL
 *				print out results to #file.
 *
 *				#p_depth will be decremented for inner
 *				test-benches until it reaches 0.
 *
 *				#SK_P_DEPTH_INF (and every value less than it)
 *				indicates infinite printing depth.
 *
 *  @param file			Pointer to the #FILE to print messages to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate
 *				printed lines
 *
 *  @return			Standard status code
 */

SlySr SK_HmapStd_key_pos_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco
)
{
	//Sanity check on arguments

	SK_NULL_RETURN_FP (p_depth, stdout, file, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (p_depth, file, deco);


	//Determine the next printing depth

	//i64 next_depth = SK_p_depth_next (p_depth);


	//Make constants that determine how many parameterizations to test

	const i64 BEG_KEY_VAL_CONCAT =		0;
	const i64 END_KEY_VAL_CONCAT =		1;
	const i64 INC_KEY_VAL_CONCAT =		1;

	const i64 BEG_KEY_DYNA_SIZE_EN =	0;
	const i64 END_KEY_DYNA_SIZE_EN =	1;
	const i64 INC_KEY_DYNA_SIZE_EN =	1;

	const i64 BEG_KEY_ZERO_EXTEND =		0;
	const i64 END_KEY_ZERO_EXTEND =		1;
	const i64 INC_KEY_ZERO_EXTEND =		1;

	const i64 BEG_KEY_STAT_SIZE =		1;
	const i64 END_KEY_STAT_SIZE =		16;
	const i64 INC_KEY_STAT_SIZE =		1;

	const i64 BEG_TEST_KEY_IN_BOX =		0;
	const i64 END_TEST_KEY_IN_BOX =		1;
	const i64 INC_TEST_KEY_IN_BOX =		1;

	const i64 BEG_TEST_KEY_BOX_SIZE =	1;
	const i64 END_TEST_KEY_BOX_SIZE =	16;
	const i64 INC_TEST_KEY_BOX_SIZE =	1;


	i64 key_val_concat =			BEG_KEY_VAL_CONCAT;

	i64 key_dyna_size_en =			BEG_KEY_DYNA_SIZE_EN;

	i64 key_zero_extend =			BEG_KEY_ZERO_EXTEND;

	i64 key_stat_size =			BEG_KEY_STAT_SIZE;

	i64 test_key_in_box =			BEG_TEST_KEY_IN_BOX;

	i64 test_key_box_size =			BEG_TEST_KEY_BOX_SIZE;


	//Enter the test-loop, in which #SK_HmapStd's will be initialized and
	//deinitialized

	int go =		1;

	int error_detected =	0;


	while (go)
	{
		//Announce the parameters of this test iteration

		if (p_depth)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"key_val_concat =    %" PRIi64,
				key_val_concat
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"key_dyna_size_en =  %" PRIi64,
				key_dyna_size_en
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"key_zero_extend =   %" PRIi64,
				key_zero_extend
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"key_stat_size =     %" PRIi64,
				key_stat_size
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"test_key_in_box =   %" PRIi64,
				test_key_in_box
			);

			SK_LineDeco_print_opt
			(
				file,
				deco,
				"test_key_box_size = %" PRIi64,
				test_key_box_size
			);

			SK_LineDeco_print_opt (file, deco, "");
		}


		//Make an #SK_HmapReqs that contains the configuration
		//requested by the current parameters

		SK_BoxStatReqs stat_r_basis =

		{
			.mem =			NULL,
			.elem_size =		0,
			.hint_num_stat_elems =	8,
			.hint_elems_per_block =	8
			//.brq_reg =		NULL
		};


		SK_BoxDynaReqs dyna_r_basis =

		{
			.hint_neg_slack =		4,
			.hint_pos_slack =		2,
			.hint_use_max_num_dyna_blocks =	0,
			.hint_max_num_dyna_blocks =	0,
			.hint_tree_avg_degree =		0
		};


		SK_BoxAllReqs key_group_arq;
		SK_BoxAllReqs val_group_arq;
		SK_BoxAllReqs key_bytes_arq;
		SK_BoxAllReqs val_bytes_arq;


		SK_HmapAllReqs arq;


		arq.stat_r =

		(SK_HmapStatReqs)

		{
			.key_val_concat =		key_val_concat,

			.key_dyna_size_en =		key_dyna_size_en,

			.val_dyna_size_en =		0,

			.key_stat_size =		key_stat_size,

			.val_stat_size =		1,

			.key_zero_extend =		key_zero_extend,

			.key_group_intf =		&SK_Array_intf,

			.val_group_intf =		&SK_Array_intf,

			.key_bytes_intf =		&SK_Array_intf,

			.val_bytes_intf =		&SK_Array_intf,

			.key_group_stat_r_basis =	&stat_r_basis,

			.val_group_stat_r_basis =	&stat_r_basis,

			.key_bytes_stat_r_basis =	&stat_r_basis,

			.val_bytes_stat_r_basis =	&stat_r_basis,

			.key_group_stat_r =		&(key_group_arq.stat_r),

			.key_group_stat_i =		&(key_group_arq.stat_i),

			.val_group_stat_r =		&(val_group_arq.stat_r),

			.val_group_stat_i =		&(val_group_arq.stat_i),

			.key_bytes_stat_r =		&(key_bytes_arq.stat_r),

			.key_bytes_stat_i =		&(key_bytes_arq.stat_i),

			.val_bytes_stat_r =		&(val_bytes_arq.stat_r),

			.val_bytes_stat_i =		&(val_bytes_arq.stat_i)
		};


		arq.dyna_r =

		(SK_HmapDynaReqs)

		{
			.val_zero_extend =		1,

			.key_group_dyna_r_basis =	&dyna_r_basis,

			.val_group_dyna_r_basis =	&dyna_r_basis,

			.key_bytes_dyna_r_basis =	&dyna_r_basis,

			.val_bytes_dyna_r_basis =	&dyna_r_basis,

			.key_group_dyna_r =		&(key_group_arq.dyna_r),

			.key_group_dyna_i =		&(key_group_arq.dyna_i),

			.val_group_dyna_r =		&(val_group_arq.dyna_r),

			.val_group_dyna_i =		&(val_group_arq.dyna_i),

			.key_bytes_dyna_r =		&(key_bytes_arq.dyna_r),

			.key_bytes_dyna_i =		&(key_bytes_arq.dyna_i),

			.val_bytes_dyna_r =		&(val_bytes_arq.dyna_r),

			.val_bytes_dyna_i =		&(val_bytes_arq.dyna_i)
		};


		SlyDr dr = SK_HmapStatReqs_box_calc (&(arq.stat_r));

		SK_RES_RETURN (SK_HMAP_STD_L_DEBUG, dr.res, SlySr_get (dr.res));


		dr = SK_HmapDynaReqs_box_calc (&(arq.stat_r), &(arq.dyna_r));

		SK_RES_RETURN (SK_HMAP_STD_L_DEBUG, dr.res, SlySr_get (dr.res));


		SK_HmapReqs req;

		dr = SK_HmapReqs_point_to_arq (&req, &arq);

		SK_RES_RETURN (SK_HMAP_STD_L_DEBUG, dr.res, SlySr_get (dr.res));


		dr = SK_HmapAllReqs_prep (&arq, &(arq.stat_r), &(arq.dyna_r));

		SK_RES_RETURN (SK_HMAP_STD_L_DEBUG, dr.res, SlySr_get (dr.res));


		//SK_HmapReqs_print (&req, 2, file, deco);


		//Allocate a storage object to store a single key that will be
		//inserted / removed from the #SK_HmapStd repeatedly

		const SK_BoxIntf * key_intf =	NULL;

		SK_BoxAllReqs key_arq =		{0};

		SK_BoxReqs key_req =		{0};

		void * key =			NULL;


		if (test_key_in_box)
		{
			//Make the configuration for #key

			key_intf = &SK_Array_intf;


			key_arq.stat_r =

			(SK_BoxStatReqs)

			{
				.mem =			&SK_MemStd,
				.elem_size =		1,
				.hint_num_stat_elems =	8,
				.hint_elems_per_block =	8
				//.brq_reg = 		NULL
			};


			key_arq.dyna_r =

			(SK_BoxDynaReqs)

			{
				.hint_neg_slack =		4,
				.hint_pos_slack =		2,
				.hint_use_max_num_dyna_blocks =	0,
				.hint_max_num_dyna_blocks =	0
			};


			SK_BoxReqs_point_to_arq (&key_req, &key_arq);


			SK_BoxReqs_prep
			(
				&key_req,
				key_req.stat_r,
				key_req.stat_i,
				key_req.dyna_r,
				key_req.dyna_i
			);


			//Determine the amount of static memory needed for
			//#key, and then allocate it

			size_t key_stat_size = 0;

			key_intf->inst_stat_mem (&key_req, &key_stat_size);


			//SlySr sr =

			SK_MemStd.take ((void **) &key, 1, key_stat_size);


			if (NULL == key)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"Could not allocate static memory for "
					"#key"
				);

				error_detected = 1;

				break;
			}


			//Initialize #key

			SlySr sr =

			key_intf->init
			(
				&key_req,
				key,
				0,
				NULL,
				NULL
			);


			if (SLY_SUCCESS != sr.res)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"Could not initialize #key as an #SK_Box"
				);

				SK_MemStd.free (key);

				error_detected = 1;

				break;
			}
		}

		else
		{
			//Allocate space for a statically-sized #key

			//SlySr sr =

			SK_MemStd.take ((void **) &key, 1, req.stat_r->key_stat_size);


			if (NULL == key)
			{
				SK_DEBUG_PRINT_FP
				(
					p_depth,
					file,
					"Could not allocate statically-sized "
					"#key"
				);

				error_detected = 1;

				break;
			}

		}


		//Allocate the static memory for an #SK_Hmap

		size_t hms_stat_size = 0;

		SK_HmapStd_inst_stat_mem (&req, &hms_stat_size);


		SK_HmapStd * hms = NULL;

		SK_MemStd.take ((void **) &hms, 1, hms_stat_size);


		if (NULL == hms)
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"Could not allocate static memory for #hms"
			);


			if (test_key_in_box)
			{
				key_intf->deinit (&key_req, key);
			}

			SK_MemStd.free (key);


			error_detected = 1;

			break;
		}


		//Intiialize the #SK_Hmap which will have keys added / removed
		//from it

		SlySr sr = SK_HmapStd_init (&req, hms);

		if (SLY_SUCCESS != sr.res)
		{
			SK_DEBUG_PRINT_FP
			(
				p_depth,
				file,
				"Could not initialize #key"
			);


			SK_MemStd.free (hms);


			if (test_key_in_box)
			{
				key_intf->deinit (&key_req, key);
			}

			SK_MemStd.free (key);


			error_detected = 1;

			break;
		}


		//TODO : Call the appropriate inner-test-bench using #key and
		//#hms


		//Free the resources that were allocated in this test iteration

		SK_HmapStd_deinit (&req, hms);

		SK_MemStd.free (hms);


		if (test_key_in_box)
		{
			key_intf->deinit (&key_req, key);
		}

		SK_MemStd.free (key);


		//Increment the test parameters

		int looped = 0;

		SK_inc_chain
		(
			&test_key_box_size,
			test_key_in_box,
			BEG_TEST_KEY_BOX_SIZE,
			END_TEST_KEY_BOX_SIZE,
			INC_TEST_KEY_BOX_SIZE,
			&looped
		);

		if (0 == test_key_in_box)
		{
			looped = 1;
		}


		SK_inc_chain
		(
			&test_key_in_box,
			looped,
			BEG_TEST_KEY_IN_BOX,
			END_TEST_KEY_IN_BOX,
			INC_TEST_KEY_IN_BOX,
			&looped
		);


		SK_inc_chain
		(
			&key_stat_size,
			looped,
			BEG_KEY_STAT_SIZE,
			END_KEY_STAT_SIZE,
			INC_KEY_STAT_SIZE,
			&looped
		);

		SK_inc_chain
		(
			&key_zero_extend,
			looped,
			BEG_KEY_ZERO_EXTEND,
			END_KEY_ZERO_EXTEND,
			INC_KEY_ZERO_EXTEND,
			&looped
		);

		SK_inc_chain
		(
			&key_dyna_size_en,
			looped,
			BEG_KEY_DYNA_SIZE_EN,
			END_KEY_DYNA_SIZE_EN,
			INC_KEY_DYNA_SIZE_EN,
			&looped
		);

		SK_inc_chain
		(
			&key_val_concat,
			looped,
			BEG_KEY_VAL_CONCAT,
			END_KEY_VAL_CONCAT,
			INC_KEY_VAL_CONCAT,
			&looped
		);


		if (looped)
		{
			break;
		}
	}


	SK_TB_END_PRINT_STD
	(
		p_depth,
		file,
		deco,
		error_detected,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




/*!
 *  Inner test-bench that is used inside of SK_HmapStd_key_pos_tb ().
 *
 *  This test-bench performs the tests that are possible when the #key is
 *  "bare", meaning stored a basic array instead of an #SK_Box.
 *
 *  @param p_depth		See SK_HmapStd_key_pos_tb ()
 *
 *  @param file			See SK_HmapStd_key_pos_tb ()
 *
 *  @param deco			See SK_HmapStd_key_pos_tb ()
 *
 *  @param req			The #SK_HmapReqs to use when accessing #hms
 *
 *  @param hms			The #SK_HmapStd in which the proper position of
 *				keys should be repeatedly determined
 *
 *  @param key			A bare array of size
 *  				(req->stat_r->key_stat_size) to use to
 *  				temporarily store keys during the test
 *
 *  @return			Standard status code
 */

SlySr SK_HmapStd_key_pos_bare_key_inner_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco,

	SK_HmapReqs *		req,
	SK_HmapStd *		hms,

	void *			key
)
{
	//Sanity check on arguments

	SK_NULL_RETURN_FP (p_depth, stdout, file, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (p_depth, file, req, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (p_depth, file, hms, SlySr_get (SLY_BAD_ARG));
	SK_NULL_RETURN_FP (p_depth, file, key, SlySr_get (SLY_BAD_ARG));


	//Make constants that determine how many parameterizations to test

	const i64 BEG_TEST_NUM =	0;
	const i64 END_TEST_NUM =	99;
	const i64 INC_TEST_NUM =	1;

	i64 test_num =			BEG_TEST_NUM;


	//Clear #hms of all keys before entering the test-loop.
	//
	//Note that this is done "manually", because this test-bench is a
	//pre-requisite for many other functions for #SK_HmapStd to be written.


	//TODO : Finish this test-bench


	//Enter the test-loop

	int go =		1;

	int error_detected =	0;


	while (go)
	{

		//Announce the parameters of this test iteration

		if (p_depth)
		{
			SK_LineDeco_print_opt
			(
				file,
				deco,
				"test_num = %" PRIi64,
				test_num
			);
		}



	}
}
