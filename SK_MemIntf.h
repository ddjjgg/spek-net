/*!****************************************************************************
 *
 * @file
 * SK_MemIntf.h
 *
 * Provides the specification of #SK_MemIntf, which is a generic interface for
 * memory allocators.
 *
 *****************************************************************************/




#ifndef SK_MEM_INTF_H
#define SK_MEM_INTF_H




#include <stdio.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




/*!
 *  Represents configuration options for a memory manager that can be set via
 *  #SK_MemIntf.set_conf ().
 */

typedef struct SK_MemIntfConf
{
	/*!
	 *  A hint to the memory manager about how much memory to pre-allocate
	 *  ahead of time.
	 *
	 *  This hint could be useful for a program when the minimum amount of
	 *  memory that the program will consume is known ahead of time.
	 *
	 *  If uncertain, leave this value as 0.
	 *
	 *  @warning
	 *  Since this is a hint, it CAN be ignored by the #SK_MemIntf
	 *  implementation.
	 */

	size_t hint_pre_alloc_size;




	/*!
	 *  A hint that describes the minimum size of allocation sizes that are
	 *  considered "common".
	 *
	 *  If uncertain, leave this value as 0.
	 *
	 *  @warning
	 *  Since this is a hint, it CAN be ignored by the #SK_MemIntf
	 *  implementation.
	 */

	size_t hint_min_common_size;




	/*!
	 *  A hint that describes the maximum size of allocation sizes that are
	 *  considred "common".
	 *
	 *  It is intended that allocation sizes within the range
	 *  [hint_min_common_size, hint_max_common_size] be considered common
	 *  by the memory manager.
	 *
	 *  If uncertain, leave this value as 0, which indicates that both
	 *  #hint_min_common_size and #hint_max_common_size should be ignored.
	 *
	 *  @warning
	 *  Since this is a hint, it CAN be ignored by the #SK_MemIntf
	 *  implementation.
	 */

	size_t hint_max_common_size;




	//TODO : Should there be hints in here to indicate the amount of
	//separate allocation requests the program intends on making? This can
	//help prepare for memory fragmentation ahead of time.




	//TODO : Should there be hints in here to notify the memory manager
	//that a large string of allocations of a similar / same size are about
	//to be made?




	//TODO : Should there be hints in here to request the memory manager to
	//to use a specific file to back memory with? Besides a field for the
	//file path, what other hints would need to go alongside such a hint?
}

SK_MemIntfConf;




/*!
 *  Defines a generic interface that can be used to allocate / deallocate
 *  memory from a memory manager.
 *
 *
 *
 *
 *  <b> GENERAL REQUIREMENTS OF #SK_MemIntf </b>
 *
 *
 *  Users / implementers of #SK_MemIntf must be aware that...
 *
 *  1)
 *
 *  	Memory allocations MUST remain valid until SK_MemIntf.free () is used
 *  	on the allocated memory block.
 *
 *  	In other words, the memory allocation can persist long after the
 *  	function that requested it returns.
 *
 *
 *  2)
 *
 *  	As a result of 1), the memory that is allocated must NOT be on the
 *  	stack.
 *
 *  	This does not necessarily mean that the memory must be allocated in the
 *  	same heap that malloc () from <stdlib.h> makes use of.
 *
 *	It is entirely possible that the running platform provides
 *	functionality to back memory allocations with other sources (ex: files,
 *	network connections, memory within peripheral devices, etc.).
 *
 *
 *  3)
 *
 *	Addresses within the contiguous range of a valid memory allocation are
 *	possible to access with the C dereference operator.
 *
 *	In other words, (*ptr) can be used.
 *
 *
 *  4)
 *
 *	The blocks of memory that are allocated are contiguous.
 *
 *	For example, if a block of 8 bytes is requested and the address
 *	0x00001000 is returned, then the address range [0x00001000, 0x00001007]
 *	is available for reading and writing. (Assuming byte-addressability
 *	here).
 *
 *
 *  5)
 *
 * 	If there is an instance of this interface named #inst_0, then calls to
 * 	memory allocators in #inst_0 MUST be paired with calls to memory
 * 	deallocators in #inst_0.
 *
 * 	If there is another instance of this interface named #inst_1, calls to
 * 	memory allocators in #inst_0 must NOT be paired with calls to memory
 * 	deallocators in #inst_1.
 *
 *
 *  6)
 *
 *  	All of the functions in #SK_MemIntf MUST be thread-safe.
 *
 *  	It is expected that memory managers provide any necessary locking
 *  	internally, and users are allowed to attempt memory allocations /
 *  	deallocations from any positive number of threads.
 */

typedef struct SK_MemIntf SK_MemIntf;




typedef struct SK_MemIntf
{
	/*!
	 *  Allocates a block of memory capable of storing a specified number
	 *  of elements of a specified size.
	 *
	 *  @warning
	 *  Sometime after calling this function, SK_MemIntf.free () MUST be
	 *  used to deallocate the allocated memory, provided that the memory
	 *  allocation was successful.
	 *
	 *  @warning
	 *  If (num_elems * elem_size) overflows a #size_t value, then this
	 *  function MUST fail.
	 *
	 *  @param addr		Where to place a pointer to the memory block.
	 *  			This will be set to NULL if the memory
	 *  			allocation fails.
	 *
	 *  			This argument can NOT be NULL.
	 *
	 *  @param num_elems	The number of elements to allocate within the
	 *			memory block.
	 *
	 *			This can NOT be 0.
	 *
	 *  @param elem_size	The size of each element measured in bytes.
	 *
	 *			This can NOT be 0.
	 *
	 *  @return		Standard status code.
	 *
	 *  			If and only if the memory allocation is
	 *  			successful, then #SLY_SUCCESS is returned.
	 */

	SlySr (* take)
	(
		void **		addr,

		size_t		num_elems,

		size_t		elem_size
	);




	/*!
	 *  Reallocates a block of memory to a different size than what it was
	 *  originally allocated with using one of the #SK_MemIntf.*take* ()
	 *  functions.
	 *
	 *  Space that is added / removed from the memory block will start from
	 *  the highest-indexed elements in the memory block, which are
	 *  associated with the highest memory addresses.
	 *
	 *  Space that is underneath the minimum of the old and new sizes of
	 *  the memory block will be unmodified. Space that is above this will
	 *  be left uninitialized, if there is any present in the resized
	 *  memory block.
	 *
	 *  @warning
	 *  If the memory block can not be resized at its current memory
	 *  address, then it may be moved to a new address.
	 *
	 *  @warning
	 *  If the size of the memory block is increased from what is was
	 *  previously, then the additional memory will be uninitialized.
	 *
	 *  @warning
	 *  If either #num_elems is 0 or #elem_size is 0, then this function
	 *  will behave as if #SK_MemIntf.free () has been called on (*addr),
	 *  and (*addr) will be set to NULL.
	 *
	 *  @warning
	 *  This function should NOT be called on a non-NULL memory address
	 *  which #SK_MemIntf.free () was used on last.
	 *
	 *  After this function is used, however, #SK_MemIntf.free () still
	 *  needs to be called on the memory block at #addr at some point in
	 *  the future.
	 *
	 *  @warning
	 *  If (num_elems * elem_size) overflows a #size_t value, then this
	 *  function MUST fail.
	 *
	 *  @note
	 *  If this function fails, the memory block at #addr will be left
	 *  unmodified.
	 *
	 *  @param addr		Pointer to the address of the memory block to
	 *			reallocate.
	 *
	 *			The memory block may be moved in memory if it
	 *			can not be resized in place, and therefore the
	 *			value of (*addr) may be updated.
	 *
	 *  			(addr) must NOT be NULL, but (*addr) CAN be
	 *  			NULL.
	 *
	 *			If the value of (*addr) is NULL, then this
	 *			function is effectively equivalent to
	 *			SK_MemIntf.take ().
	 *
	 *			If (num_elems = 0) or (elem_size = 0), then
	 *			(*addr) will be set to NULL.
	 *
	 *  @param num_elems	The new number of elements that the memory
	 *			block should be capable of storing.
	 *
	 *			This CAN be 0, which causes this function to
	 *			behave like #SK_MemIntf.free () on (*addr).
	 *
	 *  @param elem_size	The size of each element measured in bytes.
	 *
	 *			This CAN be 0, which causes this function to
	 *			behave like #SK_MemIntf.free () on (*addr).
	 *
	 *  @return		Standard status code.
	 *
	 *			If and only if the memory reallocation is
	 *			successful, then #SLY_SUCCESS is returned.
	 */

	SlySr (* retake)
	(
		void **		addr,

		size_t		num_elems,

		size_t		elem_size
	);




	//TODO : Should there be a function pointer here #retake_no_move ()
	//that operates very similarly to #retake (), but does not allow
	//(*addr) to be modified to point to a new address?




	/*!
	 *  Makes a best-effort attempt to allocate a memory block as close as
	 *  possible to a given address (which can be useful for cache
	 *  locality).
	 *
	 *  @warning
	 *  Since this is a "best-effort", there is no guarantee that the
	 *  address for the allocated memory block is as close possible to
	 *  #near_addr, though a well-behaved implementation of this will try
	 *  to provide that.
	 *
	 *  @warning
	 *  Sometime after calling this function, SK_MemIntf.free () MUST be
	 *  used to deallocate the allocated memory, provided that the memory
	 *  allocation was successful.
	 *
	 *  @warning
	 *  If (num_elems * elem_size) overflows a #size_t value, then this
	 *  function MUST fail.
	 *
	 *  @param addr		Where to place a pointer to the memory block.
	 *  			This will be set to NULL if the memory
	 *  			allocation fails.
	 *
	 *  			This argument can NOT be NULL.
	 *
	 *  @param num_elems	The number of elements to allocate within the
	 *			memory block.
	 *
	 *			This can NOT be 0.
	 *
	 *  @param elem_size	The size of each element measured in bytes.
	 *
	 *			This can NOT be 0.
	 *
	 *  @param near_addr	The memory address to try placing the allocated
	 *			memory block close to.
	 *
	 *  @return		Standard status code.
	 *
	 *			If and only if the memory allocation is
	 *			successful, then #SLY_SUCCESS is returned.
	 */

	SlySr (* take_near)
	(
		void **		addr,

		size_t		num_elems,

		size_t		elem_size,

		void *		near_addr
	);




	/*!
	 *  Makes a best-effort attempt to allocate memory in the range
	 *  [min_addr, max_addr].
	 *
	 *  @warning
	 *  This function is "soft", meaning that this function will still
	 *  SUCCEED even if the allocated memory is not in the requested range.
	 *
	 *  @warning
	 *  The requested range [min_addr, max_addr] is for the starting
	 *  address of the block. The highest memory address of the block CAN
	 *  go outside of this range.
	 *
	 *  @warning
	 *  Sometime after calling this function, SK_MemIntf.free () MUST be
	 *  used to deallocate the allocated memory, provided that the memory
	 *  allocation was successful.
	 *
	 *  @warning
	 *  If (num_elems * elem_size) overflows a #size_t value, then this
	 *  function MUST fail.
	 *
	 *  @param addr		Where to place a pointer to the memory block.
	 *  			This will be set to NULL if the memory
	 *  			allocation fails.
	 *
	 *  			This argument can NOT be NULL.
	 *
	 *  @param num_elems	The number of elements to allocate within the
	 *			memory block.
	 *
	 *			This can NOT be 0.
	 *
	 *  @param elem_size	The size of each element measured in bytes.
	 *
	 *			This can NOT be 0.
	 *
	 *  @param min_addr	The minimum address of the target address
	 *  			range.
	 *
	 *  @param max_addr	The maximum address of the target address
	 *			range.
	 *
	 *  @return		Standard status code.
	 *
	 *			If and only if the memory allocation is
	 *			successful, then #SLY_SUCCESS is returned.
	 */

	SlySr (* take_soft)
	(
		void **		addr,

		size_t		num_elems,

		size_t		elem_size,

		void *		min_addr,

		void *		max_addr
	);




	/*!
	 *  Attempts to allocate memory in the range [min_addr, max_addr]. This
	 *  function will FAIL if the memory allocation does not get placed in
	 *  this range.
	 *
	 *  @warning
	 *  This function is "hard", meaning that this function will FAIL if
	 *  the allocated memory is not in the requested range, unlike
	 *  #SK_MemIntf.take_soft ().
	 *
	 *  @warning
	 *  The requested range [min_addr, max_addr] is for the starting
	 *  address of the block. The highest memory address of the block CAN
	 *  go outside of this range.
	 *
	 *  @warning
	 *  Sometime after calling this function, SK_MemIntf.free () MUST be
	 *  used to deallocate the allocated memory, provided that the memory
	 *  allocation was successful.
	 *
	 *  @warning
	 *  If (num_elems * elem_size) overflows a #size_t value, then this
	 *  function MUST fail.
	 *
	 *  @warning
	 *  This function MAY be unimplemented by implementations of
	 *  #SK_MemIntf. Use SK_MemIntf.list_impl () to determine whether or
	 *  not it is.
	 *
	 *  @param addr		Where to place a pointer to the memory block.
	 *  			This will be set to NULL if the memory
	 *  			allocation fails.
	 *
	 *  			This argument can NOT be NULL.
	 *
	 *  @param num_elems	The number of elements to allocate within the
	 *			memory block.
	 *
	 *			This can NOT be 0.
	 *
	 *  @param elem_size	The size of each element measured in bytes.
	 *
	 *			This can NOT be 0.
	 *
	 *  @param min_addr	The minimum address of the target address
	 *			range.
	 *
	 *  @param max_addr	The maximum address of the target address range
	 *
	 *  @return		Standard status code.
	 *
	 *			If and only if the memory allocation is
	 *			successful, then #SLY_SUCCESS is returned.
	 */

	SlySr (* take_hard)
	(
		void **		addr,

		size_t		num_elems,

		size_t		elem_size,

		void *		min_addr,

		void *		max_addr
	);




	//FIXME : Should free () use a (void **) argument instead of a (void *)
	//argument, for consistency with the rest of this interface? Or is the
	//current style more ergonomic?

	/*!
	 *  Deallocates the memory associated with the memory block associated
	 *  with #addr.
	 *
	 *  @warning
	 *  This function should ONLY be used on memory blocks that have been
	 *  allocated with the memory allocating functions in the same instance
	 *  of #SK_MemIntf.
	 *
	 *  @warning
	 *  This function should NOT be used on the same #addr twice without a
	 *  memory allocation occurring between those calls.
	 *
	 *  @param addr		The address associated with the memory block
	 *			that should be deallocated.
	 *
	 *			This CAN be NULL, in which case no memory block
	 *			is deallocated.
	 *
	 *  @return		Standard status code.
	 */

	SlyDr (* free)
	(
		void *		addr
	);




	/*!
	 *  Reports the current amount of memory available for allocation on
	 *  the current system. The reported amount is a best-effort value, it
	 *  is not guaranteed to be exact.
	 *
	 *  @warning
	 *  The reported amount of available memory is NOT guaranteed to be
	 *  contiguous.
	 *
	 *  @warning
	 *  It is possible that other threads / processes can consume memory
	 *  quickly after this call completes. Therefore, a caller should
	 *  be prepared to handle the actual amount of memory available
	 *  deviating quickly from the reported amount of memory.
	 *
	 *  In instances where memory usage changes rapidly and a specific
	 *  amount of memory is required, it is recommended that this function
	 *  is "polled" several times over a small interval of time.
	 *
	 *  @warning
	 *  This function MAY be unimplemented by implementations of
	 *  #SK_MemIntf. Use SK_MemIntf.list_impl () to determine whether or
	 *  not it is.
	 *
	 *  @param avail_mem	Where to place the reported amount of memory.
	 *  			If this function fails, 0 will be placed at
	 *  			(*avail_mem).
	 *
	 *  @return		Standard status code
	 */

	SlyDr (* mem_avail)
	(
		size_t *	avail_mem
	);




	//TODO : Should there be functions #lock () and #unlock () to lock the
	//memory manager for use by a single thread when multiple threads are
	//allocating memory?
	//
	//Keep in mind, GR 6 in the documentation for #SK_MemIntf already
	//states that all of the functions in #SK_MemIntf must be thread-safe,
	//so #lock () / #unlock () would NOT exist for the purpose of
	//thread-safety.
	//
	//Rather, such functions would exist so that one thread can guarantee
	//that it is the only new consumer of memory in a program at a time.
	//Doing this removes the variance associated with available memory at
	//the cost of blocking other threads from completing allocations.
	//
	//However, such blocking would realistically only be useful in
	//circumstances where near 100% memory utilization is occurring. Since
	//such high utilization is generally undesirable by users anyway,
	//perhaps providing such functions in #SK_MemIntf provides too little
	//benefit to justfiy the work from #SK_MemIntf implementations?




	/*!
	 *  Lists which functions in this #SK_MemIntf implementation are
	 *  actually implemented and which are left unimplemented.
	 *
	 *  @param intf		Where to place a copy of this #SK_MemIntf,
	 *			except with the functions pointers for
	 *			unimplemented functions set to NULL.
	 *
	 *			This must NOT be NULL.
	 *
	 *  @return		Standard status code
	 */

	SlyDr (* list_impl)
	(
		SK_MemIntf *	intf
	);




	/*!
	 *  Sets the requested configuration for this implementation of
	 *  #SK_MemIntf.
	 *
	 *  A well-behaved implementation should make a best-effort attempt at
	 *  respecting the configuration that is requested by the caller.
	 *
	 *  FIXME : Should there be a get_conf () to go along with this
	 *  function? Or would that lead to callers unintentionally passing
	 *  information to one another thus leading to unintended coupling
	 *  between functions?
	 *
	 *  @param conf		The memory manager configuration being
	 *			requested
	 *
	 *  @return		Standard status code
	 */

	SlyDr (* set_conf)
	(
		const SK_MemIntfConf *	conf
	);
}

SK_MemIntf;




SlySr SK_MemIntf_no_impl_take
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size
);




SlySr SK_MemIntf_no_impl_retake
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size
);




SlySr SK_MemIntf_no_impl_take_near
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		near_addr
);




SlySr SK_MemIntf_no_impl_take_soft
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		min_addr,

	void *		max_addr
);




SlySr SK_MemIntf_no_impl_take_hard
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		min_addr,

	void *		max_addr
);




SlyDr SK_MemIntf_no_impl_free
(
	void *		addr
);




SlyDr SK_MemIntf_no_impl_mem_avail
(
	size_t *	avail_mem
);




SlyDr SK_MemIntf_no_impl_list_impl
(
	SK_MemIntf *	intf
);




SlyDr SK_MemIntf_no_impl_set_conf
(
	const SK_MemIntfConf *	conf
);




SlySr SK_MemIntf_tb
(
	const SK_MemIntf *	skm
);




/*!
 *  # FOOT-NOTES
 *
 *  - Naming of #SK_MemIntf
 *
 *  	Originally, #SK_MemIntf was named "SK_MemReqIntf", because "SK_MemIntf"
 *  	was considered too generic. After all, #SK_MemIntf is only an interface
 *  	for allocating / deallocating memory, not an interface for all
 *  	conceivable operations that can be performed on a set of memory.
 *
 *  	However, since memory managers are such a basic tool in C, the name
 *  	"SK_MemIntf" would probably suggest a memory allocation / deallocation
 *  	interface to a normal C programmer with no further qualification.
 *
 *  	Therefore, the name #SK_MemIntf was chosen instead.
 *
 *
 *  - Configuration Style of #SK_MemIntf
 *
 *	In #SK_BoxIntf, pointers to #SK_BoxReqs instances are passed along with
 *	each function to represent configurations of storage objects.
 *
 *	In #SK_MemIntf, such a mechanism was heavily considered to communicate
 *	configuration options for memory managers, but not used. Instead, the
 *	configuration is communicated via separate functions at the moment.
 *
 *	The reasoning is as follows:
 *
 *	Memory allocators can be described as a system that transforms a shared
 *	pool of memory into a local pool of memory for the allocation
 *	requester. Since the memory is a shared resource, the results of memory
 *	allocations from one source of requests has side effects on the
 *	processing of other requests. i.e. if one function / program requests
 *	memory allocations, there will be less memory available for other
 *	functions / programs to allocate.
 *
 *	Therefore, the configuration requested by one caller affects the
 *	processing of requests by other callers. So is it appropriate for every
 *	caller to bring a configuration along with their allocation requests?
 *
 *	Generally speaking, if a configuration affects a shared resource, the
 *	configuration is most appropriately done by a function / program that
 *	"envelops" all the other functions / programs that will share that
 *	resource.
 *
 *	So is it better for the configuration of #SK_MemIntf to be done via a
 *	distinct set of functions, to be used by the caller that is "outside"
 *	of the requesters of memory allocations?
 *
 *	Currently, the answer is thought to be "yes", and that is the solution
 *	chosen for #SK_MemIntf.
 *
 *	Note that this configuration style of #SK_MemIntf does not necessarily
 *	make the configuration "global", but rather "per-SK_MemIntf instance".
 *	If a generator of #SK_MemIntf instances were ever made, then each
 *	instance could be configured separately using SK_MemIntf.set_conf ().
 *
 *	However, there are still notable counter-arguments in favor of
 *	#SK_BoxReqs-styled configuration of #SK_MemIntf. Perhaps the memory
 *	manager does not always need to respect every configuration option
 *	requested by every caller? Perhaps the memory manager can selectively
 *	apply the configuration based on who called it? Perhaps the
 *	configuration can contain an (id) field to distinguish between
 *	different configurations, so the memory manager can store multiple
 *	"profiles", and know if a configuration is being updated or replaced?
 *
 *	One very important distinction that must be kept in mind when answering
 *	the above questions is that #SK_BoxReqs represents configurations for
 *	individual storage object instances made via #SK_BoxIntf, but the
 *	configuration of #SK_MemIntf would be per-#SK_MemIntf itself.
 *
 *	If it is revealed that #SK_BoxReqs-styled configuration is actually
 *	the superior design, either #SK_MemIntf will be updated to reflect
 *	that, or a different version of #SK_MemIntf will be made altogether.
 *
 *
 *  - Potentially Unimplemented Functions in #SK_MemIntf
 *
 *  	In the documentation for #SK_MemIntf, a few functions indicate that
 *  	they may be left unimplemented by valid implementations of #SK_MemIntf.
 *
 *	The rationale for this is that #SK_MemIntf is designed such that
 *	malloc () / free () from the C standard library are considered the
 *	lowest-common-denominator.
 *
 *  	Therefore, if a function in this interface can not be implemented with
 *  	those 2 functions as a basis, then it is considered optional for
 *  	#SK_MemIntf implementations.
 */




#endif //SK_MEM_INTF_H

