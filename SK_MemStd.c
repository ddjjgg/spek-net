/*!****************************************************************************
 *
 * @file
 * SK_MemStd.c
 *
 * See SK_MemStd.h for details.
 *
 *****************************************************************************/




#include "SK_MemStd.h"




#include "SK_debug.h"




/*!
 *  Checks if the size of a specified memory block would overflow a #size_t
 *  value.
 *
 *  @param num_elems	The number of elements in the memory block
 *
 *  @param elem_size	The size of each element in the memory block
 *
 *  @return		#SLY_SUCCESS if the memory block would NOT overflow a
 *			#size_t value
 *
 *			#SLY_BAD_ARG if the memory block WOULD overflow a
 *			#size_t value
 */

SlyDr SK_MemStd_overflow_check
(
	size_t		num_elems,

	size_t		elem_size
)
{
	if (num_elems > (SIZE_MAX / elem_size))
	{
		SK_DEBUG_PRINT
		(
			SK_MEM_STD_L_DEBUG,
			"#num_elems = %zu and #elem_size = %zu overflows to "
			"#size = %zu. SIZE_MAX = %zu",
			num_elems,
			elem_size,
			(num_elems * elem_size),
			SIZE_MAX
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Used to perform sanity checking on arguments in SK_MemStd_take ().
 *
 *  @param addr		The #addr argument from SK_MemStd_take ()
 *
 *  @param num_elems	The #num_elems argument from SK_MemStd_take ()
 *
 *  @param elem_size	The #elem_size argument from SK_MemStd_take ()
 *
 *  @return		Standard status code
 */

SlyDr SK_MemStd_take_sanity_check
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MEM_STD_L_DEBUG, addr, SlyDr_get (SLY_BAD_ARG));

	if (0 >= num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_MEM_STD_L_DEBUG,
			"#num_elems = 0, which is invalid."
		);

		return SlyDr_get (SLY_BAD_ARG);
	}

	if (0 >= elem_size)
	{
		SK_DEBUG_PRINT
		(
			SK_MEM_STD_L_DEBUG,
			"#elem_size = 0, which is invalid."
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	SlyDr dr =

	SK_MemStd_overflow_check (num_elems, elem_size);


	return dr;
}




/*!
 *  See #SK_MemIntf.take ()
 */

SlySr SK_MemStd_take
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size
)
{
	//Sanity check on arguments

	SlyDr dr =

	SK_MemStd_take_sanity_check (addr, num_elems, elem_size);

	SK_RES_RETURN (SK_MEM_STD_L_DEBUG, dr.res, SlySr_get (dr.res));


	//Attempt to allocate space using malloc ()

	*addr = malloc (num_elems * elem_size);


	//Check for errors

	if (NULL == *addr)
	{
		return SlySr_get (SLY_BAD_ALLOC);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_MemIntf.retake ()
 */

SlySr SK_MemStd_retake
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_MEM_STD_L_DEBUG, addr, SlySr_get (SLY_BAD_ARG));


	SlyDr dr =

	SK_MemStd_overflow_check (num_elems, elem_size);


	SK_RES_RETURN (SK_MEM_STD_L_DEBUG, dr.res, SlySr_get (dr.res));


	//According to #SK_MemIntf, this function MUST behave like
	//#SK_MemIntf.free () when (num_elems = 0) or (elem_size = 0).
	//
	//realloc () in GLIBC has this behavior already, but this behavior is
	//not guaranteed by POSIX. So, for the sake of portability, this
	//function must ensure this behavior before calling realloc ().

	if ((0 >= num_elems) || (0 >= elem_size))
	{
		if (NULL != (*addr))
		{
			SK_MemStd_free (*addr);

			*addr = NULL;
		}


		return SlySr_get (SLY_SUCCESS);
	}


	//Attempt to reallocate the memory block at (*addr) using realloc ()

	void * new_addr = realloc (*addr, (num_elems * elem_size));

	if (NULL == new_addr)
	{
		return SlySr_get (SLY_BAD_ALLOC);
	}


	//Since the reallocation was successful, make sure to update (*addr)
	//to match #new_addr.
	//
	//Note that the memory block may have moved to a new location in
	//memory.

	*addr = new_addr;


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_MemIntf.take_near ()
 */

SlySr SK_MemStd_take_near
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		near_addr
)
{
	//Note, as far as I know there is no way to request memory near a
	//particular memory region with the memory management available in the
	//C standard library.
	//
	//So, the best-effort attempt to do this is just to allocate memory
	//regularly.

	return SK_MemStd_take (addr, num_elems, elem_size);
}




/*!
 *  See #SK_MemIntf.take_soft ()
 */

SlySr SK_MemStd_take_soft
(
	void **		addr,

	size_t		num_elems,

	size_t		elem_size,

	void *		min_addr,

	void *		max_addr
)
{
	//As far as I know, there is no way to request memory in a specific
	//range with the C standard library.
	//
	//So, the best-effort attempt to do this is just to allocate memory
	//regularly.

	return SK_MemStd_take (addr, num_elems, elem_size);
}




/*!
 *  See #SK_MemIntf.free ()
 */

SlyDr SK_MemStd_free
(
	void *	addr
)
{
	//Note, no NULL check here because #addr is allowed to be NULL by the
	//interface. Also, free () SHOULD check for NULL in any reasonable
	//implementation.

	free (addr);


	return SlyDr_get (SLY_SUCCESS);
}



/*!
 *  See #SK_MemIntf.list_impl ()
 */

SlyDr SK_MemStd_list_impl
(
	SK_MemIntf *	intf
)
{
	//Sanity check on argument

	SK_NULL_RETURN (SK_MEM_STD_L_DEBUG, intf, SlyDr_get (SLY_BAD_ARG));


	//Indicate which functions in #SK_MemStd are unimplemented

	*intf = SK_MemStd;


	intf->take_hard = NULL;

	intf->mem_avail = NULL;


	return SlyDr_get (SLY_SUCCESS);
}





/*!
 *  See #SK_MemIntf.set_conf ()
 */

SlyDr SK_MemStd_set_conf
(
	const SK_MemIntfConf *	conf
)
{
	//Sanity check on argument

	//SK_NULL_RETURN (SK_MEM_STD_L_DEBUG, conf, SlyDr_get (SLY_BAD_ARG));


	//As far as I know, there is no way in the C standard library to make
	//malloc () / free () care about any of the hints in #SK_MemIntfConf.
	//
	//Therefore, the best-effort attempt at respecting these hints is to
	//simply return #SLY_SUCCESS here.

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  An implementation of #SK_MemIntf using the functions considered standard
 *  on the running platform.
 *
 *  Generally, this means preferring what's available in the C standard
 *  library, and then using what's provided by the operating system afterwards.
 */

const SK_MemIntf SK_MemStd =

{
	.take =				SK_MemStd_take,

	.retake =			SK_MemStd_retake,

	.take_near =			SK_MemStd_take_near,

	.take_soft =			SK_MemStd_take_soft,

	.take_hard =			SK_MemIntf_no_impl_take_hard,

	.free =				SK_MemStd_free,

	.mem_avail =			SK_MemIntf_no_impl_mem_avail,

	.list_impl =			SK_MemStd_list_impl,

	.set_conf =			SK_MemStd_set_conf
};

