/*!****************************************************************************
 *
 * @file
 * SK_StatArray.c
 *
 * See SK_StatArray.h for details.
 *
 *****************************************************************************/




#include "SK_StatArray.h"




#include <stdio.h>
#include <stdlib.h>
#include <SlyResult-0-x-x.h>




#include "SK_BoxIntf.h"
#include "SK_debug.h"
#include "SK_MemStd.h"
#include "SK_misc.h"




/*!
 *  @defgroup SK_StatArray
 *
 *  @{
 *
 *  #SK_StatArray is a storage data structure that stores all of its elements
 *  in a singular contiguous memory region. This contiguous memory region
 *  remains a static size for the lifetime of the storage object, hence the
 *  name, "SK_StatArray".
 *
 *  Even though all the elements are stored in a singular contiguous memory
 *  region that does not change size, #SK_StatArray DOES contain a
 *  #num_elems field. This represents how many elements are in-use in this
 *  memory region, NOT the size of the memory region itself. This allows an
 *  #SK_StatArray instance to store any number of elements from
 *  [0, num_stat_elems].
 *
 *  #SK_StatArray is probably most useful as a #SK_Box used to build more
 *  complex #SK_Box's. For example, #SK_StatArray would be very useful as the
 *  element blocks in a linked-list.
 *
 *  #SK_StatArray is also useful for avoiding heap-pointer dereferences,
 *  because if the static memory for an #SK_StatArray is allocated on the
 *  stack, then all of the elements themselves will be on the stack.
 *
 *  Note that #SK_StatArray is very similar to #SK_Array, except that #SK_Array
 *  has a portion that is dynamically-allocated and thus resizeable.
 *
 *
 *  The memory layout of #SK_StatArray is as follows:
 *
 *	-Number of Bits-		-Field-
 *
 *	sizeof (size_t)			num_elems
 *
 *	(num_stat_elems * elem_size)	stat_elem []
 *
 *
 *
 *
 *  @var num_elems		Represents how many elements in the #stat_elem
 *				array are actually in use.
 *
 *				This does NOT represent the size of #stat_elem
 *				itself, only how many elements are IN USE.
 *
 *  @var stat_elem		An array of statically allocated elements.
 *				These are part of the static memory of the data
 *				structure, so this array can not be resized
 *				during the life-time of the object.
 *
 *				However, #num_elems can be changed to signify
 *				that different numbers of elements in this
 *				array are in use.
 *
 *  @var num_stat_elems		This is a parameter that is received from the
 *				#SK_BoxReqs arguments in the #SK_BoxIntf.
 *
 *				This value is NOT stored in the data structure
 *				itself.
 *
 *  @var elem_size		This is a parameter that is received from the
 *				#SK_BoxReqs arguments in the #SK_BoxIntf.
 *
 *				This value is NOT stored in the data structure
 *				itself. *
 *  @}
 */




/*!
 *  Note, the implementation of #SK_StatArray's functions from this point
 *  forward bear a similar resemblance to #SK_Array's functions.
 *
 *  However, because #SK_StatArray...
 *
 *	1) Does not have a dynamically allocated portion
 *
 *	2) Does not create negative / positive slack
 *
 *  ... the functions for #SK_StatArray are far simpler than the analogous
 *  functions for #SK_Array.
 */




//FIXME : The entire implementation of #SK_StatArray needs to go through a
//round of testing.




/*!
 *  Performs a sanity check on an #SK_BoxReqs, seeing if it can be used to
 *  configure a valid #SK_StatArray.
 *
 *  @param req			The #SK_BoxReqs to perform the sanity check on.
 *
 *				(req->stat_r->elem_size) MUST be a positive
 *				non-zero value.
 *
 *				(req->stat_r->hint_num_stat_elems) MUST be a
 *				positive non-zero value.
 *
 *  @return			Standard status code.
 *
 *				SLY_SUCCESS is returned if and only if #req
 *				represents a valid configuration for
 *				#SK_StatArray. Otherwise, SLY_BAD_ARG is
 *				returned.
 */

SlyDr SK_StatArray_req_check
(
	const SK_BoxReqs *		req
)
{
	if (0 >= req->stat_r->elem_size)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"(0 >= req->stat_r->elem_size), "
			"can not use this #req to configure an #SK_StatArray. "
			"#req = %p",
			req
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//This is disabled because it IS technically possible to have an
	//#SK_StatArray with 0 statically-allocated elements.
	//
	//It would be quite useless, because then *_get_resize_limits () would
	//report 0 as the maximum number of elements that the #SK_StatArray
	//instances could hold.
	//
	//However, since the #SK_BoxReqs is otherwise valid, it appears that
	//reporting #req as valid in this case would be the technically
	//correct move.
	//
	//(FIXME : Is this argument valid?)

	//
	// if (0 >= req->stat_r->hint_num_stat_elems)
	// {
	// 	SK_DEBUG_PRINT
	// 	(
	// 		SK_STAT_ARRAY_L_DEBUG,
	// 		"(0 >= req->stat_r->hint_num_stat_elems), "
	// 		"can not use this #req to configure an #SK_StatArray. "
	// 		"#req = %p",
	// 		req
	// 	);
	//
	// 	return SlyDr_get (SLY_BAD_ARG);
	// }


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.inst_stat_mem ()
 */

SlyDr SK_StatArray_inst_stat_mem
(
	const SK_BoxReqs *		req,

	size_t *			num_bytes
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, num_bytes, SlyDr_get (SLY_BAD_ARG));


	SlyDr dr = SK_StatArray_req_check (req);

	SK_RES_RETURN (SK_STAT_ARRAY_L_DEBUG, dr.res, dr);


	//Calculate the size of an #SK_StatArray instance using this #req

	size_t inst_size = 0;


	//Size of #num_elems

	inst_size += sizeof (size_t);


	//Size of #stat_elem []

	inst_size += (req->stat_r->elem_size * req->stat_r->hint_num_stat_elems);


	//Save the calculated instance size

	*num_bytes = inst_size;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with #num_elems in #SK_StatArray.
 *
 *  @param req			The #SK_BoxReqs that represents the general
 *				parameters of the #SK_StatArray
 *
 *  @param offset		Where to place the offset associated with
 *				#num_elems
 *
 *  @return			Standard status code
 */

SlyDr SK_StatArray_offset_num_elems
(
	const SK_BoxReqs *		req,

	size_t *			offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	*offset = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the offset associated with the START of #stat_elem [] in an
 *  #SK_StatArray.
 *
 *  @param req			The #SK_BoxReqs that represents the general
 *				parameters of the #SK_StatArray
 *
 *  @param offset		Where to place the offset associated with
 *				#stat_elem [0]
 *
 *  @return			Standard status code
 */

SlyDr SK_StatArray_offset_stat_elem
(
	const SK_BoxReqs *		req,

	size_t *			offset
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, offset, SlyDr_get (SLY_BAD_ARG));


	*offset = sizeof (size_t);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets the value of #num_elems from an #SK_StatArray.
 *
 *  @param req			The #SK_BoxReqs that represents general
 *				parameters of #ssa
 *
 *  @param ssa			The #SK_StatArray to inspect
 *
 *  @param num_elems		Where to place the value of (ssa->num_elems)
 *
 *  @return			Standard status code
 */

SlyDr SK_StatArray_get_num_elems
(
	const SK_BoxReqs *		req,

	const SK_StatArray *		ssa,

	size_t *			num_elems
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, num_elems, SlyDr_get (SLY_BAD_ARG));


	size_t offset = 0;

	SK_StatArray_offset_num_elems (req, &offset);


	*num_elems = * ((size_t *) (((u8 *) ssa) + offset));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Gets a pointer to (stat_elem [ind]) from an #SK_StatArray
 *
 *  @param req			The #SK_BoxReqs that represents general
 *				parameters of #ssa
 *
 *  @param ssa			The #SK_StatArray to inspect
 *
 *  @param ind			The index of the element in (stat_elem []) to
 *				get the pointer for.
 *
 *				This MUST be less than
 *				(req->stat_r->hint_num_stat_elems).
 *
 *				However, #ind CAN be greater than
 *				(ssa->num_elems), in which case #stat_elem_ptr
 *				will point to an unused element.
 *
 *  @param stat_elem_ptr	Where to place the value of
 *				(& (ssa->stat_elem [ind]))
 *
 *  @return			Standard status code
 */

SlyDr SK_StatArray_get_stat_elem_ptr
(
	const SK_BoxReqs *		req,

	const SK_StatArray *		ssa,

	size_t				ind,

	void **				stat_elem_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, stat_elem_ptr, SlyDr_get (SLY_BAD_ARG));


	if (SK_STAT_ARRAY_H_DEBUG)
	{
		if (ind >= req->stat_r->hint_num_stat_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_H_DEBUG,
				"(ind = %zu), which is greater than "
				"(req->stat_r->hint_num_stat_elems = %zu). "
				"Could not fetch element pointer "
				"from (ssa = %p).",
				ind,
				req->stat_r->hint_num_stat_elems,
				ssa
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	size_t offset = 0;

	SK_StatArray_offset_stat_elem (req, &offset);


	*stat_elem_ptr =

	((void *) (((u8 *) ssa) + offset + (ind * req->stat_r->elem_size)));


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Sets the value of #num_elems in an #SK_StatArray.
 *
 *  @param req			The #SK_BoxReqs that represents general
 *				parameters of the #SK_StatArray
 *
 *  @param ssa			The #SK_StatArray to modify
 *
 *  @param num_elems		The new value of (ssa->num_elems)
 *
 *  @return			Standard status code
 */

SlyDr SK_StatArray_set_num_elems
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_elems
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));


	if (SK_STAT_ARRAY_L_DEBUG)
	{
		if (num_elems > req->stat_r->hint_num_stat_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"(num_elems = %zu) is greater than "
				"(req->stat_r->hint_num_stat_elems = %zu), "
				"which is not possible for a #SK_StatArray. "
				"Refusing to set #num_elems.",
				num_elems,
				req->stat_r->hint_num_stat_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	size_t offset = 0;

	SK_StatArray_offset_num_elems (req, &offset);


	* ((size_t *) (((u8 *) ssa) + offset)) = num_elems;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.get_elem_ptr ()
 */

SlyDr SK_StatArray_get_elem_ptr
(
	const SK_BoxReqs *		req,

	const SK_StatArray *		ssa,

	size_t				ind,

	void **				elem_ptr
)
{
	//Unlike #SK_Array, getting a pointer to one of the static elements is
	//the same as getting any other element for #SK_StatArray

	return SK_StatArray_get_stat_elem_ptr (req, ssa, ind, elem_ptr);
}




/*!
 *  See SK_BoxIntf.get_array ()
 */

SlyDr SK_StatArray_get_array
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				beg_ind,

	size_t				end_ind,

	size_t				array_len,

	void *				array
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, array, SlyDr_get (SLY_BAD_ARG));


	if (SK_STAT_ARRAY_L_DEBUG)
	{
		if (end_ind < beg_ind)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"Order of (beg_ind, end_ind) is wrong.\n"
				"#beg_ind = %zu\n"
				"#end_ind = %zu\n",
				beg_ind,
				end_ind
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Ensure that #end_ind is lower than the max index in #ssa

	size_t ssa_num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &ssa_num_elems);


	if (end_ind >= ssa_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"#end_ind = %zu is too large, #ssa_num_elems = %zu",
			end_ind,
			ssa_num_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//If #array_len is 0, then exit early as nothing needs to be done

	if (0 >= array_len)
	{
		return SlyDr_get (SLY_SUCCESS);
	}


	//Modify #end_ind in the event #array_len is too small for the range
	//[beg_ind, end_ind]

	size_t num_elems =	(end_ind - beg_ind) + 1;

	num_elems =		SK_MIN (num_elems, array_len);

	end_ind =		beg_ind + (num_elems - 1);


	//Determine the number of bytes that will be copied from #ssa to #array

	size_t num_bytes = num_elems * req->stat_r->elem_size;


	//Copy elements from #ssa to #array

	void * elem_ptr = NULL;

	SK_StatArray_get_stat_elem_ptr (req, ssa, beg_ind, &elem_ptr);


	SK_byte_copy (array, elem_ptr, num_bytes);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.set_array ()
 */

SlyDr SK_StatArray_set_array
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				beg_ind,

	size_t				end_ind,

	size_t				array_len,

	void *				array
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, array, SlyDr_get (SLY_BAD_ARG));


	if (SK_STAT_ARRAY_L_DEBUG)
	{
		if (end_ind < beg_ind)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"Order of (beg_ind, end_ind) is wrong.\n"
				"#beg_ind = %zu\n"
				"#end_ind = %zu\n",
				beg_ind,
				end_ind
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Check that #array is in fact large enough to supply the range of
	//elements [beg_ind, end_ind]

	size_t num_elems = (end_ind - beg_ind) + 1;

	if (num_elems > array_len)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"#array_len = %zu is too small for the range [%zu, %zu].",
			array_len,
			beg_ind,
			end_ind
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Ensure that #end_ind is lower than the max index in #ssa

	size_t ssa_num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &ssa_num_elems);


	if (end_ind >= ssa_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"#end_ind = %zu is too large, #ssa_num_elems = %zu",
			end_ind,
			ssa_num_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Determine the number of bytes that will be copied from #array to #ssa

	size_t num_bytes = num_elems * req->stat_r->elem_size;


	//Copy elements from #array to #ssa

	void * elem_ptr = NULL;

	SK_StatArray_get_stat_elem_ptr (req, ssa, beg_ind, &elem_ptr);


	SK_byte_copy (elem_ptr, array, num_bytes);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.rw_elems ()
 */

SlyDr SK_StatArray_rw_elems
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				beg_ind,

	size_t				end_ind,

	SK_BoxIntf_elem_rw_func		rw_func,

	void *				arb_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, rw_func, SlyDr_get (SLY_BAD_ARG));


	if (SK_STAT_ARRAY_L_DEBUG)
	{
		if (end_ind < beg_ind)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"Order of (beg_ind, end_ind) is wrong.\n"
				"#beg_ind = %zu\n"
				"#end_ind = %zu\n",
				beg_ind,
				end_ind
			);

			return SlyDr_get (SLY_BAD_ARG);
		}


		size_t num_elems = 0;

		SK_StatArray_get_num_elems (req, ssa, &num_elems);


		if (end_ind >= num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"[beg_ind, end_ind] = [%zu, %zu], "
				"but #num_elems = %zu",
				beg_ind,
				end_ind,
				num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Iterate element-by-element through the range [beg_ind, end_ind] and
	//apply #rw_func to each element

	size_t elem_size = req->stat_r->elem_size;


	void * elem_ptr = NULL;

	SK_StatArray_get_stat_elem_ptr (req, ssa, beg_ind, &elem_ptr);


	for (size_t elem_sel = beg_ind; elem_sel <= end_ind; elem_sel += 1)
	{
		rw_func
		(
			req,
			elem_ptr,
			elem_sel,
			arb_arg
		);


		elem_ptr =

		((u8 *) elem_ptr) + elem_size;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.get_resize_limits ()
 */

SlyDr SK_StatArray_get_resize_limits
(
	const SK_BoxReqs *	req,

	int *			limits_exist,

	size_t *		min_num_elems,
	size_t *		max_num_elems
)
{

	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));


	if (NULL != limits_exist)
	{
		*limits_exist = 1;
	}


	if (NULL != min_num_elems)
	{
		*min_num_elems = 0;
	}


	if (NULL != max_num_elems)
	{
		*max_num_elems = req->stat_r->hint_num_stat_elems;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.resize_unsafe ()
 */

SlySr SK_StatArray_resize_unsafe
(
	const SK_BoxReqs *		req,

	SK_StatArray*			ssa,

	size_t				new_num_elems
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	if (new_num_elems > req->stat_r->hint_num_stat_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"Could not perform resize operation"
			"#new_num_elems = %zu, but #max_num_elems = %zu as "
			"reported by SK_StatArray_get_resize_limits ().",
			new_num_elems,
			req->stat_r->hint_num_stat_elems
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	//Since #SK_StatArray contains only elements that are
	//statically-allocated for the lifetime of the #SK_StatArray instance,
	//resizing is as simple as modifying the #num_elems field.

	SK_StatArray_set_num_elems (req, ssa, new_num_elems);


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.resize ()
 */

SlySr SK_StatArray_resize
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				new_num_elems,

	SK_BoxIntf_elem_rw_func		elem_init_func,

	void *				arb_arg
)
{
	//Sanity check arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	//Determine how many elements are currently in #ssa

	size_t old_num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &old_num_elems);


	//Attempt to resize #SK_StatArray

	SlySr sr = SK_StatArray_resize_unsafe (req, ssa, new_num_elems);


	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"Failed to resize #ssa = %p to store %zu elements.",
			ssa,
			new_num_elems
		);

		return sr;
	}


	//In the event that #new_num_elems is greater than #old_num_elems,
	//these new elements will have to be initialized

	if (new_num_elems > old_num_elems)
	{
		//Take into account when #elem_init_func is NULL

		if (NULL == elem_init_func)
		{
			elem_init_func = SK_Box_elem_rw_set_zero;
		}


		//Execute #elem_init_func on the newly allocated elements in
		//#ssa

		SK_StatArray_rw_elems
		(
			req,
			ssa,
			old_num_elems,
			(new_num_elems - 1),
			elem_init_func,
			arb_arg
		);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.init_unsafe ()
 */

SlySr SK_StatArray_init_unsafe
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_elems
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	SlyDr dr = SK_StatArray_req_check (req);

	if (SLY_SUCCESS != dr.res)
	{
		return SlySr_get (dr.res);
	}


	if (num_elems > req->stat_r->hint_num_stat_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"Could not perform resize operation"
			"#num_elems = %zu, but #max_num_elems = %zu as "
			"reported by SK_StatArray_get_resize_limits ().",
			num_elems,
			req->stat_r->hint_num_stat_elems
		);

		return SlySr_get (SLY_BAD_ARG);
	}


	//Since all the elements in #ssa are statically allocated for the
	//lifetime of #ssa, no memory allocations need to occur here.
	//
	//So, all this function has to do is set the number of elements in #ssa
	//equal to #num_elems.

	SK_StatArray_set_num_elems (req, ssa, num_elems);


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.init ()
 */

SlySr SK_StatArray_init
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_elems,

	SK_BoxIntf_elem_rw_func		elem_init_func,

	void *				arb_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	//Use SK_StatArray_init_unsafe () to perform the initialization of
	//#ssa's fields.
	//
	//Note that the elements themselves will still be uninitialized after
	//this point, however.

	SlySr sr = SK_StatArray_init_unsafe (req, ssa, num_elems);

	if (SLY_SUCCESS != sr.res)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"Failed to initialize #ssa = %p to store %zu "
			"elements.",
			ssa,
			num_elems
		);

		return sr;
	}


	//Initialize all of the elements now contained in #ssa, if there are
	//any

	if (0 < num_elems)
	{
		//Take into account when #elem_init_func is NULL

		if (NULL == elem_init_func)
		{
			elem_init_func = SK_Box_elem_rw_set_zero;
		}


		//Execute #elem_init_func on all of the elements in #ssa

		SK_StatArray_rw_elems
		(
			req,
			ssa,
			0,
			(num_elems - 1),
			elem_init_func,
			arb_arg
		);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.deinit ()
 */

SlyDr SK_StatArray_deinit
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));


	//Since there are no dynamic memory allocations to speak of inside of
	//#SK_StatArray instances, there is nothing that needs to be done for
	//#ssa to deinitialize it.
	//
	//Simply for the purposes of helping prevent accidental re-use after
	//deinitialization, however, (ssa->num_elems) will be set to 0.

	return SK_StatArray_set_num_elems (req, ssa, 0);
}




/*!
 *  See SK_BoxIntf.inst_dyna_mem ()
 */

SlyDr SK_StatArray_inst_dyna_mem
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t *			size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, size, SlyDr_get (SLY_BAD_ARG));


	//Since there is no dynamically allocated memory associated with
	//#SK_StatArray instances, simply set #size to 0.

	*size = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.need_req_regi ()
 */

SlyDr SK_StatArray_need_req_regi
(
	const SK_BoxReqs *		req,

	int *				need_req_regi
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, need_req_regi, SlyDr_get (SLY_BAD_ARG));


	*need_req_regi = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.minify ()
 */

SlySr SK_StatArray_minify
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	//Since there is only statically allocated memory associated with the
	//lifetime of #SK_StatArray instances, there is no need for any memory
	//reallocation, nor is there any slack to speak of.
	//
	//Therefore, there is nothing that this function needs to do.

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.optimize ()
 */

SlySr SK_StatArray_optimize
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	double				size_worth,

	double				speed_worth
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	//Since the memory layout of #SK_StatArray instances does not get
	//modified during its lifetime, there is no room for optimization
	//beyond its current memory layout.
	//
	//Therefore, there is nothing that this function needs to do.

	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.anc_support ()
 */

SlyDr SK_StatArray_anc_support
(
	const SK_BoxReqs *		req,

	int *				support
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, support, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not useful for #SK_StatArray for the same reason as in
	//#SK_Array. The position of every element IS known from the
	//information in the statically-allocated memory of #SK_StatArray.

	*support = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.anc_recommend ()
 */

SlyDr SK_StatArray_anc_recommend
(
	const SK_BoxReqs *		req,

	const SK_StatArray *		ssa,

	int *				recommend
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, recommend, SlyDr_get (SLY_BAD_ARG));


	//See note in SK_StatArray_anc_support () about why anchors are not
	//used in #SK_StatArray

	*recommend = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.anc_size ()
 */

SlyDr SK_StatArray_anc_size
(
	const SK_BoxReqs *		req,

	size_t *			size
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, size, SlyDr_get (SLY_BAD_ARG));


	//#SK_StatArray does not support anchors, so this is reflected in #size

	*size = 0;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See #SK_BoxIntf.anc_validate ()
 */

SlyDr SK_StatArray_anc_validate
(
	const SK_BoxReqs *		req,

	const SK_StatArray *		ssa,

	const SK_BoxAnc *		anc,

	size_t *			ind,

	int *				is_valid
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));


	//Since #SK_StatArray does not support anchors, simply report that any
	//anchor given is invalid

	if (NULL != ind)
	{
		*ind = 0;
	}

	if (NULL != is_valid)
	{
		*is_valid = 0;
	}


	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, anchors are not supported by #SK_StatArray."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.anc_ind ()
 */

SlyDr SK_StatArray_anc_ind
(
	const SK_BoxReqs *		req,

	const SK_StatArray *		ssa,

	const SK_BoxAnc *		anc,

	size_t *			ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ind, SlyDr_get (SLY_BAD_ARG));


	//Since #SK_StatArray does not support anchors, simply report that any
	//anchor given is invalid

	if (NULL != ind)
	{
		*ind = 0;
	}


	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, anchors are not supported by #SK_StatArray."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.anc_inc ()
 */

SlyDr SK_StatArray_anc_inc
(
	const SK_BoxReqs *		req,

	const SK_StatArray *		ssa,

	const SK_BoxAnc *		anc
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));


	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, anchors are not supported by #SK_StatArray."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.anc_dec ()
 */

SlyDr SK_StatArray_anc_dec
(
	const SK_BoxReqs *		req,

	const SK_StatArray *		ssa,

	const SK_BoxAnc *		anc
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));


	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, anchors are not supported by #SK_StatArray."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.get_anc ()
 */

SlyDr SK_StatArray_get_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	SK_BoxAnc *			anc
)
{
	//Sanity check on arguments
	//
	//(This may seem useless since this function will simply return
	//#SLY_NO_IMPL anyway, but it helps catch bugs when a caller is using
	//this interface and then switched it out with another #SK_BoxIntf.)

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));


	size_t num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &num_elems);


	if (ind >= num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"#ind = %zu is out of range, "
			"#num_elems = %zu",
			ind,
			num_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, anchors are not supported by #SK_StatArray."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.region_avg_len ()
 */

SlyDr SK_StatArray_region_avg_len
(
	const SK_BoxReqs *		req,

	const SK_StatArray *		ssa,

	double *			avg_len
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, avg_len, SlyDr_get (SLY_BAD_ARG));


	//Since in #SK_StatArray instances there is only one contiguous memory
	//region in which elements are stored, report the length of the in-use
	//portion as the average region length.

	size_t num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &num_elems);


	*avg_len = num_elems;


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.get_region_ptr ()
 */

SlyDr SK_StatArray_get_region_ptr
(
	const SK_BoxReqs *	req,

	SK_StatArray *		ssa,

	size_t			ind,

	void **			reg_ptr,

	size_t *		reg_beg,

	size_t *		reg_end
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));


	//Determine the number of elements in #ssa

	size_t num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &num_elems);


	if (ind >= num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"#ind = %zu is out of range, "
			"#num_elems = %zu",
			ind,
			num_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Since all elements in an #SK_StatArray intance are stored in one
	//contiguous memory region, always report that region with #reg_ptr

	if (NULL != reg_ptr)
	{
		SK_StatArray_get_stat_elem_ptr (req, ssa, 0, reg_ptr);
	}

	if (NULL != reg_beg)
	{
		*reg_beg = 0;
	}

	if (NULL != reg_end)
	{
		*reg_end = (num_elems - 1);
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.get_elem ()
 */

SlyDr SK_StatArray_get_elem
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	void *				elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	if (SK_STAT_ARRAY_L_DEBUG)
	{
		//Check if #ind is out of range or not

		size_t num_elems = 0;

		SK_StatArray_get_num_elems (req, ssa, &num_elems);


		if (ind >= num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"#ind = %zu is out of range, num_elems = %zu",
				ind,
				num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Get a pointer to the element at #ind in #ssa

	void * src_elem_ptr = NULL;

	SK_StatArray_get_elem_ptr (req, ssa, ind, &src_elem_ptr);


	//Copy from the element in #ssa to #elem, using
	//#req->stat_i->prim_copy () if it is available.

	if (req->stat_i->prim_flag)
	{
		return req->stat_i->prim_copy (elem, src_elem_ptr);
	}

	else
	{
		return SK_byte_copy (elem, src_elem_ptr, req->stat_r->elem_size);
	}
}




/*!
 *  See SK_BoxIntf.set_elem ()
 */

SlyDr SK_StatArray_set_elem
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	const void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	if (SK_STAT_ARRAY_L_DEBUG)
	{
		//Check if #ind is out of range or not

		size_t num_elems = 0;

		SK_StatArray_get_num_elems (req, ssa, &num_elems);


		if (ind >= num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"#ind = %zu is out of range, num_elems = %zu",
				ind,
				num_elems
			);


			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Get a pointer to the element at #ind in #ssa

	void * dst_elem_ptr = NULL;

	SK_StatArray_get_elem_ptr (req, ssa, ind, &dst_elem_ptr);


	//Copy from #elem to the element in #ssa, using
	//#req->stat_i->prim_copy () if it is available

	if (req->stat_i->prim_flag)
	{
		return req->stat_i->prim_copy (dst_elem_ptr, elem);
	}

	else
	{
		return SK_byte_copy (dst_elem_ptr, elem, req->stat_r->elem_size);
	}
}




/*!
 *  See SK_BoxIntf.swap ()
 */

SlyDr SK_StatArray_swap
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind_0,

	size_t				ind_1
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));


	if (SK_STAT_ARRAY_L_DEBUG)
	{
		size_t num_elems = 0;

		SK_StatArray_get_num_elems (req, ssa, &num_elems);


		if ((ind_0 >= num_elems) || (ind_1 >= num_elems))
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"#ind_0 = %zu and #ind_1 = %zu were not "
				"both below #num_elems = %zu",
				ind_0,
				ind_1,
				num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Get pointers to the elements that have to be swapped

	void * elem_0_ptr = NULL;
	void * elem_1_ptr = NULL;

	SK_StatArray_get_stat_elem_ptr (req, ssa, ind_0, &elem_0_ptr);
	SK_StatArray_get_stat_elem_ptr (req, ssa, ind_1, &elem_1_ptr);


	//Swap the element locations, using #req->stat_i->prim_swap () if it
	//is available

	if (req->stat_i->prim_flag)
	{
		return req->stat_i->prim_swap (elem_0_ptr, elem_1_ptr);
	}

	else
	{
		return SK_byte_swap (elem_0_ptr, elem_1_ptr, req->stat_r->elem_size);
	}
}




/*!
 *  See SK_BoxIntf.swap_range ()
 */

SlyDr SK_StatArray_swap_range
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_swap,

	size_t				ind_0,

	size_t				ind_1
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));


	if (0 >= num_swap)
	{
		//If #num_elems is 0, then there is no work to perform

		return SlyDr_get (SLY_SUCCESS);
	}


	if (SK_ARRAY_L_DEBUG)
	{
		size_t num_elems = 0;

		SK_StatArray_get_num_elems (req, ssa, &num_elems);


		size_t low_ind =	SK_MIN (ind_0, ind_1);

		size_t high_ind =	SK_MAX (ind_0, ind_1);


		if ((high_ind + num_swap - 1) >= num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"#num_swap = %zu, #ind_0 = %zu, #ind_1 = %zu "
				"did not place the ranges below "
				"#num_elems = %zu",
				num_swap,
				ind_0,
				ind_1,
				num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}


		if ((low_ind + num_swap - 1) >= high_ind)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"#num_swap = %zu, #ind_0 = %zu, #ind_1 = %zu "
				"described overlapping ranges, can not "
				"perform swap operation.",
				num_swap,
				ind_0,
				ind_1
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//Get pointers to the start of the element ranges that will be swapped

	void * elem_0_ptr = NULL;
	void * elem_1_ptr = NULL;

	SK_StatArray_get_elem_ptr (req, ssa, ind_0, &elem_0_ptr);
	SK_StatArray_get_elem_ptr (req, ssa, ind_1, &elem_1_ptr);


	size_t range_size = num_swap * req->stat_r->elem_size;


	//Swap the element ranges

	return SK_byte_swap (elem_0_ptr, elem_1_ptr, range_size);
}




/*!
 *  See SK_BoxIntf.ins_range_unsafe ()
 */

SlySr SK_StatArray_ins_range_unsafe
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_ins_elems,

	size_t				ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	//If #num_ins_elems is 0, exit early because no elements need to be
	//inserted then

	if (0 >= num_ins_elems)
	{
		return SlySr_get (SLY_SUCCESS);
	}


	//#SK_StatArray instances only have a statically allocated portion, so
	//determine if #ssa has space for the elements that are about to be
	//inserted

	size_t old_num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &old_num_elems);


	size_t new_num_elems = old_num_elems + num_ins_elems;


	if (ind > old_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"(ind = %zu) is outside the index range of #ssa, "
			"(old_num_elems = %zu)",
			ind,
			old_num_elems
		);


		return SlySr_get (SLY_BAD_ARG);
	}


	if (new_num_elems < old_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"(old_num_elems + num_ins_elems) = (%zu, %zu) "
			"overflows to %zu. Can not perform insertion "
			"operation.",
			old_num_elems,
			num_ins_elems,
			new_num_elems
		);


		return SlySr_get (SLY_BAD_ARG);
	}


	if (new_num_elems > req->stat_r->hint_num_stat_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"(new_num_elems = %zu) is greater than "
			"(max_num_elems = %zu) reported by "
			"get_resize_limits (). Failed to perform insertion "
			"operation.",
			new_num_elems,
			req->stat_r->hint_num_stat_elems
		);


		return SlySr_get (SLY_BAD_ARG);
	}


	//Determine the highest index associated with the element range to be
	//inserted

	size_t ind_end = ind + (num_ins_elems - 1);


	//Get a pointer to the start of the statically allocated elements

	void * stat_elem = NULL;

	SK_StatArray_get_stat_elem_ptr (req, ssa, 0, &stat_elem);


	//Move all the elements currently occupying the range
	//[ind, old_num_elems - 1] to [ind_end, new_num_elems - 1].
	//
	//Note that SK_byte_move () is used instead of SK_byte_copy (), because
	//the 2 index ranges may overlap

	size_t range_size =

	(old_num_elems - ind) * req->stat_r->elem_size;


	SK_byte_move
	(
		((u8 *) stat_elem) + ((ind_end + 1) * req->stat_r->elem_size),
		((u8 *) stat_elem) + (ind * req->stat_r->elem_size),
		range_size
	);


	//Indicate that #new_num_elems number of elements is now stored in #ssa

	SK_StatArray_set_num_elems (req, ssa, new_num_elems);


	//At this point, all the elements in #ssa have been properly resized
	//and the elements have been moved out of the way for the index range
	//[ind, (ind + num_ins_elems - 1)].
	//
	//Note that the elements in the index range
	//[ind, (ind + num_ins_elems - 1)] are still uninitialized at this
	//point in time.
	//
	//Since this is the *_unsafe () version of ins_range* (), the function
	//will now end without initializing these elements.


	return SlySr_get (SLY_SUCCESS);
}



/*!
 *  See SK_BoxIntf.ins_range ()
 */

SlySr SK_StatArray_ins_range
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_elems,

	size_t				ind,

	SK_BoxIntf_elem_rw_func		elem_init_func,

	void *				arb_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	//Insert the element range [ind, (ind + (num_elems - 1))]

	SlySr sr =

	SK_StatArray_ins_range_unsafe (req, ssa, num_elems, ind);


	SK_RES_RETURN (SK_STAT_ARRAY_L_DEBUG, sr.res, sr);


	//If #num_elems is 0, return early because there are no newly inserted
	//elements to initialize

	if (0 >= num_elems)
	{
		return SlySr_get (SLY_SUCCESS);
	}


	//Now it is necessary to initialize the newly inserted range of
	//elements, [ind, ind + (num_elems - 1)]

	if (NULL == elem_init_func)
	{
		elem_init_func = SK_Box_elem_rw_set_zero;
	}

	SK_StatArray_rw_elems
	(
		req,
		ssa,
		ind,
		ind + (num_elems - 1),
		elem_init_func,
		arb_arg
	);


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.ins ()
 */

SlySr SK_StatArray_ins
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	const void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	//Insert the element at #ind

	SlySr sr =

	SK_StatArray_ins_range_unsafe (req, ssa, 1, ind);


	SK_RES_RETURN (SK_STAT_ARRAY_L_DEBUG, sr.res, sr);


	//Set the value of the new element located at #ind

	if (NULL != elem)
	{
		SK_StatArray_set_elem (req, ssa, ind, elem);
	}

	else
	{
		void * ins_elem = NULL;

		SK_StatArray_get_elem_ptr (req, ssa, ind, &ins_elem);


		SK_byte_set_all (ins_elem, 0, req->stat_r->elem_size);
	}


	return SlySr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.ins_beg ()
 */

SlySr SK_StatArray_ins_beg
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	//Insert #elem at index 0 of #ssa

	return SK_StatArray_ins (req, ssa, 0, elem);
}




/*!
 *  See SK_BoxIntf.ins_end ()
 */

SlySr SK_StatArray_ins_end
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlySr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlySr_get (SLY_BAD_ARG));


	//Insert #elem at 1 above the highest index in #ssa (which would be
	//equal to the number of elements)

	size_t num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &num_elems);


	return SK_StatArray_ins (req, ssa, num_elems, elem);
}




/*!
 *  See SK_BoxIntf.rem_range ()
 */

SlyDr SK_StatArray_rem_range
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_rem_elems,

	size_t				ind
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));


	//If #num_rem_elems is 0, exit early because no elements need to be
	//inserted then

	if (0 >= num_rem_elems)
	{
		return SlyDr_get (SLY_SUCCESS);
	}


	//Determine the number of elements in #ssa

	size_t old_num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &old_num_elems);


	//Determine the index of the highest-indexed element being removed

	size_t ind_end = ind + (num_rem_elems - 1);

	if (ind_end < ind)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"(ind + (num_rem_elems - 1)) = (%zu + (%zu - 1)) "
			"overflows to %zu. Can not perform removal operation.",
			ind,
			num_rem_elems,
			ind_end
		);


		return SlyDr_get (SLY_BAD_ARG);
	}


	//Check if #ind_end is in the range [0, (old_num_elems - 1)] or not.

	if (ind_end >= old_num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"#ind_end = %zu is out of range.\n"
			"old_num_elems = %zu\n"
			"ind =           %zu\n"
			"num_rem_elems = %zu\n",
			ind_end,
			old_num_elems,
			ind,
			num_rem_elems
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	//Determine the new number of elements that should be stored in #ssa
	//
	//Note, this calculation should not overflow if the above argument
	//checks were performed correctly.

	size_t new_num_elems = old_num_elems - num_rem_elems;


	//Get a pointer to the start of the statically allocated elements

	void * stat_elem = NULL;

	SK_StatArray_get_stat_elem_ptr (req, ssa, 0, &stat_elem);


	//Move all the elements (if any) currently occupying the range
	//[ind_end + 1, old_num_elems - 1] to
	//[ind, new_num_elems - 1].
	//
	//Note that SK_byte_move () is used instead of SK_byte_copy (), because
	//the 2 index ranges may overlap

	size_t range_size = (old_num_elems - ind_end) * req->stat_r->elem_size;


	SK_byte_move
	(
		((u8 *) stat_elem) + (ind * req->stat_r->elem_size),
		((u8 *) stat_elem) + ((ind_end + 1) * req->stat_r->elem_size),
		range_size
	);


	//Indicate that #new_num_elems number of elements is now stored in #ssa

	SK_StatArray_set_num_elems (req, ssa, new_num_elems);


	//Note, unlike SK_StatArray_ins_range (), this removed elements. So
	//there are no new elements in need of initialization after this
	//operation.

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.rem ()
 */

SlyDr SK_StatArray_rem
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	void *				elem
)
{
	//Sanity check on argument

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));


	//Determine the number of elements in #ssa

	size_t num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &num_elems);


	if (SK_STAT_ARRAY_L_DEBUG)
	{
		//Check if #ind is in the range [0, (num_elems - 1)] or not

		if (ind >= num_elems)
		{
			SK_DEBUG_PRINT
			(
				SK_STAT_ARRAY_L_DEBUG,
				"#ind = %zu is out of range, "
				"#num_elems = %zu",
				ind,
				num_elems
			);

			return SlyDr_get (SLY_BAD_ARG);
		}
	}


	//If #elem is not NULL, retrieve the element at #ind before removing it

	if (NULL != elem)
	{
		SK_StatArray_get_elem (req, ssa, ind, elem);
	}


	//Remove the element with index #ind from #ssa

	SK_StatArray_rem_range (req, ssa, 1, ind);


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  See SK_BoxIntf.rem_beg ()
 */

SlyDr SK_StatArray_rem_beg
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	void *				elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));


	size_t num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &num_elems);


	if (0 >= num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"Attempted to remove element from #ssa when it "
			"contained no elements."
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	return SK_StatArray_rem (req, ssa, 0, elem);
}




/*!
 *  See SK_BoxIntf.rem_end ()
 */

SlyDr SK_StatArray_rem_end
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	void *				elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));


	size_t num_elems = 0;

	SK_StatArray_get_num_elems (req, ssa, &num_elems);


	if (0 >= num_elems)
	{
		SK_DEBUG_PRINT
		(
			SK_STAT_ARRAY_L_DEBUG,
			"Attempted to remove element from #ssa when it "
			"contained no elements."
		);

		return SlyDr_get (SLY_BAD_ARG);
	}


	return SK_StatArray_rem (req, ssa, (num_elems - 1), elem);
}




/*!
 *  See SK_BoxIntf.get_anc_w_anc ()
 */

SlyDr SK_StatArray_get_anc_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc
)
{
	//Anchors are not supported by #SK_StatArray. So here, #cur_anc is
	//ignored and the same actions as #SK_StatArray_get_anc () are taken.

	return SK_StatArray_get_anc (req, ssa, ind, new_anc);
}




/*!
 *  See SK_BoxIntf.get_region_ptr_w_anc
 */

SlyDr SK_StatArray_get_region_ptr_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			prev_anc,

	SK_BoxAnc *			next_anc,

	void **				reg_ptr,

	size_t *			reg_beg,

	size_t *			reg_end
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_get_region_ptr
	(
		req,
		ssa,
		ind,
		reg_ptr,
		reg_beg,
		reg_end
	);
}




/*!
 *  See SK_BoxIntf.get_elem_w_anc ()
 */

SlyDr SK_StatArray_get_elem_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	void *				elem
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_get_elem
	(
		req,
		ssa,
		ind,
		elem
	);
}




/*!
 *  See SK_BoxIntf.get_elem_ptr_w_anc ()
 */

SlyDr SK_StatArray_get_elem_ptr_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	const SK_BoxAnc *		cur_anc,

	const SK_BoxAnc *		new_anc,

	void **				elem_ptr
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_get_elem_ptr
	(
		req,
		ssa,
		ind,
		elem_ptr
	);
}




/*!
 *  See SK_BoxIntf.set_elem_w_anc ()
 */

SlyDr SK_StatArray_set_elem_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	void *				elem
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_set_elem
	(
		req,
		ssa,
		ind,
		elem
	);
}




/*!
 *  See #SK_BoxIntf.get_array_w_anc ()
 */

SlyDr SK_StatArray_get_array_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				beg_ind,

	size_t				end_ind,

	size_t				new_anc_ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	size_t				array_len,

	void *				array
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_get_array
	(
		req,
		ssa,
		beg_ind,
		end_ind,
		array_len,
		array
	);
}




/*!
 *  See #SK_BoxIntf.set_array_w_anc ()
 */

SlyDr SK_StatArray_set_array_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				beg_ind,

	size_t				end_ind,

	size_t				new_anc_ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	size_t				array_len,

	void *				array
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_set_array
	(
		req,
		ssa,
		beg_ind,
		end_ind,
		array_len,
		array
	);
}




/*!
 *  See SK_BoxIntf.rw_elems_w_anc ()
 */

SlyDr SK_StatArray_rw_elems_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				beg_ind,

	size_t				end_ind,

	size_t				new_anc_ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	SK_BoxIntf_elem_rw_func		rw_func,

	void *				arb_arg
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_rw_elems
	(
		req,
		ssa,
		beg_ind,
		end_ind,
		rw_func,
		arb_arg
	);
}




/*!
 *  See SK_BoxIntf.swap_w_anc ()
 */

SlyDr SK_StatArray_swap_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind_0,

	const SK_BoxAnc *		cur_anc_0,

	SK_BoxAnc *			new_anc_0,

	size_t				ind_1,

	const SK_BoxAnc *		cur_anc_1,

	SK_BoxAnc *			new_anc_1
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_swap
	(
		req,
		ssa,
		ind_0,
		ind_1
	);
}




/*!
 *  See SK_BoxIntf.swap_range_w_anc ()
 */

SlyDr SK_StatArray_swap_range_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_elems,

	size_t				ind_0,

	size_t				new_anc_0_ind,

	const SK_BoxAnc *		cur_anc_0,

	SK_BoxAnc *			new_anc_0,

	size_t				ind_1,

	size_t				new_anc_1_ind,

	const SK_BoxAnc *		cur_anc_1,

	SK_BoxAnc *			new_anc_1
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_swap_range
	(
		req,
		ssa,
		num_elems,
		ind_0,
		ind_1
	);
}




/*!
 *  See SK_BoxIntf.ins_w_anc ()
 */

SlySr SK_StatArray_ins_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	const void *			elem
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_ins
	(
		req,
		ssa,
		ind,
		elem
	);
}




/*!
 *  See SK_BoxIntf.ins_beg_w_anc ()
 */

SlySr SK_StatArray_ins_beg_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	const void *			elem
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_ins_beg
	(
		req,
		ssa,
		elem
	);
}




/*!
 *  See SK_BoxIntf.ins_end_w_anc ()
 */

SlySr SK_StatArray_ins_end_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	const void *			elem
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_ins_end
	(
		req,
		ssa,
		elem
	);
}




/*!
 *  See SK_BoxIntf.ins_range_w_anc ()
 */

SlySr SK_StatArray_ins_range_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_elems,

	size_t				ind,

	size_t				new_anc_ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	SK_BoxIntf_elem_rw_func		elem_init_func,

	void *				arb_arg
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_ins_range
	(
		req,
		ssa,
		num_elems,
		ind,
		elem_init_func,
		arb_arg
	);
}




/*!
 *  See SK_BoxIntf.ins_range_unsafe_w_anc ()
 */

SlySr SK_StatArray_ins_range_unsafe_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_elems,

	size_t				ind,

	size_t				new_anc_ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_ins_range_unsafe
	(
		req,
		ssa,
		num_elems,
		ind
	);
}




/*!
 *  See SK_BoxIntf.rem_w_anc ()
 */

SlyDr SK_StatArray_rem_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	void *				elem
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_rem
	(
		req,
		ssa,
		ind,
		elem
	);
}




/*!
 *  See SK_BoxIntf.rem_beg_w_anc ()
 */

SlyDr SK_StatArray_rem_beg_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	void *				elem
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_rem_beg
	(
		req,
		ssa,
		elem
	);
}




/*!
 *  See SK_BoxIntf.rem_end_w_anc ()
 */

SlyDr SK_StatArray_rem_end_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc,

	void *				elem
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_rem_end
	(
		req,
		ssa,
		elem
	);
}




/*!
 *  See SK_BoxIntf.rem_range_w_anc ()
 */

SlyDr SK_StatArray_rem_range_w_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_elems,

	size_t				ind,

	size_t				new_anc_ind,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			new_anc
)
{
	//Anchors are not supported by #SK_StatArray, so use anchor-less
	//version

	return

	SK_StatArray_rem_range
	(
		req,
		ssa,
		num_elems,
		ind
	);
}




/*!
 *  See SK_BoxIntf.get_region_ptr_by_anc ()
 */

SlyDr SK_StatArray_get_region_ptr_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		cur_anc,

	SK_BoxAnc *			prev_anc,

	SK_BoxAnc *			next_anc,

	void **				reg_ptr,

	size_t *			reg_beg,

	size_t *			reg_end
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, cur_anc, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function is not implemented in this "
		"#SK_BoxIntf implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.get_elem_ptr_by_anc ()
 */

SlyDr SK_StatArray_get_elem_ptr_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		anc,

	void **				elem_ptr
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, elem_ptr, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function is not implemented in this "
		"#SK_BoxIntf implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.get_elem_by_anc ()
 */

SlyDr SK_StatArray_get_elem_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		anc,

	void *				elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function is not implemented in this "
		"#SK_BoxIntf implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.set_elem_by_anc ()
 */

SlyDr SK_StatArray_set_elem_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		anc,

	const void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function is not implemented in this "
		"#SK_BoxIntf implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.get_array_by_anc ()
 */

SlyDr SK_StatArray_get_array_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		beg_anc,

	const SK_BoxAnc *		end_anc,

	size_t				array_len,

	void *				array
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, beg_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, end_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, array, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function is not implemented in this "
		"#SK_BoxIntf implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.set_array_by_anc ()
 */

SlyDr SK_StatArray_set_array_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		beg_anc,

	const SK_BoxAnc *		end_anc,

	size_t				array_len,

	const void *			array
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, beg_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, end_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, array, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function is not implemented in this "
		"#SK_BoxIntf implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.rw_elems_by_anc ()
 */

SlyDr SK_StatArray_rw_elems_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		beg_anc,

	const SK_BoxAnc *		end_anc,

	SK_BoxIntf_elem_rw_func		rw_func,

	void *				arb_arg
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, beg_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, end_anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, rw_func, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.swap_by_anc ()
 */

SlyDr SK_StatArray_swap_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	const SK_BoxAnc *		anc_0,

	const SK_BoxAnc *		anc_1
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc_0, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc_1, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.swap_range_by_anc ()
 */

SlyDr SK_StatArray_swap_range_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	size_t				num_elems,

	const SK_BoxAnc *		anc_0,

	const SK_BoxAnc *		anc_1
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc_0, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc_1, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.ins_bef_by_anc ()
 */

SlyDr SK_StatArray_ins_bef_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	SK_BoxAnc *			anc,

	const void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.ins_aft_by_anc ()
 */

SlyDr SK_StatArray_ins_aft_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	SK_BoxAnc *			anc,

	const void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  See SK_BoxIntf.rem_by_anc ()
 */

SlyDr SK_StatArray_rem_by_anc
(
	const SK_BoxReqs *		req,

	SK_StatArray *			ssa,

	SK_BoxAnc *			anc,

	const void *			elem
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, ssa, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, anc, SlyDr_get (SLY_BAD_ARG));

	SK_NULL_RETURN (SK_STAT_ARRAY_L_DEBUG, elem, SlyDr_get (SLY_BAD_ARG));


	//Anchors are not supported by #SK_StatArray, so the *_by_anc ()
	//functions are not possible to implement

	SK_DEBUG_PRINT
	(
		SK_STAT_ARRAY_L_DEBUG,
		"Warning, this function not implemented in this #SK_BoxIntf "
		"implementation."
	);


	return SlyDr_get (SLY_NO_IMPL);
}




/*!
 *  Performs a simple test-bench on #SK_StatArray by calling each
 *  #SK_StatArray_* () function at least once and reporting the results.
 *
 *  As noted in the foot-note "#SK_StatArray test-benching", this function does
 *  not perform an in-depth examination on any particular function.
 *
 *  @warning
 *  This test-bench can report failure, but it should also be manually
 *  verified.
 *
 *  @warning
 *  This test-bench is notably inelegant, but it does not need to be elegant.
 *  Its purpose is simply to call each #SK_StatArray function at least once, to
 *  see if there are any glaring flaws in that singular call.
 */

SlySr SK_StatArray_trivial_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco
)
{
	//Decide on announcing the test bench

	int print_flag = (0 != p_depth);

	//i64 next_depth = SK_p_depth_next (p_depth);


	//Sanity check on arguments

	SK_NULL_RETURN_FP (print_flag, stdout, file, SlySr_get (SLY_BAD_ARG));


	//Announce the test-bench

	SK_TB_BEG_PRINT_STD (print_flag, file, deco);


	//Create an #SK_BoxReqs to configure #SK_StatArray's with

	SK_BoxStatReqs stat_r =

	{
		.mem =				&SK_MemStd,

		.elem_size =			sizeof (f64),

		.hint_num_stat_elems =		0,

		.hint_elems_per_block =		4,
	};


	SK_BoxStatImpl stat_i;


	SK_BoxDynaReqs dyna_r =

	{
		.hint_neg_slack =		0,

		.hint_pos_slack =		0,

		.hint_use_max_num_dyna_blocks =	0,

		.hint_max_num_dyna_blocks =	0,

		.hint_tree_avg_degree =		0
	};


	SK_BoxDynaImpl dyna_i;



	SK_BoxReqs req;

	SK_BoxReqs_prep (&req, &stat_r, &stat_i, &dyna_r, &dyna_i);


	//Check if SK_StatArray_req_check () properly reports success / failure
	//when it is given #SK_BoxReqs instances that can / can not be used to
	//initialize #SK_StatArray instances

	if (print_flag)
	{
		SK_LineDeco_print_opt
		(
			file,
			deco,
			"Intentionally causing SK_StatArray_req_check () to "
			"return failure, expect debug messages here..."
		);


		SK_LineDeco_print_opt (file, deco, "");
	}


	SlyDr dr = SlyDr_get (SLY_SUCCESS);




	stat_r.elem_size = 0;

	SK_BoxReqs_prep (&req, &stat_r, &stat_i, &dyna_r, &dyna_i);


	dr = SK_StatArray_req_check (&req);


	if (SLY_SUCCESS == dr.res)
	{
		SK_DEBUG_PRINT
		(
			print_flag,
			"SK_StatArray_req_check () returned #SLY_SUCCESS when "
			"#elem_size = 0."
		);

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}




	size_t elem_size =		sizeof (f64);

	stat_r.elem_size =		elem_size;

	stat_r.hint_num_stat_elems =	0;

	SK_BoxReqs_prep (&req, &stat_r, &stat_i, &dyna_r, &dyna_i);


	dr = SK_StatArray_req_check (&req);


	if (SLY_SUCCESS == dr.res)
	{
		SK_DEBUG_PRINT
		(
			print_flag,
			"SK_StatArray_req_check () returned #SLY_SUCCESS when "
			"#hint_num_stat_elems = 0."
		);

		return SlySr_get (SLY_UNKNOWN_ERROR);
	}




	stat_r.elem_size =		elem_size;

	stat_r.hint_num_stat_elems =	256;

	SK_BoxReqs_prep (&req, &stat_r, &stat_i, &dyna_r, &dyna_i);


	dr = SK_StatArray_req_check (&req);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_BoxReqs_print (&req, 2, file, deco);

		SK_LineDeco_print_opt (file, deco, "");
	}


	//Check *_inst_stat_mem ()

	size_t inst_stat_mem = 0;

	dr = SK_StatArray_inst_stat_mem (&req, &inst_stat_mem);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "#inst_stat_mem = %zu", inst_stat_mem);
	}


	//Check *_offset_num_elems ()

	size_t offset_num_elems = 0;

	dr = SK_StatArray_offset_num_elems (&req, &offset_num_elems);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "#offset_num_elems = %zu", offset_num_elems);
	}


	//Check *_offset_stat_elem ()

	size_t offset_stat_elem = 0;

	dr = SK_StatArray_offset_stat_elem (&req, &offset_stat_elem);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "#offset_stat_elem = %zu", offset_stat_elem);
	}


	//Allocate the static memory portion of an #SK_StatArray instance

	SK_StatArray * ssa = NULL;

	SlySr sr = req.stat_r->mem->take ((void **) &ssa, 1, inst_stat_mem);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_byte_set_all (ssa, 0, inst_stat_mem);


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "#ssa = %p", ssa);
	}


	//Attempt to read the #num_elems field (before initialization)

	size_t num_elems = 0;

	SK_StatArray_get_num_elems (&req, ssa, &num_elems);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "num_elems = %zu", num_elems);
	}


	//Attempt to read the #stat_elem_ptr field (before initialization)

	void * stat_elem_ptr = NULL;

	SK_StatArray_get_stat_elem_ptr (&req, ssa, 0, &stat_elem_ptr);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "stat_elem_ptr = %p", stat_elem_ptr);
	}


	//Check *_init_unsafe ()

	num_elems = 16;

	sr = SK_StatArray_init_unsafe (&req, ssa, num_elems);

	SK_RES_RETURN (print_flag, sr.res, sr);


	if (print_flag)
	{
		SK_StatArray_get_num_elems (&req, ssa, &num_elems);

		SK_StatArray_get_stat_elem_ptr (&req, ssa, 0, &stat_elem_ptr);


		SK_LineDeco_print_opt (file, deco, "After *_init_unsafe ()");

		SK_LineDeco_print_opt (file, deco, "num_elems = %zu", num_elems);

		SK_LineDeco_print_opt (file, deco, "stat_elem_ptr = %p", stat_elem_ptr);
	}


	//Check *_deinit ()

	dr = SK_StatArray_deinit (&req, ssa);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_StatArray_get_num_elems (&req, ssa, &num_elems);

		SK_StatArray_get_stat_elem_ptr (&req, ssa, 0, &stat_elem_ptr);


		SK_LineDeco_print_opt (file, deco, "After *_deinit ()");

		SK_LineDeco_print_opt (file, deco, "num_elems = %zu", num_elems);

		SK_LineDeco_print_opt (file, deco, "stat_elem_ptr = %p", stat_elem_ptr);
	}


	//Check *_init ()

	num_elems = 32;

	f64 arb_arg = 15.0;


	sr = SK_StatArray_init (&req, ssa, num_elems, SK_Box_elem_rw_set_f64, &arb_arg);

	SK_RES_RETURN (print_flag, sr.res, sr);


	if (print_flag)
	{
		SK_StatArray_get_num_elems (&req, ssa, &num_elems);

		SK_StatArray_get_stat_elem_ptr (&req, ssa, 0, &stat_elem_ptr);


		SK_LineDeco_print_opt (file, deco, "After *_init ()");

		SK_LineDeco_print_opt (file, deco, "num_elems = %zu", num_elems);

		SK_LineDeco_print_opt (file, deco, "stat_elem_ptr = %p", stat_elem_ptr);
	}


	//Check *_inst_dyna_mem ()

	size_t inst_dyna_mem = 0;

	dr = SK_StatArray_inst_dyna_mem (&req, ssa, &inst_dyna_mem);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "inst_dyna_mem = %zu", inst_dyna_mem);
	}


	//Check *_need_req_regi ()

	int need_req_regi = 0;

	dr = SK_StatArray_need_req_regi (&req, &need_req_regi);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "need_req_regi = %d", need_req_regi);
	}


	//Check *_minify ()

	sr = SK_StatArray_minify (&req, ssa);

	SK_RES_RETURN (print_flag, sr.res, sr);


	//Check *_optimize ()

	sr = SK_StatArray_optimize (&req, ssa, 1.0, 1.0);

	SK_RES_RETURN (print_flag, sr.res, sr);


	//Check *_anc_support ()

	int anc_support = 0;

	dr = SK_StatArray_anc_support (&req, &anc_support);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "anc_support = %d", anc_support);
	}


	//Check *_anc_recommend ()

	int anc_recommend = 0;

	dr = SK_StatArray_anc_recommend (&req, ssa, &anc_recommend);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "anc_recommend = %d", anc_recommend);
	}


	//Check *_anc_size ()

	size_t anc_size = 0;

	dr = SK_StatArray_anc_size (&req, &anc_size);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "anc_size = %zu", anc_size);
	}


	//Check *_anc_validate ()

	//Note that #fake_anc is used here. Why is #fake_anc used at all? It is
	//because #SK_StatArray does not support anchors, and so valid anchors
	//for it can not be made. However, it is desirable to see what would
	//happen if the *_anc_* () functions were used anyway.

	int fake_anc =			0;

	SK_BoxAnc * fake_anc_ptr =	(SK_BoxAnc *) &fake_anc;

	size_t anc_ind =		0;

	int anc_is_valid =		0;


	dr = SK_StatArray_anc_validate (&req, ssa, fake_anc_ptr, &anc_ind, &anc_is_valid);


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);

		SK_LineDeco_print_opt (file, deco, "anc_ind = %zu", anc_ind);

		SK_LineDeco_print_opt (file, deco, "anc_is_valid = %d", anc_is_valid);
	}


	//Check *_anc_ind ()

	dr = SK_StatArray_anc_ind (&req, ssa, fake_anc_ptr, &anc_ind);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);

		SK_LineDeco_print_opt (file, deco, "anc_ind = %zu", anc_ind);
	}


	//Check *_anc_inc ()

	dr = SK_StatArray_anc_inc (&req, ssa, fake_anc_ptr);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_anc_dec ()

	dr = SK_StatArray_anc_dec (&req, ssa, fake_anc_ptr);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_get_anc ()

	dr = SK_StatArray_get_anc (&req, ssa, 0, fake_anc_ptr);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_region_avg_len ()

	f64 region_avg_len = 0;

	dr = SK_StatArray_region_avg_len (&req, ssa, &region_avg_len);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "region_avg_len = %lf", region_avg_len);
	}


	//Check *_get_region_ptr ()

	void * reg_ptr =	NULL;

	size_t reg_beg =	0;

	size_t reg_end =	0;


	dr = SK_StatArray_get_region_ptr (&req, ssa, num_elems - 1, &reg_ptr, &reg_beg, &reg_end);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "reg_ptr = %p", reg_ptr);

		SK_LineDeco_print_opt (file, deco, "reg_beg = %zu", reg_beg);

		SK_LineDeco_print_opt (file, deco, "reg_end = %zu", reg_end);
	}


	//Check *_get_elem_ptr ()

	f64 * elem_ptr = 0;


	SK_LineDeco_print_opt (file, deco, "*_get_elem_ptr ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem_ptr (&req, ssa, ind, (void **) &elem_ptr);

		SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


		SK_LineDeco_print_opt
		(
			file,
			deco,
			"ind = %02zu, elem_ptr = %p, elem = %lf",
			ind,
			elem_ptr,
			*elem_ptr
		);
	}


	//Check *_get_elem ()

	f64 elem = 0;


	SK_LineDeco_print_opt (file, deco, "*_get_elem ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_set_elem ()

	SK_LineDeco_print_opt (file, deco, "*_set_elem ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = 2 * ind;


		dr = SK_StatArray_set_elem (&req, ssa, ind, &elem);

		SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));



		SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_get_array ()

	SK_LineDeco_print_opt (file, deco, "*_get_array ()");


	size_t elem_array_len = 8;

	f64 elem_array [8];


	dr = SK_StatArray_get_array (&req, ssa, 4, 11, elem_array_len, elem_array);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	SK_LineDeco_print_opt (file, deco, "elem_array []");

	for (size_t ind = 0; ind < elem_array_len; ind += 1)
	{
		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem_array [ind]);
	}


	//Check *_set_array ()

	SK_LineDeco_print_opt (file, deco, "*_set_array ()");


	for (size_t ind = 0; ind < elem_array_len; ind += 1)
	{
		elem_array [ind] = 1000 + ind;
	}


	dr = SK_StatArray_set_array (&req, ssa, 4, 11, elem_array_len, elem_array);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	SK_LineDeco_print_opt (file, deco, "elem_array []");

	for (size_t ind = 0; ind < elem_array_len; ind += 1)
	{
		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem_array [ind]);
	}


	//Check *_swap ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	dr = SK_StatArray_swap (&req, ssa, 0, num_elems - 1);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_swap ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_swap_range ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	dr = SK_StatArray_swap_range (&req, ssa, 8, 0, 8);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_swap_range ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_ins_range_unsafe ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	size_t num_ins = 16;

	num_elems += num_ins;


	sr = SK_StatArray_ins_range_unsafe (&req, ssa, num_ins, 4);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_LineDeco_print_opt (file, deco, "*_ins_range_unsafe ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_ins_range ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_ins = 16;

	num_elems += num_ins;


	sr = SK_StatArray_ins_range (&req, ssa, num_ins, 4, NULL, NULL);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_LineDeco_print_opt (file, deco, "*_ins_range ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_ins ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_ins = 1;

	num_elems += num_ins;


	elem = 1234567.0;


	sr = SK_StatArray_ins (&req, ssa, 4, &elem);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_LineDeco_print_opt (file, deco, "*_ins ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_ins_beg ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_ins = 1;

	num_elems += num_ins;


	elem = 1234567.0;


	sr = SK_StatArray_ins_beg (&req, ssa, &elem);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_LineDeco_print_opt (file, deco, "*_ins_beg ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_ins_end ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_ins = 1;

	num_elems += num_ins;


	elem = 1234567.0;


	sr = SK_StatArray_ins_end (&req, ssa, &elem);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_LineDeco_print_opt (file, deco, "*_ins_end ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_rem_range ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	size_t num_rem = 32;

	num_elems -= num_rem;


	dr = SK_StatArray_rem_range (&req, ssa, num_rem, 4);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_rem_range ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_rem ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_rem = 1;

	num_elems -= num_rem;


	dr = SK_StatArray_rem (&req, ssa, 4, &elem);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_rem ()");


	SK_LineDeco_print_opt (file, deco, "rem_elem = %lf", elem);

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_rem_beg ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_rem = 1;

	num_elems -= num_rem;


	dr = SK_StatArray_rem_beg (&req, ssa, &elem);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_rem_beg ()");


	SK_LineDeco_print_opt (file, deco, "rem_elem = %lf", elem);

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_rem_end ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_rem = 1;

	num_elems -= num_rem;


	dr = SK_StatArray_rem_end (&req, ssa, &elem);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_rem_end ()");


	SK_LineDeco_print_opt (file, deco, "rem_elem = %lf", elem);

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}

	SK_LineDeco_print_opt (file, deco, "");


	//Check *_get_anc_w_anc ()

	dr = SK_StatArray_get_anc_w_anc (&req, ssa, 0, fake_anc_ptr, fake_anc_ptr);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}

	SK_LineDeco_print_opt (file, deco, "");


	//Check *_get_region_ptr_w_anc ()

	reg_ptr = NULL;

	reg_beg = 0;

	reg_end = 0;


	dr =

	SK_StatArray_get_region_ptr_w_anc
	(
		&req,
		ssa,
		num_elems - 1,
		fake_anc_ptr,
		fake_anc_ptr,
		fake_anc_ptr,
		&reg_ptr,
		&reg_beg,
		&reg_end
	);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "reg_ptr = %p", reg_ptr);

		SK_LineDeco_print_opt (file, deco, "reg_beg = %zu", reg_beg);

		SK_LineDeco_print_opt (file, deco, "reg_end = %zu", reg_end);
	}


	//Check *_get_elem_ptr_w_anc ()

	elem_ptr = 0;


	SK_LineDeco_print_opt (file, deco, "*_get_elem_ptr_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr =

		SK_StatArray_get_elem_ptr_w_anc
		(
			&req,
			ssa,
			ind,
			fake_anc_ptr,
			fake_anc_ptr,
			(void **) &elem_ptr
		);

		SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


		SK_LineDeco_print_opt
		(
			file,
			deco,
			"ind = %02zu, elem_ptr = %p, elem = %lf",
			ind,
			elem_ptr,
			*elem_ptr
		);
	}


	//Check *_get_elem_w_anc ()

	elem = 0;


	SK_LineDeco_print_opt (file, deco, "*_get_elem_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr =

		SK_StatArray_get_elem_w_anc
		(
			&req,
			ssa,
			ind,
			fake_anc_ptr,
			fake_anc_ptr,
			&elem
		);

		SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}

	//Check *_set_elem_w_anc ()

	SK_LineDeco_print_opt (file, deco, "*_set_elem_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = 2 * ind;


		dr =

		SK_StatArray_set_elem_w_anc
		(
			&req,
			ssa,
			ind,
			fake_anc_ptr,
			fake_anc_ptr,
			&elem
		);

		SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));



		SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_get_array_w_anc ()

	SK_LineDeco_print_opt (file, deco, "*_get_array_w_anc ()");


	dr =

	SK_StatArray_get_array_w_anc
	(
		&req,
		ssa,
		4,
		11,
		7,
		fake_anc_ptr,
		fake_anc_ptr,
		elem_array_len,
		elem_array
	);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	SK_LineDeco_print_opt (file, deco, "elem_array []");

	for (size_t ind = 0; ind < elem_array_len; ind += 1)
	{
		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem_array [ind]);
	}


	//Check *_set_array_w_anc ()

	SK_LineDeco_print_opt (file, deco, "*_set_array_w_anc ()");


	for (size_t ind = 0; ind < elem_array_len; ind += 1)
	{
		elem_array [ind] = 1000 + ind;
	}


	dr =

	SK_StatArray_set_array_w_anc
	(
		&req,
		ssa,
		4,
		11,
		7,
		fake_anc_ptr,
		fake_anc_ptr,
		elem_array_len,
		elem_array
	);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	SK_LineDeco_print_opt (file, deco, "elem_array []");

	for (size_t ind = 0; ind < elem_array_len; ind += 1)
	{
		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem_array [ind]);
	}


	//Check *_swap_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	dr =

	SK_StatArray_swap_w_anc
	(
		&req,
		ssa,
		0,
		fake_anc_ptr,
		fake_anc_ptr,
		(num_elems - 1),
		fake_anc_ptr,
		fake_anc_ptr
	);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_swap_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_swap_range_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	dr = SK_StatArray_swap_range_w_anc
	(
		&req,
		ssa,
		8,
		0,
		0,
		fake_anc_ptr,
		fake_anc_ptr,
		8,
		8,
		fake_anc_ptr,
		fake_anc_ptr
	);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_swap_range_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_ins_range_unsafe_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_ins = 16;

	num_elems += num_ins;


	sr =

	SK_StatArray_ins_range_unsafe_w_anc
	(
		&req,
		ssa,
		num_ins,
		4,
		4,
		fake_anc_ptr,
		fake_anc_ptr
	);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_LineDeco_print_opt (file, deco, "*_ins_range_unsafe_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_ins_range_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_ins = 16;

	num_elems += num_ins;


	sr =

	SK_StatArray_ins_range_w_anc
	(
		&req,
		ssa,
		num_ins,
		4,
		4,
		fake_anc_ptr,
		fake_anc_ptr,
		NULL,
		NULL
	);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_LineDeco_print_opt (file, deco, "*_ins_range_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_ins_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_ins = 1;

	num_elems += num_ins;


	elem = 1234567.0;


	sr =

	SK_StatArray_ins_w_anc
	(
		&req,
		ssa,
		4,
		fake_anc_ptr,
		fake_anc_ptr,
		&elem
	);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_LineDeco_print_opt (file, deco, "*_ins_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_ins_beg_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_ins = 1;

	num_elems += num_ins;


	elem = 1234567.0;


	sr =

	SK_StatArray_ins_beg_w_anc
	(
		&req,
		ssa,
		fake_anc_ptr,
		fake_anc_ptr,
		&elem
	);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_LineDeco_print_opt (file, deco, "*_ins_beg_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_ins_end_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_ins = 1;

	num_elems += num_ins;


	elem = 1234567.0;


	sr =

	SK_StatArray_ins_end_w_anc
	(
		&req,
		ssa,
		fake_anc_ptr,
		fake_anc_ptr,
		&elem
	);

	SK_RES_RETURN (print_flag, sr.res, sr);


	SK_LineDeco_print_opt (file, deco, "*_ins_end_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_rem_range_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_rem = 32;

	num_elems -= num_rem;


	dr =

	SK_StatArray_rem_range_w_anc
	(
		&req,
		ssa,
		num_rem,
		4,
		4,
		fake_anc_ptr,
		fake_anc_ptr
	);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_rem_range_w_anc ()");


	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_rem_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_rem = 1;

	num_elems -= num_rem;


	dr =

	SK_StatArray_rem_w_anc
	(
		&req,
		ssa,
		4,
		fake_anc_ptr,
		fake_anc_ptr,
		&elem
	);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_rem_w_anc ()");


	SK_LineDeco_print_opt (file, deco, "rem_elem = %lf", elem);

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_rem_beg_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_rem = 1;

	num_elems -= num_rem;


	dr =

	SK_StatArray_rem_beg_w_anc
	(
		&req,
		ssa,
		fake_anc_ptr,
		fake_anc_ptr,
		&elem
	);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_rem_beg_w_anc ()");


	SK_LineDeco_print_opt (file, deco, "rem_elem = %lf", elem);

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}


	//Check *_rem_end_w_anc ()

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		elem = ind;

		SK_StatArray_set_elem (&req, ssa, ind, &elem);
	}


	num_rem = 1;

	num_elems -= num_rem;


	dr =

	SK_StatArray_rem_end_w_anc
	(
		&req,
		ssa,
		fake_anc_ptr,
		fake_anc_ptr,
		&elem
	);

	SK_RES_RETURN (print_flag, dr.res, SlySr_get (dr.res));


	SK_LineDeco_print_opt (file, deco, "*_rem_end_w_anc ()");


	SK_LineDeco_print_opt (file, deco, "rem_elem = %lf", elem);

	for (size_t ind = 0; ind < num_elems; ind += 1)
	{
		dr = SK_StatArray_get_elem (&req, ssa, ind, &elem);

		SK_LineDeco_print_opt (file, deco, "ind = %02zu, elem = %lf", ind, elem);
	}

	SK_LineDeco_print_opt (file, deco, "");


	//Check *_get_region_ptr_by_anc ()

	dr =

	SK_StatArray_get_region_ptr_by_anc
	(
		&req,
		ssa,
		fake_anc_ptr,
		fake_anc_ptr,
		fake_anc_ptr,
		&reg_ptr,
		&reg_beg,
		&reg_end
	);


	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_get_elem_ptr_by_anc ()

	dr = SK_StatArray_get_elem_ptr_by_anc (&req, ssa, fake_anc_ptr, (void **) &elem_ptr);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_get_elem_by_anc ()

	dr = SK_StatArray_get_elem_by_anc (&req, ssa, fake_anc_ptr, &elem);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_set_elem_by_anc ()

	dr = SK_StatArray_set_elem_by_anc (&req, ssa, fake_anc_ptr, &elem);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_get_array_by_anc ()

	dr = SK_StatArray_get_array_by_anc (&req, ssa, fake_anc_ptr, fake_anc_ptr, 1, &elem);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_set_array_by_anc ()

	dr = SK_StatArray_set_array_by_anc (&req, ssa, fake_anc_ptr, fake_anc_ptr, 1, &elem);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_rw_elems_by_anc ()

	dr =

	SK_StatArray_rw_elems_by_anc
	(
		&req,
		ssa,
		fake_anc_ptr,
		fake_anc_ptr,
		SK_Box_elem_rw_set_zero,
		NULL
	);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_swap_by_anc ()

	dr = SK_StatArray_swap_by_anc (&req, ssa, fake_anc_ptr, fake_anc_ptr);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_swap_range_by_anc ()

	dr = SK_StatArray_swap_range_by_anc (&req, ssa, 8, fake_anc_ptr, fake_anc_ptr);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_ins_bef_by_anc ()

	dr = SK_StatArray_ins_bef_by_anc (&req, ssa, fake_anc_ptr, &elem);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_ins_aft_by_anc ()

	dr = SK_StatArray_ins_aft_by_anc (&req, ssa, fake_anc_ptr, &elem);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//Check *_rem_by_anc ()

	dr = SK_StatArray_rem_by_anc (&req, ssa, fake_anc_ptr, &elem);

	if (print_flag)
	{
		SK_LineDeco_print_opt (file, deco, "dr.res = %s", SlyDrStr_get (dr).str);
	}


	//End the test-bench

	SK_TB_END_PRINT_STD
	(
		print_flag,
		file,
		deco,
		0,
		SlySr_get (SLY_UNKNOWN_ERROR),
		SlySr_get (SLY_SUCCESS)
	);
}




//FIXME : Is there a shorter way to instantiate this?

const SK_BoxIntf SK_StatArray_intf =

{
	.inst_stat_mem =

	(SlyDr (*) (const SK_BoxReqs *, size_t *)) SK_StatArray_inst_stat_mem,


	.inst_dyna_mem =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t *))

	SK_StatArray_inst_dyna_mem,


	.need_req_regi =

	(SlyDr (*) (const SK_BoxReqs *, int *))

	SK_StatArray_need_req_regi,


	.init =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_StatArray_init,


	.init_unsafe =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t
		)
	)

	SK_StatArray_init_unsafe,


	.deinit =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *)) SK_StatArray_deinit,


	.get_num_elems =

	(SlyDr (*) (const SK_BoxReqs *, const SK_Box *, size_t *))

	SK_StatArray_get_num_elems,


	.get_resize_limits =

	(SlyDr (*) (const SK_BoxReqs *, int *, size_t *, size_t *))

	SK_StatArray_get_resize_limits,


	.resize =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_StatArray_resize,


	.resize_unsafe =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t
		)
	)

	SK_StatArray_resize_unsafe,


	.minify =

	(SlySr (*) (const SK_BoxReqs *, SK_Box *))

	SK_StatArray_minify,


	.optimize =

	(SlySr (*) (const SK_BoxReqs *, SK_Box *, double, double))

	SK_StatArray_optimize,


	.anc_support =

	(SlyDr (*) (const SK_BoxReqs *, int *))

	SK_StatArray_anc_support,


	.anc_recommend =

	(SlyDr (*) (const SK_BoxReqs *, const SK_Box *, int *))

	SK_StatArray_anc_recommend,


	.anc_size =

	(SlyDr (*) (const SK_BoxReqs *, size_t *))

	SK_StatArray_anc_size,


	.anc_validate =

	(SlyDr (*) (const SK_BoxReqs *, const SK_Box *, const SK_BoxAnc *, size_t *, int *))

	SK_StatArray_anc_validate,


	.anc_ind =

	(SlyDr (*) (const SK_BoxReqs *, const SK_Box *, const SK_BoxAnc *, size_t *))

	SK_StatArray_anc_ind,


	.anc_inc =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, SK_BoxAnc *))

	SK_StatArray_anc_inc,


	.anc_dec =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, SK_BoxAnc *))

	SK_StatArray_anc_dec,


	.get_anc =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, SK_BoxAnc *))

	SK_StatArray_get_anc,


	.region_avg_len =

	(SlyDr (*) (const SK_BoxReqs *, const SK_Box *, double *))

	SK_StatArray_region_avg_len,


	.get_region_ptr =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			void **,

			size_t *,

			size_t *
		)
	)

	SK_StatArray_get_region_ptr,


	.get_elem_ptr =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, void **))

	SK_StatArray_get_elem_ptr,


	.get_elem =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, void *))

	SK_StatArray_get_elem,


	.set_elem =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, const void *))

	SK_StatArray_set_elem,


	.get_array =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, size_t, size_t, void *))

	SK_StatArray_get_array,


	.set_array =

	(SlyDr (*) (const SK_BoxReqs *, SK_Box *, size_t, size_t, size_t, void *))

	SK_StatArray_set_array,


	.rw_elems =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_StatArray_rw_elems,


	.swap =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t
		)
	)

	SK_StatArray_swap,


	.swap_range =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t
		)
	)


	SK_StatArray_swap_range,


	.ins =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const void *
		)
	)

	SK_StatArray_ins,


	.ins_beg =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const void *
		)
	)

	SK_StatArray_ins_beg,


	.ins_end =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const void *
		)
	)

	SK_StatArray_ins_end,


	.ins_range =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_StatArray_ins_range,


	.ins_range_unsafe =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t
		)
	)

	SK_StatArray_ins_range_unsafe,


	.rem =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			void *
		)
	)

	SK_StatArray_rem,


	.rem_beg =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			void *
		)
	)

	SK_StatArray_rem_beg,


	.rem_end =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			void *
		)
	)

	SK_StatArray_rem_end,


	.rem_range =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t
		)
	)

	SK_StatArray_rem_range,


	.get_anc_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *
		)
	)

	SK_StatArray_get_anc_w_anc,


	.get_region_ptr_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			SK_BoxAnc *,

			void **,

			size_t *,

			size_t *
		)
	)

	SK_StatArray_get_region_ptr_w_anc,


	.get_elem_ptr_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			void **
		)
	)

	SK_StatArray_get_elem_ptr_w_anc,


	.get_elem_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			void *
		)
	)

	SK_StatArray_get_elem_w_anc,


	.set_elem_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_StatArray_set_elem_w_anc,


	.get_array_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			size_t,

			void *
		)
	)

	SK_StatArray_get_array_w_anc,


	.set_array_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			size_t,

			const void *
		)
	)

	SK_StatArray_set_array_w_anc,


	.rw_elems_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_StatArray_rw_elems_w_anc,


	.swap_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *
		)
	)

	SK_StatArray_swap_w_anc,


	.swap_range_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *
		)
	)


	SK_StatArray_swap_range_w_anc,


	.ins_w_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_StatArray_ins_w_anc,


	.ins_beg_w_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_StatArray_ins_beg_w_anc,


	.ins_end_w_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_StatArray_ins_end_w_anc,


	.ins_range_w_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_StatArray_ins_range_w_anc,


	.ins_range_unsafe_w_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *
		)
	)

	SK_StatArray_ins_range_unsafe_w_anc,


	.rem_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			void *
		)
	)

	SK_StatArray_rem_w_anc,


	.rem_beg_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			void *
		)
	)

	SK_StatArray_rem_beg_w_anc,


	.rem_end_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			void *
		)
	)

	SK_StatArray_rem_end_w_anc,


	.rem_range_w_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			size_t,

			size_t,

			const SK_BoxAnc *,

			SK_BoxAnc *
		)
	)

	SK_StatArray_rem_range_w_anc,


	.get_region_ptr_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			SK_BoxAnc *,

			SK_BoxAnc *,

			void **,

			size_t *,

			size_t *
		)
	)

	SK_StatArray_get_region_ptr_by_anc,


	.get_elem_ptr_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			void **
		)
	)

	SK_StatArray_get_elem_ptr_by_anc,


	.get_elem_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			void *
		)
	)

	SK_StatArray_get_elem_by_anc,


	.set_elem_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			const void *
		)
	)

	SK_StatArray_set_elem_by_anc,


	.get_array_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			const SK_BoxAnc *,

			size_t,

			void *
		)
	)

	SK_StatArray_get_array_by_anc,


	.set_array_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			const SK_BoxAnc *,

			size_t,

			const void *
		)
	)

	SK_StatArray_set_array_by_anc,


	.rw_elems_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			const SK_BoxAnc *,

			SK_BoxIntf_elem_rw_func,

			void *
		)
	)

	SK_StatArray_rw_elems_by_anc,


	.swap_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			const SK_BoxAnc *,

			const SK_BoxAnc *
		)
	)

	SK_StatArray_swap_by_anc,


	.swap_range_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			size_t,

			const SK_BoxAnc *,

			const SK_BoxAnc *
		)
	)

	SK_StatArray_swap_range_by_anc,


	.ins_bef_by_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_StatArray_ins_bef_by_anc,


	.ins_aft_by_anc =

	(
		SlySr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			SK_BoxAnc *,

			const void *
		)
	)

	SK_StatArray_ins_aft_by_anc,


	.rem_by_anc =

	(
		SlyDr (*)
		(
			const SK_BoxReqs *,

			SK_Box *,

			SK_BoxAnc *,

			void *
		)
	)

	SK_StatArray_rem_by_anc
};




/*!
 *  # FOOT-NOTES
 *
 *  - #SK_StatArray test-benching
 *
 *	Within this file, there is only a small amount of test-bench related
 *	code to be found.
 *
 *	The generic SK_BoxIntf_*_tb () functions are utilized to perform more
 *	in-depth tests on #SK_StatArray, which will attempt more varied access
 *	patterns and more edge-cases.
 */

