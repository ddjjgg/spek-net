/*!****************************************************************************
 *
 * @file
 * SK_main.c
 *
 * Contains main (); currently this is where various test-benches can be
 * loaded.
 *
 *****************************************************************************/




#ifdef COMPILE_MAIN




#include <omp.h>




#include "spek-net.h"




#include "SK_BoxIntf.h"
#include "SK_Array.h"
#include "SK_HmapIntf.h"
#include "SK_HmapStd.h"
#include "SK_MemDebug.h"
#include "SK_MemIntf.h"
#include "SK_MemStd.h"
#include "SK_misc.h"
#include "SK_scratch.h"
#include "SK_StatArray.h"




int main ()
{

	//Set the output debug file pointer to #stdout, and also set the
	//debugging to #SK_H_DEBUG

	SLY_DUP_DEBUG_FILE_PTR (stdout);

	SK_set_all_debug_levels (SK_H_DEBUG);


	//Configure needed OpenMP options

	omp_set_nested (1);




	//SK_Array_set_get_tb ();

	//SK_Array_resize_tb ();

	//SK_Array_init_tb ();

	//SK_Box_calc_new_num_blocks_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std ());

	//SK_Box_le_cmp_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std ());


	const SK_BoxIntf * intf = &SK_StatArray_intf;

	//SK_BoxIntf_init_deinit_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std (), intf);

	SK_BoxIntf_ins_rem_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std (), intf);

	//SK_BoxIntf_set_get_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std (), intf);

	//SK_BoxIntf_array_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std (), intf);

	//SK_BoxIntf_mt_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std (), intf);

	//SK_BoxIntf_speed_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std (), intf);



	//SK_HmapStatReqs_box_calc_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std ());

	//SK_MemDebug_break_allocators = 1;
	//SK_MemIntf_tb (&SK_MemDebug);

	//SK_MemIntf_tb (&SK_MemStd);


	//SK_StatArray_trivial_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std ());


	//SK_inc_chain_tb ();

	//SK_range_oflow_i64_tb ();

	//SK_byte_eq_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std ());

	//SK_byte_copy_tb ();

	//SK_byte_swap_tb ();

	//SK_byte_le_cmp_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std ());

	//SK_omp_4_5_0_sp ();

	//SK_HmapStd_key_pos_tb (SK_P_DEPTH_INF, stdout, SK_LineDeco_std ());


	SLY_CLOSE_DEBUG_FILE ();

	return 0;
}




#endif //COMPILE_MAIN

