/*!****************************************************************************
 *
 * @file
 * SK_FooBarIntf.h
 *
 * Provides a file-template for how interfaces for a data structure can be
 * specified in a *.h header (following the conventions set by #SK_BoxIntf).
 *
 * To make quick use of this file, search and replace the following (where
 * "NewName" is the name of the data structure that the interface is
 * intended for) :
 *
 * 	"FOOBAR" -> "NEWNAME"
 *
 * 	"FooBar" -> "NewName"
 *
 * @warning
 * Note that this file-template specifies an interface that closely mimics the
 * implicit conventions set by "SK_BoxIntf.h". For example, there's a
 * (*StatReqs) structure and (*DynaReqs) structure defined in this header file,
 * and their purpose is to separate the static requested parameters from the
 * dynamic requested parameters. Additionally, the interface #FooBarIntf is
 * specified via a struct full of function pointers, while the data structure
 * #FooBar itself is intentionally left as a struct declaration with no
 * definition.
 *
 * This style of interface-specification allows for more than one
 * implementation of an interface to exist side-by-side, and it allows great
 * freedom for the implementation to determine the memory layout of instances,
 * though it may be quite laborious to implement.
 *
 * If mimicking #SK_BoxIntf is not desired, some other style of interface
 * specification should be pursued.
 *
 * TODO : After using this template, replace this description with one that is
 * appropriate for your data structure's interface.
 *
 *****************************************************************************/




#ifndef SK_FOOBARINTF_H
#define SK_FOOBARINTF_H




//TODO : Include needed system headers here




//TODO : Include needed project headers here




/*!
 *  Used to describe a set of desired constant parameters for an instance of
 *  the data structure implementing the interface #SK_FooBarIntf. These
 *  parameters MUST stay constant for the lifetime of the instance that uses
 *  them.
 *
 *  It is assumed that these parameters will need to be known when the data
 *  structure instance is initialized.
 *
 *  @warning
 *  These parameters should NOT change over the lifetime of the associated
 *  instance of the data structure implementing #SK_FooBarIntf.
 */

typedef struct SK_FooBarStatReqs
{

	//TODO : Fill this struct with the fields needed to configure an
	//instance of the data structure

}

SK_FooBarStatReqs;




/*!
 *  Used to describe a set of IMPLIED constant parameters for an instance of
 *  the data structure implementing the interface #SK_FooBarIntf. These
 *  parameters MUST stay constant for the lifetime of the storage object that
 *  uses them. The values in this struct are derived from values contained in
 *  #SK_FooBarStatReqs.
 *
 *  Unlike #SK_FooBarStatReqs, these values are not permitted to be changed
 *  manually. These values MUST get set using SK_FooBarReqs_prep ().
 *  Otherwise, the values in this struct are not guaranteed to be usable, and
 *  may cause interface operations to fail (i.e. exhibit memory access errors,
 *  attempt to access non-existant values, etc.)
 */

typedef struct SK_FooBarStatImpl
{

	//TODO : Fill this struct with the fields needed to configure an
	//instance of the data structure

}

SK_FooBarStatImpl;




/*!
 *  Used to describe a set of desired dynamic parameters for an instance of the
 *  data structure implementing the interface #SK_FooBarIntf. These
 *  parameters are intended to be modifiable in the lifetime of the storage
 *  object.
 *
 *  While these parameters can change, there is no requirement to do so in any
 *  particular circumstance.
 *
 *  @warning
 *  The fields in this data structure CAN change during the lifetime
 *  of the storage data structure instance.
 *
 *  @warning
 *  If any change is made to this data structure, then SK_FooBarReqs_prep ()
 *  MUST be called once again on the associated #SK_FooBarReqs.
 *
 *  @warning
 *  If a change is made to this data structure, it must be done at a time where
 *  no function is accessing it (usually done via #SK_FooBarReqs). In
 *  multi-threaded scenarios, that typically means some form of external
 *  locking is required if modification of this data structure is desired.
 */

typedef struct SK_FooBarDynaReqs
{

	//TODO : Fill this struct with the fields needed to configure an
	//instance of the data structure

}

SK_FooBarDynaReqs;




/*!
 *  Used to describe a set of IMPLIED dynamic parameters for an instance of the
 *  data structure implementing the interface #SK_FooBarIntf. These parmaeters
 *  CAN change during thelifetime of the storage object that uses them. The
 *  values in this struct are derived from values contained in
 *  #SK_FooBarDynaReqs.
 *
 *  Unlike #SK_FooBarDynaReqs, these values are not permitted to be changed
 *  manually. These values MUST get set using SK_FooBarReqs_prep ().
 *  Otherwise, the values in this struct are not guaranteed to be usable, and
 *  may cause storage object operations to exhibit memory access errors.
 *
 *  @warning
 *  If a change is made to this data structure, it must be done at a time where
 *  no function is accessing it (usually done via #SK_FooBarReqs). In
 *  multi-threaded scenarios, that typically means some form of external
 *  locking is required if modification of this data structure is desired.
 */

typedef struct SK_FooBarDynaImpl
{

	//TODO : Fill this struct with the fields needed to configure an
	//instance of the data structure

}

SK_FooBarDynaImpl;




/*!
 *  Simply contains an #SK_FooBarStatReqs, #SK_FooBarStatImpl,
 *  #SK_FooBarDynaReqs, and #SK_FooBarDynaImpl.
 *
 *  Typically when one of the above data structures is instantiated, it's
 *  useful to instantiate all of them.
 *
 *  Note that this is NOT the data structure that is used to configure
 *  instances of the data structure that implements #SK_FooBarIntf, that is
 *  #SK_FooBarReqs. #SK_FooBarReqs contains pointers to the sub-structures
 *  instead of the sub-structures themselves. See #SK_FooBarReqs's
 *  documentation as to why this is preferable.
 */

typedef struct SK_FooBarAllReqs
{
	SK_FooBarStatReqs		stat_r;

	SK_FooBarStatImpl		stat_i;

	SK_FooBarDynaReqs		dyna_r;

	SK_FooBarDynaImpl		dyna_i;
}

SK_FooBarAllReqs;




/*!
 *  Contains pointers for both the parameters that are constant and the
 *  parameters that are dynamic for the lifetime of an instance of the data
 *  structure that implements #SK_FooBarIntf.
 *
 *  The reason that these parameters are not stored in the instances themselves
 *  is that this reduces the per-instance cost of those objects. 1
 *  #SK_FooBarReqs can describe the parameters for 1000's of
 *  #SK_FooBarIntf-implementing data structure instances.
 *
 *  @warning
 *  The data structure MUST be initialized using SK_FooBarReqs_prep (),
 *  otherwise it is not guaranteed to be in a coherent state.
 *
 *  An exception exists if all the sub-structures are known to be initialized,
 *  and then the pointers to the sub-structures can be set manually.
 *
 *  @note
 *  Note how this data structure contains pointers to the sub-structures
 *  instead of the sub-structures themselves. This allows for a higher-level
 *  data structure (SK_Thing) to also have (SK_ThingStatReqs, SK_ThingStatImpl,
 *  SK_ThingDynaReqs, SK_ThingDynaImpl) structures, allow (SK_FooBarStatReqs,
 *  SK_FooBarStatImpl, SK_FooBarDynaReqs, SK_FooBarDynaImpl) to be included in
 *  those data structures respectively, and then still allow for a proper
 *  #SK_FooBarReqs and #SK_ThingReqs to be formed independently. I.e. the
 *  functions that accept #SK_FooBarReqs are easily usable by #SK_Thing related
 *  functions.
 */

typedef struct SK_FooBarReqs
{

	/*!
	 *  The parameters of the data struture instance that MUST stay
	 *  constant during its lifetime.
	 */

	const SK_FooBarStatReqs *	stat_r;




	/*!
	 *  The parameters automatically implied by #r_stat.
	 *
	 *  This MUST be set by SK_FooBarReqs_prep ().
	 */

	SK_FooBarStatImpl *		stat_i;




	/*!
	 *  The parameters of the data structure instance that are allowed to
	 *  change during its lifetime.
	 */

	const SK_FooBarDynaReqs *	dyna_r;




	/*!
	 *  The parameters automatically implied by #r_dyna.
	 *
	 *  This MUST be set by SK_FooBarReqs_prep ().
	 */

	SK_FooBarDynaImpl *		dyna_i;

}

SK_FooBarReqs;




/*!
 *  A type (WITHOUT a definition) that represents instances of one of the data
 *  structures that implement #SK_FooBarIntf.
 *
 *  This type declaration exists so that pointers like (SK_FooBar *) can be
 *  made and be differentiated from normal (void *) pointers. The memory layout
 *  of objects created via #SK_FooBarIntf is determined totally by the
 *  implementation, so there is no defintiion associated with
 *  the #SK_FooBar anywhere.
 *
 *  The goal is that any instance of any data structure that implements
 *  #SK_FooBarIntf should be safe to cast to #SK_FooBar (though the casting
 *  back is not necessarily safe).
 *
 *  @warning
 *  If #SK_FooBar is given a definition, then #SK_FooBar is almost certainly
 *  being used incorrectly. It should be understood that #SK_FooBar is
 *  intentionally generic with no singular memory layout. What WOULD have a
 *  singular memory layout is a singular implementation of #SK_FooBar.
 */

typedef struct SK_FooBar SK_FooBar;




/*!
 *  Defines a generic interface for data structures that implement
 *  #SK_FooBarIntf.
 *
 *  TODO : After making use of this file-template, add a description of what
 *  this interface is intended to accomplish.
 */

typedef struct SK_FooBarIntf
{




	//TODO : Remove this note itself, which is only here to inform the user
	//of this file-template that these funtions are here because most
	//interfaces will require very similar functions.




	/*!
	 *  Calculates the static memory cost of an #SK_FooBar instance.
	 *
	 *  @param req			The #SK_FooBarReqs describing the
	 *				requested parameters of the #SK_FooBar
	 *				instance
	 *
	 *  				Note that SK_FooBarReqs_prep () must
	 *  				be used on #req before it used by
	 *  				functions in this interface.
	 *
	 *  @param num_bytes		Where to place the calculated static
	 *				memory cost of the #SK_FooBar instance,
	 *				in units of bytes
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* inst_stat_mem)
	(
		const SK_FooBarReqs *		req,

		size_t *			num_bytes
	);




	/*!
	 *  Reports the amount of dynamically-allocated memory the #SK_FooBar
	 *  instance is currently consuming.
	 *
	 *  @warning
	 *  Note how inst_stat_mem () does NOT have an #obj argument; this is
	 *  because the current state of the object is irrelevant to the amount
	 *  of static memory it consumes. That is NOT true with dynamic memory.
	 *  With that in mind, this should only be used on #obj if #obj has
	 *  already been initialized with init ().
	 *
	 *  @warning
	 *  If the dynamic memory cost would overflow #size, it should be set
	 *  to #SIZE_MAX and #SLY_SUCCESS should NOT be used as a return value.
	 *
	 *  FIXME : Would this rule make this function require this function to
	 *  return a #SlySr instead? Should this function even attempt to
	 *  handle #size_t overflows?
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param obj			The #SK_FooBar instance to inspect
	 *
	 *  @param size			Where to place the calculated dynamic
	 *				memory cost of the #SK_FooBar instance,
	 *				in units of bytes
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* inst_dyna_mem)
	(
		const SK_FooBarReqs *	req,

		SK_FooBar *		obj,

		size_t *		size
	);




	/*!
	 *  Initializes the #SK_FooBar instance, which has had memory
	 *  allocated for it but has not had any of its fields initialized.
	 *
	 *  The function inst_stat_mem () MUST be used to determine how much
	 *  memory needs to be allocated for this instance before using
	 *  init ().
	 *
	 *  @warning
	 *  If this function is successful, the caller of this function MUST
	 *  call #deinit on #obj sometime after using this function to avoid
	 *  resource leaks.
	 *
	 *  @warning
	 *  Using init () on a storage object twice without using deinit () in
	 *  between will generally cause memory access errors.
	 *
	 *  @param req			The #SK_FooBarReqs describing
	 *  				the parameters of the #SK_FooBar
	 *  				instance. This should be the same #req
	 *  				argument as what was used in
	 *  				inst_stat_mem ().
	 *
	 *  				Note that init () is NOT permitted to
	 *  				modify #req in any way, it is only
	 *  				permitted to read it.
	 *
	 *  				Note that SK_FooBarReqs_prep () must
	 *  				be used on #req before it used by
	 *  				functions in this interface.
	 *
	 *  @param obj			The object to initialize.
	 *
	 *  				In other words, this should point to a
	 *  				memory region with the size reported by
	 *  				inst_stat_mem ().
	 *
	 *  @return			Standard status code
	 *
	 *  TODO : After using this file template, update this argument list
	 *  (if needed) for whatever information is needed to initialize
	 *  #SK_FooBar instances.
	 *
	 */

	SlySr (* init)
	(
		const SK_FooBarReqs *		req,

		SK_FooBar *			obj
	);




	/*!
	 *  Deinitializes the #SK_FooBar instance, which means
	 *  deallocating all resources (memory, files, etc.) the object uses.
	 *
	 *  @warning
	 *  This should ONLY be called on objects that were previously
	 *  initialized with #init ().
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param obj			The object to deinitialize
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* deinit)
	(
		const SK_FooBarReqs *		req,

		SK_FooBar *			obj
	);

}

SK_FooBarIntf;




//TODO : This JIT version of the interface can be left inside of this header if
//so desired.


/*!
 *  This is a version of #SK_FooBarIntf that makes use of just-in-time (JIT)
 *  compilation at run-time. Utilizing JIT'ing allows for storage objects to be
 *  optimized heavily for the static parameters of a given storage object.
 *
 *  Unlike #SK_FooBarIntf, it must be initialized / deinitialized with
 *  *_init () / *_deinit () functions respectively. These functions will
 *  accept the same kind of #SK_FooBarReqs as used by #SK_FooBarIntf
 *  functions.
 */

typedef struct SK_FooBarIntf SK_FooBarJit;




//TODO : See the example implementation in SK_FooBarIntf.c

SlyDr SK_FooBarReqs_point_to_arq
(
	SK_FooBarReqs *			req,

	const SK_FooBarAllReqs *	arq
);




//TODO : Fill in the missing section of the example implementation for this
//within SK_FooBarIntf.c after making use of this file-template.

SlyDr SK_FooBarStatImpl_prep
(
	const SK_FooBarStatReqs *	stat_r,

	SK_FooBarStatImpl *		stat_i
);




//TODO : Fill in the missing section of the example implementation for this
//within SK_FooBarIntf.c after making use of this file-template.

SlyDr SK_FooBarDynaImpl_prep
(
	const SK_FooBarStatReqs *	stat_r,

	const SK_FooBarStatImpl *	stat_i,

	const SK_FooBarDynaReqs *	dyna_r,

	SK_FooBarDynaImpl *		dyna_i
);




//TODO : See the example implementation in SK_FooBarIntf.c

SlyDr SK_FooBarReqs_prep
(
	SK_FooBarReqs *				req,

	const SK_FooBarStatReqs *		stat_r,

	SK_FooBarStatImpl *			stat_i,

	const SK_FooBarDynaReqs *		dyna_r,

	SK_FooBarDynaImpl *			dyna_i
);




//TODO : See the example implementation in SK_FooBarIntf.c

SlyDr SK_FooBarAllReqs_prep
(
	SK_FooBarAllReqs *			arq,

	const SK_FooBarStatReqs *		stat_r,

	const SK_FooBarDynaReqs *		dyna_r
);




#endif //SK_FOOBARINTF_H

