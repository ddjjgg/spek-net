/*!****************************************************************************
 *
 * @file
 * SK_FooBarIntf.c
 *
 * Companion *.c file for SK_FooBarIntf.h that contains some standard
 * implementations for basic functions.
 *
 * To make quick use of this file, search and replace the following (where
 * "NewName" is the name of the data structure that the interface is
 * intended for) :
 *
 * 	"FOOBAR" -> "NEWNAME"
 *
 * 	"FooBar" -> "NewName"
 *
 *****************************************************************************/




#include "SK_FooBarIntf.h"




//TODO : #include system headers here




//TODO : #include internal project headers here




/*!
 *  Makes an #SK_FooBarReqs have all its member pointers point to the contents
 *  of an #SK_FooBarAllReqs.
 *
 *  @warning
 *  SK_FooBarReqs_prep () will still be necessary to call on the resulting #req
 *  UNLESS SK_FooBarAllReqs_prep () has already been used on #arq.
 *
 *  @param req			The #SK_FooBarReqs that should have all its
 *  				fields point to fields in #arq
 *
 *  @param arq			The #SK_FooBarAllReqs that should have all
 *				of its fields pointed to by #req
 *
 *  @return			Standard status code
 */

SlyDr SK_FooBarReqs_point_to_arq
(
	SK_FooBarReqs *			req,

	const SK_FooBarAllReqs *	arq
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, req, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, arq, SlyDr_get (SLY_BAD_ARG));

	req->stat_r = (const SK_FooBarStatReqs *)	&(arq->stat_r);
	req->stat_i = (SK_FooBarStatImpl *)		&(arq->stat_i);
	req->dyna_r = (const SK_FooBarDynaReqs *)	&(arq->dyna_r);
	req->dyna_i = (SK_FooBarDynaImpl *)		&(arq->dyna_i);

	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Calculates the values within an #SK_FooBarStatImpl based off of the values
 *  contained within an #SK_FooBarStatReqs.
 *
 *  @note
 *  Note how #SK_FooBarDynaReqs and #SK_FooBarDynaImpl are not referenced in
 *  this function. This is because letting a dynamic parameter determine a
 *  static parameter is dangerous, though the reverse is acceptable.
 *
 *  @warning
 *  If #stat_r is detected as representing an impossible configuration, this
 *  function will fail and return without modifiying #stat_i.
 *
 *  @param stat_r		The #SK_FooBarStatReqs that represents the
 *  				requested static parameters of an #SK_FooBar.
 *
 *				This MUST be already initialized.
 *
 *  @param stat_i		Where to place the #SK_FooBarStatImpl that
 *  				will be calculated based off of #stat_r.
 *
 *  @return			Standard status code
 */

SlyDr SK_FooBarStatImpl_prep
(
	const SK_FooBarStatReqs *	stat_r,

	SK_FooBarStatImpl *		stat_i
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, stat_i, SlyDr_get (SLY_BAD_ARG));


	//TODO : Implement this function


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Calculates the values within an #SK_FooBarDynaImpl based off of the values
 *  contained within an #SK_FooBarDynaReqs.
 *
 *  @warning
 *  If #stat_r, #stat_i, or #dyna_r, are detected as representing an impossible
 *  configuration, this function will fail and return without modifiying
 *  #dyna_i.
 *
 *  @param stat_r		The #SK_FooBarStatReqs that represents the
 *  				requested static parameters of an #SK_FooBar.
 *
 *				This MUST be already initialized.
 *
 *  @param stat_i		The #SK_FooBarStatImpl that was previously
 *				calculated using SK_FooBarStatImpl_prep ().
 *
 *				This MUST be already initialized.
 *
 *  @param dyna_r		The #SK_FooBarDynaReqs that represents the
 *				requested dynamic parameters of an #SK_FooBar.
 *
 *				This MUST be already initialized.
 *
 *  @param dyna_i		Where to place the #SK_FooBarDynaImpl that
 *  				will be calculated based off of #stat_r,
 *  				#stat_i, and #dyna_r.
 *
 *  @return			Standard status code
 */

SlyDr SK_FooBarDynaImpl_prep
(
	const SK_FooBarStatReqs *	stat_r,

	const SK_FooBarStatImpl *	stat_i,

	const SK_FooBarDynaReqs *	dyna_r,

	SK_FooBarDynaImpl *		dyna_i
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, stat_i, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, dyna_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, dyna_i, SlyDr_get (SLY_BAD_ARG));


	//TODO : Implement this function


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Prepares an #SK_FooBarReqs to be used by setting its pointers and
 *  initializing the "implied" values.
 *
 *  @warning
 *  If the pointers #stat_r, #stat_i, #dyna_r, or #dyna_i become invalid while
 *  #req is being used, then memory access errors WILL occur.
 *
 *  @warning
 *  If #stat_r or #dyna_r are detected as requesting an impossible
 *  configuration, this function will fail and return without modifiying #req,
 *  #stat_i, or #dyna_i.
 *
 *  @param req			Pointer to the #SK_FooBarReqs to prepare,
 *				which is NOT expected to be initialized.
 *
 *  				(Regardless if the pointers inside of it are
 *  				already set, they will be set to (stat_r,
 *  				stat_i, dyna_r, dyna_i) respectively.)
 *
 *				This CAN be NULL if this argument is not
 *				desired, in which case the other arguments will
 *				simply be prepared.
 *
 *  @param stat_r		Pointer to the #SK_FooBarStatReqs to use,
 *				which IS expected to be manually initialized.
 *
 *  @param stat_i		Pointer to the #SK_FooBarStatImpl to use, which
 *  				is NOT expected to be intialized, but will be
 *  				initalized after this function completes.
 *
 *  @param dyna_r		Pointer to the #SK_FooBarDynaReqs to use, which
 *  				IS expected to be manually initialized.
 *
 *  @param dyna_i		Pointer to the #SK_FooBarDynaImpl to use, which
 *				is NOT expected to be intialized, but will be
 *				initalized after this function completes.
 *
 *  @return			Standard status code
 */

SlyDr SK_FooBarReqs_prep
(
	SK_FooBarReqs *			req,

	const SK_FooBarStatReqs *	stat_r,

	SK_FooBarStatImpl *		stat_i,

	const SK_FooBarDynaReqs *	dyna_r,

	SK_FooBarDynaImpl *		dyna_i
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, stat_i, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, dyna_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, dyna_i, SlyDr_get (SLY_BAD_ARG));


	//Calculate #stat_i from #stat_r

	SlyDr dr = SK_FooBarStatImpl_prep (stat_r, stat_i);

	SK_RES_RETURN (SK_FOOBAR_INTF_L_DEBUG, dr.res, dr);


	//Calculate #dyna_i from #stat_r, #stat_i, and #dyna_r

	dr = SK_FooBarDynaImpl_prep (stat_r, stat_i, dyna_r, dyna_i);

	SK_RES_RETURN (SK_FOOBAR_INTF_L_DEBUG, dr.res, dr);


	//Set the pointers inside of #req

	if (NULL != req)
	{
		req->stat_r = stat_r;

		req->stat_i = stat_i;

		req->dyna_r = dyna_r;

		req->dyna_i = dyna_i;
	}


	return SlyDr_get (SLY_SUCCESS);
}




/*!
 *  Prepares an #SK_FooBarAllReqs by initializing the "implied" values.
 *
 *  This makes it so that if #SK_FooBarReqs_point_to_arq () is used to make an
 *  #SK_FooBarReqs point to the contents of #arq, then the resulting
 *  #SK_FooBarReqs will already be prepared.
 *
 *  @param arq			The #SK_FooBarAllReqs to prepare. This is NOT
 *				expected to be initialized yet.
 *
 *  @param stat_r		Pointer to the #SK_FooBarStatReqs to use, which
 *				IS expected to be initialized.
 *
 *				(arq->stat_r) will be set equal to this.
 *
 *				This argument CAN be equal to &(arq->stat_r),
 *				provided that it is initialized.
 *
 *  @param dyna_r		Pointer to the #SK_FooBarDynaReqs to use, which
 *				IS expected to be initialized.
 *
 *				(arq->dyna_r) will be set equal to this.
 *
 *				This argument CAN be equal to &(arq->dyna_r),
 *				provided that it is intiialized.
 *
 *  @return			Standard status code
 */

SlyDr SK_FooBarAllReqs_prep
(
	SK_FooBarAllReqs *			arq,

	const SK_FooBarStatReqs *		stat_r,

	const SK_FooBarDynaReqs *		dyna_r
)
{
	//Sanity check on arguments

	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, arq, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, stat_r, SlyDr_get (SLY_BAD_ARG));
	SK_NULL_RETURN (SK_FOOBAR_INTF_L_DEBUG, dyna_r, SlyDr_get (SLY_BAD_ARG));


	//Copy the value (arq->stat_r) and (arq->dyna_r), if necessary

	if (&(arq->stat_r) != stat_r)
	{
		arq->stat_r = *stat_r;
	}


	if (&(arq->dyna_r) != dyna_r)
	{
		arq->dyna_r = *dyna_r;
	}


	//Make use of SK_FooBarReqs_prep () to prepare #arq

	return

	SK_FooBarReqs_prep
	(
		NULL,
		&(arq->stat_r),
		&(arq->stat_i),
		&(arq->dyna_r),
		&(arq->dyna_i)
	);
}

