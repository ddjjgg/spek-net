/*!
 *  @file
 *  how-to-SK_BoxIntf.c
 *
 *  This how-to demonstrates how to make use of #SK_BoxIntf.
 *
 *  To make quick use of this example, copy this file over ../SK_main.c.
 */




#include <stdio.h>
#include <stdlib.h>




#include "SK_Array.h"
#include "SK_BoxIntf.h"
#include "SK_numeric_types.h"




int main ()
{

	/*!
	 *  This function will demonstrate step-by-step how to use an
	 *  #SK_BoxIntf.
	 *
	 *  #SK_BoxIntf is an interface specification for creating #SK_Box's,
	 *  which are generic storage objects capable of storing elements of
	 *  any specified size.
	 *
	 *  Depending on the implementation of #SK_BoxIntf, the #SK_Box may be
	 *  an array, a vector, a linked-list, a tree, etc, etc. Since all of
	 *  the implementations must obey #SK_BoxIntf, they can all be
	 *  interacted with in the same way.
	 *
	 *  Ergo, #SK_BoxIntf allows for one storage object type to be easily
	 *  swapped with another storage object type by simply selecting a
	 *  different #SK_BoxIntf implementation.
	 */




	/*!
	 *  Step 0 - Request a Specific Configuration
	 *
	 *  This is arguably the most complex step of using #SK_BoxIntf.
	 *
	 *  #SK_BoxIntf specifies the existence of a struct named #SK_BoxReqs,
	 *  which is a bundle of configuration options requested by the user
	 *  for the desired storage object.
	 *
	 *  For this example code, all of the "requests" will be set for a
	 *  storage object suitable for storing #f64 values. (Note that the
	 *  #SK_BoxReqs itself will be instantiated in step 1).
	 *
	 *  Some of these configuration options are mandatory for
	 *  implementations to respect, while some of them are hints (AKA
	 *  optional to respect). The documentation in "SK_BoxIntf.h" specifies
	 *  the meaning / interpretation of each configuration option in
	 *  further detail.
	 *
	 *  It is worthwhile to note that #SK_BoxReqs strictly separates the
	 *  "static" parameters from the "dynamic" parameters. The "static"
	 *  parameters are NOT permitted to change during the lifetime of any
	 *  object created using that #SK_BoxReqs, while "dynamic" parameters
	 *  are allowed to change during those objects' lifetimes.
	 */

	//This stores the static "requests" of the #SK_BoxReqs

	SK_BoxStatReqs stat_r = (const SK_BoxStatReqs) {0};


	//This indicates that each element in the #SK_Box must be the size of
	//an #f64

	stat_r.elem_size = sizeof (f64);


	//This indicates the #SK_MemIntf to use if / when dynamic memory
	//allocations must be performed for the #SK_Box.
	//
	//Unless special behavior is desired, it is recommended to use
	//(&SK_MemStd) here.

	stat_r.mem = &SK_MemStd;


	//This stores the static "implied values" of the #SK_BoxReqs, which
	//will soon be determined based off of the values that were set in
	//#stat_r

	SK_BoxStatImpl stat_i = (const SK_BoxStatImpl) {0};


	//This stores the dynamic "requests" of the #SK_BoxReqs

	SK_BoxDynaReqs dyna_r = (const SK_BoxDynaReqs) {0};


	//This hint helps avoid expensive memory reallocations when the number
	//of elements in an #SK_Box is reduced

	dyna_r.hint_neg_slack = 128;


	//This hint helps avoid expensive memory allocations when the number of
	//elements in an #SK_Box is repeatedly increased

	dyna_r.hint_pos_slack = 64;


	//This stores the dynamic "implied values" of the #SK_BoxReqs, which
	//will soon be determined based off of the values that were set in
	//#dyna_r

	SK_BoxDynaImpl dyna_i = (const SK_BoxDynaImpl) {0};




	/*!
	 *  Step 1 - Call SK_BoxReqs_prep ()
	 *
	 *  In this section, an #SK_BoxReqs will be instantiated, and then
	 *  initialized using SK_BoxReqs_prep ().
	 *
	 *  SK_BoxReqs_prep () is an important function, because it determines
	 *  all of the values that are automatically implied by the "requests"
	 *  in the previous section.
	 */

	SK_BoxReqs req = (const SK_BoxReqs) {0};

	SK_BoxReqs_prep
	(
		&req,

		&stat_r,
		&stat_i,
		&dyna_r,
		&dyna_i
	);

	//Note that after the above function completes, #req will now point to
	//#stat_r, #stat_i, #dyna_r, and #dyna_i.




	/*!
	 *  Step 2 - Select an #SK_BoxIntf to Use
	 *
	 *  Obviously, different kinds of storage data structures have
	 *  different performance when it comes to many common operations. Some
	 *  storage data structures are better at accessing elements quickly,
	 *  while others can be resized quickly instead; some use a great deal
	 *  of memory per-element, and some use a low-amount of memory per
	 *  element; etc.
	 *
	 *  Different #SK_BoxIntf implementations represent different kinds of
	 *  storage data structures. Therefore, using an #SK_BoxIntf
	 *  implementation that is appropriate for the desired use-case is good
	 *  practice.
	 *
	 *  Note that no matter what #SK_BoxIntf is selected, however, the
	 *  programming interface remains the same. Therefore, none of the
	 *  steps before / after this point are affected by the selection made
	 *  here.
	 */

	//For this example, an array implementation of #SK_BoxIntf will be
	//used.

	const SK_BoxIntf * intf = &SK_Array_bif;




	/*!
	 *  Step 3 - Use (inst_stat_mem ())
	 *
	 *  The function (inst_stat_mem ()) inside of #SK_BoxIntf indicates how
	 *  much memory must be allocated for each instance of an #SK_Box
	 *  (configured with the given #SK_BoxReqs).
	 *
	 *  This information is needed for step 4.
	 */

	size_t inst_stat_mem = 0;

	intf->inst_stat_mem (&req, &inst_stat_mem);




	/*!
	 *  Step 4 - Allocate "Static" Memory for the #SK_Box Instance
	 *
	 *  In step 3), the per-instance static memory cost of the #SK_Box was
	 *  determined. This amount of memory now needs to be allocated by the
	 *  user.
	 *
	 *  This memory can be allocated anywhere; on the stack, on the heap,
	 *  inside of other storage objects, etc. The only requirement is that
	 *  this memory region be available during the entire lifetime of the
	 *  #SK_Box that uses it (hence why it is "static").
	 */

	//For this example, this memory will be allocated on the heap using
	//malloc ()

	SK_Box * box_0 = malloc (inst_stat_mem);

	if (NULL == box_0)
	{
		printf
		(
			"Failed to allocate %zu bytes for #box_0!\n\n",
			inst_stat_mem
		);

		return -1;
	}




	/*!
	 *  Step 5 - Initialize the #SK_Box
	 *
	 *  Now that the static memory of the #SK_Box instance is available, it
	 *  is possible to finally initialize the #SK_Box.
	 *
	 *  This is done via the function (init ()) in #SK_BoxIntf. Note that
	 *  this function is "stochastic", so it is good practice to check if
	 *  it has succeeded or not.
	 */

	//For this example, #box_0 will be initialized to have 100 elements,
	//and the function-pointer argument (SK_Box_elem_rw_set_zero) will
	//cause each of those elements to be set to 0.

	size_t num_elems = 100;


	SlySr sr =

	intf->init
	(
		&req,
		box_0,
		num_elems,
		SK_Box_elem_rw_set_zero,
		NULL
	);


	if (SLY_SUCCESS != sr.res)
	{
		printf ("Failed to initialize #box_0!\n\n");

		free (box_0);

		return -1;
	}




	/*!
	 *  Step 7 - Use Functions in #SK_BoxIntf to Interact with the #SK_Box
	 *
	 *  Now that the #SK_Box is initialized, it is ready to be used. The
	 *  functions in #SK_BoxIntf provide several common operations that
	 *  storage objects are expected to be capable of.
	 *
	 *  By using these functions, elements can be written, read, swapped,
	 *  copied, added, removed, inserted, etc.
	 *
	 *  Some more obscure operations like "getting a pointer to the next
	 *  contiguous memory region of elements" are also possible.
	 *
	 *  The example that will be given here will be simple, so these more
	 *  obscure operations will not be demonstrated.
	 *
	 *  Note that the functions that return #SlySr can "stochastically"
	 *  fail, and therefore their return values should be checked for
	 *  successful operation.
	 */

	//For this example, the functions in #SK_BoxIntf will be used to add up
	//the numbers [0, 9999].

	//This resizes #box_0 to hold 10000 elements

	num_elems = 10000;

	sr =

	intf->resize
	(
		&req,
		box_0,
		num_elems,
		SK_Box_elem_rw_set_zero,
		NULL
	);

	if (SLY_SUCCESS != sr.res)
	{
		printf ("Failed to resize #box_0\n\n");

		intf->deinit (&req, box_0);

		free (box_0);
	}


	//This sets each element in #box_0 equal to its index

	for (size_t elem_sel = 0; elem_sel < num_elems; elem_sel += 1)
	{
		f64 elem = (f64) elem_sel;

		intf->set_elem (&req, box_0, elem_sel, &elem);
	}


	//This gets each element in #box_0, and adds all their values together

	f64 total = 0;

	for (size_t elem_sel = 0; elem_sel < num_elems; elem_sel += 1)
	{
		f64 elem = 0;

		intf->get_elem (&req, box_0, elem_sel, &elem);


		total += elem;
	}


	printf ("The value of #total is.... %lf\n\n", total);




	/*!
	 *  Step 8 - Deinitialize the #SK_Box
	 *
	 *  After the #SK_Box is done being used, it is necessary to
	 *  deinitialize it to free any resources that it may be consuming.
	 *  This is done via the function (deinit ()) in #SK_BoxIntf.
	 *
	 *  Note that the #SK_Box CAN contain elements when (deinit ()) is
	 *  called. (deinit ()) WILL deallocate any memory used for elements at
	 *  the time it is called.
	 *
	 *  If this step is skipped, resource-leaks are essentially guaranteed.
	 */

	intf->deinit (&req, box_0);




	/*!
	 *  Step 9 - Deallocate the "Static" Memory for the #SK_Box Instance
	 *
	 *  Similar to step 8, it is now necessary to deallocate the static
	 *  memory that was previously allocated for the #SK_Box.
	 *
	 *  The only reason this can not be combined with step 8 is because the
	 *  user was the one who allocated this memory in step 4.
	 *
	 *  (Note that the user controlling where this memory is allocated is a
	 *  highly useful feature).
	 */

	free (box_0);




	return 0;
}




