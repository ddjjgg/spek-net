/*!****************************************************************************
 *
 * @file
 * SK_debug.h
 *
 * Provides per-feature debug levels that can be checked throughout the rest of
 * the program.
 *
 * The general idea is that when a particular feature's debugging level is set
 * to #SK_NO_DEBUG, that feature should no longer have any debugging-related
 * code execute.
 *
 * Otherwise, the amount of debugging-related code to execute can be changed by
 * setting the debugging level to #SK_L_DEBUG, #SK_M_DEBUG, or #SK_H_DEBUG.
 *
 * This file was based on ST_debug.h in sly-time.
 *
 *****************************************************************************/




#ifndef SK_DEBUG_H
#define SK_DEBUG_H




#include <SlyDebug-0-x-x.h>
#include <SlyResult-0-x-x.h>




#include "SK_numeric_types.h"




#include "SK_debug_external.h"




/*!
 *  @defgroup Debugging_Levels
 *
 *  Macros that define the debugging level for different program features.
 *
 *  Debug levels should exist PER feature, there should be NO "ALL FEATURES"
 *  DEBUG LEVEL! Otherwise, it will be very difficult to allow for granular
 *  selection of debug levels later on. (That said, all of the debug levels can
 *  be modified at once using SK_set_all_debug_levels ().)
 *
 *  The debugging levels of the program can be checked like so
 *
 *  @code
 *
 * 	//Only do something if this feature's debugging is set to high
 *
 * 	if (THIS_FEATURE_H_DEBUG)
 * 	{
 * 		//Stuff to do in high-debugging mode
 * 	}
 *
 *  @endcode
 *
 *  If #THIS_FEATURE_DEBUG_LEVEL is set to a lower level than #SK_H_DEBUG, then
 *  the statement should be removed from the program entirely. Removing
 *  if-blocks that always evaluate to zero are one of the easiest optimizations
 *  for compilers to make.
 *
 *  @{
 */




//FIXME : Is there a better way to add new debug levels here? The current
//method makes adding a new debug level slightly tedious and leaves room for
//simple errors. The current method is better than nothing, though.




//FIXME : Maybe it is time to remove the lines...
//
//
//	//#ifndef SK_ADDR_DEBUG_LEVEL
//	//
//	//	#define SK_ADDR_DEBUG_LEVEL		(SK_H_DEBUG)
//	//
//	//#endif //SK_ADDR_DEBUG_LEVEL
//
//
//...from each debugging level? The debug levels have been global variables
//instead of preprocessor macros for a long time now.




/*!
 * Stores the debug level for #SK_Addr related operations in spek-net
 */

extern u32 SK_Addr_debug_level;


//#ifndef SK_ADDR_DEBUG_LEVEL
//
//	#define SK_ADDR_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_ADDR_DEBUG_LEVEL


/*!
 * Indicates high SK_ADDR debugging is enabled
 */

#define SK_ADDR_H_DEBUG				(SK_Addr_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_ADDR debugging is enabled
 */

#define SK_ADDR_M_DEBUG				(SK_Addr_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_ADDR debugging is enabled
 */

#define SK_ADDR_L_DEBUG				(SK_Addr_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_Array related operations in spek-net
 */

extern u32 SK_Array_debug_level;


//#ifndef SK_ARRAY_DEBUG_LEVEL
//
//	#define SK_ARRAY_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_ARRAY_DEBUG_LEVEL


/*!
 * Indicates high SK_ARRAY debugging is enabled
 */

#define SK_ARRAY_H_DEBUG			(SK_Array_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_ARRAY debugging is enabled
 */

#define SK_ARRAY_M_DEBUG			(SK_Array_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_ARRAY debugging is enabled
 */

#define SK_ARRAY_L_DEBUG			(SK_Array_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_BpTree related operations in spek-net
 */

extern u32 SK_BpTree_debug_level;


//#ifndef SK_BPTREE_DEBUG_LEVEL
//
//	#define SK_BPTREE_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_BPTREE_DEBUG_LEVEL


/*!
 * Indicates high SK_BPTREE debugging is enabled
 */

#define SK_BPTREE_H_DEBUG			(SK_BpTree_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_BPTREE debugging is enabled
 */

#define SK_BPTREE_M_DEBUG			(SK_BpTree_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_BPTREE debugging is enabled
 */

#define SK_BPTREE_L_DEBUG			(SK_BpTree_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_BoxIntf related operations in spek-net
 */

extern u32 SK_BoxIntf_debug_level;


//#ifndef SK_BOX_INTF_DEBUG_LEVEL
//
//	#define SK_BOX_INTF_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_BOX_INTF_DEBUG_LEVEL


/*!
 * Indicates high SK_BOX_INTF debugging is enabled
 */

#define SK_BOX_INTF_H_DEBUG			(SK_BoxIntf_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_BOX_INTF debugging is enabled
 */

#define SK_BOX_INTF_M_DEBUG			(SK_BoxIntf_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_BOX_INTF debugging is enabled
 */

#define SK_BOX_INTF_L_DEBUG			(SK_BoxIntf_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_BoxReqsRegistry related operations in spek-net
 */

extern u32 SK_BoxReqsRegistry_debug_level;


//#ifndef SK_BOX_REQS_REGISTRY_DEBUG_LEVEL
//
//	#define SK_BOX_REQS_REGISTRY_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_BOX_REQS_REGISTRY_DEBUG_LEVEL


/*!
 * Indicates high SK_BOX_INTF debugging is enabled
 */

#define SK_BOX_REQS_REGISTRY_H_DEBUG		(SK_BoxReqsRegistry_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_BOX_INTF debugging is enabled
 */

#define SK_BOX_REQS_REGISTRY_M_DEBUG		(SK_BoxReqsRegistry_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_BOX_INTF debugging is enabled
 */

#define SK_BOX_REQS_REGISTRY_L_DEBUG		(SK_BoxReqsRegistry_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_HmapIntf related operations in spek-net
 */

extern u32 SK_HmapIntf_debug_level;


//#ifndef SK_HMAP_INTF_DEBUG_LEVEL
//
//	#define SK_HMAP_INTF_DEBUG_LEVEL	(SK_H_DEBUG)
//
//#endif //SK_HMAP_INTF_DEBUG_LEVEL


/*!
 * Indicates high SK_HMAP_INTF debugging is enabled
 */

#define SK_HMAP_INTF_H_DEBUG			(SK_HmapIntf_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_HMAP_INTF debugging is enabled
 */

#define SK_HMAP_INTF_M_DEBUG			(SK_HmapIntf_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_HMAP_INTF debugging is enabled
 */

#define SK_HMAP_INTF_L_DEBUG			(SK_HmapIntf_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_HmapStd related operations in spek-net
 */

extern u32 SK_HmapStd_debug_level;


//#ifndef SK_HMAP_STD_DEBUG_LEVEL
//
//	#define SK_HMAP_STD_DEBUG_LEVEL	(SK_H_DEBUG)
//
//#endif //SK_HMAP_STD_DEBUG_LEVEL


/*!
 * Indicates high SK_HMAP_STD debugging is enabled
 */

#define SK_HMAP_STD_H_DEBUG			(SK_HmapStd_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_HMAP_STD debugging is enabled
 */

#define SK_HMAP_STD_M_DEBUG			(SK_HmapStd_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_HMAP_STD debugging is enabled
 */

#define SK_HMAP_STD_L_DEBUG			(SK_HmapStd_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for miscellaneous features in spek-net
 */

extern u32 SK_misc_debug_level;


//#ifndef SK_MISC_DEBUG_LEVEL
//
//	#define SK_MISC_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_MISC_DEBUG_LEVEL


/*!
 * Indicates high MISC debugging is enabled
 */

#define SK_MISC_H_DEBUG				(SK_misc_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium MISC debugging is enabled
 */

#define SK_MISC_M_DEBUG				(SK_misc_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low MISC debugging is enabled
 */

#define SK_MISC_L_DEBUG				(SK_misc_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_MemIntf related operations in spek-net
 */

extern u32 SK_MemIntf_debug_level;


//#ifndef SK_MEM_INTF_DEBUG_LEVEL
//
//	#define SK_MEM_INTF_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_MEM_INTF_DEBUG_LEVEL


/*!
 * Indicates high SK_MEM_INTF debugging is enabled
 */

#define SK_MEM_INTF_H_DEBUG			(SK_MemIntf_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_MEM_INTF debugging is enabled
 */

#define SK_MEM_INTF_M_DEBUG			(SK_MemIntf_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_MEM_INTF debugging is enabled
 */

#define SK_MEM_INTF_L_DEBUG			(SK_MemIntf_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_MemStd related operations in spek-net
 */

extern u32 SK_MemStd_debug_level;


//#ifndef SK_MEM_STD_DEBUG_LEVEL
//
//	#define SK_MEM_STD_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_MEM_STD_DEBUG_LEVEL


/*!
 * Indicates high SK_MEM_STD debugging is enabled
 */

#define SK_MEM_STD_H_DEBUG			(SK_MemStd_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_MEM_STD debugging is enabled
 */

#define SK_MEM_STD_M_DEBUG			(SK_MemStd_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_MEM_STD debugging is enabled
 */

#define SK_MEM_STD_L_DEBUG			(SK_MemStd_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_StatArray related operations in spek-net
 */

extern u32 SK_StatArray_debug_level;


//#ifndef SK_STAT_ARRAY_DEBUG_LEVEL
//
//	#define SK_STAT_ARRAY_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_STAT_ARRAY_DEBUG_LEVEL


/*!
 * Indicates high SK_STAT_ARRAY debugging is enabled
 */

#define SK_STAT_ARRAY_H_DEBUG			(SK_StatArray_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_STAT_ARRAY debugging is enabled
 */

#define SK_STAT_ARRAY_M_DEBUG			(SK_StatArray_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_STAT_ARRAY debugging is enabled
 */

#define SK_STAT_ARRAY_L_DEBUG			(SK_StatArray_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_Vector related operations in spek-net
 */

extern u32 SK_Vector_debug_level;


//#ifndef SK_VECTOR_DEBUG_LEVEL
//
//	#define SK_VECTOR_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_VECTOR_DEBUG_LEVEL


/*!
 * Indicates high SK_VECTOR debugging is enabled
 */

#define SK_VECTOR_H_DEBUG			(SK_Vector_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_VECTOR debugging is enabled
 */

#define SK_VECTOR_M_DEBUG			(SK_Vector_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_VECTOR debugging is enabled
 */

#define SK_VECTOR_L_DEBUG			(SK_Vector_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_Yoke related operations in spek-net
 */

extern u32 SK_Yoke_debug_level;


//#ifndef SK_YOKE_DEBUG_LEVEL
//
//	#define SK_YOKE_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_YOKE_DEBUG_LEVEL


/*!
 * Indicates high SK_YOKE debugging is enabled
 */

#define SK_YOKE_H_DEBUG				(SK_Yoke_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_YOKE debugging is enabled
 */

#define SK_YOKE_M_DEBUG				(SK_Yoke_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_YOKE debugging is enabled
 */

#define SK_YOKE_L_DEBUG				(SK_Yoke_debug_level >= SK_L_DEBUG)




/*!
 * Stores the debug level for #SK_YokeSet related operations in spek-net
 */

extern u32 SK_YokeSet_debug_level;


//#ifndef SK_YOKE_SET_DEBUG_LEVEL
//
//	#define SK_YOKE_SET_DEBUG_LEVEL		(SK_H_DEBUG)
//
//#endif //SK_YOKE_SET_DEBUG_LEVEL


/*!
 * Indicates high SK_YOKE_SET debugging is enabled
 */

#define SK_YOKE_SET_H_DEBUG			(SK_YokeSet_debug_level >= SK_H_DEBUG)


/*!
 * Indicates medium SK_YOKE_SET debugging is enabled
 */

#define SK_YOKE_SET_M_DEBUG			(SK_YokeSet_debug_level >= SK_M_DEBUG)


/*!
 * Indicates low SK_YOKE_SET debugging is enabled
 */

#define SK_YOKE_SET_L_DEBUG			(SK_YokeSet_debug_level >= SK_L_DEBUG)




/*!
 *  @}
 */




//FIXME : Make versions of SK_*_RES_RETURN () that accept a #FILE pointer
//argument




/*!
 *  Causes a function to return if #sly_std_res is not equal to #SLY_SUCCESS,
 *  and it prints out a debug message as well.
 *
 *  Unlike SK_DEBUG_RES_RETURN (), the #debug_flag only controls whether or
 *  not a debug message gets printed out. The comparison to #sly_std_res and
 *  the potential 'return' are always performed.
 *
 *  @param debug_flag		A flag indicating that the program is in a
 *				debug mode, i.e. some macro or variable that
 *				evaluates to a non-zero value when the debug
 *				mode is active.
 *
 *  @param sly_std_res		A #SLY_STD_RES value to check if it is
 *  				#SLY_SUCCESS
 *
 *  @param return_val		The value to return in the event that
 *				#sly_std_res is not equal to #SLY_SUCCESS
 */

#define SK_RES_RETURN(debug_flag, sly_std_res, return_val)		\
									\
	do								\
	{								\
		if (SLY_SUCCESS != (sly_std_res))			\
		{							\
			if (debug_flag)					\
			{						\
				SLY_PRINT_DEBUG				\
				(					\
					#sly_std_res			\
					" = %s, returning "		\
					#return_val,			\
					SlyStdResStr_get (sly_std_res).str\
				);					\
			}						\
									\
			return (return_val);				\
		}							\
	}								\
	while (0)




/*!
 *  Causes a function to return if #sly_std_res is not equal to #SLY_SUCCESS,
 *  but _only_ if the program is in a debugging mode.
 *
 *  @param debug_flag		A flag indicating that the program is in a
 *				debug mode, i.e. some macro or variable that
 *				evaluates to a non-zero value when the debug
 *				mode is active.
 *
 *  @param sly_std_res		A #SLY_STD_RES value to check if it is
 *  				#SLY_SUCCESS
 *
 *  @param return_val		The value to return in the event that
 *				#sly_std_res is not equal to #SLY_SUCCESS
 */

#define SK_DEBUG_RES_RETURN(debug_flag, sly_std_res, return_val)	\
									\
	do								\
	{								\
		if (debug_flag)						\
		{							\
			if (SLY_SUCCESS != (sly_std_res))		\
			{						\
				SLY_PRINT_DEBUG				\
				(					\
					#sly_std_res			\
					" = %s, returning "		\
					#return_val,			\
					SlyStdResStr_get (sly_std_res).str\
				);					\
									\
									\
				return (return_val);			\
			}						\
		}							\
	}								\
	while (0)




/*!
 *  Causes a function to return if a pointer evaluates to NULL. If the
 *  #debug_flag evaluates to true, then a debug message will also be printed
 *  out.
 *
 *  Unlike #SK_DEBUG_NULL_RETURN (), the NULL check is always performed, and
 *  only the message printing is dependent on the debug flag.
 *
 *  @param debug_flag		A flag indicating that the program is in a
 *				debug mode, i.e. some macro or variable that
 *				evaluates a non-zero value when the debug mode
 *				is active.
 *
 *  @param ptr			The pointer to check for a NULL value
 *
 *  @param return_val		The value to return in the event the pointer is
 *  				NULL
 *
 *
 *  EXAMPLE
 *
 *  @code
 *
 *	//Returns an error if cool_ptr fails to be initialized
 *
 *  	int * cool_ptr = malloc (sizeof (int));
 *
 *  	SK_NULL_RETURN (DEBUG_IS_HIGH, cool_ptr, -1);
 *
 *  @endcode
 */

#define SK_NULL_RETURN(debug_flag, ptr, return_val)		\
								\
	do							\
	{							\
		if (debug_flag)					\
		{						\
			SLY_NULL_CHECK (ptr);			\
		}						\
								\
		if (NULL == (ptr))				\
		{						\
			return (return_val);			\
		}						\
	}							\
	while (0)




/*!
 *  Version of SK_NULL_RETURN () that accepts an additional argument for
 *  a (FILE *).
 *
 *  @param debug_flag		A flag indicating that the program is in a
 *				debug mode, i.e. some macro or variable that
 *				evaluates a non-zero value when the debug mode
 *				is active.
 *
 *  @param file			A (FILE *) for an _open_ file to print a debug
 *				message to
 *
 *  @param ptr			The pointer to check for a NULL value
 *
 *  @param return_val		The value to return in the event the pointer is
 *  				NULL
 *
 *
 *  EXAMPLE
 *
 *  @code
 *
 *	//Returns an error if cool_ptr fails to be initialized
 *
 *  	int * cool_ptr = malloc (sizeof (int));
 *
 *  	SK_NULL_RETURN_FP (DEBUG_IS_HIGH, file, cool_ptr, -1);
 *
 *  @endcode
 */

#define SK_NULL_RETURN_FP(debug_flag, file, ptr, return_val)	\
								\
	do							\
	{							\
		if (debug_flag)					\
		{						\
			SLY_NULL_CHECK_FP ((file), (ptr));	\
		}						\
								\
		if (NULL == (ptr))				\
		{						\
			return (return_val);			\
		}						\
	}							\
	while (0)




/*!
 *  Causes a function to return if the pointer is NULL _only_ when the program
 *  is in a debug mode
 *
 *  This macro is very similar to #SK_NULL_RETURN (), except the NULL check is
 *  only performed when the debug flag is true.
 *
 *  @param debug_flag		A flag indicating that the program is in a
 *				debug mode, i.e. some macro or variable that
 *				evaluates a non-zero value when the debug mode
 *				is active.
 *
 *  @param ptr			The pointer to check for a NULL value
 *
 *  @param return_val		The value to return in the event the pointer is
 *				NULL
 *
 *  EXAMPLE
 *
 *  @code
 *
 *  	//Returns an error if the function is passed a NULL pointer and the
 *  	//program is in a high debugging mode
 *
 *	int some_func (int * some_num_ptr)
 *	{
 *		SK_DEBUG_NULL_RETURN (DEBUG_IS_HIGH, some_num_ptr, -1);
 *
 *		//...
 *
 *		return 0;
 *	}
 *
 *  @endcode
 */

#define SK_DEBUG_NULL_RETURN(debug_flag, ptr, return_val)	\
								\
	do							\
	{							\
		if (debug_flag)					\
		{						\
			SLY_NULL_CHECK (ptr);			\
								\
			if (NULL == (ptr))			\
			{					\
				return (return_val);		\
			}					\
		}						\
	}							\
	while (0)




/*!
 *  Version of SK_NULL_RETURN () that accepts an additional argument for
 *  a (FILE *).
 *
 *  @param debug_flag		A flag indicating that the program is in a
 *				debug mode, i.e. some macro or variable that
 *				evaluates a non-zero value when the debug mode
 *				is active.
 *
 *  @param file			A (FILE *) for an _open_ file to print a debug
 *				message to
 *
 *  @param ptr			The pointer to check for a NULL value
 *
 *  @param return_val		The value to return in the event the pointer is
 *				NULL
 *
 *  EXAMPLE
 *
 *  @code
 *
 *  	//Returns an error if the function is passed a NULL pointer and the
 *  	//program is in a high debugging mode
 *
 *	int some_func (int * some_num_ptr)
 *	{
 *		SK_DEBUG_NULL_RETURN_FP (DEBUG_IS_HIGH, stdout, some_num_ptr, -1);
 *
 *		//...
 *
 *		return 0;
 *	}
 *
 *  @endcode
 */

#define SK_DEBUG_NULL_RETURN_FP(debug_flag, file, ptr, return_val)	\
									\
	do								\
	{								\
		if (debug_flag)						\
		{							\
			SLY_NULL_CHECK_FP ((file), (ptr));		\
									\
			if (NULL == (ptr))				\
			{						\
				return (return_val);			\
			}						\
		}							\
	}								\
	while (0)




/*!
 *  Prints a message if a given debug flag is asserted
 *
 *  @param debug_flag		A flag indicating that the program is in a
 *				debug mode, i.e. some macro or variable that
 *				evaluates a non-zero value when the debug mode
 *				is active.
 *
 *  @param f_str		A format string following the same conventions
 *				as printf ()
 *
 *  @param ...			The arguments associated described by the
 *				format string #f_str
 */

#define SK_DEBUG_PRINT(debug_flag, f_str, ...)				\
									\
	{								\
		if (debug_flag)						\
		{							\
			SLY_PRINT_DEBUG ((f_str), ##__VA_ARGS__);	\
		}							\
	}




/*!
 *  Version of SK_DEBUG_PRINT () that accepts an additional argument for
 *  a (FILE *).
 *
 *  @param debug_flag		A flag indicating that the program is in a
 *				debug mode, i.e. some macro or variable that
 *				evaluates to a non-zero value when the debug
 *				mode is active.
 *
 *  @param file			A (FILE *) for an _open_ file to print a debug
 *				message to
 *
 *  @param f_str		A format string following the same conventions
 *				as printf ()
 *
 *  @param ...			The arguments associated described by the
 *				format string #f_str
 */

#define SK_DEBUG_PRINT_FP(debug_flag, file, f_str, ...)				\
										\
	{									\
		if (debug_flag)							\
		{								\
			SLY_PRINT_DEBUG_FP ((file), (f_str), ##__VA_ARGS__);	\
		}								\
	}




/*!
 *  Prints a standard announcement of a test-bench to a given file.
 *
 *  @param file			A (FILE *) for an _open_ file to print a debug
 *				message to
 *
 *  FIXME : This is deprecated, it is being replaced with
 *  SK_TB_BEG_PRINT_STD ()
 */

#define SK_TB_BEG_PRINT(file)						\
									\
	{								\
		fprintf ((file), "%s () : BEGIN\n\n", __func__);	\
	}




/*!
 *  A version of SK_TB_BEG_PRINT that also takes in #SK_LineDeco argument.
 *
 *  @param file			A (FILE *) for an _open_ file to print a debug
 *				message to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *  				printed lines
 *
 *  FIXME : This is deprecated, it is being replaced with
 *  SK_TB_BEG_PRINT_STD ()
 */

#define SK_TB_BEG_PRINT_DECO(file, deco)		\
							\
	{						\
		SK_LineDeco_print_opt			\
		(					\
			(file),				\
			(deco),				\
			"%s () : BEGIN\n\n",		\
			__func__			\
		);					\
	}




/*!
 *  Prints a standard announcement of a test-bench to a given file.
 *
 *  @param print_flag		If this is true, then the announcement will be
 *  				printed.
 *
 *				If this is false, then the announcement will
 *				not be printed.
 *
 *  @param file			A (FILE *) for an _open_ file to print a debug
 *				message to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *  				printed lines
 */

#define SK_TB_BEG_PRINT_STD(print_flag, file, deco)	\
							\
	{						\
		if (print_flag)				\
		{					\
			SK_LineDeco_print_opt		\
			(				\
				(file),			\
				(deco),			\
				"%s () : BEGIN\n\n",	\
				__func__		\
			);				\
		}					\
	}




/*!
 *  Prints a standard announcement of a test-bench finishing to #stdout
 *
 *  @param file			A (FILE *) for an _open_ file to print a debug
 *				message to
 *
 *  @param error_flag		A flag indicating whether or not the test-bench
 *				passed. If 0, then the test-bench is considered
 *				to have succeeded. Otherwise, the test-bench is
 *				considered to have failed.
 *
 *  @param fail_ret		What to return if the test-bench failed
 *
 *  @param pass_ret		What to return if the test-bench succeeded
 *
 *  FIXME : This is deprecated, it is being replaced with
 *  SK_TB_BEG_PRINT_STD ()
 */

#define SK_TB_END_PRINT(file, error_flag, fail_ret, pass_ret) 		\
									\
	{								\
		if (error_flag)						\
		{							\
			fprintf ((file), "%s () : FAIL\n\n", __func__); \
									\
			return (fail_ret);				\
		}							\
									\
									\
		fprintf ((file), "%s () : PASS\n\n", __func__);		\
									\
		return (pass_ret);					\
	}




/*!
 *  A version of SK_TB_END_PRINT () that also takes in #SK_LineDeco argument.
 *
 *  @param file			A (FILE *) for an _open_ file to print a debug
 *				message to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *  				printed lines
 *
 *  @param error_flag		A flag indicating whether or not the test-bench
 *				passed. If 0, then the test-bench is considered
 *				to have succeeded. Otherwise, the test-bench is
 *				considered to have failed.
 *
 *  @param fail_ret		What to return if the test-bench failed
 *
 *  @param pass_ret		What to return if the test-bench succeeded
 *
 *  FIXME : This is deprecated, it is being replaced with
 *  SK_TB_BEG_PRINT_STD ()
 */

#define SK_TB_END_PRINT_DECO(file, deco, error_flag, fail_ret, pass_ret)	\
										\
	{									\
		if (error_flag)							\
		{								\
			SK_LineDeco_print_opt					\
			(							\
				(file),						\
				(deco),						\
				"%s () : FAIL\n\n",				\
				__func__					\
			);							\
										\
			return (fail_ret);					\
		}								\
										\
										\
		SK_LineDeco_print_opt						\
		(								\
			(file),							\
			(deco),							\
			"%s () : PASS\n\n",					\
			__func__						\
		);								\
										\
		return (pass_ret);						\
	}




/*!
 *  Prints a standard announcement of a test-bench finishing to #stdout
 *
 *  @param print_flag		If this is true, then the announcement will be
 *  				printed.
 *
 *				If this is false, then the announcement will
 *				not be printed.
 *
 *  @param file			A (FILE *) for an _open_ file to print a debug
 *				message to
 *
 *  @param deco			An #SK_LineDeco describing how to decorate the
 *  				printed lines
 *
 *  @param error_flag		A flag indicating whether or not the test-bench
 *				passed. If 0, then the test-bench is considered
 *				to have succeeded. Otherwise, the test-bench is
 *				considered to have failed.
 *
 *  @param fail_ret		What to return if the test-bench failed
 *
 *  @param pass_ret		What to return if the test-bench succeeded
 */

#define SK_TB_END_PRINT_STD(print_flag, file, deco, error_flag, fail_ret, pass_ret)	\
											\
	{										\
		if (error_flag)								\
		{									\
			if (print_flag)							\
			{								\
				SK_LineDeco_print_opt					\
				(							\
					(file),						\
					(deco),						\
					"%s () : FAIL\n\n",				\
					__func__					\
				);							\
			}								\
											\
			return (fail_ret);						\
		}									\
											\
											\
		if (print_flag)								\
		{									\
			SK_LineDeco_print_opt						\
			(								\
				(file),							\
				(deco),							\
				"%s () : PASS\n\n",					\
				__func__						\
			);								\
		}									\
											\
		return (pass_ret);							\
	}




#endif //SK_DEBUG_H

