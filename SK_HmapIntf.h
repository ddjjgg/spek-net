/*!****************************************************************************
 *
 * @file
 * SK_HmapIntf.h
 *
 * Provides #SK_HmapIntf, which is an interface specification for hash-maps,
 * AKA dictionaries, symbol tables, or associative arrays.
 *
 * #SK_HmapIntf is built on top of #SK_BoxIntf, and follows many of the same
 * conventions.
 *
 *****************************************************************************/




#ifndef SK_HMAPINTF_H
#define SK_HMAPINTF_H




#include <SlyResult-0-x-x.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>




#include "SK_BoxIntf.h"
#include "SK_misc.h"




/*!
 *  Function pointer type used in #SK_HmapIntf to represent a function that
 *  numerically compares 2 memory regions to one another, where each memory
 *  region is sized to a constant number of bytes.
 *
 *  @param mem_0		Pointer to a memory region to compare
 *
 *  @param mem_1		Pointer to another memory region to compare
 *
 *  @param res			Where to place the result of the comparison.
 *
 *  				-1 means that (mem_0 > mem_1)
 *
 *  				 0 means that (mem_0 == mem_1)
 *
 *  				+1 means that (mem_0 < mem_1)
 *
 *  @return			Standard status code
 */

typedef

SlyDr (* SK_HmapIntf_prim_cmp)
(
	const void *	mem_0,

	const void *	mem_1,

	int *		res
);




/*!
 *  Used to describe a set of desired constant parameters for an instance of
 *  the data structure implementing the interface #SK_HmapIntf. These
 *  parameters MUST stay constant for the lifetime of the instance that uses
 *  them.
 *
 *  It is assumed that these parameters will need to be known when the data
 *  structure instance is initialized.
 *
 *  @warning
 *  These parameters should NOT change over the lifetime of the associated
 *  instance of the data structure implementing #SK_HmapIntf.
 */

typedef struct SK_HmapStatReqs
{

	/*!
	 *  Design note : I considered adding a "max_elems" field here which
	 *  represented the number of key-value pairs that this singular level
	 *  of the hash-map could store, which would be helpful in setting the
	 *  max-size of each level of a multi-level hash map.
	 *
	 *  HOWEVER, to easily determine the size of a key needed for each
	 *  level in a multi-level hash-map, I would have also required the
	 *  keys to be modulo'd by (max_elems). In doing so, the keys would
	 *  effectively become equivalent to indices, which would effectively
	 *  destroy the purpose of using a hash-map in the first place. Regular
	 *  storage objects already associate indices to stored values 1-to-1,
	 *  and they don't require wastefully storing the indices in memory to
	 *  do so.
	 *
	 *  This doesn't mean that a (max_elems) field would be totally
	 *  useless, but it does mean that any attempt at constraining the
	 *  range of keys from [0, max_elems - 1] would be a large mistake.
	 *
	 *  Stated another way, hash-maps are really only useful if keys larger
	 *  than the max index for a storage object (with the same number of
	 *  elements) can be stored.
	 */




	/*!
	 *  Design note: I considered adding a "multi_level_hint" field here
	 *  which represented whether or not the hash-map should be a
	 *  single-level hash-map or a multi-level hash-map.
	 *
	 *  To be clear about these terms, a single-level hash-map is a normal
	 *  hash-map that stores values directly. A multi-level hash-map is a
	 *  hash-map that stores hash-maps instead of normal values in every
	 *  level except the last level. Essentially, a tree of hash-maps is
	 *  created, which may be more efficient for random-access of elements
	 *  than if all of the keys / values where stored in linear arrays.
	 *
	 *  Consider if the number of keys were M, and assume M = (N * N),
	 *  where N is some arbitrary number. If the keys were stored in a
	 *  linear array and searched linearly for a single-level hash-map,
	 *  then it would take ((N * N) / 2) key-reads on average to find a key
	 *  within that array. However, if a 2-level hash-map were used
	 *  instead, then it would take ((N / 2) + (N / 2)) key-reads on
	 *  average, which is less key reads for any value of N above 2.
	 *
	 *  However, that is assuming if "the keys were searched linearly". If
	 *  the keys are inserted into the key array in a sorted manner, then a
	 *  binary-search could be used instead, in which case (log_2 (M))
	 *  key-reads would be needed in the worst-case scenario, which is
	 *  equal to (log_2 (N * N)), which is equal to (log_2 (N) + log_2
	 *  (N)). This is better than the performance mentioned in the previous
	 *  paragraph, despite this being the "worst-case scenario" instead of
	 *  the "average-case scenario". In the event a binary-search were
	 *  used, it would not matter if a single-level hash-map were used or
	 *  if a multi-level hash-map were used, the number of key-reads would
	 *  remain equal to (log_2 (M)), although the implementation of the
	 *  multi-level hash-map would likely incur an additional
	 *  address-dereference (and therefore be slightly less efficient).
	 *
	 *  Ergo, binary-searches on sorted key-arrays in single-level
	 *  hash-maps are probably superior algorithmically to multi-level
	 *  hash-maps.
	 *
	 *  This raises the question, wouldn't keeping the key-array sorted
	 *  require a great deal of inserting / removing keys from random spots
	 *  within the key-array in realistic use cases, since the keys are
	 *  likely randomized hash-values? The answer is "yes", and inserting
	 *  into the middle of arrays is very inefficient when the array is
	 *  large.
	 *
	 *  This is why instead of simply using a "key-array", an #SK_BoxIntf
	 *  should be used to create a "key storage object". The keys could
	 *  then be stored in an array, linked-list, vector, tree, or etc.
	 *  depending on what the selected #SK_BoxIntf is. The selection can be
	 *  based on what is the most appropriate storage object for the size /
	 *  access-pattern of the key storage object. (Likely for most cases, a
	 *  tree will be desirable for a hash-map).
	 *
	 *  Off-loading the storage-object maintenance to #SK_BoxIntf seems to
	 *  be a far superior implementation strategy than directly writing a
	 *  multi-level hash-map, because
	 *
	 *    A)
	 *		As previously stated, it lets the user select between
	 *		using a key-array, key-vector, key-linked-list,
	 *		key-tree, or etc. depending on the intended use
	 *
	 *    B)
	 *		It does away with many of the complexities that
	 *		explicitly supporting mulit-level hash-maps would
	 *		introduce.
	 *
	 *		For example, would there then have to be hints for
	 *		"max_num_levels", "cur_level_key_size",
	 *		"total_key_size"?
	 *
	 *		Would the functions in #SK_HmapIntf accept all the keys
	 *		for each level concatenated together in its arguments,
	 *		or would it require that separate calls be used to
	 *		traverse each level of the hash-map?
	 *
	 *		How would all of the above be affected if the key size
	 *		in a given level were desired to be different from all
	 *		the other levels in the hash-map?
	 *
	 *		Clearly, there are many edge-cases to consider with
	 *		multi-level hash-maps.
	 *
	 *		If the hash-map is known to be single-level, all of
	 *		this complexity disappears from the design. Albeit, the
	 *		the keys need to be stored as sorted in an advanced
	 *		storage object, but that's more work for #SK_BoxIntf
	 *		than it is for #SK_HmapIntf.
	 *
	 *  So, the current conclusion for #SK_HmapIntf is that it is easier to
	 *  use, implement, AND more efficient to execute if the interface
	 *  presents everything as a single-level hash-map.
	 *
	 *  THEREFORE, NO FIELDS ARE GIVEN TO SUPPORT MULTI-LEVEL HASH-MAPS
	 *  WITHIN THE SK_Hmap*Reqs STRUCTS!
	 *
	 *  FIXME : IS this conclusion correct, or have I made a mistake
	 *  somewhere along the line?
	 */




	/*!
	 *  Indicates if each key should be stored side-by-side in memory with
	 *  the respective value it is paired with. Due to cache locality, this
	 *  generally makes it so that the value is quicker to access after the
	 *  key has been identified. However, because the keys will be farther
	 *  apart from one another, it generally makes it take longer to search
	 *  through the keys. In large sets of keys, however, the accessed
	 *  keys will be far apart in memory anyway, especially if a binary
	 *  search is used.
	 *
	 *  If this is 0, that indicates that the keys and values will be
	 *  stored in separate storage objects.
	 *
	 *  If this is 1, that indicates that the key-value pairs will be
	 *  stored as a singular element within the same storage object. (The
	 *  #SK_BoxIntf for this storage object will be #key_box_intf).
	 *
	 *  It is recommended to set this field to 1.
	 *
	 *  @warning
	 *  Implementations can choose to ignore this field, but if they do so,
	 *  they must keep in mind that all the #*_stat_r and #*_stat_i fields
	 *  within #SK_HmapStatReqs are calculated assuming this hint is
	 *  respected. I.e., the value of
	 *  (SK_HmapStatReqs->key_group_stat_r->elem_size) is different
	 *  depending on this field.
	 */

	int key_val_concat;




	/*!
	 *  If this is 0, that indicates that each key will be the same size.
	 *
	 *  If this is 1, that indicates that each key can be a different size.
	 *
	 *  (This is NOT a hint, implementations must respect this field, or at
	 *  least fail to initialize instances if it can not be respected.)
	 *
	 *  Setting this to 1 will generally make hash-maps consume slightly
	 *  more memory to store the key size for each key, unless the variance
	 *  in the key size outweighs the cost of those size fields.
	 */

	int key_dyna_size_en;




	/*!
	 *  If this is 0, that indicates that each value will be the same size.
	 *
	 *  If this is 1, that indicates that each value can be a different
	 *  size.
	 *
	 *  (This is NOT a hint, implementations must respect this field, or at
	 *  least fail to initialize instances if it can not be respected.)
	 *
	 *  Setting this to 1 will generally make hash-maps consume slightly
	 *  more memory to store the value size for each value, unless the
	 *  variance in the value size outweighs the cost of those size fields.
	 */

	int val_dyna_size_en;




	/*!
	 *  If #key_dyna_size_en is 0, then this represents the size (in bytes)
	 *  of each key.
	 *
	 *  If #key_dyna_size_en is 1, then this should be ignored.
	 */

	size_t key_stat_size;




	/*!
	 *  If #val_dyna_size_en is 0, then this represents the size (in bytes)
	 *  of each value.
	 *
	 *  If #val_dyna_size_en is 1, then this should be ignored.
	 */

	size_t val_stat_size;




	/*!
	 *  Indicates whether or not a smaller key (from a key-value pair)
	 *  should be extended with zero-bytes when comparing 2 keys of a
	 *  different size.
	 *
	 *  Basically, if this 0, then the byte sequence "0xAA" is considered
	 *  distinct from "0xAA 0x00". If this is 1, then both byte sequences
	 *  are considered the same.
	 *
	 *  This is only relevant if #key_dyna_size_en is 1, or if
	 *  #key_dyna_size_en is 0 but one of the *_v_key () or *_v_kv ()
	 *  functions are used.
	 *
	 *  @note
	 *  The reason this field is static (i.e. within #SK_HmapStatReqs), but
	 *  not #val_zero_extend, is that changing this value from 1 to 0 can
	 *  cause individual keys stored within an #SK_Hmap to change from
	 *  being recognized as unique to being recognized as non-unique. This
	 *  is a problem, because duplicate keys are NOT allowed within an
	 *  #SK_Hmap instance.
	 *
	 *  Since there is no restriction on duplicate values within key-value
	 *  pairs, this issue does not exist for #val_zero_extend.
	 *
	 *  @note
	 *  The choice of whether to extend on the LSB-side or MSB-side is up
	 *  to the implementation, so long as it does it consistently for every
	 *  key.
	 *
	 *  @warning
	 *  Note that this is NOT a hint, implementations MUST respect this
	 *  field.
	 */

	int key_zero_extend;




	/*!
	 *  The #SK_BoxIntf that will be used for the storage object containing
	 *  keys (of the key-value pairs).
	 *
	 *  In the event that #key_val_concat is 1 (and that field is
	 *  respected by the implementation), then that storage object will
	 *  also be the storage object in which the values of the key-value
	 *  pairs will be stored.
	 *
	 *  This field must NOT be NULL, otherwise SK_HmapReqs_prep () MUST
	 *  fail.
	 *
	 *  While implementations are free to ignore this field, they are
	 *  allowed to depend on the user providing a valid pointer here.
	 */

	const SK_BoxIntf * key_group_intf;




	/*!
	 *  The #SK_BoxIntf that will be used for the storage object containing
	 *  values (of the key-value pairs).
	 *
	 *  In the event that #key_val_concat is 1 (and that field is
	 *  respected by the implementation), then this will be ignored and the
	 *  values will be stored using #key_group_intf.
	 *
	 *  Unless #key_val_concat is 1, this field must NOT be NULL, otherwise
	 *  SK_HmapReqs_prep () MUST fail.
	 *
	 *  While implementations are free to ignore this field, they are
	 *  allowed to depend on the user providing a valid pointer here.
	 */

	const SK_BoxIntf * val_group_intf;




	/*!
	 *  If #key_dyna_size_en is 0, then this will be ignored (and is
	 *  permitted to be NULL.)
	 *
	 *  If #key_dyna_size_en is 1, then this is the #SK_BoxIntf that will
	 *  used to store the bytes of a key.
	 *
	 *  It is recommended to use a very low-memory-cost #SK_BoxIntf here,
	 *  such as #SK_Array.
	 *
	 *  This field can ONLY be NULL if #key_dyna_size_en is 0, otherwise
	 *  SK_HmapReqs_prep () MUST fail.
	 *
	 *  While implementations are free to ignore this field, they are
	 *  allowed to depend on the user providing a valid pointer here.
	 */

	const SK_BoxIntf * key_bytes_intf;




	/*!
	 *  If #val_dyna_size_en is 0, then this will be ignored (and is
	 *  permitted to be NULL.)
	 *
	 *  If #val_dyna_size_en is 1, then this is the #SK_BoxIntf that will
	 *  used to store the bytes of a value.
	 *
	 *  It is recommended to use a very low-memory-cost #SK_BoxIntf here,
	 *  such as #SK_Array.
	 *
	 *  This field can ONLY be NULL if #val_dyna_size_en is 0, otherwise
	 *  SK_HmapReqs_prep () MUST fail.
	 *
	 *  While implementations are free to ignore this field, they are
	 *  allowed to depend on the user providing a valid pointer here.
	 */

	const SK_BoxIntf * val_bytes_intf;




	/*!
	 *  Design note : Take notice of how #key_group_stat_r_basis,
	 *  #val_group_stat_r_basis, etc. are recorded within this data
	 *  structure. These fields are used by SK_HmapReqs_prep () to
	 *  calculate the #SK_BoxStatReqs, #SK_BoxStatImpl, etc. that configure
	 *  #SK_Box's (that can be used by #SK_Hmap instances to store keys
	 *  / values).
	 *
	 *  (Note that in order to be used for this purpose, the #elem_size
	 *  field within these structure MUST be set consistently with the rest
	 *  of the fields within #SK_HmapStatReqs)
	 *
	 *  Currently, these methods of storing the #SK_BoxStatReqs,
	 *  #SK_BoxStatImpl, etc. have been evaluated :
	 *
	 *
	 *  1)
	 *	Store pointers to the #SK_BoxStatReqs / #SK_BoxStatImpl within
	 *	#SK_HmapStatReqs that the user is responsible for setting,
	 *	allow the user to initialize the #SK_BoxStatReqs, and then
	 *	require the user to use separate functions *_calc_*_stat_r () /
	 *	*_calc_*_stat_i () to ensure the consistency of the
	 *	#SK_BoxStatReqs / #SK_BoxStatImpl with the rest of the
	 *	#SK_HmapStatReqs.
	 *
	 *	Pros : A singlular #SK_BoxStatReqs / #SK_BoxStatImpl can
	 *	potentially be shared by multiple #SK_HmapStatReqs structures,
	 *	which saves memory. The #SK_BoxStatReqs / #SK_BoxStatImpl that
	 *	are not needed can be left as NULL pointers. In this method,
	 *	it is still possible for SK_HmapReqs_prep () to detect flawed
	 *	configurations and return failure, because it can compare the
	 *	#SK_BoxStatReqs / #SK_BoxStatImpl to what is produced via
	 *	*_calc_*_stat_r () / *_calc_*_stat_i ().
	 *
	 * 	Cons : Perhaps this style of configuration is not terribly
	 * 	ergonomic. Particularly, sharing the resultant #SK_BoxStatReqs
	 * 	/ #SK_BoxStatImpl amongst multiple #SK_HmapStatReqs may prove
	 * 	very difficult. Changes in the other fields of #SK_HmapStatReqs
	 * 	may change what the value of the #SK_BoxStatReqs /
	 * 	#SK_BoxStatImpl must be, and as a result, it is not immediately
	 * 	clear if 2 given #SK_HmapStatReqs are sufficiently similar to
	 * 	share these pointers.
	 *
	 *
	 *  2)
	 *  	Store the full #SK_BoxStatReqs / #SK_BoxStatImpl directly
	 *  	within the #SK_HmapStatImpl, and use #SK_HmapReqs_prep () to
	 *  	calculate these structures based off of arguments stored within
	 *  	#SK_BoxStatReqs
	 *
	 *  	Pros : Very fool-proof because the SK_HmapReqs_prep () could
	 *  	ensure that the #SK_BoxStat* configurations are consistent with
	 *  	everything else requested in #SK_HmapStatReqs.
	 *
	 *  	Cons : Wastes memory when one of the #SK_BoxStat* structures
	 *  	does not need to be used (if the implementation ignores any of
	 *  	the fields), also does not allow the #SK_BoxStat*
	 *  	structures to be shared amongst different #SK_HmapStat*
	 *  	structures.
	 *
	 *  	(Arguably, since the number of #SK_HmapStatImpl instances will
	 *  	_probably_ be negligible in most use cases, this memory usage
	 *  	might just be acceptable, but is this a good pattern in
	 *  	general?)
	 *
	 *
	 *  3)
	 *	Let the user set pointers to the #SK_BoxStat* structures in
	 *	#SK_HmapStatReqs, but do not expect the structures to be
	 *	initialized. Then, let SK_HmapReqs_prep () to modify these
	 *	structures and set the values within them properly.
	 *
	 *	It may be acceptable to let the user supply
	 *	partially-initialized #SK_BoxStat* structures as well, so that
	 *	the user has some degree of control over the configuration.
	 *
	 *	Pros : Let's SK_HmapReqs_prep () always ensure that the
	 *	#SK_BoxStat* structures are initialized consistently with the
	 *	rest of the contents of #SK_HmapStatReqs, while still allowing
	 *	the user to determine where these structures are allocated.
	 *
	 *	Cons : Any attempt to share these #SK_BoxStat* structures
	 *	between different #SK_HmapStatReqs will cause one configuration
	 *	to overwrite the other, which can invalidate them in a very
	 *	obscure manner (which will almost certainly cause memory access
	 *	errors).
	 *
	 *	If they can't be shared, then the user being able to determine
	 *	where they are allocated does not seem very useful, as a
	 *	different one would have to be allocated for each
	 *	#SK_HmapStatReqs.
	 *
	 *
	 *  4)
	 *	Similar to method 2), store arguments within #SK_HmapStatReqs,
	 *	but instead store pointers inside of #SK_HmapStatImpl. Then,
	 *	have #SK_HmapReqs_prep () dynamically allocate the #SK_BoxStat*
	 *	structures and set those pointers accordingly.
	 *
	 *	Pros : Again, very fool-proof because SK_HmapReqs_prep () has
	 *	full control over the #SK_BoxStat* configurations.
	 *	Additionally, if a configuration makes certain structures
	 *	unneeded, then they are simply not allocated, which saves
	 *	memory.
	 *
	 *	Cons : In all of the other methods, it is possible for the
	 *	#SK_BoxStat* structures to appear on the stack or the heap, but
	 *	this method makes that impossible; they will always be on the
	 *	heap. This means that it is basically guaranteed that the
	 *	#SK_BoxStat* structures will exhibit poor cache-locality.
	 *
	 *	Additionally, #SK_BoxStat* structures seem quite small and not
	 *	worthwhile to use heavy dynamic memory allocation functions
	 *	for. Arguably, this pollutes the heap a little bit.
	 *
	 *	Again, it is not reasonable to share the #SK_BoxStat*
	 *	structures amongst different #SK_HmapStatReqs instances here,
	 *	because it raises the question of who owns what.
	 *
	 *	It also means that SK_HmapReqs_prep () would have to return a
	 *	#SlySr instead of a #SlyDr, and there would have to be a
	 *	function to deinitialize #SK_HmapReqs instances.
	 *
	 *
	 *  5)
	 *  	Eschew the whole design of #SK_HmapStatReqs / #SK_HmapStatImpl
	 *  	as constant-sized "structs", and instead use #SK_BoxIntf to
	 *  	create variable-sized objects which contain all of the fields
	 *  	that are actually used.
	 *
	 *  	Pros : Interesting technique, _might_ be more memory efficient
	 *  	in some cases, probably a technique that can be used in many
	 *  	other designs as well
	 *
	 *  	Cons : Means that the mere act of configuring an #SK_HmapReqs
	 *  	is something that can stochastically-fail (due to the
	 *  	initialization of #SK_Box instances). Might be unergonomic to
	 *  	use; at this point such a design is very unknown.
	 *
	 *
	 *  Unless a better technique is devised, method 1) is considered
	 *  optimal.
	 */




	/*!
	 *  The #SK_BoxStatReqs to use as a basis for #key_group_stat_r. As
	 *  many fields from #key_group_stat_r_basis will be respected in
	 *  #key_group_stat_r during its calculation.
	 *
	 *  This CAN be NULL, which indicates the calculation of
	 *  #key_group_stat_r can use default values for otherwise undetermined
	 *  fields.
	 *
	 *  @warning
	 *  This CAN point to the same #SK_BoxStatReqs as #key_group_stat_r,
	 *  but that does mean it will be overwritten during the calculation of
	 *  #key_group_stat_r.
	 */

	SK_BoxStatReqs * key_group_stat_r_basis;




	/*!
	 *  The #SK_BoxStatReqs to use as a basis for #val_group_stat_r. As
	 *  many fields from #val_group_stat_r_basis will be respected in
	 *  #val_group_stat_r during its calculation.
	 *
	 *  This CAN be NULL, which indicates the calculation of
	 *  #val_group_stat_r can use default values for otherwise undetermined
	 *  fields.
	 *
	 *  @warning
	 *  This CAN point to the same #SK_BoxStatReqs as #val_group_stat_r,
	 *  but that does mean it will be overwritten during the calculation of
	 *  #val_group_stat_r.
	 */

	SK_BoxStatReqs * val_group_stat_r_basis;




	/*!
	 *  The #SK_BoxStatReqs to use as a basis for #key_bytes_stat_r. As
	 *  many fields from #key_bytes_stat_r_basis will be respected in
	 *  #key_bytes_stat_r during its calculation.
	 *
	 *  This CAN be NULL, which indicates the calculation of
	 *  #key_bytes_stat_r can use default values for otherwise undetermined
	 *  fields.
	 *
	 *  @warning
	 *  This CAN point to the same #SK_BoxStatReqs as #key_bytes_stat_r,
	 *  but that does mean it will be overwritten during the calculation of
	 *  #key_bytes_stat_r.
	 */

	SK_BoxStatReqs * key_bytes_stat_r_basis;




	/*!
	 *  The #SK_BoxStatReqs to use as a basis for #val_bytes_stat_r. As
	 *  many fields from #val_bytes_stat_r_basis will be respected in
	 *  #val_bytes_stat_r during its calculation.
	 *
	 *  This CAN be NULL, which indicates the calculation of
	 *  #val_bytes_stat_r can use default values for otherwise undetermined
	 *  fields.
	 *
	 *  @warning
	 *  This CAN point to the same #SK_BoxStatReqs as #val_bytes_stat_r,
	 *  but that does mean it will be overwritten during the calculation of
	 *  #val_bytes_stat_r.
	 */

	SK_BoxStatReqs * val_bytes_stat_r_basis;




	/*!
	 *  The #SK_BoxStatReqs associated with the storage object meant for
	 *  storing the whole group of keys.
	 *
	 *  This field MUST point to an #SK_BoxStatReqs that has been set via
	 *  SK_HmapStatReqs_calc_key_group_stat_r ().
	 *
	 *  If #key_group_stat_r does not point to a properly calculated
	 *  #SK_BoxStatReqs, then SK_HmapReqs_prep () MUST return failure.
	 */

	SK_BoxStatReqs * key_group_stat_r;




	/*!
	 *  The #SK_BoxStatImpl associated with the storage object meant for
	 *  storing the whole group of keys.
	 *
	 *  This field MUST point to an #SK_BoxStatReqs that has been set via
	 *  SK_HmapStatReqs_calc_key_group_stat_i ().
	 *
	 *  If #key_group_stat_i does not point to a properly calculated
	 *  #SK_BoxStatImpl, then SK_HmapReqs_prep () MUST return failure.
	 */

	SK_BoxStatImpl * key_group_stat_i;




	/*!
	 *  The #SK_BoxStatReqs associated with the storage object meant for
	 *  storing the whole group of values.
	 *
	 *  This field MUST point to an #SK_BoxStatReqs that has been set via
	 *  SK_HmapStatReqs_calc_val_group_stat_r ().
	 *
	 *  If #val_group_stat_r does not point to a properly calculated
	 *  #SK_BoxStatReqs, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #key_val_concat is 1, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxStatReqs * val_group_stat_r;




	/*!
	 *  The #SK_BoxStatImpl associated with the storage object meant for
	 *  storing the whole group of values.
	 *
	 *  This field MUST point to an #SK_BoxStatImpl that has been set via
	 *  SK_HmapStatReqs_calc_val_group_stat_i ().
	 *
	 *  If #val_group_stat_i does not point to a properly calculated
	 *  #SK_BoxStatImpl, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #key_val_concat is 1, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxStatImpl * val_group_stat_i;




	/*!
	 *  The #SK_BoxStatReqs associated with the storage object meant for
	 *  storing the bytes of a singular key (if #key_dyna_size_en is 1).
	 *
	 *  This field MUST point to an #SK_BoxStatReqs that has been set via
	 *  SK_HmapStatReqs_calc_key_bytes_stat_r ().
	 *
	 *  If #key_bytes_stat_r does not point to a properly calculated
	 *  #SK_BoxStatReqs, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #key_dyna_size_en is 0, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxStatReqs * key_bytes_stat_r;




	/*!
	 *  The #SK_BoxStatImpl associated with the storage object meant for
	 *  storing the bytes of a singular key (if #key_dyna_size_en is 1).
	 *
	 *  This field MUST point to an #SK_BoxStatImpl that has been set via
	 *  SK_HmapStatReqs_calc_key_bytes_stat_i ().
	 *
	 *  If #key_bytes_stat_i does not point to a properly calculated
	 *  #SK_BoxStatImpl, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #key_dyna_size_en is 0, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxStatImpl * key_bytes_stat_i;




	/*!
	 *  The #SK_BoxStatReqs associated with the storage object meant for
	 *  storing the bytes of a singular value (if #val_dyna_size_en is 1).
	 *
	 *  This field MUST point to an #SK_BoxStatReqs that has been set via
	 *  SK_HmapStatReqs_calc_val_bytes_stat_r ().
	 *
	 *  If #val_bytes_stat_r does not point to a properly calculated
	 *  #SK_BoxStatReqs, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #val_dyna_size_en is 0, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxStatReqs * val_bytes_stat_r;




	/*!
	 *  The #SK_BoxStatImpl associated with the storage object meant for
	 *  storing the bytes of a singular value (if #val_dyna_size_en is 1).
	 *
	 *  This field MUST point to an #SK_BoxStatImpl that has been set via
	 *  SK_HmapStatReqs_calc_key_bytes_stat_i ().
	 *
	 *  If #key_bytes_stat_i does not point to a properly calculated
	 *  #SK_BoxStatImpl, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #key_dyna_size_en is 0, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxStatImpl * val_bytes_stat_i;




	//FIXME : Perhaps for some of the 1-bit flags within this structure, it
	//would be a good idea to combine them into a single #i64 field named
	//"flags". While not highly important, it does save a small bit of
	//memory.

}

SK_HmapStatReqs;




/*!
 *  Used to describe a set of IMPLIED constant parameters for an instance of
 *  the data structure implementing the interface #SK_HmapIntf. These
 *  parameters MUST stay constant for the lifetime of the storage object that
 *  uses them. The values in this struct are derived from values contained in
 *  #SK_HmapStatReqs.
 *
 *  Unlike #SK_HmapStatReqs, these values are not permitted to be changed
 *  manually. These values MUST get set using SK_HmapReqs_prep ().
 *  Otherwise, the values in this struct are not guaranteed to be usable, and
 *  may cause interface operations to fail (i.e. exhibit memory access errors,
 *  attempt to access non-existant values, etc.)
 */

typedef struct SK_HmapStatImpl
{
	// /*!
	//  *  Represents if the requested configuration from the associated
	//  *  #SK_HmapStatReqs was invalid (likely due to one of the
	//  *  #SK_BoxStatReqs / #SK_BoxStatImpl fields being incorrectly
	//  *  configured).
	//  *
	//  *  If this is 0, then the associated #SK_HmapStatReqs was detected as
	//  *  valid.
	//  *
	//  *  If this is 1, then the associated #SK_HmapStatReqs was detected as
	//  *  invalid, and #SK_HmapIntf implementations MUST fail when
	//  *  initializing an #SK_Hmap instance using this #SK_HmapStatImpl
	//  *  instance.
	//  */
	//
	// int invalid_stat_r;




	/*!
	 *  Indicates whether #prim_cmp should be used or not.
	 *
	 *  If this is 1, then #prim_cmp points to a valid comparison function.
	 *
	 *  If this is 0, then #prim_cmp should not be used, and it will be
	 *  NULL.
	 */

	int prim_flag;




	/*!
	 *  A "primitive key comparison" function, which will be used to
	 *  compare keys. This is very useful for sorting purposes.
	 */

	SK_HmapIntf_prim_cmp prim_key_cmp;




	/*!
	 *  Design note : The *_inst_stat_mem fields are present here because
	 *  it is thought to be slightly faster to access these fields than it
	 *  is to call #SK_BoxIntf.inst_stat_mem () repeatedly.
	 *
	 *  I do not feel strongly either way regarding this design choice.
	 *  Perhaps these fields will be removed in a later revision.
	 */




	/*!
	 *  The size reported by #key_group_intf_hint->inst_stat_mem ().
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #key_group_intf_hint.
	 */

	size_t key_group_inst_stat_mem;




	/*!
	 *  The size reported by #val_group_intf_hint->inst_stat_mem ().
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #val_group_intf_hint.
	 */

	size_t val_group_inst_stat_mem;




	/*!
	 *  The size reported by #key_bytes_intf_hint->inst_stat_mem ().
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #key_bytes_intf_hint.
	 */

	size_t key_bytes_inst_stat_mem;




	/*!
	 *  The size reported by #val_bytes_intf_hint->inst_stat_mem ().
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #val_bytes_intf_hint.
	 */

	size_t val_bytes_inst_stat_mem;




	/*!
	 *  Whether or not #key_group_intf_hint->anc_support () reports that
	 *  anchors are supported.
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #key_group_intf_hint.
	 */

	int key_group_anc_support;




	/*!
	 *  Whether or not #val_group_intf_hint->anc_support () reports that
	 *  anchors are supported.
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #val_group_intf_hint.
	 */

	int val_group_anc_support;




	/*!
	 *  Whether or not #key_bytes_intf_hint->anc_support () reports that
	 *  anchors are supported.
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #key_bytes_intf_hint.
	 */

	int key_bytes_anc_support;




	/*!
	 *  Whether or not #val_bytes_intf_hint->anc_support () reports that
	 *  anchors are supported.
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #val_bytes_intf_hint.
	 */

	int val_bytes_anc_support;




	/*!
	 *  The size reported by #key_group_intf_hint->anc_size ().
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #key_group_intf_hint.
	 */

	size_t key_group_anc_size;




	/*!
	 *  The size reported by #val_group_intf_hint->anc_size ().
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #val_group_intf_hint.
	 */

	size_t val_group_anc_size;




	/*!
	 *  The size reported by #key_bytes_intf_hint->anc_size ().
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #key_bytes_intf_hint.
	 */

	size_t key_bytes_anc_size;




	/*!
	 *  The size reported by #val_bytes_intf_hint->anc_size ().
	 *
	 *  The implementation may choose whether or not to use this field, as
	 *  it may choose to ignore #val_bytes_intf_hint.
	 */

	size_t val_bytes_anc_size;

}

SK_HmapStatImpl;




/*!
 *  Used to describe a set of desired dynamic parameters for an instance of the
 *  data structure implementing the interface #SK_HmapIntf. These
 *  parameters are intended to be modifiable in the lifetime of the storage
 *  object.
 *
 *  While these parameters can change, there is no requirement to do so in any
 *  particular circumstance.
 *
 *  @warning
 *  The fields in this data structure CAN change during the lifetime
 *  of the storage data structure instance.
 *
 *  @warning
 *  If any change is made to this data structure, then SK_HmapReqs_prep ()
 *  MUST be called once again on the associated #SK_HmapReqs.
 *
 *  @warning
 *  If a change is made to this data structure, it must be done at a time where
 *  no function is accessing it (usually done via #SK_HmapReqs). In
 *  multi-threaded scenarios, that typically means some form of external
 *  locking is required if modification of this data structure is desired.
 */

typedef struct SK_HmapDynaReqs
{

	/*!
	 *  Indicates whether or not a smaller value (from a key-value pair)
	 *  should be extended with zero-bytes when comparing 2 values of a
	 *  different size.
	 *
	 *  Basically, if this 0, then the byte sequence "0xAA" is considered
	 *  distinct from "0xAA 0x00". If this is 1, then both byte sequences
	 *  are considered the same.
	 *
	 *  This is only relevant if #SK_HmapStatReqs.val_dyna_size_en is 1, or
	 *  if #SK_HmapStatReqs.val_dyna_size_en is 0 but one of the *_v_key ()
	 *  or *_v_kv () functions are used.
	 *
	 *  @note
	 *  The choice of whether to extend on the LSB-side or MSB-side is up
	 *  to the implementation, so long as it does it consistently for every
	 *  key.
	 *
	 *  @warning
	 *  Note that this is NOT a hint, implementations MUST respect this
	 *  field.
	 */

	size_t val_zero_extend;




	// FIXME : Is it okay to remove these fields? It seems that they are
	// not necessary now that #SK_Box's are used as the destination
	// arguments for variable-length keys and values.
	//
	// /*!
	//  *  If this is '0' and variable-sized keys are used, then when those
	//  *  keys are copied to a destination argument, the extra bytes will NOT
	//  *  be overwritten with zero-bytes.
	//  *
	//  *  If this is '1', those extra bytes WILL be overwritten with
	//  *  zero-bytes.
	//  */
	//
	// int dyna_key_dest_reset;
	//
	//
	//
	//
	// /*!
	//  *  If this is '0' and variable-sized keys are used, then when those
	//  *  keys are copied to a destination argument, the extra bytes will NOT
	//  *  be overwritten with zero-bytes.
	//  *
	//  *  If this is '1', those extra bytes WILL be overwritten with
	//  *  zero-bytes.
	//  */
	//
	// int dyna_val_dest_reset;




	/*!
	 *  The #SK_BoxDynaReqs to use as a basis for #key_group_dyna_r. As
	 *  many fields from #key_group_dyna_r_basis will be respected in
	 *  #key_group_dyna_r during its calculation.
	 *
	 *  This CAN be NULL, which indicates the calculation of
	 *  #key_group_dyna_r can use default values for otherwise undetermined
	 *  fields.
	 *
	 *  @warning
	 *  This CAN point to the same #SK_BoxDynaReqs as #key_group_dyna_r,
	 *  but that does mean it will be overwritten during the calculation of
	 *  #key_group_dyna_r.
	 */

	SK_BoxDynaReqs * key_group_dyna_r_basis;




	/*!
	 *  The #SK_BoxDynaReqs to use as a basis for #val_group_dyna_r. As
	 *  many fields from #val_group_dyna_r_basis will be respected in
	 *  #val_group_dyna_r during its calculation.
	 *
	 *  This CAN be NULL, which indicates the calculation of
	 *  #val_group_dyna_r can use default values for otherwise undetermined
	 *  fields.
	 *
	 *  @warning
	 *  This CAN point to the same #SK_BoxDynaReqs as #val_group_dyna_r,
	 *  but that does mean it will be overwritten during the calculation of
	 *  #val_group_dyna_r.
	 */

	SK_BoxDynaReqs * val_group_dyna_r_basis;




	/*!
	 *  The #SK_BoxDynaReqs to use as a basis for #key_bytes_dyna_r. As
	 *  many fields from #key_bytes_dyna_r_basis will be respected in
	 *  #key_bytes_dyna_r during its calculation.
	 *
	 *  This CAN be NULL, which indicates the calculation of
	 *  #key_bytes_dyna_r can use default values for otherwise undetermined
	 *  fields.
	 *
	 *  @warning
	 *  This CAN point to the same #SK_BoxDynaReqs as #key_bytes_dyna_r,
	 *  but that does mean it will be overwritten during the calculation of
	 *  #key_bytes_dyna_r.
	 */

	SK_BoxDynaReqs * key_bytes_dyna_r_basis;




	/*!
	 *  The #SK_BoxDynaReqs to use as a basis for #val_bytes_dyna_r. As
	 *  many fields from #val_bytes_dyna_r_basis will be respected in
	 *  #val_bytes_dyna_r during its calculation.
	 *
	 *  This CAN be NULL, which indicates the calculation of
	 *  #val_bytes_dyna_r can use default values for otherwise undetermined
	 *  fields.
	 *
	 *  @warning
	 *  This CAN point to the same #SK_BoxDynaReqs as #val_bytes_dyna_r,
	 *  but that does mean it will be overwritten during the calculation of
	 *  #val_bytes_dyna_r.
	 */

	SK_BoxDynaReqs * val_bytes_dyna_r_basis;




	/*!
	 *  The #SK_BoxDynaReqs associated with the storage object meant for
	 *  storing the whole group of keys.
	 *
	 *  This field MUST point to an #SK_BoxDynaReqs that has been set via
	 *  SK_HmapDynaReqs_calc_key_group_dyna_r ().
	 *
	 *  If #key_group_dyna_r does not point to a properly calculated
	 *  #SK_BoxDynaReqs, then SK_HmapReqs_prep () MUST return failure.
	 */

	SK_BoxDynaReqs * key_group_dyna_r;




	/*!
	 *  The #SK_BoxDynaImpl associated with the storage object meant for
	 *  storing the whole group of keys.
	 *
	 *  This field MUST point to an #SK_BoxDynaReqs that has been set via
	 *  SK_HmapDynaReqs_calc_key_group_dyna_i ().
	 *
	 *  If #key_group_dyna_i does not point to a properly calculated
	 *  #SK_BoxDynaImpl, then SK_HmapReqs_prep () MUST return failure.
	 */

	SK_BoxDynaImpl * key_group_dyna_i;




	/*!
	 *  The #SK_BoxDynaReqs associated with the storage object meant for
	 *  storing the whole group of values.
	 *
	 *  This field MUST point to an #SK_BoxDynaReqs that has been set via
	 *  SK_HmapDynaReqs_calc_val_group_dyna_r ().
	 *
	 *  If #val_group_dyna_r does not point to a properly calculated
	 *  #SK_BoxDynaReqs, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #key_val_concat is 1, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxDynaReqs * val_group_dyna_r;




	/*!
	 *  The #SK_BoxDynaImpl associated with the storage object meant for
	 *  storing the whole group of values.
	 *
	 *  This field MUST point to an #SK_BoxDynaImpl that has been set via
	 *  SK_HmapDynaReqs_calc_val_group_dyna_i ().
	 *
	 *  If #val_group_dyna_i does not point to a properly calculated
	 *  #SK_BoxDynaImpl, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #key_val_concat is 1, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxDynaImpl * val_group_dyna_i;




	/*!
	 *  The #SK_BoxDynaReqs associated with the storage object meant for
	 *  storing the bytes of a singular key (if #key_dyna_size_en is 1).
	 *
	 *  This field MUST point to an #SK_BoxDynaReqs that has been set via
	 *  SK_HmapDynaReqs_calc_key_bytes_dyna_r ().
	 *
	 *  If #key_bytes_dyna_r does not point to a properly calculated
	 *  #SK_BoxDynaReqs, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #key_dyna_size_en is 0, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxDynaReqs * key_bytes_dyna_r;




	/*!
	 *  The #SK_BoxDynaImpl associated with the storage object meant for
	 *  storing the bytes of a singular key (if #key_dyna_size_en is 1).
	 *
	 *  This field MUST point to an #SK_BoxDynaImpl that has been set via
	 *  SK_HmapDynaReqs_calc_key_bytes_dyna_i ().
	 *
	 *  If #key_bytes_dyna_i does not point to a properly calculated
	 *  #SK_BoxDynaImpl, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #key_dyna_size_en is 0, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxDynaImpl * key_bytes_dyna_i;




	/*!
	 *  The #SK_BoxDynaReqs associated with the storage object meant for
	 *  storing the bytes of a singular value (if #val_dyna_size_en is 1).
	 *
	 *  This field MUST point to an #SK_BoxDynaReqs that has been set via
	 *  SK_HmapDynaReqs_calc_val_bytes_dyna_r ().
	 *
	 *  If #val_bytes_dyna_r does not point to a properly calculated
	 *  #SK_BoxDynaReqs, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #val_dyna_size_en is 0, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxDynaReqs * val_bytes_dyna_r;




	/*!
	 *  The #SK_BoxDynaImpl associated with the storage object meant for
	 *  storing the bytes of a singular value (if #val_dyna_size_en is 1).
	 *
	 *  This field MUST point to an #SK_BoxDynaImpl that has been set via
	 *  SK_HmapDynaReqs_calc_key_bytes_dyna_i ().
	 *
	 *  If #key_bytes_dyna_i does not point to a properly calculated
	 *  #SK_BoxDynaImpl, then SK_HmapReqs_prep () MUST return failure.
	 *
	 *  @warning
	 *  If and only if #key_dyna_size_en is 0, then this field IS
	 *  permitted to be NULL, in which case SK_HmapReqs_prep () can ignore
	 *  this field instead of returning failure.
	 */

	SK_BoxDynaImpl * val_bytes_dyna_i;

}

SK_HmapDynaReqs;




/*!
 *  Used to describe a set of IMPLIED dynamic parameters for an instance of
 *  the data structure implementing the interface #SK_HmapIntf. These
 *  parmaeters CAN change during thelifetime of the storage object that uses
 *  them. The values in this struct are derived from values contained in
 *  #SK_HmapDynaReqs.
 *
 *  Unlike #SK_HmapDynaReqs, these values are not permitted to be changed
 *  manually. These values MUST get set using SK_HmapReqs_prep ().
 *  Otherwise, the values in this struct are not guaranteed to be usable, and
 *  may cause storage object operations to exhibit memory access errors.
 *
 *  @warning
 *  If a change is made to this data structure, it must be done at a time where
 *  no function is accessing it (usually done via #SK_HmapReqs). In
 *  multi-threaded scenarios, that typically means some form of external
 *  locking is required if modification of this data structure is desired.
 */

typedef struct SK_HmapDynaImpl
{

	// /*!
	//  *  Represents if the requested configuration from the associated
	//  *  #SK_HmapDynaReqs was invalid (likely due to one of the
	//  *  #SK_BoxDynaReqs / #SK_BoxDynaImpl fields being incorrectly
	//  *  configured).
	//  *
	//  *  If this is 0, then the associated #SK_HmapDynaReqs was detected as
	//  *  valid.
	//  *
	//  *  If this is 1, then the associated #SK_HmapDynaReqs was detected as
	//  *  invalid, and #SK_HmapIntf implementations MUST fail when
	//  *  initializing an #SK_Hmap instance using this #SK_HmapDynaImpl
	//  *  instance.
	//  */
	//
	// int invalid_dyna_r;




	/*!
	 *  Design note : The following #*_req fields have the side effect of
	 *  REQUIRING the #key_group_stat_r, #key_group_stat_i, etc. fields in
	 *  #SK_HmapStatReqs / #SK_HmapDynaReqs to be pointers.
	 *
	 *  To understand why, assume that the #*_stat_r, #*_stat_i, etc.
	 *  fields were #SK_BoxStatReqs, #SK_BoxStatImpl, etc. instances
	 *  directly instead of pointers.
	 *
	 *  Now, assume there were 2 #SK_HmapAllReqs instances (A) and (B).
	 *
	 *  Then,
	 *  (A.dyna_i.key_group_req.stat_r) would point to
	 *  (A.stat_r.key_group_stat_r).
	 *
	 *  If (A) were assigned to (B), then
	 *  (B.dyna_i.key_group_req.stat_r) would point to
	 *  (A.stat_r.key_group_stat_r). (Note the position of (A) and (B)).
	 *
	 *  This behavior would be highly unexpected, because if (A) leaves
	 *  memory, then (B) would become invalid.
	 *
	 *  However, this problem does not exist if the #key_group_stat_r,
	 *  #key_group_stat_i, etc. fields are pointers instead of instances
	 *  themselves.
	 */




	//FIXME : Are these fields actually good ideas, considering they only
	//contain pointers that can be determined from the rest of the
	//information contained in an #SK_HmapReqs?
	//
	//Granted, it is necessary to pass (SK_BoxReqs *) pointer arguments to
	//many functions. So it is not simply enough to know the locations of
	//the #SK_BoxStatReqs, #SK_BoxStatImpl, etc., but to actually copy that
	//information into an #SK_BoxReqs so that a pointer to it can be made.
	//
	//Ergo, these fields DO slightly improve the execution speed of
	//#SK_HmapIntf implementations at the cost of making the configuration
	//consume more memory. Is this trade-off worth it?




	/*!
	 *  An #SK_BoxReqs that points to #key_group_stat_r,
	 *  #key_group_stat_r, #key_group_dyna_r, and #key_group_dyna_i.
	 */

	SK_BoxReqs key_group_req;




	/*!
	 *  An #SK_BoxReqs that points to #val_group_stat_r,
	 *  #val_group_stat_r, #val_group_dyna_r, and #val_group_dyna_i.
	 */

	SK_BoxReqs val_group_req;




	/*!
	 *  An #SK_BoxReqs that points to #key_bytes_stat_r,
	 *  #key_bytes_stat_r, #key_bytes_dyna_r, and #key_bytes_dyna_i.
	 */

	SK_BoxReqs key_bytes_req;




	/*!
	 *  An #SK_BoxReqs that points to #val_bytes_stat_r,
	 *  #val_bytes_stat_r, #val_bytes_dyna_r, and #val_bytes_dyna_i.
	 */

	SK_BoxReqs val_bytes_req;

}

SK_HmapDynaImpl;




/*!
 *  Simply contains an #SK_HmapStatReqs, #SK_HmapStatImpl, #SK_HmapDynaReqs,
 *  and #SK_HmapDynaImpl.
 *
 *  Typically when one of the above data structures is instantiated, it's
 *  useful to instantiate all of them.
 *
 *  Note that this is NOT the data structure that is used to configure
 *  instances of the data structure that implements #SK_HmapIntf, that is
 *  #SK_HmapReqs. #SK_HmapReqs contains pointers to the sub-structures
 *  instead of the sub-structures themselves. See #SK_HmapReqs's
 *  documentation as to why this is preferable.
 */

typedef struct SK_HmapAllReqs
{
	SK_HmapStatReqs		stat_r;

	SK_HmapStatImpl		stat_i;

	SK_HmapDynaReqs		dyna_r;

	SK_HmapDynaImpl		dyna_i;
}

SK_HmapAllReqs;




/*!
 *  Contains pointers for both the parameters that are constant and the
 *  parameters that are dynamic for the lifetime of an instance of the data
 *  structure that implements #SK_HmapIntf.
 *
 *  The reason that these parameters are not stored in the instances themselves
 *  is that this reduces the per-instance cost of those objects. 1
 *  #SK_HmapReqs can describe the parameters for 1000's of
 *  #SK_HmapIntf-implementing data structure instances.
 *
 *  @warning
 *  The data structure MUST be initialized using SK_HmapReqs_prep (),
 *  otherwise it is not guaranteed to be in a coherent state.
 *
 *  An exception exists if all the sub-structures are known to be initialized,
 *  and then the pointers to the sub-structures can be set manually.
 *
 *  @note
 *  Note how this data structure contains pointers to the sub-structures
 *  instead of the sub-structures themselves. This allows for a higher-level
 *  data structure (SK_Thing) to also have (SK_ThingStatReqs, SK_ThingStatImpl,
 *  SK_ThingDynaReqs, SK_ThingDynaImpl), include (SK_HmapStatReqs,
 *  SK_HmapStatImpl, SK_HmapDynaReqs, SK_HmapDynaImpl) to be
 *  included in those data structures respectively, and then still allow for a
 *  proper #SK_HmapReqs and #SK_ThingReqs to be formed independently. I.e.
 *  the functions that accept #SK_HmapReqs are easily usable by #SK_Thing
 *  related functions.
 */

typedef struct SK_HmapReqs
{

	/*!
	 *  The parameters of the data struture instance that MUST stay
	 *  constant during its lifetime.
	 */

	const SK_HmapStatReqs *	stat_r;




	/*!
	 *  The parameters automatically implied by #r_stat.
	 *
	 *  This MUST be set by SK_HmapReqs_prep ().
	 */

	SK_HmapStatImpl *	stat_i;




	/*!
	 *  The parameters of the data structure instance that are allowed to
	 *  change during its lifetime.
	 */

	const SK_HmapDynaReqs *	dyna_r;




	/*!
	 *  The parameters automatically implied by #r_dyna.
	 *
	 *  This MUST be set by SK_HmapReqs_prep ().
	 */

	SK_HmapDynaImpl *	dyna_i;

}

SK_HmapReqs;




/*!
 *  A type (WITHOUT a definition) that represents instances of one of the data
 *  structures that implement #SK_HmapIntf.
 *
 *  This type declaration exists so that pointers like (SK_Hmap *) can be
 *  made and be differentiated from normal (void *) pointers. The memory layout
 *  of objects created via #SK_HmapIntf is determined totally by the
 *  implementation, so there is no defintiion associated with
 *  the #SK_Hmap anywhere.
 *
 *  The goal is that any instance of any data structure that implements
 *  #SK_HmapIntf should be safe to cast to #SK_Hmap (though the casting
 *  back is not necessarily safe).
 *
 *  @warning
 *  If #SK_Hmap is given a definition, then #SK_Hmap is almost
 *  certainly being used incorrectly. It should be understood that
 *  #SK_Hmap is intentionally generic with no singular memory layout. What
 *  WOULD have a singular memory layout is a singular implementation of
 *  #SK_Hmap.
 */

typedef struct SK_Hmap SK_Hmap;




/*!
 *  Defines a generic interface for data structures that implement hash-maps
 *  (AKA key-value stores, AKA dictionaries, etc.).
 *
 *  A hash-map is a data structure that stores values, but instead of
 *  associating an index with the stored value, it associates a KEY with the
 *  stored value. To access this value later on requires using the same key
 *  once again.
 *
 *  Typically, this key is the result of some sort of hash applied to the
 *  value, hence the name hash map. (However, the key does NOT have to be a
 *  hash of the value, it can be whatever the user provides.)
 *
 *  The canonical implementation of this interface is provided by #SK_Hmap_std.
 *
 *
 *
 *
 *  <b> IMPORTANT USAGE ASSUMPTIONS OF #SK_HmapIntf </b>
 *
 *  1)
 *
 *	The multi-threading guarantees of #SK_HmapIntf are very similar to
 *	those of #SK_BoxIntf.
 *
 *	Data structures that implement #SK_HmapIntf are expected to maintain
 *	these promises for multi-threading :
 *
 *	1)
 *
 *		It IS safe for multiple threads to read values simultaneously
 *		from a hash-map object using get (), even if they are accessing
 *		the same element associated with a given key.
 *
 *
 *	2)
 *
 *		It IS safe for multiple threads to simultanesouly read / write
 *		values inside of a hash-map object using get () / set (), but
 *		ONLY if the threads do not access the same element (associated
 *		with the same key) simultaneously.
 *
 *		In the case that threads might access the same values via the
 *		same key, an external locking mechanism is necessary.
 *
 *
 *	3)
 *
 *		It is NOT safe for any thread to add / remove elements from the
 *		storage object while any other thread is accessing the hash-map
 *		object.
 *
 *		In order to add / remove elements from the storage object while
 *		other threads are accessing it, an external locking mechanism
 *		is necessary.
 *
 *
 *  2)
 *
 *	Similar to the elements #SK_BoxIntf, #SK_HmapIntf expects the contents
 *	of keys / values to be classified as directly movable data.
 *
 *	This means that the keys and values may be moved around freely in
 *	by the #SK_HmapIntf implementation whenever key-value pairs are added
 *	or removed from an #SK_Hmap instance.
 *
 *
 *  3)
 *
 *	Every key that gets added to an #SK_Hmap MUST be unique in order to be
 *	stored, otherwise it must overwrite an existing key-value pair.
 *	#SK_Hmap's are not allowed to contain duplicate keys.
 *
 *	The same is NOT true for values of a key-value pair. There CAN be
 *	duplicate values stored within an #SK_Hmap.
 *
 *
 *  4)
 *
 *	In #SK_HmapIntf, there are versions of functions with the the suffixes
 *	(*_c_val), (*_c_key), (*_c_kv), and no suffix. The suffixes indicate
 *	whether the key / value is constant-sized.
 *
 *	Note that depending on which version of a function is used, some
 *	functions may return #SlyDr and some may return #SlySr. This is because
 *	requiring that either the key / value is constant-sized makes it
 *	possible to guarantee a lack of memory reallocations during set / get
 *	operations.
 *
 *
 *  5)
 *
 *	Note that whether or not the keys / values are constant-sized within a
 *	given #SK_Hmap instance, the #get_ptr* () functions still produce
 *	(SK_BoxIntf, SK_BoxReqs, SK_Box) pointers.
 *
 *	This means that even if the #SK_Hmap stores constant-sized keys /
 *	values, it STILL must be able to expose pointers to those keys / values
 *	AS #SK_Box's. In this event the #SK_BoxIntf (SK_StatArray_bif) might be
 *	desirable to use.
 *
 *	The reason for this is that even when a key or value is constant-sized,
 *	perhaps the implementation of #SK_HmapIntf might want to store the key
 *	and / or value in a series of memory locations rather than one larger
 *	memory location. This can make the memory easier to allocate when large
 *	amounts of heap fragmentation occur. Note that this reasoning is
 *	generally only relevant for very large keys / values.
 *
 *	By exposing the pointers as #SK_Box pointers, it helps facilitate
 *	accessing these keys / values correctly independently of their memory
 *	arrangement.
 *
 *	(TODO : Implement #SK_StatArray_bif, which is a non-resizable array
 *	that always has #num_stat_elems number of elements within it).
 */

typedef struct SK_HmapIntf
{

	//FIXME : This interface still needs another round of reviewing




	/*!
	 *  Calculates the static memory cost of an #SK_Hmap instance.
	 *
	 *  @param req			The #SK_HmapReqs describing the
	 *				requested parameters of the #SK_Hmap
	 *				instance
	 *
	 *  				Note that SK_HmapReqs_prep () must
	 *  				be used on #req before it used by
	 *  				functions in this interface.
	 *
	 *  @param num_bytes		Where to place the calculated static
	 *				memory cost of the #SK_Hmap instance,
	 *				in units of bytes
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* inst_stat_mem)
	(
		const SK_HmapReqs *		req,

		size_t *			num_bytes
	);




	/*!
	 *  Reports the amount of dynamically-allocated memory the #SK_Hmap
	 *  instance is currently consuming.
	 *
	 *  @warning
	 *  Note how inst_stat_mem () does NOT have a #map argument; this is
	 *  because the current state of the object is irrelevant to the amount
	 *  of static memory it consumes. That is NOT true with dynamic memory.
	 *  With that in mind, this should only be used on #map if #map has
	 *  already been initialized with init ().
	 *
	 *  @warning
	 *  If the dynamic memory cost would overflow #size, it should be set
	 *  to #SIZE_MAX and #SLY_SUCCESS should NOT be used as a return value.
	 *
	 *  FIXME : Would this rule make this function require this function to
	 *  return a #SlySr instead? Should this function even attempt to
	 *  handle #size_t overflows?
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap instance to inspect
	 *
	 *  @param size			Where to place the calculated dynamic
	 *				memory cost of the #SK_Hmap instance,
	 *				in units of bytes
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* inst_dyna_mem)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t *		size
	);




	/*!
	 *  Initializes the #SK_Hmap instance, which has had memory
	 *  allocated for it but has not had any of its fields initialized.
	 *
	 *  The function inst_stat_mem () MUST be used to determine how much
	 *  memory needs to be allocated for this instance before using
	 *  init ().
	 *
	 *  @warning
	 *  If this function is successful, the caller of this function MUST
	 *  call #deinit on #map sometime after using this function to avoid
	 *  resource leaks.
	 *
	 *  @warning
	 *  Using init () on a storage object twice without using deinit () in
	 *  between will generally cause memory access errors.
	 *
	 *  @param req			The #SK_HmapReqs describing
	 *  				the parameters of the #SK_Hmap
	 *  				instance. This should be the same #req
	 *  				argument as what was used in
	 *  				inst_stat_mem ().
	 *
	 *  				Note that init () is NOT permitted to
	 *  				modify #req in any way, it is only
	 *  				permitted to read it.
	 *
	 *  				Note that SK_HmapReqs_prep () must
	 *  				be used on #req before it used by
	 *  				functions in this interface.
	 *
	 *  @param map			The #SK_Hmap object to initialize.
	 *
	 *  				In other words, this should point to a
	 *  				memory region with the size reported by
	 *  				inst_stat_mem ().
	 *
	 *  @return			Standard status code
	 */

	SlySr (* init)
	(
		const SK_HmapReqs *		req,

		SK_Hmap *			map
	);




	/*!
	 *  Deinitializes the #SK_Hmap instance, which means deallocating all
	 *  resources (memory, files, etc.) the object uses.
	 *
	 *  @warning
	 *  This should ONLY be called on objects that were previously
	 *  initialized with #init ().
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to deinitialize
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* deinit)
	(
		const SK_HmapReqs *		req,

		SK_Hmap *			map
	);




	//TODO : Are the #found_val arguments a good idea for the #rem*()
	//functions in this interface?


	//TODO : Finish adding all of the *_by_ind () function variants inside
	//of this interface




	/*!
	 *  Reports the number of key-value pairs stored within an #SK_Hmap
	 *  instance.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param num_pairs		Where to place the number of pairs
	 *  				currently stored in #map
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_num_pairs)
	(
		const SK_BoxReqs *	brq,

		const SK_Hmap *		map,

		size_t *		num_pairs
	);




	// FIXME : Consider if this function pointer is a good idea or not
	//
	//  /*!
	//   *  Reports the number of key-value pairs stored within an #SK_Hmap
	//   *  that have a key matching #pattern up to a specified number of
	//   *  bytes.
	//   *
	//   *
	//   *  Here's an example :
	//   *
	//   *  Assume the follow arguments are given
	//   *
	//   *  pattern_len =        2
	//   *
	//   *  ((u8 *) pattern) [0] = 0xAA
	//   *  ((u8 *) pattern) [1] = 0xBB
	//   *
	//   *
	//   *  This key WILL be considered a match for #pattern
	//   *
	//   *  ((u8 *) key_ptr) [0] = 0xAA
	//   *  ((u8 *) key_ptr) [1] = 0xBB
	//   *  ((u8 *) key_ptr) [2] = 0xCC
	//   *  ((u8 *) key_ptr) [3] = 0xDD
	//   *
	//   *
	//   *  This key will NOT be considered a match for #pattern
	//   *
	//   *  ((u8 *) key_ptr) [0] = 0xFF
	//   *  ((u8 *) key_ptr) [1] = 0xBB
	//   *  ((u8 *) key_ptr) [2] = 0xCC
	//   *  ((u8 *) key_ptr) [3] = 0xDD
	//   *
	//   *
	//   *  @warning
	//   *  If (req->stat_r.key_zero_extend) is 0, then keys with fewer bytes
	//   *  than #pattern will be an automatic mismatch.
	//   *
	//   *  If it is 1, then keys will be treated as if they are zero-extend to
	//   *  the length of #pattern.
	//   *
	//   *
	//   *  @param req			See #req in init ()
	//   *
	//   *  @param map			The #SK_Hmap object to initialize
	//   *
	//   *  @param pattern_bytes	The size of pattern in bytes
	//   *
	//   *  @param pattern_bits		The length of a bit-mask to apply to
	//   *				#pattern and every key before
	//   *				evaluating if they match or not.
	//   *
	//   *				For example, if this 5, then that is
	//   *				considered equivalent to
	//   *				bitwise-AND'ing #pattern and key with
	//   *				0x0000001F.
	//   *
	//   *				This MUST be less than
	//   *				(8 * pattern_bytes).
	//   *
	//   *				(Technically, #pattern_bytes is not
	//   *				necessary with this argument, but it
	//   *				does allow for a nice sanity check).
	//   *
	//   *  @param pattern		The pattern which keys must match in
	//   *				order to be counted towards
	//   *				#num_keys_match
	//   *
	//   *  @param num_keys_match	Where to place the number of keys that
	//   *				match pattern
	//   *
	//   *  @return			Standard status code
	//   */
	//
	//  SlyDr (* match_keys)
	//  (
	//  	const SK_HmapReqs *	req,
	//
	//  	const SK_Hmap *		map,
	//
	//  	size_t			pattern_bytes,
	//
	//  	size_t			pattern_bits,
	//
	//  	void *			pattern,
	//
	//  	size_t *		num_keys_match
	//
	//  	//FIXME : What other arguments should go here?
	//  );




	/*!
	 *  Adds a key-value pair to an #SK_Hmap instance.
	 *
	 *  @note
	 *  This function can be used regardless of the value of
	 *  (req->stat_r.key_dyna_size_en) and (req->stat_r.val_dyna_size_en).
	 *
	 *  @warning
	 *  If this function fails because of a failed memory allocation, then
	 *  it must not modify the key-value pairs in #map in any way, leave
	 *  #map in a valid state, and return #SLY_BAD_ALLOC.
	 *
	 *  However, the #found* arguments can still be modified in that event.
	 *
	 *  @warning
	 *  If (req->stat_r.key_dyna_size_en == 0), then this function MUST
	 *  fail if #key is larger than (req->stat_r.key_stat_size).
	 *
	 *  Similar rules apply for (req->stat_r.val_dyna_size_en) and
	 *  (req->stat_r.val_stat_size).
	 *
	 *  @warning
	 *
	 *  FIXME : This last warning is not really necessary if a function
	 *  SK_Box_bare_assign () is made that see's both #SK_Box's as a large
	 *  group of bytes regardless of element size (similar to how
	 *  SK_Box_le_cmp () views the #SK_Box's)
	 *
	 *  #key_req and (req->dyna_i.key_bytes_req) MUST be deemed compatible
	 *  by SK_Box_assign (), or this function MUST fail.
	 *
	 *  Similar rules apply for #val_req and (req->dyna_i.val_bytes_req).
	 *
	 *  In essence, this means (key_req->stat_r.elem_size == 1) and
	 *  (val_req->stat_r.elem_size == 1) must be true.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param force		If this is 0, then if a value with the
	 *  				same key is already in #map, it will
	 *  				NOT be overwritten.
	 *
	 *				If this is 1, then if a value with the
	 *				same key is already in #map, it WILL
	 *				be overwritten.
	 *
	 *  @param key_intf		The #SK_BoxIntf to use when accessing
	 *				#key.
	 *
	 *  @param key_req		The #SK_BoxReqs to use when accessing
	 *				#key.
	 *
	 *  @param key			An #SK_Box containing bytes of the
	 *				key to add to #map.
	 *
	 *  @param val_intf		The #SK_BoxIntf to use when accessing
	 *  				#val.
	 *
	 *  @param val_req		The #SK_BoxReqs to use when accessing
	 *  				#val.
	 *
	 *  @param val			An #SK_Box containing bytes of the
	 *  				value to add to #map.
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				value with the same key was found in
	 *				#map.
	 *
	 *				If this is set to 0, a value with the
	 *				same key was NOT found.
	 *
	 *				If this is set to 1, a value with the
	 *				same key WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_intf		The #SK_BoxIntf to use when accessing
	 *  				#found_val.
	 *
	 *  				This CAN be NULL only if #found_val is
	 *  				NULL.
	 *
	 *  @param found_req		The #SK_BoxReqs to use when accessing
	 *  				#found_val.
	 *
	 *  				This CAN be NULL only if #found_val is
	 *  				NULL.
	 *
	 *  @param found_val		An #SK_Box where the bytes of the found
	 *				value should be placed (if one is
	 *				found).
	 *
	 *				If this is the same pointer as #val,
	 *				then #val MUST be written first before
	 *				the found value is placed here.
	 *
	 *				This CAN be NULL if this value is not
	 *				desired.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* add)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		int			force,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		const SK_Box *		val,

		int *			found,

		const SK_BoxIntf *	found_intf,

		const SK_BoxReqs *	found_req,

		SK_Box *		found_val
	);




	/*!
	 *  Removes a key-value pair from an #SK_Hmap instance.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param key_intf		See same argument in #add ()
	 *
	 *  @param key_req		See same argument in #add ()
	 *
	 *  @param key			See same argument in #add ()
	 *
	 *  @param found		See same argument in #add ()
	 *
	 *  @param found_intf		See same argument in #add ()
	 *
	 *  @param found_req		See same argument in #add ()
	 *
	 *  @param found_val		See same argument in #add ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* rem)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		int *			found,

		const SK_BoxIntf *	found_intf,

		const SK_BoxReqs *	found_req,

		SK_Box *		found_val
	);




	/*!
	 *  Sets the value of a key-value pair in an #SK_Hmap instance.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param key_intf		See same argument in #add ()
	 *
	 *  @param key_req		See same argument in #add ()
	 *
	 *  @param key			See same argument in #add ()
	 *
	 *  @param val_intf		See same argument in #add ()
	 *
	 *  @param val_req		See same argument in #add ()
	 *
	 *  @param val			See same argument in #add ()
	 *
	 *  @param found		See same argument in #add ()
	 *
	 *  @param found_intf		See same argument in #add ()
	 *
	 *  @param found_req		See same argument in #add ()
	 *
	 *  @param found_val		See same argument in #add ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* set)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		const SK_Box *		val,

		int *			found,

		const SK_BoxIntf *	found_intf,

		const SK_BoxReqs *	found_req,

		SK_Box *		found_val
	);




	/*!
	 *  Gets the value of a key-value pair in an #SK_Hmap instance.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param key_intf		See same argument in #add ()
	 *
	 *  @param key_req		See same argument in #add ()
	 *
	 *  @param key			See same argument in #add ()
	 *
	 *  @param found		See same argument in #add ()
	 *
	 *  @param found_intf		See same argument in #add ()
	 *
	 *  @param found_req		See same argument in #add ()
	 *
	 *  @param found_val		See same argument in #add ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* get)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		int *			found,

		const SK_BoxIntf *	found_intf,

		const SK_BoxReqs *	found_req,

		SK_Box *		found_val
	);




	/*!
	 *  Gets a pointer to the value of a key-value pair in an #SK_Hmap
	 *  instance.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param key_intf		The #SK_BoxIntf to use when accessing
	 *				#key.
	 *
	 *  @param key_req		The #SK_BoxReqs to use when accessing
	 *				#key.
	 *
	 *  @param key			An #SK_Box containing bytes of the
	 *				key to search for in #map.
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				value with the same key was found in
	 *				#map.
	 *
	 *				If this is set to 0, a value with the
	 *				same key was NOT found.
	 *
	 *				If this is set to 1, a value with the
	 *				same key WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_intf		Where to place a pointer to the
	 *				#SK_BoxIntf that should be used to
	 *				access #found_ptr.
	 *
	 *				Note, the reported #SK_BoxIntf does NOT
	 *				have to match
	 *				(req->stat_r.val_bytes_intf).
	 *
	 *  @param found_req		Where to place a pointer to the
	 *  				#SK_BoxIntf that should be used to
	 *				access #found_ptr.
	 *
	 *				Note, the reported #SK_BoxReqs does NOT
	 *				have to match
	 *				(req->dyna_i.val_bytes_req).
	 *
	 *  @param found_ptr		Where to place a pointer to the #SK_Box
	 *  				containing the value associated with
	 *  				#ind.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_ptr)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		int *			found,

		const SK_BoxIntf **	found_intf,

		const SK_BoxReqs **	found_req,

		SK_Box **		found_ptr
	);




	/*!
	 *  Removes a key-value pair from an #SK_Hmap instance by index.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param ind			The index of the key to remove.
	 *
	 *  				This MUST have a value lower than the
	 *  				#num_pairs value reported by
	 *  				#get_num_pairs ().
	 *
	 *  @param key_intf		The #SK_BoxIntf to use when accessing
	 *  				#key.
	 *
	 *  				This CAN be NULL only if #key is NULL.
	 *
	 *  @param key_req		The #SK_BoxReqs to use when accessing
	 *  				#key.
	 *
	 *				This CAN be NULL only if #key is NULL.
	 *
	 *  @param key			The #SK_Box where the bytes of the
	 *				key associated with the removed
	 *				key-value pair should be placed.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param val_intf		Same as #key_intf, but for #val
	 *
	 *  @param val_req		Same as #key_req, but for #val
	 *
	 *  @param val			The #SK_Box where the bytes of the
	 *				value associated with the removed
	 *				key-value pair should be placed.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* rem_by_ind)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		SK_Box *		val
	);




	/*!
	 *  Gets a key-value pair from an #SK_Hmap instance by index, with a
	 *  variable-sized key.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param ind			See same argument in #rem_by_ind ()
	 *
	 *  @param key			See same argument in #rem_by_ind ()
	 *
	 *  @param val_intf		See same argument in #rem_by_ind ()
	 *
	 *  @param val_req		See same argument in #rem_by_ind ()
	 *
	 *  @param val			See same argument in #rem_by_ind ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* get_by_ind)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		SK_Box *		val
	);




	/*!
	 *  Gets a pointer to a key-value pair in an #SK_Hmap instace by index.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param ind			The index of the key-value pair to
	 *				obtain pointers for.
	 *
	 *  				This MUST have a value lower than the
	 *  				#num_pairs value reported by
	 *  				#get_num_pairs ().
	 *
	 *  @param key_intf		Where to place a pointer to the
	 *				#SK_BoxIntf that should be used to
	 *				access #key_ptr.
	 *
	 *				Note, the reported #SK_BoxIntf does NOT
	 *				have to match
	 *				(req->stat_r.val_bytes_intf).
	 *
	 *  @param key_req		Where to place a pointer to the
	 *  				#SK_BoxIntf that should be used to
	 *				access #key_ptr.
	 *
	 *				Note, the reported #SK_BoxReqs does NOT
	 *				have to match
	 *				(req->dyna_i.val_bytes_req).
	 *
	 *  @param key_ptr		Where to place a pointer to the #SK_Box
	 *  				containing the key associated with
	 *  				#ind.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param val_intf		Same as #key_intf, but for the value of
	 *				the key-value pair.
	 *
	 *  @param val_reqs		Same as #key_reqs, but for the value of
	 *				the key-value pair.
	 *
	 *  @param val_ptr		Same as #key_ptr, but for the value of
	 *				the key-value pair.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_ptr_by_ind)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		const SK_BoxIntf **	key_intf,

		const SK_BoxReqs **	key_req,

		SK_Box **		key_ptr,

		const SK_BoxIntf **	val_intf,

		const SK_BoxReqs **	val_req,

		SK_Box **		val_ptr
	);



	/*!
	 *  Performs a reverse-search to get the key of a key-value pair in an
	 *  #SK_Hmap instance.
	 *
	 *  @note
	 *  The function name "opp" stands for "opposite", as in "opposite
	 *  search".
	 *
	 *  @warning
	 *  In almost every instance, trying to find a key via a value is MUCH
	 *  SLOWER than trying to find a value via a key. On large #SK_Hmap's,
	 *  this function should be considered a heavy-weight operation.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param match_sel		The number of matching key-value pairs
	 *				to skip within #map before considering
	 *				one to be a match.
	 *
	 *  @param val_intf		The #SK_BoxIntf to use when accessing
	 *  				#val.
	 *
	 *  @param val_req		The #SK_BoxReqs to use when accessing
	 *  				#val.
	 *
	 *  @param val			An #SK_Box containing bytes of the
	 *  				value to search for in #map.
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				matching value was found in #map.
	 *
	 *				If this is set to 0, a matching value
	 *				was NOT found.
	 *
	 *				If this is set to 1, a matching value
	 *				WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_sel		Where to place the number of matching
	 *				key-value pairs that were skipped in
	 *				finding #found_key (if it was in fact
	 *				found). If no match is found, then this
	 *				will not be modified.
	 *
	 *				If there were less matching key-value
	 *				pairs in #map than #match_sel, than
	 *				this will be given the closest possible
	 *				value to #match_sel.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_intf		The #SK_BoxIntf to use when accessing
	 *				#found_key.
	 *
	 *				This CAN be NULL only if #found_key is
	 *				NULL.
	 *
	 *  @param found_req		The #SK_BoxReqs to use when accessing
	 *  				#found_key.
	 *
	 *  				This CAN be NULL only if #found_key is
	 *  				NULL.
	 *
	 *  @param found_key		An #SK_Box where the bytes of the key
	 *  				of the matching pair should be copied
	 *  				(assuming that one was found). If no
	 *  				match is found, then this will not be
	 *  				modified.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* opp)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			match_sel,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		const SK_Box *		val,

		int *			found,

		size_t *		found_sel,

		const SK_BoxIntf *	found_intf,

		const SK_BoxReqs *	found_req,

		SK_Box *		found_val
	);




	/*!
	 *  Reports the total number of keys that are associated with a given
	 *  value contained in an #SK_Hmap instance.
	 *
	 *  This is meant to be used in conjunction with the #opp ()
	 *  function.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param val_intf		See same argument in #opp ()
	 *
	 *  @param val_req		See same argument in #opp ()
	 *
	 *  @param val			See same argument in #opp ()
	 *
	 *  @param num_keys		Where to place the total number of keys
	 *				that were found to be associated with
	 *				#val
	 *
	 *  @return			Standard status code
	 */

	SlyDr (*opp_num)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		const SK_Box *		val,

		size_t *		num_keys
	);




	/*!
	 *  Adds a key-value pair to an #SK_Hmap instance, with a
	 *  constant-sized value.
	 *
	 *  @warning
	 *  If this function fails because of a failed memory allocation, then
	 *  it must not modify the key-value pairs in #map in any way, leave
	 *  #map in a valid state, and return #SLY_BAD_ALLOC.
	 *
	 *  However, the #found* arguments can still be modified in that event.
	 *
	 *  @warning
	 *  To use this version of the function (req->stat_r.val_dyna_size_en)
	 *  MUST be equal to (0).
	 *
	 *  If this is not true, this function MUST return failure.
	 *
	 *  @warning
	 *  If (req->stat_r.key_dyna_size_en == 0), then this function MUST
	 *  fail if #key is larger than (req->stat_r.key_stat_size).
	 *
	 *  @warning
	 *  #key_req and (req->dyna_i.key_bytes_req) MUST be deemed compatible
	 *  by #SK_Box_assign (), otherwise this function MUST fail.
	 *
	 *  Generally, this means (key_req->stat_r.elem_size == 1) should be
	 *  true.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param force		If this is 0, then if a value with the
	 *  				same key is already in #map, it will
	 *  				NOT be overwritten.
	 *
	 *				If this is 1, then if a value with the
	 *				same key is already in #map, it WILL
	 *				be overwritten.
	 *
	 *  @param key_intf		The #SK_BoxIntf to use when accessing
	 *				#key
	 *
	 *  @param key_req		The #SK_BoxReqs to use when accessing
	 *				#key.
	 *
	 *  @param key			An #SK_Box containing bytes of the key
	 *				to add to #map.
	 *
	 *  @param val			The pointer to the value to add, which
	 *  				MUST have a size of
	 *  				(req->stat_r.val_stat_size) bytes.
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				value with the same key was found in
	 *				#map.
	 *
	 *				If this is set to 0, a value with the
	 *				same key was NOT found.
	 *
	 *				If this is set to 1, a value with the
	 *				same key WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_val		Where to copy the value that was found
	 *				associated with #key in #map
	 *				(assuming that one was found). If no
	 *				value is found, then this will not be
	 *				modified.
	 *
	 *				The pointed memory region MUST have a
	 *				size of (req->stat_r.val_stat_size).
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *				If this is the same pointer as #val,
	 *				then #val MUST be written first before
	 *				the found value is placed here.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* add_c_val)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		int			force,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		const void *		val,

		int *			found,

		void *			found_val
	);




	/*!
	 *  Removes a key-value pair from an #SK_Hmap instance, with a
	 *  constant-sized value.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_val () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param key_intf		See same argument in #add_c_val ()
	 *
	 *  @param key_req		See same argument in #add_c_val ()
	 *
	 *  @param key			See same argument in #add_c_val ()
	 *
	 *  @param found		See same argument in #add_c_val ()
	 *
	 *  @param found_val		See same argument in #add_c_val ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* rem_c_val)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		int *			found,

		void *			found_val
	);




	/*!
	 *  Sets the value of a key-value pair in an #SK_Hmap instance, with a
	 *  constant-sized value.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_val () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param key_intf		See same argument in #add_c_val ()
	 *
	 *  @param key_req		See same argument in #add_c_val ()
	 *
	 *  @param key			See same argument in #add_c_val ()
	 *
	 *  @param val			See same argument in #add_c_val ()
	 *
	 *  @param found		See same argument in #add_c_val ()
	 *
	 *  @param found_val		See same argument in #add_c_val ()
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* set_c_val)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		const void *		val,

		int *			found,

		void *			found_val
	);




	/*!
	 *  Gets the value of a key-value pair in an #SK_Hmap instance, with a
	 *  constant-sized value.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_val () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param key_intf		See same argument in #add_c_val ()
	 *
	 *  @param key_req		See same argument in #add_c_val ()
	 *
	 *  @param key			See same argument in #add_c_val ()
	 *
	 *  @param found		See same argument in #add_c_val ()
	 *
	 *  @param found_val		See same argument in #add_c_val ()
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_c_val)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		int *			found,

		void *			found_val
	);




	/*!
	 *  Gets a pointer to the value of a key-value pair in an #SK_Hmap
	 *  instance, with a constant-sized value.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_val () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param key_intf		The #SK_BoxIntf to use when accessing
	 *				#key
	 *
	 *  @param key_req		The #SK_BoxReqs to use when accessing
	 *				#key.
	 *
	 *  @param key			An #SK_Box containing bytes of the key
	 *				to add to #map.
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				value with the same key was found in
	 *				#map.
	 *
	 *				If this is set to 0, a value with the
	 *				same key was NOT found.
	 *
	 *				If this is set to 1, a value with the
	 *				same key WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_intf		Where to place a pointer to the
	 *				#SK_BoxIntf that should be used to
	 *				access #found_ptr.
	 *
	 *				Note, the reported #SK_BoxIntf does NOT
	 *				have to match
	 *				(req->stat_r.val_bytes_intf).
	 *
	 *  @param found_req		Where to place a pointer to the
	 *  				#SK_BoxReqs that should be used to
	 *				access #found_ptr.
	 *
	 *				Note, the reported #SK_BoxReqs does NOT
	 *				have to match
	 *				(req->dyna_i.val_bytes_req).
	 *
	 *  @param found_ptr		Where to place a pointer to the #SK_Box
	 *  				containing the key associated with
	 *  				#ind.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_ptr_c_val)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		int *			found,

		const SK_BoxIntf **	found_intf,

		const SK_BoxReqs **	found_req,

		SK_Box **		found_ptr
	);




	/*!
	 *  Removes a key-value pair from an #SK_Hmap instance by index, with a
	 *  constant-sized value.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_val () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param ind			The index of the key to remove.
	 *
	 *  				This MUST have a value lower than the
	 *  				#num_pairs value reported by
	 *  				#get_num_pairs ().
	 *
	 *  @param key_intf		The #SK_BoxIntf to use when accessing
	 *				#key.
	 *
	 *				This CAN be NULL only if #key is NULL.
	 *
	 *  @param key_req		The #SK_BoxReqs to use when accessing
	 *  				#key.
	 *
	 *  				This CAN be NULL only if #key is NULL.
	 *
	 *  @param key			The #SK_Box where the bytes of the key
	 *				associated with the removed key-value
	 *				pair should be placed.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param val			Where to place the value that was
	 *				associated with the removed key-value
	 *				pair.
	 *
	 *				The pointed memory region MUST have a
	 *				size of (req->stat_r.val_stat_size).
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* rem_by_ind_c_val)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		void *			val
	);




	/*!
	 *  Gets a key-value pair from an #SK_Hmap instance by index, with a
	 *  constant-sized value.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_val () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param ind			The index of the key to remove.
	 *
	 *  				This MUST have a value lower than the
	 *  				#num_pairs value reported by
	 *  				#get_num_pairs ().
	 *
	 *  @param key_full_size	See same argument in #rem_by_ind_c_val ()
	 *
	 *  @param key_size		See same argument in #rem_by_ind_c_val ()
	 *
	 *  @param key			See same argument in #rem_by_ind_c_val ()
	 *
	 *  @param val			See same argument in #rem_by_ind_c_val ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* get_by_ind_c_val)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		const SK_BoxIntf *	key_intf,

		const SK_BoxReqs *	key_req,

		const SK_Box *		key,

		void *			val
	);




	/*!
	 *  Gets a pointer to a key-value pair in an #SK_Hmap instance, with a
	 *  constant-sized value.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_val () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param ind			The index of the key-value pair to
	 *				obtain pointers for.
	 *
	 *  				This MUST have a value lower than the
	 *  				#num_pairs value reported by
	 *  				#get_num_pairs ().
	 *
	 *  @param key_intf		Where to place a pointer to the
	 *				#SK_BoxIntf that should be used to
	 *				access #key_ptr.
	 *
	 *				Note, the reported #SK_BoxIntf does NOT
	 *				have to match
	 *				(req->stat_r.val_bytes_intf).
	 *
	 *  @param key_req		Where to place a pointer to the
	 *  				#SK_BoxIntf that should be used to
	 *				access #key_ptr.
	 *
	 *				Note, the reported #SK_BoxReqs does NOT
	 *				have to match
	 *				(req->dyna_i.val_bytes_req).
	 *
	 *  @param key_ptr		Where to place a pointer to the #SK_Box
	 *  				containing the key associated with
	 *  				#ind.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param val_intf		Same as #key_intf, but for the value of
	 *				the key-value pair.
	 *
	 *  @param val_reqs		Same as #key_reqs, but for the value of
	 *				the key-value pair.
	 *
	 *  @param val_ptr		Same as #key_ptr, but for the value of
	 *				the key-value pair.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_ptr_by_ind_c_val)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		const SK_BoxIntf **	key_intf,

		const SK_BoxReqs **	key_req,

		SK_Box **		key_ptr,

		const SK_BoxIntf **	val_intf,

		const SK_BoxReqs **	val_req,

		SK_Box **		val_ptr
	);




	/*!
	 *  Performs a reverse-search to get the key of a key-value pair in an
	 *  #SK_Hmap instance, with the key being variable-sized.
	 *
	 *  If the value is not found, then #found will be set accordingly and
	 *  #found_key will not be modified.
	 *
	 *  @note
	 *  The function name "opp" stands for "opposite", as in "opposite
	 *  search".
	 *
	 *  @warning
	 *  In almost every instance, trying to find a key via a value is MUCH
	 *  SLOWER than trying to find a value via a key. On large #SK_Hmap's,
	 *  this function should be considered a heavy-weight operation.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_val () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param match_sel		The number of matching key-value pairs
	 *				to skip within #map before considering
	 *				one to be a match.
	 *
	 *  @param val			The pointer to the value of the
	 *  				key-value pair, which MUST have a size
	 *  				of (req->stat_r.val_stat_size) bytes.
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				matching value was found in #map.
	 *
	 *				If this is set to 0, a matching value
	 *				was NOT found.
	 *
	 *				If this is set to 1, a matching value
	 *				WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_sel		Where to place the number of matching
	 *				key-value pairs that were skipped in
	 *				finding #found_key (if it was in fact
	 *				found). If no match is found, then this
	 *				will not be modified.
	 *
	 *				If there were less matching key-value
	 *				pairs in #map than #match_sel, than
	 *				this will be given the closest possible
	 *				value to #match_sel.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_intf		The #SK_BoxIntf to use when accessing
	 *				#found_key.
	 *
	 *				This CAN be NULL only if #found_key is
	 *				NULL.
	 *
	 *  @param found_req		The #SK_BoxReqs to use when accessing
	 *  				#found_key.
	 *
	 *  				This CAN be NULL only if #found_key is
	 *  				NULL.
	 *
	 *  @param found_key		An #SK_Box where the bytes of the key
	 *  				of the matching pair should be copied
	 *  				(assuming that one was found). If no
	 *  				match is found, then this will not be
	 *  				modified.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* opp_c_val)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			match_sel,

		const void *		val,

		int *			found,

		size_t *		found_sel,

		const SK_BoxIntf *	found_intf,

		const SK_BoxReqs *	found_req,

		SK_Box *		found_key
	);




	/*!
	 *  See #opp_num_c_kv (), as this is logically equivalent to it. This
	 *  is merely provided for completeness' sake.
	 */

	SlyDr (*opp_num_c_val)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		void *			val,

		size_t *		num_keys
	);




	/*!
	 *  Adds a key-value pair to an #SK_Hmap instance, with a
	 *  constant-sized key.
	 *
	 *  @warning
	 *  To use this version of the function (req->stat_r.key_dyna_size_en)
	 *  MUST be equal to (0).
	 *
	 *  If this is not true, this function MUST return failure.
	 *
	 *  @warning
	 *  If this function fails because of a failed memory allocation, then
	 *  it must not modify the key-value pairs in #map in any way, leave
	 *  #map in a valid state, and return #SLY_BAD_ALLOC.
	 *
	 *  However, the #found* arguments can still be modified in that event.
	 *
	 *  @warning
	 *  If (req->stat_r.val_dyna_size_en == 0), then this function MUST
	 *  fail if #val is larger than (req->stat_r.val_stat_size).
	 *
	 *  @warning
	 *  #val_req and (req->dyna_i.val_bytes_req) MUST be deemed compatible
	 *  by SK_Box_assign (), or this function MUST fail.
	 *
	 *  In essence, this means (val_req->stat_r.elem_size == 1) must be
	 *  true.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param force		If this is 0, then if a value with the
	 *  				same key is already in #map, it will
	 *  				NOT be overwritten.
	 *
	 *				If this is 1, then if a value with the
	 *				same key is already in #map, it WILL
	 *				be overwritten.
	 *
	 *  @param key			The pointer to the key to use, which
	 *				MUST have a size of
	 *				(req->stat_r.key_stat_size) bytes.
	 *
	 *  @param val_intf		The #SK_BoxIntf to use when accessing
	 *				#val
	 *
	 *  @param val_req		The #SK_BoxReqs to use when accessing
	 *				#val
	 *
	 *  @param val			An #SK_Box containing bytes of the
	 *				value to add to #map.
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				value with the same key was found in
	 *				#map.
	 *
	 *				If this is set to 0, a value with the
	 *				same key was NOT found.
	 *
	 *				If this is set to 1, a value with the
	 *				same key WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_intf		Same as #val_intf, but for #found_val.
	 *
	 *				This CAN be NULL only if #found_val is
	 *				NULL.
	 *
	 *  @param found_req		Same as #val_req, but for #found_val.
	 *
	 *				This CAN be NULL only if #found_val is
	 *				NULL.
	 *
	 *  @param found_val		An #SK_Box where the bytes of the found
	 *				value should be placed (if one is
	 *				found).
	 *
	 *				If this is the same pointer as #val,
	 *				then #val MUST be written first before
	 *				the found value is placed here.
	 *
	 *				This CAN be NULL if this value is not
	 *				desired.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* add_c_key)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		int			force,

		const void *		key,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		const SK_Box *		val,

		int *			found,

		const SK_BoxIntf *	found_intf,

		const SK_BoxReqs *	found_req,

		SK_Box *		found_val
	);




	/*!
	 *  Removes a key-value pair from an #SK_Hmap instance, with a
	 *  constant-sized key.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_key () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param key			See same argument in add_c_key ()
	 *
	 *  @param found		See same argument in add_c_key ()
	 *
	 *  @param found_val_full_size	See same argument in add_c_key ()
	 *
	 *  @param found_val_size	See same argument in add_c_key ()
	 *
	 *  @param found_val		See same argument in add_c_key ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* rem_c_key)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const void *		key,

		int *			found,

		const SK_BoxIntf *	found_intf,

		const SK_BoxReqs *	found_req,

		SK_Box *		found_val
	);




	/*!
	 *  Sets the value of a key-value pair in an #SK_Hmap instance, with a
	 *  constant-sized key.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_key () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param key			See same argument in add_c_key ()
	 *
	 *  @param val_intf		See same argument in add_c_key ()
	 *
	 *  @param val_req		See same argument in add_c_key ()
	 *
	 *  @param val			See same argument in add_c_key ()
	 *
	 *  @param found		See same argument in add_c_key ()
	 *
	 *  @param found_intf		See same argument in add_c_key ()
	 *
	 *  @param found_req		See same argument in add_c_key ()
	 *
	 *  @param found_val		See same argument in add_c_key ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* set_c_key)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const void *		key,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		const SK_Box *		val,

		int *			found,

		const SK_BoxIntf *	found_intf,

		const SK_BoxReqs *	found_req,

		SK_Box *		found_val
	);




	/*!
	 *  Gets the value of a key-value pair in an #SK_Hmap instance, with a
	 *  constant-sized key.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_key () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param key			See same argument in add_c_key ()
	 *
	 *  @param found		See same argument in add_c_key ()
	 *
	 *  @param found_val_full_size	See same argument in add_c_key ()
	 *
	 *  @param found_val_size	See same argument in add_c_key ()
	 *
	 *  @param found_val		See same argument in add_c_key ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* get_c_key)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const void *		key,

		int *			found,

		const SK_BoxIntf *	found_intf,

		const SK_BoxReqs *	found_req,

		SK_Box *		found_val
	);




	/*!
	 *  Gets a pointer to the value of a key-value pair in an #SK_Hmap
	 *  instance, with a constant-sized key.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_key () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param key			The pointer to the key to use, which
	 *				MUST have a size of
	 *				(req->stat_r.key_stat_size) bytes.
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				value with the same key was found in
	 *				#map.
	 *
	 *				If this is set to 0, a value with the
	 *				same key was NOT found.
	 *
	 *				If this is set to 1, a value with the
	 *				same key WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_intf		Where to place a pointer to the
	 *				#SK_BoxIntf that should be used to
	 *				access #found_ptr.
	 *
	 *				Note, the reported #SK_BoxIntf does NOT
	 *				have to match
	 *				(req->stat_r.val_bytes_intf).
	 *
	 *  @param found_req		Where to place a pointer to the
	 *  				#SK_BoxIntf that should be used to
	 *				access #found_ptr.
	 *
	 *				Note, the reported #SK_BoxReqs does NOT
	 *				have to match
	 *				(req->dyna_i.val_bytes_req).
	 *
	 *  @param found_ptr		Where to place a pointer to the #SK_Box
	 *  				containing the value associated with
	 *  				#ind.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_ptr_c_key)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const void *		key,

		int *			found,

		const SK_BoxIntf **	found_intf,

		const SK_BoxReqs **	found_req,

		SK_Box **		found_ptr
	);




	/*!
	 *  Removes a key-value pair from an #SK_Hmap instance by index, with a
	 *  constant-sized value.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_key () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param ind			The index of the key to remove.
	 *
	 *  				This MUST have a value lower than the
	 *  				#num_pairs value reported by
	 *  				#get_num_pairs ().
	 *
	 *  @param key			Where to place the key that was
	 *				associated with the removed key-value
	 *				pair.
	 *
	 *				The pointed memory region MUST have a
	 *				size of (req->stat_r.key_stat_size).
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param val_intf		The #SK_BoxIntf to use when accessing
	 *  				#val.
	 *
	 *  				This CAN be NULL only if #val is NULL.
	 *
	 *  @param val_req		The #SK_BoxReqs to use when accessing
	 *  				#val.
	 *
	 *				This CAN be NULL only if #val is NULL.
	 *
	 *  @param val			The #SK_Box where the bytes of the
	 *				value associated with the removed
	 *				key-value pair should be placed.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* rem_by_ind_c_key)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		void *			key,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		SK_Box *		val
	);




	/*!
	 *  Gets a key-value pair from an #SK_Hmap instance by index, with a
	 *  constant-sized value.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_key () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param ind			See same argument in #rem_by_ind_c_key ()
	 *
	 *  @param key			See same argument in #rem_by_ind_c_key ()
	 *
	 *  @param val_intf		See same argument in #rem_by_ind_c_key ()
	 *
	 *  @param val_req		See same argument in #rem_by_ind_c_key ()
	 *
	 *  @param val			See same argument in #rem_by_ind_c_key ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* get_by_ind_c_key)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		void *			key,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		SK_Box *		val
	);




	/*!
	 *  Gets a pointer to a key-value pair in an #SK_Hmap instance, with a
	 *  constant-sized key.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_key () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param ind			The index of the key-value pair to
	 *				obtain pointers for.
	 *
	 *  				This MUST have a value lower than the
	 *  				#num_pairs value reported by
	 *  				#get_num_pairs ().
	 *
	 *  @param key_intf		Where to place a pointer to the
	 *				#SK_BoxIntf that should be used to
	 *				access #key_ptr.
	 *
	 *				Note, the reported #SK_BoxIntf does NOT
	 *				have to match
	 *				(req->stat_r.val_bytes_intf).
	 *
	 *  @param key_req		Where to place a pointer to the
	 *  				#SK_BoxIntf that should be used to
	 *				access #key_ptr.
	 *
	 *				Note, the reported #SK_BoxReqs does NOT
	 *				have to match
	 *				(req->dyna_i.val_bytes_req).
	 *
	 *  @param key_ptr		Where to place a pointer to the #SK_Box
	 *  				containing the key associated with
	 *  				#ind.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param val_intf		Same as #key_intf, but for the value of
	 *				the key-value pair.
	 *
	 *  @param val_reqs		Same as #key_reqs, but for the value of
	 *				the key-value pair.
	 *
	 *  @param val_ptr		Same as #key_ptr, but for the value of
	 *				the key-value pair.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_ptr_by_ind_c_key)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		const SK_BoxIntf **	key_intf,

		const SK_BoxReqs **	key_req,

		SK_Box **		key_ptr,

		const SK_BoxIntf **	val_intf,

		const SK_BoxReqs **	val_req,

		SK_Box **		val_ptr
	);




	/*!
	 *  Performs a reverse-search to get the key of a key-value pair in an
	 *  #SK_Hmap instance, with the value being variable-sized.
	 *
	 *  @note
	 *  The function name "opp" stands for "opposite", as in "opposite
	 *  search".
	 *
	 *  @warning
	 *  In almost every instance, trying to find a key via a value is MUCH
	 *  SLOWER than trying to find a value via a key. On large #SK_Hmap's,
	 *  this function should be considered a heavy-weight operation.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_key () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param match_sel		The number of matching key-value pairs
	 *				to skip within #map before considering
	 *				one to be a match.
	 *
	 *  @param val_intf		The #SK_BoxIntf to use when accessing
	 *				#val
	 *
	 *  @param val_req		The #SK_BoxReqs to use when accessing
	 *				#val
	 *
	 *  @param val			The #SK_Box containg the bytes of the
	 *  				value to search for in #map
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				matching value was found in #map.
	 *
	 *				If this is set to 0, a matching value
	 *				was NOT found.
	 *
	 *				If this is set to 1, a matching value
	 *				WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_sel		Where to place the number of matching
	 *				key-value pairs that were skipped in
	 *				finding #found_key (if it was in fact
	 *				found). If no match is found, then this
	 *				will not be modified.
	 *
	 *				If there were less matching key-value
	 *				pairs in #map than #match_sel, than
	 *				this will be given the closest possible
	 *				value to #match_sel.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_key		Where to copy the key of a matching
	 *				key-value pair that was found in #map
	 *				(assuming that one was found). If no
	 *				match is found, then this will not be
	 *				modified.
	 *
	 *				The pointed memory region MUST have a
	 *				size of (req->stat_r.key_stat_size).
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *				This must NOT point to the same memory
	 *				region as #val.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* opp_c_key)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			match_sel,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		const SK_Box *		val,

		int *			found,

		size_t *		found_sel,

		void *			found_key
	);




	/*!
	 *  Reports the total number of keys of a key-value pairs that are
	 *  associated with a given value contained in an #SK_Hmap instance.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_key () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param val_intf		See same argument in #opp_c_key ()
	 *
	 *  @param val_req		See same argument in #opp_c_key ()
	 *
	 *  @param val			See same argument in #opp_c_key ()
	 *
	 *  @param num_keys		Where to place the total number of keys
	 *  				that were found to be associated with
	 *  				#val
	 *
	 *  @return			Standard status code
	 */

	SlyDr (*opp_num_c_key)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const SK_BoxIntf *	val_intf,

		const SK_BoxReqs *	val_req,

		const SK_Box *		val,

		size_t *		num_keys
	);




	/*!
	 *  Adds a key-value pair to an #SK_Hmap instance.
	 *
	 *  If this function fails to add the key-value pair because
	 *  of a failed memory allocation, then it must return #SLY_BAD_ALLOC.
	 *
	 *  @warning
	 *  To use this version of the function,
	 *  (req->stat_r.key_dyna_size_en, req->stat_r.val_dyna_size_en) MUST
	 *  be equal to (0, 0).
	 *
	 *  If this is not true, this function MUST return failure.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param force		If this is 0, then if a value with the
	 *  				same key is already in #map, it will
	 *  				NOT be overwritten.
	 *
	 *				If this is 1, then if a value with the
	 *				same key is already in #map, it WILL
	 *				be overwritten.
	 *
	 *  @param key			The pointer to the key to use, which
	 *				MUST have a size of
	 *				(req->stat_r.key_stat_size) bytes.
	 *
	 *  @param val			The pointer to the value to add, which
	 *  				MUST have a size of
	 *  				(req->stat_r.val_stat_size) bytes.
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				value with the same key was found in
	 *				#map.
	 *
	 *				If this is set to 0, a value with the
	 *				same key was NOT found.
	 *
	 *				If this is set to 1, a value with the
	 *				same key WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_val		Where to copy the value that was found
	 *				associated with key in #map (assuming
	 *				that one was found). If no value is
	 *				found, then this will not be modified.
	 *
	 *				The pointed memory region MUST have a
	 *				size of (req->stat_r.val_stat_size).
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *				If this is the same pointer as #val,
	 *				then #val MUST be written first before
	 *				the found value is placed here.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* add_c_kv)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		int			force,

		const void *		key,

		const void *		val,

		int *			found,

		void *			found_val
	);




	/*!
	 *  Removes a key-value pair from an #SK_Hmap instance.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_kv () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param key			See same argument in #add_c_kv ()
	 *
	 *  @param found		See same argument in #add_c_kv ()
	 *
	 *  @param found_val		See same argument in #add_c_kv ()
	 *
	 *  @return			Standard status code
	 */

	SlySr (* rem_c_kv)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const void *		key,

		int *			found,

		void *			found_val
	);




	/*!
	 *  Sets the value of a key-value pair in an #SK_Hmap instance.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_kv () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param key			See same argument in #add_c_kv ()
	 *
	 *  @param val			See same argument in #add_c_kv ()
	 *
	 *  @param found		See same argument in #add_c_kv ()
	 *
	 *  @param found_val		See same argument in #add_c_kv ()
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* set_c_kv)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const void *		key,

		const void *		val,

		int *			found,

		void *			found_val
	);




	/*!
	 *  Gets the value of a key-value pair in an #SK_Hmap instance.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_kv () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param key			See same argument in #add_c_kv ()
	 *
	 *  @param found		See same argument in #add_c_kv ()
	 *
	 *  @param found_val		See same argument in #add_c_kv ()
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_c_kv)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const void *		key,

		int *			found,

		void *			found_val
	);




	/*!
	 *  Gets a pointer to the value of a key-value pair in an #SK_Hmap
	 *  instance.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_kv () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param key			See same argument in #add_c_kv ()
	 *
	 *  @param found		See same argument in #add_c_kv ()
	 *
	 *  @param found_intf		Where to place a pointer to the
	 *				#SK_BoxIntf that should be used to
	 *				access #found_ptr.
	 *
	 *				Note, the reported #SK_BoxIntf does NOT
	 *				have to match
	 *				(req->stat_r.val_bytes_intf).
	 *
	 *  @param found_req		Where to place a pointer to the
	 *  				#SK_BoxIntf that should be used to
	 *				access #found_ptr.
	 *
	 *				Note, the reported #SK_BoxReqs does NOT
	 *				have to match
	 *				(req->dyna_i.val_bytes_req).
	 *
	 *  @param found_ptr		Where to place a pointer to the #SK_Box
	 *  				containing the value associated with
	 *  				#key (assuming that one was found). If
	 *  				no value is found, then this will not
	 *  				be modified.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_ptr_c_kv)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		const void *		key,

		int *			found,

		const SK_BoxIntf **	found_intf,

		const SK_BoxReqs **	found_req,

		SK_Box **		found_ptr
	);




	/*!
	 *  Removes a key-value pair from an #SK_Hmap instance by index.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_kv () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to modify
	 *
	 *  @param ind			The index of the key to remove.
	 *
	 *  				This MUST have a value lower than the
	 *  				#num_pairs value reported by
	 *  				#get_num_pairs ().
	 *
	 *  @param key			Where to place the key that was
	 *				associated with the removed key-value
	 *				pair.
	 *
	 *				The pointed memory region MUST have a
	 *				size of (req->stat_r.key_stat_size).
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param val			Where to place the value that was
	 *				associated with the removed key-value
	 *				pair.
	 *
	 *				The pointed memory region MUST have a
	 *				size of (req->stat_r.val_stat_size).
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @return			Standard status code
	 */

	SlySr (* rem_by_ind_c_kv)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		void *			key,

		void *			val
	);




	/*!
	 *  Gets a key-value pair from an #SK_Hmap instance by index.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_kv () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param ind			The index of the key to remove.
	 *
	 *  				This MUST have a value lower than the
	 *  				#num_pairs value reported by
	 *  				#get_num_pairs ().
	 *
	 *  @param key			See same argument in #rem_by_ind ()
	 *
	 *  @param val			See same argument in #rem_by_ind ()
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_by_ind_c_kv)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		void *			key,

		void *			val
	);




	/*!
	 *  Gets a pointer to a key-value pair in an #SK_Hmap instance.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_kv () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param ind			The index of the key-value pair to
	 *				obtain pointers for.
	 *
	 *  				This MUST have a value lower than the
	 *  				#num_pairs value reported by
	 *  				#get_num_pairs ().
	 *
	 *  @param key_intf		Where to place a pointer to the
	 *				#SK_BoxIntf that should be used to
	 *				access #key_ptr.
	 *
	 *				Note, the reported #SK_BoxIntf does NOT
	 *				have to match
	 *				(req->stat_r.key_bytes_intf).
	 *
	 *  @param key_req		Where to place a pointer to the
	 *  				#SK_BoxIntf that should be used to
	 *				access #key_ptr.
	 *
	 *				Note, the reported #SK_BoxReqs does NOT
	 *				have to match
	 *				(req->dyna_i.key_bytes_req).
	 *
	 *  @param key_ptr		Where to place a pointer to the #SK_Box
	 *  				containing the key associated with
	 *  				#ind.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param val_intf		Same as #key_intf, but for the value of
	 *				the key-value pair.
	 *
	 *  @param val_reqs		Same as #key_reqs, but for the value of
	 *				the key-value pair.
	 *
	 *  @param val_ptr		Same as #key_ptr, but for the value of
	 *				the key-value pair.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* get_ptr_by_ind_c_kv)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			ind,

		const SK_BoxIntf **	key_intf,

		const SK_BoxReqs **	key_req,

		SK_Box **		key_ptr,

		const SK_BoxIntf **	val_intf,

		const SK_BoxReqs **	val_req,

		SK_Box **		val_ptr
	);




	/*!
	 *  Performs a reverse-search to get the key of a key-value pair in an
	 *  #SK_Hmap instance.
	 *
	 *  If the value is not found, then #found will be set accordingly and
	 *  #found_key will not be modified.
	 *
	 *  @note
	 *  The function name "opp" stands for "opposite", as in "opposite
	 *  search".
	 *
	 *  @warning
	 *  In almost every instance, trying to find a key via a value is MUCH
	 *  SLOWER than trying to find a value via a key. On large #SK_Hmap's,
	 *  this function should be considered a heavy-weight operation.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_kv () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param match_sel		The number of matching key-value pairs
	 *				to skip within #map before considering
	 *				one to be a match.
	 *
	 *  @param val			The pointer to the value of the
	 *  				key-value pair, which MUST have a size
	 *  				of (req->stat_r.val_stat_size) bytes.
	 *
	 *  @param found		Where to place a flag that indicates a
	 *				matching value was found in #map.
	 *
	 *				If this is set to 0, a matching value
	 *				was NOT found.
	 *
	 *				If this is set to 1, a matching value
	 *				WAS found.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_sel		Where to place the number of matching
	 *				key-value pairs that were skipped in
	 *				finding #found_key (if it was in fact
	 *				found). If no match is found, then this
	 *				will not be modified.
	 *
	 *				If there were less matching key-value
	 *				pairs in #map than #match_sel, than
	 *				this will be given the closest possible
	 *				value to #match_sel.
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *  @param found_key		Where to copy the key of a matching
	 *				key-value pair that was found in #map
	 *				(assuming that one was found). If no
	 *				match is found, then this will not be
	 *				modified.
	 *
	 *				The pointed memory region MUST have a
	 *				size of (req->stat_r.key_stat_size).
	 *
	 *				This CAN be set to NULL if this value
	 *				is not desired.
	 *
	 *				This must NOT point to the same memory
	 *				region as #val.
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* opp_c_kv)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		size_t			match_sel,

		const void *		val,

		int *			found,

		size_t *		found_sel,

		void *			found_key
	);




	/*!
	 *  Reports the total number of keys of a key-value pairs that are
	 *  associated with a given value contained in an #SK_Hmap instance.
	 *
	 *  This is meant to be used in conjunction with the #opp () function.
	 *
	 *  @warning
	 *  Any "note" and "warning" messages for #add_c_kv () apply to this
	 *  function as well.
	 *
	 *  @param req			See #req in init ()
	 *
	 *  @param map			The #SK_Hmap object to inspect
	 *
	 *  @param val			The pointer to the value to use, which
	 *				MUST have a size of
	 *				(req->stat_r.key_stat_size) bytes.
	 *
	 *  @param num_keys		Where to place the total number of keys
	 *  				that were found to be associated with
	 *  				#val
	 *
	 *  @return			Standard status code
	 */

	SlyDr (* opp_num_c_kv)
	(
		const SK_HmapReqs *	req,

		SK_Hmap *		map,

		void *			val,

		size_t *		num_keys
	);




	//TODO : Consider if #box_avail () and #get_box_ptr () are good ideas
	//for functions in this interface. At this time, these functions seem
	//like they specify too much / too specific behavior for
	//implementations.
	//
	//It should be noted that the availability of #box_avail () essentially
	//makes it allowable for #get_box_ptr () to be unimplemented (because
	//the implementation can just report that all the #SK_Box's are
	//unavailable).
	//
	//However, if these functions are to be expected to be used by users,
	//it seems very inconvenient (for these functions in particular) that
	//some implementations will have them available and some won't.
	//
	//It may be asked why these functions are even desired in the first
	//place, and the reason is that exposing the #SK_Box's directly can
	//allow for transposing of keys / values into other data structures
	//very easy (via functions that acccept #SK_Box's), rather than using
	//#get () repeatedly in a loop.
	//
	//However, that seems like a small benefit for the cost of largely
	//discouraging any variety in #SK_HmapIntf implementations.
	//
	//
	// /*!
	//  *  Indicates whether or not the implementation of #SK_HmapIntf is
	//  *  capable of providing a pointer to the #SK_Box's used to store keys
	//  *  and values (of key-value pairs) via #get_box_ptr ().
	//  *
	//  *  Although many sub-fields in #SK_HmapReqs are intended to support
	//  *  #SK_Box's within #SK_Hmap's, implementations of #SK_HmapIntf are
	//  *  not actually required to use #SK_Box's to store anything.
	//  *
	//  *  For this reason, this function is necessary to use to see if
	//  *  #get_box_ptr () is available in a given implementation.
	//  *
	//  *  @warning
	//  *  Note how this has no #map argument. #SK_HmapIntf implementations
	//  *  must be aware that the result of this function is dependent only on
	//  *  the configuration #req and the implementation itself; it is NOT
	//  *  dependent on the current state of any given #SK_Hmap object.
	//  *
	//  *  So, if a given implementation (configured with #req) is / is not
	//  *  capable of providing a kind of #SK_Box pointer, then it will always
	//  *  be that way when #req is used.
	//  *
	//  *  This makes the behavior of the #SK_HmapIntf implementation more
	//  *  predictable for users.
	//  *
	//  *  @param req			See #req in init ()
	//  *
	//  *  @param combo_key_val	Where to place a flag that will be 1 if
	//  *				the implementation will use a singular
	//  *				#SK_Box to store the group of keys /
	//  *				values together.
	//  *
	//  *				Generally speaking, this will be 1 if
	//  *				the implementation respects
	//  *				#req->stat_r.key_val_concat and that
	//  *				field is also 1.
	//  *
	//  *				@warning
	//  *				If this gets set to 1, then
	//  *				#key_group_box_avail MUST be set to 1,
	//  *				and #val_group_box_avail MUST be set to
	//  *				0 (assuming those arguments are
	//  *				non-NULL).
	//  *
	//  *				This argument CAN be NULL if this
	//  *				information is not desired.
	//  *
	//  *  @param key_group_box_avail	Where to place a flag that will be 1 if
	//  *				#get_box_ptr () will produce an #SK_Box
	//  *				pointer to the key group box containing
	//  *				the entire group of keys.
	//  *
	//  *				This argument CAN be NULL if this
	//  *				information is not desired.
	//  *
	//  *				@warning
	//  *				If #combo_key_val gets set to 1, then
	//  *				then it is understood that the key
	//  *				group box will also be used to store
	//  *				the group of values.
	//  *
	//  *  @param val_group_box_avail	Where to place a flag that will be 1 if
	//  *  				#get_box_ptr () will produce an #SK_Box
	//  *				pointer to the value group box containg
	//  *				the entire group of values.
	//  *
	//  *				This argument CAN be NULL if this
	//  *				information is not desired.
	//  *
	//  *  @param key_bytes_box_avail	Where to place a flag that will be 1 if
	//  *  				#get_box_ptr will produce an
	//  *  				#SK_BoxIntf that should be used to
	//  *				access the bytes of each key.
	//  *
	//  *				Otherwise, this will be set to 0,
	//  *				meaning that the bytes of each key can
	//  *				be accessed without an #SK_BoxIntf.
	//  *
	//  *				This argument CAN be NULL if this
	//  *				information is not desired.
	//  *
	//  *  @param val_bytes_box_avail  Where to place a flag that will be 1 if
	//  *  				#get_box_ptr will produce an
	//  *  				#SK_BoxIntf that should be used to
	//  *  				access the bytes of each value.
	//  *
	//  *  				Otherwise, this will be set to 0,
	//  *				meaning that the bytes of each value
	//  *				can be accessed without an #SK_BoxIntf
	//  *
	//  *				This argument CAN be NULL if this
	//  *				information is not desired.
	//  *
	//  *  @return			Standard status code
	//  */
	//
	// SlyDr (* box_avail)
	// (
	// 	const SK_HmapReqs *	req,
	//
	// 	int *			combo_key_val,
	//
	// 	int *			key_group_box_avail,
	//
	// 	int *			val_group_box_avail,
	//
	// 	int *			key_bytes_box_avail,
	//
	// 	int *			val_bytes_box_avail
	// );
	//
	//
	//
	//
	// /*!
	//  *  Produces pointers to the #SK_BoxIntf's / #SK_Box's used to store
	//  *  key-value pairs.
	//  *
	//  *  @warning
	//  *  The values produced for #key_group_intf, #key_group_arq,
	//  *  #val_group_intf, etc. do NOT have to match the associated fields
	//  *  within #req. This is because the implementation is free to ignore
	//  *  those provided values, and use one's of its own choosing instead.
	//  *
	//  *  @warning
	//  *  Performing any write operation whatsoever on #key_group_box,
	//  *  #val_group_box, #key_bytes_box, or #val_bytes_box after using this
	//  *  function may result in #map becoming corrupted.
	//  *
	//  *  @warning
	//  *  Other than #req and #map, every pointer argument in this function
	//  *  CAN be NULL if the associated information is not desired by the
	//  *  caller.
	//  *
	//  *  @warning
	//  *  This function in general will either be the easiest or hardest
	//  *  function to implement within the entire interface, depending on how
	//  *  much the implementation makes of #SK_BoxIntf's. This is largely due
	//  *  to the requirements made in the next warning.
	//  *
	//  *  @warning
	//  *  Implementations of this function MUST respect the following
	//  *  organization requirements.
	//  *
	//  *  Each element of #key_group_box is expected to be associated with
	//  *  ONLY 1 key, and each element of #val_group_box is expected to be
	//  *  associated with ONLY 1 value (of the key-value pairs).
	//  *
	//  *  For any given index #ind, the key in #key_group_box (at #ind) and
	//  *  value in #val_group_box (at #ind) WILL be considered to be part of
	//  *  the same key-value pair.
	//  *
	//  *  If #key_bytes_intf gets set to NULL (when the caller has provided a
	//  *  non-NULL argument), it is understood that the bytes of singular
	//  *  keys are stored directly within the bytes of singular elements of
	//  *  #key_group_box.
	//  *
	//  *  If #key_bytes_intf gets set to a non-NULL value (when the caller
	//  *  has provided a non-NULL argument), then the bytes of singular keys
	//  *  are stored within #SK_Box's that must be accessed via
	//  *  #key_bytes_intf.
	//  *
	//  *  (Analogous requirements apply for #val_bytes_intf and values)
	//  *
	//  *  Implementations of #SK_HmapIntf ARE allowed to store extra
	//  *  information at each element within #key_group_box and
	//  *  #val_group_box. Therefore, to get the pointer for a key, callers
	//  *  MUST use #elem_key_ptr () to convert a #key_group_box element
	//  *  pointer to a key pointer. Similarly, to get the pointer for a
	//  *  value, callers MUST use #elem_val_ptr () to convert a
	//  *  #val_group_box element pointer to a value pointer.
	//  *
	//  *  If #combo_key_val is set to 1 (when the caller has provided a
	//  *  non-NULL argument), then it is understood that the values are
	//  *  stored in #key_group_box instead of #val_group_box. In that event,
	//  *  to get the value associated with a key at a given index,
	//  *  #elem_val_ptr () should be used on an element pointer from
	//  *  #key_group_box.
	//  *
	//  *  @param req			See #req in init ()
	//  *
	//  *  @param map			The #SK_Hmap object to inspect
	//  *
	//  *  @param combo_key_val	Where to place a flag that will be 1 if
	//  *				a singular #SK_Box is used to store the
	//  *				group of keys / values together.
	//  *
	//  *				Generally speaking, this will be 1 if
	//  *				the implementation respects
	//  *				#req->stat_r.key_val_concat and that
	//  *				field is also 1.
	//  *
	//  *				@warning
	//  *				If this gets set to 1, then
	//  *				#key_group_box_avail MUST be set to 1,
	//  *				and #val_group_box_avail MUST be set to
	//  *				0 (assuming those arguments are
	//  *				non-NULL).
	//  *
	//  *  @param key_group_intf	Where to place a pointer to the
	//  *				#SK_BoxIntf used to store the group of
	//  *				keys within #map.
	//  *
	//  *				This will be set to NULL if it is not
	//  *				available.
	//  *
	//  *  @param key_group_arq	Where to place an #SK_BoxAllReqs that
	//  *				represents the configuration associated
	//  *				with #key_group_box.
	//  *
	//  *				This will be set to NULL if it is not
	//  *				available.
	//  *
	//  *  @param key_group_box	Where to place a pointer to the #SK_Box
	//  *				that is used to store the group of
	//  *				keys.
	//  *
	//  *				This will be set to NULL if it is not
	//  *				available.
	//  *
	//  *  @param val_group_intf	Where to place a pointer to the
	//  *	 			#SK_BoxIntf used to store the group of
	//  *	 			values within #map.
	//  *
	//  *	 			This will be set to NULL if it is not
	//  *		 		available.
	//  *
	//  *  @param val_group_arq	Where to place an #SK_BoxAllReqs that
	//  *	 			represents the configuration associated
	//  *				#val_group_box.
	//  *
	//  *	 			This will be set to NULL if it is not
	//  *	 			available.
	//  *
	//  *  @param val_group_box	Where to place a pointer to the #SK_Box
	//  *				that is used to store the group of
	//  *				values.
	//  *
	//  *				This will be set to NULL if it is not
	//  *				available.
	//  *
	//  *  @param key_bytes_intf	Where to place a pointer to the
	//  *				#SK_BoxIntf used to store the bytes of
	//  *				singular keys within #map.
	//  *
	//  *				This will be set to NULL if it is not
	//  *				available.
	//  *
	//  *  @param key_bytes_arq	Where to place an #SK_BoxAllReqs that
	//  *				represents the configuration associated
	//  *				with #key_bytes_box.
	//  *
	//  *				This will be set to NULL if it is not
	//  *				available.
	//  *
	//  *  @param val_bytes_intf	Where to place a pointer to the
	//  *				#SK_BoxIntf used to store the bytes of
	//  *				singular values within #map.
	//  *
	//  *				This will be set to NULL if it is not
	//  *				available.
	//  *
	//  *  @param val_bytes_arq	Where to place an #SK_BoxAllReqs that
	//  *				represents the configuration associated
	//  *				with #val_bytes_box.
	//  *
	//  *				This will be set to NULL if it is not
	//  *				available.
	//  *
	//  *  @return			Standard status code
	//  */
	//
	// SlyDr (* get_box_ptr)
	// (
	// 	const SK_HmapReqs *	req,
	//
	// 	const SK_Hmap *		map,
	//
	// 	const SK_BoxIntf **	key_group_intf,
	//
	// 	SK_BoxAllReqs *		key_group_arq,
	//
	// 	SK_Box **		key_group_box,
	//
	// 	const SK_BoxIntf **	val_group_intf,
	//
	// 	SK_BoxAllReqs *		val_group_arq,
	//
	// 	SK_Box **		val_group_box,
	//
	// 	const SK_BoxIntf **	key_bytes_intf,
	//
	// 	SK_BoxAllReqs *		key_bytes_arq,
	//
	// 	const SK_BoxIntf **	val_bytes_intf,
	//
	// 	SK_BoxAllReqs *		val_bytes_arq
	// );

}

SK_HmapIntf;




/*!
 *  This is a version of #SK_HmapIntf that makes use of just-in-time (JIT)
 *  compilation at run-time. Utilizing JIT'ing allows for storage objects to be
 *  optimized heavily for the static parameters of a given storage object.
 *
 *  Unlike #SK_HmapIntf, it must be initialized / deinitialized with
 *  *_init () / *_deinit () functions respectively. These functions will
 *  accept the same kind of #SK_HmapReqs as used by #SK_HmapIntf
 *  functions.
 */

typedef struct SK_HmapIntf SK_HmapJit;




SlyDr SK_HmapReqs_point_to_arq
(
	SK_HmapReqs *			req,

	const SK_HmapAllReqs *		arq
);




SlyDr SK_HmapStatImpl_prep
(
	const SK_HmapStatReqs *		stat_r,

	SK_HmapStatImpl *		stat_i
);




SlyDr SK_HmapDynaImpl_prep
(
	const SK_HmapStatReqs *		stat_r,

	const SK_HmapStatImpl *		stat_i,

	const SK_HmapDynaReqs *		dyna_r,

	SK_HmapDynaImpl *		dyna_i
);




SlyDr SK_HmapReqs_prep
(
	SK_HmapReqs *			req,

	const SK_HmapStatReqs *		stat_r,

	SK_HmapStatImpl *		stat_i,

	const SK_HmapDynaReqs *		dyna_r,

	SK_HmapDynaImpl *		dyna_i
);




SlyDr SK_HmapAllReqs_prep
(
	SK_HmapAllReqs *		arq,

	const SK_HmapStatReqs *		stat_r,

	const SK_HmapDynaReqs *		dyna_r
);




SlyDr SK_HmapStatImpl_sel_prim_flag
(
	const SK_HmapStatReqs *		stat_r,

	int *				prim_flag
);




SlyDr SK_HmapStatImpl_sel_prim_key_cmp
(
	const SK_HmapStatReqs *		stat_r,

	SK_HmapIntf_prim_cmp *		prim_key_cmp
);




SlyDr SK_HmapStatReqs_sanity_check
(
	const SK_HmapStatReqs *		stat_r
);




SlyDr SK_HmapDynaReqs_sanity_check
(
	const SK_HmapStatReqs *		stat_r,

	const SK_HmapDynaReqs *		dyna_r
);




SlyDr SK_HmapStatReqs_box_calc
(
	SK_HmapStatReqs *		req
);




SlyDr SK_HmapDynaReqs_box_calc
(
	const SK_HmapStatReqs *		srq,

	SK_HmapDynaReqs *		drq
);




SlySr SK_HmapStatReqs_print
(
	const SK_HmapStatReqs *		stat_r,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
);




SlySr SK_HmapStatImpl_print
(
	const SK_HmapStatImpl *		stat_i,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
);




SlySr SK_HmapDynaReqs_print
(
	const SK_HmapDynaReqs *		dyna_r,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
);




SlySr SK_HmapDynaImpl_print
(
	const SK_HmapDynaImpl *		dyna_i,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
);




SlySr SK_HmapReqs_print
(
	const SK_HmapReqs *		req,
	i64				p_depth,
	FILE *				f_ptr,
	SK_LineDeco			deco
);




SlyDr SK_HmapStatReqs_box_calc_tb
(
	i64				p_depth,
	FILE *				file,
	SK_LineDeco			deco
);




#endif //SK_HMAPINTF_H

