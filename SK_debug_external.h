/*!****************************************************************************
 *
 * @file
 * SK_debug_external.h
 *
 * The publicly accessible declarations associated with "SK_debug.c"
 *
 * Refer to the Doxygen generated documentation for information on the
 * following items.
 *
 * This file was based on ST_debug_public.h in sly-time.
 *
 *****************************************************************************/




#ifndef SK_DEBUG_EXTERNAL_H
#define SK_DEBUG_EXTERNAL_H




#include <inttypes.h>




/*!
 *  @defgroup Debugging_Level_Values
 *
 *  Valid values that a debugging level can be set to.
 *
 *  EX:
 *
 * 	//Put the program into a low-level debugging state
 *
 * 	SK_set_all_debug_levels (SK_L_DEBUG);
 *
 *  @{
 */


/*!
 *  Represents a debug level where no debugging code should be executed
 */

#define SK_NO_DEBUG		(0)


/*!
 *  Represents a debug level where a low amount of debugging code should
 *  execute
 */

#define SK_L_DEBUG		(10000)


/*!
 *  Represents a debug level where a medium amount of debugging code should
 *  execute
 */

#define SK_M_DEBUG		(20000)


/*!
 *  Represents a debug level where a high amount of debugging code should
 *  execute
 */

#define SK_H_DEBUG		(30000)


/*!
 *  @}
 */




SlyDr SK_set_all_debug_levels (uint32_t debug_level);




SlyDr SK_Addr_set_debug_level (uint32_t debug_level);




SlyDr SK_Array_set_debug_level (uint32_t debug_level);




SlyDr SK_BoxIntf_set_debug_level (uint32_t debug_level);




SlyDr SK_BoxReqsRegistry_set_debug_level (uint32_t debug_level);




SlyDr SK_HmapIntf_set_debug_level (uint32_t debug_level);




SlyDr SK_HmapStd_set_debug_level (uint32_t debug_level);




SlyDr SK_misc_set_debug_level (uint32_t debug_level);




SlyDr SK_MemIntf_set_debug_level (uint32_t debug_level);




SlyDr SK_MemStd_set_debug_level (uint32_t debug_level);




SlyDr SK_StatArray_set_debug_level (uint32_t debug_level);




SlyDr SK_Vector_set_debug_level (uint32_t debug_level);




SlyDr SK_Yoke_set_debug_level (uint32_t debug_level);




SlyDr SK_YokeSet_set_debug_level (uint32_t debug_level);




#endif //SK_DEBUG_EXTERNAL_H

