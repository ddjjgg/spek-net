/*!****************************************************************************
 *
 * @file
 * SK_HmapStd.h
 *
 * See SK_HmapStd.c for details.
 *
 *****************************************************************************/




#ifndef SK_HMAPSTD_H
#define SK_HMAPSTD_H




#include <SlyResult-0-x-x.h>




#include "SK_HmapIntf.h"




typedef struct SK_HmapStd SK_HmapStd;




SlySr SK_HmapStd_key_pos_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco
);




#endif //SK_HMAPSTD_H

