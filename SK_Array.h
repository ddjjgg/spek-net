/*!****************************************************************************
 *
 * @file
 * SK_Array.h
 *
 * Declares #SK_Array_intf and #SK_Array.
 *
 * #SK_Array_intf is a generic array implementation of #SK_BoxIntf, which
 * attempts to respect as many #SK_BoxReqs hints as possible.
 *
 *****************************************************************************/




#ifndef SK_ARRAY_H
#define SK_ARRAY_H




#include "SK_BoxIntf.h"




extern const SK_BoxIntf SK_Array_intf;




typedef struct SK_Array SK_Array;




SlySr SK_Array_set_get_tb ();




SlySr SK_Array_resize_tb ();




SlySr SK_Array_init_tb ();




#endif //SK_ARRAY_H

