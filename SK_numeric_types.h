/*!****************************************************************************
 *
 * @file
 * SK_numeric_types.h
 *
 * Some aliases for fixed-width types available in C. These are intended for
 * internal usage in this library, and not for any of the *_public.h headers.
 *
 * This file was based on ST_debug.h in sly-time.
 *
 *
 * <b> KNOWN DESIGN QUIRKS </b>
 *
 * 1)
 *	Take note that typedef's for numeric types in this file do not begin
 *	with SK_* like every other major identifier in this project. The
 *	reason behind this is the typedef's provide short names, which make for
 *	pithy variable declarations. As a result, however, these typedef's
 *	should not be publicly visible. Therefore, references such as u32 in a
 *	*_public.h header should be replaced with types like uint32_t.
 *
 *****************************************************************************/




#ifndef SK_NUMERIC_TYPES_H
#define SK_NUMERIC_TYPES_H




#include <inttypes.h>




typedef float		f32;
typedef double		f64;
typedef long double	f128;


typedef uint8_t		u8;
typedef uint16_t	u16;
typedef uint32_t	u32;
typedef uint64_t	u64;


typedef int8_t		i8;
typedef int16_t		i16;
typedef int32_t		i32;
typedef int64_t		i64;


typedef uint_fast8_t	uf8;
typedef uint_fast16_t	uf16;
typedef uint_fast32_t	uf32;
typedef uint_fast64_t	uf64;


typedef int_fast8_t	if8;
typedef int_fast16_t	if16;
typedef int_fast32_t	if32;
typedef int_fast64_t	if64;


#define PRIf32		"f"
#define PRIf64		"lf"
#define PRIf128		"Lf"




#endif //SK_NUMERIC_TYPES_H

