# UNDER CONSTRUCTION

This is an experimental networking protocol based on hierarchical addresssing.
This project is more for fun than anything else, and it is has only just
started. It will take a while before it becomes interesting, and after it
becomes interesting, it will take a while before it's not total crap.

The current goal of this project is to build a network simulator, and then to
refine a series of documents describing the protocol. After a while, the goal
will shift over towards packaging bits of logic used to generate and interpret
messages into a library. The next step after that would be implementing the
communication protocol on some microcontroller boards (which will probably be a
separate project itself).

The project is in a very ad-hoc state right now.


## Dependencies

* [sly-debug](https://gitlab.com/ddjjgg/sly-debug)

* [sly-time](https://gitlab.com/ddjjgg/sly-time) (Not used yet, but planned
  when GUI is added)

* [sly-result](https://gitlab.com/ddjjgg/sly-result)
