/*!****************************************************************************
 *
 * @file
 * SK_StatArray.h
 *
 * Declares #SK_StatArray_intf and #SK_StatArray.
 *
 * #SK_StatArray_intf is an implementation of #SK_BoxIntf, which keeps all
 * elements in a singular array that is statically-sized for the lifetime of
 * the #SK_StatArray instance.
 *
 *****************************************************************************/




#ifndef SK_STATARRAY_H
#define SK_STATARRAY_H




#include "SK_BoxIntf.h"




extern const SK_BoxIntf SK_StatArray_intf;




typedef struct SK_StatArray SK_StatArray;




SlySr SK_StatArray_trivial_tb
(
	i64			p_depth,
	FILE *			file,
	SK_LineDeco		deco
);




#endif //SK_STATARRAY_H

